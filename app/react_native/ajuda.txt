SQL DE AMIZADE
Enviar pedido de amizade:

INSERT INTO `app_amigos` (`id_usuario_1`, `id_usuario_2`, `status`, `action_id_usuario`)
VALUES (1, 2, 0, 1)

Aceita Pedido de Amizade:

UPDATE `app_amigos` SET `status` = 1, `action_id_usuario` = `2`
WHERE `id_usuario_1` = 1 AND `id_usuario_2` = 2

Verificar se e amigo:

SELECT * FROM `app_amigos`
WHERE `id_usuario_1` = 1 AND `id_usuario_2` = 2 AND `status` = 1

Pegar todos os amigos:

SELECT * FROM `app_amigos`
WHERE (`id_usuario_1` = 1 OR `id_usuario_2` = 1)
AND `status` = 1

Pegar usuario pendentes de outros usuarios:

SELECT * FROM `app_amigos`
WHERE (`id_usuario_1` = 1 OR `id_usuario_2` = 1)
AND `status` = 0
AND `action_id_usuario` != 1