import { AsyncStorage, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import RNFetchBlob from 'react-native-fetch-blob';
import Config from '../constants/config';
var moment = require('moment');
require('moment/locale/pt-br');

export const auth_facebook_login = ($dados, uploadProgress) => {
  var url = Config.url+"auth/facebook_login";
  return {
    type: 'DEFAULT',
    payload : new Promise(function(resolve, reject) {
      dados = [
        {name: "modelo", data: Platform.OS+""}
      ];

      Object.keys($dados).map(function(key, index) {
         if($dados[key]){
          if(typeof $dados[key] === 'object'){
            dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
          }else{
            dados.push({name:key, data: $dados[key]+""});
          }
         }
      });
      RNFetchBlob.fetch(
        'POST',
        url,
        {
          Authorization : "Bearer " + Sessao.auth_token,
          'Content-Type' : 'multipart/form-data'
        },
        dados
      ).uploadProgress((written, total) => {
         if(uploadProgress)
          uploadProgress(written / total);
      })
      .then(response => { return response.json();})
      .then((resp) => {
        if(resp.status == "sucesso"){
          resolve(resp);
        }else if(resp.status == "erro"){
          resolve(resp);
        }else if(resp.status == "logout"){
          Actions.auth({ type: "reset" });
        }else{
          reject({status:"erro", msg: "Um erro inesperado aconteceu."});
        }
        
      }).catch((err) => {
        reject({status:"erro", msg: JSON.stringify(err)});
      });
    }),
  }
}

export const post_criar = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"post/criar";
  if(Sessao){
      // fazer o pedido HTTP para receber todos os posts
      return {
        type: 'DEFAULT',
        payload : new Promise(function(resolve, reject) {
          Sessao = JSON.parse(Sessao);
          if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
            dados = [
              {name: "auth_token", data: Sessao.auth_token+""},
              {name: "usuario_id", data: Sessao.id+""},
              {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
              {name: "modelo", data: Platform.OS+""}
            ];
            Object.keys($dados).map(function(key, index) {
               if($dados[key]){
                if(typeof $dados[key] === 'object'){
                  dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
                }else{
                  dados.push({name:key, data: $dados[key]+""});
                }
               }
            });
            RNFetchBlob.fetch(
              'POST',
              url,
              {
                Authorization : "Bearer " + Sessao.auth_token,
                'Content-Type' : 'multipart/form-data',
              },
              dados
            ).uploadProgress((written, total) => {
               if(uploadProgress)
                uploadProgress(written / total);
            })
            .then(response => { return response.json();})
            .then((resp) => {
              if(resp.status == "sucesso"){
                resolve(resp);
              }else if(resp.status == "erro"){
                resolve(resp);
              }else if(resp.status == "logout"){
                Actions.auth({ type: "reset" });
              }else{
                reject({status:"erro", msg: "Um erro inesperado aconteceu."});
              }
              
            }).catch((err) => {
              reject({status:"erro", msg: JSON.stringify(err)});
            });
          }
        }),
      }
  }
}

export const postlike_like = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"postlike/like";
  if(Sessao){
    return {
      type: 'DEFAULT',
      payload : new Promise(function(resolve, reject) {
        Sessao = JSON.parse(Sessao);
        if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
          dados = [
            {name: "auth_token", data: Sessao.auth_token+""},
            {name: "usuario_id", data: Sessao.id+""},
            {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
            {name: "modelo", data: Platform.OS+""}
          ];

          Object.keys($dados).map(function(key, index) {
             if($dados[key]){
              if(typeof $dados[key] === 'object'){
                dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
              }else{
                dados.push({name:key, data: $dados[key]+""});
              }
             }
          });
          RNFetchBlob.fetch(
            'POST',
            url,
            {
              Authorization : "Bearer " + Sessao.auth_token,
              'Content-Type' : 'multipart/form-data'
            },
            dados
          ).uploadProgress((written, total) => {
             if(uploadProgress)
              uploadProgress(written / total);
          })
          .then(response => { return response.json();})
          .then((resp) => {
            if(resp.status == "sucesso"){
              resolve(resp);
            }else if(resp.status == "erro"){
              resolve(resp);
            }else if(resp.status == "logout"){
              Actions.auth({ type: "reset" });
            }else{
              reject({status:"erro", msg: "Um erro inesperado aconteceu."});
            }
            
          }).catch((err) => {
            reject({status:"erro", msg: JSON.stringify(err)});
          });
        }
      }),
    }
  }
}

export const postcomentario_comentar = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"postcomentario/comentar";
  if(Sessao){
    return {
      type: 'DEFAULT',
      payload : new Promise(function(resolve, reject) {
        Sessao = JSON.parse(Sessao);
        if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
          dados = [
            {name: "auth_token", data: Sessao.auth_token+""},
            {name: "usuario_id", data: Sessao.id+""},
            {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
            {name: "modelo", data: Platform.OS+""}
          ];

          Object.keys($dados).map(function(key, index) {
             if($dados[key]){
              if(typeof $dados[key] === 'object'){
                dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
              }else{
                dados.push({name:key, data: $dados[key]+""});
              }
             }
          });
          RNFetchBlob.fetch(
            'POST',
            url,
            {
              Authorization : "Bearer " + Sessao.auth_token,
              'Content-Type' : 'multipart/form-data'
            },
            dados
          ).uploadProgress((written, total) => {
             if(uploadProgress)
              uploadProgress(written / total);
          })
          .then(response => { return response.json();})
          .then((resp) => {
            if(resp.status == "sucesso"){
              resolve(resp);
            }else if(resp.status == "erro"){
              resolve(resp);
            }else if(resp.status == "logout"){
              Actions.auth({ type: "reset" });
            }else{
              reject({status:"erro", msg: "Um erro inesperado aconteceu."});
            }
            
          }).catch((err) => {
            reject({status:"erro", msg: JSON.stringify(err)});
          });
        }
      }),
    }
  }
}

export const post_timeline = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"post/timeline";
  if(Sessao){
    return {
      type: 'DEFAULT',
      payload : new Promise(function(resolve, reject) {
        Sessao = JSON.parse(Sessao);
        if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
          dados = [
            {name: "auth_token", data: Sessao.auth_token+""},
            {name: "usuario_id", data: Sessao.id+""},
            {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
            {name: "modelo", data: Platform.OS+""}
          ];

          Object.keys($dados).map(function(key, index) {
             if($dados[key]){
              if(typeof $dados[key] === 'object'){
                dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
              }else{
                dados.push({name:key, data: $dados[key]+""});
              }
             }
          });
          RNFetchBlob.fetch(
            'POST',
            url,
            {
              Authorization : "Bearer " + Sessao.auth_token,
              'Content-Type' : 'multipart/form-data'
            },
            dados
          ).uploadProgress((written, total) => {
             if(uploadProgress)
              uploadProgress(written / total);
          })
          .then(response => { return response.json();})
          .then((resp) => {
            if(resp.status == "sucesso"){
              resolve(resp);
            }else if(resp.status == "erro"){
              resolve(resp);
            }else if(resp.status == "logout"){
              Actions.auth({ type: "reset" });
            }else{
              reject({status:"erro", msg: "Um erro inesperado aconteceu."});
            }
            
          }).catch((err) => {
            reject({status:"erro", msg: JSON.stringify(err)});
          });
        }
      }),
    }
  }
}

export const post_getpost = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"post/getpost";
  if(Sessao){
    return {
      type: 'DEFAULT',
      payload : new Promise(function(resolve, reject) {
        Sessao = JSON.parse(Sessao);
        if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
          dados = [
            {name: "auth_token", data: Sessao.auth_token+""},
            {name: "usuario_id", data: Sessao.id+""},
            {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
            {name: "modelo", data: Platform.OS+""}
          ];

          Object.keys($dados).map(function(key, index) {
             if($dados[key]){
              if(typeof $dados[key] === 'object'){
                dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
              }else{
                dados.push({name:key, data: $dados[key]+""});
              }
             }
          });
          RNFetchBlob.fetch(
            'POST',
            url,
            {
              Authorization : "Bearer " + Sessao.auth_token,
              'Content-Type' : 'multipart/form-data'
            },
            dados
          ).uploadProgress((written, total) => {
             if(uploadProgress)
              uploadProgress(written / total);
          })
          .then(response => { return response.json();})
          .then((resp) => {
            if(resp.status == "sucesso"){
              resolve(resp);
            }else if(resp.status == "erro"){
              resolve(resp);
            }else if(resp.status == "logout"){
              Actions.auth({ type: "reset" });
            }else{
              reject({status:"erro", msg: "Um erro inesperado aconteceu."});
            }
            
          }).catch((err) => {
            reject({status:"erro", msg: JSON.stringify(err)});
          });
        }
      }),
    }
  }
}


export const post_excluir = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"post/excluir";
  if(Sessao){
    return {
      type: 'DEFAULT',
      payload : new Promise(function(resolve, reject) {
        Sessao = JSON.parse(Sessao);
        if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
          dados = [
            {name: "auth_token", data: Sessao.auth_token+""},
            {name: "usuario_id", data: Sessao.id+""},
            {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
            {name: "modelo", data: Platform.OS+""}
          ];

          Object.keys($dados).map(function(key, index) {
             if($dados[key]){
              if(typeof $dados[key] === 'object'){
                dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
              }else{
                dados.push({name:key, data: $dados[key]+""});
              }
             }
          });
          RNFetchBlob.fetch(
            'POST',
            url,
            {
              Authorization : "Bearer " + Sessao.auth_token,
              'Content-Type' : 'multipart/form-data'
            },
            dados
          ).uploadProgress((written, total) => {
             if(uploadProgress)
              uploadProgress(written / total);
          })
          .then(response => { return response.json();})
          .then((resp) => {
            if(resp.status == "sucesso"){
              resolve(resp);
            }else if(resp.status == "erro"){
              resolve(resp);
            }else if(resp.status == "logout"){
              Actions.auth({ type: "reset" });
            }else{
              reject({status:"erro", msg: "Um erro inesperado aconteceu."});
            }
            
          }).catch((err) => {
            reject({status:"erro", msg: JSON.stringify(err)});
          });
        }
      }),
    }
  }
}

export const postcomentario_excluir = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"postcomentario/excluir";
  if(Sessao){
    return {
      type: 'DEFAULT',
      payload : new Promise(function(resolve, reject) {
        Sessao = JSON.parse(Sessao);
        if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
          dados = [
            {name: "auth_token", data: Sessao.auth_token+""},
            {name: "usuario_id", data: Sessao.id+""},
            {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
            {name: "modelo", data: Platform.OS+""}
          ];

          Object.keys($dados).map(function(key, index) {
             if($dados[key]){
              if(typeof $dados[key] === 'object'){
                dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
              }else{
                dados.push({name:key, data: $dados[key]+""});
              }
             }
          });
          RNFetchBlob.fetch(
            'POST',
            url,
            {
              Authorization : "Bearer " + Sessao.auth_token,
              'Content-Type' : 'multipart/form-data'
            },
            dados
          ).uploadProgress((written, total) => {
             if(uploadProgress)
              uploadProgress(written / total);
          })
          .then(response => { return response.json();})
          .then((resp) => {
            if(resp.status == "sucesso"){
              resolve(resp);
            }else if(resp.status == "erro"){
              resolve(resp);
            }else if(resp.status == "logout"){
              Actions.auth({ type: "reset" });
            }else{
              reject({status:"erro", msg: "Um erro inesperado aconteceu."});
            }
            
          }).catch((err) => {
            reject({status:"erro", msg: JSON.stringify(err)});
          });
        }
      }),
    }
  }
}

export const postcomentario_get_ultimos_comentarios = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"postcomentario/get_ultimos_comentarios";
  if(Sessao){
      // fazer o pedido HTTP para receber todos os posts
      return {
        type: 'DEFAULT',
        payload : new Promise(function(resolve, reject) {
          Sessao = JSON.parse(Sessao);
          if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
            dados = [
              {name: "auth_token", data: Sessao.auth_token+""},
              {name: "usuario_id", data: Sessao.id+""},
              {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
              {name: "modelo", data: Platform.OS+""}
            ];

            Object.keys($dados).map(function(key, index) {
               if($dados[key]){
                if(typeof $dados[key] === 'object'){
                  dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
                }else{
                  dados.push({name:key, data: $dados[key]+""});
                }
               }
            });
            RNFetchBlob.fetch(
              'POST',
              url,
              {
                Authorization : "Bearer " + Sessao.auth_token,
                'Content-Type' : 'multipart/form-data',
              },
              dados
            ).uploadProgress((written, total) => {
               if(uploadProgress)
                uploadProgress(written / total);
            })
            .then(response => { return response.json();})
            .then((resp) => {
              if(resp.status == "sucesso"){
                resolve(resp);
              }else if(resp.status == "erro"){
                resolve(resp);
              }else if(resp.status == "logout"){
                Actions.auth({ type: "reset" });
              }else{
                reject({status:"erro", msg: "Um erro inesperado aconteceu."});
              }
              
            }).catch((err) => {
              reject({status:"erro", msg: JSON.stringify(err)});
            });
          }
        }),
      }
  }
}

export const usuariorelacionamento_get_relacionamentos = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"usuariorelacionamento/get_relacionamentos";
  if(Sessao){
      // fazer o pedido HTTP para receber todos os posts
      return {
        type: 'DEFAULT',
        payload : new Promise(function(resolve, reject) {
          Sessao = JSON.parse(Sessao);
          if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
            dados = [
              {name: "auth_token", data: Sessao.auth_token+""},
              {name: "usuario_id", data: Sessao.id+""},
              {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
              {name: "modelo", data: Platform.OS+""}
            ];

            Object.keys($dados).map(function(key, index) {
               if($dados[key]){
                if(typeof $dados[key] === 'object'){
                  dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
                }else{
                  dados.push({name:key, data: $dados[key]+""});
                }
               }
            });
            RNFetchBlob.fetch(
              'POST',
              url,
              {
                Authorization : "Bearer " + Sessao.auth_token,
                'Content-Type' : 'multipart/form-data',
              },
              dados
            ).uploadProgress((written, total) => {
               if(uploadProgress)
                uploadProgress(written / total);
            })
            .then(response => { return response.json();})
            .then((resp) => {
              if(resp.status == "sucesso"){
                resolve(resp);
              }else if(resp.status == "erro"){
                resolve(resp);
              }else if(resp.status == "logout"){
                Actions.auth({ type: "reset" });
              }else{
                reject({status:"erro", msg: "Um erro inesperado aconteceu."});
              }
              
            }).catch((err) => {
              reject({status:"erro", msg: JSON.stringify(err)});
            });
          }
        }),
      }
  }
}

export const usuario_pesquisar = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"usuario/pesquisar";
  if(Sessao){
      // fazer o pedido HTTP para receber todos os posts
      return {
        type: 'DEFAULT',
        payload : new Promise(function(resolve, reject) {
          Sessao = JSON.parse(Sessao);
          if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
            dados = [
              {name: "auth_token", data: Sessao.auth_token+""},
              {name: "usuario_id", data: Sessao.id+""},
              {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
              {name: "modelo", data: Platform.OS+""}
            ];

            Object.keys($dados).map(function(key, index) {
               if($dados[key]){
                if(typeof $dados[key] === 'object'){
                  dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
                }else{
                  dados.push({name:key, data: $dados[key]+""});
                }
               }
            });
            RNFetchBlob.fetch(
              'POST',
              url,
              {
                Authorization : "Bearer " + Sessao.auth_token,
                'Content-Type' : 'multipart/form-data',
              },
              dados
            ).uploadProgress((written, total) => {
               if(uploadProgress)
                uploadProgress(written / total);
            })
            .then(response => { return response.json();})
            .then((resp) => {
              if(resp.status == "sucesso"){
                resolve(resp);
              }else if(resp.status == "erro"){
                resolve(resp);
              }else if(resp.status == "logout"){
                Actions.auth({ type: "reset" });
              }else{
                reject({status:"erro", msg: "Um erro inesperado aconteceu."});
              }
              
            }).catch((err) => {
              reject({status:"erro", msg: JSON.stringify(err)});
            });
          }
        }),
      }
  }
}

export const usuario_atualizar = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"usuario/atualizar";
  if(Sessao){
      // fazer o pedido HTTP para receber todos os posts
      return {
        type: 'DEFAULT',
        payload : new Promise(function(resolve, reject) {
          Sessao = JSON.parse(Sessao);
          if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
            dados = [
              {name: "auth_token", data: Sessao.auth_token+""},
              {name: "usuario_id", data: Sessao.id+""},
              {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
              {name: "modelo", data: Platform.OS+""}
            ];

            Object.keys($dados).map(function(key, index) {
               if($dados[key]){
                if(typeof $dados[key] === 'object'){
                  dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
                }else{
                  dados.push({name:key, data: $dados[key]+""});
                }
               }
            });
            RNFetchBlob.fetch(
              'POST',
              url,
              {
                Authorization : "Bearer " + Sessao.auth_token,
                'Content-Type' : 'multipart/form-data',
              },
              dados
            ).uploadProgress((written, total) => {
               if(uploadProgress)
                uploadProgress(written / total);
            })
            .then(response => { return response.json();})
            .then((resp) => {
              if(resp.status == "sucesso"){
                resolve(resp);
              }else if(resp.status == "erro"){
                resolve(resp);
              }else if(resp.status == "logout"){
                Actions.auth({ type: "reset" });
              }else{
                reject({status:"erro", msg: "Um erro inesperado aconteceu."});
              }
              
            }).catch((err) => {
              reject({status:"erro", msg: JSON.stringify(err)});
            });
          }
        }),
      }
  }
}

export const usuario_get = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"usuario/get";
  if(Sessao){
      // fazer o pedido HTTP para receber todos os posts
      return {
        type: 'DEFAULT',
        payload : new Promise(function(resolve, reject) {
          Sessao = JSON.parse(Sessao);
          if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
            dados = [
              {name: "auth_token", data: Sessao.auth_token+""},
              {name: "usuario_id", data: Sessao.id+""},
              {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
              {name: "modelo", data: Platform.OS+""}
            ];
            if(!$dados.usuario_perfil_id_search)
              dados.push({name:"usuario_perfil_id_search", data: Sessao.usuario_perfil_id.id+""});

            Object.keys($dados).map(function(key, index) {
               if($dados[key]){
                if(typeof $dados[key] === 'object'){
                  dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
                }else{
                  dados.push({name:key, data: $dados[key]+""});
                }
               }
            });
            RNFetchBlob.fetch(
              'POST',
              url,
              {
                Authorization : "Bearer " + Sessao.auth_token,
                'Content-Type' : 'multipart/form-data',
              },
              dados
            ).uploadProgress((written, total) => {
               if(uploadProgress)
                uploadProgress(written / total);
            })
            .then(response => { return response.json();})
            .then((resp) => {
              if(resp.status == "sucesso"){
                resolve(resp);
              }else if(resp.status == "erro"){
                resolve(resp);
              }else if(resp.status == "logout"){
                Actions.auth({ type: "reset" });
              }else{
                reject({status:"erro", msg: "Um erro inesperado aconteceu."});
              }
              
            }).catch((err) => {
              reject({status:"erro", msg: JSON.stringify(err)});
            });
          }
        }),
      }
  }
}

export const postdenuncia_denunciar = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"postdenuncia/denunciar";
  if(Sessao){
      // fazer o pedido HTTP para receber todos os posts
      return {
        type: 'DEFAULT',
        payload : new Promise(function(resolve, reject) {
          Sessao = JSON.parse(Sessao);
          if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
            dados = [
              {name: "auth_token", data: Sessao.auth_token+""},
              {name: "usuario_id", data: Sessao.id+""},
              {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
              {name: "modelo", data: Platform.OS+""}
            ];

            Object.keys($dados).map(function(key, index) {
               if($dados[key]){
                if(typeof $dados[key] === 'object'){
                  dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
                }else{
                  dados.push({name:key, data: $dados[key]+""});
                }
               }
            });
            RNFetchBlob.fetch(
              'POST',
              url,
              {
                Authorization : "Bearer " + Sessao.auth_token,
                'Content-Type' : 'multipart/form-data',
              },
              dados
            ).uploadProgress((written, total) => {
               if(uploadProgress)
                uploadProgress(written / total);
            })
            .then(response => { return response.json();})
            .then((resp) => {
              if(resp.status == "sucesso"){
                resolve(resp);
              }else if(resp.status == "erro"){
                resolve(resp);
              }else if(resp.status == "logout"){
                Actions.auth({ type: "reset" });
              }else{
                reject({status:"erro", msg: "Um erro inesperado aconteceu."});
              }
              
            }).catch((err) => {
              reject({status:"erro", msg: JSON.stringify(err)});
            });
          }
        }),
      }
  }
}

export const usuarioamigo_seguir = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"usuarioamigo/seguir";
  if(Sessao){
      // fazer o pedido HTTP para receber todos os posts
      return {
        type: 'DEFAULT',
        payload : new Promise(function(resolve, reject) {
          Sessao = JSON.parse(Sessao);
          if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
            dados = [
              {name: "auth_token", data: Sessao.auth_token+""},
              {name: "usuario_id", data: Sessao.id+""},
              {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
              {name: "modelo", data: Platform.OS+""}
            ];

            Object.keys($dados).map(function(key, index) {
               if($dados[key]){
                if(typeof $dados[key] === 'object'){
                  dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
                }else{
                  dados.push({name:key, data: $dados[key]+""});
                }
               }
            });
            RNFetchBlob.fetch(
              'POST',
              url,
              {
                Authorization : "Bearer " + Sessao.auth_token,
                'Content-Type' : 'multipart/form-data',
              },
              dados
            ).uploadProgress((written, total) => {
               if(uploadProgress)
                uploadProgress(written / total);
            })
            .then(response => { return response.json();})
            .then((resp) => {
              if(resp.status == "sucesso"){
                resolve(resp);
              }else if(resp.status == "erro"){
                resolve(resp);
              }else if(resp.status == "logout"){
                Actions.auth({ type: "reset" });
              }else{
                reject({status:"erro", msg: "Um erro inesperado aconteceu."});
              }
              
            }).catch((err) => {
              reject({status:"erro", msg: JSON.stringify(err)});
            });
          }
        }),
      }
  }
}

export const usuarioamigo_aceitar = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"usuarioamigo/aceitar";
  if(Sessao){
      // fazer o pedido HTTP para receber todos os posts
      return {
        type: 'DEFAULT',
        payload : new Promise(function(resolve, reject) {
          Sessao = JSON.parse(Sessao);
          if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
            dados = [
              {name: "auth_token", data: Sessao.auth_token+""},
              {name: "usuario_id", data: Sessao.id+""},
              {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
              {name: "modelo", data: Platform.OS+""}
            ];

            Object.keys($dados).map(function(key, index) {
               if($dados[key]){
                if(typeof $dados[key] === 'object'){
                  dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
                }else{
                  dados.push({name:key, data: $dados[key]+""});
                }
               }
            });
            RNFetchBlob.fetch(
              'POST',
              url,
              {
                Authorization : "Bearer " + Sessao.auth_token,
                'Content-Type' : 'multipart/form-data',
              },
              dados
            ).uploadProgress((written, total) => {
               if(uploadProgress)
                uploadProgress(written / total);
            })
            .then(response => { return response.json();})
            .then((resp) => {
              if(resp.status == "sucesso"){
                resolve(resp);
              }else if(resp.status == "erro"){
                resolve(resp);
              }else if(resp.status == "logout"){
                Actions.auth({ type: "reset" });
              }else{
                reject({status:"erro", msg: "Um erro inesperado aconteceu."});
              }
              
            }).catch((err) => {
              reject({status:"erro", msg: JSON.stringify(err)});
            });
          }
        }),
      }
  }
}

export const usuarionotificacao_get = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"usuarionotificacao/get";
  if(Sessao){
      // fazer o pedido HTTP para receber todos os posts
      return {
        type: 'DEFAULT',
        payload : new Promise(function(resolve, reject) {
          Sessao = JSON.parse(Sessao);
          if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
            dados = [
              {name: "auth_token", data: Sessao.auth_token+""},
              {name: "usuario_id", data: Sessao.id+""},
              {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
              {name: "modelo", data: Platform.OS+""}
            ];

            Object.keys($dados).map(function(key, index) {
               if($dados[key]){
                if(typeof $dados[key] === 'object'){
                  dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
                }else{
                  dados.push({name:key, data: $dados[key]+""});
                }
               }
            });
            RNFetchBlob.fetch(
              'POST',
              url,
              {
                Authorization : "Bearer " + Sessao.auth_token,
                'Content-Type' : 'multipart/form-data',
              },
              dados
            ).uploadProgress((written, total) => {
               if(uploadProgress)
                uploadProgress(written / total);
            })
            .then(response => { return response.json();})
            .then((resp) => {
              if(resp.status == "sucesso"){
                resolve(resp);
              }else if(resp.status == "erro"){
                resolve(resp);
              }else if(resp.status == "logout"){
                Actions.auth({ type: "reset" });
              }else{
                reject({status:"erro", msg: "Um erro inesperado aconteceu."});
              }
              
            }).catch((err) => {
              reject({status:"erro", msg: JSON.stringify(err)});
            });
          }
        }),
      }
  }
}

export const usuarionotificacao_get_total = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"usuarionotificacao/get_total";
  if(Sessao){
      // fazer o pedido HTTP para receber todos os posts
      return {
        type: 'DEFAULT',
        payload : new Promise(function(resolve, reject) {
          Sessao = JSON.parse(Sessao);
          if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
            dados = [
              {name: "auth_token", data: Sessao.auth_token+""},
              {name: "usuario_id", data: Sessao.id+""},
              {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
              {name: "modelo", data: Platform.OS+""}
            ];

            Object.keys($dados).map(function(key, index) {
               if($dados[key]){
                if(typeof $dados[key] === 'object'){
                  dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
                }else{
                  dados.push({name:key, data: $dados[key]+""});
                }
               }
            });
            RNFetchBlob.fetch(
              'POST',
              url,
              {
                Authorization : "Bearer " + Sessao.auth_token,
                'Content-Type' : 'multipart/form-data',
              },
              dados
            ).uploadProgress((written, total) => {
               if(uploadProgress)
                uploadProgress(written / total);
            })
            .then(response => { return response.json();})
            .then((resp) => {
              if(resp.status == "sucesso"){
                resolve(resp);
              }else if(resp.status == "erro"){
                resolve(resp);
              }else if(resp.status == "logout"){
                Actions.auth({ type: "reset" });
              }else{
                reject({status:"erro", msg: "Um erro inesperado aconteceu."});
              }
              
            }).catch((err) => {
              reject({status:"erro", msg: JSON.stringify(err)});
            });
          }
        }),
      }
  }
}

export const usuarionotificacao_excluir = async ($dados, uploadProgress) => {
  var Sessao = await AsyncStorage.getItem("sessao");
  var url = Config.url+"usuarionotificacao/excluir";
  if(Sessao){
      // fazer o pedido HTTP para receber todos os posts
      return {
        type: 'DEFAULT',
        payload : new Promise(function(resolve, reject) {
          Sessao = JSON.parse(Sessao);
          if(Sessao.usuario_perfil_id.id && Sessao.auth_token && Sessao.id){
            dados = [
              {name: "auth_token", data: Sessao.auth_token+""},
              {name: "usuario_id", data: Sessao.id+""},
              {name: "usuario_perfil_id", data: Sessao.usuario_perfil_id.id+""},
              {name: "modelo", data: Platform.OS+""}
            ];

            Object.keys($dados).map(function(key, index) {
               if($dados[key]){
                if(typeof $dados[key] === 'object'){
                  dados.push({ name : key, type:$dados[key].type, filename : $dados[key].name, data : RNFetchBlob.wrap($dados[key].uri) });
                }else{
                  dados.push({name:key, data: $dados[key]+""});
                }
               }
            });
            RNFetchBlob.fetch(
              'POST',
              url,
              {
                Authorization : "Bearer " + Sessao.auth_token,
                'Content-Type' : 'multipart/form-data',
              },
              dados
            ).uploadProgress((written, total) => {
               if(uploadProgress)
                uploadProgress(written / total);
            })
            .then(response => { return response.json();})
            .then((resp) => {
              if(resp.status == "sucesso"){
                resolve(resp);
              }else if(resp.status == "erro"){
                resolve(resp);
              }else if(resp.status == "logout"){
                Actions.auth({ type: "reset" });
              }else{
                reject({status:"erro", msg: "Um erro inesperado aconteceu."});
              }
              
            }).catch((err) => {
              reject({status:"erro", msg: JSON.stringify(err)});
            });
          }
        }),
      }
  }
}