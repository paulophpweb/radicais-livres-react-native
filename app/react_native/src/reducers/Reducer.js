import { Actions } from 'react-native-router-flux';
import { AsyncStorage } from 'react-native';
export default (state = {}, action) => {

  switch (action.type) {
    case 'DEFAULT':
      if(action.payload.data){
        // o ... pega os dados do state e concatena com o retorno do get
        return { ...state, data: action.payload.data};
      }else{
        return { ...state, data: {status:"erro", msg: action.payload.msg}};
      }
    default:
      return state;
  }
};
