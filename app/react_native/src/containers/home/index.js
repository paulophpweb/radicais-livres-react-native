import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, AsyncStorage, FlatList, Dimensions, Keyboard, WebView, Alert, AlertIOS, Platform } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Animatable from 'react-native-animatable';
var {FBLoginManager} = require('react-native-facebook-login');
var moment = require('moment');
require('moment/locale/pt-br');
import Icon from 'react-native-vector-icons/MaterialIcons';
import Iconz from 'react-native-vector-icons/Ionicons';
import VideoPlayer from 'react-native-video-player';
import Modal from 'react-native-modal';
import { Actions } from 'react-native-router-flux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RNFetchBlob from 'react-native-fetch-blob';
import * as mime from 'react-native-mime-types';
import Share, {ShareSheet, Button} from 'react-native-share';
import Prompt from 'react-native-prompt';
import { SideMenu, List, ListItem } from 'react-native-elements';
import Image from '../../components/fast_image/FastImage'
import { 
  post_timeline, 
  postlike_like, 
  post_getpost, 
  post_excluir, 
  postcomentario_excluir, 
  postcomentario_get_ultimos_comentarios,
  postdenuncia_denunciar
} from '../../actions';
import Uteis from '../../lib/Uteis';
import Body from '../../components/ajax_body';
import Header from '../../components/header';
import Btn from '../../components/btn';
import Input from '../../components/inputs';
import Config from '../../constants/config';
import Template from '../../constants/cores';
import styles from './style';
var {height: deviceHeight, width:deviceWidth} = Dimensions.get("window");

class Index extends Component {
  _keyExtractor = (item, index) => index;

  constructor(props) {
    super(props);
    this.state = {
      token: "",
      posts: [],
      novidade: [],
      sessao:"",
      body_loading:true,
      refreshing:false,
      footer_loading:false,
      footer_loading_break:false,
      body_erro:"",
      url : "",
      camera:false,
      form_post:false,
      form_data_post_image:null,
      form_data_post_video:null,
      form_data_post_texto:null,
      post_like:[],
      pagina:1,
      menu: [],
      token_firebase:"",
      denuncia:false,
      post_denuncia_id:"",
      latitude_usuario:"",
      longitude_usuario:"",
    }

  }

  async componentDidMount(){
    var Sessao = await AsyncStorage.getItem("sessao");
    if(Sessao){
      Sessao = JSON.parse(Sessao);
      this.setState({"sessao": Sessao});
      var body_ = new Body();
      body_._getMyLocation().then((position) => {
        position = position.coords;
        this.setState({
          latitude_usuario: position.latitude,
          longitude_usuario: position.longitude
        });
      });
      this._getPost();
      this.sockets();
    }
  }
  sockets = () => {
    var _this = this;
    this.props.io.socket.on('postlike_like', function (broadcastedData){
      if(broadcastedData.post_id && _this.state.posts.length){
         post_index = _this.state.posts.findIndex(function(obj) { return obj.id == broadcastedData.post_id });
         if(post_index != "-1"){
            // add o contador do like
            _this.state.posts[post_index].total_likes++;
            // add os likers
            if(_this.state.posts[post_index].ultimos_likes){
              if(_this.state.posts[post_index].ultimos_likes.length >= 3){
                _this.state.posts[post_index].ultimos_likes.splice(2,1);
                _this.state.posts[post_index].ultimos_likes.unshift(broadcastedData);
              }else{
                _this.state.posts[post_index].ultimos_likes.unshift(broadcastedData);
              }
            }else{
              _this.state.posts[post_index].ultimos_likes = [];
              _this.state.posts[post_index].ultimos_likes.unshift(broadcastedData);
            }
            _this.setState({posts:_this.state.posts});
         }
       }
    });
    this.props.io.socket.on('post_criar', function (broadcastedData){
      if(broadcastedData.id){
        _this.props.post_getpost({
          post_id:broadcastedData.id,
          token_firebase:_this.state.token_firebase,
          latitude_usuario:_this.state.latitude_usuario,
          longitude_usuario:_this.state.longitude_usuario,
        }).then(function (response) {
            if(!response.error){
              if(response.payload.status == "sucesso"){
                if(response.payload.data){
                  if(
                    _this.state.sessao.usuario_perfil_id.id == response.payload.data.perfil_id || 
                    response.payload.data.amigo
                  ){
                    _this.state.novidade.push(response.payload.data);
                    _this.setState({novidade:_this.state.novidade});
                  }
                  
                }
              }
            }
        });
      }

    });
    this.props.io.socket.on('postcomentario_comentar', function (broadcastedData){
      if(broadcastedData.post_id && _this.state.posts.length){
        post_index = _this.state.posts.findIndex(function(obj) { return obj.id == broadcastedData.post_id });
        if(post_index != "-1"){
            // add o contador do like
            _this.state.posts[post_index].total_comentarios++;
            // add os comentarios
            if(_this.state.posts[post_index].ultimos_comentarios){
              if(_this.state.posts[post_index].ultimos_comentarios.length >= 3){
                _this.state.posts[post_index].ultimos_comentarios.splice(2,1);
                _this.state.posts[post_index].ultimos_comentarios.unshift(broadcastedData);
              }else{
                _this.state.posts[post_index].ultimos_comentarios.unshift(broadcastedData);
              }
            }else{
              _this.state.posts[post_index].ultimos_comentarios = [];
              _this.state.posts[post_index].ultimos_comentarios.push(broadcastedData);
            }
            _this.setState({posts:_this.state.posts});
        }
      }

    });

    this.props.io.socket.on('post_excluir', function (broadcastedData){
      if(broadcastedData.id && _this.state.posts.length){
        post_index = _this.state.posts.findIndex(function(obj) { return obj.id == broadcastedData.id });
        if(post_index != "-1"){
            // remove o post
            _this.state.menu[post_index] = false;
            _this.setState({menu:_this.state.menu});
            _this.state.posts.splice(post_index,1);
            _this.setState({posts:_this.state.posts});
        }
      }

    });

    this.props.io.socket.on('postcomentario_excluir', function (broadcastedData){
      if(broadcastedData.post_id){
        post_index = _this.state.posts.findIndex(function(obj) { return obj.id == broadcastedData.post_id });
        if(post_index != "-1"){
          _this.props.postcomentario_get_ultimos_comentarios({
            post_id:broadcastedData.post_id,
            token_firebase:_this.state.token_firebase,
            latitude_usuario:_this.state.latitude_usuario,
            longitude_usuario:_this.state.longitude_usuario,
          }).then(function (response) {
              if(!response.error){
                if(response.payload.status == "sucesso"){
                  if(response.payload.data){
                    _this.state.posts[post_index].ultimos_comentarios = response.payload.data;
                    _this.setState({posts:_this.state.posts});
                    
                  }
                }
              }
          });
        }
      }

    });
  }
  _getPost = () => {
    var _this = this;
    _this.props.post_timeline({
      sort:"createdAt ASC",
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    }).then(function (response) {
      _this.setState({body_loading:false});
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg, pagina:1});
        }else if(response.payload.status == "sucesso"){
          if(response.payload.data.length){
            _this.setState({posts:response.payload.data,body_loading:false,novidade:[], pagina:1});
          }else{
            _this.setState({body_loading:false,novidade:[], pagina:1});
          }
        }
      }else{
        _this.setState({
          body_erro:response.payload.msg, 
          pagina:1, 
          body_loading:false,
          novidade:[]
        });
      }

    });
  }

  _onRefresh = () => {
    var _this = this;
    _this.setState({refreshing:true});
    _this.props.post_timeline({
      sort:"createdAt ASC",
      pagina:1,
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    }).then(function (response) {
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg, refreshing:false, pagina:1});
        }else if(response.payload.status == "sucesso"){
          if(response.payload.data.length){
            _this.setState({
              posts:response.payload.data,
              body_loading:false, 
              refreshing:false, 
              pagina:1, 
              novidade:[]
            });
          }else{
            _this.setState({
              body_loading:false, 
              refreshing:false, 
              pagina:1, 
              novidade:[]
            });
          }
        }else{
          _this.setState({
            body_loading:false, 
            refreshing:false, 
            pagina:1, 
            novidade:[],
            body_erro:response.payload.msg
          });
        }
      }else{
        _this.setState({
          body_loading:false, 
          refreshing:false, 
          pagina:1, 
          novidade:[],
          body_erro:"Ops, algo de errado aconteceu"
        });
      }

    });
  }

  _addMais = (info: {distanceFromEnd: 40}) => {
    _this = this;
    if(!_this.state.footer_loading_break){
      _this.setState({footer_loading_break:true, footer_loading:true});
      _this.props.post_timeline({
        pagina:_this.state.pagina+1,
        sort:"createdAt ASC",
        token_firebase:_this.state.token_firebase,
        latitude_usuario:_this.state.latitude_usuario,
        longitude_usuario:_this.state.longitude_usuario,
      }).then(function (response) {

        if(!response.error){
          if(response.payload.status == "erro"){
            _this.setState({footer_loading:false,footer_loading_break:false});
          }else if(response.payload.status == "sucesso"){
            if(response.payload.data.length){
              _this.setState({
                posts: [..._this.state.posts, ...response.payload.data], 
                pagina: _this.state.pagina+1, 
                footer_loading:response.payload.data.length ? true : false,
                footer_loading_break:false 
              });
            }else{
              _this.setState({
                footer_loading:false,
                footer_loading_break:false 
              });
            }
          }else{
             _this.setState({
              footer_loading:false,
              footer_loading_break:false 
            });
          }
        }else{
          _this.setState({
            body_erro:response.error,
            footer_loading_break:false,
            footer_loading:false,
          });
        }
      });
    }
  }

  clickLike = (post_id) => {
    var _this = this;
    _this.props.postlike_like({
      post_id:post_id,
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    }).then(function (response) {
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg});
        }
      }else{
        _this.setState({
          body_erro:response.payload.msg
        });
      }

    });

  }
  menuLeft = () => {
    /*var _this = this;
    Actions.camera({
      camera:true,
      video:false,
      segundos:10,
      get_video:function(dados){
        _this.setState({form_data_post_video:dados});
      },
      onBack:function(){
        Actions.pop();
        setTimeout(function(){
          if(_this.state.form_data_post_image || _this.state.form_data_post_video){
            Actions.postar({
              form_data_post_image:_this.state.form_data_post_image,
              form_data_post_video:_this.state.form_data_post_video
            });
          }
        },100);
      },
      get_imagem:function(dados){
        _this.setState({form_data_post_image:dados});
      }
    });*/
    Actions.refresh({key: 'drawer', open: true, side:"left"});
  }
  menuRight = () => {
    Actions.refresh({key: 'drawer', open: true, side:"right"});
  }
  modalComentaios = (post_id) => {
    var _this = this;
    Actions.comentar({
      id:post_id
    });
  }
  clickOpcao = (index,item) => {
    var _this = this;
    if(index==1){
      var url = "";
      if(item.imagem){
        url = item.imagem.imagemUrl
      }else if(item.video){
        url = item.video.videoUrl
      }
      _this.setState({body_loading:true});
      RNFetchBlob.fetch('GET', _this.state.url+'postimagem/imagem/'+item.imagem.id+'/800/800')
      // when response status code is 200
      .then((res) => {
        var mimetype = mime.lookup(item.imagem.name);
        let base64Str = res.base64();
        _this.setState({body_loading:false});
        Share.shareSingle(Object.assign({
          title: item.usuario_perfil_id.nome+" comparilhou um post dos Radicais Livre App",
          url:"data:"+mimetype+";base64,"+base64Str,
          message: item.texto,
        }, {
          "social": "facebook"
        }));

      });
      
    }else if(index==2){
      _this.setState({post_denuncia_id:item.id, denuncia:true});
    // remove o post
    }else if(index==3){
      $objeto = [
        {text: 'Sim', onPress: () => {
          _this.props.post_excluir({
            post_id:item.id,
            token_firebase:_this.state.token_firebase,
            latitude_usuario:_this.state.latitude_usuario,
            longitude_usuario:_this.state.longitude_usuario,
          }).then(function (response) {
            if(!response.error){
              if(response.payload.status == "erro"){
                _this.setState({body_erro:response.payload.msg});
              }
            }else{
              _this.setState({body_erro:response.payload.msg});
            }

          });
        }},
        {text: 'Não', onPress: () => {}, style: 'cancel'},
        {text: 'Cancelar', onPress: () => {}, style: 'cancel'},
      ];
      if(Platform.OS === 'android'){
        Alert.alert(
          'Deseja realmente excluir?',
          'Este post sera excluído',
          $objeto
        );
      }else{
        AlertIOS.alert(
          'Deseja realmente excluir?',
          'Este post sera excluído',
          $objeto
        );
      }
      
    }
  }
  
  clickExcluirComentatio = (comentario_id,post_id) => {
    var _this = this;
    $objeto = [
      {text: 'Sim', onPress: () => {
        _this.props.postcomentario_excluir({
          comentario_id:comentario_id,
          post_id:post_id,
          token_firebase:_this.state.token_firebase,
          latitude_usuario:_this.state.latitude_usuario,
          longitude_usuario:_this.state.longitude_usuario,
        }).then(function (response) {
          if(!response.error){
            if(response.payload.status == "erro"){
              _this.setState({body_erro:response.payload.msg});
            }
          }else{
            _this.setState({body_erro:response.payload.msg});
          }

        });
      }},
      {text: 'Não', onPress: () => {}, style: 'cancel'},
      {text: 'Cancelar', onPress: () => {}, style: 'cancel'},
    ];
    if(Platform.OS === 'android'){
      Alert.alert(
        'Deseja realmente excluir?',
        'Este comentário sera excluído',
        $objeto
      );
    }else{
      AlertIOS.alert(
        'Deseja realmente excluir?',
        'Este comentário sera excluído',
        $objeto
      );
    }
    
  }
  openMenu = (index) => {
    var _this = this;
    if(!_this.state.menu[index]){
      _this.state.menu[index] = true;
      _this.setState({menu:_this.state.menu});
    }else{
      _this.state.menu[index] = false;
      _this.setState({menu:_this.state.menu});
    }
  }

  clickDenunciar = (texto) => {
    if(!texto){
      alert("Descreve a denúncia");
      return;
    }
    var _this = this;
    _this.props.postdenuncia_denunciar({
      post_id:_this.state.post_denuncia_id,
      texto:texto,
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    }).then(function (response) {
      _this.setState({post_denuncia_id:"", denuncia:false});
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg});
        }else if(response.payload.status == "sucesso"){
          _this.setState({
            body_erro:"Denunciado com sucesso, após 5 denúncias o post irá sair do ar."
          });
        }
      }else{
        _this.setState({
          body_erro:response.payload.msg
        });
      }

    });
  }
  renderFooter () {
      return this.state.footer_loading ? <View style={{width:"100%", height:40, justifyContent: "center", alignItems:"center"}}>
        <Text style={{fontFamily:"Montserrat-Bold", fontSize:12}}>Carregando...</Text>
      </View> : <View style={{width:"100%", height:40}}/>
  }
  clickPerfil = (perfil_id) => {
    if(perfil_id == this.state.sessao.usuario_perfil_id.id){
      Actions.perfil();
    }else{
      Actions.perfil_amigo({usuario_perfil_id_search:perfil_id});
    }
    
  }
  render(){
      var _this = this;
      return (
        <View style={{flex:1}}>
            <Body
              onToken={(token)=>{
                _this.setState({token_firebase:token});
              }}
              getUrl={(url)=>{
                _this.setState({url:url});
              }}
              onErro={(erro)=>{
                _this.setState({body_erro:erro});
              }}
              btnOk={
                <Btn estilo={1} style={{width:100, marginTop:10}} texto={"OK"} onPress={()=>{
                  _this.setState({body_erro:""});
                }}/>
              }
              carregando={this.state.body_loading}
              erro={this.state.body_erro}
              ignoreKeyboard
              conteudo={
                <View
                  style={[styles.container]}>
                    <View style={[styles.conteudo]}>
                          <View style={styles.meio}>
                            <View style={{flex:1,width:"100%", marginBottom:50}}>
                            <Header right={<View/>} onPressCamera={()=> _this.menuLeft()} onPressDirect={()=> _this.menuRight()}/>
                              <FlatList
                                data={_this.state.posts}
                                numColumns={1}
                                style={{flex:1}}
                                keyExtractor={_this._keyExtractor}
                                ListFooterComponent={this.renderFooter.bind(this)}
                                onRefresh={_this._onRefresh}
                                onEndReached={this._addMais}
                                refreshing={_this.state.refreshing}
                                renderItem={({item,index}) => {
                                  return(
                                  <View id={index} style={{marginBottom:10}}> 
                                    {_this.state.menu[index] && !item.publicidade ?
                                    <View style={{position:"absolute", right:40, top:10, width:250, backgroundColor:"#fff", zIndex:1000, padding:10, borderRadius:10}}>
                                      <TouchableOpacity onPress={()=>{_this.clickOpcao(1,item)}} style={{padding:5, borderColor:"#E8E8E8", borderWidth:1, marginBottom:2}}>
                                        <Text style={{color:Template.cor_1, fontFamily:'Montserrat-Regular'}}>Compartilhar no Facebook</Text>
                                      </TouchableOpacity>
                                      <TouchableOpacity onPress={()=>{_this.clickOpcao(2,item)}} style={{padding:5, borderColor:"#E8E8E8", borderWidth:1, marginBottom:2}}>
                                        <Text style={{color:Template.cor_1, fontFamily:'Montserrat-Regular'}}>Denunciar</Text>
                                      </TouchableOpacity>
                                      {_this.state.sessao.usuario_perfil_id.id == item.perfil_id ?
                                      <TouchableOpacity onPress={()=>{_this.clickOpcao(3,item)}} style={{padding:5, borderColor:"#E8E8E8", borderWidth:1, marginBottom:2}}>
                                        <Text style={{color:Template.cor_1, fontFamily:'Montserrat-Regular'}}>Excluir</Text>
                                      </TouchableOpacity>
                                      : null}
                                    </View>
                                    : <View/> }
                                    {!_this.state.menu[index] && !item.publicidade ?
                                    <View style={{position:"absolute", right:10, top:10, zIndex:10000000}}>
                                      <TouchableOpacity onPress={()=>{_this.openMenu(index)}}>
                                        <Iconz name="md-list-box" size={30} color={Template.cor_3} />
                                      </TouchableOpacity>
                                    </View>
                                    : <View/> }
                                    {_this.state.menu[index] && !item.publicidade ?
                                      <View style={{position:"absolute", right:10, top:10, zIndex:10000000}}>
                                        <TouchableOpacity onPress={()=>{_this.openMenu(index)}}>
                                          <Iconz name="md-close" size={30} color={Template.cor_1} />
                                        </TouchableOpacity>
                                      </View>
                                    : <View/>}
                                    <View style={{backgroundColor:"#fff", padding:5, flexDirection:"row", alignItems:"center", justifyContent:"space-between"}}>
                                      {item.usuario_perfil_id ?
                                      <TouchableOpacity activeOpacity={.9} onPress={()=>{this.clickPerfil(item.usuario_perfil_id.id)}} style={{flexDirection:"row", alignItems:"center"}}>
                                        {item.usuario_perfil_id.avatar_id.id ?
                                        <Image
                                          style={{width:40, height:40, borderRadius:20}}
                                          source={
                                          { uri: _this.state.url+'usuarioavatar/avatar/'+item.usuario_perfil_id.avatar_id.id+'/100/100'}
                                           }/>
                                        : null }
                                        <View style={{paddingLeft:10}}>
                                          <Text style={styles.name_pessoa}>{item.usuario_perfil_id.nome}</Text>
                                          {item.createdAt ?
                                          <Text style={{fontFamily:'Montserrat-Regular',fontSize:10, color:"#999"}}>
                                            {moment(item.createdAt).fromNow()}
                                          </Text>
                                          : null}
                                        </View>
                                      </TouchableOpacity>
                                      : null }
                                    </View>
                                    {item.texto ?
                                    <View style={{padding:10}}>
                                      <Text style={{fontFamily:'Montserrat-Regular',fontSize:12, color:Template.cor_4, backgroundColor:"transparent"}}>{item.texto}</Text>
                                    </View>
                                    : null }
                                    {item.imagem ?
                                    <TouchableOpacity activeOpacity={.9} onPress={()=>{_this.modalComentaios(item.id)}}>
                                        <Image
                                          style={{width:"100%", height:320}}
                                          source={{ uri: _this.state.url+'postimagem/imagem/'+item.imagem.id+'/800/800'}}
                                          resizeMode={Image.resizeMode.cover}
                                        />
                                    </TouchableOpacity>
                                    : null}
                                    {item.video ?
                                    <View>
                                        <VideoPlayer
                                          video={{ uri:this.state.url+"postvideo/video/"+item.id+"/"+item.video.name }}
                                          videoWidth={300}
                                          videoHeight={300}
                                          muted={false}
                                          defaultMuted={false}
                                          thumbnail={{uri:this.state.url+"postvideo/imagem/"+item.video.id}}
                                        />
                                    </View>
                                    : null }
                                    {item.publicidade ?
                                        <Image
                                          style={{width:"100%", height:320}}
                                          source={{ uri: _this.state.url+'postpublicidade/imagem/'+item.publicidade.id+'/800/800'}}
                                          resizeMode={Image.resizeMode.cover}
                                        />
                                    : null}
                                    {!item.publicidade ?
                                    <View style={{height:60, justifyContent:"space-between", flexDirection:"row", alignItems:"center", borderBottomWidth:1, borderColor:"#ccc", paddingLeft:10, paddingRight:10}}>
                                      <View style={{flex:1, flexDirection:"row", alignItems:"center", position:"relative"}}>
                                        <TouchableOpacity onPress={()=>{_this.clickLike(item.id)}} style={{position:"absolute", left:15, top:-5, width:14, height:14, backgroundColor:Template.cor_1, zIndex:10, borderRadius:7, justifyContent:"center", alignItems:"center"}}>
                                          <Text style={{backgroundColor:"transparent", fontSize:8, color:"#fff", fontFamily:'Montserrat-Regular'}}>{item.total_likes}</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={()=>{_this.clickLike(item.id)}}>
                                          <Iconz name="ios-hand-outline" size={30} color={Template.cor_2} />
                                        </TouchableOpacity>
                                        {item.ultimos_likes ?
                                        <View style={{position:"absolute", left:33, top:-5, width:220, height:45, justifyContent:"flex-start", alignItems:"center", flexDirection:"row"}}>
                                          {item.ultimos_likes.map((person, index) => (
                                            <View key={index}>
                                            {item.ultimos_likes[index].usuario_perfil_id ?
                                            <Image
                                              style={{width:20, height:20, borderRadius:10, marginLeft:index <=0 ? 0 : -10}}
                                              source={{uri:_this.state.url+'usuarioavatar/avatar/'+item.ultimos_likes[index].usuario_perfil_id.avatar_id+'/40/40'}}
                                              resizeMode={Image.resizeMode.cover}
                                            />
                                            : null}
                                            </View>
                                          ))}
                                        </View>
                                        : null}
                                      </View>
                                      <TouchableOpacity onPress={()=>{_this.modalComentaios(item.id)}} style={{flexDirection:"row", alignItems:"center"}}>
                                        <View style={{position:"absolute", right:0, top:0, width:14, height:14, backgroundColor:Template.cor_1, zIndex:10, borderRadius:7, justifyContent:"center", alignItems:"center"}}>
                                          <Text style={{backgroundColor:"transparent", fontSize:8, color:"#fff", fontFamily:'Montserrat-Regular'}}>{item.total_comentarios}</Text>
                                        </View>
                                        <Iconz name="ios-chatbubbles-outline" size={30} color={Template.cor_2} />
                                      </TouchableOpacity>
                                    </View>
                                    : null }
                                    {!item.publicidade ?
                                    <View style={{padding:10, flexDirection:"row", flexWrap:"wrap"}}>
                                      {item.ultimos_likes.length ?
                                      <Text style={{fontFamily:'Montserrat-Regular',fontSize:12, color:"#999", marginRight:5}}>
                                        Acharam Radical:
                                      </Text>
                                      : <Text/>}
                                      {item.ultimos_likes && item.ultimos_likes.map((person, index) => (
                                      <TouchableOpacity onPress={()=>{this.clickPerfil(item.ultimos_likes[index].usuario_perfil_id.id)}} key={index} style={{marginRight:5}}>
                                        <Text style={{fontFamily:'Montserrat-Bold',fontSize:12, color:Template.cor_2}}>{item.ultimos_likes[index].usuario_perfil_id.nome}
                                        </Text>
                                      </TouchableOpacity>
                                      ))}
                                      {item.ultimos_likes && item.total_likes > 3 ?
                                      <View style={{marginRight:5}}>
                                        <Text style={{fontFamily:'Montserrat-Bold',fontSize:12, color:Template.cor_2}}>E mais {item.total_likes - 3} pessoa(s)</Text>
                                      </View>
                                      : <View/>}
                                    </View>
                                    : null }
                                    {item.ultimos_comentarios.length && !item.publicidade ?
                                    <View style={{flexDirection:"column"}}>
                                      {item.ultimos_comentarios.map((person, index) => (
                                      <View key={index} style={{padding:10, flexDirection:"row", flexWrap:"wrap", paddingTop:0, alignItems:"center"}}>
                                        <TouchableOpacity onPress={()=>{this.clickPerfil(item.ultimos_comentarios[index].usuario_perfil_id.id)}}>
                                          <Text style={{fontFamily:'Montserrat-Bold',fontSize:12, color:Template.cor_2, marginRight:5}}>
                                            {item.ultimos_comentarios[index].usuario_perfil_id.nome}:
                                          </Text>
                                        </TouchableOpacity>
                                        {item.ultimos_comentarios[index].usuario_perfil_id.id != _this.state.sessao.usuario_perfil_id.id ?
                                        <Text style={{fontFamily:'Montserrat-Regular',fontSize:12, color:Template.cor_2}}>
                                          {item.ultimos_comentarios[index].texto}
                                        </Text>
                                        : 
                                         <TouchableOpacity onPress={()=>{_this.clickExcluirComentatio(item.ultimos_comentarios[index].id,item.id)}}>
                                          <Text style={{fontFamily:'Montserrat-Regular',fontSize:12, color:Template.cor_2}}>
                                            {item.ultimos_comentarios[index].texto}
                                          </Text>
                                         </TouchableOpacity>
                                        }
                                      </View>
                                      ))}
                                    </View>
                                    : <View/> }
                                  </View>);
                                }}
                              />
                            </View>
                          </View>
                    </View>
                    {_this.state.novidade.length ?
                    <TouchableOpacity onPress={()=>{_this._onRefresh()}} style={styles.alerta}>
                      <Text style={styles.alerta_texto}>Existe {_this.state.novidade.length} novidade abençoada.</Text>
                    </TouchableOpacity>
                    : null}
                </View>
              }
            />
            <Prompt
            title="Denunciar Post"
            placeholder="Escreve a denúncia"
            visible={ this.state.denuncia }
            onCancel={ () => this.setState({
              denuncia: false,
              post_denuncia_id:""
            }) }
            onSubmit={ (value) => {
              _this.clickDenunciar(value);
            }}/>
          </View>
      );
    }
}

const mapStateToPropos = (state) => {
  return { data: state.reducer.dados };
}

const mapDispatchToPropos = (dispatch) => {
  return bindActionCreators({
    post_timeline,
    postlike_like,
    post_getpost,
    post_excluir,
    postcomentario_excluir,
    postcomentario_get_ultimos_comentarios,
    postdenuncia_denunciar
  },dispatch);
}

export default connect(mapStateToPropos, mapDispatchToPropos)(Index);
