const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;
var {height: deviceHeight, width:deviceWidth} = Dimensions.get("window");
import Template from '../../../constants/cores';

export default {
  alerta: {
    backgroundColor:Template.cor_1,
    width:"100%", 
    height:40, 
    position:"absolute", 
    top:0, 
    left:0, 
    zIndex:20,
    justifyContent:"center",
    alignItems:"center"
  },
  alerta_texto: {
    color:"#fff",
    fontFamily:'Montserrat-Bold',
  },
  container: {
    backgroundColor:'transparent',
    justifyContent: 'flex-end',
    alignItems: 'center',
    flex:1,
    width:"100%",
    backgroundColor:"#fff"
  },
  conteudo: {
    justifyContent: 'center',
    alignItems: 'center',
    flex:1,
    width:"100%"
  },
  scroll_100:{
    flex:1,
    width:"100%"
  },
  altura100:{
    flexGrow: 1,
    width:"100%"
  },
  meio: {
    width:'100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flex:1,
  },
  menu:{
    position:"absolute", 
    left:5, 
    top:10,
    zIndex:2
  },
  blc_header:{
    height:50, 
    justifyContent:"center", 
    alignItems:"center", 
    backgroundColor:"#fff",
    width:"100%",
    borderColor:Template.cor_3,
    borderBottomWidth:1
  },
  icon_menu:{
    backgroundColor:"transparent"
  },
  title_h1:{
    color:"#fff",
    fontFamily:'Montserrat-Bold',
  },
  content: {
      padding: 16,
      paddingTop: 0,
      paddingBottom: 0
  },
  name_pessoa:{
    fontFamily:'Montserrat-Bold',
    fontSize:14,
    color:Template.cor_1
  },
  imageWrapper: {
    flex: 1,
    alignItems: 'stretch'
  },
  fitImage:{
    flex: 1, alignSelf: 'stretch'
  },
  KeyboardAwareScrollView:{
    width:"100%"
  },
  KeyboardAwareScrollViewContent:{
    width:"100%", 
    justifyContent:"center", 
    alignItems:"center",
    flexGrow: 1
  },
  KeyboardAwareScrollViewContentEnd:{
    width:"100%", 
    justifyContent:"flex-end", 
    alignItems:"center",
    flexGrow: 1
  },
  text_completar_dados: {
    fontFamily:"Montserrat-Regular", 
    fontSize:18, 
    textAlign:"center",
    color:Template.cor_2,
    marginBottom:10,
    padding:10
  },

};
