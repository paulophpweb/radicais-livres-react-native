import React, { Component } from 'react';
import { View, TouchableOpacity, Text, AsyncStorage, FlatList, ScrollView, Image } from 'react-native';
import { List, ListItem, SearchBar, CheckBox, Slider, ButtonGroup } from "react-native-elements";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { usuario_pesquisar, usuarioamigo_seguir  } from '../../actions';
import Icon from 'react-native-vector-icons/MaterialIcons';
var moment = require('moment');
require('moment/locale/pt-br');
import Header from '../../components/header';
import Uteis from '../../lib/Uteis';
import Btn from '../../components/btn';
import Body from '../../components/ajax_body';
import Input from '../../components/inputs';
import Template from '../../constants/cores';
import styles from './style';

class Postar extends Component {
  _keyExtractor = (item, index) => index;

  constructor(props) {
    super(props);
    this.state = {
      body_erro:"",
      body_loading:false,
      url:"",
      sessao:null,
      filtro:true,
      membro:false,
      lider:false,
      discipulador:false,
      pastor:false,
      naorelacionando:true,
      idade_minima:18,
      idade_maxima:60,
      sexo:null,
      token_firebase:"",
      estado_civil:[],
      usuarios:[],
      pesquisa:"",
      qrcode:"",
      refreshing:false,
      footer_loading:false,
      footer_loading_break:false,
      pagina:1,
      latitude_usuario:"",
      longitude_usuario:"",
      aguarde:false
    }

  }
  async componentDidMount(){
    var _this = this;
    var Sessao = await AsyncStorage.getItem("sessao");
    if(Sessao){
      Sessao = JSON.parse(Sessao);
      this.setState({"sessao": Sessao});
      var body_ = new Body();
      body_._getMyLocation().then((position) => {
        position = position.coords;
        _this.setState({
          latitude_usuario: position.latitude,
          longitude_usuario: position.longitude
        });
      });
    }

  }
  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  }
  renderHeader = () => {
    return <SearchBar 
      showLoadingIcon={true}
      placeholder="Pesquisar amizade..." 
      lightTheme 
      round />;
  }
  clickFiltro = () => {
    if(this.state.filtro){
      this.setState({filtro:false});
    }else{
      this.setState({filtro:true});
    }
  }
  clickCheckbox = (tipo) => {
    if(tipo == "membro"){
      if(this.state.membro){
        this.setState({membro:false});
      }else{
        this.setState({membro:true});
        this.setState({lider:false});
        this.setState({discipulador:false});
        this.setState({pastor:false});
      }
    }else if(tipo == "lider"){
      if(this.state.lider){
        this.setState({lider:false});
      }else{
        this.setState({lider:true});
        this.setState({membro:false});
        this.setState({discipulador:false});
        this.setState({pastor:false});
      }
    }else if(tipo == "discipulador"){
      if(this.state.discipulador){
        this.setState({discipulador:false});
      }else{
        this.setState({discipulador:true});
        this.setState({membro:false});
        this.setState({lider:false});
        this.setState({pastor:false});
      }
    }else if(tipo == "pastor"){
      if(this.state.pastor){
        this.setState({pastor:false});
      }else{
        this.setState({naorelacionando:false});
        this.setState({pastor:true});
        this.setState({membro:false});
        this.setState({lider:false});
        this.setState({discipulador:false});
      }
    }else if(tipo == "naorelacionando"){
      if(this.state.naorelacionando){
        this.setState({naorelacionando:false});
      }else{
        this.setState({pastor:false});
        this.setState({naorelacionando:true});
      }
    }else if(tipo == "seguidores"){
      Actions.search({seguidores:false, type: "refresh", seguindo:false})
    }else if(tipo == "seguindo"){
      Actions.search({seguidores:false, type: "refresh", seguindo:false})
    }
  }

  clickScanner = () => {
    var _this = this;
    var detectou = false;
    Actions.camera({
      camera:false,
      video:false,
      onBarCodeRead:function(retorno){
        if(retorno.data && !detectou){
          detectou = true;
          var id = retorno.data;
          if(id){
            _this.setState({qrcode:id});
            _this.clickPesquisar();
          }else{
            _this.setState({body_erro:"Não conseguimos detectar este código, tente um código válido."});
          }
          Actions.pop();
        }
      }
    });
  }
  clickPesquisar = () => {
     var sexo = "";
     if(this.state.sexo){
        if(this.state.sexo === 0){
          sexo = "male";
        }else{
          sexo = "female";
        }
     }
     var _this = this;
     if(_this.state.pastor && !_this.state.pesquisa){
       _this.setState({body_erro:"Para usar o filtro de pastor o campo Procurar Amizade deve ser preenchido."});
       return;
     }
     _this.setState({body_loading:true, filtro:false});
    _this.props.usuario_pesquisar({
      seguindo:_this.props.navigationState.seguindo,
      seguidores:_this.props.navigationState.seguidores,
      qrcode:_this.state.qrcode,
      pesquisa:_this.state.pesquisa,
      membro:_this.state.membro,
      lider:_this.state.lider,
      discipulador:_this.state.discipulador,
      pastor:_this.state.pastor,
      naorelacionando:_this.state.naorelacionando,
      genero:sexo,
      idade_minima:_this.state.idade_minima,
      idade_maxima:_this.state.idade_maxima,
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    }).then(function (response) {
      _this.setState({body_loading:false, qrcode:"", aguarde:false});
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg,usuarios:[]});
        }else if(response.payload.status == "sucesso"){
          if(response.payload.data.length){
            _this.setState({usuarios:response.payload.data});
          }else{
            _this.setState({usuarios:[],body_erro:response.payload.msg});
          }
        }
      }else{
        _this.setState({body_erro:"Ops, algo de errado aconteceu", pagina:1});
      }

    });
  }
  clickSeguir = (item,index) => {
    //usuarioamigo_seguir
    var _this = this;
    _this.setState({body_loading:true});
    _this.props.usuarioamigo_seguir({
      perfil_id_2:item.usuario_perfil_id.id,
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    }).then(function (response) {
      _this.setState({body_loading:false});
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg});
        }else if(response.payload.status == "sucesso"){
            _this.state.usuarios.splice(index,1);
            _this.setState({usuarios:_this.state.usuarios, body_erro:response.payload.msg});
        }
      }else{
        _this.setState({body_erro:"Ops, algo de errado aconteceu"});
      }

    });
  }
  _onRefresh = () => {
    var sexo = "";
    if(this.state.sexo){
      if(this.state.sexo === 0){
        sexo = "male";
      }else{
        sexo = "female";
      }
    }
    var _this = this;
    _this.setState({refreshing:true});
    _this.props.usuario_pesquisar({
      seguindo:_this.props.navigationState.seguindo,
      seguidores:_this.props.navigationState.seguidores,
      qrcode:_this.state.qrcode,
      pesquisa:_this.state.pesquisa,
      membro:_this.state.membro,
      lider:_this.state.lider,
      discipulador:_this.state.discipulador,
      pastor:_this.state.pastor,
      naorelacionando:_this.state.naorelacionando,
      genero:sexo,
      idade_minima:_this.state.idade_minima,
      idade_maxima:_this.state.idade_maxima,
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    }).then(function (response) {
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg, refreshing:false, pagina:1});
        }else if(response.payload.status == "sucesso"){
          if(response.payload.data.length){
            _this.setState({
              usuarios:response.payload.data,
              body_loading:false, 
              refreshing:false, 
              pagina:1
            });
          }else{
            _this.setState({
              body_loading:false, 
              refreshing:false, 
              pagina:1, 
              usuarios:[]
            });
          }
        }
      }else{
        _this.setState({
          body_loading:false, 
          refreshing:false, 
          pagina:1, 
          usuarios:[],
          body_erro:"Ops, algo de errado aconteceu"
        });
      }

    });
  }
  _addMais = (info: {distanceFromEnd: 10}) => {
    var sexo = "";
    if(this.state.sexo){
      if(this.state.sexo === 0){
        sexo = "male";
      }else{
        sexo = "female";
      }
    }
    _this = this;
    if(!_this.state.footer_loading_break){
      _this.setState({footer_loading_break:true, footer_loading:true});
      _this.props.usuario_pesquisar({
        seguindo:_this.props.navigationState.seguindo,
        seguidores:_this.props.navigationState.seguidores,
        pagina:_this.state.pagina+1,
        qrcode:_this.state.qrcode,
        pesquisa:_this.state.pesquisa,
        membro:_this.state.membro,
        lider:_this.state.lider,
        discipulador:_this.state.discipulador,
        pastor:_this.state.pastor,
        naorelacionando:_this.state.naorelacionando,
        genero:_this.state.sexo === 0 ? "male" : "female",
        idade_minima:_this.state.idade_minima,
        idade_maxima:_this.state.idade_maxima,
        token_firebase:_this.state.token_firebase,
        latitude_usuario:_this.state.latitude_usuario,
        longitude_usuario:_this.state.longitude_usuario,
      }).then(function (response) {
        if(!response.error){
          if(response.payload.status == "erro"){
            _this.setState({footer_loading:false,footer_loading_break:false});
          }else if(response.payload.status == "sucesso"){
            if(response.payload.data.length){
              _this.setState({
                usuarios: [..._this.state.usuarios, ...response.payload.data], 
                pagina: _this.state.pagina+1, 
                footer_loading:response.payload.data.length ? true : false,
                footer_loading_break:false 
              });
            }else{
              _this.setState({
                footer_loading:false,
                footer_loading_break:false 
              });
            }
          }
        }else{
          _this.setState({
            body_erro:response.error,
            footer_loading_break:false,
            footer_loading:false,
          });
        }
      });
    }
  }
  renderFooter () {
      return this.state.footer_loading ? <View style={{width:"100%", height:40, justifyContent: "center", alignItems:"center"}}>
        <Text style={{fontFamily:"Montserrat-Bold", fontSize:12}}>Carregando...</Text>
      </View> : <View style={{width:"100%", height:40}}/>
  }
  clickPerfil = (perfil_id) => {
    if(perfil_id == this.state.sessao.usuario_perfil_id.id){
      Actions.perfil();
    }else{
      Actions.perfil_amigo({usuario_perfil_id_search:perfil_id});
    }
    
  }

  render() {
       return (
          <Body
            getUrl={(url)=>{
              this.setState({url:url});
            }}
            onErro={(erro)=>{
              this.setState({body_erro:erro});
            }}
            btnOk={
              <Btn estilo={1} style={{width:100, marginTop:10}} texto={"OK"} onPress={()=>{
                this.setState({body_erro:""});
              }}/>
            }
            onToken={(token)=>{
                this.setState({token_firebase:token});
            }}
            carregando={this.state.body_loading}
            erro={this.state.body_erro}
            ignoreKeyboard
            conteudo={
              <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0, flex:1, marginTop:0 }}>
                <Input
                  estilo="search"
                  ref="0"
                  onChangeText={(value) => this.setState({pesquisa: value})}
                  texto={"Pesquisar Amizade"}
                  style={{marginLeft:2, marginRight:2, marginTop:2, borderColor:Template.cor_1}}
                  style_input={{borderWidth:0}}
                  clickScanner={this.clickScanner}
                  pesquisar={this.clickPesquisar}
                />
                {!this.state.filtro ?
                <View style={{justifyContent:"center", flexDirection:"row"}}>
                  <TouchableOpacity onPress={()=>{this.clickFiltro()}}>
                    <Image
                      source={require('../../images/drop_down.png')}
                      style={{height:10}}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                </View>
                : 
                <View style={{justifyContent:"center", flexDirection:"row", marginTop:10}}>
                  <TouchableOpacity onPress={()=>{this.clickFiltro()}}>
                    <Image
                      source={require('../../images/drop_down_up.png')}
                      style={{height:10}}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                </View>
                }
                {this.state.filtro ?
                <ScrollView style={{marginTop:73, width:"100%", backgroundColor:"#fff", zIndex:2, bottom:60}}>
                  {this.props.navigationState.seguidores ?
                  <CheckBox
                    title='Seguidores'
                    checked={this.props.navigationState.seguidores}
                    onPress={()=>{this.clickCheckbox("seguidores")}}
                  />
                  : null }
                  {this.props.navigationState.seguindo ?
                  <CheckBox
                    title='Seguindo'
                    checked={this.props.navigationState.seguindo}
                    onPress={()=>{this.clickCheckbox("seguindo")}}
                  />
                  : null }
                  <CheckBox
                    title='Membro'
                    checked={this.state.membro}
                    onPress={()=>{this.clickCheckbox("membro")}}
                  />
                  <CheckBox
                    title='Líder'
                    checked={this.state.lider}
                    onPress={()=>{this.clickCheckbox("lider")}}
                  />
                  <CheckBox
                    title='Discipulador'
                    checked={this.state.discipulador}
                    onPress={()=>{this.clickCheckbox("discipulador")}}
                  />
                  <CheckBox
                    title='Pastor(a)'
                    checked={this.state.pastor}
                    onPress={()=>{this.clickCheckbox("pastor")}}
                  />
                  <CheckBox
                    title='Solteiro (Não Relacionando)'
                    checked={this.state.naorelacionando}
                    onPress={()=>{this.clickCheckbox("naorelacionando")}}
                  />
                  <ButtonGroup
                  textStyle={{color:Template.cor_1, fontFamily:"Montserrat-Bold"}}
                  onPress={(selectedIndex)=>{this.setState({sexo:selectedIndex})}}
                  selectedIndex={this.state.sexo}
                  buttons={['Masculino', 'Feminino']}
                  selectedBackgroundColor={Template.cor_1}
                  selectedTextStyle={{color:"#fff"}}
                  containerStyle={{height: 40}} />
                  {/*<Input
                    dados={this.state.estado_civil}
                    estilo="select"
                    ref="1"
                    onSelect={(value) => this.setState({cidade: value})}
                    texto={"Selecione um estado civil"}
                    style={{marginLeft:10, marginRight:10, marginTop:10, marginBottom:10}}
                    style_input={{borderWidth:0}}
                  />*/}
                  <View style={{paddingLeft:10, paddingRight:10, flexDirection:"row"}}>
                    <View style={{flex:.5, marginRight:30}}>
                      <Text style={{color:Template.cor_1, fontFamily:'Montserrat-Bold'}}>De {this.state.idade_minima} anos</Text>
                      <Slider
                        step={2}
                        value={this.state.idade_minima}
                        maximumValue={99}
                        animateTransitions={true}
                        onValueChange={(value) => this.setState({idade_minima:parseInt(value)})} />
                    </View>
                    <View style={{flex:.5}}>
                      <Text style={{color:Template.cor_1, fontFamily:'Montserrat-Bold'}}>Até {this.state.idade_maxima} anos</Text>
                      <Slider
                        step={2}
                        value={this.state.idade_maxima}
                        maximumValue={99}
                        animateTransitions={true}
                        onValueChange={(value) => this.setState({idade_maxima:parseInt(value)})} />
                    </View>
                  </View>
                  <View style={{marginTop:10, width:"100%", alignItems:"center", marginBottom:20}}>
                    <Btn onPress={()=>{this.clickPesquisar();}} style={{width:220, marginTop:25}} estilo={1} texto={"PESQUISAR"}/>
                  </View>
                </ScrollView>
                : <View></View> }
                {!this.state.filtro && this.state.usuarios.length ?
                <FlatList
                  data={this.state.usuarios}
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                  onEndReached={this._addMais}
                  style={{flex:1, marginBottom:50, width:"100%"}}
                  ListFooterComponent={this.renderFooter.bind(this)}
                  keyExtractor={this._keyExtractor}
                  ItemSeparatorComponent={this.renderSeparator}
                  renderItem={({item,index}) => {
                    var detalhes = "";
                    if(item.data_nascimento)
                      detalhes += moment().diff(item.data_nascimento, 'years')+" anos, ";
                    if(
                      this.state.latitude_usuario && 
                      this.state.longitude_usuario &&
                      item.latitude &&
                      item.longitude
                    ){
                      $distancia = Uteis.calculate_distance(
                        this.state.latitude_usuario,
                        this.state.longitude_usuario,
                        item.latitude,
                        item.longitude
                      );
                      detalhes += ""+$distancia+" km";
                    }

                    return(
                      <ListItem
                        roundAvatar
                        onPress={()=>{this.clickPerfil(item.usuario_perfil_id.id)}}
                        title={item.usuario_perfil_id.nome}
                        subtitle={detalhes}
                        avatar={{ uri: this.state.url+'usuarioavatar/avatar/'+item.usuario_perfil_id.avatar_id.id+'/100/100' }}
                        containerStyle={{ borderBottomWidth: 0 }}
                        rightIcon={
                          <View>
                            {!item.amigo && !item.aguardando_amizade && item.id != this.state.sessao.id ?
                            <Btn onPress={()=>{this.clickSeguir(item,index)}} style={{padding:10}} estilo={1} texto={"Seguir"}/>
                            : null}
                            {!item.amigo && item.aguardando_amizade ?
                            <Btn style={{padding:10, backgroundColor:Template.cor_3}} estilo={1} texto={"Aguardando"}/>
                            : null}
                          </View>
                        }
                        
                      />
                    );
                  }}
                />
                : null }
                {!this.state.usuarios.length && !this.state.filtro ?
                <View style={{justifyContent:"center", alignItems:"center", flex:1}}>
                  <Text style={{textAlign:"center", fontFamily:'Montserrat-Regular',fontSize:16, color:"#999"}}>Procure um amigo, você pode usar a pesquisa por câmera ou filtrar a sua busca.</Text>
                </View>
                : null }
              </List>
            }/>
       );
   }
}

const mapStateToPropos = (state) => {
  return { data: state.reducer.dados };
}

const mapDispatchToPropos = (dispatch) => {
  return bindActionCreators({
    usuario_pesquisar,
    usuarioamigo_seguir
  },dispatch);
}

export default connect(mapStateToPropos, mapDispatchToPropos)(Postar);
