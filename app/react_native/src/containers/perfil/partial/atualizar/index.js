import React, { Component } from 'react'
import { View, Image, Text, AsyncStorage, TouchableOpacity } from 'react-native'
import { Actions } from 'react-native-router-flux';
var {FBLoginManager} = require('react-native-facebook-login');
import Modal from 'react-native-modal';
import { List, ListItem, SearchBar, CheckBox, Slider, ButtonGroup } from "react-native-elements";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
var moment = require('moment');
require('moment/locale/pt-br');
import { usuariorelacionamento_get_relacionamentos, usuario_atualizar, usuario_get } from '../../../../actions';
import Uteis from '../../../../lib/Uteis';
import Loading from '../../../../components/loading';
import Body from '../../../../components/ajax_body';
import Btn from '../../../../components/btn';
import Input from '../../../../components/inputs';
import Template from '../../../../constants/cores';


class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url:"",
      body_erro:"",
      carregando:false,
      latitude_usuario:"",
      longitude_usuario:"",
      relacionamentos:[],
      sessao:"",
      relacionamento_id:"",
      relacionando:false,
      membro:false,
      lider:false,
      discipulador:false,
      pastor:false,
      data_nascimento:"",
      token_firebase:"",
      btn_salvar:"SALVAR",
      carregougeral:false,
      usuario:"",
      email:""
    }

  }
  async componentDidMount(){
    var _this = this;
    var Sessao = await AsyncStorage.getItem("sessao");
    if(Sessao){
      Sessao = JSON.parse(Sessao);
      this.setState({"sessao": Sessao});
      var body_ = new Body();
      body_._getMyLocation().then((position) => {
        position = position.coords;
        _this.setState({
          latitude_usuario: position.latitude,
          longitude_usuario: position.longitude
        });
      });
      _this._getPerfil();
    }
  }

  clickSalvar = () => {
     var _this = this;
     var $erro = '';
     if(!_this.state.relacionamento_id){
      $erro += 'Por favor, selecione o seu estado civil';
     }else if(!_this.state.membro && !_this.state.lider && !_this.state.discipulador && !_this.state.pastor){
      $erro += 'Por favor, marque uma das funções abaixo para as pessoas te acharem melhor.';
     }else if(!_this.state.email){
      $erro += 'Por favor, preencha o campo email';
     }else if(!Uteis.validaEmail(this.state.email)){
      $erro += 'Email inválido';
     }else if(!_this.state.data_nascimento){
      $erro += 'Preencha o campo data de nascimento';
     }else if(!moment(_this.state.data_nascimento, 'DD/MM/YYYY', true).isValid()){
      $erro += 'Data de nascimento inválida';
     }
     if($erro){
      Uteis.alerta($erro,"error");
     }else{
      if(this.state.btn_salvar != "Salvando..."){
         _this.setState({body_loading:true,btn_salvar:"Salvando..."});
        _this.props.usuario_atualizar({
          token_firebase:_this.state.token_firebase,
          latitude_usuario:_this.state.latitude_usuario,
          longitude_usuario:_this.state.longitude_usuario,
          relacionamento_id:_this.state.relacionamento_id,
          membro:_this.state.membro,
          lider:_this.state.lider,
          discipulador:_this.state.discipulador,
          pastor:_this.state.pastor,
          email:_this.state.email,
          data_nascimento:_this.state.data_nascimento,
          relacionando:_this.state.relacionando
        }).then(function (response) {
          _this.setState({body_loading:false});
          if(!response.error){
            if(response.payload.status == "erro"){
              _this.setState({body_erro:response.payload.msg,btn_salvar:"SALVAR",body_loading:false});
            }else if(response.payload.status == "sucesso"){
              if(response.payload.data){
                Uteis.alerta("Dados atualizado com sucesso!","success",4000,function(){
                    Actions.pop();
                    if(_this.props.onBack)
                      _this.props.onBack();
                });
              }else{
                _this.setState({body_erro:response.payload.msg,btn_salvar:"SALVAR",body_loading:false});
              }
            }else{
              _this.setState({body_erro:"Algo de errado aconteceu, tente novamente.",btn_salvar:"SALVAR",body_loading:false});
            }
          }else{
            _this.setState({body_erro:"Ops, algo de errado aconteceu",btn_salvar:"SALVAR",body_loading:false});
          }

        });
      }
    }
  }

  _getPerfil = () => {
    var _this = this;
    $objeto = {
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    }
    _this.setState({carregougeral:false});
    _this.props.usuario_get($objeto).then(function (response) {
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg, pagina:1});
        }else if(response.payload.status == "sucesso"){
          if(response.payload.data){
            _this.setState({usuario:response.payload.data});
            _this.setState({
              relacionando:_this.state.usuario.usuario_perfil_id.relacionando ? true : false,
              membro:_this.state.usuario.usuario_perfil_id.membro ? true : false,
              lider: _this.state.usuario.usuario_perfil_id.lider ? true : false,
              discipulador: _this.state.usuario.usuario_perfil_id.discipulador ? true : false,
              pastor: _this.state.usuario.usuario_perfil_id.ministro ? true : false,
              relacionamento_id:_this.state.usuario.usuario_perfil_id.relacionamento_id,
              data_nascimento:moment(_this.state.usuario.usuario_perfil_id.data_nascimento,'YYYY-MM-DD').format('DD/MM/YYYY'),
              email:_this.state.usuario.email
            });
            _this.getRelacionamentos();
          }else{
            _this.setState({body_loading:false});
          }
        }
      }else{
        _this.setState({
          body_erro:"Ops, algo de errado aconteceu",  
          body_loading:false
        });
      }

    });
  }

  getRelacionamentos = () => {
     var _this = this;
     _this.setState({body_loading:true});
    _this.props.usuariorelacionamento_get_relacionamentos({
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario
    }).then(function (response) {
      _this.setState({body_loading:false});
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg,usuarios:[]});
        }else if(response.payload.status == "sucesso"){
          if(response.payload.data.length){
            var newArray = [];
            for(var i in response.payload.data){
              newArray.push({
                key:response.payload.data[i].id,
                chave:response.payload.data[i].id,
                label:response.payload.data[i].nome,
                long_name:response.payload.data[i].nome
              });
            }
            _this.setState({relacionamentos:newArray, carregougeral:true});
          }else{
            _this.setState({relacionamentos:[],body_erro:response.payload.msg});
          }
        }else{
          _this.setState({body_erro:"Algo de errado aconteceu, tente novamente."});
        }
      }else{
        _this.setState({body_erro:"Ops, algo de errado aconteceu"});
      }

    });
  }
  clickCheckbox = (tipo) => {
    if(tipo == "membro"){
      if(this.state.membro){
        this.setState({membro:false});
      }else{
        this.setState({membro:true});
      }
    }else if(tipo == "lider"){
      if(this.state.lider){
        this.setState({lider:false});
      }else{
        this.setState({lider:true});
      }
    }else if(tipo == "discipulador"){
      if(this.state.discipulador){
        this.setState({discipulador:false});
      }else{
        this.setState({discipulador:true});
      }
    }else if(tipo == "pastor"){
      if(this.state.pastor){
        this.setState({pastor:false});
      }else{
        this.setState({pastor:true});
      }
    }else if(tipo == "relacionando"){
      if(this.state.relacionando){
        this.setState({relacionando:false});
      }else{
        this.setState({relacionando:true});
      }
    }
  }
  render() {
    var _this = this;
    if(_this.state.carregougeral){
      return(
        <Modal
          isVisible={true}
          backdropColor={"#000"}
          backdropOpacity={1}
          animationIn={'zoomInDown'}
          animationOut={'zoomOutUp'}
          animationInTiming={1000}
          animationOutTiming={1000}
          backdropTransitionInTiming={1000}
          backdropTransitionOutTiming={1000}
        >
        <Body
          getUrl={(url)=>{
            _this.setState({url:url});
          }}
          onErro={(erro)=>{
            _this.setState({body_erro:erro});
          }}
          btnOk={
            <Btn estilo={1} style={{width:100, marginTop:10}} texto={"OK"} onPress={()=>{
              _this.setState({body_erro:""});
            }}/>
          }
          onToken={(token)=>{
            _this.setState({token_firebase:token});
          }}
          carregando={this.state.carregando}
          erro={this.state.body_erro}
          conteudo={
                <View style={{ flex: 1, backgroundColor:"#fff", justifyContent:"center", alignItems:"center" }}>
                    <View style={{flexDirection:"row", justifyContent:"center", alignItems:"center", flex:1}}>
                      <TouchableOpacity
                          onPress={()=>{Actions.pop()}}
                          style={{flex:.2, justifyContent: "center", alignItems:"center"}}
                      >
                        <Icon name="arrow-back" size={30} color={"#000"} />
                      </TouchableOpacity>
                      <Text style={{fontFamily:"Montserrat-Regular", fontSize:14, textAlign:"center",color:Template.cor_2,marginBottom:10,padding:10, flex:.8}}>Para as pessoas te acharem mais fácil preencha os dados abaixo:</Text>
                    </View>
                      <View style={{marginTop:10, width:280}}>
                        <CheckBox
                          title='Relacionando'
                          checked={_this.state.relacionando}
                          onPress={()=>{this.clickCheckbox("relacionando")}}
                        />
                        <CheckBox
                          title='Membro'
                          checked={_this.state.membro}
                          onPress={()=>{this.clickCheckbox("membro")}}
                        />
                        <CheckBox
                          title='Líder'
                          checked={_this.state.lider}
                          onPress={()=>{this.clickCheckbox("lider")}}
                        />
                        <CheckBox
                          title='Discipulador'
                          checked={this.state.discipulador}
                          onPress={()=>{this.clickCheckbox("discipulador")}}
                        />
                        <CheckBox
                          title='Pastor(a)'
                          checked={this.state.pastor}
                          onPress={()=>{this.clickCheckbox("pastor")}}
                        />
                        {_this.state.sessao ?
                          <View>
                              <Input
                                dados={this.state.relacionamentos}
                                estilo="select"
                                ref="1"
                                onSelect={(value) => this.setState({relacionamento_id: value.key})}
                                texto={this.state.usuario.relacionamento}
                                style={{marginLeft:10, marginRight:10, marginTop:10, marginBottom:10}}
                                style_input={{borderWidth:0}}
                              />
                              <Input
                                ref="2"
                                refInput="2"
                                onChangeText={value => this.setState({email: value})}
                                texto={"Email"}
                                value={this.state.email}
                                returnKeyType={'next'}
                                keyboardType={"email-address"}
                                blurOnSubmit={false}
                                style={{marginLeft:10, marginRight:10, marginTop:10, marginBottom:10}}
                              />
                              <Input
                                ref="3"
                                refInput="3"
                                onChangeText={value => this.setState({data_nascimento: Uteis.datamask(value)})}
                                texto={"Data de Nascimento"}
                                value={this.state.data_nascimento}
                                returnKeyType={'done'}
                                keyboardType={"phone-pad"}
                                blurOnSubmit={false}
                                style={{marginLeft:10, marginRight:10, marginTop:10, marginBottom:10}}
                                onSubmitEditing={() => this.clickAtualizar()}
                              />
                          </View>
                        : null}
                      </View>
                      <View style={{marginTop:10, width:"100%", alignItems:"center", marginBottom:20}}>
                        <Btn onPress={()=>{_this.clickSalvar();}} style={{width:220, marginTop:25}} estilo={1} texto={_this.state.btn_salvar}/>
                      </View>
                </View>
          }/>
          </Modal>
      );
    }else{
      return(
        <Modal
          isVisible={true}
          backdropColor={"#000"}
          backdropOpacity={1}
          animationIn={'zoomInDown'}
          animationOut={'zoomOutUp'}
          animationInTiming={1000}
          animationOutTiming={1000}
          backdropTransitionInTiming={1000}
          backdropTransitionOutTiming={1000}
        >
        <Body
          carregando={this.state.carregando}
          erro={this.state.body_erro}
          conteudo={
            <View/>
          }/>

        </Modal>

      );
    }
  }
}

const mapStateToPropos = (state) => {
  return { data: state.reducer.dados };
}

const mapDispatchToPropos = (dispatch) => {
  return bindActionCreators({
    usuariorelacionamento_get_relacionamentos,
    usuario_atualizar,
    usuario_get
  },dispatch);
}

export default connect(mapStateToPropos, mapDispatchToPropos)(Index);