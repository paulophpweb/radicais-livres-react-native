import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, AsyncStorage, FlatList, Dimensions, Keyboard, WebView, Alert, AlertIOS, Platform } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Animatable from 'react-native-animatable';
var {FBLoginManager} = require('react-native-facebook-login');
var moment = require('moment');
require('moment/locale/pt-br');
import Icon from 'react-native-vector-icons/MaterialIcons';
import Iconz from 'react-native-vector-icons/Ionicons';
import VideoPlayer from 'react-native-video-player';
import Modal from 'react-native-modal';
import { Actions } from 'react-native-router-flux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RNFetchBlob from 'react-native-fetch-blob';
import * as mime from 'react-native-mime-types';
import Share, {ShareSheet, Button} from 'react-native-share';
import Prompt from 'react-native-prompt';
import { SideMenu, List, ListItem } from 'react-native-elements';
import Image from '../../components/fast_image/FastImage';
import QRCode from 'react-native-qrcode-svg';
import { 
  post_timeline, 
  postlike_like, 
  post_getpost, 
  post_excluir, 
  postcomentario_excluir, 
  postcomentario_get_ultimos_comentarios,
  postdenuncia_denunciar,
  usuario_get,
  usuario_atualizar,
  usuarioamigo_seguir,
} from '../../actions';
import Uteis from '../../lib/Uteis';
import Body from '../../components/ajax_body';
import Header from '../../components/header';
import Btn from '../../components/btn';
import Input from '../../components/inputs';
import Config from '../../constants/config';
import Template from '../../constants/cores';
import styles from './style';
var {height: deviceHeight, width:deviceWidth} = Dimensions.get("window");

class Index extends Component {
  _keyExtractor = (item, index) => index;

  constructor(props) {
    super(props);
    this.state = {
      token: "",
      posts: [],
      novidade: "",
      sessao:"",
      body_loading:true,
      refreshing:false,
      footer_loading:false,
      footer_loading_break:false,
      body_erro:"",
      url : "",
      camera:false,
      form_post:false,
      form_data_post_image:null,
      form_data_post_video:null,
      form_data_post_texto:null,
      post_like:[],
      pagina:1,
      menu: [],
      token_firebase:"",
      denuncia:false,
      post_denuncia_id:"",
      latitude_usuario:"",
      longitude_usuario:"",
      usuario:"",
      qrcode:false,
      menutipo:1,
      carregougeral:false
    }

  }

  async componentDidMount(){
    var Sessao = await AsyncStorage.getItem("sessao");
    if(Sessao){
      Sessao = JSON.parse(Sessao);
      this.setState({"sessao": Sessao});
      var body_ = new Body();
      body_._getMyLocation().then((position) => {
        position = position.coords;
        this.setState({
          latitude_usuario: position.latitude,
          longitude_usuario: position.longitude
        });
      });
      this._getPerfil();
      this.sockets();
    }
  }
  sockets = () => {
    var _this = this;
    this.props.io.socket.on('postlike_like', function (broadcastedData){
      if(broadcastedData.post_id && _this.state.posts.length){
         post_index = _this.state.posts.findIndex(function(obj) { return obj.id == broadcastedData.post_id });
         if(post_index != "-1"){
            // add o contador do like
            _this.state.posts[post_index].total_likes++;
            // add os likers
            if(_this.state.posts[post_index].ultimos_likes){
              if(_this.state.posts[post_index].ultimos_likes.length >= 3){
                _this.state.posts[post_index].ultimos_likes.splice(2,1);
                _this.state.posts[post_index].ultimos_likes.unshift(broadcastedData);
              }else{
                _this.state.posts[post_index].ultimos_likes.unshift(broadcastedData);
              }
            }else{
              _this.state.posts[post_index].ultimos_likes = [];
              _this.state.posts[post_index].ultimos_likes.unshift(broadcastedData);
            }
            _this.setState({posts:_this.state.posts});
         }
       }
    });
    this.props.io.socket.on('post_criar', function (broadcastedData){
      if(broadcastedData.id){
        _this.props.post_getpost({
          post_id:broadcastedData.id,
          token_firebase:_this.state.token_firebase,
          latitude_usuario:_this.state.latitude_usuario,
          longitude_usuario:_this.state.longitude_usuario,
        }).then(function (response) {
            if(!response.error){
              if(response.payload.status == "sucesso"){
                if(response.payload.data){
                  if(
                    _this.state.sessao.usuario_perfil_id.id == response.payload.data.perfil_id || 
                    response.payload.data.amigo
                  ){
                    _this.state.novidade = [];
                    _this.state.novidade.push(response.payload.data);
                    _this.setState({novidade:_this.state.novidade});
                  }
                  
                }
              }
            }
        });
      }

    });
    this.props.io.socket.on('postcomentario_comentar', function (broadcastedData){
      if(broadcastedData.post_id && _this.state.posts.length){
        post_index = _this.state.posts.findIndex(function(obj) { return obj.id == broadcastedData.post_id });
        if(post_index != "-1"){
            // add o contador do like
            _this.state.posts[post_index].total_comentarios++;
            // add os comentarios
            if(_this.state.posts[post_index].ultimos_comentarios){
              if(_this.state.posts[post_index].ultimos_comentarios.length >= 3){
                _this.state.posts[post_index].ultimos_comentarios.splice(2,1);
                _this.state.posts[post_index].ultimos_comentarios.unshift(broadcastedData);
              }else{
                _this.state.posts[post_index].ultimos_comentarios.unshift(broadcastedData);
              }
            }else{
              _this.state.posts[post_index].ultimos_comentarios = [];
              _this.state.posts[post_index].ultimos_comentarios.push(broadcastedData);
            }
            _this.setState({posts:_this.state.posts});
        }
      }

    });

    this.props.io.socket.on('post_excluir', function (broadcastedData){
      if(broadcastedData.id && _this.state.posts.length){
        post_index = _this.state.posts.findIndex(function(obj) { return obj.id == broadcastedData.id });
        if(post_index != "-1"){
            // remove o post
            _this.state.menu[post_index] = false;
            _this.setState({menu:_this.state.menu});
            _this.state.posts.splice(post_index,1);
            _this.setState({posts:_this.state.posts});
        }
      }

    });

    this.props.io.socket.on('postcomentario_excluir', function (broadcastedData){
      if(broadcastedData.post_id){
        post_index = _this.state.posts.findIndex(function(obj) { return obj.id == broadcastedData.post_id });
        if(post_index != "-1"){
          _this.props.postcomentario_get_ultimos_comentarios({
            post_id:broadcastedData.post_id,
            token_firebase:_this.state.token_firebase,
            latitude_usuario:_this.state.latitude_usuario,
            longitude_usuario:_this.state.longitude_usuario,
          }).then(function (response) {
              if(!response.error){
                if(response.payload.status == "sucesso"){
                  if(response.payload.data){
                    _this.state.posts[post_index].ultimos_comentarios = response.payload.data;
                    _this.setState({posts:_this.state.posts});
                    
                  }
                }
              }
          });
        }
      }

    });
  }
  _getPerfil = () => {
    var _this = this;
    $objeto = {
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    }
    if(this.props.usuario_perfil_id_search)
      $objeto.usuario_perfil_id_search = this.props.usuario_perfil_id_search;
    _this.setState({carregougeral:false, posts:[]});
    _this.props.usuario_get($objeto).then(function (response) {
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg, pagina:1});
        }else if(response.payload.status == "sucesso"){
          if(response.payload.data){
            _this.setState({usuario:response.payload.data});
            _this._getPost();
          }else{
            _this.setState({body_loading:false});
          }
        }
      }else{
        _this.setState({
          body_erro:"Ops, algo de errado aconteceu",  
          body_loading:false
        });
      }

    });
  }
  _getPost = () => {
    var _this = this;
    var $objeto = {
      sort:"createdAt ASC",
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    };
    if(_this.props.usuario_perfil_id_search){
      $objeto.usuario_perfil_id_search = _this.props.usuario_perfil_id_search;
    }else{
      $objeto.usuario_perfil_id_search = _this.state.sessao.usuario_perfil_id.id;
    }

    _this.props.post_timeline($objeto).then(function (response) {
      _this.setState({body_loading:false});
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({pagina:1, carregougeral:true});
        }else if(response.payload.status == "sucesso"){
          if(response.payload.data.length){
            _this.setState({posts:response.payload.data,body_loading:false,novidade:"", pagina:1, carregougeral:true});
          }else{
            _this.setState({body_loading:false,novidade:"", pagina:1, carregougeral:true});
          }
        }
      }else{
        _this.setState({
          body_erro:"Ops, algo de errado aconteceu", 
          pagina:1, 
          body_loading:false,
          novidade:"",
          carregougeral:true
        });
      }

    });
  }

  _onRefresh = () => {
    var _this = this;
    _this.setState({refreshing:true});
    var $objeto = {
      sort:"createdAt ASC",
      pagina:1,
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    };
    if(_this.props.usuario_perfil_id_search){
      $objeto.usuario_perfil_id_search = _this.props.usuario_perfil_id_search;
    }else{
      $objeto.usuario_perfil_id_search = _this.state.sessao.usuario_perfil_id.id;
    }

    _this.props.post_timeline($objeto).then(function (response) {
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg, refreshing:false, pagina:1});
        }else if(response.payload.status == "sucesso"){
          if(response.payload.data.length){
            _this.setState({
              posts:response.payload.data,
              body_loading:false, 
              refreshing:false, 
              pagina:1, 
              novidade:""
            });
          }else{
            _this.setState({
              body_loading:false, 
              refreshing:false, 
              pagina:1, 
              novidade:""
            });
          }
        }
      }else{
        _this.setState({
          body_loading:false, 
          refreshing:false, 
          pagina:1, 
          novidade:"",
          body_erro:"Ops, algo de errado aconteceu"
        });
      }

    });
  }

  _addMais = (info: {distanceFromEnd: 40}) => {
    _this = this;
    if(!_this.state.footer_loading_break){
      _this.setState({footer_loading_break:true, footer_loading:true});
      var $objeto = {
        sort:"createdAt ASC",
        pagina:_this.state.pagina+1,
        token_firebase:_this.state.token_firebase,
        latitude_usuario:_this.state.latitude_usuario,
        longitude_usuario:_this.state.longitude_usuario,
      };
      if(_this.props.usuario_perfil_id_search){
        $objeto.usuario_perfil_id_search = _this.props.usuario_perfil_id_search;
      }else{
        $objeto.usuario_perfil_id_search = _this.state.sessao.usuario_perfil_id.id;
      }

      _this.props.post_timeline($objeto).then(function (response) {
        if(!response.error){
          if(response.payload.status == "erro"){
            _this.setState({footer_loading:false,footer_loading_break:false});
          }else if(response.payload.status == "sucesso"){
            if(response.payload.data.length){
              _this.setState({
                posts: [..._this.state.posts, ...response.payload.data], 
                pagina: _this.state.pagina+1, 
                footer_loading:response.payload.data.length ? true : false,
                footer_loading_break:false 
              });
            }else{
              _this.setState({
                footer_loading:false,
                footer_loading_break:false 
              });
            }
          }
        }else{
          _this.setState({
            body_erro:response.error,
            footer_loading_break:false,
            footer_loading:false,
          });
        }
      });
    }
  }

  clickLike = (post_id) => {
    var _this = this;
    _this.props.postlike_like({
      post_id:post_id,
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    }).then(function (response) {
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg});
        }
      }else{
        _this.setState({
          body_erro:"Ops, algo de errado aconteceu"
        });
      }

    });

  }
  menuLeft = () => {
    /*var _this = this;
    Actions.camera({
      camera:true,
      video:false,
      segundos:10,
      get_video:function(dados){
        _this.setState({form_data_post_video:dados});
      },
      onBack:function(){
        Actions.pop();
        setTimeout(function(){
          if(_this.state.form_data_post_image || _this.state.form_data_post_video){
            Actions.postar({
              form_data_post_image:_this.state.form_data_post_image,
              form_data_post_video:_this.state.form_data_post_video
            });
          }
        },100);
      },
      get_imagem:function(dados){
        _this.setState({form_data_post_image:dados});
      }
    });*/
    Actions.refresh({key: 'drawer', open: true, side:"left"});
  }
  menuRight = () => {
    Actions.refresh({key: 'drawer', open: true, side:"right"});
  }
  modalComentaios = (post_id) => {
    var _this = this;
    Actions.comentar({
      id:post_id
    });
  }
  clickOpcao = (index,item) => {
    var _this = this;
    if(index==1){
      var url = "";
      if(item.imagem){
        url = item.imagem.imagemUrl
      }else if(item.video){
        url = item.video.videoUrl
      }
      _this.setState({body_loading:true});
      RNFetchBlob.fetch('GET', _this.state.url+'postimagem/imagem/'+item.imagem.id+'/800/800')
      // when response status code is 200
      .then((res) => {
        var mimetype = mime.lookup(item.imagem.name);
        let base64Str = res.base64();
        _this.setState({body_loading:false});
        Share.shareSingle(Object.assign({
          title: item.usuario_perfil_id.nome+" comparilhou um post dos Radicais Livre App",
          url:"data:"+mimetype+";base64,"+base64Str,
          message: item.texto,
        }, {
          "social": "facebook"
        }));

      });
      
    }else if(index==2){
      _this.setState({post_denuncia_id:item.id, denuncia:true});
    // remove o post
    }else if(index==3){
      $objeto = [
        {text: 'Sim', onPress: () => {
          _this.props.post_excluir({
            post_id:item.id,
            token_firebase:_this.state.token_firebase,
            latitude_usuario:_this.state.latitude_usuario,
            longitude_usuario:_this.state.longitude_usuario,
          }).then(function (response) {
            if(!response.error){
              if(response.payload.status == "erro"){
                _this.setState({body_erro:response.payload.msg});
              }
            }else{
              _this.setState({body_erro:"Ops, algo de errado aconteceu"});
            }

          });
        }},
        {text: 'Não', onPress: () => {}, style: 'cancel'},
        {text: 'Cancelar', onPress: () => {}, style: 'cancel'},
      ];
      if(Platform.OS === 'android'){
        Alert.alert(
          'Deseja realmente excluir?',
          'Este post sera excluído',
          $objeto
        );
      }else{
        AlertIOS.alert(
          'Deseja realmente excluir?',
          'Este post sera excluído',
          $objeto
        );
      }
      
    }
  }
  
  clickExcluirComentatio = (comentario_id,post_id) => {
    var _this = this;
    $objeto = [
      {text: 'Sim', onPress: () => {
        _this.props.postcomentario_excluir({
          comentario_id:comentario_id,
          post_id:post_id,
          token_firebase:_this.state.token_firebase,
          latitude_usuario:_this.state.latitude_usuario,
          longitude_usuario:_this.state.longitude_usuario,
        }).then(function (response) {
          if(!response.error){
            if(response.payload.status == "erro"){
              _this.setState({body_erro:response.payload.msg});
            }
          }else{
            _this.setState({body_erro:"Ops, algo de errado aconteceu"});
          }

        });
      }},
      {text: 'Não', onPress: () => {}, style: 'cancel'},
      {text: 'Cancelar', onPress: () => {}, style: 'cancel'},
    ];
    if(Platform.OS === 'android'){
      Alert.alert(
        'Deseja realmente excluir?',
        'Este comentário sera excluído',
        $objeto
      );
    }else{
      AlertIOS.alert(
        'Deseja realmente excluir?',
        'Este comentário sera excluído',
        $objeto
      );
    }
    
  }
  openMenu = (index) => {
    var _this = this;
    if(!_this.state.menu[index]){
      _this.state.menu[index] = true;
      _this.setState({menu:_this.state.menu});
    }else{
      _this.state.menu[index] = false;
      _this.setState({menu:_this.state.menu});
    }
  }

  clickDenunciar = (texto) => {
    if(!texto){
      alert("Descreve a denúncia");
      return;
    }
    var _this = this;
    _this.props.postdenuncia_denunciar({
      post_id:_this.state.post_denuncia_id,
      texto:texto,
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    }).then(function (response) {
      _this.setState({post_denuncia_id:"", denuncia:false});
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg});
        }else if(response.payload.status == "sucesso"){
          _this.setState({
            body_erro:"Denunciado com sucesso, após 5 denúncias o post irá sair do ar."
          });
        }
      }else{
        _this.setState({
          body_erro:"Algo de errado aconteceu, tenta novamente."
        });
      }

    });
  }
  clickSeguir = (item) => {
    //usuarioamigo_seguir
    var _this = this;
    _this.setState({body_loading:true});
    _this.props.usuarioamigo_seguir({
      perfil_id_2:item.usuario_perfil_id.id,
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    }).then(function (response) {
      _this.setState({body_loading:false});
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg});
        }else if(response.payload.status == "sucesso"){
            _this.state.usuario.aguardando_amizade = 1;
            _this.setState({usuario:_this.state.usuario, body_erro:response.payload.msg});
        }
      }else{
        _this.setState({body_erro:"Ops, algo de errado aconteceu"});
      }

    });
  }
  renderFooter () {
      return this.state.footer_loading ? <View style={{width:"100%", height:40, justifyContent: "center", alignItems:"center"}}>
        <Text style={{fontFamily:"Montserrat-Bold", fontSize:12}}>Carregando...</Text>
      </View> : <View style={{width:"100%", height:40}}/>
  }
  clickEditarPerfil () {
    var _this = this;
    Actions.atualizar({"onBack":function(){
      _this._getPerfil();
    }});
  }
  renderHeader () {
      var _this = this;
      if(this.state.usuario && this.state.carregougeral){
        item = this.state.usuario;
        relacionando = "";
        detalhes = "";
        if(item.usuario_perfil_id.membro)
          detalhes += "Membro, ";
        if(item.usuario_perfil_id.lider)
          detalhes += "Líder, ";
        if(item.usuario_perfil_id.ministro)
          detalhes += "Pastor, ";
        if(item.usuario_perfil_id.discipulador)
          detalhes += "Discipulador, ";
        if(item.usuario_perfil_id.relacionando)
          relacionando = true;
        /*if(item.data_nascimento)
          detalhes += moment().diff(item.data_nascimento, 'years')+" anos, ";
        if(item.estado_civil)
          detalhes += item.estado_civil;*/
        return (
          <View style={styles.parallaxHeader} >
            {!this.props.usuario_perfil_id_search ?
            <TouchableOpacity style={{position:"absolute", right:10, top:10, zIndex:2}} onPress={()=> {_this.setState({qrcode:true})}}>
              <Iconz name="md-barcode" size={30} color={Template.cor_2}/>
            </TouchableOpacity>
            : null }
            <View style={[styles.parallaxHeaderLeft]}>
              {this.props.usuario_perfil_id_search ?
              <TouchableOpacity onPress={()=> {Actions.pop()}} style={styles.icon_back}>
                <Icon name="keyboard-arrow-left" size={50} color="#000"/>
              </TouchableOpacity>
              : null }
              {!this.props.usuario_perfil_id_search ?
                <View style={[styles.icon_back,{alignItems:"center", justifyContent:"center"}]}>
                  <TouchableOpacity onPress={()=> {this.clickAlterarFoto()}} style={{padding:5, backgroundColor:Template.cor_1, justifyContent:"center", alignItems:"center", borderRadius:2}}>
                    <Text style={{color:"#fff", fontFamily:"Montserrat-Regular", fontSize:11}}>Alterar Foto</Text>
                  </TouchableOpacity>
                </View>
              : null }
              <View style={styles.blc_perfil_left}>
                {relacionando ?
                  <Icon style={{position:"absolute", left:10, top:0, zIndex:2, backgroundColor:"transparent"}} name="favorite" size={40} color={Template.cor_1}/>
                : null }
                <Image
                    style={styles.img_perfil}
                    source={{
                      uri: this.state.url+'usuarioavatar/avatar/'+item.usuario_perfil_id.avatar_id+'/80/80'
                    }}
                    resizeMode={Image.resizeMode.contain}
                 />
                <View style={styles.blc_idade}>
                  <Text style={styles.txt_idade} >
                    {detalhes}
                  </Text>
                  <Text style={styles.txt_idade} >
                    {item.relacionamento}
                  </Text>
                  <Text style={styles.txt_idade} >
                    {moment().diff(item.usuario_perfil_id.data_nascimento, 'years')+" anos "}
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.parallaxHeaderRight}>
              <View style={styles.blc_header_right}>
                <Text style={[styles.nome_pessoa]}>{item.usuario_perfil_id.nome}</Text>
                <View style={styles.blc_seguidores_seguidos}>
                  <View style={styles.blc_seguidores_seguidos_}>
                    <TouchableOpacity onPress={() => {Actions.search({seguidores:true, type: "refresh", seguindo:false})}}>
                      <Text style={[styles.blc_seguidores_seguidos_numero]}>{item.seguidores}</Text>
                      <Text style={[styles.blc_seguidores_seguidos_texto]}>Seguidores</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {Actions.search({seguindo:true, type: "refresh", seguidores:false})}}>
                      <Text style={[styles.blc_seguidores_seguidos_numero]}>{item.seguindo}</Text>
                      <Text style={[styles.blc_seguidores_seguidos_texto]}>Seguindo</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                {!this.props.usuario_perfil_id_search ?
                <TouchableOpacity onPress={() => {this.clickEditarPerfil()}} style={styles.blc_editar_perfil}>
                  <Text style={styles.editar_perfil}>
                    EDITAR MEU PERFIL
                  </Text>
                  <Icon name="person" size={20} color={Template.cor_3}/>
                </TouchableOpacity>
                : <View/> }
                {item.amigo ?
                <View style={styles.blc_editar_perfil}>
                  <Text style={styles.editar_perfil}>
                    SEU AMIGO
                  </Text>
                  <Icon name="done" size={20} color={Template.cor_3}/>
                </View>
                : <View/> }
                {item.aguardando_amizade ?
                <View style={styles.blc_editar_perfil}>
                  <Text style={styles.editar_perfil}>
                    AGUARDANDO CONVITE
                  </Text>
                  <Icon name="person-outline" size={20} color={Template.cor_3}/>
                </View>
                : <View/> }
                {!item.amigo && !item.aguardando_amizade && item.id != _this.state.sessao.id ?
                <TouchableOpacity onPress={() => {_this.clickSeguir(item)}} style={styles.blc_editar_perfil}>
                  <Text style={styles.editar_perfil}>
                    SEGUIR
                  </Text>
                  <Icon name="person-add" size={20} color={Template.cor_3}/>
                </TouchableOpacity>
                : <View/> }
              </View>
            </View>
          </View>
        )
      }else{
        return(<View/>);
      }
  }
  renderMiniMenu () {
      var _this = this;
      if(this.state.usuario && this.state.carregougeral && this.props.usuario_perfil_id_search){
        item = this.state.usuario;
        return (
          <View style={styles.miniatura}>
              <View style={styles.header_miniatura}>
                <TouchableOpacity onPress={()=>{Actions.pop()}} style={[styles.blc_top_voltar]}>
                  <Icon name="keyboard-arrow-left" size={30} color="#000"/>
                </TouchableOpacity>
                <View style={styles.blc_cabecalho_texto}>
                  <View style={styles.blc_cabecalho_texto_int}>
                    <View style={styles.blc_cabecalho_texto_int_blc_img}>
                       <Image
                          style={[styles.blc_cabecalho_texto_int_img]}
                          source={{
                            uri: this.state.url+'usuarioavatar/avatar/'+item.usuario_perfil_id.avatar_id+'/100/100'
                          }}
                          resizeMode={Image.resizeMode.contain}
                       />
                    </View>
                    <View style={styles.blc_cabecalho_texto_int_texto}>
                      <Text numberOfLines={1} style={styles.blc_cabecalho_texto_int_texto_label}>{item.usuario_perfil_id.nome}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
        )
      }else{
        return(<View/>);
      }
  }
  onScroll = (event) => {
    if (event.nativeEvent.contentOffset.y > 280) {
      this.setState({menutipo:2});
    } else {
      this.setState({menutipo:1});
    }
  }
  clickPerfil = (perfil_id) => {
    if(perfil_id == this.state.sessao.usuario_perfil_id.id){
      Actions.perfil();
    }else{
      Actions.perfil_amigo({usuario_perfil_id_search:perfil_id});
    }
  }
  /**
    Callback ao clica em fechar modal
  */
  clickAlterarFoto = () => {
    var _this = this;
    Actions.picker({
      get_imagem:function(dados){
        Actions.pop();
        _this.atualizarFoto(dados);
      }
    });
  }

  atualizarFoto = (imagem) => {
    var _this = this;
    _this.setState({body_loading:true});
    _this.props.usuario_atualizar({
      photo:imagem,
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    }).then(function (response) {
      _this.setState({body_loading:false});
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg});
        }else if(response.payload.status == "sucesso"){
          AsyncStorage.setItem('sessao', JSON.stringify(response.payload.data)+"");
          _this.setState({sessao:response.payload.data});
          _this._getPerfil();
        }
      }else{
        _this.setState({
          body_erro:"Algo de errado aconteceu, tenta novamente."
        });
      }

    });
  }
  render(){
      const { onScroll = () => {} } = this.props;
      var _this = this;
      return (
        <View style={{flex:1}}>
            <Body
              onToken={(token)=>{
                _this.setState({token_firebase:token});
              }}
              getUrl={(url)=>{
                _this.setState({url:url});
              }}
              onErro={(erro)=>{
                _this.setState({body_erro:erro});
              }}
              btnOk={
                <Btn estilo={1} style={{width:100, marginTop:10}} texto={"OK"} onPress={()=>{
                  _this.setState({body_erro:""});
                }}/>
              }
              carregando={this.state.body_loading}
              erro={this.state.body_erro}
              ignoreKeyboard
              conteudo={
                <View
                  style={[styles.container]}>
                    <View style={[styles.conteudo]}>
                          <View style={styles.meio}>
                            <View style={{flex:1,width:"100%", marginBottom:!this.props.usuario_perfil_id_search ? 50 : 0}}>
                              {_this.state.menutipo == 2 && this.props.usuario_perfil_id_search ?
                              <View style={{zIndex:2, position:"absolute", top:0, left:0, width:"100%",height:40,backgroundColor:"rgba(204,33,40,0.5)"}}>
                                {this.renderMiniMenu()}
                              </View>
                              : <View/> }
                              <FlatList
                                data={_this.state.posts}
                                numColumns={1}
                                style={{flex:1}}
                                keyExtractor={_this._keyExtractor}
                                ListFooterComponent={this.renderFooter.bind(this)}
                                ListHeaderComponent={this.renderHeader.bind(this)}
                                onRefresh={_this._onRefresh}
                                onEndReached={this._addMais}
                                refreshing={_this.state.refreshing}
                                onScroll={ _this.onScroll }
                                renderItem={({item,index}) => {
                                  return(
                                  <View id={index} style={{marginBottom:10}}> 
                                    {_this.state.menu[index] && !item.publicidade ?
                                    <View style={{position:"absolute", right:40, top:10, width:250, backgroundColor:"#fff", zIndex:1000, padding:10, borderRadius:10}}>
                                      <TouchableOpacity onPress={()=>{_this.clickOpcao(1,item)}} style={{padding:5, borderColor:"#E8E8E8", borderWidth:1, marginBottom:2}}>
                                        <Text style={{color:Template.cor_1, fontFamily:'Montserrat-Regular'}}>Compartilhar no Facebook</Text>
                                      </TouchableOpacity>
                                      <TouchableOpacity onPress={()=>{_this.clickOpcao(2,item)}} style={{padding:5, borderColor:"#E8E8E8", borderWidth:1, marginBottom:2}}>
                                        <Text style={{color:Template.cor_1, fontFamily:'Montserrat-Regular'}}>Denunciar</Text>
                                      </TouchableOpacity>
                                      {_this.state.sessao.usuario_perfil_id.id == item.perfil_id ?
                                      <TouchableOpacity onPress={()=>{_this.clickOpcao(3,item)}} style={{padding:5, borderColor:"#E8E8E8", borderWidth:1, marginBottom:2}}>
                                        <Text style={{color:Template.cor_1, fontFamily:'Montserrat-Regular'}}>Excluir</Text>
                                      </TouchableOpacity>
                                      : null}
                                    </View>
                                    : <View/> }
                                    {!_this.state.menu[index] && !item.publicidade ?
                                    <View style={{position:"absolute", right:10, top:10, zIndex:10000000}}>
                                      <TouchableOpacity onPress={()=>{_this.openMenu(index)}}>
                                        <Iconz name="md-list-box" size={30} color={Template.cor_3} />
                                      </TouchableOpacity>
                                    </View>
                                    : <View/> }
                                    {_this.state.menu[index] && !item.publicidade ?
                                      <View style={{position:"absolute", right:10, top:10, zIndex:10000000}}>
                                        <TouchableOpacity onPress={()=>{_this.openMenu(index)}}>
                                          <Iconz name="md-close" size={30} color={Template.cor_1} />
                                        </TouchableOpacity>
                                      </View>
                                    : <View/>}
                                    <View style={{backgroundColor:"#fff", padding:5, flexDirection:"row", alignItems:"center", justifyContent:"space-between"}}>
                                      {item.usuario_perfil_id ?
                                      <TouchableOpacity activeOpacity={.9} onPress={()=>{this.clickPerfil(item.usuario_perfil_id.id)}} style={{flexDirection:"row", alignItems:"center"}}>
                                        {item.usuario_perfil_id.avatar_id.id ?
                                        <Image
                                          style={{width:40, height:40, borderRadius:20}}
                                          source={
                                          { uri: _this.state.url+'usuarioavatar/avatar/'+item.usuario_perfil_id.avatar_id.id+'/100/100'}
                                           }/>
                                        : null }
                                        <View style={{paddingLeft:10}}>
                                          <Text style={styles.name_pessoa}>{item.usuario_perfil_id.nome}</Text>
                                          {item.createdAt ?
                                          <Text style={{fontFamily:'Montserrat-Regular',fontSize:10, color:"#999"}}>
                                            {moment(item.createdAt).fromNow()}
                                          </Text>
                                          : null}
                                        </View>
                                      </TouchableOpacity>
                                      : null }
                                    </View>
                                    {item.texto ?
                                    <View style={{padding:10}}>
                                      <Text style={{fontFamily:'Montserrat-Regular',fontSize:12, color:Template.cor_4, backgroundColor:"transparent"}}>{item.texto}</Text>
                                    </View>
                                    : null }
                                    {item.imagem ?
                                    <TouchableOpacity activeOpacity={.9} onPress={()=>{_this.modalComentaios(item.id)}}>
                                        <Image
                                          style={{width:"100%", height:320}}
                                          source={{ uri: _this.state.url+'postimagem/imagem/'+item.imagem.id+'/800/800'}}
                                          resizeMode={Image.resizeMode.cover}
                                        />
                                    </TouchableOpacity>
                                    : null}
                                    {item.video ?
                                    <View>
                                        <VideoPlayer
                                          video={{ uri:this.state.url+"upload/"+item.video.name }}
                                          videoWidth={300}
                                          videoHeight={300}
                                          muted={false}
                                          defaultMuted={false}
                                        />
                                    </View>
                                    : null }
                                    {item.publicidade ?
                                        <Image
                                          style={{width:"100%", height:320}}
                                          source={{ uri: _this.state.url+'postpublicidade/imagem/'+item.publicidade.id+'/800/800'}}
                                          resizeMode={Image.resizeMode.cover}
                                        />
                                    : null}
                                    {!item.publicidade ?
                                    <View style={{height:60, justifyContent:"space-between", flexDirection:"row", alignItems:"center", borderBottomWidth:1, borderColor:"#ccc", paddingLeft:10, paddingRight:10}}>
                                      <View style={{flex:1, flexDirection:"row", alignItems:"center", position:"relative"}}>
                                        <TouchableOpacity onPress={()=>{_this.clickLike(item.id)}} style={{position:"absolute", left:15, top:-5, width:14, height:14, backgroundColor:Template.cor_1, zIndex:10, borderRadius:7, justifyContent:"center", alignItems:"center"}}>
                                          <Text style={{backgroundColor:"transparent", fontSize:8, color:"#fff", fontFamily:'Montserrat-Regular'}}>{item.total_likes}</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={()=>{_this.clickLike(item.id)}}>
                                          <Iconz name="ios-hand-outline" size={30} color={Template.cor_2} />
                                        </TouchableOpacity>
                                        {item.ultimos_likes ?
                                        <View style={{position:"absolute", left:33, top:-5, width:220, height:45, justifyContent:"flex-start", alignItems:"center", flexDirection:"row"}}>
                                          {item.ultimos_likes.map((person, index) => (
                                            <View key={index}>
                                            {item.ultimos_likes[index].usuario_perfil_id ?
                                            <Image
                                              style={{width:20, height:20, borderRadius:10, marginLeft:index <=0 ? 0 : -10}}
                                              source={{uri:_this.state.url+'usuarioavatar/avatar/'+item.ultimos_likes[index].usuario_perfil_id.avatar_id+'/40/40'}}
                                              resizeMode={Image.resizeMode.cover}
                                            />
                                            : null}
                                            </View>
                                          ))}
                                        </View>
                                        : null}
                                      </View>
                                      <TouchableOpacity onPress={()=>{_this.modalComentaios(item.id)}} style={{flexDirection:"row", alignItems:"center"}}>
                                        <View style={{position:"absolute", right:0, top:0, width:14, height:14, backgroundColor:Template.cor_1, zIndex:10, borderRadius:7, justifyContent:"center", alignItems:"center"}}>
                                          <Text style={{backgroundColor:"transparent", fontSize:8, color:"#fff", fontFamily:'Montserrat-Regular'}}>{item.total_comentarios}</Text>
                                        </View>
                                        <Iconz name="ios-chatbubbles-outline" size={30} color={Template.cor_2} />
                                      </TouchableOpacity>
                                    </View>
                                    : null }
                                    {!item.publicidade ?
                                    <View style={{padding:10, flexDirection:"row", flexWrap:"wrap"}}>
                                      {item.ultimos_likes.length ?
                                      <Text style={{fontFamily:'Montserrat-Regular',fontSize:12, color:"#999", marginRight:5}}>
                                        Acharam Radical:
                                      </Text>
                                      : <Text/>}
                                      {item.ultimos_likes && item.ultimos_likes.map((person, index) => (
                                      <TouchableOpacity onPress={()=>{this.clickPerfil(item.ultimos_likes[index].usuario_perfil_id.id)}} key={index} style={{marginRight:5}}>
                                        <Text style={{fontFamily:'Montserrat-Bold',fontSize:12, color:Template.cor_2}}>{item.ultimos_likes[index].usuario_perfil_id.nome}
                                        </Text>
                                      </TouchableOpacity>
                                      ))}
                                      {item.ultimos_likes && item.total_likes > 3 ?
                                      <View style={{marginRight:5}}>
                                        <Text style={{fontFamily:'Montserrat-Bold',fontSize:12, color:Template.cor_2}}>E mais {item.total_likes - 3} pessoa(s)</Text>
                                      </View>
                                      : <View/>}
                                    </View>
                                    : null }
                                    {item.ultimos_comentarios.length && !item.publicidade ?
                                    <View style={{flexDirection:"column"}}>
                                      {item.ultimos_comentarios.map((person, index) => (
                                      <View key={index} style={{padding:10, flexDirection:"row", flexWrap:"wrap", paddingTop:0, alignItems:"center"}}>
                                        <TouchableOpacity onPress={()=>{this.clickPerfil(item.ultimos_comentarios[index].usuario_perfil_id.id)}}>
                                          <Text style={{fontFamily:'Montserrat-Bold',fontSize:12, color:Template.cor_2, marginRight:5}}>
                                            {item.ultimos_comentarios[index].usuario_perfil_id.nome}:
                                          </Text>
                                        </TouchableOpacity>
                                        {item.ultimos_comentarios[index].usuario_perfil_id.id != _this.state.sessao.usuario_perfil_id.id ?
                                        <Text style={{fontFamily:'Montserrat-Regular',fontSize:12, color:Template.cor_2}}>
                                          {item.ultimos_comentarios[index].texto}
                                        </Text>
                                        : 
                                         <TouchableOpacity onPress={()=>{_this.clickExcluirComentatio(item.ultimos_comentarios[index].id,item.id)}}>
                                          <Text style={{fontFamily:'Montserrat-Regular',fontSize:12, color:Template.cor_2}}>
                                            {item.ultimos_comentarios[index].texto}
                                          </Text>
                                         </TouchableOpacity>
                                        }
                                      </View>
                                      ))}
                                    </View>
                                    : <View/> }
                                  </View>);
                                }}
                              />
                            </View>
                          </View>
                    </View>
                </View>
              }
            />
            <Prompt
            title="Denunciar Post"
            placeholder="Escreve a denúncia"
            visible={ this.state.denuncia }
            onCancel={ () => this.setState({
              denuncia: false,
              post_denuncia_id:""
            }) }
            onSubmit={ (value) => {
              _this.clickDenunciar(value);
            }}/>
            <Modal 
              isVisible={this.state.qrcode}
              backdropColor={"#fff"}
              backdropOpacity={1}
              animationIn={'zoomInDown'}
              animationOut={'zoomOutUp'}
              animationInTiming={1000}
              animationOutTiming={1000}
              backdropTransitionInTiming={1000}
              backdropTransitionOutTiming={1000}
            >
              <View style={{flex:1, justifyContent:"center", alignItems:"center"}}>
                <TouchableOpacity onPress={()=> {_this.setState({qrcode:false})}} style={{position:"absolute", left:10, top:10}}>
                  <Icon name="keyboard-arrow-left" size={50} color="#000"/>
                </TouchableOpacity>
                <Text style={{fontFamily:"Montserrat-Regular", position:"absolute", paddingLeft:20, paddingRight:20, left:0, bottom:20, width:"100%", textAlign:"center", color:"#000"}}>Peça seu amigo para ler o código para te adicionar, o leitor de código fica no menu de pesquisa.</Text>
                {this.state.usuario.id ?
                <QRCode
                  size={250}
                  color={"#000"}
                  logoBackgroundColor={"#000"}
                  value={this.state.usuario.id.toString()}
                />
                : null}
              </View>
            </Modal>
            {_this.state.novidade ?
            <TouchableOpacity onPress={()=>{_this._onRefresh()}} style={styles.alerta}>
              <Text style={styles.alerta_texto}>Existe {_this.state.novidade.length} novidade abençoada.</Text>
            </TouchableOpacity>
            : null}
          </View>
      );
    }
}

const mapStateToPropos = (state) => {
  return { data: state.reducer.dados };
}

const mapDispatchToPropos = (dispatch) => {
  return bindActionCreators({
    post_timeline,
    postlike_like,
    post_getpost,
    post_excluir,
    postcomentario_excluir,
    postcomentario_get_ultimos_comentarios,
    postdenuncia_denunciar,
    usuario_get,
    usuario_atualizar,
    usuarioamigo_seguir
  },dispatch);
}

export default connect(mapStateToPropos, mapDispatchToPropos)(Index);
