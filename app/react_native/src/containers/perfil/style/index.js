const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;
var {height: deviceHeight, width:deviceWidth} = Dimensions.get("window");
import Template from '../../../constants/cores';

export default {
  alerta: {
    marginTop:(Platform.OS === 'ios') ? 20 : 0,
    backgroundColor:Template.cor_1,
    width:"100%", 
    height:40, 
    position:"absolute", 
    top:0, 
    left:0, 
    zIndex:20,
    justifyContent:"center",
    alignItems:"center"
  },
  alerta_texto: {
    color:"#fff",
    fontFamily:'Montserrat-Bold',
  },
  container: {
    backgroundColor:'transparent',
    justifyContent: 'flex-end',
    alignItems: 'center',
    flex:1,
    width:"100%",
    backgroundColor:"#fff"
  },
  conteudo: {
    justifyContent: 'center',
    alignItems: 'center',
    flex:1,
    width:"100%"
  },
  scroll_100:{
    flex:1,
    width:"100%"
  },
  altura100:{
    flexGrow: 1,
    width:"100%"
  },
  meio: {
    width:'100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flex:1,
  },
  menu:{
    position:"absolute", 
    left:5, 
    top:10,
    zIndex:2
  },
  blc_header:{
    height:50, 
    justifyContent:"center", 
    alignItems:"center", 
    backgroundColor:"#fff",
    width:"100%",
    borderColor:Template.cor_3,
    borderBottomWidth:1
  },
  icon_menu:{
    backgroundColor:"transparent"
  },
  title_h1:{
    color:"#fff",
    fontFamily:'Montserrat-Bold',
  },
  content: {
      padding: 16,
      paddingTop: 0,
      paddingBottom: 0
  },
  name_pessoa:{
    fontFamily:'Montserrat-Bold',
    fontSize:14,
    color:Template.cor_1
  },
  imageWrapper: {
    flex: 1,
    alignItems: 'stretch'
  },
  fitImage:{
    flex: 1, alignSelf: 'stretch'
  },
  KeyboardAwareScrollView:{
    width:"100%"
  },
  KeyboardAwareScrollViewContent:{
    width:"100%", 
    justifyContent:"center", 
    alignItems:"center",
    flexGrow: 1
  },
  KeyboardAwareScrollViewContentEnd:{
    width:"100%", 
    justifyContent:"flex-end", 
    alignItems:"center",
    flexGrow: 1
  },
  text_completar_dados: {
    fontFamily:"Montserrat-Regular", 
    fontSize:18, 
    textAlign:"center",
    color:Template.cor_2,
    marginBottom:10,
    padding:10
  },
   parallaxHeader: {
    height:280,
    flexDirection:"row",
    marginBottom:20
  },
  parallaxHeaderLeft: {
    flex:.4,
    borderBottomRightRadius:5,
    backgroundColor:Template.cor_5
  },
  parallaxHeaderRight: {
    backgroundColor:'#fff',
    flex:.6,
    padding:20
  },
  icon_back: {
    marginTop:10,
    flex:.2
  },
  img_perfil: {
    width:80,
    height:80,
    borderRadius:40,
    marginTop:10
  },
  blc_perfil_left: {
    flex:.8,
    justifyContent:"flex-start",
    alignItems:"center",
  },
  txt_idade: {
    textAlign:'center',
    color:Template.cor_1,
    fontSize:15,
    fontFamily: 'Montserrat-Bold',
  },
  txt_idade_: {
    fontFamily: 'Montserrat-Regular',
  },
  blc_idade: {
    position:"absolute", 
    bottom:20
  },
  blc_idade: {
    position:"absolute", 
    bottom:20
  },
  bolinhas_container: {
    flexDirection:"row", 
    justifyContent:"flex-end"
  },
  blc_bolinhas: {
    height:24, 
    borderRadius:12, 
    borderWidth:2, 
    borderColor:"#fff", 
    paddingLeft:5, 
    paddingRight:9, 
    flexDirection:"row", 
    justifyContent:"center", 
    alignItems:"center"
  },
  bolinhas: {
    width:4, 
    height:4, 
    borderRadius:2, 
    marginLeft:4
  },
  blc_header_right: {
    flex:1, 
    alignItems:'center', 
    justifyContent:"center", 
    marginTop:20
  },
  nome_pessoa: {
    flex:.4, 
    fontSize:28, 
    textAlign:"left", 
    width:"100%",
    fontFamily: 'Montserrat-Bold',
    color:Template.cor_1
  },
  blc_seguidores_seguidos: {
    flex:.4, 
    justifyContent:"center", 
    alignItems:"flex-start", 
    width:"100%"
  },
  blc_seguidores_seguidos_: {
    flex:1, 
    width:"100%", 
    justifyContent:"space-between", 
    flexDirection:"row", 
    alignItems:"center"
  },
  blc_seguidores_seguidos_numero: {
    fontSize:20,
    fontFamily: 'Montserrat-Bold',
  },
  blc_seguidores_seguidos_texto: {
    fontSize:14,
    fontFamily: 'Montserrat-Regular',
    color:Template.cor_4
  },
  blc_editar_perfil: {
    flex:.2, 
    justifyContent:"center", 
    alignItems:"flex-end", 
    width:"100%",
    flexDirection:"row"
  },
  editar_perfil: {
    fontSize:13,
    fontFamily: 'Montserrat-Regular',
    marginRight:10,
    color:Template.cor_2
  },
  editar_perfil_img: {
    width:20, 
    height:20
  },
  miniatura: {
    backgroundColor:"rgba(255,255,255,0.8)"
  },
  miniaturaText: {
    color: 'white',
    fontSize: 20,
    margin: 10
  },
  header_miniatura: {
    flexDirection:"row", 
    height:"100%"
  },
  blc_top_voltar: {
    flex:.1, 
    borderBottomRightRadius:10,
    justifyContent:"center",
    alignItems:"center",
    height:"100%"
  },
  blc_cabecalho_texto: {
    flex:.9, 
    backgroundColor:"#fff"
  },
  blc_cabecalho_texto_int: {
    flexDirection:"row", 
    flex:1
  },
  blc_cabecalho_texto_int_blc_img: {
    flex:.2, 
    justifyContent:"center", 
    alignItems:"center"
  },
  blc_cabecalho_texto_int_img: {
    width:30, 
    height:30, 
    borderWidth:2, 
    borderRadius:15
  },
  blc_cabecalho_texto_int_texto: {
    flex:.6, 
    justifyContent:"center", 
    alignItems:"center",
  },
  blc_cabecalho_texto_int_texto_label: {
    fontFamily: 'Montserrat-Regular',
    fontSize:15
  },
  blc_cabecalho_texto_int_bolas: {
    flex:.2, 
    justifyContent:"center", 
    alignItems:"center"
  },

};
