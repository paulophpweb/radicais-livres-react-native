import React, { Component } from 'react';
import { View, Text, Dimensions, TextInput, Button, TouchableOpacity, Image, ScrollView, AsyncStorage, KeyboardAvoidingView, Alert, Platform } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Animatable from 'react-native-animatable';
import { Actions } from 'react-native-router-flux';
import { auth_facebook_login  } from '../../actions';
var {FBLoginManager} = require('react-native-facebook-login');
var moment = require('moment');
require('moment/locale/pt-br');
import Modal from 'react-native-modal'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Body from '../../components/ajax_body';
var FBLoginMock = require('../../components/facebook/FBLoginMock.js');
import Uteis from '../../lib/Uteis';
import Btn from '../../components/btn';
import Input from '../../components/inputs';
import Template from '../../constants/cores';
import styles from './style';

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      completar: false,
      form_name: "",
      facebook_name: "",
      form_genre:"",
      facebook_genre:"",
      form_date_of_birth:"",
      facebook_date_of_birth:"",
      form_facebookId:"",
      form_facebookToken:"",
      form_email: "",
      facebook_email: "",
      form_imagem: "",
      url: "",
      body_erro:"",
      carregando:false,
      title_atualizar:"ATUALIZAR",
      atualizar:false,
      title_salvar:"SALVAR",
      salvar:false
    }
  }
  /**
    Metodo valida login facebook
  */
  validaLogin(data){
    var _this = this;
    _this.setState({carregando:true});
    Uteis.getUserDataFacebook(data.credentials.token,data.credentials.userId).then((user) => {
      _this.setState({
        form_name:user.name,
        facebook_name:user.name,
        form_genre:user.gender,
        facebook_genre:user.gender,
        form_facebookId:user.id,
        form_facebookToken:data.credentials.token,
        form_imagem: user.imagem,
        form_email:"",
        facebook_email:""
      });
      _this.props.auth_facebook_login({
        facebookId:user.id,
        nome:user.name,
        genero:user.gender,
        facebookToken:data.credentials.token,
        photo:user.imagem,
      }).then(function (response) {
        if(!response.error){
          if(response.payload.status == "erro"){
            _this.setState({
                completar:false,
                carregando:false,
                salvar:false,
                atualizar:false,
                body_erro:"Um erro inesperado aconteceu."
            });
          }else if(response.payload.status == "sucesso"){
            if(!response.payload.data.email){
              _this.setState({
                  completar:true,
                  carregando:false,
                  atualizar:true,
                  salvar:false,
              });
            }else{
              AsyncStorage.setItem('sessao', JSON.stringify(response.payload.data)+"");
              Actions.root({ type: "reset" });
            }
          }else{
            _this.setState({
                completar:false,
                carregando:false,
                salvar:false,
                atualizar:false,
                body_erro:"Um erro aconteceu, tente novamente."
            });
          }
        }else{
          _this.setState({
              completar:false,
              carregando:false,
              salvar:false,
              atualizar:false,
              body_erro:"Ops, um erro de conexão com o servidor aconteceu."
          });
        }
        
      });
    });
  }
  /**
    Metodo Cadastrar
  */
  clickCadastrar = () => {
    var _this = this;
    var $erro = '';
    if(!this.state.form_name){
      $erro += 'Por favor, preencha o campo nome';
    }else if(!this.state.form_facebookToken){
      $erro += 'Ops, não achamos o seu token do facebook, que triste isso.';
    }else if(!this.state.form_facebookId){
      $erro += 'Ops, não achamos o seu id de usuário do facebook, lamentamos.';
    }else if(!this.state.form_genre){
      $erro += 'Não conseguimos obter o seu gênero pelo facebook';
    }else if(!this.state.form_date_of_birth){
      $erro += 'Preencha o campo data de nascimento';
    }else if(!moment(this.state.form_date_of_birth, 'DD/MM/YYYY', true).isValid()){
      $erro += 'Data de nascimento inválida';
    }else if(!this.state.form_email){
      $erro += 'Por favor, preencha o campo email';
    }else if(!Uteis.validaEmail(this.state.form_email)){
      $erro += 'Este email não e um email válido';
    }
    if($erro){
      alert($erro);
    }else{
      _this.setState({title_salvar:"..."});
      _this.props.auth_facebook_login({
        email:this.state.form_email,
        facebookId:this.state.form_facebookId,
        nome:this.state.form_name,
        genero:this.state.form_genre,
        data_nascimento:this.state.form_date_of_birth,
        facebookToken:this.state.form_facebookToken,
        photo:this.state.form_imagem
      }).then(function (response) {
        if(!response.error){
          if(response.payload.status == "erro"){
            alert(response.payload.msg);
            _this.setState({title_salvar:"SALVAR"});
          }else{
            AsyncStorage.setItem('sessao', JSON.stringify(response.payload.data)+"");
            setTimeout(function(){
              Actions.root({ type: "reset" });
            },2000);
          }
        }else{
          _this.setState({
              completar:false,
              carregando:false,
              salvar:false,
              atualizar:false,
              body_erro:"Um erro inesperado aconteceu."
          });
        }
        
      });
    }
  }
  /**
    Metodo Atualizar
  */
  clickAtualizar = () => {
    var _this = this;
    var $erro = '';
    if(!this.state.form_name){
      $erro += 'Por favor, preencha o campo nome';
    }else if(!this.state.form_facebookToken){
      $erro += 'Ops, não achamos o seu token do facebook, que triste isso.';
    }else if(!this.state.form_facebookId){
      $erro += 'Ops, não achamos o seu id de usuário do facebook, lamentamos.';
    }else if(!this.state.form_genre){
      $erro += 'Não conseguimos obter o seu gênero pelo facebook';
    }else if(!this.state.form_date_of_birth){
      $erro += 'Não conseguimos obter a sua data de nascimento pelo facebook';
    }else if(!this.state.form_email){
      $erro += 'Por favor, preencha o campo email';
    }else if(!Uteis.validaEmail(this.state.form_email)){
      $erro += 'Este email não e um email válido';
    }
    if($erro){
      alert($erro);
    }else{
      _this.setState({title_atualizar:"..."});
      _this.props.auth_facebook_login({
        email:this.state.form_email,
        facebookId:this.state.form_facebookId,
        nome:this.state.form_name,
        genero:this.state.form_genre,
        data_nascimento:this.state.form_date_of_birth,
        facebookToken:this.state.form_facebookToken,
        photo:""
      }).then(function (response) {
        if(!response.error){
          if(response.payload.status == "erro"){
            alert(response.payload.msg);
            _this.setState({title_atualizar:"ATUALIZAR"});
          }else{
            AsyncStorage.setItem('sessao', JSON.stringify(response.payload.data)+"");
            setTimeout(function(){
              Actions.root({ type: "reset" });
            },2000);
          }
        }else{
          _this.setState({
              completar:false,
              carregando:false,
              salvar:false,
              atualizar:false,
              body_erro:"Um erro inesperado aconteceu."
          });
        }
        
      });
    }
  }
  /**
    Metodo ir para proximo input
  */
  focusNextField = (nextField) => {
    this.refs[nextField].refs[nextField].focus();
  };
  render(){
    var _this = this;
    return(
      <Body
        logo={
          <Image source={require('../../images/mao.png')} style={{height:40}} resizeMode={Image.resizeMode.contain}/>
        }
        getUrl={(url)=>{
          _this.setState({url:url});
        }}
        onErro={(erro)=>{
          _this.setState({body_erro:erro});
        }}
        btnOk={
          <Btn estilo={1} style={{width:100, marginTop:10}} texto={"OK"} onPress={()=>{
            _this.setState({body_erro:""});
          }}/>
        }
        carregando={this.state.carregando}
        erro={this.state.body_erro}
        conteudo={
          <View style={styles.meio}>
            <Image style={styles.logo} resizeMode={Image.resizeMode.contain} source={require('../../images/logo.png')}/>
            <FBLoginMock
              style={{marginTop:10, width:220}}
              onPress={function(){
                
              }}
              onLogin={function(data){
                _this.validaLogin(data);
              }}
            />
            <View style={{marginTop:5}}>
              <Text style={styles.text_facebook}>Não postamos nada em seu Facebook.</Text>
            </View>
            <Modal 
              isVisible={this.state.completar}
              backdropColor={Template.cor_2}
              backdropOpacity={1}
              animationIn={'zoomInDown'}
              animationOut={'zoomOutUp'}
              animationInTiming={1000}
              animationOutTiming={1000}
              backdropTransitionInTiming={1000}
              backdropTransitionOutTiming={1000}
            >
              <View style={{ flex: 1, backgroundColor:"#fff", justifyContent:"center", alignItems:"center" }}>
                <KeyboardAwareScrollView 
                style={styles.KeyboardAwareScrollView} 
                contentContainerStyle={styles.KeyboardAwareScrollViewContent}
                automaticallyAdjustContentInsets={false}
                bounces={false}
                showsVerticalScrollIndicator={false}
                >
                  <Text style={styles.text_completar_dados}>Complete os seus dados abaixo:</Text>
                  <View style={{width:80, height:80, borderWidth:2, borderColor:"#000", borderRadius:40, padding:10, alignItems:"center", justifyContent:"center"}}>
                    {!this.state.form_imagem ?
                    <Image style={{width:70, height:70, borderRadius:35}} source={require('../../images/no_image.png')} resizeMode={'cover'} />
                    : <Image style={{width:70, height:70, borderRadius:35}} source={{uri:this.state.form_imagem.uri}} resizeMode={'cover'} /> }
                  </View>
                    {!this.state.facebook_name ?
                    <View style={{marginTop:10, width:280}}>
                      <Input
                        ref="1"
                        refInput="1"
                        onChangeText={value => this.setState({form_nome: value})}
                        texto={"Nome"}
                        returnKeyType={'next'}
                        blurOnSubmit={false}
                        value={this.state.form_name}
                        style={{width:"100%"}}
                        onSubmitEditing={() => this.focusNextField('2')}
                      />
                    </View>
                    : null }
                    {!this.state.facebook_email ?
                    <View style={{marginTop:10, width:280}}>
                      <Input
                        ref="2"
                        refInput="2"
                        onChangeText={value => this.setState({form_email: value})}
                        texto={"Email"}
                        value={this.state.form_email}
                        returnKeyType={'next'}
                        keyboardType={"email-address"}
                        blurOnSubmit={false}
                        style={styles.mb10}
                        onSubmitEditing={() => this.focusNextField('3')}
                      />
                    </View>
                    : null }
                    {!this.state.facebook_date_of_birth ?
                    <View style={{marginTop:10, width:280}}>
                      <Input
                        ref="3"
                        refInput="3"
                        onChangeText={value => this.setState({form_date_of_birth: Uteis.datamask(value)})}
                        texto={"Data de Nascimento"}
                        value={this.state.form_date_of_birth}
                        returnKeyType={'done'}
                        keyboardType={"phone-pad"}
                        blurOnSubmit={false}
                        style={styles.mb10}
                        onSubmitEditing={() => this.clickAtualizar()}
                      />
                    </View>
                    : null }
                    <View style={{marginTop:10, width:"100%", alignItems:"center"}}>
                      {this.state.salvar ?
                      <Btn onPress={this.clickCadastrar} style={{width:220, marginTop:20}} estilo={1} texto={this.state.title_salvar}/>
                      : null}
                      {this.state.atualizar ?
                      <Btn onPress={this.clickAtualizar} style={{width:220, marginTop:20}} estilo={1} texto={this.state.title_atualizar}/>
                      : null}
                    </View>
                </KeyboardAwareScrollView>
              </View>
            </Modal>
          </View>
        }
      />
    );
  }
}

const mapStateToPropos = (state) => {
  return { data: state.reducer.dados };
}

const mapDispatchToPropos = (dispatch) => {
  return bindActionCreators({
    auth_facebook_login
  },dispatch);
}

export default connect(mapStateToPropos, mapDispatchToPropos)(Index);
