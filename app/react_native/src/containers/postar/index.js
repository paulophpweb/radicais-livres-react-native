import React, { Component } from 'react';
import { View, Text, Image, Animated, Dimensions, Modal, TouchableOpacity, AsyncStorage, Platform, Alert } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Actions, ActionConst } from 'react-native-router-flux';
import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconIonic from 'react-native-vector-icons/Ionicons';
import ImageResizer from 'react-native-image-resizer';
import {
  CameraKitGallery,
  CameraKitCamera,
  CameraKitGalleryView
} from 'react-native-camera-kit';
var mimetype = require('react-native-mime-types');
import Upload from '../../lib/Upload';
import styles from './style';

var {
  height: deviceHeight
} = Dimensions.get("window");

class Index extends Component {
  constructor(props){
      super (props);
      this.state = {
        
      };
      this.upload = new Upload();
  }
  /**
    Callback ao clica em fechar modal
  */
  fecharModal = () => {
    this.container.setNativeProps({style: {backgroundColor:"rgba(0,0,0,0)"}});
    setTimeout(function(){
      Actions.pop();
    },100);
  }
  /**
    Callback ao clica em fechar modal
  */
  clickCrop = (objeto) => {
    ImageResizer.createResizedImage(objeto.selected, 800, 800, 'JPEG', 100)
    .then((resizedImageUri) => {
      var objeto = {selected:resizedImageUri};
      Actions.crop({
        type: ActionConst.REPLACE,
        objeto, 
        get_imagem:function(dados){
          Actions.postar({
            type: ActionConst.REPLACE,
            form_data_post_image:dados,
          });
        }
      });
    });
    
  }

  cameraRapida = () => {
    var _this = this;
    /*Actions.camera({
      type: ActionConst.REPLACE,
      camera:true,
      video:false,
      segundos:10
    });*/
    this.upload.openCamera(function($data){
      Actions.postar({
        type: ActionConst.REPLACE,
        form_data_post_image:$data,
      });
    },function(){

    });
  }
  cameraRapidaVideo = () => {
    var _this = this;
    this.upload.openVideoEscolha(function($data){
      Actions.postar({
        type: ActionConst.REPLACE,
        form_data_post_video:$data,
      });
    },function(){

    });
  }
  render(){
    return (
      <Modal
         transparent={true}
         animationType={"fade"}
         onRequestClose={() => {}}
      >
        <Animatable.View
          style={[styles.container]} ref={component => this.container = component}>
             <CameraKitGalleryView
              ref={gallery => this.gallery = gallery}
              style={{flex: 0.8, marginTop: Platform.OS === 'ios' ? 20 : 0}}
              minimumInteritemSpacing={10}
              minimumLineSpacing={10}
              albumName={'All Photos'}
              columnCount={3}
              onTapImage={(result)=>{
                this.clickCrop(result.nativeEvent);
              }}
              getUrlOnTapImage={true}
            />
            <View style={[styles.container_int]}>
                <View style={styles.container_botoes}>
                    <TouchableOpacity onPress={() => {this.cameraRapida();}} style={styles.botoes}>
                        <IconIonic name="ios-camera-outline" size={45} color="#fff" style={[styles.imgIcon_]}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.fecharModal}>
                     <View style={styles.btnBottom}>
                        <IconIonic name="ios-close-circle" size={45} color="#fff"/>
                     </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {this.cameraRapidaVideo();}} style={styles.botoes}>
                      <IconIonic name="ios-videocam" size={45} color="#fff" style={[styles.imgIcon_]}/>
                    </TouchableOpacity>
                </View>

                {/*<View style={styles.container_botoes}>
                    <TouchableOpacity onPress={this.fecharModal} style={styles.botoes}>
                        <Icon name="arrow-back" size={30} color="#fff"/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {this.cameraRapida();}} style={styles.botoes}>
                        <IconIonic name="ios-camera-outline" size={45} color="#fff" style={[styles.imgIcon_]}/>
                        <Text style={styles.texto_botoes}>TIRAR FOTO</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.botoes,{opacity:0}]}>
                        
                    </TouchableOpacity>
                </View>*/}
                
               
             </View>
         </Animatable.View>
       </Modal>
    );
  }
}

const mapStateToPropos = (state) => {
  return {};
}

const mapDispatchToPropos = (dispatch) => {
  return bindActionCreators({},dispatch);
}

export default connect(mapStateToPropos, mapDispatchToPropos)(Index);
