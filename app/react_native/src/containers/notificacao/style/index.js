const React = require('react-native');

const { StyleSheet, Platform } = React;
import Template from '../../../constants/cores';

export default {
  meio: {
    justifyContent: 'center',
    alignItems: 'center',
    flex:1
  },
  logo: {
    width:200, 
    height:90, 
    marginBottom:10
  },
  text_facebook: {
    fontFamily:"Montserrat-Regular", 
    fontSize:10, 
    textAlign:"center",
    color:Template.cor_2
  },
  KeyboardAwareScrollView:{
    width:"100%"
  },
  KeyboardAwareScrollViewContent:{
    width:"100%", 
    justifyContent:"center", 
    alignItems:"center",
    flexGrow: 1
  },
  text_completar_dados: {
    fontFamily:"Montserrat-Regular", 
    fontSize:18, 
    textAlign:"center",
    color:Template.cor_2,
    marginBottom:10,
    padding:10
  },
  container_list:{
    flex: 1,
    justifyContent: 'center'
  },
  container_list_fundo: {
    position: 'absolute', 
    right: 0, 
    height: 50, 
    flexDirection: 'row', 
    alignItems: 'center', 
    backgroundColor:Template.cor_1, 
    width:170
  },
  container_list_fundo_inners: {
    flex:1,
    height:"100%",
    alignItems:"center",
    justifyContent:"center"
  },
  container_list_fundo_inners_text: {
    fontSize:12,
    color:"#fff",
    fontFamily: 'Montserrat-Regular'
  },
};
