import React, { Component } from 'react';
import { View, TouchableOpacity, Text, AsyncStorage, FlatList, Image, Animated, Alert, Platform, AlertIOS } from 'react-native';
import { List, ListItem, SearchBar, CheckBox, Slider, ButtonGroup } from "react-native-elements";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
var moment = require('moment');
require('moment/locale/pt-br');
import Interactable from 'react-native-interactable';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { usuarionotificacao_get, usuarioamigo_aceitar, usuarionotificacao_excluir  } from '../../actions';
import Uteis from '../../lib/Uteis';
import Header from '../../components/header';
import Btn from '../../components/btn';
import Body from '../../components/ajax_body';
import Input from '../../components/inputs';
import Template from '../../constants/cores';
import styles from './style';

class Postar extends Component {
  _keyExtractor = (item, index) => index;

  constructor(props) {
    super(props);
    this.state = {
      body_erro:"",
      body_loading:true,
      token_firebase:"",
      url:"",
      sessao:null,
      notificacao:[],
      refreshing:false,
      footer_loading:false,
      footer_loading_break:false,
      pagina:1,
      latitude_usuario:"",
      longitude_usuario:""
    }

  }
  async componentDidMount(){
    var _this = this;
    var Sessao = await AsyncStorage.getItem("sessao");
    if(Sessao){
      Sessao = JSON.parse(Sessao);
      this.setState({"sessao": Sessao});
      var body_ = new Body();
      body_._getMyLocation().then((position) => {
        position = position.coords;
        _this.setState({
          latitude_usuario: position.latitude,
          longitude_usuario: position.longitude
        });
      });
      this._getNotificacao();
    }
  }
  _getNotificacao = () => {
    var _this = this;
    _this.props.usuarionotificacao_get({
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    }).then(function (response) {
      _this.setState({body_loading:false});
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg, pagina:1});
        }else if(response.payload.status == "sucesso"){
          if(response.payload.data.length){
            _this.setState({notificacao:response.payload.data,body_loading:false, pagina:1});
          }else{
            _this.setState({body_loading:false,notificacao:[], pagina:1});
          }
        }
      }else{
        _this.setState({
          body_erro:"Ops, algo de errado aconteceu", 
          pagina:1, 
          body_loading:false,
          notificacao:[]
        });
      }

    });
  }
  _onRefresh = () => {
    var _this = this;
    _this.setState({refreshing:true});
    _this.props.usuarionotificacao_get({
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
    }).then(function (response) {
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg, refreshing:false, pagina:1});
        }else if(response.payload.status == "sucesso"){
          if(response.payload.data.length){
            _this.setState({
              notificacao:response.payload.data,
              body_loading:false, 
              refreshing:false, 
              pagina:1
            });
          }else{
            _this.setState({
              body_loading:false, 
              refreshing:false, 
              pagina:1, 
              notificacao:[]
            });
          }
        }
      }else{
        _this.setState({
          body_loading:false, 
          refreshing:false, 
          pagina:1, 
          notificacao:[],
          body_erro:"Ops, algo de errado aconteceu"
        });
      }

    });
  }
  _addMais = (info: {distanceFromEnd: 10}) => {
    _this = this;
    if(!_this.state.footer_loading_break){
      _this.setState({footer_loading_break:true, footer_loading:true});
      _this.props.usuarionotificacao_get({
        pagina:_this.state.pagina+1,
        token_firebase:_this.state.token_firebase,
        latitude_usuario:_this.state.latitude_usuario,
        longitude_usuario:_this.state.longitude_usuario,
      }).then(function (response) {
        if(!response.error){
          if(response.payload.status == "erro"){
            _this.setState({footer_loading:false,footer_loading_break:false});
          }else if(response.payload.status == "sucesso"){
            if(response.payload.data.length){
              _this.setState({
                notificacao: [..._this.state.notificacao, ...response.payload.data], 
                pagina: _this.state.pagina+1, 
                footer_loading:response.payload.data.length ? true : false,
                footer_loading_break:false 
              });
            }else{
              _this.setState({
                footer_loading:false,
                footer_loading_break:false 
              });
            }
          }
        }else{
          _this.setState({
            body_erro:response.error,
            footer_loading_break:false,
            footer_loading:false,
          });
        }
      });
    }
  }
  clickAceitar = (item,index) => {
    var _this = this;
    _this.setState({body_loading:true});
    _this.props.usuarioamigo_aceitar({
      id:item.notificacao.id,
      perfil_id_1:item.notificacao.perfil_id_1.id,
    }).then(function (response) {
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg,body_loading:false});
        }else if(response.payload.status == "sucesso"){
          _this.state.notificacao.splice(index,1);
          _this.setState({body_loading:false, notificacao:_this.state.notificacao});
        }
      }else{
        _this.setState({
          body_erro:"Ops, algo de errado aconteceu",
          body_loading:false
        });
      }

    });

  }
  menuLeft = () => {
    Actions.refresh({key: 'drawer', open: true, side:"left"});
  }
  clickExcluirNotificacao = (item,index,rejeitar) => {
    var _this = this;
    $objeto = [
      {text: 'Sim', onPress: () => {
        _this.props.usuarionotificacao_excluir({
          perfil_id_1:rejeitar ? item.notificacao.perfil_id_1.id : "",
          notificacao_id:item.notificacao.id,
          token_firebase:_this.state.token_firebase,
          latitude_usuario:_this.state.latitude_usuario,
          longitude_usuario:_this.state.longitude_usuario,
        }).then(function (response) {
          if(!response.error){
            if(response.payload.status == "erro"){
              _this.setState({body_erro:response.payload.msg});
            }else if(response.payload.status == "sucesso"){
              _this.state.notificacao.splice(index,1);
              _this.setState({notificacao:_this.state.notificacao});
            }
          }else{
            _this.setState({body_erro:"Ops, algo de errado aconteceu"});
          }

        });
      }},
      {text: 'Não', onPress: () => {}, style: 'cancel'},
      {text: 'Cancelar', onPress: () => {}, style: 'cancel'},
    ];
    if(Platform.OS === 'android'){
      Alert.alert(
        'Deseja realmente excluir?',
        'Esta notificação será excluída',
        $objeto
      );
    }else{
      AlertIOS.alert(
        'Deseja realmente excluir?',
        'Esta notificação será excluída',
        $objeto
      );
    }
    
  }
  renderFooter () {
      return this.state.footer_loading ? <View style={{width:"100%", height:40, justifyContent: "center", alignItems:"center"}}>
        <Text style={{fontFamily:"Montserrat-Bold", fontSize:12}}>Carregando...</Text>
      </View> : <View style={{width:"100%", height:40}}/>
  }
  clickPerfil = (perfil_id) => {
    if(perfil_id == this.state.sessao.usuario_perfil_id.id){
      Actions.perfil();
    }else{
      Actions.perfil_amigo({usuario_perfil_id_search:perfil_id});
    }
    
  }
  render() {
      var _this = this;
       return (
          <Body
            getUrl={(url)=>{
              _this.setState({url:url});
            }}
            onErro={(erro)=>{
              _this.setState({body_erro:erro});
            }}
            btnOk={
              <Btn estilo={1} style={{width:100, marginTop:10}} texto={"OK"} onPress={()=>{
                _this.setState({body_erro:""});
              }}/>
            }
            onToken={(token)=>{
                this.setState({token_firebase:token});
            }}
            carregando={this.state.body_loading}
            erro={this.state.body_erro}
            ignoreKeyboard
            conteudo={
            <View style={{ flex: 1, backgroundColor:"#fff", justifyContent:"flex-start", alignItems:"center" }}>
                <Header right={<View></View>} onPressCamera={()=> _this.menuLeft()} onPressDirect={()=>{alert("Em desenvolvimento");}}/>
                <FlatList
                  data={this.state.notificacao}
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                  onEndReached={this._addMais}
                  style={{flex:1, marginBottom:50, width:"100%"}}
                  ListFooterComponent={this.renderFooter.bind(this)}
                  keyExtractor={this._keyExtractor}
                  ItemSeparatorComponent={this.renderSeparator}
                  renderItem={({item,index}) => {
                    if(item.notificacao.type == "amizade"){
                      return(
                        <View style={styles.container_list}>
                          <View style={styles.container_list_fundo}>
                            <View style={[styles.container_list_fundo_inners]} >
                              <TouchableOpacity onPress={()=>{this.clickExcluirNotificacao(item, index, true)}} style={{alignItems:"center"}}>
                                <Icon name="clear" size={15} color={"#fff"}/>
                                <Text style={styles.container_list_fundo_inners_text}>NÃO ACEITAR</Text>
                              </TouchableOpacity>
                            </View>
                          </View>
                          <Interactable.View
                            horizontalOnly={true}
                            animatedValueX={{x:-170}}
                            snapPoints={[{x: 0}, {x: -170}]}>
                            <ListItem
                              roundAvatar
                              onPress={()=>{this.clickPerfil(item.notificacao.perfil_id_1.id)}}
                              title={item.notificacao.perfil_id_1.nome}
                              subtitle={item.notificacao.texto}
                              avatar={{ uri: this.state.url+'usuarioavatar/avatar/'+item.notificacao.perfil_id_1.avatar_id+'/100/100' }}
                              containerStyle={{ borderBottomWidth: 0, flex:1, backgroundColor:"#fff" }}
                              rightIcon={
                                <Btn onPress={()=>{this.clickAceitar(item,index)}} style={{padding:10}} estilo={1} texto={"Aceitar"}/>
                              }
                              
                            />
                          </Interactable.View>
                        </View>
                      );
                    }else if(item.notificacao.type == "comentario" || item.notificacao.type == "like" || item.notificacao.type == "amizade_aceita"){
                      return(
                        <View style={styles.container_list}>
                          <View style={styles.container_list_fundo}>
                            <View style={[styles.container_list_fundo_inners]} >
                              <TouchableOpacity onPress={()=>{this.clickExcluirNotificacao(item, index)}} style={{alignItems:"center"}}>
                                <Icon name="clear" size={15} color={"#fff"}/>
                                <Text style={styles.container_list_fundo_inners_text}>EXCLUIR</Text>
                              </TouchableOpacity>
                            </View>
                          </View>
                          <Interactable.View
                            horizontalOnly={true}
                            animatedValueX={{x:-170}}
                            snapPoints={[{x: 0}, {x: -170}]}>
                            <ListItem
                              roundAvatar
                              onPress={()=>{this.clickPerfil(item.notificacao.perfil_id_1.id)}}
                              title={item.notificacao.perfil_id_1.nome}
                              subtitle={item.notificacao.texto}
                              avatar={{ uri: this.state.url+'usuarioavatar/avatar/'+item.notificacao.perfil_id_1.avatar_id+'/100/100' }}
                              containerStyle={{ borderBottomWidth: 0, flex:1, backgroundColor:"#fff" }}
                              rightIcon={
                                <Icon name="reply" size={20} color={Template.cor_2}/>
                              }
                              
                            />
                          </Interactable.View>
                        </View>
                      );
                    }else{
                      return(<View/>);
                    }
                  }}
                />
            </View>
            }/>
       );
   }
}

const mapStateToPropos = (state) => {
  return { data: state.reducer.dados };
}

const mapDispatchToPropos = (dispatch) => {
  return bindActionCreators({
    usuarionotificacao_get,
    usuarioamigo_aceitar,
    usuarionotificacao_excluir
  },dispatch);
}

export default connect(mapStateToPropos, mapDispatchToPropos)(Postar);
