/**
 * Created by ggoma on 12/17/16.
 */
const profile = [
    {
        source: require('../images/bob.png'),
        name: 'Bob the Builder',
        online: false,
    },
    {
        source: require('../images/cookiemonster.jpeg'),
        name: 'Cookie Monster',
        online: true,
    },
    {
        source: require('../images/elmo.jpg'),
        name: 'Elmo',
        online: false,
    }
];

const images = {
    '1': require('../images/1.jpg'),
    '2': require('../images/2.jpg'),
    '3': require('../images/3.jpg'),
    '4': require('../images/4.jpg'),
    '5': require('../images/5.jpg')
};

export function randomProfile() {
    var random = Math.floor((Math.random() * profile.length));

    return profile[random];
}

export function getImage(index) {
    return images[index];
}
