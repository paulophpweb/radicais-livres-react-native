import React, { Component } from 'react';
import { AsyncStorage, View } from 'react-native';
import { Router, Scene, ActionConst, Schema, Route, Reducer, Modal, Actions, Switch } from 'react-native-router-flux';
import Login from './containers/login';
import Home from './containers/home';
import Pesquisa from './containers/pesquisa';
import PostarHome from './containers/postar';
import Notificacao from './containers/notificacao';
import Perfil from './containers/perfil';
import Erro from './components/error';
import Loading from './components/loading';
import Camera from './components/camera';
import Postar from './components/postar';
import ModalComentarios from './components/comentar';
import TabIcon from './components/tabicon';
import Placeholder from './components/placeholder';
import Crop from './components/crop';
import Completar from './components/completar';
import NavigationDrawer from './components/drawer';
import Picker from './components/picker';
import Atualizar from './containers/perfil/partial/atualizar';
import Uteis from './lib/Uteis';

//navBar={HeaderSearch}
class RouterComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      io: "",
      sessao:""
    }
    Uteis.permissao();
  }
  async componentWillMount(){
    var _this = this;
    Uteis.getSocketId(function(io,socketid){
      _this.setState({io:io});
    });
  }
  render() {
    if(this.state.io){
      return (
        <Router sceneStyle={{ paddingTop: 0, backgroundColor:"transparent" }}>
          <Scene key="root">
            <Scene key="placeholder" component={Placeholder} hideNavBar={true}/>
          </Scene>
          <Scene key="auth">
            <Scene key="login" component={Login} hideNavBar={true}/>
          </Scene>
          <Scene key="drawer" hideNavBar={true} component={NavigationDrawer} open={false} >
            <Scene key="main">
              <Scene key="roottab" tabs={true} pressOpacity={1}>
                <Scene key="roottab_1" hideNavBar={true} icon={TabIcon} icone_name="iconMenu">
                  <Scene key="home" io={this.state.io} component={Home}/>
                </Scene>
                <Scene key="roottab_2" hideNavBar={true} icon={TabIcon} icone_name="iconSearch">
                  <Scene key="search" io={this.state.io} component={Pesquisa}/>
                </Scene>
                <Scene key="roottab_3" hideNavBar={true} icon={TabIcon} onPress={()=> { Actions.postar_home(); }} icone_name="iconCamera"/>
                <Scene key="roottab_4" hideNavBar={true} icon={TabIcon} icone_name="iconNotificacao" io={this.state.io}>
                  <Scene key="notificacao" io={this.state.io} component={Notificacao}/>
                </Scene>
                <Scene key="perfil" hideNavBar={true} icon={TabIcon} icone_name="iconPerfil">
                  <Scene key="perfil_1" io={this.state.io} component={Perfil}/>
                  <Scene key="atualizar" io={this.state.io} component={Atualizar} hideNavBar={true} direction="vertical" applyAnimation={()=> {  }} panHandlers={null}/>
                </Scene>
              </Scene>
              <Scene hideNavBar={true} key="perfil_amigo" io={this.state.io} component={Perfil}/>
            </Scene>
          </Scene>
          <Scene key="erro" io={this.state.io} component={Erro} hideNavBar={true} direction="vertical" applyAnimation={()=> {  }}/>
          <Scene key="loading" io={this.state.io} component={Loading} hideNavBar={true} direction="vertical"/>
          <Scene key="camera" io={this.state.io} component={Camera} hideNavBar={true} direction="vertical" panHandlers={null}/>
          <Scene key="postar_home" io={this.state.io} component={PostarHome} hideNavBar={true} direction="vertical" panHandlers={null}/>
          <Scene key="postar" io={this.state.io} component={Postar} hideNavBar={true} direction="vertical" panHandlers={null}/>
          <Scene key="comentar" io={this.state.io} component={ModalComentarios} hideNavBar={true} direction="vertical" applyAnimation={()=> {  }} panHandlers={null}/>
          <Scene key="crop" io={this.state.io} component={Crop} hideNavBar={true} direction="vertical" panHandlers={null}/>
          <Scene key="completar" io={this.state.io} component={Completar} hideNavBar={true} direction="vertical" panHandlers={null}/>
          <Scene key="picker" io={this.state.io} component={Picker} hideNavBar={true} direction="vertical" panHandlers={null}/>
        </Router>
      );
    }else{
      return(<Loading/>);
    }
  }
}

export default RouterComponent;
