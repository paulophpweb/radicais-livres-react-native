import React, { Component } from 'react'
import { View, Image, Text } from 'react-native'
import { Actions } from 'react-native-router-flux';
import { AsyncStorage } from 'react-native';
var {FBLoginManager} = require('react-native-facebook-login');
import Modal from 'react-native-modal';
import { List, ListItem, SearchBar, CheckBox, Slider, ButtonGroup } from "react-native-elements";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { usuariorelacionamento_get_relacionamentos } from '../../actions';
import Uteis from '../../lib/Uteis';
import Loading from '../../components/loading';
import Body from '../../components/ajax_body';
import Btn from '../../components/btn';
import Input from '../../components/inputs';
import Template from '../../constants/cores';


class Placeholder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url:"",
      body_erro:"",
      carregando:false,
      latitude_usuario:"",
      longitude_usuario:"",
      relacionamentos:[]
    }

  }
  componentWillMount(){
    var _this = this;
    AsyncStorage.getItem('sessao')
     .then((data) => {
       if(data) {
        Sessao = JSON.parse(data);
        if(
          Sessao.usuario_perfil_id.lider 
          || Sessao.usuario_perfil_id.discipulador
          || Sessao.usuario_perfil_id.membro
          || Sessao.usuario_perfil_id.ministro
          ){
          Actions.drawer({ type: "reset" });
        }else{
          Actions.completar();
        }
       }
       else {
         Actions.auth({ type: "reset" });
       }
     })
     .catch((err) => Actions.auth({ type: "reset" }));
  }
  render() {
    var _this = this;
    return(
      <View/>
    );
  }
}

const mapStateToPropos = (state) => {
  return {};
}

const mapDispatchToPropos = (dispatch) => {
  return bindActionCreators({},dispatch);
}

export default connect(mapStateToPropos, mapDispatchToPropos)(Placeholder);