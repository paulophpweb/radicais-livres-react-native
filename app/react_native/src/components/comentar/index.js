import React, { Component } from 'react';
import { View, TouchableOpacity, Text, AsyncStorage, Alert, ScrollView, Image, AlertIOS, Platform } from 'react-native';
import Modal from 'react-native-modal';
import Video from 'react-native-video';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconIonic from 'react-native-vector-icons/Ionicons';
import { post_getpost, postcomentario_comentar, postcomentario_excluir, postcomentario_get_ultimos_comentarios  } from '../../actions';
import Uteis from '../../lib/Uteis';
import Btn from '../../components/btn';
import Body from '../../components/ajax_body';
import Input from '../../components/inputs';
import Template from '../../constants/cores';
import styles from './style';

class Postar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form_data_post_texto:"",
      btn_postar:"COMENTAR",
      latitude_usuario:"",
      longitude_usuario:"",
      body_erro:"",
      carregando:true,
      url:"",
      post:[],
      sessao:null,
      post_id:props.id,
      token_firebase:""
    }

  }
  async componentDidMount(){
    var Sessao = await AsyncStorage.getItem("sessao");
    var _this = this;
    if(Sessao){
      Sessao = JSON.parse(Sessao);
      this.setState({"sessao": Sessao});
      var body_ = new Body();
      body_._getMyLocation().then((position) => {
        position = position.coords;
        _this.setState({
          latitude_usuario: position.latitude,
          longitude_usuario: position.longitude
        });
      });
      this._getPost();
      this.sockets();

    }
  }

  _getPost = () => {
    var _this = this;
    _this.props.post_getpost({
      post_id:_this.state.post_id,
      ignorelimit:true,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario,
      token_firebase:_this.state.token_firebase,
    }).then(function (response) {
        if(!response.error){
          if(response.payload.status == "sucesso"){
            if(response.payload.data){
              _this.setState({post:response.payload.data, carregando:false});
            }
          }else{
            _this.setState({carregando:false});
          }
        }else{
          _this.setState({carregando:false});
        }
    });
  }
  sockets = () => {
    var _this = this;
    this.props.io.socket.on('postcomentario_excluir', function (broadcastedData){
      if(broadcastedData.post_id){
        _this.props.postcomentario_get_ultimos_comentarios({
          post_id:_this.state.post_id,
          token_firebase:_this.state.token_firebase,
          ignorelimit:true,
          latitude_usuario:_this.state.latitude_usuario,
          longitude_usuario:_this.state.longitude_usuario,
        }).then(function (response) {
            if(!response.error){
              if(response.payload.status == "sucesso"){
                if(response.payload.data){
                  _this.state.post.ultimos_comentarios = response.payload.data;
                  _this.setState({post:_this.state.post});
                  
                }
              }
            }
        });
      }

    });

    this.props.io.socket.on('postcomentario_comentar', function (broadcastedData){
      if(broadcastedData.post_id){
        // add os comentarios
        if(_this.state.post.ultimos_comentarios){
          _this.state.post.ultimos_comentarios.unshift(broadcastedData);
        }else{
          _this.state.post.ultimos_comentarios = [];
          _this.state.post.ultimos_comentarios.unshift(broadcastedData);
        }
        _this.setState({post:_this.state.post});
      }

    });
  }
  /**
    Metodo Cadastrar
  */
  clickPostar = () => {
    var _this = this;
    var $erro = '';
    if(!_this.state.form_data_post_texto){
      $erro += 'Por favor, abençoe este post com o seu comentário';
    }
    if($erro){
      Uteis.alerta($erro,"error");
    }else{
      if(this.state.btn_postar != "Aguarde..."){
        _this.setState({btn_postar:"Aguarde..."});
        _this.props.postcomentario_comentar({
          texto:_this.state.form_data_post_texto,
          post_id:_this.state.post.id,
          latitude_usuario:_this.state.latitude_usuario,
          longitude_usuario:_this.state.longitude_usuario,
          token_firebase:_this.state.token_firebase,
        }).then(function (response) {
          if(!response.error){
            if(response.payload.status == "erro"){
              Uteis.alerta(response.payload.msg,"error");
              _this.setState({btn_postar:"COMENTAR"});
            }else if(response.payload.status == "sucesso"){
              _this.setState({btn_postar:"COMENTAR"});
              Uteis.alerta(response.payload.msg,"success",4000);
            }
          }else{
            _this.setState({body_erro:"Ops, algo de errado aconteceu", btn_postar:"COMENTAR"});
          }
          
        });
      }
    }
  }
  clickExcluirComentatio = (comentario_id,post_id) => {
    var _this = this;
    $objeto = [
      {text: 'Sim', onPress: () => {
        _this.props.postcomentario_excluir({
          comentario_id:comentario_id,
          post_id:post_id,
          token_firebase:_this.state.token_firebase,
          latitude_usuario:_this.state.latitude_usuario,
          longitude_usuario:_this.state.longitude_usuario,
        }).then(function (response) {
          if(!response.error){
            if(response.payload.status == "erro"){
              _this.setState({body_erro:response.payload.msg});
            }
          }else{
            _this.setState({body_erro:"Ops, algo de errado aconteceu"});
          }

        });
      }},
      {text: 'Não', onPress: () => {}, style: 'cancel'},
      {text: 'Cancelar', onPress: () => {}, style: 'cancel'},
    ];
    if(Platform.OS === 'android'){
      Alert.alert(
        'Deseja realmente excluir?',
        'Este comentário sera excluído',
        $objeto
      );
    }else{
      AlertIOS.alert(
        'Deseja realmente excluir?',
        'Este comentário sera excluído',
        $objeto
      );
    }
    
  }
  render() {
      var _this = this;
       return (
         <Modal 
            isVisible={true}
            backdropColor={Template.cor_2}
            backdropOpacity={1}
            animationIn={'zoomInDown'}
            animationOut={'zoomOutUp'}
            animationInTiming={1000}
            animationOutTiming={1000}
            backdropTransitionInTiming={1000}
            backdropTransitionOutTiming={1000}
          >
          <Body
            logo={
              <Image source={require('../../images/mao.png')} style={{height:40}} resizeMode={Image.resizeMode.contain}/>
            }
            getUrl={(url)=>{
              _this.setState({url:url});
            }}
            onErro={(erro)=>{
              _this.setState({body_erro:erro});
            }}
            btnOk={
              <Btn estilo={1} style={{width:100, marginTop:10}} texto={"OK"} onPress={()=>{
                _this.setState({body_erro:""});
              }}/>
            }
            onToken={(token)=>{
              _this.setState({token_firebase:token});
            }}
            carregando={_this.state.carregando}
            erro={this.state.body_erro}
            conteudo={
            <View style={{ flex: 1, backgroundColor:"#fff", justifyContent:"flex-end", alignItems:"center" }}>
               {_this.state.post.ultimos_comentarios && _this.state.post.ultimos_comentarios.length ?
               <View style={{position:"absolute", top:50, left:10, zIndex:100, bottom:120 ,backgroundColor:"rgba(255,255,255,0.8)", right:10, borderRadius:3,}}>
                 <ScrollView>
                    <View style={{marginBottom:10}}>
                      {_this.state.post.ultimos_comentarios.map((person, index) => (
                      <View key={index} style={{padding:10, flexDirection:"row", flexWrap:"wrap", alignItems:"center"}}>
                        <Text style={{fontFamily:'Montserrat-Bold',fontSize:12, color:Template.cor_1, marginRight:5}}>
                          {_this.state.post.ultimos_comentarios[index].usuario_perfil_id.nome}:
                        </Text>
                        {_this.state.post.ultimos_comentarios[index].usuario_perfil_id.id != _this.state.sessao.usuario_perfil_id.id ?
                        <Text style={{fontFamily:'Montserrat-Regular',fontSize:12, color:Template.cor_2}}>
                          {_this.state.post.ultimos_comentarios[index].texto}
                        </Text>
                        : 
                         <TouchableOpacity onPress={()=>{_this.clickExcluirComentatio(_this.state.post.ultimos_comentarios[index].id,_this.state.post.id)}}>
                          <Text style={{fontFamily:'Montserrat-Regular',fontSize:12, color:Template.cor_2}}>
                            {_this.state.post.ultimos_comentarios[index].texto}
                          </Text>
                         </TouchableOpacity>
                        }
                      </View>
                      ))}
                    </View>
                  </ScrollView>
               </View>
               : null }
                {/*<View style={{flex:1, flexDirection:"row", alignItems:"center", position:"absolute", left:50, top:10, zIndex:100, backgroundColor:"#fff", width:"100%"}}>
                  <View style={{position:"absolute", left:15, top:-5, width:14, height:14, backgroundColor:Template.cor_1, zIndex:10, borderRadius:7, justifyContent:"center", alignItems:"center"}}>
                    <Text style={{backgroundColor:"transparent", fontSize:8, color:"#fff", fontFamily:'Montserrat-Regular'}}>{_this.state.post.total_likes}</Text>
                  </View>
                  <View style={{backgroundColor:"transparent"}}>
                    <IconIonic name="ios-hand-outline" size={30} color={Template.cor_2} />
                  </View>
                  {_this.state.post.ultimos_likes ?
                  <View style={{position:"absolute", left:33, top:-5, width:220, height:45, justifyContent:"flex-start", alignItems:"center", flexDirection:"row"}}>
                    {_this.state.post.ultimos_likes.map((person, index) => (
                      <View key={index}>
                      {_this.state.post.ultimos_likes[index].usuario_perfil_id ?
                      <ResponsiveImage initWidth="20" initHeight="20" style={{width:20, height:20, borderRadius:10}} source={{uri:_this.state.url+'usuarioavatar/avatar/'+_this.state.post.ultimos_likes[index].usuario_perfil_id.avatar_id+'/40/40'}}/>
                      : null}
                      </View>
                    ))}
                  </View>
                  : null}
                </View>*/}
                {_this.state.post.imagem ?
                <Image style={{position:"absolute", left:0, top:0, right:0, bottom:0, zIndex:-1}} source={{uri:_this.state.url+'postimagem/imagem/'+_this.state.post.imagem.id+'/800/800'}} resizeMode={Image.resizeMode.cover} />
                : null }
                {_this.state.post.video ?
                <Video
                  repeat
                  volume={0} 
                  resizeMode='cover'
                  source={{uri:_this.state.url+"postvideo/video/"+_this.state.post.id+"/"+_this.state.post.video.name}} 
                  style={{position:"absolute", left:0, top:0, right:0, bottom:0}}
                />
                : null }
                <View style={{marginTop:10, width:280}}>
                    <Input
                      ref="1"
                      refInput="1"
                      texto={"Deixe o seu comentário"}
                      multiline={true}
                      onChangeText={value => _this.setState({form_data_post_texto: value})}
                      blurOnSubmit={false}
                      value={_this.state.form_data_post_texto}
                      style={{width:"100%"}}
                      style_input={{backgroundColor:"rgba(255,255,255,0.7)", height:50}}
                    />
                  </View>
                  <View style={{marginTop:10, width:"100%", alignItems:"center" ,marginBottom:10}}>
                    <Btn onPress={_this.clickPostar} style={{width:220}} estilo={1} texto={_this.state.btn_postar}/>
                  </View>
                  <TouchableOpacity onPress={()=>{Actions.pop()}} style={{backgroundColor:"transparent", marginRight:10, justifyContent:"center", position:"absolute", top:10, left:10}}>
                    <Icon name="arrow-back" size={30} color={Template.cor_1} />
                  </TouchableOpacity>
            </View>
            }/>
          </Modal>
       );
   }
}

const mapStateToPropos = (state) => {
  return { data: state.reducer.dados };
}

const mapDispatchToPropos = (dispatch) => {
  return bindActionCreators({
    post_getpost,
    postcomentario_comentar,
    postcomentario_excluir,
    postcomentario_get_ultimos_comentarios
  },dispatch);
}

export default connect(mapStateToPropos, mapDispatchToPropos)(Postar);
