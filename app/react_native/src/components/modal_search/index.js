import React, { Component } from 'react';
import { View, Text, ActivityIndicator, Modal, Image, FlatList, TouchableOpacity } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Actions, ActionConst } from 'react-native-router-flux';
import styles from './style';
import * as Animatable from 'react-native-animatable';
import { get_igreja } from '../../actions';
import Input from '../../components/inputs';
import {
  _rosa,
  _verde
} from '../../constants/cores';

class Modal_search extends Component {
  _keyExtractor = (item, index) => item.igreja_id;

  constructor(props) {
    super(props);
    var array = [];
    this.state = {
      igrejas:''
    };
  }

  componentDidMount(){
    _this = this;
    // chama o action creator para pegar os dados da lista
    this.props.get_igreja().then(function (response) {
      _this.setState({igrejas: response.payload.data.data});

    })
    .catch(function (error) {
      Actions.erro({data:error});
    });
  }

  componentWillUnmount() {

  }

  _onPressItem = (objeto) => {
    Actions.pop()
       setTimeout(()=>{
       Actions.refresh({key:'igreja_cadastro', cadastro_igreja_id: objeto.id, nome_igreja: objeto.nome});
    })


  };

  render() {
      var _this = this;
       return (
           <View style={styles.container}>
             <Input
               texto={"Digite o nome da igreja"}
               style={styles.input_pesquisa}
               onFocus={this.clickModalSearch}
               onChangeText={value => this.setState({cadastro_igreja_id: value})}
             />
             {this.state.igrejas ?
             <View style={{flex:1, width:'100%', paddingLeft:3, paddingRight:3}}>
               <FlatList
                 ref={"flatlist"}
                 data={this.state.igrejas}
                 keyExtractor={this._keyExtractor}
                 numColumns={1}
                 style={{flex:1, width:'100%',backgroundColor:'#fff',}}
                 renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
                 renderItem={({item, index}) => {
                   return (
                     <TouchableOpacity id={item.igreja_id} onPress={() => {
                       _this._onPressItem({id:item.igreja_id,nome:item.nome});
                     }}>
                       <View style={styles.container_list}>
                          <Image source={require('../../images/appcelerator/icon-mapa-igreja.png')} style={styles.photo_list} resizeMode={Image.resizeMode.contain}/>
                          <Text style={styles.text_list}>
                            {item.nome}
                          </Text>
                        </View>
                      </TouchableOpacity>
                   );
                 }}
               />
             </View>
             :
             <View style={{flex:1, width:'100%', paddingLeft:3, paddingRight:3}}>
              <View style={{backgroundColor:'#fff', flex:1, width:'100%'}}></View>
             </View>
             }
           </View>
       );
   }
}

const mapStateToPropos = (state) => {
  return { igrejas: state.posts.dados };
}

const mapDispatchToPropos = (dispatch) => {
  return bindActionCreators({
    get_igreja
  },dispatch);
}

export default connect(mapStateToPropos, mapDispatchToPropos)(Modal_search);
