const React = require('react-native');

const { StyleSheet, Dimensions, Platform} = React;
const deviceAltura = Dimensions.get('window').height;

export default {
    container: {
        backgroundColor:"rgba(0,0,0,0.8)",
        justifyContent: 'flex-start',
        alignItems: 'center',
        flex:1,
        paddingTop:(Platform.OS === 'ios') ? 20 : 0,
    },
    input_pesquisa: {
        width:'100%',
        backgroundColor:"#fff"
    },
    separator: {
      flex: 1,
      height: 1,
      backgroundColor: '#8E8E8E',
    },
    container_list: {
      flex: 1,
      padding: 12,
      flexDirection: 'row',
      alignItems: 'center',
      marginRight:20
    },
    text_list: {
      marginLeft: 12,
      fontSize: 16,
      marginRight:12
    },
    photo_list: {
      height: 40,
      width: 40,
      borderRadius: 20,
    },
};
