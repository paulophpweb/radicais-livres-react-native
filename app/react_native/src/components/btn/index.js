import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import styles from './style';
import {
  _rosa,
  _verde
} from '../../constants/cores';

export default class Inputs extends Component {
  render() {
     if(this.props.estilo == 1){
       return (
         <TouchableOpacity onPress={this.props.onPress}>
           <View style={[styles.btn,this.props.style]}>
             <Text style={styles.text_btn}>
               {this.props.texto}
             </Text>
           </View>
         </TouchableOpacity>
       );
     }else if(this.props.estilo == 2){
       return (
         <TouchableOpacity onPress={this.props.onPress}>
           <View style={[styles.btn_sem_fundo,this.props.style]}>
             <Text style={styles.text_btn}>
               {this.props.texto}
             </Text>
           </View>
         </TouchableOpacity>
       );
     }
   }
}
