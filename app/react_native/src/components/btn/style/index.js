const React = require('react-native');

const { StyleSheet, Dimensions } = React;
import Template from '../../../constants/cores';

export default {
  btn: {
    height:30,
    width:'100%',
    borderRadius:3,
    alignItems:'center',
    backgroundColor:Template.cor_1,
    justifyContent:"center",
  },
  btn_sem_fundo: {
    height:30,
    width:'100%',
    borderRadius:3,
    alignItems:'center',
    backgroundColor:Template.cor_2,
    justifyContent:"center",
  },
  text_btn: {
    textAlign:'center',
    color:'#fff',
    backgroundColor:"transparent"
  },
};
