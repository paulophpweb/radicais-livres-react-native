const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;
var {
  height: deviceHeight,
  width: deviceWidth
} = Dimensions.get("window");

export default {
  container: {
      backgroundColor:"rgba(0,0,0,0.5)",
      flex:1

  },
  container_int: {
    width:"100%",
    height:"100%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor:"transparent",
    flex:0.2
  },
  btnBottom: {
    justifyContent: 'center',
    alignItems: 'center',
    width:"100%"
  },
  imgIcon_:{
    width:40,
    height:40
  },
  container_botoes:{
    width:"100%",
    backgroundColor:'rgba(204,33,40,1)',
    flexDirection:"row",
    flex:1,
    justifyContent:"center",
    alignItems:"center"
  },
  botoes:{
    flex:.3,
    marginRight:10,
    justifyContent:"center",
    alignItems:"center"
  },
  botoes_last:{
    flex:.5,
    justifyContent:"center",
    alignItems:"center"
  },
  texto_botoes:{
    color:"#fff",
    fontFamily:"Montserrat-Bold"
  }

};
