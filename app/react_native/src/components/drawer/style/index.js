const React = require('react-native');

const { StyleSheet, Dimensions, Platform } = React;
import Template from '../../../constants/cores';

export default {
  drawer: { 
    backgroundColor:Template.cor_1,
    paddingTop:(Platform.OS === 'ios') ? 20 : 0,
    overflow:"hidden"
  },
  main: {backgroundColor:"transparent"},
  container:{
  	backgroundColor:"#FFFAFA", 
  	flex:1, 
  	alignItems:"center", 
  	justifyContent:"flex-start"
  },
  container_int:{
  	flex:1, 
  	width:"100%", 
  	alignItems:"center"
  },
  logo:{
  	height:40,
    marginBottom:10,
    marginTop:10
  },
  menu:{
  	borderColor:"#EEE5DE", 
  	borderBottomWidth:1, 
  	borderTopWidth:1, 
  	flexDirection:"row", 
  	height:40, 
  	width:"100%", 
  	justifyContent:"flex-start", 
  	alignItems:"center", 
  	paddingLeft:10, 
  	paddingRight:10
  },
  menu_texto:{
  	paddingLeft:5,
  	fontFamily:'Montserrat-Regular',
  	fontSize:12,
  	color:Template.cor_2,
  },
  scroll:{
  	width:"100%"
  },
  scroll_content:{
  	alignItems:"center"
  }
};
