import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Button, ScrollView, AsyncStorage, Image } from 'react-native';
import Drawer from 'react-native-drawer';
import {Actions, DefaultRenderer} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialIcons';
var {FBLoginManager} = require('react-native-facebook-login');
import { List, ListItem } from 'react-native-elements';
import styles from './style';
import Template from '../../constants/cores';

export default class NavigationDrawer extends Component {
  goTo = (chave) => {
    var _this = this;
    Actions[chave]();
    setTimeout(function(){
        Actions.refresh({key: _this.props.navigationState.key, open: false});
    },100);
  }
  logout = () => {
    FBLoginManager.logout(function(error, data){
      if (!error) {
        AsyncStorage.setItem('sessao', "");
        Actions.auth({ type: "reset" });
      }
    });
  }
  render () {
    const state = this.props.navigationState
    const children = state.children
    return (
          <Drawer
            ref='navigation'
            type="static"
            open={state.open}
            onOpen={() => Actions.refresh({key: state.key, open: true, side:"left"})}
            onClose={() => Actions.refresh({key: state.key, open: false, side:"left"})}
            content={ 
                <View style={styles.container}>
                    <View style={styles.container_int}>
                        <ScrollView
                            showsHorizontalScrollIndicator={true}
                            showsVerticalScrollIndicator={true}
                            style={styles.scroll}
                            contentContainerStyle={styles.scroll_content}
                            >
                            <Image source={require('../../images/mao.png')} style={styles.logo} resizeMode={Image.resizeMode.contain}/>
                            <TouchableOpacity onPress={() => this.goTo('perfil')} style={styles.menu}>
                                <Icon name='settings' color={Template.cor_1} size={20} />
                                <Text style={styles.menu_texto}>EDITAR PERFIL</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.menu} onPress={() => this.logout()}>
                                <Icon name='cancel' color={Template.cor_2} size={20} />
                                <Text style={styles.menu_texto}>SAIR</Text>
                            </TouchableOpacity>
                        </ScrollView>
                    </View>
                </View>
            }
            openDrawerOffset={0.3}
            panCloseMask={0.3}
            negotiatePan={true}
            styles={styles}
            apToClose={true}
            captureGestures
            tapToClose
            side={"left"}
            tweenHandler={Drawer.tweenPresets.parallax}
            tweenDuration={100}
          >
            <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate} />
          </Drawer> 
    )
  }
}
