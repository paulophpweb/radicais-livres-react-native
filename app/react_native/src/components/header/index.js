import React, { Component } from 'react';
import { View, TouchableOpacity, Text, TextInput, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconIonic from 'react-native-vector-icons/Ionicons';
import styles from './style';
import {
  _rosa,
  _verde
} from '../../constants/cores';

export default class Header_search extends Component {
  render() {
    return(
        <View style={styles.conteudo}>
          <View style={[styles.container]}>
              {!this.props.left ?
              <TouchableOpacity onPress={this.props.onPressCamera}>
                <IconIonic name="md-menu" size={35} color="#000" />
              </TouchableOpacity>
              : 
              this.props.left
              }
              {!this.props.center ?
              <TouchableOpacity onPress={this.props.onPressLogo}>
                  <Image style={{width:100, height:40}} resizeMode={Image.resizeMode.contain} source={require('../../images/mao.png')}/>
              </TouchableOpacity>
              : 
              this.props.center
              }
              {!this.props.right ?
              <TouchableOpacity style={styles.icon} onPress={this.props.onPressDirect}>
                  <IconIonic name="ios-paper-plane-outline" size={30} color="#000" />
              </TouchableOpacity>
              : 
              this.props.right
              }
          </View>
        </View>
    );
   }
}
