const React = require('react-native');

const { StyleSheet, Dimensions } = React;
import Template from '../../../constants/cores';

export default {
  conteudo:{
  	height:50, 
    justifyContent:"center", 
    alignItems:"center", 
    backgroundColor:"#fff",
    width:"100%",
    borderColor:Template.cor_3,
    borderBottomWidth:1,
    flexDirection:"row"
  },
  container:{
    flexDirection:"row",
    flex:1,
    justifyContent:"space-between", 
    alignItems:"center",
    paddingLeft:10,
    paddingRight:10
  }
};
