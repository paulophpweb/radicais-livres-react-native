import React, { Component } from 'react';
var ReactNative = require('react-native');
import {
  StyleSheet,
  Text,
  Image,
  View,
  TouchableHighlight,
  Platform,
} from 'react-native';

var {FBLoginManager} = require('react-native-facebook-login');
//
//905438167

class FBLoginMock extends Component {

  
  constructor(props) {
    super(props);
    this.state = {
      user: null,
    }
  }

  handleLogin = () => {
    var _this = this;
    FBLoginManager.loginWithPermissions(["email","user_friends","user_birthday","user_location","user_relationships","user_religion_politics"], function(error, data){
      if (!error) {
        _this.setState({ user : data});
        _this.props.onLogin && _this.props.onLogin(data);
      } else {
        console.log(error, data);
      }
    });
  }

  handleLogout = () => {
    var _this = this;
    FBLoginManager.logout(function(error, data){
      if (!error) {
        _this.setState({ user : null});
        _this.props.onLogout && _this.props.onLogout();
      } else {
        console.log(error, data);
      }
    });
  }

  onPress = () => {
    this.state.user
      ? this.handleLogout()
      : this.handleLogin();

    this.props.onPress && this.props.onPress();
  }

  componentDidMount(){
    
  }

  render(){
    var text = "Entrar com o Facebook";
    return (
      <View style={this.props.style}>
        <TouchableHighlight
          style={styles.container}
          onPress={this.onPress}
        >
          <View style={styles.FBLoginButton}>
            <Image style={styles.FBLogo} source={require('./images/FB-f-Logo__white_144.png')} />
            <Text style={[styles.FBLoginButtonText, styles.FBLoginButtonTextLoggedOut]}
              numberOfLines={1}>{text}</Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width:"100%"
  },
  FBLoginButton: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',

    height: 40,
    width: "100%",
    paddingLeft: 2,

    backgroundColor: 'rgb(66,93,174)',
    borderRadius: 3,
    borderWidth: 1,
    borderColor: 'rgb(66,93,174)',

    shadowColor: "#000000",
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 0
    },
  },
  FBLoginButtonText: {
    color: 'white',
    fontWeight: '600',
    fontFamily:"Montserrat-Regular", 
    fontSize: 14.2,
  },
  FBLoginButtonTextLoggedIn: {
    marginLeft: 5,
  },
  FBLoginButtonTextLoggedOut: {
    marginLeft: 18,
  },
  FBLogo: {
    position: 'absolute',
    height: 20,
    width: 20,

    left: 12,
  },
});

module.exports = FBLoginMock;
