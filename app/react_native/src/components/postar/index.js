import React, { Component } from 'react';
import { View, TouchableOpacity, Text, AsyncStorage, Image, Modal, Platform } from 'react-native';
import Video from 'react-native-video';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconIonic from 'react-native-vector-icons/Ionicons';
import { post_criar  } from '../../actions';
import Uteis from '../../lib/Uteis';
import Btn from '../../components/btn';
import Body from '../../components/ajax_body';
import Input from '../../components/inputs';
import Template from '../../constants/cores';
import styles from './style';

class Postar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form_data_post_image:props.form_data_post_image,
      form_data_post_video:props.form_data_post_video,
      form_data_post_texto:"",
      latitude_usuario:"",
      longitude_usuario:"",
      btn_postar:"ENVIAR",
      body_erro:"",
      url:"",
      sessao:null,
      token_firebase:""
    }
    console.warn(JSON.stringify(props.form_data_post_video));

  }
  async componentDidMount(){
    var _this = this;
    var Sessao = await AsyncStorage.getItem("sessao");
    if(Sessao){
      Sessao = JSON.parse(Sessao);
      this.setState({"sessao": Sessao});
      var body_ = new Body();
      body_._getMyLocation().then((position) => {
        position = position.coords;
        _this.setState({
          latitude_usuario: position.latitude,
          longitude_usuario: position.longitude
        });
      });
    }
  }
  /**
    Metodo Cadastrar
  */
  clickPostar = () => {
    var _this = this;
    var $erro = '';
    if(!_this.state.form_data_post_image && !_this.state.form_data_post_video){
      $erro += 'Por favor, tire uma foto ou faça uma filmagem deste momento especial';
    }else if(!_this.state.form_data_post_texto){
      $erro += 'Por favor, descreve este momento especial em algumas palavras';
    }
    if($erro){
      Uteis.alerta($erro,"error");
    }else{
      if(this.state.btn_postar != "Postando..."){
        _this.setState({btn_postar:"Postando..."});
        _this.props.post_criar({
          auth_token:_this.state.sessao.auth_token,
          usuario_id:_this.state.sessao.id,
          perfil_id:_this.state.sessao.usuario_perfil_id.id,
          texto:_this.state.form_data_post_texto,
          video:_this.state.form_data_post_video,
          imagem:_this.state.form_data_post_image,
          latitude_usuario:_this.state.latitude_usuario,
          longitude_usuario:_this.state.longitude_usuario,
          token_firebase:_this.state.token_firebase,
        },function(preloader){
          _this.setState({body_erro:"Carregando: "+(parseInt(preloader*100))+"%"});
        }).then(function (response) {
          if(!response.error){
            if(response.payload.status == "erro"){
              Uteis.alerta(response.payload.msg,"error");
              _this.setState({btn_postar:"ENVIAR - DEIXAR FLUIR"});
            }else if(response.payload.status == "sucesso"){
              Uteis.alerta(response.payload.msg,"success",4000,function(){
                  Actions.pop();
                  if(_this.props.onBack)
                    _this.props.onBack();
              });
              
            }
          }
          
        });
      }
    }
  }
  render() {
      var _this = this;
       return (
          <Body
            logo={
              <Image source={require('../../images/mao.png')} style={{height:40}} resizeMode={Image.resizeMode.contain}/>
            }
            getUrl={(url)=>{
              _this.setState({url:url});
            }}
            onErro={(erro)=>{
              _this.setState({body_erro:erro});
            }}
            btnOk={
              <Btn estilo={1} style={{width:100, marginTop:10}} texto={"CANCELAR"} onPress={()=>{
                Actions.pop();
              }}/>
            }
            onToken={(token)=>{
              _this.setState({token_firebase:token});
            }}
            carregando={false}
            erro={this.state.body_erro}
            conteudo={
            <View style={{ flex: 1, backgroundColor:"#fff", justifyContent:"flex-end", alignItems:"center" }}>
                {_this.state.form_data_post_image ?
                <Image style={{position:"absolute", left:0, top:0, right:0, bottom:0}} source={_this.state.form_data_post_image} resizeMode={'cover'} />
                : null }
                {_this.state.form_data_post_video ?
                <Video
                  repeat
                  volume={0} 
                  resizeMode='cover'
                  source={_this.state.form_data_post_video} 
                  style={{position:"absolute", left:0, top:0, right:0, bottom:0}}
                />
                : null }
                <View style={{marginTop:10, width:280}}>
                    <Input
                      ref="1"
                      refInput="1"
                      texto={"Deixe a sua mensagem abençoada aqui"}
                      multiline={true}
                      onChangeText={value => _this.setState({form_data_post_texto: value})}
                      blurOnSubmit={false}
                      value={_this.state.form_data_post_texto}
                      style={{width:"100%"}}
                      style_input={{backgroundColor:"rgba(255,255,255,0.7)", height:120}}
                    />
                  </View>
                  <View style={{marginTop:10, width:"100%", alignItems:"center" ,marginBottom:10}}>
                    <Btn onPress={_this.clickPostar} style={{width:220}} estilo={1} texto={_this.state.btn_postar}/>
                  </View>
                  <TouchableOpacity onPress={()=>{Actions.pop()}} style={{backgroundColor:"transparent", marginRight:10, justifyContent:"center", position:"absolute", top:10, left:10}}>
                    <Icon name="arrow-back" size={30} color={Template.cor_1} />
                  </TouchableOpacity>
            </View>
            }/>
       );
   }
}

const mapStateToPropos = (state) => {
  return { data: state.reducer.dados };
}

const mapDispatchToPropos = (dispatch) => {
  return bindActionCreators({
    post_criar
  },dispatch);
}

export default connect(mapStateToPropos, mapDispatchToPropos)(Postar);
