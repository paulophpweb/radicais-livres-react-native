const React = require('react-native');

const { StyleSheet, Dimensions } = React;
import Template from '../../../constants/cores';

export default {
  KeyboardAwareScrollView:{
    width:"100%"
  },
  KeyboardAwareScrollViewContentEnd:{
    width:"100%", 
    justifyContent:"flex-end", 
    alignItems:"center",
    flexGrow: 1
  },
};
