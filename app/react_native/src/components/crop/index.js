import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  Dimensions,
  View,
  Image,
  TouchableOpacity,
  Platform
} from 'react-native';

import ImageCrop2 from 'react-native-image-crop';
import * as mime from 'react-native-mime-types';
import Btn from '../../components/btn';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button: {
    padding: 10,
    position: 'absolute',
    top: 20,
    right: 10,
    backgroundColor: '#fff',
  },
  imageCropContainer: {
    flex: 1,
    backgroundColor: '#000',
  },
});

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      croppedImage: null,
      croppedImageRatio: 1,
    };
  }

  onButtonPress() {

    this.imageCrop.crop().then((uri) => {
      var mimetype = mime.lookup(uri.uri);
      var extensao = mime.extension(mimetype);
      name = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
      if(this.props.get_imagem){
        $objeto = {
            name:name+"."+extensao,
            type:mimetype,
            uri: Platform.OS == "android" ? uri.uri : uri.path
        };
        this.props.get_imagem($objeto);
      }
        
    });
  }

  render() {
    if (this.state.croppedImage) {
      return (
        <View style={{flex:1, justifyContent:"center", alignItems:"center"}}>
        <Image
          source={this.state.croppedImage}
          resizeMode="contain"
          style={
            {width:"100%", height:300}
          }
        />
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <View style={styles.imageCropContainer}>
          <ImageCrop2
            ref={(c) => { this.imageCrop = c; }}
            cropWidth={800}
            cropHeight={800}
            source={this.props.objeto.selected}
          />
        </View>
        <View style={{position:"absolute", bottom:10, width:"100%", justifyContent:"center", alignItems:"center"}}>
          <Btn onPress={()=>{this.onButtonPress()}} style={{width:220}} estilo={1} texto={"Usar Imagem"}/>
        </View>
      </View>
    );
  }
}