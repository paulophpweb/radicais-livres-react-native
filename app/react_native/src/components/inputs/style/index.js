const React = require('react-native');

const { StyleSheet, Dimensions } = React;
import {
  _cinza1,
  _cinza2,
} from '../../../constants/cores';

export default {
  inputContainer: {
    borderColor: _cinza2,
    borderBottomWidth: 2,
    borderTopWidth: 2,
    borderLeftWidth: 2,
    borderRightWidth: 2,
  },
  input: {
    color: _cinza2,
    height: 40,
    fontSize: 16,
    textAlign:'center',
    borderColor:_cinza2,
    paddingLeft:10,
    paddingRight:40
  },
  inputContainer_search: {
    borderColor: _cinza2,
    borderBottomWidth: 2,
    borderTopWidth: 2,
    borderLeftWidth: 2,
    borderRightWidth: 2,
    width:"80%"
  },
  input_search: {
    color: _cinza2,
    height: 40,
    fontSize: 16,
    textAlign:'center',
    paddingLeft:10,
    paddingRight:40,
    width:"80%"
  },
  input_multine: {
    color: _cinza2,
    height: 80,
    fontSize: 16,
    textAlign:'left',
    borderColor:_cinza2,
    paddingLeft:10,
    paddingRight:10
  },
  mao: {
    width:20,
    height:20,
    position:"absolute",
    right:10,
    top:10
  },
  search: {
    width:38,
    height:38,
    position:"absolute",
    right:0,
    top:4
  },
};
