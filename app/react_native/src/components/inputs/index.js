import React, { Component } from 'react';
import { View, TextInput, Image, TouchableOpacity } from 'react-native';
import styles from './style';
import * as Animatable from 'react-native-animatable';
import ModalPicker from '../../components/react-native-modal-picker';
import Iconz from 'react-native-vector-icons/Ionicons';
import Template from '../../constants/cores';
import Btn from '../../components/btn';
import {
  _cinza1,
  _cinza2
} from '../../constants/cores';

export default class Inputs extends Component {
  render() {
    if(!this.props.estilo){
      return (
       <TouchableOpacity activeOpacity={1} onPress={this.props.onPress} style={[styles.inputContainer,this.props.style]}>
         <TextInput
           style={[this.props.multiline ? styles.input_multine : styles.input,this.props.style_input]}
           autoCorrect={false}
           autoCapitalize="none"
           placeholder={this.props.texto}
           secureTextEntry={this.props.secureTextEntry}
           placeholderTextColor={_cinza2}
           underlineColorAndroid="transparent"
           onBlur={this.props.onBlur}
           onFocus={this.props.onFocus}
           onChangeText={this.props.onChangeText}
           value={this.props.value}
           keyboardType={this.props.keyboardType}
           editable={this.props.editable}
           keyboardAppearance={'dark'}
           returnKeyType={this.props.returnKeyType}
           onSubmitEditing={this.props.onSubmitEditing}
           blurOnSubmit={this.props.blurOnSubmit}
           ref={this.props.refInput}
           multiline={this.props.multiline}
         />
         {!this.props.multiline ?
         <Image source={require('../../images/mao.png')} style={styles.mao} resizeMode={Image.resizeMode.contain}/>
         : null}
       </TouchableOpacity>
     );
    }else if(this.props.estilo == "select"){
      return(
        <TouchableOpacity activeOpacity={1} onPress={this.props.onPress} style={[styles.inputContainer,this.props.style]}>
            <ModalPicker
                selectTextStyle={[styles.input_select_text,this.props.style_input]}
                    data={this.props.dados}
                    cancelText="Cancelar"
                    initValue={this.props.texto}
                    onChange={(option)=>{ this.props.onSelect(option) }} />
         </TouchableOpacity>
      );
     }else if(this.props.estilo == "search"){
        return (
        <View style={{width:"100%", flexDirection:"row"}}>
           <View activeOpacity={1} onPress={this.props.onPress} style={[styles.inputContainer_search,this.props.style]}>
             <TextInput
               style={[styles.input_search,this.props.style_input]}
               autoCorrect={false}
               autoCapitalize="none"
               placeholder={this.props.texto}
               secureTextEntry={this.props.secureTextEntry}
               placeholderTextColor={_cinza2}
               underlineColorAndroid="transparent"
               onBlur={this.props.onBlur}
               onFocus={this.props.onFocus}
               onChangeText={this.props.onChangeText}
               value={this.props.value}
               keyboardType={this.props.keyboardType}
               editable={this.props.editable}
               keyboardAppearance={'dark'}
               returnKeyType={this.props.returnKeyType}
               onSubmitEditing={this.props.onSubmitEditing}
               blurOnSubmit={this.props.blurOnSubmit}
               ref={this.props.refInput}
               multiline={this.props.multiline}
             />
              <TouchableOpacity onPress={this.props.clickScanner} style={styles.search}>
                <Iconz name="md-qr-scanner" size={30} color={Template.cor_2} />
              </TouchableOpacity>
           </View>
           <View style={{width:"20%", marginTop:2, paddingRight:6}}>
            <Btn estilo={1} style={{width:"100%", height:45}} texto={"OK"} onPress={this.props.pesquisar}/>
           </View>
         </View>
       );
     }
   }
}
