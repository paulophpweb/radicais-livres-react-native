import React, { Component } from 'react'
import { View, Image, Text, AsyncStorage } from 'react-native'
import { Actions } from 'react-native-router-flux';
var {FBLoginManager} = require('react-native-facebook-login');
import Modal from 'react-native-modal';
import { List, ListItem, SearchBar, CheckBox, Slider, ButtonGroup } from "react-native-elements";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { usuariorelacionamento_get_relacionamentos, usuario_atualizar } from '../../actions';
import Uteis from '../../lib/Uteis';
import Loading from '../../components/loading';
import Body from '../../components/ajax_body';
import Btn from '../../components/btn';
import Input from '../../components/inputs';
import Template from '../../constants/cores';


class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url:"",
      body_erro:"",
      carregando:false,
      latitude_usuario:"",
      longitude_usuario:"",
      relacionamentos:[],
      sessao:"",
      relacionamento_id:"",
      membro:false,
      lider:false,
      discipulador:false,
      pastor:false,
      token_firebase:"",
      btn_salvar:"SALVAR"
    }

  }
  async componentDidMount(){
    var _this = this;
    var Sessao = await AsyncStorage.getItem("sessao");
    if(Sessao){
      Sessao = JSON.parse(Sessao);
      this.setState({"sessao": Sessao});
      var body_ = new Body();
      body_._getMyLocation().then((position) => {
        position = position.coords;
        _this.setState({
          latitude_usuario: position.latitude,
          longitude_usuario: position.longitude,
          relacionamento_id:Sessao.usuario_perfil_id.relacionamento_id ? Sessao.usuario_perfil_id.relacionamento_id : null
        });
      });
      _this.getRelacionamentos();
    }
  }

  clickSalvar = () => {
     var _this = this;
     var $erro = '';
     if(!_this.state.relacionamento_id){
      $erro += 'Por favor, selecione o seu estado civil';
     }else if(!_this.state.membro && !_this.state.lider && !_this.state.discipulador && !_this.state.pastor){
      $erro += 'Por favor, marque uma das funções abaixo para as pessoas te acharem melhor.';
     }
     if($erro){
      Uteis.alerta($erro,"error");
     }else{
      if(this.state.btn_salvar != "Salvando..."){
         _this.setState({body_loading:true,btn_salvar:"Salvando..."});
        _this.props.usuario_atualizar({
          token_firebase:_this.state.token_firebase,
          latitude_usuario:_this.state.latitude_usuario,
          longitude_usuario:_this.state.longitude_usuario,
          relacionamento_id:_this.state.relacionamento_id,
          membro:_this.state.membro,
          lider:_this.state.lider,
          discipulador:_this.state.discipulador,
          pastor:_this.state.pastor
        }).then(function (response) {
          _this.setState({body_loading:false});
          if(!response.error){
            if(response.payload.status == "erro"){
              _this.setState({body_erro:response.payload.msg,btn_salvar:"SALVAR",body_loading:false});
            }else if(response.payload.status == "sucesso"){
              if(response.payload.data){
                AsyncStorage.setItem('sessao', JSON.stringify(response.payload.data)+"");
                Actions.drawer({ type: "reset" });
              }else{
                _this.setState({body_erro:response.payload.msg,btn_salvar:"SALVAR",body_loading:false});
              }
            }
          }else{
            _this.setState({body_erro:"Ops, algo de errado aconteceu",btn_salvar:"SALVAR",body_loading:false});
          }

        });
      }
    }
  }

  getRelacionamentos = () => {
     var _this = this;
     _this.setState({body_loading:true});
    _this.props.usuariorelacionamento_get_relacionamentos({
      token_firebase:_this.state.token_firebase,
      latitude_usuario:_this.state.latitude_usuario,
      longitude_usuario:_this.state.longitude_usuario
    }).then(function (response) {
      _this.setState({body_loading:false});
      if(!response.error){
        if(response.payload.status == "erro"){
          _this.setState({body_erro:response.payload.msg,usuarios:[]});
        }else if(response.payload.status == "sucesso"){
          if(response.payload.data.length){
            var newArray = [];
            for(var i in response.payload.data){
              newArray.push({
                key:response.payload.data[i].id,
                chave:response.payload.data[i].id,
                label:response.payload.data[i].nome,
                long_name:response.payload.data[i].nome
              });
            }
            _this.setState({relacionamentos:newArray});
          }else{
            _this.setState({relacionamentos:[],body_erro:response.payload.msg});
          }
        }
      }else{
        _this.setState({body_erro:"Ops, algo de errado aconteceu"});
      }

    });
  }
  clickCheckbox = (tipo) => {
    if(tipo == "membro"){
      if(this.state.membro){
        this.setState({membro:false});
      }else{
        this.setState({membro:true});
      }
    }else if(tipo == "lider"){
      if(this.state.lider){
        this.setState({lider:false});
      }else{
        this.setState({lider:true});
      }
    }else if(tipo == "discipulador"){
      if(this.state.discipulador){
        this.setState({discipulador:false});
      }else{
        this.setState({discipulador:true});
      }
    }else if(tipo == "pastor"){
      if(this.state.pastor){
        this.setState({pastor:false});
      }else{
        this.setState({pastor:true});
      }
    }
  }
  render() {
    var _this = this;
    return(
      <Body
        logo={
          <Image source={require('../../images/mao.png')} style={{height:40}} resizeMode={Image.resizeMode.contain}/>
        }
        getUrl={(url)=>{
          _this.setState({url:url});
        }}
        onErro={(erro)=>{
          _this.setState({body_erro:erro});
        }}
        btnOk={
          <Btn estilo={1} style={{width:100, marginTop:10}} texto={"OK"} onPress={()=>{
            _this.setState({body_erro:""});
          }}/>
        }
        onToken={(token)=>{
          _this.setState({token_firebase:token});
        }}
        carregando={this.state.carregando}
        erro={this.state.body_erro}
        conteudo={

              <View style={{ flex: 1, backgroundColor:"#fff", justifyContent:"center", alignItems:"center" }}>
                  <Text style={{fontFamily:"Montserrat-Regular", fontSize:16, textAlign:"center",color:Template.cor_2,marginBottom:10,padding:10}}>Para as pessoas te acharem mais fácil preencha os dados abaixo:</Text>
                    <View style={{marginTop:10, width:280}}>
                      <CheckBox
                        title='Membro'
                        checked={this.state.membro}
                        onPress={()=>{this.clickCheckbox("membro")}}
                      />
                      <CheckBox
                        title='Líder'
                        checked={this.state.lider}
                        onPress={()=>{this.clickCheckbox("lider")}}
                      />
                      <CheckBox
                        title='Discipulador'
                        checked={this.state.discipulador}
                        onPress={()=>{this.clickCheckbox("discipulador")}}
                      />
                      <CheckBox
                        title='Pastor(a)'
                        checked={this.state.pastor}
                        onPress={()=>{this.clickCheckbox("pastor")}}
                      />
                      {_this.state.sessao ?
                        <View>
                          {!_this.state.sessao.usuario_perfil_id.relacionamento_id ?
                            <Input
                              dados={this.state.relacionamentos}
                              estilo="select"
                              ref="1"
                              onSelect={(value) => this.setState({relacionamento_id: value.key})}
                              texto={"Selecione um estado civil"}
                              style={{marginLeft:10, marginRight:10, marginTop:10, marginBottom:10}}
                              style_input={{borderWidth:0}}
                            />
                          : null}
                        </View>
                      : null}
                    </View>
                    <View style={{marginTop:10, width:"100%", alignItems:"center"}}>
                      <Btn onPress={()=>{_this.clickSalvar();}} style={{width:220, marginTop:25}} estilo={1} texto={_this.state.btn_salvar}/>
                    </View>
              </View>
        }/>
    );
  }
}

const mapStateToPropos = (state) => {
  return { data: state.reducer.dados };
}

const mapDispatchToPropos = (dispatch) => {
  return bindActionCreators({
    usuariorelacionamento_get_relacionamentos,
    usuario_atualizar,
  },dispatch);
}

export default connect(mapStateToPropos, mapDispatchToPropos)(Index);