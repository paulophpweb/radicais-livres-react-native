import React, { Component } from 'react';
import { Text, View, AsyncStorage, Image } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconIonic from 'react-native-vector-icons/Ionicons';
import {Actions} from "react-native-router-flux";
import { 
  usuarionotificacao_get_total, 
} from '../../actions';
import Template from '../../constants/cores';
import Config from '../../constants/config';
import styles from './style';

class Tabicon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sessao:"",
      total_notificacao:"",
      resgatou:false
    }

  }
  async componentDidMount(){
    var Sessao = await AsyncStorage.getItem("sessao");
    if(Sessao){
      Sessao = JSON.parse(Sessao);
      this.setState({"sessao": Sessao});
      this._getTotalNotificacao();
      if(this.props.io)
        this.sockets();
    }
  }
  _getTotalNotificacao = (force=false) => {
    var _this = this;
    if(!this.state.resgatou || force){
      this.setState({resgatou:true});
      _this.props.usuarionotificacao_get_total({}).then(function (response) {
        if(!response.error){
          if(response.payload.status == "sucesso"){
            //console.warn("deu certo",response.payload.data);
            _this.setState({total_notificacao:response.payload.data});
          }
        }

      });
    }
  }

  sockets = () => {
    var _this = this;
    this.props.io.socket.on('usuarionotificacao_get', function (broadcastedData){
      if(broadcastedData.usuario_perfil_id){
         if(_this.state.sessao.usuario_perfil_id.id == broadcastedData.usuario_perfil_id){
          _this._getTotalNotificacao(true);
         }
       }
    });
    this.props.io.socket.on('push_notificacao', function (broadcastedData){
      if(broadcastedData.usuario_perfil_id){
         if(_this.state.sessao.usuario_perfil_id.id == broadcastedData.usuario_perfil_id){
          _this._getTotalNotificacao(true);
         }
       }
    });
  }
  render() {
     if(this.props.icone_name == "iconMenu"){
       $icone = "home";
       $iconeColor = "#000";
       if(this.props.selected){
         $iconeColor = Template.cor_1;
       }
     }else if(this.props.icone_name == "iconSearch"){
       $icone = "search";
       $iconeColor = "#000";
       if(this.props.selected){
         $iconeColor = Template.cor_1;
       }
     }else if(this.props.icone_name == "iconCamera"){
       $icone = "md-add-circle";
       $iconeColor = "#000";
       if(this.props.selected){
         $iconeColor = Template.cor_1;
       }
     }else if(this.props.icone_name == "iconNotificacao"){
       $icone = "favorite-border";
       $iconeColor = "#000";
       if(this.props.selected){
         $iconeColor = Template.cor_1;
       }
     }
     if(this.props.icone_name != "iconCamera"){
       return (
         <View style={[styles.container]}>
            {this.props.icone_name != "iconPerfil" && this.props.icone_name != "iconNotificacao" ?
            <Icon name={$icone} size={30} color={$iconeColor}/>
            : 
            null
            }
            {this.state.sessao.usuario_perfil_id && this.props.icone_name == "iconPerfil" ?
            <Image
            style={{width:30, height:30, borderRadius:15}}
            source={{ uri: Config.url+'usuarioavatar/avatar/'+(this.state.sessao.usuario_perfil_id.avatar_id)+'/100/100'}}/>
            : null }
            {this.props.icone_name == "iconNotificacao" ?
            <View style={{position:"relative"}}>
              {this.state.total_notificacao ?
              <View style={{position:"absolute", right:0, top:0, zIndex:2, width:20, height:20, backgroundColor:"red", justifyContent:"center", alignItems:"center", borderRadius:10}}>
                <Text style={{fontSize:9, backgroundColor:"transparent", color:"#fff", fontFamily:'Montserrat-Regular'}}>{this.state.total_notificacao >= 100 ? '+99' : this.state.total_notificacao}</Text>
              </View>
              : <View/> }
              <Icon name={$icone} size={30} color={$iconeColor}/>
            </View>
            : 
            null
            }
         </View>
       );
     }else{
        return (
         <View style={[styles.container]}>
            <IconIonic name={$icone} size={35} color={$iconeColor}/>
         </View>
       );
     }
   }
}

const mapStateToPropos = (state) => {
  return { data: state.reducer.dados };
}

const mapDispatchToPropos = (dispatch) => {
  return bindActionCreators({
    usuarionotificacao_get_total,
  },dispatch);
}

export default connect(mapStateToPropos, mapDispatchToPropos)(Tabicon);
