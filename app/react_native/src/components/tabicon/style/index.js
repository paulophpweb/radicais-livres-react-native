const React = require('react-native');

const { StyleSheet, Dimensions } = React;
import Template from '../../../constants/cores';
const screenWidth = Dimensions.get('window').width;

export default {
  container: {
    backgroundColor:"#fff",
    width:"100%",
    height:"100%",
    justifyContent:"center",
    alignItems:'center',
    borderTopWidth:1, 
    borderColor:"#ccc",
  },
};
