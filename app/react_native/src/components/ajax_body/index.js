import React, { Component } from 'react';
import { View, Text, ActivityIndicator, Platform, AsyncStorage } from 'react-native';
var MessageBarAlert = require('react-native-message-bar').MessageBar;
var MessageBarManager = require('react-native-message-bar').MessageBarManager;
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {checkPermission} from 'react-native-android-permissions';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import Image from 'react-native-fast-image';
import styles from './style';
import Fcm from '../../components/push_fcm';
import Uteis from '../../lib/Uteis';
import Config from '../../constants/config';


export default class Ajax_body extends Component { 
	watchID = null;
	constructor(props) {
	    super(props);
	}
	async componentDidMount(){
	  if(this.props.getUrl)
      	this.props.getUrl(Config.url);
      MessageBarManager.registerMessageBar(this.refs.alert);
    }
    componentWillUnmount() {
	    MessageBarManager.unregisterMessageBar();
	    navigator.geolocation.clearWatch(this.watchID);
	}
    _getMyLocation($onSucess, $onError){
    	var _this = this;
    	return new Promise(function (resolve, reject) {
	    	if (navigator.geolocation) {
		    	if(Platform.OS === 'ios'){
			        navigatorOptions = {
			            enableHighAccuracy: true,
			            timeout: 10000,
			            maximumAge: 0
			        }
					navigator.geolocation.getCurrentPosition(resolve,reject,navigatorOptions);
				}else{
					checkPermission("android.permission.ACCESS_FINE_LOCATION").then((result) => {
						LocationServicesDialogBox.checkLocationServicesIsEnabled({
						    message: "<h2>Gps desativado</h2>Precisamos do acesso ao seu gps para criar a ocorrência, deseja habilitar?",
						    ok: "Sim",
						    cancel: "Não",
						    enableHighAccuracy: true
						}).then(function(success) {
						    navigatorOptions = {
					            enableHighAccuracy: true,
					            timeout: 10000,
					            maximumAge: 0
					        }
							navigator.geolocation.getCurrentPosition(resolve,reject,navigatorOptions);
						}).catch((error) => {
							 reject(error);
						});
				        
				    }, (result) => {
						reject({message:"Erro de Gps, verifique se seu gps esta ativo ou se foi permitido no seu android"});
				    });
				}
			}else{
			   reject({message:"Erro de Gps, verifique se seu gps esta ativo"});
			}
		});
    }
    _trackingLocation(){
    	var _this = this;
    	// opcao de gps
	    navigatorOptions = {};
	    if (Platform.OS === 'android') {
	        navigatorOptions = {
	            enableHighAccuracy: false,
	            timeout: 10000,
	            maximumAge: 0
	        }
	    }
	    navigator.geolocation.getCurrentPosition(
	      (position) => {
	        _this.watchID = navigator.geolocation.watchPosition(
		      (position) => {
		        if(_this.props.trackingLocation)
		        	_this.props.trackingLocation(position.coords);
		      }
		    );
	      },
	      (error) => {
	      	if(_this.props.onErro)
	      		_this.props.onErro("Erro de Gps, verifique se seu gps esta ativo: "+error.message);
	      },
	      { navigatorOptions },
	    );
	    
    }
	_getLoading(){
		return (
			 <View style={{position:"absolute", left:0, right:0, top:0, bottom:0, justifyContent:"center", alignItems:"center", zIndex:10, backgroundColor:"rgba(255,255,255,0.4)"}}>
				 {this.props.logo ? this.props.logo : null}
				 <ActivityIndicator
					style={{marginTop:10}}
					size="large"
					color="#000"
				  />	
			 </View>
		 );
	}
	_getErro(props){
		return (
			 <View style={styles.container_erro}>
			 	{this.props.logo ? this.props.logo : null}
				<Text style={styles.h1_erro}>
					Alerta
				</Text>
				<Text style={styles.h2_erro}>
					{props.erro}
				</Text>
				{this.props.btnOk ? this.props.btnOk : null}
			 </View>
		 );
	}

	render() {
			 if(this.props.trackingLocation){
				this._trackingLocation();
			 }
			 return (
			 	<View style={styles.conteudoGeral}>
		        	<View
		            style={[styles.container]}>
		              <View style={[styles.conteudo]}>
		              	 {this.props.carregando ?
							this._getLoading()
						 : <View></View>}
						 {this.props.erro ?
							 this._getErro(this.props)
						 : <View></View>}
		                  <View style={styles.meio}>
		                  	{!this.props.ignoreKeyboard ?
		                   <KeyboardAwareScrollView 
			                  style={styles.KeyboardAwareScrollView} 
			                  contentContainerStyle={styles.KeyboardAwareScrollViewContent}
			                  automaticallyAdjustContentInsets={false}
			                  bounces={false}
			                  showsVerticalScrollIndicator={false}
			                  >
			                  <Fcm
				                onChangeToken={token => this.props.onToken ? this.props.onToken(token || "") : null}
				              />
							 <View style={{flex:1, width:"100%"}}>
								 {this.props.conteudo}
							 </View>
						 	</KeyboardAwareScrollView>
			                : 
			                <View
			                  style={{flex:1,width:"100%"}} 
			                  >
			                  <Fcm
				                onChangeToken={token => this.props.onToken ? this.props.onToken(token || "") : null}
				              />
							 <View style={{flex:1, width:"100%"}}>
								 {this.props.conteudo}
							 </View>
						 	</View>
			            	}
							 	 
              				</View>
              			</View>
              		</View>
              		<MessageBarAlert style={{zIndex:10}} ref="alert" />
              	</View>
			 );
	 }
}
