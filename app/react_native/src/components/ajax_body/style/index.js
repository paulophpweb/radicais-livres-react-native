const React = require('react-native');

const { StyleSheet, Dimensions, Platform} = React;
import Template from '../../../constants/cores';

export default {
     conteudoGeral: {
        paddingTop:(Platform.OS === 'ios') ? 20 : 0,
        flex:1,
        backgroundColor:Template.cor_1
      },
      container: {
        flex:1,
        backgroundColor:"#fff"
      },
      conteudo: {
        justifyContent: 'center',
        alignItems: 'center',
        flex:1,
      },
      meio: {
        justifyContent: 'center',
        alignItems: 'center',
        flex:1,
        width:"100%",
      },
      KeyboardAwareScrollView:{
        width:"100%"
      },
      KeyboardAwareScrollViewContent:{
        width:"100%", 
        justifyContent:"center", 
        alignItems:"center",
        flexGrow: 1
      },
    box: {
        width:200,
        height:40,
        color:"#fff"
    },
    container_erro: {
        backgroundColor:"rgba(255,255,255,0.7)",  
        justifyContent:"center", 
        alignItems:"center",
        padding:20,
        position:"absolute",
        top:0,
        left:0,
        right:0,
        bottom:0,
        zIndex:10
    },
    h1_erro: {
        color:Template.cor_1,
        fontFamily:'Montserrat-Bold',
        fontSize:20
    },
    h2_erro: {
        color:"#000",
        fontFamily:'Montserrat-Regular',
        fontSize:14
    },
    
};
