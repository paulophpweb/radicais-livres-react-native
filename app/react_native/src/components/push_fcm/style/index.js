const React = require('react-native');

const { StyleSheet, Dimensions} = React;
const deviceAltura = Dimensions.get('window').height;

export default {
    container: {
        backgroundColor:"rgba(0,0,0,0.4)",
        justifyContent: 'center',
        alignItems: 'center',
        flex:1
    },
    box: {
        width:200,
        height:40,
        color:"#fff"
    },
};
