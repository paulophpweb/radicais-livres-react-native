import React, { Component } from 'react';
import { View, Text, ActivityIndicator, Modal, Image } from 'react-native';
import styles from './style';
import * as Animatable from 'react-native-animatable';
import {
  _rosa,
  _verde
} from '../../constants/cores';

export default class Loading extends Component {
  render() {
     return (
       <Modal
       animationType={"fade"}
       transparent={true}
       onRequestClose={() => {}}>
         <View style={styles.container}>
              <Animatable.View animation="fadeIn">
                <Animatable.Image style={{height:200}} source={require('../../images/mao.png')} resizeMode={Image.resizeMode.contain}/>
              </Animatable.View>
         </View>
       </Modal>
     );
   }
}
