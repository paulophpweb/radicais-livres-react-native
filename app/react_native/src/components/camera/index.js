import React from 'react';
import {
  Image,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Platform,
  Modal,
  PermissionsAndroid,
} from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';
import Camera from './include';
var mimetype = require('react-native-mimetype');
import Icon from 'react-native-vector-icons/MaterialIcons';
var RNFS = require('react-native-fs');
import MovToMp4 from 'react-native-mov-to-mp4';
import ImageResizer from 'react-native-image-resizer';
var downloadTimer = null;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  overlay: {
    position: 'absolute',
    padding: 16,
    right: 0,
    left: 0,
    alignItems: 'center',
  },
  topOverlay: {
    top: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bottomOverlay: {
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.4)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  captureButton: {
    padding: 15,
    backgroundColor: 'white',
    borderRadius: 40,
  },
  typeButton: {
    padding: 5,
  },
  flashButton: {
    padding: 5,
  },
  buttonsSpace: {
    width: 10,
  },
  tempo: {
    position:"absolute",
    left:"50%",
    top:"50%"
  },
  carregando: {
    position:"absolute",
    top:0,
    left:0,
    right:0,
    bottom:0,
    justifyContent:"center",
    alignItems:"center",
    backgroundColor:"rgba(0,0,0,0.9)"
  },
  carregando_texto: {
    fontSize:20,
    fontFamily:'Montserrat-Bold',
    color:"#fff"
  },
  tempo_texto: {
    fontSize:35,
    fontFamily:'Montserrat-Bold',
    color:"#000"
  },
  icon_back: {
    position:"absolute",
    bottom:30,
    left:5
  },
});

export default class Example extends React.Component {
  constructor(props) {
    super(props);

    this.camera = null;

    this.state = {
      camera: {
         aspect: "fill",
         captureTarget: "disk",
         type: "back",
         flashMode: "auto",
      },
      isRecording: false,
      count:0,
      video:""
    };
  }

  takePicture = () => {
    var _this = this;
    if (this.camera) {
      this.setState({
        carregando:true
      });
      this.camera.capture({
        captureTarget:"disk"
      })
        .then((data) => {
          name = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase()+".jpg";
          ImageResizer.createResizedImage(data.path, 800, 800, 'JPEG', 100)
          .then((resizedImageUri) => {
            var objeto = {selected:resizedImageUri};
            Actions.crop({
              type: ActionConst.REPLACE,
              objeto, 
              get_imagem(dados){
                if(_this.props.get_imagem){
                  _this.props.get_imagem(dados);
                }else{
                  Actions.postar({
                    type: ActionConst.REPLACE,
                    form_data_post_image:dados,
                  });
                }
                if(_this.props.onBack)
                    _this.props.onBack(); 
              }
            });
            
          });
        });
    }
  }

  contar = () => {
    var _this = this;
    $tempo = _this.props.segundos ? _this.props.segundos : 60;
    var timeleft = $tempo+1;
    var downloadTimer = setInterval(function(){
      _this.setState({count:--timeleft});
      if(timeleft <= 0){
        clearInterval(downloadTimer);
        _this.setState({carregando:true});
      }
    },1000);
  }

  startRecording = () => {
    var _this = this;
    if (_this.camera) {
      _this.camera.capture({
        mode: Camera.constants.CaptureMode.video,
        captureQuality:Camera.constants.CaptureQuality.low,
        captureTarget:Camera.constants.CaptureTarget.disk,
        audio:false
        })
          .then((data) => {
              if(downloadTimer)
                clearInterval(downloadTimer);
              if(Platform.OS === 'ios'){
                name = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase()+".mp4";
                MovToMp4.convertMovToMp4(data.path, name, function (results) {
                  $objeto = {
                    uri:results.replace("file://",""),
                    name:name,
                    type:"video/mp4"
                  };
                  if(_this.props.get_video){
                    _this.props.get_video($objeto);
                  }else{
                    Actions.postar({
                      type: ActionConst.REPLACE,
                      form_data_post_video:$objeto,
                    });
                  }

                  if(_this.props.onBack)
                     _this.props.onBack();
                });
              }else{
                name = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase()+".mp4";
                $objeto = {
                  uri:data.path,
                  name:name,
                  type:"video/mp4"
                };
                _this.setState({video:$objeto});
              }
            
            
          });
          _this.setState({isRecording: true});
          _this.contar();
    }
  }

  stopRecording = () => {
    var _this = this;
    if (this.camera) {
      if(downloadTimer)
          clearInterval(downloadTimer);
      this.setState({count:0});
      this.camera.stopCapture();
      this.setState({
        isRecording: false,
        carregando:true,
        count:0
      });
      if(Platform.OS === 'android'){
        if(_this.props.get_video){
          _this.props.get_video(_this.state.video);
        }else{
          Actions.postar({
            type: ActionConst.REPLACE,
            form_data_post_video:_this.state.video,
          });
        }
        if(_this.props.onBack)
           _this.props.onBack();
      }
    }
  }

  switchType = () => {
    let newType;

    if (this.state.camera.type === 'back') {
      newType = 'front';
    } else if (this.state.camera.type === 'front') {
      newType = 'back';
    }

    this.setState({
      camera: {
        ...this.state.camera,
        type: newType,
      },
    });
  }

  get typeIcon() {
    let icon;
    const { back, front } = Camera.constants.Type;

    if (this.state.camera.type === 'back') {
      icon = require('../../images/camera2/ic_camera_rear_white.png');
    } else if (this.state.camera.type === 'front') {
      icon = require('../../images/camera2/ic_camera_front_white.png');
    }

    return icon;
  }

  switchFlash = () => {
    let newFlashMode;

    if (this.state.camera.flashMode === 'auto') {
      newFlashMode = 'on';
    } else if (this.state.camera.flashMode === 'on') {
      newFlashMode = 'off';
    } else if (this.state.camera.flashMode === 'off') {
      newFlashMode = 'auto';
    }

    this.setState({
      camera: {
        ...this.state.camera,
        flashMode: newFlashMode,
      },
    });
  }

  onBarCodeRead = (data) => {
    if(this.props.onBarCodeRead)
      this.props.onBarCodeRead(data);
  }

  get flashIcon() {
    let icon;

    if (this.state.camera.flashMode === 'auto') {
      icon = require('../../images/camera2/ic_flash_auto_white.png');
    } else if (this.state.camera.flashMode === 'on') {
      icon = require('../../images/camera2/ic_flash_on_white.png');
    } else if (this.state.camera.flashMode === 'off') {
      icon = require('../../images/camera2/ic_flash_off_white.png');
    }

    return icon;
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          animated
          hidden
        />
        <Camera
          ref={(cam) => {
            this.camera = cam;
          }}
          style={styles.preview}
          aspect={this.state.camera.aspect}
          type={this.state.camera.type}
          flashMode={this.state.camera.flashMode}
          captureTarget={this.state.camera.captureTarget}
          onBarCodeRead={this.onBarCodeRead}
          defaultTouchToFocus
          mirrorImage={true}
          captureAudio={false}
        />
        {/*<View style={[styles.overlay, styles.topOverlay]}>
          <TouchableOpacity
            style={styles.typeButton}
            onPress={this.switchType}
          >
            <Image
              source={this.typeIcon}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.flashButton}
            onPress={this.switchFlash}
          >
            <Image
              source={this.flashIcon}
            />
          </TouchableOpacity>
        </View>*/}
        <View style={[styles.overlay, styles.bottomOverlay]}>
          {this.props.camera ?
            !this.state.isRecording
            &&
            <TouchableOpacity
                style={styles.captureButton}
                onPress={this.takePicture}
            >
              <Image
                  source={require('../../images/camera2/ic_photo_camera_36pt.png')}
              />
            </TouchableOpacity>
            ||
            null
          : null}
          <View style={styles.buttonsSpace} />
          {this.props.video ?
              !this.state.isRecording
              &&
              <TouchableOpacity
                  style={styles.captureButton}
                  onPress={this.startRecording}
              >
                <Image
                    source={require('../../images/camera2/ic_videocam_36pt.png')}
                />
              </TouchableOpacity>
              ||
              <TouchableOpacity
                  style={styles.captureButton}
                  onPress={this.stopRecording}
              >
                <Image
                    source={require('../../images/camera2/ic_stop_36pt.png')}
                />
              </TouchableOpacity>
          : null}
        </View>
        <View style={styles.tempo}>
          <Text style={styles.tempo_texto}>{this.state.count === 0 ? "" : this.state.count}</Text>
        </View>
        {this.state.carregando ?
        <View style={styles.carregando}>
          <Text style={styles.carregando_texto}>Processando...</Text>
        </View>
        : null }
        {this.props.onBack ?
        <TouchableOpacity
            style={styles.icon_back}
            onPress={this.props.onBack}
        >
          <Icon name="arrow-back" size={30} color={"#fff"} />
        </TouchableOpacity>
        : 
          <TouchableOpacity
            style={styles.icon_back}
            onPress={()=>{Actions.pop()}}
        >
          <Icon name="arrow-back" size={30} color={"#fff"} />
        </TouchableOpacity>
      }
      </View>
    );
  }
}