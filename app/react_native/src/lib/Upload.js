import React, { Component } from 'react';
import { Platform, StyleSheet, Alert, View, Text } from 'react-native';
var ImagePicker = require('react-native-image-picker');
import * as mime from 'react-native-mime-types';

export default class Upload extends Component {

  constructor(props) {
    super(props);
  }

  openVideoEscolha = ($sucess, $error, $largura=640, $altura=480) => {
    var _this = this;
      Alert.alert(
        'Vídeos',
        'Selecione um vídeo ou escolha filmar',
        [
          {text: 'Filmar', onPress: () => {
            var options = {
              mediaType:"video",
              maxWidth:$largura,
              maxHeight:$altura,
              allowsEditing:true
            };
            ImagePicker.launchCamera(options, (response)  => {
              if (response.didCancel) {
                if($error)
                  $error("Cancelado pelo usuário");
              }
              else if (response.error) {
                if($error)
                  $error(response.error);
              }
              else {
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                if (Platform.OS === 'android') {
                  var mimetype = mime.lookup(response.uri);
                  var extensao = mime.extension(mimetype);
                  name = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
                  response.name = name+".mov";
                  response.type = mimetype;
                  response.uri = response.uri;
                  if($sucess)
                    $sucess(response);
                }else{
                  var mimetype = mime.lookup(response.uri.replace("file://",""));
                  var extensao = mime.extension(mimetype);
                  name = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
                  response.name = name+".mov";
                  response.type = mimetype;
                  response.uri = response.uri.replace("file://","");
                  if($sucess)
                    $sucess(response);
                }
                
              }
            });
          }},
          {text: 'Selecionar Vídeo', onPress: () => {
            var options = {
              mediaType:"video",
              maxWidth:$largura,
              maxHeight:$altura,
              allowsEditing:true
            };
            ImagePicker.launchImageLibrary(options, (response)  => {
              if (response.didCancel) {
                if($error)
                  $error("Cancelado pelo usuário");
              }
              else if (response.error) {
                if($error)
                  $error(response.error);
              }
              else {
                if (Platform.OS === 'android') {
                  var mimetype = mime.lookup(response.uri);
                  var extensao = mime.extension(mimetype);
                  name = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
                  response.name = name+".mov";
                  response.type = mimetype;
                  response.uri = response.uri;
                  if($sucess)
                    $sucess(response);
                }else{
                  var resposta = {};
                  var mimetype = mime.lookup(response.uri.replace("file://",""));
                  var extensao = mime.extension(mimetype);
                  name = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
                  resposta.name = name+".mov";
                  resposta.type = mimetype;
                  resposta.uri = response.uri.replace("file://","");
                  if($sucess)
                    $sucess(resposta);
                }
              }
            });
          }},
          {text: 'Cancelar', onPress: () => {
            if($error)
              $error("Cancelado pelo usuário");
          }, style: 'cancel'}
        ],
        { cancelable: false }
      );

  }
  /**
    Metodo que abre a camera e cropa a imagem
    
    $sucess = function(resposta)
    $error = function(resposta)
    $largura = Boolean
    $altura = Boolean
  */
  openCamera = ($sucess, $error, $largura=800, $altura=800) => {
    var _this = this;
    ImagePicker.launchCamera({
        mediaType:"photo",
        maxWidth:$largura,
        maxHeight:$altura,
        quality:1
      } , (response)  => {
      if (response.didCancel) {
        if($error)
          $error("Cancelado pelo usuário");
      }
      else if (response.error) {
        if($error)
          $error(response.error);
      }
      else {
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        if (Platform.OS === 'android') {
          var mimetype = mime.lookup(response.uri);
          var extensao = mime.extension(mimetype);
          name = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
          response.name = name+"."+extensao;
          response.type = mimetype;
          response.uri = response.uri;
          if($sucess)
            $sucess(response);
        }else{
          var mimetype = mime.lookup(response.uri.replace("file://",""));
          var extensao = mime.extension(mimetype);
          name = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
          response.name = name+"."+extensao;
          response.type = mimetype;
          response.uri = response.uri.replace("file://","");
          if($sucess)
            $sucess(response);
        }
        
      }
    });

  }
  /**
    Metodo que abre a biblioteca e cropa a mesma

    $sucess = function(resposta)
    $error = function(resposta)
    $largura = Boolean
    $altura = Boolean
    $url = Url de upload
    $onPogress = function(progresso)
    $onPogress = $onPogressComplete
  */
  openGaleria = ($sucess, $error, $largura=1600, $altura=1600) => {
    var _this = this;
    ImagePicker.openPicker({
      width: $largura,
      height: $altura,
      cropping: true,
      includeBase64:false,
      loadingLabelText:"Processando..."
    }).then((response) => {
        if (Platform.OS === 'android') {
          var mimetype = mime.lookup(response.uri);
          var extensao = mime.extension(mimetype);
          name = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
          response.name = name+"."+extensao;
          response.type = mimetype;
          response.uri = response.uri;
          if($sucess)
            $sucess(response);
        }else{
          var resposta = {};
          var mimetype = mime.lookup(response.uri.replace("file://",""));
          var extensao = mime.extension(mimetype);
          name = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
          resposta.name = name+"."+extensao;
          resposta.type = mimetype;
          resposta.uri = response.uri.replace("file://","");
          if($sucess)
            $sucess(resposta);
        }

    }).catch(e => {
      if($error)
        $error("Cancelado pelo usuário");
    });

  }
}