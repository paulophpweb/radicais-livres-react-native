var MessageBarManager = require('react-native-message-bar').MessageBarManager;
import { Platform } from 'react-native';
const Permissions = require('react-native-permissions');
var mimetype = require('react-native-mimetype');
import RNFetchBlob from 'react-native-fetch-blob';
var CryptoJS = require("crypto-js");
import SocketIOClient from 'socket.io-client/dist/socket.io';
import sailsIOClient from 'sails.io.js';
import Config from '../constants/config';
var io = sailsIOClient(SocketIOClient);
io.sails.url = Config.url;
io.sails.reconnection = true;
io.sails.autoConnect = true;
io.sails.transports = ['websocket'];
io.sails.environment = 'production';

class Uteis {

  chave = "jesusunicocaminho";

  validaEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  async permissao(){
      Permissions.request('location')
      .then(response => {
        Permissions.request('camera')
        .then(response => {
          Permissions.request('microphone')
          .then(response => {
            Permissions.request('photo')
            .then(response => {
              //console.warn(JSON.stringify(response));
            });
          });
        });
      });
  }

  alerta(texto,type,duracao=5000,onHide) {
    MessageBarManager.showAlert({
      title: 'Alerta',
      message: texto,
      alertType: type,
      duration:duracao,
      viewTopOffset : Platform.OS === 'android' ? 0 : 20,
      onHide:function(){
        if(onHide)
          onHide();
      }
    });
  }

  cript(mensagem) {
    var ciphertext = CryptoJS.AES.encrypt(mensagem, this.chave);
    console.warn(ciphertext);
    return ciphertext;
  }

  decript(codigo) {
    var bytes  = CryptoJS.AES.decrypt(codigo, this.chave);
    var plaintext = bytes.toString(CryptoJS.enc.Utf8);
    return plaintext;
  }

  getSocketId(callback) {
    callback(io,{});
    return;
    /*io.socket.request({
      method: 'get',
      url: '/post/getSocketID',
    }, function (resData, jwres) {
      console.warn(jwres);
      if (jwres.error) {
        
        return false;
      }else{
        console.warn("foi");
        callback(io,resData);
      }
    });*/
  }

  datamask(v){
    v = v.replace(/\D/g,"");
    v = v.replace(/^(\d\d)(\d)/g,"$1/$2");
    if(v.length>5)v = v.replace(/(\d{2})(\d)/,"$1/$2");
    v = v.slice(0, 10);
    return v;
  }

  getDataImageFacebook(token_facebook, userid) {
    const PictureDir = RNFetchBlob.fs.dirs.PictureDir;
    var api = `https://graph.facebook.com/v2.3/${userid}/picture?width=200&redirect=false&access_token=${token_facebook}`;
    return new Promise(function (resolve, reject) {
      fetch(api)
      .then((response) => response.json())
      .then((responseData) => {
        // send http request in a new thread (using native code)
        RNFetchBlob
        .config({
          fileCache : true,
          appendExt : 'jpg'
        })
        .fetch('GET', responseData.data.url)
          .then((res) => {
            let nomeImagem = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase()+".jpg";
            resolve({
              uri:Platform.OS === 'android' ? "file://"+res.data : res.data,
              type:"image/jpg",
              name: nomeImagem
            });
          })
          // Status code is not 200
          .catch((errorMessage, statusCode) => {
            reject(errorMessage);
          })
      }).done();
    });
    
  }

  getUserDataFacebook(token_facebook, userid) {
    var  _this = this;
    var api = `https://graph.facebook.com/v2.3/${userid}?fields=birthday,email,cover,first_name,id,last_name,picture,gender,locale,name&access_token=${token_facebook}`;
    return new Promise(function (resolve, reject) {
      fetch(api)
      .then((response) => response.json())
      .then((responseData) => {
        if(responseData.error){
          reject(responseData.error.message);
        }else{
          _this.getDataImageFacebook(token_facebook, userid).then((image) => {
            responseData.imagem = image;
            resolve(responseData);
          })
          .catch(function (error) {
            reject(error);
          });
        }
      }).done();
    });
    
  }

  generateUUID() {
      var d = new Date().getTime();
      var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          var r = (d + Math.random()*16)%16 | 0;
          d = Math.floor(d/16);
          return (c=='x' ? r : (r&0x3|0x8)).toString(16);
      });
      return uuid;
  }

  calculate_distance($lat1, $lon1, $lat2, $lon2, $unit='K') 
  { 
    var deg2rad = function (deg) { return deg * (Math.PI / 180); },
        R = 6371,
        dLat = deg2rad($lat2 - $lat1),
        dLng = deg2rad($lon2 - $lon1),
        a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
            + Math.cos(deg2rad($lat1))
            * Math.cos(deg2rad($lon1))
            * Math.sin(dLng / 2) * Math.sin(dLng / 2),
        c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return (R * c).toFixed(2);
  }

  returnDataImage(data) {
    
    
  }

}

let UteisClass = new Uteis();
export default UteisClass;
