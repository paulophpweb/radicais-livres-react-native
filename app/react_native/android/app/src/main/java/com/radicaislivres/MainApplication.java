package com.radicaislivres;

import android.app.Application;

import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactApplication;
import com.wix.RNCameraKit.RNCameraKitPackage;
import com.lwansbrough.RCTCamera.RCTCameraPackage;
import com.burnweb.rnpermissions.RNPermissionsPackage;
import com.wix.interactable.Interactable;
import com.dylanvann.fastimage.FastImageViewPackage;
import fr.bamlab.rnimageresizer.ImageResizerPackage;
import cl.json.RNSharePackage;
import com.lwansbrough.RCTCamera.*;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.showlocationservicesdialogbox.LocationServicesDialogBoxPackage;
import com.brentvatne.react.ReactVideoPackage;
import com.magus.fblogin.FacebookLoginPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.imagepicker.ImagePickerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.evollu.react.fcm.FIRMessagingPackage;
import com.rnfs.RNFSPackage;
import com.horcrux.svg.SvgPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new VectorIconsPackage(),
            new RNCameraKitPackage(),
            new RCTCameraPackage(),
            new RNPermissionsPackage(),
            new Interactable(),
            new FastImageViewPackage(),
            new ImageResizerPackage(),
            new RNSharePackage(),
            new RNFetchBlobPackage(),
            new LocationServicesDialogBoxPackage(),
            new ReactVideoPackage(),
            new FacebookLoginPackage(),
            new RNDeviceInfo(),
            new ImagePickerPackage(),
          new FIRMessagingPackage(),
          new RNFSPackage(),
          new SvgPackage()
      );
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
