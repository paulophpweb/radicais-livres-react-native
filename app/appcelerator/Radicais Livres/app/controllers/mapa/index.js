var args = arguments[0] || {};
exports.baseController = "base";
exports.controllerName = "/mapa/index";
$.container.add($.conteudo);
var _this = this;
var $list = [];
var $buscandoAnotation = false;
var Map = require('ti.map');
var mapview = null;
var annotations = [];
var annotationsPessoaRemote = [];
var annotationsMapa = [];
var annotationsIndexRemove = [];
var $latitudeUm = "";
var $latitudeTres = "";
var $longitudeUm = "";
var $longitudeDois = "";
var $latitudeCentro = "";
var $longitudeCentro = "";


function setMapa($data){
	var $latiCentralMapa = -16.695037;
	var $lngCentralMapa = -49.281845;
	if(args.latitude_central && args.longitude_central){
		$latiCentralMapa = args.latitudeCentral;
		$lngCentralMapa = args.latitudeCentral;
	}else if(Ti.App.Properties.getDouble("lng") && Ti.App.Properties.getDouble("lat")){
		$latiCentralMapa = Ti.App.Properties.getDouble("lat");
		$lngCentralMapa = Ti.App.Properties.getDouble("lng");
	}
	mapview = Map.createView({
		width:Ti.UI.FILL,
		height:Ti.UI.FILL,
	    mapType: Map.NORMAL_TYPE,
	    region: {
	    	latitude: $latiCentralMapa,
			longitude: $lngCentralMapa,
	    	latitudeDelta:0.01,
	    	longitudeDelta:0.01
	    },
	    animate:true,
	    regionFit:true,
	    userLocation:true,
	    enableZoomControls:true
	});
	
	$.mapa.add(mapview);
	
	mapview.addEventListener('regionchanged', function(obj) {
		console.log(" regionchanged");
		centerRegion(obj);
		
	});
	
	
}
function centerRegion(obj){
	if(Ti.Geolocation.getLocationServicesEnabled()){
		var $r = obj;
		var tl = GlobalFunctions.ll($r, 0,0);
		var tr = GlobalFunctions.ll($r, SizeView.pw(),0);
		var br = GlobalFunctions.ll($r, SizeView.pw(),SizeView.ph());
		var bl = GlobalFunctions.ll($r, 0,SizeView.ph());
		$latitudeUm = tl.latitude;
		$latitudeTres = br.latitude;
		$longitudeUm = tl.longitude;
		$longitudeDois = tr.longitude;
		$latitudeCentro = obj.latitude;
		$longitudeCentro = obj.longitude;
		var $tmp = {
				latitudeUm	: tl.latitude,
				latitudeTres	: br.latitude,
				longitudeUm	: tl.longitude,
				longitudeDois	: tr.longitude,
				latitudeCentro : obj.latitude,
		        longitudeCentro : obj.longitude
		};
		if(!$buscandoAnotation)
			getAnotation({data:$tmp});
	}
}

function clickPesquisa(e){
	_this.openWindow("mapa/procurar", false, {latitudeUm:$latitudeUm,latitudeTres:$latitudeTres,longitudeUm:$longitudeUm,longitudeDois:$longitudeDois,latitudeCentro:$latitudeCentro,longitudeCentro:$longitudeCentro});
}

function removeAnnotations(){
	for(var i=0;i<annotations.length;i++){
		mapview.removeAnnotation(annotations[i]);
	}
	annotations = [];
        
}

function addAnotation($data){
	if($data){
		annotationsPessoaRemote = [];
		var $setarMapa = false;
		var $dataObjeto = $data[0];
		var $dataObjetoEvento = $data[1];
		var $dataObjetoIgreja = $data[2];
		var $tamanho = $dataObjeto["dados"].length;
		if($tamanho > 0){
			// adciona as pessoas no mapa
			for(var a= 0; a<$tamanho; a++){
				var $p = $dataObjeto["dados"][a];
				if($p.lat != null && $p.lat != "" && $p.lng != null && $p.lng != ""){
					var $imagemicone = $p.foto ? GlobalFunctions.getURL($p.foto.replace("100x100","icone")) : $p.foto;
					var $index = findIndexByKeyValue(annotationsMapa,['cadastro_id_mapa','latitude','longitude'],[$p.cadastro_id,$p.lat,$p.lng]);
					if($index){
							if($index[3]){
								//console dos anotation no mapa
								console.log($index[3]+" - "+$p.cadastro_id);
								console.log($index[1]+" - "+$p.lat);
								console.log($index[2]+" - "+$p.lng);
								// se os anotation do mapa for igual a do banco nao atualiza
								if($index[3] == $p.cadastro_id && $index[1] == $p.lat && $index[2] == $p.lng){	
								}else{
									console.log("vamos remover");
									removeAnnotations();
								}
							}
					}
					if($index == null){
						$setarMapa = true;
						console.log("addcioando pessoa no mapa");
						GlobalFunctions.mapa.remoteUrl(mapview,Map,{
							latitude: $p.lat,
							longitude: $p.lng,
							title: $p.nome+" "+$p.sobrenome,
							subtitle: "esteve aqui "+GlobalFunctions.countDown($p.data_ult_localizacao),
							animate: false,
							image: $p.foto ? $imagemicone : "/images/icon-mapa-sem.png",
							cadastro_id_mapa:$p.cadastro_id,
							tipo: $p.foto ? "url" : "image"
						});
						annotations.push({
							latitude: $p.lat,
							longitude: $p.lng,
							title: $p.nome+" "+$p.sobrenome,
							subtitle: "esteve aqui "+GlobalFunctions.countDown($p.data_ult_localizacao),
							animate: false,
							image: $p.foto && OS_ANDROID ? $imagemicone : "/images/icon-mapa-sem.png",
							cadastro_id_mapa:$p.cadastro_id
						});
					}
					annotationsPessoaRemote.push({pessoa:{cadastro_id_mapa: $p.cadastro_id}});
				}
				
			}
		}
		// adciona os eventos no mapa
		
		var $tamanho = $dataObjetoEvento["evento"].length;
		if($tamanho){
			if($tamanho > 0){
				for(var a= 0; a<$tamanho; a++){
					var $p = $dataObjetoEvento["evento"][a];
					var eventoIgualMapa = findIndexByKeyValue(annotationsMapa,['evento_id_mapa','latitude','longitude'],[$p.evento_id,$p.latitude,$p.longitude]);
					if(eventoIgualMapa == null){
						$setarMapa = true;
						console.log("addcioando e no mapa");
						GlobalFunctions.mapa.remoteUrl(mapview,Map,{
							latitude: $p.latitude,
							longitude: $p.longitude,
							title: $p.titulo,
							subtitle: $p.endereco,
							animate: false,
							image: "/images/icon-mapa-evento.png",
							evento_id_mapa: $p.evento_id,
							tipo:"image"
						});
						annotations.push({
							latitude: $p.latitude,
							longitude: $p.longitude,
							title: $p.titulo,
							subtitle: $p.endereco,
							animate: false,
							image: "/images/icon-mapa-evento.png",
							evento_id_mapa: $p.evento_id
						});
					}
				}
			}
			
		}
		// adciona as igrejas no mapa
		var $tamanho = $dataObjetoIgreja["igreja"].length;
		if($tamanho){
			if($tamanho > 0){
				for(var a= 0; a<$tamanho; a++){
					var $p = $dataObjetoIgreja["igreja"][a];
					var igrejaIgualMapa = findIndexByKeyValue(annotationsMapa,['igreja_id_mapa','latitude','longitude'],[$p.igreja_id,$p.latitude,$p.longitude]);
					if(igrejaIgualMapa == null){
						$setarMapa = true;
						console.log("addcioando e no mapa");
						GlobalFunctions.mapa.remoteUrl(mapview,Map,{
							latitude: $p.latitude,
							longitude: $p.longitude,
							title: $p.nome,
							subtitle: $p.endereco+" - "+($p.pastor ? $p.pastor : ""),
							animate: false,
							image: "/images/icon-mapa-igreja.png",
							igreja_id_mapa: $p.igreja_id,
							tipo:"image"
						});
						annotations.push({
							latitude: $p.latitude,
							longitude: $p.longitude,
							title: $p.nome,
							subtitle: $p.endereco,
							animate: false,
							image: "/images/icon-mapa-evento.png",
							igreja_id_mapa: $p.evento_id
						});
					}
				}
			}
			
		}
		
		$buscandoAnotation = false;
		
	}
}

function getAnotation(e){
	var $dados = e.data;
	if(args.nome_pessoa)
		$dados.nome_pessoa = args.nome_pessoa;
	if(args.igreja_id)
		$dados.igreja_id = args.igreja_id;
	$buscandoAnotation = true;
	_this.ajaxDefaultLoader({
		onComplete:function($data){
			var $dadosMapa = mapview.getAnnotations();
			if($dadosMapa)
				annotationsMapa = JSON.parse(JSON.stringify(mapview.getAnnotations()));	
			addAnotation($data);
			
		},
		data:$dados,
		action:"getusuarioslatlng",
		onStart:function(){
			
		},
		onError:function(){
			
		}
	});
}

function clickItem(e){
	
}
function onMarker(e){
	
}
function clickHeader(e){
	_this.openWindow(e.controller, false, e);
}
function clickFooter(e){
	_this.openWindow(e.controller, false, e);
}
exports.startup = function($data){
	setMapa($data);
	GlobalFunctions.permissaoGeoLocalizacao();
	
};
exports.destroy = function(){
	
};

exports.update = function($args){
	if($args){
		args = $args;
		mapview.removeAllAnnotations();
		if(args.latitude_central && args.longitude_central){
			$buscandoAnotation = false;
			mapview.setRegion({
		    	latitude: args.latitude_central,
				longitude: args.longitude_central,
		    	latitudeDelta:0.08,
		    	longitudeDelta:0.08
		    });
		    
		    delete args.latitude_central;
		    delete args.longitude_central;
		    
	    }else{
	    	getAnotation({data:$args});
	    }
		
	}
};

function findIndexByKeyValue(obj, arrayKey, arrayValue)
{
	if(obj){
	    for (var i = 0; i < obj.length; i++) {
	        if (obj[i][arrayKey[0]] == arrayValue[0] && obj[i][arrayKey[1]] == arrayValue[1] && obj[i][arrayKey[2]] == arrayValue[2]) {
	            return [i,obj[i].latitude,obj[i].longitude,obj[i].cadastro_id_mapa];
	        }
	    }
    }
    return null;
}
