exports.baseController = "base";
exports.controllerName = "/mapa/procurar";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
var $id_igreja = null;
var $id_cidade = null;
var $id_estado = null;
var $latitudeCentral = "";
var $longitudeCentral = "";

function clickSearchIgreja(e){
	Navigate.selectGrid({
		parent:$,
		action:"procurarigreja",
		hintText:'nome_igreja',
		onCancel:function(e){
			$.igraja_nome.text = L("nome_igreja");
			$.igraja_nome.color = "#9aadc5";
			$id_igreja = null;
		},
		onComplete:function(e){
			$.igraja_nome.text = e.text;
			$.igraja_nome.color = "#3d3b3d";
			$id_igreja = e.igreja_id;
		},
		lineItem:function(e){
			return {
				text:e.nome,
				igreja_id:e.igreja_id
			};
		}
	});
}

function clickSearchEstado(e){
	Navigate.selectGrid({
		parent:$,
		action:"getestado",
		hintText:'estado',
		onCancel:function(e){
			$.estado.text = "Estado:";
			$.estado.color = "#9aadc5";
			$id_estado = null;
		},
		onComplete:function(e){
			$.estado.text = e.text+" - "+e.sigla;
			$.estado.color = "#3d3b3d";
			$id_estado = e.estado_id;
		},
		lineItem:function(e){
			return {
				text:e.nome,
				estado_id:e.estado_id,
				sigla: e.sigla
			};
		}
	});
}

function clickSearchCidade(e){
	if($id_estado != null){
		Navigate.selectGrid({
			parent:$,
			action:"getcidade",
			data:{estado_id:$id_estado}, 
			hintText:'cidade',
			onCancel:function(e){
				$.cidade.text = "Cidade:";
				$.cidade.color = "#9aadc5";
				$id_cidade = null;
			},
			onComplete:function(e){
				$.cidade.text = e.text;
				$.cidade.color = "#3d3b3d";
				$id_cidade = e.cidade_id;
				
				var xhr2 = Titanium.Network.createHTTPClient();
				xhr2.onload = function() {
					   var jsonObject = JSON.parse(this.responseText);
					   
					   if(jsonObject.status != "ZERO_RESULTS"){
					   		var latLong = jsonObject.results[0].geometry.location;
					   		$latitudeCentral = latLong.lat;
					   		$longitudeCentral = latLong.lng;
					   }
				};
				xhr2.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
				xhr2.open("GET","https://maps.googleapis.com/maps/api/geocode/json?address="+encodeURI(e.text));
				xhr2.send();
				console.log("https://maps.googleapis.com/maps/api/geocode/json?address="+encodeURI(e.text));
			},
			lineItem:function(e){
				return {
					text:e.nome,
					cidade_id:e.cidade_id
				};
			}
		});
	}else{
		_this.dialog.alert({mensagem:"err_cidade_mapa_pesquisar"});
	}
}

function clickPesquisar(e){
	var $dados = [];
	if(args){
		delete args.onAJAXError;
		delete args.onError;
		delete args.controllerName;
		args["igreja_id"] = $id_igreja;
		args["nome_pessoa"] = $.nome_pessoa.value;
		args["nome_evento"] = $.nome_evento.value;
		args["latitude_central"] = $latitudeCentral;
		args["longitude_central"] = $longitudeCentral;
	}
	
	Navigate.backWindow(args);
}

