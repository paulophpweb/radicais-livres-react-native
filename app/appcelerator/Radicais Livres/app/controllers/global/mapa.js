exports.baseController = "base";
exports.controllerName = "/global/mapa";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
var $currentCallBack = null;
var $latitude = null;
var $longitude = null;

function ClickBack(e){
	if($currentCallBack.onCancel){
		$currentCallBack.onCancel();
	}
	Navigate.backWindow();
}

function getGeoLocation(){
	if(!$currentCallBack.latitude && !$currentCallBack.longitude){
		Ti.Geolocation.preferredProvider = Titanium.Geolocation.PROVIDER_GPS;
		Titanium.Geolocation.accuracy=Titanium.Geolocation.ACCURACY_BEST;
		Titanium.Geolocation.distanceFilter=0;
		
		if(Titanium.Geolocation.locationServicesEnabled==false){ 
		    Titanium.UI.createAlertDialog({title:'Erro', message:'Para melhor experiência da ferramenta Drive, é necessário ativar o GPS.',buttonNames:['Ok'] }).show();
		} else {
	        Titanium.Geolocation.getCurrentPosition(function(e){
	            if (e.error) {
	                    Ti.API.error(JSON.stringify(e.error));
	                return;
	                } else {
	                    longitude = e.coords.longitude;
	                    latitude = e.coords.latitude;   
	           			showMap(latitude,longitude);
	                }
	        });
		}
	}else{
		showMap($currentCallBack.latitude,$currentCallBack.longitude);
	}
}

function marcarLocal(e){
	$currentCallBack.onComplete({latitude:$latitude,longitude:$longitude});
	Navigate.backWindow();
}

function showMap(latitude,longitude){
	var Map = require('ti.map');

	var mapview = Map.createView({
		width:Ti.UI.FILL,
		height:Ti.UI.FILL,
	    mapType: Map.NORMAL_TYPE,
	    regionFit:true,
    	userLocation:true,
	    animate:true,
	    userLocation:true,
	    enableZoomControls:false
	});
	mapview.setRegion({
		latitude:latitude,
	    longitude:longitude,
	    latitudeDelta : 0.01,
		longitudeDelta : 0.01
	});
	$.mapa.add(mapview);

	mapview.addEventListener('regionchanged', function(e){
		$latitude = e.latitude;
		$longitude = e.longitude;
	});
	
}

exports.startup = function($data){
	
};
exports.destroy = function(){
	
};


exports.selectMapa = function($obj/*{onComplete:Function, parent:Component*/){
	$currentCallBack = $obj;
	getGeoLocation();
};