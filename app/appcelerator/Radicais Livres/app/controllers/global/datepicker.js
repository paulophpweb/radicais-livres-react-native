exports.baseController = "base";
exports.controllerName = "/timeline";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
var $currentCallBack = null;
var $data = null;

function ClickBack(e){
	if($currentCallBack.onCancel){
		$currentCallBack.onCancel();
	}
	Navigate.backWindow();
}

function marcarData(e){
	if(!$data){
		$data = e.source.value;
	}
	$currentCallBack.onComplete({data:$data});
	Navigate.backWindow();
}

function gravaData(e){
	$data = e.source.value;
}

exports.selectDatepicker = function($obj/*{onComplete:Function, parent:Component*/){
	$currentCallBack = $obj;
};