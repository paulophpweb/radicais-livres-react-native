exports.baseController = "base";
exports.controllerName = "/timeline";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
var $loading = false;
var $interval = null;
var $prorucar = "";
var $itemSelected = null;
var $list = null;
var $dataGrid = null;
var $currentCallBack = null;
var dados = {};
function filterSelect(){
	$loading = true;
	$prorucar = $.procurar.value;
	$currentCallBack = $currentCallBack || {data:{}};

	$currentCallBack.data = $currentCallBack.data || {};
	$currentCallBack.data.texto = StringUtil.trim($.procurar.value);
	$.opcoes.setItems([]);
	Ajax.post({
		onComplete:function($data){
			if($data.length>0){
				$dataGrid = [];
				for(var a in $data){
					var $item = $data[a];
					$dataGrid.push({
						title:$currentCallBack.lineItem($item)
					});
				}
				$.opcoes.setItems($dataGrid);
			}
			if($prorucar != $.procurar.value && $.procurar.value.split(" ").join("") != ""){
				filterSelect();
			}else{
				$loading = false;
				$.ic_entrar_loader.visible = false;
				$.ic_entrar_loader.stop();
			}
		},
		onStart:function(){
			$.ic_entrar_loader.visible = true;
			$.ic_entrar_loader.start();
		},
		onError:function(e){
			$loading = false;
			$.ic_entrar_loader.visible = false;
			$.ic_entrar_loader.stop();
			$.dialog.confirm({
				mensagem:e.msg,
				textLivre:true,
				confirmar:"tentar_novamente",
				onConfirm:function(){
					Ajax.post(e);
				}
			});
		},
		action:$currentCallBack.action,
		data:$currentCallBack.data
	});
}
function getLimit5($data){
	if($data.data){
		$data.data.limit = 10;
		dados = $data.data;
	}else{
		dados.limit = 10;
	}
	Ajax.post({
		onComplete:function($data){
			if($data.length>0){
				$dataGrid = [];
				for(var a in $data){
					var $item = $data[a];
					$dataGrid.push({
						title:$currentCallBack.lineItem($item)
					});
				}
				$.opcoes.setItems($dataGrid);
			}
		},
		action:$data.action,
		data:dados
	});
}
function blur(e){
	Triggers.shoot({parans:e.source, type:'text:login-blur'});
}
function focus(e){
	Triggers.shoot({parans:e.source, type:'text:login-focus'});
}
function change(e){
	if($interval != null){
		clearInterval($interval);
		$interval = null;
	}

	if(!$loading && $prorucar != $.procurar.value && $.procurar.value.split(" ").join("") != ""){
		$interval = setTimeout(filterSelect, 500);
	}
}
function clickItemSelected(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	$currentCallBack.onComplete(item.title);
	Navigate.backWindow();
}

function ClickBack(e){
	if($currentCallBack.onCancel){
		$currentCallBack.onCancel();
	}
	Navigate.backWindow();
}

exports.selectGrid = function($obj/*{onComplete:Function, lineItem:Function, parent:Component, action:String, hintText:Action*/){
	$currentCallBack = $obj;
	$.opcoes.setItems([]);
	$.procurar.hintText = L($obj.hintText);
	$.procurar.focus();
	getLimit5($currentCallBack);

};
