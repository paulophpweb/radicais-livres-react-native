exports.baseController = "base";
exports.controllerName = "/timeline";
$.container.add($.conteudo);
var args = arguments[0] || {};
$.campo_1.focus();

var callBack = {
	exec:function(type, key){
		if(callBack[key] != null && callBack[key][type] != null){
			callBack[key][type]();
		}
	},
	isexec:function(type, key){
		switch(type){
			case "data":
			case "email":
			case "min":
				callBack.exec(type, key);
				break;
		}
	},
	format:{
		password : function(){
			$.campos_extra.show();
			$.campo_1.action = "min";
			$.campo_1.hintText = L(args.hint_senhaatual);
			$.campo_2.hintText = L(args.hint_novasenha);
			$.campo_3.hintText = L(args.hint_senhaconfirmar);
			$.campo_1.passwordMask = true;
			$.campo_1.keyboardType=Ti.UI.KEYBOARD_DEFAULT;
			$.campo_2.keyboardType=Ti.UI.KEYBOARD_DEFAULT;
			$.campo_3.keyboardType=Ti.UI.KEYBOARD_DEFAULT;
		},
		email : function(){
			$.campo_1.action = "mail";
			$.campo_1.hintText = L(args.hint_email);
			$.campo_1.value =  args.text || "";
			if(OS_ANDROID)$.campo_1.setSelection($.campo_1.value.length, $.campo_1.value.length);
			$.campo_1.passwordMask = false;
			$.campo_1.keyboardType=Ti.UI.KEYBOARD_EMAIL;
		},
		data : function(){
			$.campo_1.action = "date";
			$.campo_1.hintText =  L(args.hint_data);
			$.campo_1.value = args.text || "";
			if(OS_ANDROID)$.campo_1.setSelection($.campo_1.value.length, $.campo_1.value.length);
			$.campo_1.passwordMask = false;
			$.campo_1.keyboardType=Ti.UI.KEYBOARD_PHONE_PAD;
		},
		min : function(){
			$.campo_1.min = 4;
			$.campo_1.action = "min";
			$.campo_1.hintText = L(args.hint_min);
			$.campo_1.value = args.text || "";
			if(OS_ANDROID)$.campo_1.setSelection($.campo_1.value.length, $.campo_1.value.length);
			$.campo_1.keyboardType=Ti.UI.KEYBOARD_DEFAULT;
		}
	},
	check:{
		password : function(){
			if($.campo_1.value.length<6)$.dialog.alert({mensagem:"senha_atual_invalida"});
			else if($.campo_2.value != $.campo_3.value)$.dialog.alert({mensagem:"senha_nao_confere"});
			else{
				args.onComplete({password:$.campo_1.value, newpassword:$.campo_2.value});
				Navigate.backWindow();
			}
		},
		email : function(){
			if(!Validate.command.mail($.campo_1))$.dialog.alert({mensagem:"cadastro_fase_1_erro_mensagem"});
			else{
				args.onComplete({email:$.campo_1.value});
				Navigate.backWindow();
			}
		},
		data : function(){
			if(!Validate.command.date($.campo_1))$.dialog.alert({mensagem:"data_invalida"});
			else{
				args.onComplete({data:$.campo_1.value});
				Navigate.backWindow();
			}
		},
		min : function(){
			if(!Validate.command.min($.campo_1))$.dialog.alert({mensagem:"qtd_char_invalida"});
			else{
				args.onComplete({min:$.campo_1.value});
				Navigate.backWindow();
			}
		}
	},
	change:{
		data : function(){
			Mask.mask($.campo_1, "date");
		}
	}
};
function change(e){
	callBack.isexec(args.type, "change");
}
function returnCampoUm(e){
	callBack.isexec(args.type, "check");
}
function clickSave(e){
	$.campo_1.blur();
	$.campo_2.blur();
	$.campo_3.blur();
	callBack.exec(args.type, "check");
}
/*Sample
 Navigate.editText({
	parent:$.parent,
	
//	type:"password",
//	hint_senhaatual:"senha_atual",
//	hint_novasenha:"senha_nova",
//	hint_senhaconfirmar:"senha_confirmar",
	
//	type:"email",
//	text:"joisiney@gmail.com",
//	hint_email:"email",
	
//	type:"data",
//	text:"26/03/1984",
//	hint_data:"data_nascimento",
	
	type:"min",
	text:"Joisiney",
	hint_min:"cadastro_fase_2_campo_nome",
	
	onComplete:function(e){
		Ti.API.error(JSON.stringify(e));
	}
});*/
exports.editText = function($obj){
	args = $obj;
	if(args.type != null)callBack.exec(args.type, "format");
};