exports.baseController = "base";
exports.controllerName = "/chat/listar";
$.container.add($.conteudo);
var _this = this;
var $itensTemplate = [];
var $qtdePerPage = 10;
var $page = 1;
var $qtde = 10;

function gridAmigos($data){
	var $iconStatus = "";
	$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
	if($data){
		var $tamanho = $data.length;
		if($tamanho > 0){
			for(var a= 0; a<$tamanho; a++){
				var $p = $data[a];
				if($p.online == 1){
					$iconStatus = "/images/icon-online.png";
				}else if($p.ausente == 1){
					$iconStatus = "/images/icon-ausente.png";
				}else if($p.offline == 1){
					$iconStatus = "/images/icon-offline.png";
				}
				var item = {
						foto:{image:GlobalFunctions.getURL($p.foto, $p.genero)},
						nome:{text:$p.nome+" "+$p.sobrenome},
						status:{image:$iconStatus},
						nao_lido:{text:$p.nao_lido, visible:$p.nao_lido ? true : false},
						dados:{cadastro_id_amizade:$p.cadastro_id}
					};
				
				$itensTemplate.push(item);
			}
		}
		$.iterator.setItems($itensTemplate);
	}
}

function clickItem(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	_this.openWindow("chat/chatconversa", false, {cadastro_id_pessoa:item.dados.cadastro_id_amizade,nome_amizade:item.nome.text,action:"getchatconversa"});
}

function clickNav(e){
	_this.openWindow(e.controller, false, e);
}
function onMarker(e){
	$page = $page+1;
	$qtdePerPage = ($qtde * $page);
	_this.ajaxDefaultLoader({
		onStart:function(){
			$.loaderResultado.start();
			$.loaderResultado.visible = true;
		},
		onComplete:function($data){
			$.loaderResultado.stop();
			$.loaderResultado.visible = false;
			gridAmigos($data);
		},
		action:"getamigosonline",
		data:{
			limit:$qtde,
			offset:($qtde * ($page - 1))
		}
	});
}
function onPull(e){
	$itensTemplate = [];
	_this.ajaxDefaultLoader({
		onComplete:function($data){
			$qtdePerPage = 10;
			$page = 1;
			$qtde = 10;
			
			$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
			gridAmigos($data);
			$.ptr.hide();
		},
		action:"getamigosonline",
		onStart:function(){
			
		}
	});
	
}
exports.startup = function($data){
	gridAmigos($data);
};
exports.update = function(args){
	onPull({});
};
exports.destroy = function(){
	
};