exports.baseController = "base";
exports.controllerName = "/chat/chatconversa";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
var $itensTemplate = [];
var $qtdePerPage = 50;
var $page = 1;
var $qtde = 50;

function blur(e){
	Triggers.shoot({parans:e.source, type:'text:login-blur'});
}
function focus(e){
	Triggers.shoot({parans:e.source, type:'text:login-focus'});
}
function onReturn(e){
	
}

function gridChatConversa($data){
	$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
	if($data){
		$data.reverse();
		var $tamanho = $data.length;
		if($tamanho > 0){
			for(var a= 0; a<$tamanho; a++){
				var $p = $data[a];
				var item = {
						nome:{
							text:$p.nome+" "+$p.sobrenome,
							color: $p.cadastro_id == DataPerfil.cadastro_id ? "#79bc58" : "#373ab8"
						},
						texto:{
							text:$p.texto
						},
						horario_cadastro:{
							text:GlobalFunctions.countDown($p.data)
						}
					};
				
				$itensTemplate.push(item);
			}
		}
		$.iterator.setItems($itensTemplate);
		$.list.scrollToItem(0,($itensTemplate.length-1),{
			animated:true
		});
	}
}

function clickEnviar(e){
	var $data = {};
	$data.texto = $.textchat.value;
	$data.notificacao = "Seu amigo "+DataPerfil.nome+" te chamou no chat.";
	$data.cadastro_id_amizade = args.cadastro_id_pessoa;
	$.ajaxDefaultLoader({
		onComplete:function($data){
			// reload nos dados
			onPull({});
			$.textchat.value = "";
		},
		action:"chatinsert",
		data:$data
	});
}

function onPull(e){
	$itensTemplate = [];
	_this.ajaxDefaultLoader({
		onComplete:function($data){
			$qtdePerPage = 50;
			$page = 1;
			$qtde = 50;
			
			$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
			if($data.length)
				gridChatConversa($data);
			$.ptr.hide();
			$.list.scrollToItem(0,($data.length-1),{
				animated:true
			});
		},
		action:"getchatconversa",
		data:{cadastro_id_pessoa:args.cadastro_id_pessoa},
		onStart:function(){
			
		}
	});
	
}

function onMarker(e){
	$page = $page+1;
	$qtdePerPage = ($qtde * $page);
	_this.ajaxDefaultLoader({
		onStart:function(){
			$.loaderResultado.start();
			$.loaderResultado.visible = true;
		},
		onComplete:function($data){
			$.loaderResultado.stop();
			$.loaderResultado.visible = false;
			if($data.length)
				gridChatConversa($data);
		},
		action:"getchatconversa",
		data:{
			limit:$qtde,
			offset:($qtde * ($page - 1)),
			cadastro_id_pessoa:args.cadastro_id_pessoa
		}
	});
}
function notifier(e){
	if(e.controller == "/chat/chatconversa"){
		onPull({});
		Ti.API.error(" NOTIFICACAO",JSON.stringify(e));
	}
}

exports.startup = function($data){
	if(args.nome_amizade)
		$.nome_amizade.text = 'Conversando com: '+args.nome_amizade;
	if($data.length)
		gridChatConversa($data);
	Ti.App.addEventListener("notifier", notifier);
};
exports.destroy = function(){
	Ti.App.removeEventListener("notifier", notifier);
};