exports.baseController = "base";
exports.controllerName = "/timeline/teste";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
$.listposts.parent = this;
function clickNav(e){
	_this.openWindow(e.controller, true, e);
}


function clickChat(e){
	$.openWindow(e.controller, false,{action:e.action});
}

function searchHeader(e){
	if(e.value != ""){
		$.openWindow("perfil/sobre/amigo/resultado", false, {
			nome:e.value,
			action:"procurar"
		});
	}else $.dialog.alert({mensagem:"search_vazio"});
}
exports.update = function($args){
	$.listposts.update($args);
};
exports.startup = function($data){
	//$.header.on("search-header", searchHeader);
	//$.header.on("search-header-sobre", clickChat);
	//$.footer.on("click-footer", clickNav);
	$.listposts.startup($data);
	$.listposts.argumento(args);
};
exports.destroy = function(){
	//$.header.off("search-header", searchHeader);
	//$.footer.off("click-footer", clickNav);
	//$.header.off("search-header-sobre", clickChat);
	$.listposts.destroy();
};