var data_confirm = {};
var removedBtns = false;
var objLoading = null;
var inValid = new RegExp("[\s]");
function clickConfirmOk(e){
	closeConfirm(data_confirm.onConfirm);
}
function clickConfirmCancel(e){
	closeConfirm(data_confirm.onCancel);
}
function closeConfirm($method){
	Animation.fadeOut($.dialog_bg, 200, function(){
		visivel($.dialog_box, false);
		$.dialog_box_botton.bottom = -150;
		if($method != null)$method();
	});
	Animation.fadeOut($.dialog_box_botton, 100);
}

function visivel($view, $sim){
	$view.setVisible($sim);
	$view.setTouchEnabled($sim);
	$view.setOpacity($sim ? 1 : 0);
}
function clickLoading(e){
	if(objLoading != null && objLoading.onCancel != null){
		Animation.fadeOut($.dialog_bg, 200, function(){
			visivel($.dialog_box, false);
			visivel($.dialog_box_botton, true);
			visivel($.fechar, false);
			$.loader.stop();
			objLoading = null;
			visivel($.loader, false);
		});
		objLoading.onCancel();
		objLoading = null;
	}
}
function _loading($obj){
	objLoading = $obj;
	if($obj.show){
		Ti.API.info("OPEN LOADING");
		visivel($.dialog_box_botton, false);
		visivel($.dialog_box, true);
		visivel($.fechar, $obj.onCancel != null ? true : false);
		Animation.fadeIn($.dialog_bg, 100);
		visivel($.loader, true);
		$.loader.start();
	}else{
		Ti.API.info("CLOSE LOADING");
		$.dialog_bg.opacity = 0;
		visivel($.dialog_box, false);
		visivel($.dialog_box_botton, true);
		visivel($.fechar, false);
		$.loader.stop();
		objLoading = null;
		visivel($.loader, false);
		Ti.API.info("CLOSE LOADING COMPLETE ANIMATION");
		if($obj.onComplete != null)$obj.onComplete();
		
	}
};
function fecharLoading(){
	
}
exports.showLoading = function(){
	_loading({show:true});
};

exports.hideLoading = function(){
	_loading({show:false});
};

exports.loading = function($obj){
	$obj = $obj || {show:true};
	if($obj.show == null)$obj.show = true;
	_loading($obj);
};
exports.alert = function($obj){
	var $vars = {onComplete:null, tempo:3, titulo:"alert_radicais", mensagem:'alert_global_erro_mensagem',textLivre:false};
	_.extend($vars, $obj || {});
	console.log($vars.textLivre,"$vars.textLivre");
	
	$.dialog_title.text = L($vars.titulo);
	if(!$vars.textLivre){
		$.dialog_message.text = L($vars.mensagem);
	}else{
		$.dialog_message.text = $vars.mensagem;
	}
	if(!removedBtns){
		removedBtns = true;
		$.group_base.remove($.btn_ok);
		$.group_base.remove($.btn_cancel);
	}
	
	visivel($.loader, false);
	visivel($.dialog_box_botton, true);
	visivel($.dialog_box, true);
	Animation.fadeIn($.dialog_bg, 500);
	
	$.dialog_box_botton.bottom = -150;
	
	var animation 		= Titanium.UI.createAnimation();
	animation.curve 	= Ti.UI.ANIMATION_CURVE_EASE_IN_OUT;
	animation.opacity 	= 1;
	animation.bottom 	= 10;
	animation.duration 	= 300;
	
	$.dialog_box_botton.animate(animation, function(){
		Animation.shake($.dialog_box_botton, 500);
	});
	
	setTimeout(function(){
		Animation.fadeOut($.dialog_bg, 200, function(){
			visivel($.dialog_box, false);
			if($vars.onComplete != null)$vars.onComplete();
		});
		animation.opacity 	= 0;
		animation.bottom 	= -150;
		animation.duration 	= 180;
		$.dialog_box_botton.animate(animation);
	}, 500+($vars.tempo*1000));
};

exports.confirm = function($obj){
	var $vars = {onCancel:null, onConfirm:null, tempo:3, titulo:"alert_radicais", confirmar:"ok", mensagem:'alert_global_erro_mensagem'};
	_.extend($vars, $obj || {});
	data_confirm = $vars;
	
	if(removedBtns){
		removedBtns = false;
		$.group_base.add($.btn_ok);
		$.group_base.add($.btn_cancel);
	}
	
	$.dialog_title.text = L($vars.titulo);
	if(!$vars.textLivre){
		$.dialog_message.text = L($vars.mensagem);
	}else{
		$.dialog_message.text = $vars.mensagem;
	}
	$.btn_ok.title = L($vars.confirmar);

	visivel($.loader, false);
	visivel($.dialog_box_botton, true);
	visivel($.dialog_box, true);
	Animation.fadeIn($.dialog_bg, 500);
	$.dialog_box_botton.bottom = -150;
	
	var animation 		= Titanium.UI.createAnimation();
	animation.curve 	= Ti.UI.ANIMATION_CURVE_EASE_IN_OUT;
	animation.opacity 	= 1;
	animation.bottom 	= 10;
	animation.duration 	= 300;
	
	$.dialog_box_botton.animate(animation);
};
