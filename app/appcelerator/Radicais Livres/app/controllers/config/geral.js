exports.baseController = "base";
exports.controllerName = "menu/lista";
$.container.add($.conteudo);
var _this = this;
var $list = [{
	texto: {text:"Alterar Senha",controller:"timeline", action:"timeline", type:"window"},
	nav:{controller:"timeline", action:"timeline", type:"window"}
},{
	texto: {text:"Cancelar conta"}
}];
if(DataPerfil.is_root == 1){
	$list.splice (1, 1);
}
$.iterator.setItems($list);
function clickItem(e){
	var item = e.section.getItemAt(e.itemIndex);
	var $prop = item[e.bindId];

	if($prop != null){
		var $controller = $prop.controller;
		if($controller == null){
			$.dialog.confirm({
				mensagem:"excluir_conta",
				confirmar:"sim",
				onConfirm:function(){
					$.ajaxDefaultLoader({
						onComplete:function($data){
							_this.openWindow("login/index", true);
						},
						action:"removerconta",
						data: {
							excluido:1
						}
					});
				}
			});
		}else{
			var textfield = Ti.UI.createTextField({
				backgroundColor:"#fff",
				left:10,
				right:10,
				color:"#000",
				passwordMask: true
			});

			var dialog = Ti.UI.createAlertDialog({
			    title: 'Digite a sua nova senha.',
			    style: OS_IOS ? Ti.UI.iPhone.AlertDialogStyle.SECURE_TEXT_INPUT : "",
			    androidView: textfield,
			    buttonNames: ['Salvar','Cancelar'],
			    cancel: 1
			  });
			  dialog.addEventListener('click', function(e){
			    Ti.API.info('e.text: ' + JSON.stringify(e));
			    // se não cancelar entra aqui
			    if (e.index !== e.source.cancel){
			      var $senha = OS_IOS ? e.text : textfield.value;
			      $.dialog.confirm({
						mensagem:"alterar_senha",
						confirmar:"sim",
						onConfirm:function(){
							$.ajaxDefaultLoader({
								onComplete:function($data){
									_this.openWindow("login/index", true);
								},
								action:"alterarsenha",
								data: {
									senha:$senha
								}
							});
						}
				  });
			    }
			  });
			  dialog.show();
		}
		/*var $type = $prop.type;
		if($type == "window" && $controller != null)_this.openWindow($controller, true, $prop);
		else if($type == "url" && $controller != null)Ti.Platform.openURL($controller);*/
	}
}
function clickNav(e){
	_this.openWindow(e.controller, true, e);
}
exports.startup = function(){
	//$.lista.on("click-footer", clickNav);
};
exports.destroy = function(){
	//$.lista.off("click-footer", clickNav);
};
