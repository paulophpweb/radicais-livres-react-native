var args = arguments[0] || {};
console.log("args.controller",args.controller);
var $controllerHeader = Alloy.createController(args.controller);
var $viewHeader = $controllerHeader.getView();
if($controllerHeader != null && $controllerHeader.startup != null)$controllerHeader.startup();
$.headerView.add($viewHeader);
var $qtdePerPage = 10;
var $page = 1;
var $qtde = 10;
var $itensTemplate = [];
var $makerAction = "";
var $makercadastro_id_pessoa = "";
var $makertimeline = "";

function clickItem(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	GlobalFunctions.timeline.itemClick({
		event:e,
		section:$.list.sections[e.sectionIndex],
		window:clickSobre,
		video:GlobalFunctions.timeline.video,
		privacidade:GlobalFunctions.timeline.privacidade,
		like:function($obj){
			if(!$obj.like.curtiu && item.isliked.image != "/images/icon-curtido.png"){
				var $data = {};
				$data.post_id = $obj.dados.post_id;
				$data.cadastro_id_amizade = $obj.user.cadastro_id_pessoa;
				$data.tokenpushamizade = $obj.dados.tokenpush;
				$data.notificacao = "Seu amigo "+DataPerfil.nome+" curtiu seu post.";
				var qtdAtual = item.like.text.replace(" "+L('likes'),"");
				$.parent.ajaxDefaultLoader({
					onComplete:function($data){
						onPull({});
					},
					action:"curtir",
					data:$data
				});
			// implementar o discurtir
			}else{
				var $data = {};
				$data.post_id = $obj.dados.post_id;
				$.parent.ajaxDefaultLoader({
					onComplete:function($data){
						onPull({});
					},
					action:"descurtir",
					data:$data
				});
			}
		},
		excluir:function($obj){
			$.parent.dialog.confirm({
				mensagem:"excluir_post",
				confirmar:"sim",
				onConfirm:function(){
					$.parent.ajaxDefaultLoader({
						onComplete:function($data){
							$.parent.dialog.alert({mensagem:"del_sucesso",onComplete:onPull({})});
						},
						action:"removerpost",
						data:{
							post_id:$obj.dados.post_id
						}
					});
				}
			});
		},
		privacidade:function($obj){
			var lista = {
			  cancel: 0,
			  options: [
			  			L("cancelar")
			  			],
			  selectedIndex: 0,
			  destructive: 0
			};

			if($obj.user.cadastro_id_pessoa == DataPerfil.cadastro_id){
				lista.options.push("Excluir post");
			}else{
				lista.options.push("Denunciar");
				lista.options.push("Deixar de seguir");
			}

			var dialog = Ti.UI.createOptionDialog(lista);

			dialog.addEventListener("click", function(result){

				if(result.cancel != result.index && result.index>0){
					// excluir post
					if(lista.options[result.index] == "Excluir post"){
						$.parent.dialog.confirm({
							mensagem:"excluir_post",
							confirmar:"sim",
							onConfirm:function(){
								$.parent.ajaxDefaultLoader({
									onComplete:function($data){
										$.parent.dialog.alert({mensagem:"del_sucesso",onComplete:onPull({})});
									},
									action:"removerpost",
									data:{
										post_id:$obj.dados.post_id
									}
								});
							}
						});
					// denunciar
					}else if(lista.options[result.index] == "Denunciar"){
						var listaDenuncia = [];
						var listaDenunciaId = [];
						$.parent.ajaxDefaultLoader({
							onComplete:function($data){
								listaDenuncia.push(L("cancelar"));
								for(var a= 0; a<$data.length; a++){
									listaDenuncia.push($data[a].texto);
									listaDenunciaId.push($data[a].motivo_denuncia_cadastro_id);
								}

								var lista = {
								  cancel: 0,
								  options: listaDenuncia,
								  selectedIndex: 0,
								  destructive: 0
								};

								var dialog = Ti.UI.createOptionDialog(lista);

								dialog.addEventListener("click", function(result){
									if(result.cancel != result.index && result.index>0){
										$.parent.ajaxDefaultLoader({
											onComplete:function($data){
												$.parent.dialog.alert({mensagem:"denunciado_msg"});
											},
											action:"denunciar",
											data:{
												motivo_denuncia_cadastro:listaDenunciaId[result.index],
												cadastro_id_amizade:$obj.user.cadastro_id_pessoa,
												cadastro_id_denunciado:$obj.user.cadastro_id_pessoa,
												denuncia:lista.options[result.index],
												email:$obj.user.email,
												notificacao: "Seu amigo "+DataPerfil.nome+" denunciou você: "+lista.options[result.index]
											}
										});
									}
								});
								dialog.show();
							},
							action:"getmotivodenuncia"
						});
					// 	deixar de seguir
					}else if(lista.options[result.index] == "Deixar de seguir"){
						$.parent.dialog.confirm({
							mensagem:"excluir_amigo",
							confirmar:"sim",
							onConfirm:function(){
								$.parent.ajaxDefaultLoader({
									onComplete:function($data){
										$.parent.dialog.alert({mensagem:$data.msg,onComplete:onPull({})});
									},
									action:"removeramizade",
									data:{
										cadastro_id_amizade:$obj.user.cadastro_id_pessoa,
										action: "getperfil"
									}
								});
							}
						});
					}
				}
			});
			dialog.show();
		},
		compartilhar_button:function($obj){
			if($obj.share.cadastro_id != DataPerfil.cadastro_id){
				$.parent.dialog.confirm({
					mensagem:"compartilhar_post",
					confirmar:"sim",
					onConfirm:function(){
						var $data = {
							texto: $obj.share.texto,
							foto: $obj.share.foto == null ? "" : $obj.share.foto,
							cadastro_id_compartilhado: $obj.share.cadastro_id,
							post_id_compartilhado: $obj.share.post_id,
							compartilhando:true,
							video_id:$obj.share.video_id,
							video_tipo: $obj.share.video_tipo,
							facebook_usuario: $obj.share.facebook_usuario
						};
						$data.tokenpushamizade = $obj.dados.tokenpush;
						$data.notificacao = "Seu amigo "+DataPerfil.nome+" compartilhou seu post.";
						$data.cadastro_id_amizade = $obj.user.cadastro_id_pessoa;
						$.parent.ajaxDefaultLoader({
							onComplete:function($data){
								$.parent.dialog.alert({mensagem:"post_inserido",onComplete:onPull({})});
							},
							action:"post_insert",
							data:$data
						});

					}
				});
			}else{
				$.parent.dialog.alert({mensagem:"warn_prop_post"});
			}
		},
		texto:function($obj){
			$.parent.openWindow("timeline/post", false, {post_id:$obj.dados.post_id, action: "getpostid"});
		},
		video:function($obj){
			var section = $obj.section;
		    var item = section.getItemAt($obj.event.itemIndex);

			GlobalFunctions.h264videosWithYoutubeURL(item.dados.video_id,function(data){
				console.log(data.hd720);
				item.foto_video.visible = false;
		    	item.video_icon.visible = false;
		    	item.video_webview.visible = true;
		    	item.video_webview.height = 170;
		    	item.video_webview.url = data.small;
		    	section.updateItemAt($obj.event.itemIndex, item);
			});
		},
		foto:function($obj){
			$.parent.openWindow("timeline/post", false, {post_id:$obj.dados.post_id, action: "getpostid"});
		}
	});
}
function clickSobre(e){
	$.parent.openWindow(e.controller, false, e);
}
function getMyTimeline($data,$append){
	$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
	var $dataPerfil = {};
	$valida = true;
	if($data.timeline && $data.perfil){
		$dataPerfil = $data.perfil[0];
		$data = $data.timeline;
		$valida = false;
	}else{
		$dataPerfil = $data;
	}
	// verifica e ta entrando e um perfil de pessoa se esta pessoa e um amigo.
	if($dataPerfil.privacidade_ver_conteudo == "todos" || $dataPerfil.amigo == 1 && !$valida){
		$valida = true;
	}

	if($data.length && $valida){
		var $tamanho = $data.length;
		if(!$append)
			$itensTemplate = [];
		if($tamanho > 0){
		for(var a= 0; a<$tamanho; a++){
			var $p = $data[a];
			console.log("$p",$p);
			var $texto_acao = L('postou');
			if($p.nome_compartilhado != null){
				$texto_acao = L('shared_for');
			}else if($p.arquivo != null){
				$texto_acao = L('add_photo');
			}

			var item = {
					user:{
						image:GlobalFunctions.getURL($p.foto, $p.genero),
						cadastro_id_pessoa: $p.cadastro_id,
						amigo: $p.amigo,
						email: $p.email
					},
					user_name:{
						text:$p.nome
					},
					texto:{
						text:StringUtil.abreviar($p.texto, 140, "[...]"),
						height:40
					},
					status_compartilhamento:{
						text:$texto_acao
					},
					horario_compartilhamento:{
						text:GlobalFunctions.countDown($p.data_cadastro),
						right: 5
					},
					container_video:{

					},
					video_webview:{
						visible:false,
						enableZoomControls:false,
						scalesPageToFit:true,
						scrollsToTop:false,
						showScrollbars:false,
						url : $p.video_id ? 'http://www.youtube.com/embed/'+$p.video_id+'?autoplay=1&autohide=1&cc_load_policy=0&color=white&controls=0&fs=0&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0' : '',
						height:$p.video_id ? 170 : 0

					},
					container_foto:{
						height:$p.video_id ? 0 : Ti.UI.SIZE
					},
					foto:{
						backgroundColor:'#d6d6d6',
						defaultImage:"/images/sem-foto.png"
					},
					foto_video:{
						backgroundColor:'#d6d6d6',
						defaultImage:"/images/sem-foto.png",
						visible:false
					},
					video_icon:{
						visible:false
					},
					like:{
						text:$p.likes_db != 0 ? $p.likes_db+" "+L('likes') : "",
						post_id: $p.post_id,
						cadastro_id: $p.cadastro_id,
						curtiu: $p.curtiu
					},
					isliked:{
						 image:$p.curtiu == true ? "/images/icon-curtido.png" : "/images/icon-curtir.png"
					},
					comentarios:{
						text:$p.comentarios_db != 0 ? $p.comentarios_db+" "+L('comment') : ""
					},
					compartilhar:{
						text:$p.compartilhamentos_db != 0 ? $p.compartilhamentos_db+" "+L('share') : ""
					},
					container_post:{

					},
					dados:{
						post_id: $p.post_id,
						tokenpush:$p.tokenpush,
						video_id:$p.video_id,
						video_tipo:$p.video_tipo,
						facebook_usuario:$p.facebook_usuario
					},
					botao:{
						visible:$p.cadastro_id == DataPerfil.cadastro_id ? true : false
					},
					share:{
						text:$p.nome_compartilhado == null ? "" : $p.nome_compartilhado,
						cadastro_id_compartilhado: $p.cadastro_id_compartilhado == null ? "" : $p.cadastro_id_compartilhado,
						post_id:$p.post_id == null ? "" : $p.post_id,
						cadastro_id:$p.cadastro_id == null ? "" : $p.cadastro_id,
						texto:$p.texto == null ? "" : $p.texto,
						foto:$p.arquivo == null ? "" : $p.arquivo,
						video_id:$p.video_id,
						video_tipo:$p.video_tipo,
						facebook_usuario:$p.facebook_usuario
					},
				};
			if($p.arquivo && OS_ANDROID){
				$newImage = $p.arquivo.replace('300x170', "300xprop");
				item.foto.image = GlobalFunctions.getURL($newImage,$p.genero);
			}else if($p.arquivo && OS_IOS){
				$newImage = $p.arquivo.replace('300x170', "300xprop");
				item.foto.image = GlobalFunctions.getURL($newImage,$p.genero);
			}else{
				item.foto.image = "";
			}
			if($p.video_id){
				//item.container_post.height = 360;
				item.foto_video.height = 170;
				if($p.video_tipo == 'youtube' || $p.video_tipo == ''){
					item.foto_video.image = "http://img.youtube.com/vi/"+$p.video_id+"/0.jpg";
				}else if($p.video_tipo == 'facebook'){
					item.foto_video.image = "https://graph.facebook.com/"+$p.video_id+"/picture";
				}
				//item.foto.height = 0;
				item.foto_video.visible = true;
				item.video_icon.visible = true;
			}else if($p.arquivo == null){
				item.foto.height = 0;
				//item.container_post.height = 180;
				item.foto_video.height = 0;
				item.video_icon.height = 0;
			}else{
				//item.container_post.height = 290;
				//item.foto.height = 140;
				item.foto_video.height = 0;
				item.video_icon.height = 0;
			}
			if(!$p.texto){
				item.texto.height = 0;
				//item.container_post.height = 290;
			}
			if($p.url_texto && $p.texto){
				/*var attr = Titanium.UI.createAttributedString({
				    text: $p.texto,
				    attributes: [
				        {
				            type: Titanium.UI.ATTRIBUTE_LINK,
	                		value: $p.url_texto,
	                		range: [0, $p.texto.length]
				        }
				    ]
				});
				item.texto.attributedString = attr;*/
			}

			$itensTemplate.push(item);
		}
		}
		$.iterator.setItems($itensTemplate);
	}
}

function onPull(e){
	$.parent.ajaxDefaultLoader({
		onComplete:function($data){
			$qtdePerPage = 10;
			$page = 1;
			$qtde = 10;

			$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});

			getMyTimeline($data);
			if($data.timeline && $data.perfil){
				$data = $data.perfil;
			}
			$.ptr.hide();
		},
		data:{
			cadastro_id_pessoa: $makercadastro_id_pessoa ? $makercadastro_id_pessoa : "",
			timeline: $makertimeline ? "true" : ""
		},
		action:$makerAction,
		onStart:function(){

		}
	});
}
function onMarker(e){
	$page = $page+1;
	$qtdePerPage = ($qtde * $page);
	$.parent.ajaxDefaultLoader({
		onStart:function(){
			$.loaderResultado.start();
			$.loaderResultado.visible = true;
		},
		onComplete:function($data){
			$.loaderResultado.stop();
			$.loaderResultado.visible = false;
			getMyTimeline($data,true);
		},
		action:$makerAction,
		data:{
			limit:$qtde,
			offset:($qtde * ($page - 1)),
			cadastro_id_pessoa: $makercadastro_id_pessoa ? $makercadastro_id_pessoa : null
		}
	});
}

exports.startup = function($data){
	getMyTimeline($data,false);
	if($data.timeline && $data.perfil){
		$data = $data.perfil;
	}
	$controllerHeader.parent = $.parent;
	$controllerHeader.startup($data);

};

exports.argumento = function($data){
	$makerAction = $data.action;
	$makercadastro_id_pessoa = $data.cadastro_id_pessoa;
	$makertimeline = $data.timeline;
};

exports.update = function($args){
	onPull({});
};

exports.destroy = function(){
	$controllerHeader.destroy();
	if($controllerHeader != null && $controllerHeader.destroy != null)$controllerHeader.destroy();
};
