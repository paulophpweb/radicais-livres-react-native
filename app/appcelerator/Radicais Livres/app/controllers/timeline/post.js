exports.baseController = "base";
exports.controllerName = "/timeline/post";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;

function setaData($data){
	var $texto_acao = L('postou');
	if($data.nome_compartilhado != null){
		$texto_acao = L('shared_for');
	}else if($data.arquivo != null){
		$texto_acao = L('add_photo');
	}
	$.pessoa_foto.defaultImage=GlobalFunctions.getImageGenero($data.genero);
	$.pessoa_foto.image = GlobalFunctions.getURL($data.foto,$data.genero);
    $.pessoa_nome.text = $data.nome;
    if($data.arquivo)
   		$.pessoa_arquivo.image = GlobalFunctions.getURL($data.arquivo.replace("/300x170/","/300xprop/"));
		$.pessoa_adcionou_foto.text = $texto_acao;
	if($data.data_cadastro)
		$.pessoa_horario_compartilhamento.text = GlobalFunctions.getDataBr($data.data_cadastro);
	if($data.texto)
		$.pessoa_texto.text = $data.texto;
	if($data.nome_compartilhado)
		$.share.text = $data.nome_compartilhado;
}	
exports.startup = function($data){
	if($data){
		setaData($data[0]);
	}
};
exports.update = function(){
	Ti.API.error("fechou post");
};

exports.destroy = function(){
	
};
