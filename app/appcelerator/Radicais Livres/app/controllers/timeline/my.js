exports.baseController = "base";
exports.controllerName = "/timeline/teste";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
$.listposts.parent = this;

function clickInfo(e){
	clickFooter({controller:e.source.controller}); 
}

function clickFooter(e){
	_this.openWindow(e.controller, false, e);
}

exports.startup = function($data){
	//$.footer.on("click-footer", clickFooter);
	$.listposts.startup($data);
	$.listposts.argumento(args);
};
exports.update = function(){
	$.listposts.update();
};
exports.destroy = function(){
	//$.footer.off("click-footer", clickFooter);
	$.listposts.destroy();
};