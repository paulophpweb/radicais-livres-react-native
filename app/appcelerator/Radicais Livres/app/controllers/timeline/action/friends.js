exports.baseController = "base";
exports.controllerName = "/perfil/friends";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
var $itensTemplate = [];

function getPerfis($data){
	if($data){
		var $tamanho = $data.length;
		if($tamanho > 0){
			for(var a= 0; a<$tamanho; a++){
				
				var $p = $data[a];
				
				var item = {
						user:{
							cadastro_id_pessoa:$p.cadastro_id
						},
						nome:{
							text:$p.nome+" "+$p.sobrenome
						},
						img:{
							defaultImage:GlobalFunctions.getImageGenero($p.genero),
							image:$p.foto ? GlobalFunctions.getURL($p.foto,$p.genero) : GlobalFunctions.getURL($p.foto,$p.genero)
						}
					};
				
				$itensTemplate.push(item);
			}
		}
		$.iterator.setItems($itensTemplate);
	}		
}

function clickItem(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	if(e.bindId == "img" || e.bindId == "nome"){
		_this.openWindow("timeline/people", false, {cadastro_id_pessoa:item.user.cadastro_id_pessoa, action: "getperfil", timeline:"true"});
	}
	
}

exports.startup = function($data){
	perfis = $data;
	getPerfis($data);
	
};