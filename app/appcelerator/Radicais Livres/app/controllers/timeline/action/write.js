exports.baseController = "base";
exports.controllerName = "/timeline/escrever";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
var $param_foto = null;
var $param_texto = null;

function clickRemoveFoto(e){
	$.texto.blur();
	$.close.visible = false;
	$.close.touchEnabled = false;
	$.ctrl_expand.visible = false;
	$.ctrl_expand.touchEnabled = false;
	$.foto.images = "/images/check-box.png";
	$.container_text.top = 0;
	$.container_text.expanded = null;
	$.container_text.top_oud = null;
	ModalGalery.destroy();
	$param_foto = "";
}
function clickCtrlExpand(e){
	$.texto.blur();
	if($.container_text.expanded){
		$.container_text.expanded = false;
		$.container_text.top = 50;
		$.ctrl_expand.backgroundImage="/images/expand.png";
	}else{
		$.container_text.expanded = true;
		$.container_text.top = $.container_text.top_oud;
		$.ctrl_expand.backgroundImage="/images/comprimir.png";
	}
}
function changeTextArea(e){
	if($.container_text.expanded){
		$.container_text.expanded = false;
		$.container_text.top = 50;
		$.ctrl_expand.backgroundImage="/images/expand.png";
	}
}
function clickFoto(e){
	$.texto.blur();
	ModalGalery.show().onComplete(function(){
		$.foto.image = ModalGalery.getBlog();
		$param_foto = ModalGalery.getFile();
		var $w = ModalGalery.getWidth();
		var $h = ModalGalery.getHeight();
		var $r = $h/$w;
		var $nw = SizeView.pw(30);
		var $nh = $nw*$r;
		$.texto.blur();
		
		$.close.visible = true;
		$.close.touchEnabled = true;
		
		$.ctrl_expand.visible = true;
		$.ctrl_expand.touchEnabled = true;
		
		$.container_text.expanded = true;
		$.container_text.top_oud = $.container_text.top = $nh;
		
		$.ctrl_expand.backgroundImage="/images/comprimir.png";
	});
}
$.photo_perfil.image = GlobalFunctions.getURL(DataPerfil.foto,DataPerfil.genero);

function ClickPublicar(){
	$param_texto = $.texto.value;
	var $video_id = GlobalFunctions.getYoutubeId($param_texto);
	$video_id_face = null;
	//var $video_id_face = GlobalFunctions.getFacebookVideoId($param_texto);
	var $url_in_text = GlobalFunctions.getUrlInBlock($param_texto);
	
	if($video_id && $video_id != ''){
		var $data = {
			texto: $param_texto,
			arquivo: $param_foto,
			video_id:$video_id,
			video_tipo: 'youtube',
			//facebook_usuario: $video_id_face.facebook_usuario ? $video_id_face.facebook_usuario : '',
		};
	}else if($url_in_text){
		var $data = {
			texto: $param_texto,
			arquivo: $param_foto,
			url_texto: $url_in_text
		};
	}else{
		var $data = {
			texto: $param_texto,
			arquivo: $param_foto
		};
	}
	
	_this.ajaxDefaultLoader({
		onComplete:function($data){
			ModalGalery.destroy();
			$.dialog.alert({mensagem:"post_inserido",onComplete:Navigate.backWindow});
		},
		action:"post_insert",
		data:$data
	});
}


exports.startup = function(){
	if(args.open_galeria)clickFoto(null);
};

exports.update = function(){
	
};