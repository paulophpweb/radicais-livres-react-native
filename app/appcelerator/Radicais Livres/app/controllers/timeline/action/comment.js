exports.baseController = "base";
exports.controllerName = "/timeline/comentar";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
var comentarios = [];
var $itensTemplate = [];
var $qtdePerPage = 10;
var $page = 1;
var $qtde = 10;

function getComments($data){
	$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
	if($data){
		var $tamanho = $data.length;
		if($tamanho > 0){
			for(var a= 0; a<$tamanho; a++){
				var $p = $data[a];
				var item = {
						nome:{
							text:$p.nome+" "+$p.sobrenome
						},
						user:{
							defaultImage:GlobalFunctions.getImageGenero($p.genero),
							image:$p.foto ? GlobalFunctions.getURL($p.foto, $p.genero) : GlobalFunctions.getURL(DataPerfil.foto,DataPerfil.genero),
							cadastro_id_pessoa: $p.cadastro_id_pessoa
						},
						texto:{
							text:$p.texto
						},
						like:{
							text:$p.likes ? $p.likes+" "+L('likes') : "",
							post_id: $p.post_id,
							cadastro_id: $p.cadastro_id,
							curtiu: $p.curtiu,
							comment_id: $p.comment_id
						},
						isliked:{
							 image:$p.curtiu ? "/images/icon-curtido.png" : "/images/icon-curtir.png"
						},
						botao:{
							 visible:$p.cadastro_id == DataPerfil.cadastro_id ? true : false
						}
					};

				$itensTemplate.push(item);
			}
		}
		$.iterator.setItems($itensTemplate);
	}
}

function clickItem(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	if(item.isliked && e.bindId == "like_button"){
		var $obj = item;
		var qtdAtual = item.like.text.replace(" "+L('likes'),"");
		if(!$obj.like.curtiu && item.isliked.image != "/images/icon-curtido.png"){
			var $data = {};
			$data.post_id = $obj.like.post_id;
			$data.cadastro_id_amizade = $obj.user.cadastro_id_pessoa;
			$data.comment_id = $obj.like.comment_id;
			$data.tokenpushamizade = DataPerfil.tokenpush;
			$data.notificacao = "Seu amigo "+DataPerfil.nome+" curtiu seu comentário.";
			$.ajaxDefaultLoader({
				onComplete:function($data){
					onPull({});
				},
				action:"curtircomment",
				data:$data
			});
		// implementar o discurtir
		}else{

		}
	}else if(e.bindId == "nome" || e.bindId == "user"){

		_this.openWindow("timeline/people", false, {cadastro_id_pessoa:item.user.cadastro_id_pessoa, action: "getperfil", timeline:"true"});
	}else if(e.bindId == "botao" || e.bindId == "viewBotao"){
		$.dialog.confirm({
			mensagem:"excluir_mensagem",
			confirmar:"sim",
			onConfirm:function(){
				_this.ajaxDefaultLoader({
					onComplete:function($data){
						Ti.API.error(JSON.stringify($data));
						$.dialog.alert({mensagem:'del_sucesso',onComplete:onPull({})});
					},
					action:"removercomentario",
					data:{
						comment_id:item.like.comment_id,
						post_id:item.like.post_id
					}
				});
			}
		});
	}

}

function clickEnviar(e){
	var $data = {};
	$data.post_id = args.post_id;
	$data.texto = $.textcomment.value;
	$data.tokenpushamizade = args.tokenpushamizade;
	$data.notificacao = "Seu amigo "+DataPerfil.nome+" comentou no seu post.";
	$data.cadastro_id_amizade = args.cadastro_id_pessoa;
	$.ajaxDefaultLoader({
		onComplete:function($data){
			/*$.list.scrollToItem(0,1,{
				animated:true
			});*/
			// reload nos dados
			onPull({});
		},
		action:"comment_insert",
		data:$data
	});
}

function onMarker(e){
	$page = $page+1;
	$qtdePerPage = ($qtde * $page);
	_this.ajaxDefaultLoader({
		onStart:function(){
			$.loaderResultado.start();
			$.loaderResultado.visible = true;
		},
		onComplete:function($data){
			$.loaderResultado.stop();
			$.loaderResultado.visible = false;
			getComments($data);
		},
		action:"getcomentarios",
		data:{
			limit:$qtde,
			offset:($qtde * ($page - 1)),
			post_id:args.post_id,
			tokenpushamizade:args.tokenpushamizade,
			cadastro_id_pessoa:args.cadastro_id_pessoa
		}
	});
}
function onPull(e){
	$itensTemplate = [];
	_this.ajaxDefaultLoader({
		onComplete:function($data){
			$qtdePerPage = 10;
			$page = 1;
			$qtde = 10;

			$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
			getComments($data);
			$.ptr.hide();
		},
		action:"getcomentarios",
		data:{
			post_id:args.post_id,
			tokenpushamizade:args.tokenpushamizade,
			cadastro_id_pessoa:args.cadastro_id_pessoa
		},
		onStart:function(){

		}
	});

}

exports.startup = function($data){
	Ti.API.error(JSON.stringify(args));
	comentarios = $data;
	getComments($data);

};
exports.update = function(){

};
