exports.baseController = "base";
exports.controllerName = "menu/lista";
$.container.add($.conteudo);
var _this = this;
var $list = [{
	icone: {image:'/images/icon/icon_timeline.png',controller:"timeline", action:"timeline", type:"window"},
	texto: {text:L("mb_timeline"),controller:"timeline", action:"timeline", type:"window"},
	nav:{controller:"timeline", action:"timeline", type:"window"}
},{
	icone: {image:'/images/icon/icon_perfil.png',controller:"timeline/my", type:"window",action: "my_gettimeline"},
	texto: {text:L("mb_perfil"),controller:"timeline/my", type:"window",action: "my_gettimeline"},
	nav:{controller:"timeline/my", type:"window",action: "my_gettimeline"}
},{
	icone: {image:'/images/icon/icon_foto.png',controller:"perfil/sobre/foto", action:"timelineperfilfotos", type:"window"},
	texto: {text:L("mb_fotos"),controller:"perfil/sobre/foto", action:"timelineperfilfotos", type:"window"},
	nav:{controller:"perfil/sobre/foto", action:"timelineperfilfotos", type:"window"}
},{
	icone: {image:'/images/icon/icon_amigo.png',controller:"perfil/sobre/amigo", type:"window", action:"listaramigos"},
	texto: {text:L("mb_amigos"),controller:"perfil/sobre/amigo", type:"window", action:"listaramigos"},
	nav:{controller:"perfil/sobre/amigo", type:"window", action:"listaramigos"}
},{
	icone: {image:'/images/icon/icon_evento.png',controller:"evento/proximo", type:"window",action:"geteventoproximo", tipo: "proximo"},
	texto: {text:L("mb_eventos"),controller:"evento/proximo", type:"window",action:"geteventoproximo", tipo: "proximo"},
	nav:{controller:"evento/proximo", type:"window",action:"geteventoproximo", tipo: "proximo"}
},{
	icone: {image:'/images/icon/icon_mapa.png',controller:"mapa/index", type:"window"},
	texto: {text:L("mb_mapas"),controller:"mapa/index", type:"window"},
	nav:{controller:"mapa/index", type:"window"}
},{
	icone: {image:'/images/icon/icon_loja.png',controller:"http://www.radicaislivres.com/loja", type:"url"},
	texto: {text:L("mb_loja"),controller:"http://www.radicaislivres.com/loja", type:"url"},
	nav:{controller:"http://www.radicaislivres.com/loja", type:"url"}
},{
	icone: {image:'/images/icon/icon_mensagem.png',controller:"perfil/mensagem", type:"window", action:"getmensagem"},
	texto: {text:L("mb_mensagens"),controller:"perfil/mensagem", type:"window", action:"getmensagem"},
	nav:{controller:"perfil/mensagem", type:"window", action:"getmensagem"}//controller:"perfil/mensagem", type:"window"}
},{
	icone: {image:'/images/icon/icon_cadeado.png',controller:"perfil/privacidade", type:"window"},
	texto: {text:L("mb_privacidade"),controller:"perfil/privacidade", type:"window"},
	nav:{controller:"perfil/privacidade", type:"window"}//controller:"perfil/privacidade", type:"window", 
},{
	icone: {image:'/images/icon/icon_lapis.png',controller:"perfil/sobre/sobre", type:"window"},
	texto: {text:L("mb_atualizar"),controller:"perfil/sobre/sobre", type:"window"},
	nav:{controller:"perfil/sobre/sobre", type:"window"}
},{
	icone: {image:'/images/icon/icon_folha.png',controller:"perfil/termo", type:"window"},
	texto: {text:L("mb_termos_politicas"),controller:"perfil/termo", type:"window"},
	nav:{controller:"perfil/termo", type:"window"}
},{
	icone: {image:'/images/icon/icon_rullers.png',controller:"menu/configuracao", type:"window"},
	texto: {text:L("mb_configuracoes"),controller:"menu/configuracao", type:"window"},
	nav:{controller:"menu/configuracao", type:"window"}
},{
	icone: {image:'/images/icon/icon_logout.png',controller:"login/index", type:"window"},
	texto: {text:L("mb_sair"),controller:"login/index", type:"window"},
	nav:{controller:"login/index", type:"window"}
}];

$.iterator.setItems($list);
function clickItem(e){
	var item = e.section.getItemAt(e.itemIndex);
	var $prop = item[e.bindId];
	if($prop != null){
		var $controller = $prop.controller;
		var $type = $prop.type;
		if($type == "window" && $controller != null)_this.openWindow($controller, false, $prop);
		else if($type == "url" && $controller != null)Ti.Platform.openURL($controller);
	}
}
function clickNav(e){
	_this.openWindow(e.controller, false, e);
}
exports.startup = function(){
	//$.lista.on("click-footer", clickNav);
};
exports.destroy = function(){
	//$.lista.off("click-footer", clickNav);
}; 
