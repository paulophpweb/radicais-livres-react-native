exports.baseController = "base";
exports.controllerName = "/menu/bloco";
$.container.add($.conteudo);
$.header.parent = $;
$.header.parentController = this;
var args = arguments[0] || {};
var _this = this;

var $list = [{
	icone1: {image:'/images/icon-time-line.png',controller:"timeline", action:"timeline", type:"window"},
	texto1: {text:L('menu_timeline'),controller:"timeline", action:"timeline", type:"window"},
	nav1:{controller:"timeline", action:"timeline", type:"window"},
	
	icone2: {image:'/images/icon-perfil.png',controller:"timeline/my", type:"window",action: "my_gettimeline"},
	texto2: {text:L('menu_perfil'),controller:"timeline/my", type:"window",action: "my_gettimeline"},
	nav2:{controller:"timeline/my", type:"window",action: "my_gettimeline"},
},{
	icone1: {image:'/images/icon-fotos.png',controller:"perfil/sobre/foto", action:"timelineperfilfotos", type:"window"},
	texto1: {text:L('menu_fotos'),controller:"perfil/sobre/foto", action:"timelineperfilfotos", type:"window"},
	nav1:{controller:"perfil/sobre/foto", action:"timelineperfilfotos", type:"window"},
	
	icone2: {image:'/images/icon-amigos.png',type:"window",action:"listaramigos",controller:"perfil/sobre/amigo"},
	texto2: {text:L('menu_amigos'),type:"window",action:"listaramigos",controller:"perfil/sobre/amigo"},
	nav2:{controller:"perfil/sobre/amigo", type:"window", action:"listaramigos"},
},{
	icone1: {image:'/images/icon-eventos.png',controller:"evento/proximo", type:"window",action:"geteventoproximo", tipo: "proximo"},
	texto1: {text:L('menu_eventos'),controller:"evento/proximo", type:"window",action:"geteventoproximo", tipo: "proximo"},
	nav1:{controller:"evento/proximo", type:"window",action:"geteventoproximo", tipo: "proximo"},
	
	icone2: {image:'/images/icon-mapas.png',controller:"mapa/index", type:"window"},
	texto2: {text:L('menu_mapas'),controller:"mapa/index", type:"window"},
	nav2:{controller:"mapa/index", type:"window"},
},{
	icone1: {image:'/images/icon-loja.png',controller:"http://www.radicaislivres.com/loja", type:"url"},
	texto1: {text:L('menu_loja_radical'),controller:"http://www.radicaislivres.com/loja", type:"url"},
	nav1:{controller:"http://www.radicaislivres.com/loja", type:"url"},
	
	icone2: {image:'/images/icon-configuracoes.png',controller:"menu/configuracao", type:"window"},
	texto2: {text:L('menu_configuracoes'),controller:"menu/configuracao", type:"window"},
	nav2:{controller:"menu/configuracao", type:"window"},
}];

$.iterator.setItems($list);


function searchHeader(e){
	if(e.value != ""){
		$.openWindow("perfil/sobre/amigo/resultado", false, {
			nome:e.value,
			action:"procurar"
		});
	}else $.dialog.alert({mensagem:"search_vazio"});
}

function clickChat(e){
	$.openWindow(e.controller, false,{action:e.action});
}

function updateNotificacao(){
	$itensTemplate = [];
	_this.ajaxDefaultLoader({
		onComplete:function($data){
			$.header.addQtdeNot($data);
			$.footer.addQtdeNotGeral($data);
		},
		action:"getnotificacaoall",
		onStart:function(){
			
		}
	});
}

function verificaRelacionamento(){
	/*setTimeout(function(){
		_this.ajaxDefaultLoader({
			onComplete:function($data){
				$.dialog.confirm({
					mensagem:"Joisiney deseja se relacionar com você, deseja aceitar 'Em Côrte'?",
					textLivre:true,
					confirmar:"tentar_novamente",
					onConfirm:function(){
						//Ajax.post(e);
					}
				});
			},
			action:"getnotificacaoall",
			onStart:function(){
				
			}
		});
		
	},1000);*/
	
}


function clickItem(e){
	var item = e.section.getItemAt(e.itemIndex);
	var $prop = item[e.bindId];
	if($prop != null){
		var $controller = $prop.controller;
		var $type = $prop.type;
		if($type == "window" && $controller != null)_this.openWindow($controller, false, $prop);
		else if($type == "url" && $controller != null)Ti.Platform.openURL($controller);
	}else{
		$.header.fecharTeclado();
	}
}

function clickNav(e){
	_this.openWindow(e.controller, false, e);
}

exports.startup = function(){
	$.footer.on("click-footer", clickNav);
	$.header.on("search-header", searchHeader);
	$.header.on("search-header-sobre", clickChat);
	updateNotificacao();
	verificaRelacionamento();
};
exports.destroy = function(){
	$.footer.off("click-footer", clickNav);
	$.header.off("search-header", searchHeader);
	$.header.off("search-header-sobre", clickChat);
};
exports.update = function(){
	if(DataPerfil == null || DataPerfil.nome == null){
		$.dialog.alert({
			mensagem:"logout",
			onComplete:function(){
				_this.openWindow("login/index", true);
			}
		});
	}else{
		updateNotificacao();
		verificaRelacionamento();
	}
	
};
exports.boot = function(){
	Navigate.startNavigation($.index, exports, _this);
};