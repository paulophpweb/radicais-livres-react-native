var args = arguments[0] || {};
exports.baseController = "base";
exports.controllerName = "menu/configuracao";
$.container.add($.conteudo);
var _this = this;
var $list = [{
	icone: {image:'/images/icon/icon_rullers.png',controller:"config/geral", type:"window"},
	texto: {text:"Geral",controller:"config/geral", type:"window"},
	nav:{controller:"config/geral", type:"window"}
},{
	icone: {image:'/images/icon/icon_negacao.png',controller:"perfil/sobre/amigo", type:"window", action:"listaramigos"},
	texto: {text:"Lista bloqueados",controller:"perfil/sobre/amigo", type:"window", action:"listaramigos"},
	nav:{controller:"perfil/sobre/amigo", type:"window", action:"listaramigos"}
}/*,{
	icone: {image:'/images/icon/icon_cadeado.png',controller:"timeline/my", type:"window",action: "my_gettimeline"},
	texto: {text:"Segurança",controller:"timeline/my", type:"window",action: "my_gettimeline"},
	nav:{controller:"timeline/my", type:"window",action: "my_gettimeline"}
}*/];
if(DataPerfil.is_root == 1){
	$list.splice (1, 1);
}
$.iterator.setItems($list);
function clickItem(e){
	var item = e.section.getItemAt(e.itemIndex);
	var $prop = item[e.bindId];
	if($prop != null){
		var $controller = $prop.controller;
		var $type = $prop.type;
		if($type == "window" && $controller != null)_this.openWindow($controller, false, $prop);
		else if($type == "url" && $controller != null)Ti.Platform.openURL($controller);
	}
}
function clickNav(e){
	_this.openWindow(e.controller, false, e);
}
exports.startup = function(){
	//$.lista.on("click-footer", clickNav);
};
exports.destroy = function(){
	//$.lista.off("click-footer", clickNav);
}; 
