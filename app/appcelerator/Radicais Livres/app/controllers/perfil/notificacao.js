exports.baseController = "base";
exports.controllerName = "/perfil/notificacoes";
$.container.add($.conteudo);

var _this = this;
var $itensTemplate = [];
var $qtdePerPage = 10;
var $page = 1;
var $qtde = 10;

function gridNotificacao($data){
	$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
	if($data){
		var $tamanho = $data.length;
		if($tamanho > 0){
			for(var a= 0; a<$tamanho; a++){
				var $p = $data[a];
				var item = {
						nome:{
							text:$p.nome_amigo+" "+$p.sobrenome_amigo,
							cadastro_id_pessoa:$p.cadastro_id_amizade
						},
						img:{
							defaultImage:GlobalFunctions.getImageGenero($p.genero_amigo),
							image:GlobalFunctions.getURL($p.foto_amigo,$p.genero_amigo)
						},
						texto:{
							text:$p.texto.replace("{{obs}}","")
						},
						data:{
							text:GlobalFunctions.getDataBr($p.data_cadastro)
						},
					};
				
				$itensTemplate.push(item);
			}
		}
		$.iterator.setItems($itensTemplate);
	}
}

function clickItem(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	if(e.bindId == "img" || e.bindId == "nome"){
		_this.openWindow("timeline/people", false, {
			cadastro_id_pessoa:item.nome.cadastro_id_pessoa,
			timeline:"true",
			action:"getperfil"
		});
	}
}


function onMarker(e){
	$page = $page+1;
	$qtdePerPage = ($qtde * $page);
	_this.ajaxDefaultLoader({
		onStart:function(){
			$.loaderResultado.start();
			$.loaderResultado.visible = true;
		},
		onComplete:function($data){
			$.loaderResultado.stop();
			$.loaderResultado.visible = false;
			gridNotificacao($data);
		},
		action:"getnotificacao",
		data:{
			limit:$qtde,
			offset:($qtde * ($page - 1))
		}
	});
}
function onPull(e){
	$itensTemplate = [];
	_this.ajaxDefaultLoader({
		onComplete:function($data){
			$qtdePerPage = 10;
			$page = 1;
			$qtde = 10;
			
			$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
			gridNotificacao($data);
			$.ptr.hide();
		},
		action:"getnotificacao",
		onStart:function(){
			
		}
	});
	
}

function clickNav(e){
	_this.openWindow(e.controller, true, e);
}

exports.startup = function($data){
	gridNotificacao($data);
	//$.notificacoes.on("click-footer", clickNav);
};
exports.destroy = function(){
	//$.notificacoes.off("click-footer", clickNav);
};