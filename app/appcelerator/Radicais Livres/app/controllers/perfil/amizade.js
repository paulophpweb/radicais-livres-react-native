exports.baseController = "base";
exports.controllerName = "/perfil/amizade";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
function popularDataGrid($data){
	var $d = {};
	_.each($data, function($value, $key){
		$d[$value.tipo] = $d[$value.tipo] || [];
		var $nomePessoa = $value.nome+" "+$value.sobrenome+($value.funcao ? " ("+$value.funcao+")" : "");
		var $obTmp = {
			foto:{image:GlobalFunctions.getURL($value.foto, $value.genero)},
			nome:{text:StringUtil.abreviar($nomePessoa, 25, "...")},
			estado_cidade: {
				text: StringUtil.abreviar($value.estado+" - "+$value.cidade+"", 25, "...") 
			},
			relacionamento: {
				text: $value.nome_relacionado ? StringUtil.abreviar($value.relacionamento+" com "+$value.nome_relacionado, 25, "...") : $value.relacionamento
			},
			botao:{
				image:"/images/solicitacoes.png",
				cadastro_id_solicitado:$value.cadastro_id,
				cadastro_id:$value.cadastro_id,
				tokenpush:$value.tokenpush,
				udid:$value.udid,
				solicitacao_id:$value.solicitacao_id,
				tipo:$value.tipo,
				action: "getperfil"
			},

		};
		if($value.tipo == "solicitacao"){
			$obTmp.bloquear = {
				visible:true,
				image:"/images/close.png",
				right:10
			};
			$obTmp.botao.right = 48;
		}
		$d[$value.tipo].push($obTmp);
	});
	for(var a in $d){
		if($[a] != null)$[a].setItems($d[a]);
	}
}
function clickItem(e){
	var section = $.list.sections[e.sectionIndex];
	var items = section.getItemAt(e.itemIndex);
	var iten = items[e.bindId];
	if(e.bindId == "viewBotao" || e.bindId == "botao"){
		if(items.botao.tipo == "solicitacao"){
			_this.ajaxDefaultLoader({
				onComplete:function($data){
					$.dialog.alert({mensagem:$data.msg});
					if(OS_IOS)e.section.deleteItemsAt(e.itemIndex, 1, {animationStyle: Titanium.UI.iPhone.RowAnimationStyle.LEFT});
		    		else if(OS_ANDROID)e.section.deleteItemsAt(e.itemIndex, 1);
				},
				action:"aceitaramizade",
				data:{
					cadastro_id_solicitado:items.botao.cadastro_id_solicitado,
					cadastro_id_amizade:items.botao.cadastro_id_solicitado,
					solicitacao_id:items.botao.solicitacao_id,
					notificacao: ""+DataPerfil.nome+" aceitou sua amizade."
				}
			});
		}else if(items.botao.tipo == "sugestao"){
			_this.ajaxDefaultLoader({
				onComplete:function($data){
					$.dialog.alert({mensagem:$data.msg});
					if(OS_IOS)e.section.deleteItemsAt(e.itemIndex, 1, {animationStyle: Titanium.UI.iPhone.RowAnimationStyle.LEFT});
		    		else if(OS_ANDROID)e.section.deleteItemsAt(e.itemIndex, 1);
				},
				action:"solicitaramizade",
				data:{
					cadastro_id_solicitado:items.botao.cadastro_id_solicitado,
					cadastro_id_amizade:items.botao.cadastro_id_solicitado,
					notificacao: ""+DataPerfil.nome+" solicitou sua amizade."
				}
			});
		}else $.dialog.alert("tipoamizadeinvalido");
	}else if(e.bindId == "foto" || e.bindId == "nome"){
		_this.openWindow("timeline/people", false, {
			action:items.botao.action,
			cadastro_id_pessoa:items.botao.cadastro_id_solicitado,
		    timeline: "true"
		});
	}else if(e.bindId == "viewBloquear" || e.bindId == "bloquear"){
		$.dialog.confirm({
			mensagem:"excluir_amigo",
			confirmar:"sim",
			onConfirm:function(){
				_this.ajaxDefaultLoader({
					onComplete:function($data){
						$.dialog.alert({mensagem:$data.msg});
						if(OS_IOS)e.section.deleteItemsAt(e.itemIndex, 1, {animationStyle: Titanium.UI.iPhone.RowAnimationStyle.LEFT});
			    		else if(OS_ANDROID)e.section.deleteItemsAt(e.itemIndex, 1);
					},
					action:"removeramizade",
					data:{
						cadastro_id_amizade:items.botao.cadastro_id_solicitado,
						action: "getperfil"
					}
				});
			}
		});
	}
}
function clickNav(e){
	_this.openWindow(e.controller, true, e);
}
function clickPesquisa(e){
	_this.openWindow("perfil/sobre/amigo/procurar", false, e);
}
function onPull(e){
	_this.ajaxDefaultLoader({
		onComplete:function($data){
			popularDataGrid($data);
			$.ptr.hide();
		},
		action:"solicitacaosugestaodeamizade",
		onStart:function(){

		}
	});

}
exports.startup = function($data){
	popularDataGrid($data);
	//$.amizade.on("click-footer", clickNav);
	if(DataPerfil.is_root == 1){
		$.mensagemAdmin.visible = true;
	}
};
exports.update = function(){
	//alert("update amizade");
};
exports.destroy = function(){
	//$.amizade.off("click-footer", clickNav);
};
