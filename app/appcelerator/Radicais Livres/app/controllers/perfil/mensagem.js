exports.baseController = "base";
exports.controllerName = "/perfil/mensagem";
$.container.add($.conteudo);
var _this = this;
var $itensTemplate = [];
var $qtdePerPage = 10;
var $page = 1;
var $qtde = 10;

function gridMensagens($data){
	$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
	if($data){
		var $tamanho = $data.length;
		if($tamanho > 0){
			for(var a= 0; a<$tamanho; a++){
				var $p = $data[a];
				var item = {
						nome:{
							text:$p.nome+" "+$p.sobrenome,
							cadastro_id_pessoa:$p.cadastro_id_amizade
						},
						img:{
							defaultImage:GlobalFunctions.getImageGenero($p.genero),
							image:GlobalFunctions.getURL($p.foto,$p.genero)
						},
						texto:{
							text:$p.texto
						},
						horario_cadastro:{
							text:GlobalFunctions.getDataBr($p.data_cadastro)
						},
						mensagem_id: $p.mensagem_id
					};

				$itensTemplate.push(item);
			}
		}
		$.iterator.setItems($itensTemplate);
	}
}

function clickItem(e){
	Ti.API.error(JSON.stringify(e.bindId));
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	if(e.bindId == "img" || e.bindId == "nome"){
		_this.openWindow("timeline/people", false, {
			cadastro_id_pessoa:item.nome.cadastro_id_pessoa,
			timeline:"true",
			action:"getperfil"
		});

	}else if(e.bindId == "botao" || e.bindId == "viewBotao"){

		$.dialog.confirm({
			mensagem:"excluir_mensagem",
			confirmar:"sim",
			onConfirm:function(){
				_this.ajaxDefaultLoader({
					onComplete:function($data){
						$.dialog.alert({mensagem:"Deletado com sucesso!"});
						if(OS_IOS)e.section.deleteItemsAt(e.itemIndex, 1, {animationStyle: Titanium.UI.iPhone.RowAnimationStyle.LEFT});
			    		else if(OS_ANDROID)e.section.deleteItemsAt(e.itemIndex, 1);
					},
					action:"removermensagem",
					data:{
						mensagem_id:item.mensagem_id
					}
				});
			}
		});
	}
}

function clickNav(e){
	_this.openWindow(e.controller, true, e);
}
function onMarker(e){
	$page = $page+1;
	$qtdePerPage = ($qtde * $page);
	_this.ajaxDefaultLoader({
		onStart:function(){
			$.loaderResultado.start();
			$.loaderResultado.visible = true;
		},
		onComplete:function($data){
			$.loaderResultado.stop();
			$.loaderResultado.visible = false;
			gridMensagens($data);
		},
		action:"getmensagem",
		data:{
			limit:$qtde,
			offset:($qtde * ($page - 1))
		}
	});
}
function onPull(e){
	$itensTemplate = [];
	_this.ajaxDefaultLoader({
		onComplete:function($data){
			$qtdePerPage = 10;
			$page = 1;
			$qtde = 10;

			$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
			gridMensagens($data);
			$.ptr.hide();
		},
		action:"getmensagem",
		onStart:function(){

		}
	});

}
exports.startup = function($data){
	gridMensagens($data);
	//$.mensagem.on("click-footer", clickNav);
};
exports.destroy = function(){
	//$.mensagem.off("click-footer", clickNav);
};
