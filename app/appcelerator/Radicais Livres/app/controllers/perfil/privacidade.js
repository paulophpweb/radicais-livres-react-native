exports.baseController = "base";
exports.controllerName = "/timeline/privacidade";
$.container.add($.conteudo);
var args = arguments[0] || {};
var itensPopulados = {
		meu:{
			data:[{
				descricao: {text:L('priv_amig_pess_n_conh')},
				template: 'check',
				tipo:"privacidade_ver_conteudo-todos",
				icone:{image:DataPerfil.privacidade_ver_conteudo == "todos" ? "/images/icon-certo.png" : ""}
			},{
				descricao: {text:L('priv_somen_amigo')},
				template: 'check',
				tipo:"privacidade_ver_conteudo-somente_amigos",
				icone:{image:DataPerfil.privacidade_ver_conteudo == "somente_amigos" ? "/images/icon-certo.png" : ""}
			}],
			is_open:false
		},
		contato:{
			data:[{
				descricao: {text:L('priv_amig_pess_n_conh')},
				template: 'check',
				tipo:"privacidade_entrar_contato-todos",
				icone:{image:DataPerfil.privacidade_entrar_contato == "todos" ? "/images/icon-certo.png" : ""}
			},{
				descricao: {text:L('priv_somen_amigo')},
				template: 'check',
				tipo:"privacidade_entrar_contato-somente_amigos",
				icone:{image:DataPerfil.privacidade_entrar_contato == "somente_amigos" ? "/images/icon-certo.png" : ""}
			}],
			is_open:false
		},
		naoperturbe:{
			data:[{
				descricao: {text:L('priv_nao_pertube')},
				template: 'descricao'
			}],
			is_open:false
		}
};
$.meu.parentExports = exports;
$.contato.parentExports = exports;
$.naoperturbe.parentExports = exports;

function clickItem(e){
	var section = $.list.sections[e.sectionIndex];
    var item = section.getItemAt(e.itemIndex);
    if(item.icone == null || (item.icone != null && item.icone.image == "")){
    	item.icone = {image:"/images/icon-certo.png"};
    	
    	if(e.sectionIndex === 0){
	    	for(var j = 0;j<itensPopulados.meu.data.length;j++){
	    		itensPopulados.meu.data[j].icone = {image:""};
	    	}
	    	$["group_meu"].setItems(itensPopulados.meu.data);
    	}else if(e.sectionIndex === 1){
    		for(var j = 0;j<itensPopulados.contato.data.length;j++){
	    		itensPopulados.contato.data[j].icone = {image:""};
	    	}
	    	$["group_contato"].setItems(itensPopulados.contato.data);
    	}
    	
    	var $data = {};
	    var campoValue = item.tipo.split("-"); 
	    $data[campoValue[0]] = campoValue[1];
		$.ajaxDefaultLoader({
			onComplete:function($data){
				$.dialog.alert({mensagem:"success_msg"});
				DataPerfil[campoValue[0]] = campoValue[1];
			},
			action:"privacidade",
			data:$data
		});
		
    }else if(item.icone.image == "/images/icon-certo.png"){
    	var $data = {};
	    var campoValue = item.tipo.split("-");
	    DataPerfil[campoValue[0]] = "todos";
    	delete item.icone;
    	
    }
    e.section.updateItemAt(e.itemIndex, item);

}

exports.click = function(e){
	if(!itensPopulados[e.id].is_open){
		$["group_"+e.id].setItems(itensPopulados[e.id].data);
		itensPopulados[e.id].is_open = true;
		$[e.id].open();
	}else{
		$["group_"+e.id].setItems([]);
		itensPopulados[e.id].is_open = false;
		$[e.id].close();
	}
};