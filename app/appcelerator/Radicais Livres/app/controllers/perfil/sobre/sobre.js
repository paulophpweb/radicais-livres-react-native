exports.baseController = "base";
exports.controllerName = "/perfil/sobre/sobre";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
var $retornoEstadoCivil = null;
Ti.API.error(JSON.stringify(DataPerfil));
var $list = [
{
	param: {text:L("ps_nome")},
	value: {
		text:DataPerfil.nome,
		controller: "editar_nome"
		},
},{
	param: {text:L("ps_sobrenome")},
	value: {
		text:DataPerfil.sobrenome,
		controller: "editar_sobrenome"
		},
},{
	param: {text:L("ps_email")},
	value: {
		text:DataPerfil.email,
		controller: "editar_email"
		},
},{
	param: {text:L("ps_sexo")},
	value: {
		text:DataPerfil.genero,
		controller: "editar_sexo"
		}
},{
	param: {text:L("ps_idade")},
	value: {
		text:moment().diff(DataPerfil.data_nascimento, 'years')+" "+L('sobre_anos'),
		controller: "editar_data_nascimento",
		data_en: moment(DataPerfil.data_nascimento).format("YYYY-MM-DD").toString(),
		data_br: moment(DataPerfil.data_nascimento).format("DD/MM/YYYY").toString()
		}
},{
	param: {text:L("ps_igreja")},
	value: {
		text:DataPerfil.igreja_nome,
		controller: "editar_igreja"
		}
},{
	param: {text:L("ps_funcao")},
	value: {
		text:DataPerfil.funcao,
		controller: "editar_funcao"
		}
},{
	param: {text:L("estado")},
	value: {
		text:DataPerfil.estado,
		controller: "editar_estado"
		}
},{
	param: {text:L("cidade")},
	value: {
		text:DataPerfil.cidade,
		controller: "editar_cidade"
		}
},{
	param: {text:L("ps_formacao")},
	value: {
		text:!DataPerfil.formacao_nome ? L("ps_nao_informado") : DataPerfil.formacao_nome,
		controller: "editar_formacao"
		}
},{
	param: {text:L("ps_estado_civil")},
	value: {
		text:!DataPerfil.estado_civil ? L("ps_nao_informado") : DataPerfil.estado_civil,
		controller: "editar_estado_civil"
		},
	subvalue: {text:!DataPerfil.relacionamento ? "" : DataPerfil.relacionamento}
}];
var $commands = {
	editar_nome: function(e){
		editCamposText(e,"nome");
	},
	editar_email: function(e){
		editCampoEmail(e,"email");
	},
	editar_sobrenome: function(e){
		editCamposText(e,"sobrenome");
	},
	editar_sexo: function(e){
		var section = $.list.sections[e.sectionIndex];
		var itens = section.getItemAt(e.itemIndex);
		var valorOld = itens.value.text;

		var listaSexo = {
		  cancel: 0,
		  options: [L("cancelar"),L("alt_masculino"),L("alt_feminino")],
		  selectedIndex: 0,
		  destructive: 0
		};

		var dialogSexo = Ti.UI.createOptionDialog(listaSexo);

		dialogSexo.addEventListener("click", function(result){
			if(result.cancel != result.index && result.index>0){
				editCampoAlert(e,"genero",dialogSexo.options[result.index]);
			}else{
				itens.value.text = valorOld;
			}
		});
		dialogSexo.show();
	},
	editar_data_nascimento: function(e){
		editCamposData(e,"data_nascimento");
	},
	editar_igreja: function(e){
		var section = $.list.sections[e.sectionIndex];
		var itens = section.getItemAt(e.itemIndex);

		Navigate.selectGrid({
			parent:$,
			action:"procurarigreja",
			hintText:'nome_igreja',
			onCancel:function(result){

			},
			onComplete:function(result){
				editCampoSuperFiltro(e,"igreja_id",result.igreja_id,result.text,"igreja_nome");
			},
			lineItem:function(result){
				return {
					text:result.nome,
					igreja_id:result.igreja_id
				};
			}
		});
	},
	editar_estado: function(e){
		var section = $.list.sections[e.sectionIndex];
		var itens = section.getItemAt(e.itemIndex);

		Navigate.selectGrid({
			parent:$,
			action:"getestado",
			hintText:'estado',
			onCancel:function(result){

			},
			onComplete:function(result){
 				e.itemIndex = e.itemIndex+1;
 				e.estado_id = result.estado_id;
 				e.estado = result.text;
 				setTimeout(function(){
 					clickEditar(e);
 				},500);
			},
			lineItem:function(result){
				return {
					text:result.nome,
					estado_id:result.estado_id
				};
			}
		});
	},
	editar_cidade: function(e){
		var section = $.list.sections[e.sectionIndex];
		var itens = section.getItemAt(e.itemIndex);

		Navigate.selectGrid({
			parent:$,
			action:"getcidade",
			data:{estado_id:e.estado_id},
			hintText:'cidade',
			onCancel:function(result){

			},
			onComplete:function(result){
				editCampoSuperFiltro(e,"cidade_id",result.cidade_id,result.text,"cidade");
				GlobalFunctions.update('estado',e.estado);

				var itens_anterior = section.getItemAt(e.itemIndex);
				itens_anterior.value.text = e.estado;
				e.section.updateItemAt(e.itemIndex-1, itens_anterior);
			},
			lineItem:function(result){
				return {
					text:result.nome,
					cidade_id:result.cidade_id
				};
			}
		});
	},
	editar_formacao: function(e){
		var section = $.list.sections[e.sectionIndex];
		var itens = section.getItemAt(e.itemIndex);

		Navigate.selectGrid({
			parent:$,
			action:"procurarformacao",
			hintText:'nome_formacao',
			onCancel:function(result){

			},
			onComplete:function(result){
				editCampoSuperFiltro(e,"formacao_id",result.formacao_id,result.text,"formacao_nome");
			},
			lineItem:function(result){
				return {
					text:result.nome,
					formacao_id:result.formacao_id
				};
			}
		});
	},
	editar_estado_civil: function(e){
		var $retorno = editCampoRelacionamento(e);
	},
	editar_funcao: function(e){
		var section = $.list.sections[e.sectionIndex];
		var itens = section.getItemAt(e.itemIndex);

		Navigate.selectGrid({
			parent:$,
			action:"procurarfuncao",
			hintText:'nome_funcao',
			onCancel:function(result){

			},
			onComplete:function(result){
				editCampoSuperFiltro(e,"funcao_id",result.funcao_id,result.text,"funcao_nome");
			},
			lineItem:function(result){
				return {
					text:result.nome,
					funcao_id:result.funcao_id
				};
			}
		});
	},
};
$.iterator.setItems($list);
function clickHeader(e){
	e.onStart = $.dialog.showLoading;
	e.onComplete = $.dialog.hideLoading;
	e.onAJAXError = function(e){
		$.dialog.hideLoading();
		$.dialog.confirm({
			mensagem:e.msg,
			textLivre:true,
			confirmar:"tentar_novamente",
			onConfirm:function(){
				$.dialog.showLoading();
				Ajax.post(e);
			}
		});
	};
	Navigate.openWindowAndCloseBack(e.controller, e);
}
function clickFooter(e){
	Navigate.openWindow(e.controller, {page:e.page, onStart:$.dialog.showLoading, onComplete:$.dialog.hideLoading});
}
function clickEditar(e){
	var section = $.list.sections[e.sectionIndex];
    var itens = section.getItemAt(e.itemIndex);
 	$commands[itens.value.controller](e);
}
function editCamposText(e,valor){
	var section = $.list.sections[e.sectionIndex];
	var itens = section.getItemAt(e.itemIndex);
	Navigate.editText({
		parent:$,
		type:"min",
		text:itens.value.text,
		hint_min:"ed_editar_"+valor,
		onComplete:function(result){
			$.dialog.showLoading();
			var $data = {};
			$data[valor] = result.min;
			$data.cadastro_id = DataPerfil.cadastro_id;
			_this.ajaxDefaultLoader({
				onComplete:function($data){
					if(valor == "nome"){
						$.headers_photo.nome.text = result.min;
					}
					itens.value.text = result.min;
					GlobalFunctions.update(valor,result.min);
					e.section.updateItemAt(e.itemIndex, itens);
					$.dialog.alert({mensagem:"ed_dados_atualizados"});
				},
				action:"atualizar",
				data:$data
			});
		}
	});
}
function editCampoEmail(e,valor){
	var section = $.list.sections[e.sectionIndex];
	var itens = section.getItemAt(e.itemIndex);
	console.log("entrou no editar email");
	Navigate.editText({
		parent:$,
		type:"email",
		text:itens.value.text,
		hint_email:"ed_editar_"+valor,
		onComplete:function(result){
			var $dados = {};
			$dados[valor] = result.email;
			$dados.cadastro_id = DataPerfil.cadastro_id;
			Ti.API.error(JSON.stringify($dados));
			_this.ajaxDefaultLoader({
				onComplete:function($data){
					console.log($data);
					itens.value.text = result.email;
					GlobalFunctions.update(valor,result.email);
					e.section.updateItemAt(e.itemIndex, itens);
					$.dialog.alert({mensagem:"ed_dados_atualizados"});
				},
				onError:function($data){
					$.dialog.hideLoading();
					$.dialog.alert({mensagem:$data.msg,textLivre:true});
				},
				action:"atualizar",
				data:$dados
			});
		}
	});
}
function editCamposData(e,valor){
	var section = $.list.sections[e.sectionIndex];
	var itens = section.getItemAt(e.itemIndex);

	Navigate.editText({
		parent:$,
		type:"data",
		text:itens.value.data_br,
		hint_data:"ed_editar_"+valor,
		onComplete:function(result){
			$.dialog.showLoading();
			var $data = {};
			$data[valor] = result.data;
			$data.cadastro_id = DataPerfil.cadastro_id;
			_this.ajaxDefaultLoader({
				onComplete:function($data){
					var dateFormatEn = moment(result.data,"DD/MM/YYYY").format("YYYY-MM-DD").toString();
					itens.value.text = moment().diff(dateFormatEn, 'years')+" "+L('sobre_anos');
					itens.value.data_br = result.data;
					itens.value.data_en = dateFormatEn;
					GlobalFunctions.update(valor,dateFormatEn);
					e.section.updateItemAt(e.itemIndex, itens);
					$.dialog.alert({mensagem:"ed_dados_atualizados"});
				},
				action:"atualizar",
				data:$data
			});
		}
	});
}
function editCampoAlert(e,valor,valorSelected){
	var section = $.list.sections[e.sectionIndex];
	var itens = section.getItemAt(e.itemIndex);
	$.dialog.showLoading();
	var $data = {};
	$data[valor] = valorSelected;
	$data.cadastro_id = DataPerfil.cadastro_id;
	_this.ajaxDefaultLoader({
		onComplete:function($data){
			itens.value.text = valorSelected;
			GlobalFunctions.update(valor,valorSelected);
			e.section.updateItemAt(e.itemIndex, itens);
			$.dialog.alert({mensagem:"ed_dados_atualizados"});
			if(valor = "genero")
				GlobalFunctions.setImageGenero();
		},
		action:"atualizar",
		data:$data
	});
}
function editCampoSuperFiltro(e,valor,valorSelected,valorString,valorNome){
	var section = $.list.sections[e.sectionIndex];
	var itens = section.getItemAt(e.itemIndex);
	$.dialog.showLoading();
	var $data = {};
	$data[valor] = valorSelected;
	$data.cadastro_id = DataPerfil.cadastro_id;
	_this.ajaxDefaultLoader({
		onComplete:function($data){
			itens.value.text = valorString;
			GlobalFunctions.update(valor,valorSelected);
			GlobalFunctions.update(valorNome,valorString);
			e.section.updateItemAt(e.itemIndex, itens);
			$.dialog.alert({mensagem:"ed_dados_atualizados"});
		},
		action:"atualizar",
		data:$data
	});
}
function editCampoRelacionamento(e){
	var section = $.list.sections[e.sectionIndex];
	var itens = section.getItemAt(e.itemIndex);
	$.dialog.showLoading();
	_this.ajaxDefaultLoader({
		onComplete:function($data){
			var listaRelacionamento = {
			  cancel: 0,
			  options: [L("cancelar")],
			  options_id: [""],
			  options_relaciona: [""],
			  selectedIndex: 0,
			  destructive: 0
			};
			_.each($data,function(value,key){
				listaRelacionamento.options[key+1] = value.nome;
				listaRelacionamento.options_id[key+1] = value.relacionamento_id;
				listaRelacionamento.options_relaciona[key+1] = value.relaciona;
			});
			var dialogRelacionamento = Ti.UI.createOptionDialog(listaRelacionamento);
			dialogRelacionamento.addEventListener("click", function(result){
				if(result.cancel != result.index && result.index>0){
					if(dialogRelacionamento.options_relaciona[result.index]){
						Navigate.selectGrid({
							parent:$,
							action:"listaramigoaserrelacionar",
							hintText:'ed_editar_relacionamento',
							onCancel:function(retorno){

							},
							onComplete:function(retorno){
								var $data = {};
								$data["cadastro_id_relacionamento"] = retorno.cadastro_id;
								$data["relacionamento_id"] = retorno.relacionamento_id;
								$data.cadastro_id = DataPerfil.cadastro_id;
								_this.ajaxDefaultLoader({
									onComplete:function($data){
										Ti.API.error(JSON.stringify(retorno));
										itens.subvalue.text = retorno.text;
										itens.value.text = retorno.estado_civil;
										GlobalFunctions.update("cadastro_id_relacionamento",retorno.cadastro_id);
										GlobalFunctions.update("relacionamento_id",retorno.relacionamento_id);
										GlobalFunctions.update("relacionamento",retorno.text);
										GlobalFunctions.update("estado_civil",retorno.estado_civil);
										e.section.updateItemAt(e.itemIndex, itens);
										$.dialog.alert({mensagem:"ed_dados_atualizados"});
									},
									action:"atualizar",
									data:$data
								});
							},
							lineItem:function(retorno){
								return {
									text:retorno.nome+" "+retorno.sobrenome,
									cadastro_id:retorno.cadastro_id,
									relacionamento_id: listaRelacionamento.options_id[result.index],
									estado_civil: listaRelacionamento.options[result.index]
								};
							},
							data : {genero : DataPerfil.genero}
						});
					// atualiza os que nao relaciona
					}else{
						var $data = {};
						$data["relacionamento_id"] = dialogRelacionamento.options_id[result.index];
						$data.cadastro_id = DataPerfil.cadastro_id;
						$data.cadastro_id_relacionamento = "";
						_this.ajaxDefaultLoader({
							onComplete:function($data){
								itens.value.text = dialogRelacionamento.options[result.index];
								itens.subvalue.text = "";
								GlobalFunctions.update("relacionamento_id",dialogRelacionamento.options_id[result.index]);
								GlobalFunctions.update("estado_civil",dialogRelacionamento.options[result.index]);
								GlobalFunctions.update("cadastro_id_relacionamento","");
								GlobalFunctions.update("relacionamento","");
								e.section.updateItemAt(e.itemIndex, itens);
								$.dialog.alert({mensagem:"ed_dados_atualizados"});
							},
							action:"atualizar",
							data:$data
						});
					}
				}
			});
			dialogRelacionamento.show();
		},
		action:"procurarrelacionamento"
	});
}
exports.startup = function(){
	$.sobre.on("click-footer", clickHeader);
	//$.sobre_footer.on("click-footer", clickFooter);
};
exports.destroy = function(){
	$.sobre.off("click-footer", clickHeader);
	//$.sobre_footer.off("click-footer", clickFooter);
};
