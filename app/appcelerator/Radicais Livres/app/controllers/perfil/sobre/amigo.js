exports.baseController = "base";
exports.controllerName = "/perfil/peopletimeline";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
var $list = [];
var $qtdePerPage = 10;
var $page = 1;
var $qtde = 10;
function pupularListView($data){
	$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
	for(var a in $data){
		var $p = $data[a];
		var $nomePessoa = $p.nome+" "+$p.sobrenome+($p.funcao ? " ("+$p.funcao+")" : "");
		var $item = {
				foto: {
					image : GlobalFunctions.getURL($p.foto,$p.genero)
				},
				nome: {
					text: StringUtil.abreviar($nomePessoa, 25, "...")
				},
				estado_cidade: {
					text: StringUtil.abreviar($p.estado+" - "+$p.cidade+"", 25, "...") 
				},
				relacionamento: {
					text: $p.nome_relacionado ? StringUtil.abreviar($p.relacionamento+" com "+$p.nome_relacionado, 25, "...") : $p.relacionamento
				},
				bloquear:{
					visible:DataPerfil.is_root ? false : true,
					image:$p.bloqueado ? "/images/icon/icon_cadeado_bloqueado.png" : "/images/icon/icon_cadeado_desbloqueado.png"
				},
				viewBloquear:{
					visible:DataPerfil.is_root ? false : true
				},
				viewBotao:{
					visible:DataPerfil.is_root ? false : true
				},
				botao:{
					image : "/images/close.png",
					cadastro_id:$p.cadastro_id,
					tokenpush:$p.tokenpush,
					visible:DataPerfil.is_root ? false : true
				}
			};
			$list.push($item);
	}
	$.iterator.setItems($list);
}
function clickItem(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	Ti.API.error(JSON.stringify(e.bindId));
	if(e.bindId == "viewBotao"){
		$.dialog.confirm({
			mensagem:"excluir_amigo",
			confirmar:"sim",
			onConfirm:function(){
				_this.ajaxDefaultLoader({
					onComplete:function($data){
						$.dialog.alert({mensagem:$data.msg});
						if(OS_IOS)e.section.deleteItemsAt(e.itemIndex, 1, {animationStyle: Titanium.UI.iPhone.RowAnimationStyle.LEFT});
			    		else if(OS_ANDROID)e.section.deleteItemsAt(e.itemIndex, 1);
					},
					action:"removeramizade",
					data:{
						cadastro_id_amizade:item.botao.cadastro_id,
						action: "getperfil"
					}
				});
			}
		});

	}else if(e.bindId == "foto" || e.bindId == "nome"){
		_this.openWindow("timeline/people", false, {cadastro_id_pessoa:item.botao.cadastro_id, action: "getperfil", timeline:"true"});
	}else if(e.bindId == "viewBloquear"){
		if(item.bloquear.image == "/images/icon/icon_cadeado_desbloqueado.png"){
			_this.ajaxDefaultLoader({
			onComplete:function($data){
				$.dialog.alert({mensagem:$data.msg});
				item.bloquear.image = "/images/icon/icon_cadeado_bloqueado.png";
				e.section.updateItemAt(e.itemIndex, item);
			},
			action:"bloquearamigo",
			data:{
				cadastro_id_amizade:item.botao.cadastro_id
			}
		});

		}else{
			_this.ajaxDefaultLoader({
				onComplete:function($data){
					$.dialog.alert({mensagem:$data.msg});
					item.bloquear.image = "/images/icon/icon_cadeado_desbloqueado.png";
					e.section.updateItemAt(e.itemIndex, item);
				},
				action:"desbloquearamigo",
				data:{
					cadastro_id_amizade:item.botao.cadastro_id
				}
			});
		}
	}
}
function onMarker(e){
	$page = $page+1;
	$qtdePerPage = ($qtde * $page);
	_this.ajaxDefaultLoader({
		onStart:function(){
			$.loaderResultado.start();
			$.loaderResultado.visible = true;
		},
		onComplete:function($data){
			$.loaderResultado.stop();
			$.loaderResultado.visible = false;
			pupularListView($data);
		},
		action:"listaramigos",
		data:{
			limit:$qtde,
			offset:($qtde * ($page - 1))
		}
	});
}
function clickHeader(e){
	_this.openWindow(e.controller, false, e);
}
function clickFooter(e){
	_this.openWindow(e.controller, false, e);
}
function clickPesquisar(e){
	clickFooter({controller:"perfil/sobre/amigo/procurar"});
}
exports.startup = function($data){
	$.amigo.on("click-footer", clickHeader);
	//$.sobre_footer.on("click-footer", clickFooter);
	pupularListView($data);
};
exports.destroy = function(){
	$.amigo.off("click-footer", clickHeader);
	//$.sobre_footer.off("click-footer", clickFooter);
};
