exports.baseController = "base";
exports.controllerName = "/perfil/sobre/amigo/procurar";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
var $id_igreja = null;
var $id_relacionamento_id = null;
var $estado_id = null;
var $funcao_id = null;
var $cidade_id = null;

function clickSearchIgreja(e){
	Navigate.selectGrid({
		parent:$,
		action:"procurarigreja",
		hintText:'nome_igreja',
		onCancel:function(e){
			$.igraja_nome.text = L("nome_igreja");
			$.igraja_nome.color = "#9aadc5";
			$id_igreja = null;
		},
		onComplete:function(e){
			$.igraja_nome.text = e.text;
			$.igraja_nome.color = "#3d3b3d";
			$id_igreja = e.igreja_id;
		},
		lineItem:function(e){
			return {
				text:e.nome,
				igreja_id:e.igreja_id
			};
		}
	});
}

function clickSearchEstado(e){
	Navigate.selectGrid({
		parent:$,
		action:"getestado",
		hintText:'estado',
		onCancel:function(e){
			$.estado_nome.text = L("estado");
			$.estado_nome.color = "#9aadc5";
			$estado_id = null;
		},
		onComplete:function(e){
			$.estado_nome.text = e.text;
			$.estado_nome.color = "#3d3b3d";
			$estado_id = e.estado_id;
		},
		lineItem:function(e){
			return {
				text:e.nome,
				estado_id:e.estado_id,
				sigla: e.sigla
			};
		}
	});
}

function clickSearchFuncao(e){
	Navigate.selectGrid({
		parent:$,
		action:"procurarfuncao",
		hintText:'nome_funcao',
		onCancel:function(e){
			$.estado_nome.text = L("nome_funcao");
			$.estado_nome.color = "#9aadc5";
			$estado_id = null;
		},
		onComplete:function(e){
			$.funcao_nome.text = e.text;
			$.funcao_nome.color = "#3d3b3d";
			$funcao_id = e.funcao_id;
		},
		lineItem:function(e){
			return {
				text:e.nome,
				funcao_id:e.funcao_id
			};
		}
	});
}

function clickSearchCidade(e){
	if($estado_id != null){
		Navigate.selectGrid({
			parent:$,
			action:"getcidade",
			data:{estado_id:$estado_id}, 
			hintText:'cidade',
			onCancel:function(e){
				$.cidade_nome.text = L("cidade");
				$.cidade_nome.color = "#3d3b3d";
				$cidade_id = null;
			},
			onComplete:function(e){
				$.cidade_nome.text = e.text;
				$.cidade_nome.color = "#3d3b3d";
				$cidade_id = e.cidade_id;
			},
			lineItem:function(e){
				return {
					text:e.nome,
					cidade_id:e.cidade_id
				};
			}
		});
	}else{
		$.dialog.alert({mensagem:"Selecione primeiro o seu estado",textLivre:true});
	}
}

function clickSearchEstadoCivil(e){
	Navigate.selectGrid({
		parent:$,
		action:"procurarrelacionamento",
		hintText:'pra_estado_civil',
		onCancel:function(e){
			$.estado_civil.text = L("pra_estado_civil");
			$.estado_civil.color = "#9aadc5";
			$id_relacionamento_id = null;
		},
		onComplete:function(e){
			$.estado_civil.text = e.text;
			$.estado_civil.color = "#3d3b3d";
			$id_relacionamento_id = e.relacionamento_id;
		},
		lineItem:function(e){
			return {
				text:e.nome,
				relacionamento_id:e.relacionamento_id
			};
		}
	});
}

function clickPesquisar(e){
	_this.openWindow("perfil/sobre/amigo/resultado", false, {
		igreja_id:$id_igreja, 
		relacionamento_id:$id_relacionamento_id,
		estado_id:$estado_id,
		funcao_id:$funcao_id,
		cidade_id:$cidade_id,
		nome:$.nome.value,
		action:"procuraramizade"
	});
}

