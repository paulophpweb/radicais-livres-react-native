exports.baseController = "base";
exports.controllerName = "/perfil/sobre/amigo/resultado";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
$.header.parent = $;
$.header.parentController = this;
$.header.closechat();
var $list = [];
var $qtdePerPage = 10;
var $page = 1;
var $qtde = 10;
function popularDataGrid($data){
	$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
	for(var a in $data){
		var $p = $data[a];
		var $nomePessoa = $p.nome+" "+$p.sobrenome+($p.funcao ? " ("+$p.funcao+")" : "");
		var $item = {
			foto: {
				image:  GlobalFunctions.getURL($p.foto, $p.genero)
			},
			nome: {
				text: StringUtil.abreviar($nomePessoa, 30, "...")
			},
			estado_cidade: {
				text: StringUtil.abreviar($p.estado+" - "+$p.cidade+"", 30, "...") 
			},
			relacionamento: {
				text: $p.nome_relacionado ? StringUtil.abreviar($p.relacionamento+" com "+$p.nome_relacionado, 30, "...") : $p.relacionamento
			},
			botao:{
				cadastro_id_amizade:$p.cadastro_id,
				image:"/images/solicitacoes.png",
				tokenpush:$p.tokenpush,
				visible:DataPerfil.is_root ? false : true
			}
		};
		$list.push($item);
	}
	$.iterator.setItems($list);
}
function searchHeader(e){
	if(e.value != ""){
		args.nome = e.value;
		_this.ajaxDefaultLoader({
			onComplete:popularDataGrid,
			action:args.action,
			data:{
				igreja_id:args.igreja_id, 
				relacionamento_id:args.relacionamento_id,
				nome:args.nome
			}
		});
	}else $.dialog.alert({mensagem:"search_vazio"});
}
function clickItem(e){
	Ti.API.error(e.bindId);
    var item = $.iterator.getItemAt(e.itemIndex);
    if(e.bindId == "viewBotao" || e.bindId == "botao"){
    	if(item.botao.image != "/images/close.png"){
    		_this.ajaxDefaultLoader({
				onComplete:function($data){
					$.dialog.alert({mensagem:$data.msg});
					item.botao.image = "/images/close.png";
			    	e.section.updateItemAt(e.itemIndex, item);
				},
				action:"solicitaramizade",
				data:{
					cadastro_id_solicitado:item.botao.cadastro_id_amizade,
					tokenpushamizade:item.botao.tokenpush,
				    notificacao: DataPerfil.nome+" solicitou sua amizade."
				}
			});
    	}else{
    		_this.ajaxDefaultLoader({
				onComplete:function($data){
					$.dialog.alert({mensagem:$data.msg});
					item.botao.image = "/images/solicitacoes.png";
			    	e.section.updateItemAt(e.itemIndex, item);
				},
				action:"removeramizade",
				data:{
					cadastro_id_amizade:item.botao.cadastro_id_amizade
				}
			});
    	}
    }else if(e.bindId == "foto" || e.bindId == "nome"){
		_this.openWindow("timeline/people", false, {cadastro_id_pessoa:item.botao.cadastro_id_amizade,action: "getperfil", timeline:"true"});
	}
}
function onMarker(e){
	
	$page = $page+1;
	$qtdePerPage = ($qtde * $page);
	_this.ajaxDefaultLoader({
		onStart:function(){
			$.loaderResultado.start();
		},
		onComplete:function($data){
			$.loaderResultado.stop();
			popularDataGrid($data);
		},
		action:"procuraramizade",
		data:{
			igreja_id:args.igreja_id,
			relacionamento_id:args.relacionamento_id,
			nome:args.nome,
			limit:$qtde,
			offset:($qtde * ($page - 1))
		}
	});
	
}
exports.startup = function($data){
	popularDataGrid($data);
	$.header.search.value = args.nome || "";
	$.header.search.editable = false;
	$.header.search.setSelection($.header.search.value.length, $.header.search.value.length);
	$.header.on("search-header", searchHeader);
	
};
exports.destroy = function(){
	$.header.off("search-header", searchHeader);
};

