exports.baseController = "base";
exports.controllerName = "/perfil/peopletimeline";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
$.headers_photo.parent = $.foto.parent = this;
function popularListView($data){
	var $list = [];
	var $x = 0;
	var $y = 0;
	for(var a in $data){
		var $p = $data[a];
		$list[$y] = $list[$y] || {};
		$list[$y]["img"+$x] = {
			imageDefault:"/images/sem-foto.png",
			image: GlobalFunctions.getURL($p.foto.replace("/300x170/", "/150x150/")),
			imageBig: GlobalFunctions.getURL($p.foto.replace("/300x170/", "/300xprop/"))
		};
		if($x>=2){
			$x = -1;
			$y++;
		}
		$x++;
	}
	$.iterator.setItems($list);
}
function clickItem(e){
	var section = $.list.sections[e.sectionIndex];
	var fotos = section.getItemAt(e.itemIndex);
	var foto = fotos[e.bindId];
	_this.openWindow("openImage", false, {image:foto.imageBig});
}
function clickHeader(e){
	_this.openWindow(e.controller, true, e);
}
function clickMais(e){
	$.openWindow("timeline/action/write", false,{open_galeria:true});
}
function clickFooter(e){
	_this.openWindow(e.controller,false, e);
}
function reload(e){
	$.ajaxDefaultLoader({
		onComplete:function($data){
			popularListView($data);
		},
		action:"timelineperfilfotos",
		onStart:function(){
			
		}
	});
}
exports.update = function($args){
	reload({});
};
exports.startup = function($data){
	popularListView($data);
	$.foto.on("click-footer", clickHeader);
	//$.sobre_footer.on("click-footer", clickFooter);
};
exports.destroy = function(){
	$.foto.off("click-footer", clickHeader);
	//$.sobre_footer.off("click-footer", clickFooter);
};