exports.baseController = "base";
exports.controllerName = "/perfil/escrever_mensagem";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;


function changeTextArea(e){
	if($.container_text.expanded){
		$.container_text.expanded = false;
		$.container_text.top = 50;
		$.ctrl_expand.backgroundImage="/images/expand.png";
	}
}

exports.startup = function($data){
	Ti.API.error(JSON.stringify(args));
	$.foto.image = GlobalFunctions.getURL(DataPerfil.foto,DataPerfil.genero);
	$.publicar.email = args.email;
};

function clickEnviar(e){
	var data = {
		cadastro_id: args.cadastro_id,
		cadastro_id_amizade: DataPerfil.cadastro_id,
		texto: $.texto.value,
		nome: DataPerfil.nome+" "+DataPerfil.sobrenome,
		email: e.source.email,
		tokenpushamizade: args.tokenpush,
		notificacao: "Seu amigo "+DataPerfil.nome+" te enviou uma mensagem."
		
	};
	$.ajaxDefaultLoader({
		onComplete:function($data){
			$.dialog.alert({mensagem:'mensagem_enviada', onComplete: Navigate.backWindow});
		},
		action:"enviarmensagem",
		data:data
	});
}
