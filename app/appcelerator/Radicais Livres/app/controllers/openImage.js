exports.baseController = "base";
exports.controllerName = "/perfil/peopletimeline";
$.container.add($.conteudo);
var args = arguments[0] || {};
var _this = this;
if(OS_ANDROID){
	var $url;

	$url = GlobalFunctions.getURL(args.image);
	var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,$url.substring($url.lastIndexOf("/"), $url.length));
	if(file.exists()){
		visualizarImagem();
	}else{
		downloadImage(file, $url);
	}

	
}else if(OS_IOS){
	var image_view = Titanium.UI.createImageView({
        image: GlobalFunctions.getURL(args.image),
        defaultImage:"/images/sem-foto.png",
        id: "foto",
        'class': "wf hs"        
     });
	 var scrollViewCustom = Titanium.UI.createScrollView({
        contentWidth: 'auto',
        contentHeight: 'auto',
        top: 0,
        bottom: 0,  
        showVerticalScrollIndicator: false,
        showHorizontalScrollIndicator: true,  
        maxZoomScale: 3,
        minZoomScale: 1,  
        zoomScale: 1  
     });
     
     $.addCustomScroll.add(scrollViewCustom);
     scrollViewCustom.add(image_view);
}

function visualizarImagem(){
	/*var scrollViewCustom = TiTouchImageView.createView({
		top:0,
		left:0,
		right:0,
		bottom:0,
		image:file.nativePath,
		zoom:1,
		maxZoom:3,
		minZoom:1,
	});
	$.addCustomScroll.add(scrollViewCustom);*/
}

function prepareFile(file){
	var timeStampName = new Date().getTime();
	var tempFile = Ti.Filesystem.getFile(Ti.Filesystem.tempDirectory,timeStampName +'.jpg');
	if(tempFile.exists()){
	    tempFile.deleteFile();
	}
	tempFile.write(file.read());
	file = null;
	return tempFile;
}


function downloadImage(file, httpURL){
	
	var retrieveReq = Ti.Network.createHTTPClient();
	retrieveReq.setTimeout(100000);
    retrieveReq.onload = function(e){
		file.write(this.responseData);
		visualizarImagem();
    };
    retrieveReq.onerror = function(e) {
    	$.activityIndicator.hide();
    	_this.showNotify("Não foi possível carregar a imagem. Error("+e.error+")");
    };
    
    retrieveReq.open("GET",httpURL);
    retrieveReq.send();
}
