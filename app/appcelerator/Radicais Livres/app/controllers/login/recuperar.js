var $data = {};
var $newChilds = ["", "login/recuperar/codigo", "login/recuperar/senha"];
this.getData = function($key){
	return $key == null ? $data : $data[$key];
};

var _this = this;
$.email.parent = _this;

function navRecuperar(e){
	$.parent.blurCurrentFocus();
	$data[e.action] = e[e.action];
	if($newChilds[e.page] != ""){
		var $controller = Alloy.createController($newChilds[e.page]);
			$controller.parent = _this;
		
		var $view = $controller.getView();
		$.conteudo.addView($view);
		
		$newChilds[e.page] = "";
	}
	$.conteudo.scrollToView($.conteudo.views[e.page]);
}

$.on("click", navRecuperar);