var $inputs = [$.email, $.senha, $.entrar];
var ModelUsuario = Alloy.Models.instance('usuario');


setTimeout(function(e){
    $.email.editable = true;
    $.senha.editable = true;
},2000);


if(ENV_DEV){
	$.email.value = "root@root.com";
	$.senha.value = "123";
}

function blur(e){
	Ti.API.info("login/blur/input");
	Triggers.shoot({parans:e.source, type:'text:login-blur'});
}
function focus(e){
	Ti.API.info("login/focus/input");
	Triggers.shoot({parans:e.source, type:'text:login-focus'});
}
function onReturn(e){
	Validate.next($, e.source);
}
function clickRecuperarSenha(e){
	$.parent.trigger("click",{page:0});
}
function clickEntrar(e){
	if(Validate.check($, $inputs)){
		var _this = this;
		
		// Start
		ModelUsuario.login({email:$.email.value,senha:$.senha.value},function(){
			Ti.API.error("START LOGIN");
			ButtonAnimate.showLoader(_this, $.parent);
		// Sucess
		},function($data){
			Ti.API.error("CERTO LOGIN");
			GlobalFunctions.setDataPerfil($data);
			Navigate.openWindowAndCloseBack("menu/bloco", {onComplete:function(){
				ButtonAnimate.hideLoader(_this, $.parent);
			}});
		// Error
		},function(e){
			Ti.API.error("ERRO NO LOGIN");
			ButtonAnimate.hideLoader(_this, $.parent);
			$.parent.dialog.confirm({
				mensagem:e.msg,
				textLivre:true,
				confirmar:"tentar_novamente",
				onConfirm:function(){
					Ajax.post(e);
				}
			});
		});
	}else $.parent.dialog.alert({mensagem:"login_erro_mensagem"});
}

function clickNovaConta(e){
	$.parent.trigger("click",{page:2});
}

if(!GlobalFunctions.iosAppleHabilitarValidacao() && OS_IOS){
	$.nova_conta.visible = false;
}