var $inputs = [$.senha, $.senhacompare, $.entrar];
function blur(e){
	Triggers.shoot({parans:e.source, type:'text:login-blur'});
}
function focus(e){
	Triggers.shoot({parans:e.source, type:'text:login-focus'});
}
function onReturn(e){
	Validate.next($, e.source);
}

function addInputError($child){
	$["border_"+$child.id].borderColor = "#cc2128";
}
function removeInputError($child){
	$["border_"+$child.id].borderColor = "#5f5c5d";
}

function clickNext(e){
	if(Validate.check($, $inputs,addInputError,removeInputError)){
		$.parent.trigger("form-data", {
			data:{
				senha:$.senha.value
			}
		});
		$.parent.trigger("click", {page:10});
	}else{
		$.parent.dialog.alert({mensagem:"cadastro_fase_3_erro_mensagem"});
	}
}
function clickBack(e){
	$.parent.trigger("click", {page:8});
}