var $estado_id = null;
var $cidade_id = null;
function clickSearchEstado(e){
	Navigate.selectGrid({
		parent:$.parent,
		action:"getestado",
		hintText:'estado',
		onCancel:function(e){
			$.estado_nome.text = L("estado");
			$.estado_nome.color = "#3d3b3d";
			$estado_id = null;
		},
		onComplete:function(e){
			$.estado_nome.text = e.text;
			$.estado_nome.color = "#3d3b3d";
			$estado_id = e.estado_id;
		},
		lineItem:function(e){
			return {
				text:e.nome,
				estado_id:e.estado_id,
				sigla: e.sigla
			};
		}
	});
}
function clickSearchCidade(e){
	if($estado_id != null){
		Navigate.selectGrid({
			parent:$.parent,
			action:"getcidade",
			data:{estado_id:$estado_id}, 
			hintText:'cidade',
			onCancel:function(e){
				$.cidade_nome.text = L("cidade");
				$.cidade_nome.color = "#3d3b3d";
				$cidade_id = null;
			},
			onComplete:function(e){
				$.cidade_nome.text = e.text;
				$.cidade_nome.color = "#3d3b3d";
				$cidade_id = e.cidade_id;
			},
			lineItem:function(e){
				return {
					text:e.nome,
					cidade_id:e.cidade_id
				};
			}
		});
	}else{
		$.parent.dialog.alert({mensagem:"selecione_um_estado_cidade"});
	}
}
function clickNext(e){
	if($estado_id != null && $cidade_id != null){
		$.parent.trigger("form-data", {
			data:{
				cidade_id:$cidade_id
			}
		});
		$.parent.trigger("click", {page:8});
	}
	else $.parent.dialog.alert({mensagem:"selecione_um_estado_cidade"});
}

function clickBack(e){
	$.parent.trigger("click", {page:6});
}
exports.update = function(){
	
};