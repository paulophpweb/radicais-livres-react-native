var $inputs = [$.email, $.entrar];

function startFacebook(){
	//if(Facebook == null){
		
		//Facebook.permissions = Facebook.readPermissions = ['public_profile', 'email', "user_friends", "user_birthday"];
		//Facebook.forceDialogAuth = false;
		Facebook.addEventListener('login', function(e) {
			Ti.API.error("Retorno",JSON.stringify(e));
		    if (e.success) { 
		    	console.log(e);
		    	/*var data = JSON.parse(e.data);
		    	$.parent.trigger("form-data", {
		    		data:{
			    		email			: data.email,
			    		foto			: e.uid,
			    		data_nascimento	: moment(data.birthday, "MM/DD/YYYY").format("DD/MM/YYYY").toString(),
			    		nome			: data.first_name,
			    		genero			: data.gender.toLowerCase() == "male" ? "masculino" : "feminino",
			    		sobrenome		: data.last_name,
			    	}
		    	});
		    	$.parent.dialog.loading({show:false, onComplete:function(){
		    		$.parent.trigger("nav-scroll-by-index", {page:7});
		    	}});*/
		    }
		    else if (e.cancelled) {
		    	$.parent.dialog.alert({mensagem:"cadastro_fase_1_erro_mensagem"});
		    }
		    else {
		    	alert("nada");
		    	$.parent.dialog.alert({mensagem:"cadastro_fase_1_erro_mensagem"});
		    }
		});
	//}
}
function blur(e){
	Triggers.shoot({parans:e.source, type:'text:login-blur'});
}
function focus(e){
	Triggers.shoot({parans:e.source, type:'text:login-focus'});
}
function onReturn(e){
	Validate.next($, e.source);
}
function clickNext(e){
	if(Validate.check($, $inputs)){
		$.parent.trigger("form-data", {data:{email:$.email.value}});
		$.parent.trigger("click", {page:3});
	}else{
		$.parent.dialog.alert({onComplete:function(){
			$.email.focus();
		},mensagem:"cadastro_fase_1_erro_mensagem"});
	}
}
function clickFacebook(e){
	if(OS_IOS)$.parent.dialog.loading();
	startFacebook();
	Facebook.authorize();
}
function clickBack(e){
	$.parent.trigger("click", {page:1});
}
exports.update = function(){
	var $formData = $.parent.getFormData();
	$.email.value = $formData.email || "";
};