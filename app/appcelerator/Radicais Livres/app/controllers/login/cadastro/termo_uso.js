
function clickAceito(e){
	if($.ic_aceito.image == "/images/check-box.png")$.ic_aceito.image = "";
	else $.ic_aceito.image = "/images/check-box.png";
}
function clickContinue(e){
	if($.ic_aceito.image == "/images/check-box.png"){
		var _this = this;
		var $data = $.parent.getFormData();
		Ajax.post({
			onComplete:function($result){
				ButtonAnimate.hideLoader(_this, $.parent);
				GlobalFunctions.setDataPerfil($result[0]);
				$.parent.dialog.hideLoading();
				$.parent.trigger("click", {page:11});
			},
			onStart:function(){
				ButtonAnimate.showLoader(_this, $.parent);
			},
			onError:function(e){
				ButtonAnimate.hideLoader(_this, $.parent);
				$.parent.dialog.confirm({
					textLivre:true,
					mensagem:e.msg,
					confirmar:"tentar_novamente",
					onConfirm:function(){
						Ajax.post(e);
					}
				});
			},
			action:"cadastro_perfil",
			data:$data
		});
	}
	else $.parent.dialog.alert({mensagem:"cadastro_fase_7_botao_erro_mensagem"});
}
function clickBack(e){
	$.parent.trigger("click", {page:9});
}