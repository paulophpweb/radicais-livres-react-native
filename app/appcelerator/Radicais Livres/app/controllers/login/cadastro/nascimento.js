function blur(e){
	Triggers.shoot({parans:e.source, type:'text:login-blur'});
}
function focus(e){
	Triggers.shoot({parans:e.source, type:'text:login-focus'});
}

function onChange(e){
	if(GlobalFunctions.getLanguage() == "pt"){
		Mask.mask(e.source, "date");
	}else if(GlobalFunctions.getLanguage() == "en"){
		Mask.mask(e.source, "dateen");
	}else{
		Mask.mask(e.source, "date");
	}
}
function clickContinue(e){
	var $language = GlobalFunctions.getLanguage();
	/*if(!Validate.command.date($.data_nascimento)){
		// gambiart para aprovar na apple.
		if(GlobalFunctions.iosAppleHabilitarValidacao()){
			$.border_data.borderColor = "#cc2128";
			if($.data_nascimento.value.length != 10)$.parent.dialog.alert({mensagem:"cadastro_fase_3_erro_mensagem"});
			else $.parent.dialog.alert({mensagem:"data_invalida"});
		}else{
			$data = $.data_nascimento.value ? $.data_nascimento.value : GlobalFunctions.getDataDefault();
			$.parent.trigger("form-data", {
				data:{
					data_nascimento : $data
				}
			});
			$.parent.trigger("click", {page:5});
		}
	}else{*/
		$.border_data.borderColor = "#5f5c5d";
		$.parent.trigger("form-data", {
			data:{
				data_nascimento : $.data_nascimento.value
			}
		});
		$.parent.trigger("click", {page:5});
	//}
}

function clickBack(e){
	$.parent.trigger("click", {page:3});
}

exports.update = function(){
	var $formData = $.parent.getFormData();
	if($formData.data_nascimento != "" && $formData.data_nascimento != null){
		$.data_nascimento.value = $formData.data_nascimento;
		$.border_data.borderColor = "#5f5c5d";
	}
};