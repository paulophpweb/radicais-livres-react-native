var $id_igreja = null;
function clickSearchIgreja(e){
	Navigate.selectGrid({
		parent:$.parent,
		action:"procurarigreja",
		hintText:'nome_igreja',
		onComplete:function(e){
			$.igraja_nome.text = e.text;
			$.igraja_nome.color = "#3d3b3d";
			$id_igreja = e.igreja_id;
			if(e.igreja_id == 9){
				$.parent.dialog.alert({mensagem:"Caso a sua igreja não esteja cadastrada, entre em contato com o administrador pedrozapublicacoes@gmail.com, ou deixa a sua mensagem na google play ou apple store.",textLivre:true,tempo:7});
			}
		},
		lineItem:function(e){
			return {
				text:e.nome,
				igreja_id:e.igreja_id
			};
		}
	});
}
function clickNext(e){
	if($id_igreja != null){
		$.parent.trigger("form-data", {
			data:{
				igreja_id:$id_igreja
			}
		});
		$.parent.trigger("click", {page:9});
	}
	else $.parent.dialog.alert({mensagem:"selecione_uma_igreja"});
}

function clickBack(e){
	$.parent.trigger("click", {page:7});
}
exports.update = function(){
	
};