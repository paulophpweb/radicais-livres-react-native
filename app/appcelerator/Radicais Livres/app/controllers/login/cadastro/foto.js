
function clickContinue(e){
	if(ModalGalery.getBlog() != null){
		Ti.API.error(ModalGalery.getFile());
		Ti.API.error(ModalGalery.getFile(), ModalGalery.getFile().nativePath);
		$.parent.trigger("form-data", {
    		data:{
	    		foto			: ModalGalery.getFile()
	    	}
    	});
		$.parent.trigger("click", {page:7});
	}
	else $.parent.dialog.alert({mensagem:"cadastro_fase_6_erro_mensagem"});
}

function clickGetFoto(e){
	ModalGalery.show().resizeMedia(600).onComplete(submitImage);
}

function clickPular(e){
	$.parent.trigger("click", {page:7});
}

function clickBack(e){
	$.parent.trigger("click", {page:5});
}

function submitImage($blog){
	$.ic_foto.image = $blog;
	$.ic_foto.width = ModalGalery.getWidth();
	$.ic_foto.height =  ModalGalery.getHeight();
	$.txt_foto.opacity = 0;
};

exports.update = function(){
	var $formData = $.parent.getFormData();
	if($formData.foto != "" && $formData.foto != null){
		if($formData.foto.length<100 && ($.ic_foto.image == null || $.ic_foto.image == "")){
			$.ic_foto.image = "http://graph.facebook.com/"+$formData.foto+"/picture?type=normal";
			$.txt_foto.opacity = 0;
		}
	}
};