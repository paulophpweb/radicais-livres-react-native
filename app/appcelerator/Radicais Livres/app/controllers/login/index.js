exports.baseController = "base";
exports.controllerName = "/index";
$.container.add($.conteudo);
var _this = this;
var _is_root = false;
var _formData = {};
var $childs = [$.login, $.recuperar, $.email];
var $newChilds = ["", "", "", "login/cadastro/nome_sobrenome", "login/cadastro/nascimento", "login/cadastro/sexo", "login/cadastro/foto","login/cadastro/localizacao", "login/cadastro/igreja", "login/cadastro/senha", "login/cadastro/termo_uso", "login/cadastro/salvar_cadastro"];
_.each($childs, function($item){
	if($item != null)$item.parent = _this;
});
if(DataPerfil != null){
	_this.ajaxDefaultLoader({
		onComplete:function($data){
			DataPerfil = null;
			Cookie.removeProperty("cadastro_perfil");
		},
		onError:function(e){
			DataPerfil = null;
			Cookie.removeProperty("cadastro_perfil");
		},
		onStart:function(){
			
		},
		action:"deslogar",
		data:{
			cadastro_id:DataPerfil.cadastro_id
		}
	});
	
}
function navScroll(e){
	_this.blurCurrentFocus();
	if(e.page == 10)$.navigate.scrollingEnabled = false;
	if($newChilds[e.page] != ""){
		var $controller = Alloy.createController($newChilds[e.page]);
			$controller.parent = _this;
		
		var $view = $controller.getView();
		
		$childs.push($controller);
		$.navigate.addView($view);
		console.log($newChilds[e.page],"$newChilds[e.page]");
		if($newChilds[e.page] == "login/cadastro/termo_uso"){
			_this.destroyEvent();
		}
		$newChilds[e.page] = "";
		
	}
	$.navigate.scrollToView($.navigate.views[e.page]);
}

function onScrollEnd(e){
	if($childs[e.currentPage] != null && $childs[e.currentPage].update != null){
		$childs[e.currentPage].update();
	}
}

function navScrollCreateList(e){
	for(var a=0; a<=e.page; a++){
		if($newChilds[a] != ""){
			var $controller = Alloy.createController($newChilds[a]);
			$controller.parent = _this;
		
			var $view = $controller.getView();
			
			$childs.push($controller);
			$.navigate.addView($view);
			
			$newChilds[a] = "";
		}
	}
	$.navigate.scrollToView($.navigate.views[e.page]);
}
function saveData(e){
	_.each(e.data, function(value, key){
		_formData[key] = value;
	});
}
exports.getFormData = function(){
	return _formData;
};
exports.is_root = function(){
	return _is_root;
};

exports.destroy = function(){
	Ti.API.info("login/index/close");
	_this.off("click", navScroll);
	_this.off("nav-scroll-by-index", navScrollCreateList);
	_this.off("form-data", saveData);
};

exports.startup = function(){
	Ti.API.info("login/index/startup");
	_is_root = false;
	_this.on("click", navScroll);
	_this.on("nav-scroll-by-index", navScrollCreateList);
	_this.on("form-data", saveData);
};
exports.boot = function(){
	_is_root = true;
	Navigate.startNavigation($.index, exports, _this);
};

if(!GlobalFunctions.iosAppleHabilitarValidacao() && OS_IOS){
	$.navigate.scrollingEnabled = false;
}
