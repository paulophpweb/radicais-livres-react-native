var $inputs = [$.email, $.entrar];
var ModelUsuario = Alloy.Models.instance('usuario');

function blur(e){
	Triggers.shoot({parans:e.source, type:'text:login-blur'});
}
function focus(e){
	Triggers.shoot({parans:e.source, type:'text:login-focus'});
}
function onReturn(e){
	Validate.next($, e.source);
}
function clickRecuperarSenha(e){
	if(Validate.check($, $inputs)){
		var _this = this;

		// Start
		ModelUsuario.enviar_senha({email:$.email.value},function(){
			ButtonAnimate.showLoader(_this, $.parent.parent);
		// Sucess
		},function($data){
			ButtonAnimate.hideLoader(_this, $.parent.parent);
			if($data.status == "alerta")$.parent.trigger("click", {page:1, email:$.email.value, action:"email"});
			$.parent.parent.dialog.alert({mensagem:$data.msg, tempo:6});
		// Error
		},function(e){
			ButtonAnimate.hideLoader(_this, $.parent.parent);
			$.parent.parent.dialog.confirm({
				mensagem:e.msg,
				textLivre:true,
				confirmar:"tentar_novamente",
				onConfirm:function(){
					Ajax.post(e);
				}
			});
		});
	}else $.parent.parent.dialog.alert({mensagem:"recuperar_erro_email"});
}
function clickBack(e){
	$.parent.parent.trigger("click", {page:1});
}