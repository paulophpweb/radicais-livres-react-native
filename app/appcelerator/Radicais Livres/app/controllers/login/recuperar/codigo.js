var $inputs = [$.codigo, $.entrar];
var ModelUsuario = Alloy.Models.instance('usuario');

function blur(e){
	Triggers.shoot({parans:e.source, type:'text:login-blur'});
}
function focus(e){
	Triggers.shoot({parans:e.source, type:'text:login-focus'});
}
function onReturn(e){
	Validate.next($, e.source);
}
function clickRecuperarSenha(e){
	if(/^\d+$/.test($.codigo.value)){
		var _this = this;
		// Start
		ModelUsuario.checar_codigo({codigo_alterar_senha:$.codigo.value,email:$.parent.getData("email")},function(){
			ButtonAnimate.showLoader(_this, $.parent.parent);
		// Sucess
		},function($data){
			ButtonAnimate.hideLoader(_this, $.parent.parent);
			$.parent.trigger("click", {page:2, cadastro_id:$data.cadastro_id, action:"cadastro_id"});
		// Error
		},function(e){
			ButtonAnimate.hideLoader(_this, $.parent.parent);
			$.parent.parent.dialog.confirm({
				textLivre:true,
				mensagem:e.msg,
				confirmar:"tentar_novamente",
				onConfirm:function(){
					Ajax.post(e);
				}
			});
		});
	}else $.parent.parent.dialog.alert({mensagem:"recuperar_erro_codigo"});
}
function clickBack(e){
	$.parent.trigger("click", {page:0});
}