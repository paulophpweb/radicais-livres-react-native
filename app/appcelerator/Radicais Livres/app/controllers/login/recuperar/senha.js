var $inputs = [$.senha, $.senhacompare, $.entrar];
var ModelUsuario = Alloy.Models.instance('usuario');

function blur(e){
	Triggers.shoot({parans:e.source, type:'text:login-blur'});
}
function focus(e){
	Triggers.shoot({parans:e.source, type:'text:login-focus'});
}
function onReturn(e){
	Validate.next($, e.source);
}

function addInputError($child){
	$["border_"+$child.id].borderColor = "#cc2128";
}
function removeInputError($child){
	$["border_"+$child.id].borderColor = "#5f5c5d";
}

function clickNext(e){
	if(Validate.check($, $inputs,addInputError,removeInputError)){
		var _this = this;
		// Start
		ModelUsuario.atualizar({senha:$.senha.value,cadastro_id:$.parent.getData("cadastro_id")},function(){
			ButtonAnimate.showLoader(_this, $.parent.parent);
		// Sucess
		},function($data){
			ButtonAnimate.hideLoader(_this, $.parent.parent);
			Ti.API.error("status alerta em senha:", JSON.stringify($data));
			$.parent.parent.dialog.alert({mensagem:$data.msg});
			$.parent.parent.trigger("click", {page:1});
		// Error
		},function(e){
			ButtonAnimate.hideLoader(_this, $.parent.parent);
			$.parent.parent.dialog.confirm({
				textLivre:true,
				mensagem:e.msg,
				confirmar:"tentar_novamente",
				onConfirm:function(){
					Ajax.post(e);
				}
			});
		});
	}else{
		$.parent.dialog.alert({mensagem:"cadastro_fase_3_erro_mensagem"});
	}
}