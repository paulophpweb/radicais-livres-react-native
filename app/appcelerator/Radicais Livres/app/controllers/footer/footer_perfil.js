var args = arguments[0] || {};
if($[args.id] != null){
	$[args.id].image = $[args.id].image.replace(".png", "-over.png");
}

function clickMenu(e){
	if(args.id != "lista")$.trigger("click-footer", {controller:"menu/lista"});
}

function clickAmizade(e){
	if(args.id != "amizade")$.trigger("click-footer", {controller:"perfil/amizade", action:"solicitacaosugestaodeamizade"});
}

function clickMensagem(e){
	if(args.id != "mensagem")$.trigger("click-footer", {controller:"perfil/mensagem", action:"getmensagem", type:"window"});
}

function clickContato(e){
	//if(args.id != "mensagem")$.trigger("click-footer", {controller:"perfil/mensagem"});
}

function clickNotificacao(e){
	Ti.API.error(args.id);
	if(args.id != "notificacoes")$.trigger("click-footer", {controller:"perfil/notificacao", action:"getnotificacao",});
}

exports.addQtdeNotGeral = function($obj){
	
	if($obj.solicitacao.total != 0 && $obj.solicitacao.total != null){
		$.solicitacaonot.visible = true;
		$.solicitacaonot.text = $obj.solicitacao.total;
	}else{
		$.solicitacaonot.visible = false;
	}
	
	if($obj.mensagem.total != 0 && $obj.mensagem.total != null){
		$.mensagemnot.visible = true;
		$.mensagemnot.text = $obj.mensagem.total;
	}else{
		$.mensagemnot.visible = false;
	}
	
	if($obj.notificacao.total != 0 && $obj.notificacao.total != null){
		$.notificacaonot.visible = true;
		$.notificacaonot.text = $obj.notificacao.total;
	}else{
		$.notificacaonot.visible = false;
	}
};