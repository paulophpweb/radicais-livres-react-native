var args = arguments[0] || {};

function blur(e){
	console.log("blur");
	Triggers.shoot({parans:e.source, type:'text:login-blur'});
}
function focus(e){
	console.log("focus");
	Triggers.shoot({parans:e.source, type:'text:login-focus'});
}
function clickSearch(e){
	$.trigger("search-header", {value:StringUtil.trim($.search.value), controller:"perfil/sobre/amigo/resultado"});
}
function clickChat(e){
	$.trigger("search-header-sobre", {controller:"chat/listar", action:"getamigosonline"});
}
function clickPesquisaAvancada(e){
	$.trigger("search-header-sobre", {controller:"perfil/sobre/amigo/procurar"});
}
function clickBack(e){
	if($.ic.image == "/images/back-logo.png"){
		Navigate.backWindow();
	}
}
exports.addQtdeNot = function($obj){
	if($obj.chat.total != 0){
		$.qtdeNot.visible = true;
		$.qtdeNot.text = $obj.chat.total;
	}else{
		$.qtdeNot.visible = false;
	}
};
exports.fecharTeclado = function(){
	//$.search.blur();
};
exports.closechat = function(){
	$.search_all.remove($.btn_chat);
	$.container_search.right = 10;
	$.ic.image = "/images/back-logo.png";
	$.ic.height = 48;
};