var args = arguments[0] || {};
var _this = this;
var t = Ti.UI.create2DMatrix();
$.icone.image = args.image;
$.texto.text = args.text;
function clickItem(e){
	_this.parentExports.click({id:args.id});
}
exports.open = function(){
	$.select.transform = t.rotate(180);
	$.group.backgroundColor = "#dcdcdc";
};
exports.close = function(){
	$.select.transform = t.rotate(0);
	$.group.backgroundColor = "transparent";
};