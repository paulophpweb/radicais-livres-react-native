//Ti.API.error("passei por aqui header my");
function clickConfig(e){
	$.parent.openWindow("perfil/sobre/sobre",false,e);
}
function clickEditarFoto(e){
	ModalGalery.show().resizeMedia(600).onComplete(function(blob){
		var $file = ModalGalery.getFile();
		var $data = {};
		$data.foto = $file;
		$.parent.ajaxDefaultLoader({
			onComplete:function($data){
				$.foto_perfil.image = GlobalFunctions.getURL($data.data.foto,$data.data.genero);
				GlobalFunctions.update("foto",$data.data.foto);
				$.parent.dialog.alert({mensagem:$data.msg});
			},
			action:"atualizar",
			data:$data
		});
	});
}
function clickEditarCapa(e){
	ModalGalery.show().resizeMedia(800).onComplete(function(blob){
		var $file = ModalGalery.getFile();
		var $data = {};
		$data.capa = $file;
		$.parent.ajaxDefaultLoader({
			onComplete:function($data){
				$.foto_capa.image = GlobalFunctions.getURL($data.data.capa);
				GlobalFunctions.update("capa",$data.data.capa);
				$.parent.dialog.alert({mensagem:$data.msg});
			},
			action:"atualizar",
			data:$data
		});
	});
}
function clickSobre(e){
	$.parent.openWindow(e.controller, false, e);
}
function clickEscreverMensagem(e){
	$.parent.openWindow("timeline/action/write", false);
}

exports.startup = function($data){
	$.header_menu.off("click-footer", clickSobre);
	$.header_menu.on("click-footer", clickSobre);
	$data = ($data == null || $data.length == null || $data.length == 0) ? [DataPerfil] : $data;
	$.foto_perfil.defaultImage=GlobalFunctions.getImageGenero($data[0].genero);
	$.foto_perfil.image = GlobalFunctions.getURL($data[0].foto,$data[0].genero);
	$.foto_capa.image = GlobalFunctions.getURL(DataPerfil.capa);
	$.foto.image = GlobalFunctions.getURL(DataPerfil.foto, DataPerfil.genero);
};

exports.update = function($args){

};

exports.destroy = function(){
	$.header_menu.off("click-footer", clickSobre);
};