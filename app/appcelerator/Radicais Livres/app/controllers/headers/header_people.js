
var args = arguments[0] || {};
var listaDenuncia = [];
var listaDenunciaId = [];
function clickEnviarMensagem(e){
	$.parent.openWindow("perfil/escrever_mensagem",false,{cadastro_id:e.source.cadastro_id,email:e.source.email,tokenpush:e.source.tokenpush});
}
function clickSolicitarAmizade(e){
	
	if(e.source.solicitou == 0 && e.source.aguardando == 0){
		$.parent.ajaxDefaultLoader({
			onComplete:function($data){
				$.solicitar_amizade.title = L("aguardando_solicitacao");
				$.solicitar_amizade.solicitou = 1;
			},
			action:"solicitaramizade",
			data:{
				cadastro_id_solicitado:e.source.cadastro_id_solicitado,
				tokenpushamizade: e.source.tokenpush,
				notificacao: ""+DataPerfil.nome+" te solicitou amizade."
			}
		});
	}else if(e.source.aguardando == 1){
		$.parent.ajaxDefaultLoader({
			onComplete:function($data){
				$.parent.dialog.alert({mensagem:$data.msg, onComplete: Navigate.backWindow});
				
			},
			action:"aceitaramizade",
			data:{
				cadastro_id_solicitado:e.source.cadastro_id_solicitado,
				solicitacao_id:e.source.solicitacao_id,
				tokenpushamizade: e.source.tokenpush,
				notificacao: "Seu amigo "+DataPerfil.nome+" aceitou sua amizade."
			}
		});
	}else{
		$.parent.dialog.alert({mensagem:L("aguardando_solicitacao")});
	}
}

function clickBloquearPerfil(e){
	if(e.source.bloqueado == 0){
		$.parent.ajaxDefaultLoader({
			onComplete:function($data){
				$.bloquear_perfil.title = L("p_desboquear");
				$.bloquear_perfil.color = "#cc2128";
				$.bloquear_perfil.bloqueado = 1;
			},
			action:"bloquearamigo",
			data:{
				cadastro_id_amizade:e.source.cadastro_id_amizade,
				tokenpushamizade: e.source.tokenpush,
				notificacao: "Seu amigo "+DataPerfil.nome+" bloqueou você."
			}
		});
	 }else{
	 	$.parent.ajaxDefaultLoader({
			onComplete:function($data){
				$.bloquear_perfil.title = L("p_boquear");
				$.bloquear_perfil.color = "#3d3b3d";
				$.bloquear_perfil.bloqueado = "";
			},
			action:"desbloquearamigo",
			data:{
				cadastro_id_amizade:e.source.cadastro_id_amizade,
				tokenpushamizade: e.source.tokenpush,
				notificacao: "Seu amigo "+DataPerfil.nome+" desbloqueou você."
			}
		});
	 }
}

function clickDenunciarPerfil(e){
	
	var lista = {
	  cancel: 0,
	  options: listaDenuncia,
	  selectedIndex: 0,
	  destructive: 0
	};
	
	var dialog = Ti.UI.createOptionDialog(lista);

	dialog.addEventListener("click", function(result){
		if(result.cancel != result.index && result.index>0){
			$.parent.ajaxDefaultLoader({
				onComplete:function($data){
					$.denunciar_perfil.title = "Denunciado";
					$.denunciar_perfil.color = "#cc2128";
					Ti.API.error($data);
				},
				action:"denunciar",
				data:{
					motivo_denuncia_cadastro:listaDenunciaId[result.index],
					cadastro_id_amizade:e.source.cadastro_id_amizade,
					cadastro_id_denunciado:e.source.cadastro_id_amizade,
					denuncia:lista.options[result.index],
					email:e.source.email,
					notificacao: "Seu amigo "+DataPerfil.nome+" denunciou você: "+lista.options[result.index]
				}
			});
		}
	});
	dialog.show();
}
function clickEditarFoto(e){
	
}
function getDenuncia(){
	$.parent.ajaxDefaultLoader({
		onComplete:function($data){
			listaDenuncia.push(L("cancelar"));
			for(var a= 0; a<$data.length; a++){
				listaDenuncia.push($data[a].texto);
				listaDenunciaId.push($data[a].motivo_denuncia_cadastro_id);
			}
		},
		onStart: function(){
			
		},
		action:"getmotivodenuncia"
	});
}
function clickSobre(e){
	$.parent.openWindow(e.controller, false, e);
}
function ClickOpenPhoto(e){
	
	$.parent.openWindow("openImage",false,{controller:"openImage",image: e.source.image.replace("/300x170/","/300xprop/")});
}

exports.startup = function($data){
	if($data){
		$data = $data[0];
		$.foto_perfil.defaultImage=GlobalFunctions.getImageGenero($data.genero);
		$.foto_perfil.image = GlobalFunctions.getURL($data.foto,$data.genero);
		$.foto_perfil.imagem = GlobalFunctions.getURL($data.foto,$data.genero);
		$.foto_capa.image = GlobalFunctions.getURL($data.capa);
		$.enviar_mensagem.cadastro_id=$data.cadastro_id;
		$.enviar_mensagem.email=$data.email;
		$.enviar_mensagem.tokenpush=$data.tokenpush;
		//se for amigo
		if($data.amigo == 1 || $data.is_root || DataPerfil.is_root){
			if($data.is_root == 1 || DataPerfil.is_root){
				$.amigo_d_b.visible = false;
			}else{
				$.amigo_d_b.visible = true;
			}
			$.solicitar_amizade.visible = false;
			//$.solicitar_amizade_icon.visible = false;
			$.bloquear_perfil.cadastro_id_amizade = $data.cadastro_id;
			$.denunciar_perfil.cadastro_id_amizade = $data.cadastro_id;
			$.denunciar_perfil.email = $data.email;
			$.bloquear_perfil.bloqueado = $data.bloqueado;
			$.bloquear_perfil.tokenpush=$data.tokenpush;
			if($data.bloqueado == 1){
				$.bloquear_perfil.title = L("p_desboquear");
				$.bloquear_perfil.color = "#cc2128";
			}
			getDenuncia();
		}else if($data.privacidade_ver_conteudo == "todos" && $data.privacidade_entrar_contato == "todos"){
			$.amigo_d_b.visible = true;
			$.bloquear_perfil.visible = false;
			$.solicitar_amizade.visible = false;
			$.denunciar_perfil.visible = false;
			$.solicitar_amizade.visible = true;
			$.solicitar_amizade.cadastro_id_solicitado = $data.cadastro_id;
			$.solicitar_amizade.solicitou = $data.solicitou;
			$.solicitar_amizade.aguardando = $data.aguardando;
			$.solicitar_amizade.solicitacao_id = $data.solicitacao_id;
			$.solicitar_amizade.tokenpush=$data.tokenpush;
			if($data.solicitou == 1 && $data.aguardando == 0){
				$.solicitar_amizade.title = L("aguardando_solicitacao");
			}else if($data.aguardando == 1){
				$.solicitar_amizade.title = L("aceitar_amizade");
			}
		}else if($data.privacidade_entrar_contato == "somente_amigos"){
			$.enviar_mensagem.visible = false;
			$.amigo_d_b.visible = true;
			$.bloquear_perfil.visible = false;
			$.solicitar_amizade.visible = false;
			$.denunciar_perfil.visible = false;
			$.solicitar_amizade.visible = true;
			$.solicitar_amizade.cadastro_id_solicitado = $data.cadastro_id;
			$.solicitar_amizade.solicitou = $data.solicitou;
			$.solicitar_amizade.aguardando = $data.aguardando;
			$.solicitar_amizade.solicitacao_id = $data.solicitacao_id;
			$.solicitar_amizade.tokenpush=$data.tokenpush;
			if($data.solicitou == 1 && $data.aguardando == 0){
				$.solicitar_amizade.title = L("aguardando_solicitacao");
			}else if($data.aguardando == 1){
				$.solicitar_amizade.title = L("aceitar_amizade");
			}
		}else if($data.privacidade_entrar_contato == "todos"){
			$.enviar_mensagem.visible = true;
			$.amigo_d_b.visible = true;
			$.bloquear_perfil.visible = false;
			$.solicitar_amizade.visible = false;
			$.denunciar_perfil.visible = false;
			$.solicitar_amizade.visible = true;
			$.solicitar_amizade.cadastro_id_solicitado = $data.cadastro_id;
			$.solicitar_amizade.solicitou = $data.solicitou;
			$.solicitar_amizade.aguardando = $data.aguardando;
			$.solicitar_amizade.solicitacao_id = $data.solicitacao_id;
			$.solicitar_amizade.tokenpush=$data.tokenpush;
			if($data.solicitou == 1 && $data.aguardando == 0){
				$.solicitar_amizade.title = L("aguardando_solicitacao");
			}else if($data.aguardando == 1){
				$.solicitar_amizade.title = L("aceitar_amizade");
			}
		}else{
			//$.icone_msg.visible = false;
			$.denunciar_perfil.visible = false;
			$.enviar_mensagem.visible = false;
			$.bloquear_perfil.visible = false;
			$.solicitar_amizade.visible = true;
			//$.solicitar_amizade_icon.visible = true;
			$.solicitar_amizade.cadastro_id_solicitado = $data.cadastro_id;
			$.solicitar_amizade.solicitou = $data.solicitou;
			$.solicitar_amizade.aguardando = $data.aguardando;
			$.solicitar_amizade.solicitacao_id = $data.solicitacao_id;
			$.solicitar_amizade.tokenpush=$data.tokenpush;
			if($data.solicitou == 1 && $data.aguardando == 0){
				$.solicitar_amizade.title = L("aguardando_solicitacao");
			}else if($data.aguardando == 1){
				$.solicitar_amizade.title = L("aceitar_amizade");
			}
		}
	}
	
};

exports.destroy = function(){
	
};