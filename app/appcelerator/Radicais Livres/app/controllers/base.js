var args = arguments[0] || {};
var focusInput = null;
function textBlur(e){
	console.log("focusInput",e);
	focusInput = null;
}
function textFocus(e){
	focusInput = e;
	console.log("textFocus",e);
	Triggers.shoot({type:'text:focus'});
}
function onBlurFocus(e){
	//console.log("onBlurFocus",focusInput);
	if(focusInput != null){
		if(e.source.apiName != 'Ti.UI.TextField'){
			focusInput.blur();
			Triggers.shoot({type:'text:blur'});
			focusInput = null;
		}
	}
}
function addDefaultMethods($obj){
	Ti.API.error("addDefaultMethods",JSON.stringify($obj));
	$obj = $obj || {};
	$obj.onStart = $obj.onStart || function(){
		$.dialog.showLoading();
	}; 
	$obj.onComplete = $obj.onComplete || function(){
		$.dialog.hideLoading();
	};
	
	$obj.onLogout = function(e){
		$.dialog.confirm({
			mensagem:"logout",
			onConfirm:function(){
				exports.openWindow("login/index", true);
			}
		});
	};
	if(!$obj.onError){
		$obj.onAJAXError = $obj.onError = function(e){
			Ti.API.error("ALERT",JSON.stringify(e));
			$.dialog.hideLoading();
			$.dialog.confirm({
				mensagem:e.msg,
				textLivre:true,
				confirmar:"tentar_novamente",
				onConfirm:function(){
					$.dialog.showLoading();
					Ajax.post(e);
				}
			});
		};
	}
	
	

	return $obj;
}
exports.destroyEvent = function(){
	$.container_all.removeEventListener("click", onBlurFocus);
};
exports.openWindow = function($controller, $closeBack, $obj){
	if((DataPerfil == null || DataPerfil.nome == null) && $controller != "login/index"){
		$.dialog.alert({
			mensagem:"logout",
			onComplete:function(){
				DataPerfil = null;
				exports.openWindow("login/index", true);
			}
		});
		return;
	}
	
	$obj = addDefaultMethods($obj);
	
	Ti.API.error("Base.js",$controller, $closeBack, JSON.stringify($obj));
	if($closeBack)Navigate.openWindowAndCloseBack($controller, $obj);
	else Navigate.openWindow($controller, $obj);
};
exports.ajaxDefaultLoader = function($obj){
	Ti.API.error("default loader");
	Ti.API.error("OBJETO INICIAL - "+JSON.stringify($obj));
	if(DataPerfil == null || DataPerfil.nome == null)return;
	var $onComplete = $obj.onComplete;
	
	$obj = addDefaultMethods($obj);
	delete $obj.onAJAXError;
	delete $obj.onComplete;
	$obj.onComplete = function($data){
		$.dialog.hideLoading();
		$onComplete($data);
	};
	Ti.API.error("OBJETO - "+JSON.stringify($obj));
	Ajax.post($obj);
};
exports.blurCurrentFocus = function(){
	if(focusInput != null)focusInput.blur();
	focusInput = null;
};
exports.parentOpen = function(){
	Ti.API.info("base/open");
	Triggers.on({type:"text:login-blur", callBack:textBlur});
	Triggers.on({type:"text:login-focus", callBack:textFocus});
};
exports.parentClose = function(){
	Ti.API.info("base/close");
	Triggers.off({type:"text:login-blur", callBack:textBlur});
	Triggers.off({type:"text:login-focus", callBack:textFocus});
};
exports.screen = function($visible){
	$.look_scree.setVisible($visible);
	$.look_scree.setTouchEnabled($visible);
};
exports.style = function($props){
	for(var a in $props){
		$.index[a] = $props[a];
	}
};

GlobalFunctions.permissaoDeEscrita();
