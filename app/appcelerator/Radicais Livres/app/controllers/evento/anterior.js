var args = arguments[0] || {};
exports.baseController = "base";
exports.controllerName = "/evento/anterior";
$.container.add($.conteudo);
var _this = this;
var $list = [];
var $qtdePerPage = 10;
var $page = 1;
var $qtde = 10;
var intervalevento = "";

function pupularListView($data){
	$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
	for(var a in $data){
		var $p = $data[a];
		var $item = { 
				foto:{
					image:$p.foto ? GlobalFunctions.getURL($p.foto,$p.genero) : ""
				},
				titulo:{
					text:$p.titulo
				},
				localidade:{
					text:$p.endereco
				},
				criado:{
					text:"Por: "+$p.cadastro_nome+" "+$p.cadastro_sobrenome
				},
				data:{
					text:GlobalFunctions.getDataTimeBr($p.data)
				},
				total:{
					text:"total de pessoas"
				},
				qtdepessoa:{
					text:$p.total != 0 && $p.total != null  ? $p.total+" Pessoa(s) comparecerão" : ""
				},
				compareci:{
					text:$p.comparecerei ? "Eu fui" : "Eu não fui"
				},
				dados:{
					comparecerei: $p.comparecerei,
					talvez: $p.talvez,
					nao_posso_ir:$p.nao_posso_ir
				}
			};
			$list.push($item);
	}
			
	$.iterator.setItems($list);	
}

function clickItem(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	Ti.API.error(JSON.stringify(item));
}

function onPull(e){
	 $list = [];
	_this.ajaxDefaultLoader({
		onComplete:function($data){
			$qtdePerPage = 10;
			$page = 1;
			$qtde = 10;
			
			$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
			pupularListView($data);
			$.ptr.hide();
		},
		data:{
			tipo:"anterior"
		},
		action:"geteventoanterior",
		onStart:function(){}
	});
}

function onMarker(e){
	$page = $page+1;
	$qtdePerPage = ($qtde * $page);
	_this.ajaxDefaultLoader({
		onStart:function(){
			$.loaderResultado.start();
			$.loaderResultado.visible = true;
		},
		onComplete:function($data){
			$.loaderResultado.stop();
			$.loaderResultado.visible = false;
			pupularListView($data);
		},
		action:"geteventoanterior",
		data:{
			limit:$qtde,
			offset:($qtde * ($page - 1)),
			tipo:"anterior"
		}
	});
}

function clickHeader(e){
	_this.openWindow(e.controller, false, e);
}

function clickFooter(e){
	_this.openWindow(e.controller, false, e);
}

function updateListViewInterval(){
	intervalevento = setInterval(function() {
	    _this.ajaxDefaultLoader({
			onComplete:function($data){
				$list = [];
				pupularListView($data);
				
			},
			onStart:function(){},
			action:"geteventoanterior",
			data:{
				tipo:"anterior"
			}
		});
	}, 10000);
}

exports.startup = function($data){
	//$.amigo.on("click-footer", clickHeader);
	//$.sobre_footer.on("click-footer", clickFooter);
	pupularListView($data);
	//updateListViewInterval();
};

exports.destroy = function(){
	//$.amigo.off("click-footer", clickHeader);
	//$.sobre_footer.off("click-footer", clickFooter);

};
