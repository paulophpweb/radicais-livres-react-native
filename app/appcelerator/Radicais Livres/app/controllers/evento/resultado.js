var args = arguments[0] || {};
exports.baseController = "base";
exports.controllerName = "/evento/resultado";
$.container.add($.conteudo);
var _this = this;
var $list = [];
var $qtdePerPage = 10;
var $page = 1;
var $qtde = 10;
var intervalevento = "";

function pupularListView($data){
	$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
	for(var a in $data){
		var $p = $data[a];
		var $item = {
				foto:{
					image:$p.foto ? GlobalFunctions.getURL($p.foto,$p.genero) : ""
				},
				titulo:{
					text:$p.titulo
				},
				localidade:{
					text:$p.endereco
				},
				criado:{
					text:"Por: "+$p.cadastro_nome+" "+$p.cadastro_sobrenome
				},
				data:{
					text:GlobalFunctions.getDataTimeBr($p.data)
				},
				total:{
					text:"total de pessoas"
				},
				qtdepessoa:{
					text:$p.total != 0 && $p.total != null  ? $p.total+" Pessoa(s) compareceram" : ""
				},
				comparecerei:{
					backgroundColor:$p.comparecerei ? "#423f41" : "#bababa",
					color:$p.comparecerei ? "#fff" : "#3d3b3d",
					evento_id: $p.evento_id
				},
				talvez:{
					backgroundColor:$p.talvez ? "#423f41" : "#bababa",
					color:$p.talvez ? "#fff" : "#3d3b3d",
					evento_id: $p.evento_id
				},
				nao_posso_ir:{
					backgroundColor:$p.nao_posso_ir ? "#423f41" : "#bababa",
					color:$p.nao_posso_ir ? "#fff" : "#3d3b3d",
					evento_id: $p.evento_id
				},
				dados:{
					comparecerei: $p.comparecerei,
					talvez: $p.talvez,
					nao_posso_ir:$p.nao_posso_ir
				}
			};
			$list.push($item);
	}

	$.iterator.setItems($list);
}

function btComparecer(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	var $acao = "eventopessoaupdate";
	if(item.dados.comparecerei == 0 && item.dados.talvez == 0 && item.dados.nao_posso_ir == 0){
		$acao = "eventopessoainsert";
	}

	if(item.dados.comparecerei == 0){
		$.dialog.confirm({
			textLivre:true,
			mensagem:"Deseja realmente aplicar esta ação?",
			confirmar:"sim",
			onConfirm:function(){
				_this.ajaxDefaultLoader({
					onComplete:function($data){
						// reload nos dados
						_this.ajaxDefaultLoader({
							onComplete:function($data){
								$list = [];
								pupularListView($data);
							},
							onStart:function(){},
							action:"pesquisarevento",
							data:{
								tipo:"proximo",
								estado_id:args.estado_id,
								cidade_id:args.cidade_id,
								titulo:args.titulo
							}
						});
					},
					action:$acao,
					data:{
						evento_id:e.source.evento_id,
						tipo:"comparecerei"
					}
				});
			}
		});
	}
}

function btTalvez(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	var $acao = "eventopessoaupdate";
	if(item.dados.comparecerei == 0 && item.dados.talvez == 0 && item.dados.nao_posso_ir == 0){
		$acao = "eventopessoainsert";
	}

	if(item.dados.talvez == 0){
		$.dialog.confirm({
			textLivre:true,
			mensagem:"Deseja realmente aplicar esta ação?",
			confirmar:"sim",
			onConfirm:function(){
				_this.ajaxDefaultLoader({
					onComplete:function($data){
						// reload nos dados
						_this.ajaxDefaultLoader({
							onComplete:function($data){
								$list = [];
								pupularListView($data);
							},
							onStart:function(){},
							action:"pesquisarevento",
							data:{
								tipo:"proximo",
								estado_id:args.estado_id,
								cidade_id:args.cidade_id,
								titulo:args.titulo
							}
						});
					},
					action:$acao,
					data:{
						evento_id:e.source.evento_id,
						tipo:"talvez"
					}
				});
			}
		});
	}
}

function btNaoPossoIr(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	var $acao = "eventopessoaupdate";
	if(item.dados.comparecerei == 0 && item.dados.talvez == 0 && item.dados.nao_posso_ir == 0){
		$acao = "eventopessoainsert";
	}

	if(item.dados.nao_posso_ir == 0){
		$.dialog.confirm({
			textLivre:true,
			mensagem:"Deseja realmente aplicar esta ação?",
			confirmar:"sim",
			onConfirm:function(){
				_this.ajaxDefaultLoader({
					onComplete:function($data){
						// reload nos dados
						_this.ajaxDefaultLoader({
							onComplete:function($data){
								$list = [];
								pupularListView($data);
							},
							onStart:function(){},
							action:"pesquisarevento",
							data:{
								tipo:"proximo",
								estado_id:args.estado_id,
								cidade_id:args.cidade_id,
								titulo:args.titulo
							}
						});
					},
					action:$acao,
					data:{
						evento_id:e.source.evento_id,
						tipo:"nao_posso_ir"
					}
				});
			}
		});
	}
}

function clickItem(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	Ti.API.error(JSON.stringify(item));
}

function onPull(e){
	Ti.API.error("args",JSON.stringify(args));
	 $list = [];
	_this.ajaxDefaultLoader({
		onComplete:function($data){
			$qtdePerPage = 10;
			$page = 1;
			$qtde = 10;

			$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
			pupularListView($data);
			$.ptr.hide();
		},
		data:{
			tipo:"proximo",
			estado_id:args.estado_id,
			cidade_id:args.cidade_id,
			titulo:args.titulo
		},
		action:"pesquisarevento",
		onStart:function(){}
	});
}

function onMarker(e){
	Ti.API.error(JSON.stringify(args));
	$page = $page+1;
	$qtdePerPage = ($qtde * $page);
	_this.ajaxDefaultLoader({
		onStart:function(){
			$.loaderResultado.start();
			$.loaderResultado.visible = true;
		},
		onComplete:function($data){
			$.loaderResultado.stop();
			$.loaderResultado.visible = false;
			pupularListView($data);
		},
		action:"pesquisarevento",
		data:{
			limit:$qtde,
			offset:($qtde * ($page - 1)),
			tipo:"proximo",
			estado_id:args.estado_id,
			cidade_id:args.cidade_id,
			titulo:args.titulo
		}
	});
}

function clickHeader(e){
	_this.openWindow(e.controller, false, e);
}

function clickFooter(e){
	_this.openWindow(e.controller, false, e);
}

function clickCriar(){
	_this.openWindow("evento/criar", false,{});
}

function updateListViewInterval(){
	intervalevento = setInterval(function() {
	    _this.ajaxDefaultLoader({
			onComplete:function($data){
				$list = [];
				pupularListView($data);

			},
			onStart:function(){},
			action:"pesquisarevento",
			data:{
				tipo:"proximo",
				estado_id:args.estado_id,
				cidade_id:args.cidade_id,
				titulo:args.titulo
			}
		});
	}, 10000);
}

exports.startup = function($data){
	$.amigo.on("click-footer", clickHeader);
	$.sobre_footer.on("click-footer", clickFooter);
	pupularListView($data);
	//updateListViewInterval();
};

exports.destroy = function(){
	$.amigo.off("click-footer", clickHeader);
	$.sobre_footer.off("click-footer", clickFooter);

};
