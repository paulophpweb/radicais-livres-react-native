var args = arguments[0] || {};
exports.baseController = "base";
exports.controllerName = "/evento/proximo";
$.container.add($.conteudo);
var _this = this;
var $list = [];
var $qtdePerPage = 10;
var $page = 1;
var $qtde = 10;
var intervalevento = "";

function pupularListView($data){
	$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
	for(var a in $data){
		var $p = $data[a];
		var $item = { 
				foto:{
					image:$p.foto ? GlobalFunctions.getURL($p.foto,$p.genero) : ""
				},
				titulo:{
					text:$p.titulo
				},
				localidade:{
					text:$p.endereco
				},
				criado:{
					text:"Por: "+$p.cadastro_nome+" "+$p.cadastro_sobrenome
				},
				data:{
					text:GlobalFunctions.getDataTimeBr($p.data)+" até "+GlobalFunctions.getDataTimeBr($p.data_fim)
				},
				total:{
					text:"total de pessoas"
				},
				qtdepessoa:{
					text:$p.total != 0 && $p.total != null  ? $p.total+" Pessoa(s) compareceram" : ""
				},
				comparecerei:{
					backgroundColor:$p.comparecerei ? "#423f41" : "#bababa",
					color:$p.comparecerei ? "#fff" : "#3d3b3d",
					evento_id: $p.evento_id
				},
				talvez:{
					backgroundColor:$p.talvez ? "#423f41" : "#bababa",
					color:$p.talvez ? "#fff" : "#3d3b3d",
					evento_id: $p.evento_id
				},
				nao_posso_ir:{
					backgroundColor:$p.nao_posso_ir ? "#423f41" : "#bababa",
					color:$p.nao_posso_ir ? "#fff" : "#3d3b3d",
					evento_id: $p.evento_id
				},
				saiba_mais:{
					backgroundColor:"#bababa",
					color:"#3d3b3d",
					evento_id: $p.evento_id,
					visible:$p.url ? true : false,
					url:$p.url
				},
				dados:{
					comparecerei: $p.comparecerei,
					talvez: $p.talvez,
					nao_posso_ir:$p.nao_posso_ir
				}
			};
			$list.push($item);
			if($p.url){
				$item.comparecerei.width = "24%";
				$item.talvez.width = "22%";
				$item.nao_posso_ir.width = "22%";
				$item.nao_posso_ir.right = "3%";
				$item.saiba_mais.width = "22%";
			}else{
				$item.comparecerei.width = "31%";
				$item.talvez.width = "31%";
				$item.nao_posso_ir.width = "31%";	
				$item.saiba_mais.width = "31%";
			}
	}
			
	$.iterator.setItems($list);	
}

function btComparecer(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	var $acao = "eventopessoaupdate";
	if(item.dados.comparecerei == 0 && item.dados.talvez == 0 && item.dados.nao_posso_ir == 0){
		$acao = "eventopessoainsert";
	}
	
	if(item.dados.comparecerei == 0){
		_this.ajaxDefaultLoader({
			onComplete:function($data){
				// reload nos dados
				_this.ajaxDefaultLoader({
					onComplete:function($data){
						$list = [];
						pupularListView($data);
					},
					onStart:function(){},
					action:"geteventoproximo",
					data:{
						tipo:"proximo"
					}
				});
			},
			action:$acao,
			data:{
				evento_id:item.comparecerei.evento_id,
				tipo:"comparecerei"
			}
		});
	}
}

function btTalvez(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	var $acao = "eventopessoaupdate";
	if(item.dados.comparecerei == 0 && item.dados.talvez == 0 && item.dados.nao_posso_ir == 0){
		$acao = "eventopessoainsert";
	}
	
	if(item.dados.talvez == 0){
		_this.ajaxDefaultLoader({
			onComplete:function($data){
				// reload nos dados
				_this.ajaxDefaultLoader({
					onComplete:function($data){
						$list = [];
						pupularListView($data);
					},
					onStart:function(){},
					action:"geteventoproximo",
					data:{
						tipo:"proximo"
					}
				});
			},
			action:$acao,
			data:{
				evento_id:item.talvez.evento_id,
				tipo:"talvez"
			}
		});
	}
}

function btNaoPossoIr(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	var $acao = "eventopessoaupdate";
	if(item.dados.comparecerei == 0 && item.dados.talvez == 0 && item.dados.nao_posso_ir == 0){
		$acao = "eventopessoainsert";
	}
	
	if(item.dados.nao_posso_ir == 0){
		_this.ajaxDefaultLoader({
			onComplete:function($data){
				// reload nos dados
				_this.ajaxDefaultLoader({
					onComplete:function($data){
						$list = [];
						pupularListView($data);
					},
					onStart:function(){},
					action:"geteventoproximo",
					data:{
						tipo:"proximo"
					}
				});
			},
			action:$acao,
			data:{
				evento_id:item.nao_posso_ir.evento_id,
				tipo:"nao_posso_ir"
			}
		});
	}
}

function btSaibaMais(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	Ti.Platform.openURL(item.saiba_mais.url);
}

function clickItem(e){
	var section = $.list.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	Ti.API.error(JSON.stringify(item.bindId));
}

function onPull(e){
	 $list = [];
	_this.ajaxDefaultLoader({
		onComplete:function($data){
			$qtdePerPage = 10;
			$page = 1;
			$qtde = 10;
			
			$.list.setMarker({sectionIndex:0,itemIndex:($qtdePerPage - 1)});
			pupularListView($data);
			$.ptr.hide();
		},
		data:{
			tipo:"proximo"
		},
		action:"geteventoproximo",
		onStart:function(){}
	});
}

function onMarker(e){
	$page = $page+1;
	$qtdePerPage = ($qtde * $page);
	_this.ajaxDefaultLoader({
		onStart:function(){
			$.loaderResultado.start();
			$.loaderResultado.visible = true;
		},
		onComplete:function($data){
			$.loaderResultado.stop();
			$.loaderResultado.visible = false;
			pupularListView($data);
		},
		action:"geteventoproximo",
		data:{
			limit:$qtde,
			offset:($qtde * ($page - 1)),
			tipo:"proximo"
		}
	});
}

function clickHeader(e){
	_this.openWindow(e.controller, false, e);
}

function clickFooter(e){
	_this.openWindow(e.controller, false, e);
}

function clickCriar(){
	_this.openWindow("evento/criar", false,{});
}

function updateListViewInterval(){
	intervalevento = setInterval(function() {
	    _this.ajaxDefaultLoader({
			onComplete:function($data){
				$list = [];
				pupularListView($data);
				
			},
			onStart:function(){},
			action:"geteventoproximo",
			data:{
				tipo:"proximo"
			}
		});
	}, 10000);
}

exports.update = function($args){
	onPull({});
};

exports.startup = function($data){
	$.header_evento.selecionarMenu("evento/proximo");
	$.header_evento.on("click-footer", clickHeader);
	//$.sobre_footer.on("click-footer", clickFooter);
	pupularListView($data);
};

exports.destroy = function(){
	$.header_evento.off("click-footer", clickHeader);
	//$.sobre_footer.off("click-footer", clickFooter);

};
