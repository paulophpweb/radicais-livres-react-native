var args = arguments[0] || {};
exports.baseController = "base";
exports.controllerName = "/evento/criar";
$.container.add($.conteudo);
var _this = this;
var $id_estado = null;
var $nm_estado = null;
var $id_cidade = null;
var $nm_cidade = null;
var $lat = null;
var $lng = null;
var $titulo = null;
var $url = "";
var $data_evento = null;
var $data_evento_fim = null;
var $data_en_evento = null;
var $data_en_evento_fim = null;
var $horario_evento = null;
var $endereco = null;
var $sobre = null;
var $foto = null;

function blur(e){
	Triggers.shoot({parans:e.source, type:'text:login-blur'});
}
function focus(e){
	Triggers.shoot({parans:e.source, type:'text:login-focus'});
}
function onReturn(e){
	//Validate.next($, e.source);
}

function onChangeHora(e){
	$horario = $.hora.value.replace(/(\d{2})(\d{2})/, '$1:$2');
	if($horario.length >= 5){
		var v = $horario.slice(0, 5);
		$.hora.value = v;
		$.hora.blur();
	}
}

function onChangeHoraFinal(e){
	$horario = $.horafinal.value.replace(/(\d{2})(\d{2})/, '$1:$2');
	if($horario.length >= 5){
		var v = $horario.slice(0, 5);
		$.horafinal.value = v;
		$.horafinal.blur();
	}
}

function verificaHora(e){
	if($.hora.value.length < 5){
		$.hora.value = "";
	}
}

function verificaHoraFinal(e){
	if($.horafinal.value.length < 5){
		$.horafinal.value = "";
	}
}

function clickSearchEstado(e){
	Navigate.selectGrid({
		parent:$,
		action:"getestado",
		hintText:'estado',
		onCancel:function(e){
			remEstado();
			remCidadeView();
			remLocalidadeView();
			remEnderecoView();
		},
		onComplete:function(e){
			$.estado.text = e.text+" - "+e.sigla;
			$.estado.color = "#3d3b3d";
			$id_estado = e.estado_id;
			$nm_estado = e.text;
			$.viewCidade.visible = true;
			$.viewCidade.touchEnabled = true;
			$.viewCidade.height = 48;
			$.viewCidade.top = 10;
			
			remCidade();
			remLocalidadeView();
			remEnderecoView();
		},
		lineItem:function(e){
			return {
				text:e.nome,
				estado_id:e.estado_id,
				sigla: e.sigla
			};
		}
	});
}

function clickSearchCidade(e){
	Navigate.selectGrid({
		parent:$,
		action:"getcidade",
		data:{estado_id:$id_estado}, 
		hintText:'cidade',
		onCancel:function(e){
			remCidade();
			remLocalidadeView();
			remEnderecoView();
		},
		onComplete:function(e){
			$.cidade.text = e.text;
			$.cidade.color = "#3d3b3d";
			$id_cidade = e.cidade_id;
			$nm_cidade = e.text;
			
			$.viewMapa.visible = true;
			$.viewMapa.touchEnabled = true;
			$.viewMapa.height = 48;
			$.viewMapa.top = 10;
			
			remEnderecoView();
			remLocalidade();
		},
		lineItem:function(e){
			return {
				text:e.nome,
				cidade_id:e.cidade_id
			};
		}
	});
}

function remEstado(){
	$.estado.text = L("estado");
	$.estado.color = "#9aadc5";
	$id_estado = null;
	$nm_estado = null;
}

function remCidade(){
	$.cidade.text = L("cidade");
	$.cidade.color = "#9aadc5";
	$id_cidade = null;
	$nm_cidade = null;
}

function remCidadeView(){
	$.cidade.text = L("cidade");
	$.cidade.color = "#9aadc5";
	$id_cidade = null;
	$nm_cidade = null;
	$.viewCidade.visible = false;
	$.viewCidade.touchEnabled = false;
	$.viewCidade.height = 0;
	$.viewCidade.top = 0;
}

function remLocalidadeView(){
	$.mapa.text = L("localidade");
	$.mapa.color = "#9aadc5";
	$lat = null;
	$lng = null;
	$.viewMapa.visible = false;
	$.viewMapa.touchEnabled = false;
	$.viewMapa.height = 0;
	$.viewMapa.top = 0;
}

function remLocalidade(){
	$.mapa.text = L("localidade");
	$.mapa.color = "#9aadc5";
	$lat = null;
	$lng = null;
}

function remEndereco(){
	$.endereco.text = L("endereco");
	$.endereco.color = "#9aadc5";
	$endereco = null;
}

function remEnderecoView(){
	$.endereco.text = L("endereco");
	$.endereco.color = "#9aadc5";
	$.viewEndereco.visible = false;
	$.viewEndereco.touchEnabled = false;
	$.viewEndereco.height = 0;
	$.viewEndereco.top = 0;
	$endereco = null;
}

function getLatLong(){
	var xhr2 = Titanium.Network.createHTTPClient();
	xhr2.onload = function() {
	   var $latitude = "";
	   var $longitude = "";
	   
	   var jsonObject = JSON.parse(this.responseText);
	   
	   if(jsonObject.status != "ZERO_RESULTS"){
	   		var latLong = jsonObject.results[0].geometry.location;
	   		$latitude = latLong.lat;
	   		$longitude = latLong.lng;
	   }
	
	  Navigate.selectMapa({
			parent:$,
			latitude:$latitude,
			longitude:$longitude,
			onComplete:function(e){
				if(e.latitude && e.longitude){
					$.mapa.text = e.latitude+", "+e.longitude;
					$.mapa.color = "#3d3b3d";
					$lat = e.latitude;
					$lng = e.longitude;
					var xhr3 = Titanium.Network.createHTTPClient();
					xhr3.onload = function() {
						var jsonObject = JSON.parse(this.responseText);
						$.viewEndereco.visible = true;
						$.viewEndereco.touchEnabled = true;
						$.viewEndereco.height = 48;
						$.viewEndereco.top = 10;
						if(jsonObject.results[0]){
							$.endereco.value = jsonObject.results[0].formatted_address;
							$endereco = jsonObject.results[0].formatted_address;
						}
						$.endereco.color = "#3d3b3d";
						
					};
					xhr3.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
					xhr3.open("GET","https://maps.googleapis.com/maps/api/geocode/json?latlng="+e.latitude+", "+e.longitude);
					xhr3.send();
				}
			},
			onCancel:function(e){
				remLocalidade();
				remEnderecoView();
			},
		});
	};
	xhr2.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
	xhr2.open("GET","https://maps.googleapis.com/maps/api/geocode/json?address="+$nm_cidade);
	xhr2.send();
}

function clickSearchMapa(e){
	if($id_estado && $id_cidade){
		getLatLong();
		
	}
}

function clickSetData(e){
	Navigate.selectDatepicker({
		parent:$,
		onComplete:function(e){
			$.data.value = moment(e.data).format("DD/MM/YYYY").toString();
			$data_en_evento = moment(e.data).format("YYYY-MM-DD").toString();
		},
		onCancel:function(e){
			$.data.value = "";
			$data_en_evento = null;
		},
	});
}

function clickSetDataFinal(e){
	Navigate.selectDatepicker({
		parent:$,
		onComplete:function(e){
			$.datafinal.value = moment(e.data).format("DD/MM/YYYY").toString();
			$data_en_evento_fim = moment(e.data).format("YYYY-MM-DD").toString();
		},
		onCancel:function(e){
			$.datafinal.value = "";
			$data_en_evento_fim = null;
		},
	});
}

function getFoto(){
	ModalGalery.show().resizeMedia(110).onComplete(function(blob){
		var $file = ModalGalery.getFile();
		$foto = $file;
		$.foto.image = $file.nativePath;
	});
}

function validarCampos(){
	$retorno = true;
	$validarUrl = GlobalFunctions.isValidUrl($.url.value);
	if($titulo == null || $titulo == ""){
		_this.dialog.alert({mensagem:"err_evento_titulo"});
		$retorno = false;
	}else if(!$validarUrl){
		_this.dialog.alert({mensagem:"err_evento_url"});
		$retorno = false;
	}else if($data_en_evento == null || $data_en_evento == "" || !moment($data_en_evento, 'YYYY-MM-DD').isValid()){
		_this.dialog.alert({mensagem:"err_evento_data"});
		$retorno = false;
	}else if($horario_evento == null || $horario_evento == "" || !moment($horario_evento, 'HH:mm').isValid()){
		_this.dialog.alert({mensagem:"err_evento_horario"});
		$retorno = false;
	}else if($data_en_evento_fim == null || $data_en_evento_fim == "" || !moment($data_en_evento_fim, 'YYYY-MM-DD').isValid()){
		_this.dialog.alert({mensagem:"err_evento_data_fim"});
		$retorno = false;
	}else if($horario_evento_fim == null || $horario_evento_fim == "" || !moment($horario_evento_fim, 'HH:mm').isValid()){
		_this.dialog.alert({mensagem:"err_evento_horario_fim"});
		$retorno = false;
	}else if($id_estado == null || $id_estado == ""){
		_this.dialog.alert({mensagem:"err_evento_estado"});
		$retorno = false;
	}else if($id_cidade == null || $id_cidade == ""){
		_this.dialog.alert({mensagem:"err_evento_cidade"});
		$retorno = false;
	}else if($lat == null || $lat == "" || $lng == null || $lng == ""){
		_this.dialog.alert({mensagem:"err_evento_localidade"});
		$retorno = false;
	}else if($foto == null || $foto == ""){
		_this.dialog.alert({mensagem:"err_evento_foto"});
		$retorno = false;
	}else if($endereco == null || $endereco == ""){
		_this.dialog.alert({mensagem:"err_evento_endereco"});
		$retorno = false;
	}else if(moment($data_en_evento_fim+" "+$horario_evento_fim+":00", 'YYYY-MM-DD HH:mm:ss').unix() < moment($data_en_evento+" "+$horario_evento+":00", 'YYYY-MM-DD HH:mm:ss').unix()){
		_this.dialog.alert({mensagem:"err_evento_datamenor"});
		$retorno = false;
	}
	return $retorno;
}

function salvar(){
	$titulo = $.titulo.value;
	$data_evento = $.data.value;
	$data_evento_fim = $.datafinal.value;
	$horario_evento = $.hora.value;
	$horario_evento_fim = $.horafinal.value;
	$endereco = $.endereco.value;
	$sobre = $.sobre.value;
	$url = $.url.value;
	$validar = validarCampos();
	if($validar){
		_this.ajaxDefaultLoader({
			onComplete:function($data){
				$.dialog.alert({mensagem:'evento_criado', onComplete: Navigate.backWindow});
			},
			action:"eventoinsert",
			data:{
				titulo: $titulo,
				url: $url,
				latitude:$lat,
				longitude:$lng,
				cidade_id:$id_cidade,
				sobre:$sobre,
				data:$data_en_evento+" "+$horario_evento+":00",
				data_fim:$data_en_evento_fim+" "+$horario_evento_fim+":00",
				foto:$foto,
				endereco:$endereco,
				notificacao:"Seu amigo "+DataPerfil.nome+" criou um evento."
			}
		});
	}
	
}

exports.update = function($args){
	
};

exports.startup = function($data){
	
};
exports.destroy = function(){
	
};
