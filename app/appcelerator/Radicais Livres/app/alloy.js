var _ 					= require("alloy/underscore");
var	CloudPush 			= OS_ANDROID ? require('ti.cloudpush') : null;
//var Cloud 				= require('ti.cloud');
var	moment 				= require('alloy/moment');
moment.locale(Ti.Locale.currentLocale);
var	Animation 			= require('alloy/animation');
var	String 				= require('alloy/string');
var	StringUtil 			= require('/StringUtil');
var	SizeView 			= require('/SizeView');
var OS_VERSION			= Number((Ti.Platform.version.split("."))[0]);
	//NappDrawerModule 	= require('dk.napp.drawer'),
var	KeyAuthentication 	= require('/KeyAuthentication');
var	CipherCaesar 		= require('CipherCaesar');
var	Cookie 				= Titanium.App.Properties;//getString, getList, getObject, setBool, setInt
var	DataPerfil			= Cookie.getObject("cadastro_perfil") || {};
var	GlobalFunctions		= {};
var	Navigate 			= require('Navigate');
var	Ajax				= require('Ajax');
var	ButtonAnimate		= require("ButtonAnimate");
var	Triggers			= require('ShootEvents');
var	Mask				= require('form/Mask');
var	Validate			= require('form/Validate');
var	ModalGalery			= require("ModalGalery");
var	Sql 				= require('Sql');
//var	i18n				= require("I18n");
var appVersion			= Cookie.getString("app-version") || {};
//var TiTouchImageView    = OS_ANDROID ? require('org.iotashan.TiTouchImageView') : null;


if(OS_IOS)Ti.UI.iPhone.appBadge = 0;

// Esta funcao serve para mudar a opcao dos campos que nao sao obrigatorios para obrigatorios daqui 30 dias.
GlobalFunctions.iosAppleHabilitarValidacao = function(){
	var $dataHoje = moment().unix();
	var $dataExpiracao = moment("20160202", "YYYYMMDD").day(20).unix();
	/*if($dataExpiracao <= $dataHoje){
		return true;
	}else{
		return false;
	}*/
	return true;
};

if(!appVersion || appVersion == "" ||  Ti.App.version != appVersion){
	if(!DataPerfil)Cookie.removeProperty("cadastro_perfil");
	DataPerfil = null;
	Cookie.setString("app-version",Ti.App.version);
}
SizeView.gridPortrait("foto_galeria", 3,40);
SizeView.setSize("image_capa",780, 506, 0);

GlobalFunctions.update = function(chave,valor){
	DataPerfil[chave] = valor;
	Cookie.setObject("cadastro_perfil",DataPerfil);
};
GlobalFunctions.getLanguage = function(){
	$language = Ti.Locale.currentLanguage;
	if($language.indexOf("-"))$language = $language.split("-")[0];
	return $language;
};
GlobalFunctions.getDataDefault = function(){
	var $language = Ti.Locale.currentLanguage;
	var $data = "";
	if($language.indexOf("-"))$language = $language.split("-")[0];
	if($language == "pt"){
		$data = "22/01/1987";
	}else if($language == "en"){
		$data = "1987-01-22";
	}
	return $data;
};
GlobalFunctions.setDataPerfil = function($data){
	console.log($data);
	DataPerfil = $data;
	GlobalFunctions.setImageGenero();
};
GlobalFunctions.setImageGenero = function(){
	DataPerfil.image_sexo = GlobalFunctions.getImageGenero(DataPerfil.image_sexo);
	Cookie.setObject("cadastro_perfil",DataPerfil);
};
GlobalFunctions.getImageGenero = function($genero){
	if($genero != '' && $genero != null){
		if($genero.toLowerCase() == "masculino")$genero = "/images/masculino.jpg";
		else $genero = "/images/feminino.jpg";
		return $genero;
	}else{
		return '';
	}
};
GlobalFunctions.getDataBr = function($data){
	return moment($data).format("DD/MM/YYYY").toString();
};
GlobalFunctions.getDataTimeBr = function($data){
	return moment($data,"YYYY-MM-DD H:mm:ss").format("DD/MM/YYYY H:mm").toString();
};
GlobalFunctions.getDataTimeNow = function(){
	return moment().format().toString();
};
GlobalFunctions.countDown = function($data){
	return moment($data,"YYYY-MM-DD HH:mm:ss").fromNow();
};
GlobalFunctions.isValidUrl = function (url){
	console.log(url+ " url");
	if(url){
	    var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
	
	    if(RegExp.test(url)){
	        return true;
	    }else{
	        return false;
	    }
    }else{
    	return true;
    }
};
GlobalFunctions.getRegiao = function (region, xPixels, yPixels) {
   var widthInPixels = Number(target.scale(480));
   var heightInPixels = Number(target.scale(800));
   var heightDegPerPixel = -Number(region.latitudeDelta) / heightInPixels; 
   var widthDegPerPixel = Number(region.longitudeDelta) / widthInPixels;
   return {
       latitude : (yPixels - heightInPixels / 2) * heightDegPerPixel + Number(region.latitude),
       longitude : (xPixels - widthInPixels / 2) * widthDegPerPixel + Number(region.longitude)
   };
};

GlobalFunctions.permissaoGeoLocalizacao = function () {

	// The new cross-platform way to check permissions
	// The first argument is required on iOS and ignored on other platforms
	var hasLocationPermissions = Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS);

	// The new cross-platform way to request permissions
	// The first argument is required on iOS and ignored on other platforms
	Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS, function(e) {
		if (!e.success){

			// We already check AUTHORIZATION_DENIED earlier so we can be sure it was denied now and not before
			Ti.UI.createAlertDialog({
				title: 'Permissão negada.',

				// We also end up here if the NSLocationAlwaysUsageDescription is missing from tiapp.xml in which case e.error will say so
				message: e.error
			}).show();
		}
	});
};

GlobalFunctions.permissaoDeEscrita = function(){


	// This is now the cross-platform way to check permissions.
	// The above is still useful as it provides the reason of denial.
	var hasCameraPermissions = Ti.Media.hasCameraPermissions();

	// This iOS-only property is available since Ti 4.0
	if (OS_IOS) {

		// Map constants to names
		var map = {};
		map[Ti.Media.CAMERA_AUTHORIZATION_AUTHORIZED] = 'CAMERA_AUTHORIZATION_AUTHORIZED';
		map[Ti.Media.CAMERA_AUTHORIZATION_DENIED] = 'CAMERA_AUTHORIZATION_DENIED';
		map[Ti.Media.CAMERA_AUTHORIZATION_RESTRICTED] = 'CAMERA_AUTHORIZATION_RESTRICTED';
		map[Ti.Media.CAMERA_AUTHORIZATION_NOT_DETERMINED] = 'CAMERA_AUTHORIZATION_NOT_DETERMINED';

		var cameraAuthorizationStatus = Ti.Media.cameraAuthorizationStatus;

		if (cameraAuthorizationStatus === Ti.Media.CAMERA_AUTHORIZATION_RESTRICTED) {
			return alert('Because permission are restricted by some policy which you as user cannot change, we don\'t request as that might also cause issues.');

		} else if (cameraAuthorizationStatus === Ti.Media.CAMERA_AUTHORIZATION_DENIED) {
			return dialogs.confirm({
				title: 'Permissão negada',
				message: 'Por favor habilite a sua permissão de acesso a camera.',
			});
		}
	}

	// FIXME: https://jira.appcelerator.org/browse/TIMOB-19851
	// You will be prompted to grant to permissions. If you deny either one weird things happen
	Ti.Media.requestCameraPermissions(function(e) {

		if (!e.success) {
			
			alert('Permissão negada');
		}
	});

};

GlobalFunctions.mapa = new function(){
var $loadingsList = {};
	this.remoteUrl = function($map, Map, $arguments){
	if($arguments.tipo == "url"){
		var $url = $arguments.image;
		delete $arguments.image;
		var $md5 = Ti.Utils.md5HexDigest($url)+".jpg";
		var $file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,$md5);
		var $ann = Map.createAnnotation($arguments);
		var newLoader = false;
		if($file.exists()){
			$ann.image = $file.nativePath;//$file.read();
			$map.addAnnotation($ann);
		}else{
			if($loadingsList[$url] == null){
				$loadingsList[$url] = [];
				var xhr = Titanium.Network.createHTTPClient();
				xhr.onload = function(e){
					var $data = this.responseData;
					$file.write($data);
					while($loadingsList[$url].length>0){
						var $item = $loadingsList[$url].pop();
						$item.image = $file.nativePath;
						$map.addAnnotation($item);
					}
				};
				xhr.setTimeout(30000);
				xhr.open('GET', $url);
				xhr.send();
			}
			$loadingsList[$url].push($ann);
		}
	}else{
		var $ann = Map.createAnnotation($arguments);
		$map.addAnnotation($ann);
	}
};
return this;
};

GlobalFunctions.getUrlInBlock = function(texto){
	var regExp = /(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/;
	var match = texto.match(regExp);
	if (match) if (match.length >= 2) return match[0];
};

GlobalFunctions.getYoutubeId = function(url){
	var regExp = /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/ytscreeningroom\?v=|\/feeds\/api\/videos\/|\/user\S*[^\w\-\s]|\S*[^\w\-\s]))([\w\-]{11})[?=&+%\w-]*/ig;
	var match = url.match(regExp);
	if(match != null){
		return url.replace(regExp,
        	'$1');
    }else{
    	return '';
    }
};

GlobalFunctions.getFacebookVideoId = function(url){
	var regExpUsuario = /(?:https?:\/\/)?(?:www\.)?facebook\.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*?(\/)?([\w\-\.]*)/;
	var regExp = /videos\/(\d+)+|v=(\d+)|vb.\d+\/(\d+(?=\/\?|\/?$))/;
	var retorno = {};
	var match = url.match(regExp);
	var match2 = url.match(regExpUsuario);
	if (match) if (match.length >= 2){
		retorno.video_id = match[1];
	}
	if (match2) if (match2.length >= 2){
		retorno.facebook_usuario = match2[2];
	}
	if (retorno) return retorno;
};

GlobalFunctions.GPS = new function(){
	var activity;
	var $despararAlert;
	var alertIsDisable = false;
	var alertDlg = Titanium.UI.createAlertDialog({
	    title:'GPS', 
	    message:"GPS desativado. Deseja Ativa-lo, para uma melhor experiencia.",
	    buttonNames: ['Não', 'Sim']
	});
	
	function handleLocation(e){
	    if (e != null && e.coords != null && e.coords.latitude != null && e.coords.longitude != null){
	        Ti.App.Properties.setDouble("lat",e.coords.latitude);
			Ti.App.Properties.setDouble("lng",e.coords.longitude);
			Ti.App.fireEvent("updade_gps");
	    }else{
	
		for(var a in e){
		
		Ti.API.info(a, e[a]);
		
		}
	
		if(alertDlg != null && !alertIsDisable && $despararAlert){
			alertIsDisable = true;
			alertDlg.show();
		}
	
	setTimeout(function(){
	
			try{
			
			Ti.Geolocation.getCurrentPosition(handleLocation);
			
			}catch(e){
			
			Ti.API.warn("Erro ao tentar carregar sua latitude e longitude.", e);
			
			}
	
	    },1000*10);
	
	   }
	
	};
	
	
	alertDlg.addEventListener('click', clickAlert);
	
	function _inicialize(){
		
		$despararAlert = true;
		Ti.API.error("serviço de localização esta com o status de:", Ti.Geolocation.locationServicesEnabled ? "Sim" :"Não");
		if(!Ti.Geolocation.locationServicesEnabled && alertDlg != null && !alertIsDisable && $despararAlert){
			alertIsDisable = true;
			if(OS_ANDROID)alertDlg.show();
		}else{
			Ti.Geolocation.distanceFilter = 100;
			Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
			Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_GPS;
			Ti.Geolocation.addEventListener('location', handleLocation);
			Ti.Geolocation.getCurrentPosition(handleLocation);
		}
	}
	
	
	function clickAlert(e){
		alertDlg.removeEventListener('click', clickAlert);
		if(e.index == 1){
			if(OS_ANDROID){
				activity = Ti.Android.currentActivity;
				var settingsIntent = Titanium.Android.createIntent({action:'android.settings.LOCATION_SOURCE_SETTINGS'});
				activity.startActivity(settingsIntent);
			}
		}
		
		alertDlg = null;
	
	}
	
	return {
		inicialize:_inicialize
	};

};

GlobalFunctions.ll = function (region, xPixels, yPixels) {
   var widthInPixels = SizeView.pw();
   var heightInPixels = SizeView.ph();
   var heightDegPerPixel = -Number(region.latitudeDelta) / heightInPixels; 
   var widthDegPerPixel = Number(region.longitudeDelta) / widthInPixels;
   return {
       latitude : (yPixels - heightInPixels / 2) * heightDegPerPixel + Number(region.latitude),
       longitude : (xPixels - widthInPixels / 2) * widthDegPerPixel + Number(region.longitude)
   };
};
Navigate.selectGrid = function($obj){
	Navigate
		.openWindow("global/search",{onStart:$obj.parent.dialog.showLoading,onComplete:$obj.parent.dialog.hideLoading})
		.selectGrid($obj);
};
Navigate.selectMapa = function($obj){
	Navigate
		.openWindow("global/mapa",{onStart:$obj.parent.dialog.showLoading,onComplete:$obj.parent.dialog.hideLoading})
		.selectMapa($obj);
};
Navigate.selectDatepicker = function($obj){
	Navigate
		.openWindow("global/datepicker",{onStart:$obj.parent.dialog.showLoading,onComplete:$obj.parent.dialog.hideLoading})
		.selectDatepicker($obj);
};
Navigate.editText = function($obj){
	Navigate
	.openWindow("global/edit",{onStart:$obj.parent.dialog.showLoading,onComplete:$obj.parent.dialog.hideLoading})
	.editText($obj);
};

GlobalFunctions.getURL = function($base, $genero){
	
	if($base != '' && $base != null){
		if($base.indexOf("http://") != -1){
			return $base;
		}
		return Alloy.CFG.apache+$base;
	}else{
		if($genero == "Feminino"){
			return '/images/feminino.jpg';
		}else if($genero == "Masculino"){
			return '/images/masculino.jpg';
		}
		return null;
	}
};
GlobalFunctions.timeline = {
	itemClick:function($obj/*{onComplete:Function}*/){
		
		var $itens = $obj.section.getItemAt($obj.event.itemIndex);
		var $item = $itens[$obj.event.bindId];
		switch($obj.event.bindId){
			case "user":
			case "status_compartilhamento":
			case "user_name":
				if($itens.like.cadastro_id != DataPerfil.cadastro_id)$obj.window({controller:"timeline/people",action: "getperfil",cadastro_id_pessoa: $itens.user.cadastro_id_pessoa, timeline: "true"});
				else $obj.window({controller:"timeline/my", action: "my_gettimeline", timeline: "true"});
				break;
			case "share":
				$obj.window({controller:"timeline/people",action: "getperfil",cadastro_id_pessoa: $itens.share.cadastro_id_compartilhado, timeline: "true"});
				break;
			case "privacidade":
			case "viewPrivacidade":
				$obj.privacidade($itens);
				break;
			case "texto":
				$obj.texto($itens);
				break;
			case "texto_url":
				Ti.Platform.openURL($item.url);
				break;
			case "container_foto":
			case "foto":
				$obj.window({controller:"openImage",image: $itens.foto.image.replace("/300x170/","/300xprop/")});
			break;
			case "container_video":
			case "foto_video":
			case "video_icon":
			case "video_webview":
				if($itens.dados.video_tipo == 'facebook'){
					Ti.Platform.openURL('https://www.facebook.com/'+$itens.dados.facebook_usuario+'/videos/'+$itens.dados.video_id);
				}else if($itens.dados.video_tipo == 'youtube'){
					Ti.Platform.openURL('http://www.youtube.com/embed/'+$itens.dados.video_id+'?autoplay=1');
					//$obj.video($obj);
				}
			break;
			case "like":
				$obj.window({controller:"timeline/action/friends",post_id: $itens.dados.post_id, action:"getusuarioslike"});
				break;
			case "like_button":
			case "isliked":
				$obj.like($itens);
				break;
			case "botao":
				$obj.excluir($itens);
				break;
			case "comentarios":
				$obj.window({controller:"timeline/action/commented",post_id: $itens.dados.post_id, action:"getusuarioscommented"});
				break;
			case "comentarios_button":
				$obj.window({controller:"timeline/action/comment",action: "getcomentarios", post_id: $itens.dados.post_id, tokenpushamizade: $itens.dados.tokenpush, cadastro_id_pessoa: $itens.user.cadastro_id_pessoa, itemIndex: $obj.event.itemIndex, bindId: $obj.event.bindId});
				break;
			case "compartilhar":
				$obj.window({controller:"timeline/action/shared",post_id: $itens.dados.post_id, action:"getusuariosshared"});
				break;
			case "compartilhar_button":
				$obj.compartilhar_button($itens);
				break;
		}
	},
	video : function($url){
		alert("video");
	},
	like : function($obj){
		
	},
	texto : function($obj){
		
	},
	foto : function($obj){
		
	},
	excluir : function($obj){
		
	},
	compartilhar_button : function($obj){
		
	},
	privacidade : function($obj){
		
	}
};

function registerForAndroid(){
	if(CloudPush != null && CloudPush.SUCCESS ){
		Ti.API.info("start retriever Token ANDROID");
		CloudPush.debug = false;
		CloudPush.enabled = true;
		CloudPush.focusAppOnPush = false;
		CloudPush.showTrayNotificationsWhenFocused = false;
		CloudPush.setShowAppOnTrayClick(true);
		CloudPush.addEventListener('callback', function(evt) {
			Ti.API.info(evt,"push aqui");
			var $data = JSON.parse(evt.payload);
			
			//Padronizando o JSON para que tanto o android quanto o IOS tenha o mesmo resultado.
			for(var a in $data.android)$data[a] = $data.android[a];

			Ti.App.fireEvent("notifier", $data);
		});
		CloudPush.retrieveDeviceToken({
			success : function deviceTokenSuccess(e) {
				Ti.API.info("Token ANDROID:",e.deviceToken);
				Cookie.setString("token", e.deviceToken);
			},
			error : function deviceTokenError(e) {
				Titanium.API.error('Failed to register for push! ' + e.error);
			}
		});
	}else Ti.API.error("Não indetificamos o google play services instalado");
}

function registerForIOS(e, $args) {
	Ti.API.info("start retriever Token IOS");
	var $mt = {
		success:function(e){
			Ti.API.info("Token IOS:",e.deviceToken);
			Cookie.setString("token", e.deviceToken);
		},
		error: function(e){
			Ti.API.error("error IOS>=8:"+e.error);
		},
		callback: function(e){
			Ti.App.fireEvent("notifier", e.data);
		}
	};
	for(var a in $args)$mt[a] = $args[a];
	Ti.Network.registerForPushNotifications($mt);
	if(e != null)Ti.App.iOS.removeEventListener('usernotificationsettings', registerForIOS); 
};

function registerForIos(){
	if (OS_VERSION >= 8){
		if(!ENV_DEV){
			Ti.App.iOS.addEventListener('usernotificationsettings', registerForIOS);
			Ti.App.iOS.registerUserNotificationSettings({types:[Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT,Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND,Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE]});
		}
	}else registerForIOS(null, {types: [Ti.Network.NOTIFICATION_TYPE_BADGE,Ti.Network.NOTIFICATION_TYPE_ALERT, Ti.Network.NOTIFICATION_TYPE_SOUND]});
};
GlobalFunctions.h264videosWithYoutubeURL = function(_youtubeId, _callbackOk, _callbackError)
{
        var youtubeInfoUrl = 'http://www.youtube.com/get_video_info?video_id=' + _youtubeId;
        var request = Titanium.Network.createHTTPClient({ timeout : 10000  /* in milliseconds */});
        request.open("GET", youtubeInfoUrl);
        request.onerror = function(_event){
            if (_callbackError)
                _callbackError({status: this.status, error:_event.error});
        };  
        request.onload = function(_event){
            var qualities = {};
            var response = this.responseText;
            var args = getURLArgs(response);
            if (!args.hasOwnProperty('url_encoded_fmt_stream_map'))
            {
                if (_callbackError)
                    _callbackError();
            }
            else
            {
                var fmtstring = args['url_encoded_fmt_stream_map'];
                var fmtarray = fmtstring.split(',');
                for(var i=0,j=fmtarray.length; i<j; i++){
                    var args2 = getURLArgs(fmtarray[i]);
                    var type = decodeURIComponent(args2['type']);
                    if (type.indexOf('mp4') >= 0)
                    {
                        var url = decodeURIComponent(args2['url']);
                        var quality = decodeURIComponent(args2['quality']);
                        qualities[quality] = url;

                    }

                }
                if (_callbackOk)
                    _callbackOk(qualities);
                }
        };
        request.send();


};

function getURLArgs(_string) {
  var args = {};
  var pairs = _string.split("&");
  for(var i = 0; i < pairs.length; i++) {
      var pos = pairs[i].indexOf('=');
          if (pos == -1) continue;
          var argname = pairs[i].substring(0,pos);
          var value = pairs[i].substring(pos+1);
          args[argname] = unescape(value);
      }
      return args;
}

if(OS_ANDROID)registerForAndroid();
else if(OS_IOS)registerForIos();

// convert json to xml
/*var $i18n = "";
var a = "";
for( a in i18n){
$i18n+='<string name="'+a+'">'+i18n[a]+'</string>'+"\n";
}
Ti.API.error($i18n);*/
/*if(OS_ANDROID && Ti.Locale.currentLocale != "pt-BR"){
	L = function($key){
		Ti.API.error("L("+$key+")");
		if($key == "" || $key == null)return "L($key) = null";
		if($key.split(" ").length>1){
			return $key;
		}
		return i18n[$key];
	};
}*/