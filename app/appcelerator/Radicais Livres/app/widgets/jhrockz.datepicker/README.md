jhrockz.datepicker
==================

A Titanium Datepicker widget that works in both iOS and Android. This simple widget enables the user to select a date and input the selection into a specified textfield. When the textfield is tapped, the datepicker slides up and down the screen for iOS and it pops up as a dialog in Android. Based on Tony Nuzzi's wriststrap.picker widget.

Pre-Installation
=====
1. Install Tony Nuzzi's Wriststrap into the app.

Installation
============
1. Download the zipfile and unzip.
2. In the app folder in your mobile app project, make a folder and name it <code>widgets</code>. If the folder exists, just paste <code>jhrockz.datepicker</code> inside it.
3. Edit the config.json file by adding <code>"jhrockz.datepicker": "1.0"</code> inside the <code>"dependencies"</code> section of the config.json.

Notice!
=======
This widget is based off from the wriststrap.picker and, therefore, cannot be added via the <code>Require</code> in the view. Please add it programmatically in the controller.

App Demo:
=========
<a href="https://github.com/jhrockwell/Datepicker-widget-demo">Datepicker Widget Demo</a>



