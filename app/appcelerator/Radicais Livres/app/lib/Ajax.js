var xhr = null;
var isCancel = false;
function _extends($use, $dest){
	for(var a in $use){
		if($dest[a] == null)$dest[a] = $use[a];
	}
}
function _ajax($obj){
	Ti.API.error("AJAX.TUDO",JSON.stringify($obj));
	if($obj.onStart != null){
		if($obj.onStartParans != null) $obj.onStart.apply(this,$obj.onStartParans);
		else $obj.onStart();
	}
	 
	if(!$obj.url){
		$obj.url = Alloy.CFG.node+Alloy.CFG.host[$obj.action];
	}
	$obj.timeout = $obj.timeout || 40;
	xhr = Titanium.Network.createHTTPClient({timeout:1000*$obj.timeout});
	for(var rh in $obj.requestHeader){
		Ti.API.info("Request Header:", rh, $obj.requestHeader[rh]);
		xhr.setRequestHeader(rh, $obj.requestHeader[rh]);
	}
	xhr.onerror = function(e){
		Ti.API.error("AJAX.ONERROR");
		xhr = null;
		if(isCancel){
			isCancel = false;
			if($obj.onCancel != null){
				$obj.onCancel();
			}
		}else{
			Ti.API.error("AJAX.ALERT");
			if($obj.onError != null){
				//if(e.code == -1)$obj.msg = L('semconexao');
				if(e.code == -1)$obj.msg = L('semconexao');
				else $obj.msg = L('errorgenerico');
				Ti.API.error("4) ON ERROR: "+$obj.msg);
				$obj.status = "erro";
				$obj.onError($obj);
			}
		}
	};
	 
	xhr.open($obj.method,$obj.url);
	Ti.API.info("Metodo:", $obj.method, " Url:", $obj.url);
	$obj.data = $obj.data || {};
	$obj.data.lang = Ti.Locale.currentLanguage;
	if($obj.data.lang.indexOf("-"))$obj.data.lang = $obj.data.lang.split("-")[0];
	
	$obj.data.udid = Titanium.Platform.id;
	var excludes = ["controller", "action"];
	
	if(DataPerfil != null && $obj.data != null && $obj.data.cadastro_id == null){
		$obj.data.cadastro_id = DataPerfil.cadastro_id;
		$obj.data.is_root = DataPerfil.is_root;
	}
	if(DataPerfil != null && $obj.data != null && $obj.data.cadastro_id_pessoa == null){
		$obj.data.cadastro_id_pessoa = DataPerfil.cadastro_id;
	}
	var $token = {
		udid:$obj.data.udid,
		key:KeyAuthentication.get()
	};
	//Ti.App.Properties.setDouble("lat",e.coords.latitude);
	//Ti.App.Properties.setDouble("lng",e.coords.longitude);
	//Ti.App.fireEvent("updade_gps");
	$obj.data.lat = Ti.App.Properties.getDouble("lat");
	$obj.data.lng = Ti.App.Properties.getDouble("lng");
	$obj.data.ult_login = GlobalFunctions.getDataTimeNow();
	$obj.data.os = OS_ANDROID ? "android" : "ios";
	$obj.data.tokenpush = Cookie.getString("token");
	$obj.data.token = CipherCaesar.set($token);
	Ti.API.error(JSON.stringify($obj.data));
	var $sendData = {};
	for(var a in $obj.data){
		var $item = StringUtil.trim($obj.data[a]);
		if($item != "" && excludes.indexOf(a) == -1)$sendData[a] = $item;
	}
	Ti.API.info("Post Parans Ajax:", JSON.stringify($sendData));
	try{
		xhr.send($sendData);
	}catch(e){
		Ti.API.error("AJAX.CATCH", e);
	}
	 
	xhr.onload = function(){
		xhr = null;
		Ti.API.info("HTTP CLIENTE", this.responseText);
		if(this.status == '200'){
			Ti.API.info("Action:"+$obj.action+" Method:"+$obj.method+" Status:success("+this.status+")");
			var response;
			if(this.readyState == 4){
				try{
					response = JSON.parse(this.responseText);
				}catch($e){
					Ti.API.error("5)", $e);
					response = {status:"json_erro", msg:$e};
				}
				if(response.status == "sucesso"){
					if($obj.onComplete != null){
						Ti.API.info(JSON.stringify(response.data));
						$obj.onComplete(response.data);
					}
				}else if(response.status == "erro"){
					if($obj.onError != null){
						Ti.API.error("6)", response.msg);
						$obj.msg = response.msg;
						$obj.status = "erro";
						$obj.onError($obj);
					}else{
						Ti.API.error("sem funcao erro");
					}
				}else if(response.status == "alerta"){
					$obj.onComplete(response);
				}else if(response.status == "logout"){
					if($obj.onLogout != null){
						$obj.onLogout(response);
					}else alert("Favor verificar o método LOGOUT para esta página");
				}else{
					if($obj.onError != null){
						$obj.msg = this.responseText;
						Ti.API.error("1)", $obj.msg);
						$obj.status = "erro";
						$obj.onError($obj);
					}
				}
			}else{
				if($obj.onError != null){
					$obj.msg = '2) HTTP Ready State != 4';
					Ti.API.error($obj.msg);
					$obj.status = "erro";
					$obj.onError($obj);
				}
			}           
		}else{
			Ti.API.error("6) Action:"+$obj.action+" Method:"+$obj.method+" Status:error("+this.status+") Response:"+this.response);
			if($obj.onError != null){
				$obj.msg = "3) Response:"+this.response;
				Ti.API.error($obj.msg);
				$obj.status = "erro";
				$obj.onError($obj);
			}
		}              
	};
	return xhr;
}
exports.cancel = function(){
	if(xhr != null){
		try{
			xhr.abort();
			Ti.API.error("TENTANDO CANCELAR O AJAX");
			isCancel = true;
		}catch(e){
			isCancel = false;
			Ti.API.error('FALHA AO CANCELAR O AJAX ERROR:'+e);
		}
		xhr = null;
	}else{
		Ti.API.error('NÃO FOI POSSÍVEL CANCELAR O AJAX');
	}
};
exports.post = function($obj){
	_extends({onComplete:null, onCompleteParans:null,onStart:null,onStartParans:null,onError:null, onCancel:null, onErrorParans:null,data:null, action:""}, $obj);
	$obj.method = "POST";
	return _ajax($obj);
};
exports.get = function($obj){
	_extends({onComplete:null, onCompleteParans:null,onStart:null,onStartParans:null,onError:null, onCancel:null, onErrorParans:null, action:""}, $obj);
	$obj.method = "GET";
	return _ajax($obj);
};