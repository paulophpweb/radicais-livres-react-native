exports.upperCamelCase = function($str){
	if($str == null)return "";
	$str = this.trim($str);
	return $str.replace(/[a-zA-Z\u00C0-\u00FF]+/g,function(w){
		return w[0].toUpperCase() + w.slice(1).toLowerCase();
	});
};
exports.capitalize = function($str){
	if($str == null)return "";
	$str = this.trim($str);
	return $str.charAt(0).toUpperCase() + $str.slice(1).toLowerCase();
};
exports.doisDigitos = function($day){
	if($day>9)return $day;
	return "0"+$day;
};
exports.trim = function($str){
	if($str == null)return "";
	else if(typeof $str == "string")return $str.replace(/^\s+|\s+$/g, "");
	else if(typeof $str == "number")return ""+$str;
	else if(typeof $str == "boolean")return $str ? "1" : "0";
	return $str;
};

exports.splice = function($text, idx, rem, s){
	return ($text.slice(0,idx) + s + $text.slice(idx + Math.abs(rem)));
};

exports.abreviar = function(texto, length, padrao){
	if(texto == null)return "";
	padrao = padrao || "...";
	if(texto.length<length)return texto;
    texto = texto.substr(0,length);
    //texto = texto.substr(0,texto.lastIndexOf(" "));
    return texto+padrao;
};
exports.clear = function(str, espaco){
	espaco = espaco || "=";
    var tmpDois = str.split("/").join(espaco);
    var comAcentos = ["☻","♥","♦","♣","♠","•","◘","○","Š","Œ","Ž","š","œ","ž","¥","µ","À","Á","Â","Ã","Ä","Å","Æ","Ç","È","É","Ê","Ë","Ì","Í","Î","Ï","Ð","Ñ","Ò","Ó","Ô","Õ","Ö","Ø","Ù","Ú","Û","Ü","Ý","ß","à","á","â","ã","ä","å","æ","ç","è","é","ê","ë","ì","í","î","ï","ð","ñ","ò","ó","ô","õ","ö","ø","ù","ú","û","ü","ý","ÿ"];
    var semAcentos = ["_","_","_","_","_","_","_","_","S","O","Z","s","o","z","Y","u","A","A","A","A","A","A","A","C","E","E","E","E","I","I","I","I","D","N","O","O","O","O","O","O","U","U","U","U","Y","s","a","a","a","a","a","a","a","c","e","e","e","e","i","i","i","i","o","n","o","o","o","o","o","o","u","u","u","u","y","y"];
    var tmp = tmpDois.split(" ").join(espaco);
    for(var a=0; a<comAcentos.length; a++)tmp = tmp.split(comAcentos[a]).join(semAcentos[a]);
    return tmp.toLocaleLowerCase();
};