var SizeView = require('/SizeView');
var listWin = [];
var indexView = null;
var indexExportsController = null;
var indexThisController = null;
var navigate = null;

function _startNavigation($winIndex, $controllerExports, $thisController){
	//Titanium.Analytics.featureEvent($controllerExports.controllerName);
	if(navigate == null){
		indexView = $winIndex;
		indexExportsController = $controllerExports;
		indexThisController = $thisController;
		if(OS_IOS){
			navigate = Titanium.UI.iOS.createNavigationWindow({window: $winIndex});
			navigate.open({
			    modal:true,
			    modalTransitionStyle: Ti.UI.iOS.MODAL_TRANSITION_STYLE_FLIP_HORIZONTAL,
			    modalStyle: Ti.UI.iPhone.MODAL_PRESENTATION_FORMSHEET
			});
			navigate.setStatusBarStyle(Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT);
			navigate.hideNavBar();
		}else{
			navigate = $winIndex;
			navigate.open();
		}
		if($thisController != null && $thisController.parentOpen != null)$thisController.parentOpen();
		if($thisController != null && $thisController.startup != null)$thisController.startup();
		if($controllerExports != null && $controllerExports.open != null)$controllerExports.open();
		Ti.Gesture.addEventListener('orientationchange',function(){
			var $controllerExports = _getCurrent();
			if($controllerExports == null && listWin.length==0)$controllerExports = indexExportsController;
			if($controllerExports != null && $controllerExports.resize != null)$controllerExports.resize(SizeView.isPortrait());
		});
		if($controllerExports != null && $controllerExports.resize != null)$controllerExports.resize(SizeView.isPortrait());
	}
};


function _openWindowReset($controlerName, $parans){
	return _openWindow($controlerName, $parans);
};
function _getCurrent(){
	if(listWin != null){
		if(listWin.length>0){
			return listWin[listWin.length-1];
		}
	}
	return null;
};

function _updateCurrent(){
	if(listWin != null){
		if(listWin.length>0){
			var $controller = listWin[listWin.length-1];
			if($controller != null && $controller.update != null)$controller.update();
		}else{
			if(indexExportsController != null && indexExportsController.update != null){
				indexExportsController.update();
			}
		}
	}else{
		if(indexExportsController != null && indexExportsController.update != null){
			indexExportsController.update();
		}
	}
};

function _openWindow($controlerName, $parans){
	$parans = $parans || {};
	
	var $onStart = $parans.onStart;
	var $onComplete = $parans.onComplete;
	var $onLogout = $parans.onLogout;
	if($parans.onLogout != null)delete $parans.onLogout;
	if($parans.onComplete != null)delete $parans.onComplete;
	if($parans.onStart != null)delete $parans.onStart;
	
	
	Ti.API.info("Navigate/openWindow("+$controlerName+", "+JSON.stringify($parans)+");");
	//Titanium.Analytics.featureEvent($controlerName);
	$parans = $parans || {};
	$parans.controllerName = $controlerName;
	var $controller = Alloy.createController($controlerName, $parans);
	var $view = $controller.getView();
	
	listWin.push($controller);
	
	if($parans.action == "" || $parans.action == null){
		if($onStart != null && OS_ANDROID)$onStart();
		_openWindowLoaded($view, $parans, $onComplete, $controller);
	}
	else{
		if($onStart != null)$onStart();
		var $onError = $parans.onAJAXError;
		delete $parans.onAJAXError;
		delete $parans.controllerName;
		Ti.API.error("Navigate Open Window");
		Ajax.post({
			onComplete:function($data){
				if($onComplete != null && OS_IOS)$onComplete();
				_openWindowLoaded($view, $parans, $onComplete, $controller, $data);
			},
			onLogout:$onLogout,
			onError:$onError,
			action:$parans.action,
			data:$parans
		});
	}
	return $controller;
};
function _openWindowLoaded($view, $parans, $onComplete, $controller, $data){
	//FPS
	var $date = new Date();
	$date = $date.getTime();
	
	if(OS_IOS){
		navigate.openWindow($view, {animated:true});
	}else{
		$view.addEventListener("android:back", function(e){
			e.cancelBubble = true;
			_backWindow();
		});
		$view.addEventListener("open", function(e) {
			var $n = new Date();
			Ti.API.info("INTERVAL OPEN MS:", ($n.getTime()-$date));
			if($onComplete != null)$onComplete();
			if($parans.close_back)_backPrevWindow();
		});
		$view.open();
	}
	if($controller != null && $controller.resize != null)$controller.resize(SizeView.isPortrait());
	if($controller != null && $controller.startup != null){
		if($data == null)$controller.startup();
		else $controller.startup($data);
	}
}
function _openWindowAndCloseBack($controlerName, $parans){
	$parans = $parans || {};
	$parans.close_back = true;
	_openWindow($controlerName, $parans);
}
function _backWindow(args){
	if(listWin != null){
		if(listWin.length>0){
			var $controller = listWin.pop();
			_closeItem($controller,args);
		}
	}
};
function _backPrevWindow(){
	var $controller;
	if(listWin.length == 1){
		$controller = indexView;
	}
	else $controller = (listWin.splice(listWin.length-2, 1))[0];
	_closeItem($controller);
};
function _closeItem($controller,args){
	var $view = $controller.getView != null ? $controller.getView() : $controller;
	if($controller != null && $controller.destroy != null)$controller.destroy();
	if(OS_IOS)navigate.closeWindow($view, {animated:true});
	else $view.close();
	
	if(listWin.length>0){
		var $controller = listWin[listWin.length-1];
		if($controller != null && $controller.update != null){
			if(args != null){
				$controller.update(args);
			}else{
				$controller.update();
			}
			
		}
	}else{
		if(indexExportsController != null && indexExportsController.update != null){
			if(args != null){
				indexExportsController.update(args);
			}else{
				indexExportsController.update();
			}
		}
	}
}
function _getPrev($controller){
	var $controller;
	if(listWin.length == 1){
		$controller = indexView;
	}
	else $controller = listWin[listWin.length - 2];
	return $controller;
}
module.exports = {
		updateCurrent:_updateCurrent,
		openWindowReset:_openWindowReset,
		startNavigation:_startNavigation,
		getCurrent:_getCurrent,
		getPrev: _getPrev, 
		openWindow:_openWindow,
		backWindow:_backWindow,
		openWindowAndCloseBack:_openWindowAndCloseBack
};