var globalPath = {};
/*
 * e = {source:*, type:String, callBack:Function}
 * */
exports.on = function(e){
	if(e != null){
		if(e.source == null)e.source = globalPath;
		if(e.type != null){
			if(e.callBack != null){
				e.source[e.type] = e.callBack;
			}else{
				//Ti.API.error("Não é possivel adicionar este evento pois o parametro callBack não foi setado");
			}
		}else{
			//Ti.API.error("Não é possivel adicionar este evento pois o parametro type não foi setado");
		}
	}else{
		//Ti.API.error("Não é possivel adicionar este evento sem nenhum argumento");
	}
};
/*
 * e = {source:*, type:String}
 * */
exports.off = function(e){
	if(e != null){
		if(e.source == null)e.source = globalPath;
		if(e.type != null && e.source[e.type] != null)delete e.source[e.type];
	}else{
		//Ti.API.error("Não é possivel remover este evento sem nenhum argumento");
	}
};
/*
 * e = {source:*, type:String, parans:Object}
 * */
exports.shoot = function(e){
	if(e != null){
		if(e.source == null)e.source = globalPath;
		if(e.type != null){
			if(e.source[e.type] != null){
				if(e.parans == null)e.source[e.type]();
				else e.source[e.type](e.parans);
			}//else Ti.API.error("Não é possivel disparar este evento pois o parametro callBack não foi setado");
		}else{
			//Ti.API.error("Não é possivel disparar este evento pois o parametro type não foi setado");
		}
	}else{
		//Ti.API.error("Não é possivel disparar este evento sem nenhum argumento");
	}
};