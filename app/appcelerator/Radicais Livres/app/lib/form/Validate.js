var $commands = {
	mail : function($child){
		return /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test($child.value);
	},
	phone : function($child){
		return /\([0-9]{1,2}\)\s[0-9]{4,5}[-][0-9]{4,5}/.test($child.value);
	},
	postcode : function($child){
		return /[0-9]{5}-[0-9]{3}/.test($child.value);
	},
	select : function($child){
		return $child.value != "" && $child.value != null;
	},
	min : function($child){
		if($child.value.length>=($child.min || 2))return true;
		else return false;
	},
	equal : function($child){
		if($child.value.length==($child.equal || 2))return true;
		else return false;
	},
	compare : function($child1, $child2){
		if($child1.value == $child2.value)return true;
		return false;
	},
	date : function($child){
		if($child.value.length != 10)return false;
		var comp = $child.value.split('/');
		var lang = "pt";
		if(comp.length != 3){
			comp = $child.value.split('-');
			lang = "en";
		}

		if(comp.length != 3)return false;
		if(lang == "pt"){
			var m = parseInt(comp[1], 10);
			var d = parseInt(comp[0], 10);
			var y = parseInt(comp[2], 10);
		}else{
			var m = parseInt(comp[1], 10);
			var d = parseInt(comp[2], 10);
			var y = parseInt(comp[0], 10);
		}
		var date = new Date(y,m-1,d);
		if (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d)return true;
		return false;
	},
};
exports.command = $commands;
exports.next = function($view, $input){
	if($view[$input.next] != null){
		if($view[$input.next].apiName != 'Ti.UI.Button' && $view[$input.next].apiName != 'Ti.UI.View'){
			$view[$input.next].focus();
		}else{
			$view[$input.next].fireEvent("click");
		}
	}
};

exports.check = function($view, $listView, $callBackAddError, $callBackRemoveError){
	var $actions = {
			upperCamelCase : "min",
			moeda:"min",
			phone : "phone",
			mail : "mail",
			captalize: "min",
			equal:"equal",
			min:"min",
			senha:"min",
			compare:"compare",
			postcode:"postcode",
			select:"select"
		};
	
	var $send = true;
	for(var a in $listView){
		var $child = $listView[a];
		if($child.check == "true"){
			var $action = $actions[$child.action];
			var $arr = [$child];
			if($child.action == "compare")$arr.push($view[$child.compare]);
			if(!$commands[$action].apply(this, $arr)){
				$send = false;
				if($callBackAddError != null)$callBackAddError($child);
			}
			else if($callBackRemoveError != null)$callBackRemoveError($child);
		}
	}
	return $send;
};