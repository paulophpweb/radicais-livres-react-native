exports.mask = function(_field, _function, _genericParameters) {
	switch(_function){
		case "senha":
		case "compare":
		case "min":
		case "mail":
		case "upperCamelCase":
		case "charAtStartLower":
		case "captalize":
			return;
	}
	
	if(_field.action != "select"){
		if(_field.android_prevent_change && OS_ANDROID){
			_field.android_prevent_change = false;
			return;
		}
		if (_genericParameters)_field.value = this[_function](_field.value, _genericParameters);
		else _field.value = this[_function](_field.value);
		if(OS_ANDROID)_field.setSelection(_field.value.length, _field.value.length);
		_field.android_prevent_change = true;
	}
};

exports.generic = function(v, _genericParameters) {
	var _regex = _genericParameters.regex;
	var _syntax = _genericParameters.syntax;
	var _maxValue = _genericParameters.maxValue;
	v = v.replace(/D/g,"");
	v = v.replace(_regex, _syntax);
	return (_maxValue != null) ? v.slice(0, _maxValue) : v;
};

exports.number = function(v){
	v = v.replace(/D/g,"");
	return v;
};

exports.moeda = function(v){
	v = v.replace("R$ ","");
	var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var SeparadorMilesimo = ".";
    var SeparadorDecimal = ",";
    var strCheck = '0123456789';
    var aux = aux2 = '';
    key = v.substring(v.length-1,v.length);
    v = v.substring(0,v.length-1);
    len = v.length;
    for(i = 0; i < len; i++)
        if ((v.charAt(i) != '0') && (v.charAt(i) != SeparadorDecimal)) break;
    aux = '';
    for(; i < len; i++)
        if (strCheck.indexOf(v.charAt(i))!=-1) aux += v.charAt(i);
    aux += key;
    len = aux.length;
    if (len == 0) v = '';
    if (len == 1) v = '0'+ SeparadorDecimal + '0' + aux;
    if (len == 2) v = '0'+ SeparadorDecimal + aux;
    if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += SeparadorMilesimo;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        v = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
        v += aux2.charAt(i);
        v += SeparadorDecimal + aux.substr(len - 2, len);
    }
    return "R$ "+v;
};

exports.postcode = function(v) {
	v = v.replace(/D/g,"");
	v = v.replace(/^(\d{5})(\d)/,"$1-$2");
	return v.slice(0, 9);
};
exports.dateen = function(v){
	v = v.replace(/\D/g,"");
	v = v.replace(/^(\d{4})(\d)/g,"$1-$2");
	if(v.length>7)v = v.replace(/(-\d{2})(\d)/g,"$1-$2");
	v = v.slice(0, 10);
	return v;
};
exports.date = function(v){
	v = v.replace(/\D/g,"");
	v = v.replace(/^(\d\d)(\d)/g,"$1/$2");
	if(v.length>5)v = v.replace(/(\d{2})(\d)/,"$1/$2");
	v = v.slice(0, 10);
	return v;
};
exports.phone = function(v) {
	v = v.replace(/\D/g,"");
	var onzeDigitos = false;
	if(v.length>=11)onzeDigitos = true;
	v = v.replace(/^(\d\d)(\d)/g,"($1) $2");
	if(onzeDigitos)v = v.replace(/(\d{5})(\d)/,"$1-$2");
	else v = v.replace(/(\d{4})(\d)/,"$1-$2");
	v = v.slice(0, 15);
	return v;
};