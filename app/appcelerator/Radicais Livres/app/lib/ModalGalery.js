var imageThumb;
var $img;
var $callBackComplete = null;
var $w_media = 500;
var $media;
var $w_final_croped;
var $h_final_croped;

var tipoFoto = null;
function submitImage(e){
	$media = e.media;
	var $r;
	$r = $media.width/$media.height;
	var $w = $w_media;
	var $h = Math.round($w/$r);
	if($h<$w_media){
		$r = $media.height/$media.width;
		$h = $w_media;
		$w = Math.round($h/$r);
	}
	$h_final_croped = $h;
	$w_final_croped = $w;
	console.log($h,$w);
	$img = $media.imageAsResized($w,$h);
	if($callBackComplete != null)$callBackComplete($img);
}
//Camera transform
function openGaleria(){
	Titanium.Media.openPhotoGallery({
		allowImageEditing : true,
	    success : submitImage,
	    cancel : function() {},
	    error : function(error) {
	    	 $.parent.dialog.alert({mensagem:"cadastro-fase-6-erro-galeria"});
	    },
	    mediaTypes : [Ti.Media.MEDIA_TYPE_PHOTO]
	});
}

function openCamera(){
	Ti.Media.showCamera({
        success : submitImage,
        cancel : function() {},
        error : function(error) {
            var message;
            if (error.code == Ti.Media.NO_CAMERA)message = "cadastro-fase-6-erro-galeria";
            else message = "cadastro-fase-6-erro-padrao";//) + error.code;
            $.parent.dialog.alert({mensagem:message});
        },
        saveToPhotoGallery : true,
        allowEditing : true,
        allowImageEditing : true,
        mediaTypes : [Ti.Media.MEDIA_TYPE_PHOTO]
    });
}
exports.resizeMedia = function($w){
	$w_media = $w;
	return this;
};
exports.show = function(){
	if(tipoFoto == null){ 
		tipoFoto = Ti.UI.createAlertDialog({
			cancel: 1,
			buttonNames:  [L("galeria"), L("cancelar"), L("camera")],
			message: L('camera_ou_galeria'),
			title: L("alert_radicais")
		});
	
	
		tipoFoto.addEventListener('click', function(e){
			if(e.index==2)openCamera();
			else if(e.index==0)openGaleria();
		});
	}
	tipoFoto.show();
	return this;
};
exports.getBase64 = function(){
	if($img == null)return null;
	else{
		if(imageThumb == null){
			
		}else{
			return imageThumb;
		}
	}
	return $img == null ? null : (imageThumb || (imageThumb = Ti.Utils.base64encode($img).toString()));
};
exports.getHeight = function(){
	return $h_final_croped;
};
exports.getWidth = function(){
	return $w_final_croped;
};
exports.getMedia = function(){
	return $media;
};
exports.getBlog = function(){
	return $img;
};
exports.getFile = function(){
	var photo = Ti.Filesystem.getFile(Titanium.Filesystem.tempDirectory, "thumb.jpg");
	photo.write($img);
    return photo.read();
};
exports.destroy = function(){
	$img = null;
	imageThumb = null;
};
exports.onComplete = function($callBack){
	$callBackComplete = $callBack;
	return this;
};