exports.focus = function(e){
	for(var a in this.children){
		this.children[a].opacity = .4;
	}
};
exports.blur = function(e){
	for(var a in this.children){
		this.children[a].opacity = 1;
	}
};

exports.showLoader = function(_this, _base){
	_this.children[1].text = L("carregando");
	_this.children[0].visible = true;
	_this.children[0].opacity = 1;
	_this.children[0].start();
	_base.screen(true);
};

exports.hideLoader = function(_this, _base){
	_this.children[1].text = _this.children[1].textoud;
	_this.children[0].visible = false;
	_this.children[0].opacity = 0;
	_this.children[0].stop();
	_base.screen(false);
};