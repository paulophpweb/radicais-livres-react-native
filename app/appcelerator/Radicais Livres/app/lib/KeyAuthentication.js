var moment = require('alloy/moment');
var StringUtil = require('StringUtil');
function rand(n) {
	return Math.round(Math.random()*n);
}
function mod(dividendo,divisor){
	return Math.round(dividendo - (Math.floor(dividendo/divisor)*divisor));
}
exports.get = function(){
	var n = 9, n1 = rand(n), n2 = rand(n), n3 = rand(n), n4 = rand(n), n5 = rand(n), n6 = rand(n), n7 = rand(n), n8 = rand(n), n9 = rand(n);
	var d1 = n9*2+n8*3+n7*4+n6*5+n5*6+n4*7+n3*8+n2*9+n1*10;
	d1 = 11 - (mod(d1,11));
	if (d1>=10) d1 = 0;
	var d2 = d1*2+n9*3+n8*4+n7*5+n6*6+n5*7+n4*8+n3*9+n2*10+n1*11;
	d2 = 11 - ( mod(d2,11) );
	if (d2>=10) d2 = 0;
	key = ''+n1+n2+n3+n4+n5+n6+n7+n8+n9+d1+d2;
	var nKey = "";
	for(var c = 0; c<key.length; c++)nKey+=Math.floor(Math.random()*9)+key.charAt(c);
	var $date = moment();//.format("YYYY-MM-DD hh:mm");
	var $dia = $date.format("DD");
	var $mes = $date.format("MM");
	var $ano = $date.format("YYYY");
	var $hora = $date.format("HH");
	var $minuto = $date.format("mm");
	var $segundo = $date.format("ss");
	
	nKey = StringUtil.splice(nKey, 5,0, $dia);
	nKey = StringUtil.splice(nKey, 10,0, $mes);
	nKey = StringUtil.splice(nKey, 15,0, $hora);
	nKey = StringUtil.splice(nKey, 20,0, $minuto);
	nKey = StringUtil.splice(nKey, 25,0, $segundo);
	nKey += $ano;
	return nKey;
};
exports.is = function(key){
	if(typeof message != "number")return false;
	if(key.length != 36)return false;
	var $ano = key.slice(key.length-4,key.length);
	key = StringUtil.splice(key, key.length-4,key.length,"");
	
	var $segundo = key.slice(25,27);
	key = StringUtil.splice(key, 25,2,"");
	
	var $minuto = key.slice(20,22);
	key = StringUtil.splice(key, 20,2,"");

	var $hora = key.slice(15,17);
	key = StringUtil.splice(key, 15,2,"");

	var $mes = key.slice(10,12);
	key = StringUtil.splice(key, 10,2,"");

	var $dia = key.slice(5,7);
	key = StringUtil.splice(key, 5,2,"");
	
	var d = new Date($ano+"/"+$mes+"/"+$dia);
	if(!isNaN(d.valueOf()))return false;
	var nKey = "", sum = 0, rest;
	for(var d = 0; d<11; d++)nKey += key.charAt((d*2)+1);
	for (i=1; i<=9; i++) sum = sum + parseInt(nKey.substring(i-1, i)) * (11 - i);
	rest = (sum * 10) % 11;
	if ((rest == 10) || (rest == 11)) rest = 0;
	if (rest != parseInt(nKey.substring(9, 10)) ) return false;
	sum = 0;
	for (i = 1; i <= 10; i++) sum = sum + parseInt(nKey.substring(i-1, i)) * (12 - i);
	rest = (sum * 10) % 11; if ((rest == 10) || (rest == 11)) rest = 0;
	if (rest != parseInt(nKey.substring(10, 11) ) )return false;
	return true;
};