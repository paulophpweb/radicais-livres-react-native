exports.noEmpty = function($value){
	if($value == "" || $value == null || $value == undefined)return false;
	return true;
};
exports.pw = function($subtractDP, $type){
	$type = $type || "dp";
	$subtractDP = $subtractDP || 0;
	var $pxsubtract = exports.dp2px($subtractDP);
	$pxsubtract = $pxsubtract<0 ? 0 : $pxsubtract;
	var $r = Titanium.Platform.displayCaps.platformWidth-$pxsubtract;
	if($type == "dp")$r = exports.px2dp($r);
	return $r;
};
exports.ph = function($subtractDP, $type){
	$type = $type || "dp";
	$subtractDP = $subtractDP || 0;
	var $pxsubtract = exports.dp2px($subtractDP);
	$pxsubtract = $pxsubtract<0 ? 0 : $pxsubtract;
	var $r = Titanium.Platform.displayCaps.platformHeight-$pxsubtract;
	if($type == "dp")$r = exports.px2dp($r);
	return $r;
};
exports.px2dp = function(ThePixels){
		if( OS_IOS )return ThePixels;
	    if ( Titanium.Platform.displayCaps.dpi > 160 )return (ThePixels / (Titanium.Platform.displayCaps.dpi / 160));
	    return ThePixels;
	};
exports.dp2px = function(TheDPUnits){
	if( OS_IOS )return TheDPUnits;
    if ( Titanium.Platform.displayCaps.dpi > 160 )return (TheDPUnits * (Titanium.Platform.displayCaps.dpi / 160));
    return TheDPUnits;
};
exports.isPortrait = function(){
	return Titanium.Platform.displayCaps.platformWidth<Titanium.Platform.displayCaps.platformHeight;
};
exports.gridPortrait = function($name, $col, $subtractStage){
	var $stage = exports.portraitW($subtractStage);
	Ti[$name] = Math.floor($stage/$col);
	Ti[$name+"_stage"] = Ti[$name]*$col;
};
exports.portraitW = function($subtractW){
	if(exports.isPortrait())return exports.pw($subtractW);
	return exports.ph($subtractW);
};
exports.setSize = function($name, $w, $h, $subtractW){
	var $ob = {width:0, height:0};
	$ob.width = exports.portraitW($subtractW);
	var $ratio = $h/$w;
	$ob.height = Math.round($ob.width*$ratio);
	Ti[$name] = $ob;
	Ti.API.error("Ti."+$name+":"+JSON.stringify(Ti[$name]));
};