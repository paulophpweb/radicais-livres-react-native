var moment = require('alloy/moment');
var Cookie = Titanium.App.Properties;
var db;
exports.connection = function(){
	var $ud = Titanium.Platform.getId();
	var $bd_name = $ud+"_"+moment().format("YYYY")+"_02";
	Ti.API.debug("BD_NAME:", $bd_name);
	$bd_name = Titanium.Utils.md5HexDigest($bd_name);
	Ti.API.debug("BD_MD5_NAME:", $bd_name);
	if(db != null){
		try{
			db.close();
		}catch(e){
			Ti.API.error("Falha ao desconectar a db SQLite");
		}
	}
	
	var $bg = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, 'sql/sd.db');
	if($bg.exists()){
		if(Cookie.getString($bd_name) == null || Cookie.getString($bd_name) == ""){
			Ti.API.debug("Novo banco SQLite instalado");
			Ti.Database.install($bg.nativePath,$bd_name);
			Cookie.setString($bd_name, "sim");
		}else Ti.API.debug("banco SQLite está instalado com sucesso");
		db = Ti.Database.open($bd_name);
		if(OS_IOS)db.file.setRemoteBackup(false);
		return db;
	}else{
		Ti.API.error("Banco de dados em 'assets/sql.sd.db' não existe.["+$bg.nativePath+"]");
	}
};