
exports.definition = {
	config: {
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			login: function($data, startCallback, successCallback, errorCallback) {
				var $retorno = Ajax.post({
					onComplete:function($data){
						successCallback($data);
						return $data;
					},
					onStart:function(){
						startCallback();
					},
					onError:function(e){
						errorCallback(e);
						return e;
					},
					action:"login",
					data:$data
				});

				return $retorno;
			},
			atualizar: function($data, startCallback, successCallback, errorCallback) {
				return Ajax.post({
					onComplete:function($data){
						successCallback($data);
						return $data;
					},
					onStart:function(){
						startCallback();
					},
					onError:function(e){
						errorCallback(e);
						return e;
					},
					action:"atualizar",
					data:$data
				});
			},
			enviar_senha: function($data, startCallback, successCallback, errorCallback) {
				return Ajax.post({
					onComplete:function($data){
						successCallback($data);
						return $data;
					},
					onStart:function(){
						startCallback();
					},
					onError:function(e){
						errorCallback(e);
						return e;
					},
					action:"enviarsenha",
					data:$data
				});
			},
			checar_codigo: function($data, startCallback, successCallback, errorCallback) {
				return Ajax.post({
					onComplete:function($data){
						successCallback($data);
						return $data;
					},
					onStart:function(){
						startCallback();
					},
					onError:function(e){
						errorCallback(e);
						return e;
					},
					action:"checarcodigo",
					data:$data
				});
			}
		});

		return Model;
	}
};