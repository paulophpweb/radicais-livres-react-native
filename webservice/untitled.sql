

--
-- Table structure for table `app_post`
--

DROP TABLE IF EXISTS `app_post`;
CREATE TABLE `app_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perfil_id` int(11) DEFAULT NULL,
  `texto` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `app_post_app_usuario_perfil_FK` (`perfil_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;


--
-- Table structure for table `app_post_comentario`
--

DROP TABLE IF EXISTS `app_post_comentario`;
CREATE TABLE `app_post_comentario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `texto` varchar(255) NOT NULL,
  `usuario_perfil_id` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_post_comentario_app_post_FK` (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Table structure for table `app_post_denuncia`
--

DROP TABLE IF EXISTS `app_post_denuncia`;
CREATE TABLE `app_post_denuncia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `usuario_perfil_id` int(11) NOT NULL,
  `texto` varchar(255) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id_app_post_id_idx` (`post_id`),
  KEY `app_post_denuncia_app_usuario_perfil_FK` (`usuario_perfil_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Table structure for table `app_post_imagem`
--

DROP TABLE IF EXISTS `app_post_imagem`;
CREATE TABLE `app_post_imagem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `imagemFd` varchar(255) NOT NULL,
  `mime` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_post_imagem_app_post_FK` (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

--
-- Table structure for table `app_post_like`
--

DROP TABLE IF EXISTS `app_post_like`;
CREATE TABLE `app_post_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `usuario_perfil_id` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_post_like_app_post_FK` (`post_id`),
  KEY `app_post_like_app_usuario_perfil_FK` (`usuario_perfil_id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

--
-- Table structure for table `app_post_palavrao`
--

DROP TABLE IF EXISTS `app_post_palavrao`;
CREATE TABLE `app_post_palavrao` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=latin1;

LOCK TABLES `app_post_palavrao` WRITE;
INSERT INTO `app_post_palavrao` VALUES (1,'ANUS','2017-08-14 13:00:16','2017-08-14 13:00:16'),(2,'BABA-OVO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(3,'BABAOVO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(4,'BABACA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(5,'BACURA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(6,'BAGOS','2017-08-14 13:00:16','2017-08-14 13:00:16'),(7,'BAITOLA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(8,'BEBUM','2017-08-14 13:00:16','2017-08-14 13:00:16'),(9,'BESTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(10,'BICHA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(11,'BISCA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(12,'BIXA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(13,'BOAZUDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(14,'BOCETA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(15,'BOCO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(16,'BOC+','2017-08-14 13:00:16','2017-08-14 13:00:16'),(17,'BOIOLA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(18,'BOLAGATO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(19,'BOQUETE','2017-08-14 13:00:16','2017-08-14 13:00:16'),(20,'BOLCAT','2017-08-14 13:00:16','2017-08-14 13:00:16'),(21,'BOSSETA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(22,'BOSTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(23,'BOSTANA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(24,'BRECHA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(25,'BREXA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(26,'BRIOCO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(27,'BRONHA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(28,'BUCA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(29,'BUCETA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(30,'BUNDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(31,'BUNDUDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(32,'BURRA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(33,'BURRO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(34,'BUSSETA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(35,'CACHORRA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(36,'CACHORRO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(37,'CADELA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(38,'CAGA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(39,'CAGADO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(40,'CAGAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(41,'CAGONA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(42,'CANALHA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(43,'CARALHO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(44,'CASSETA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(45,'CASSETE','2017-08-14 13:00:16','2017-08-14 13:00:16'),(46,'CHECHECA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(47,'CHERECA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(48,'CHIBUMBA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(49,'CHIBUMBO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(50,'CHIFRUDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(51,'CHIFRUDO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(52,'CHOTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(53,'CHOCHOTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(54,'CHUPADA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(55,'CHUPADO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(56,'CLITORIS','2017-08-14 13:00:16','2017-08-14 13:00:16'),(57,'CLIT+RIS','2017-08-14 13:00:16','2017-08-14 13:00:16'),(58,'COCAINA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(59,'COCA-NA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(60,'COCO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(61,'COC+','2017-08-14 13:00:16','2017-08-14 13:00:16'),(62,'CORNA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(63,'CORNO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(64,'CORNUDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(65,'CORNUDO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(66,'CORRUPTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(67,'CORRUPTO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(68,'CRETINA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(69,'CRETINO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(70,'CRUZ-CREDO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(71,'CU','2017-08-14 13:00:16','2017-08-14 13:00:16'),(72,'C+','2017-08-14 13:00:16','2017-08-14 13:00:16'),(73,'CULHAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(74,'CULH+O','2017-08-14 13:00:16','2017-08-14 13:00:16'),(75,'CULH+ES','2017-08-14 13:00:16','2017-08-14 13:00:16'),(76,'CURALHO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(77,'CUZAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(78,'CUZ+O','2017-08-14 13:00:16','2017-08-14 13:00:16'),(79,'CUZUDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(80,'CUZUDO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(81,'DEBIL','2017-08-14 13:00:16','2017-08-14 13:00:16'),(82,'DEBILOIDE','2017-08-14 13:00:16','2017-08-14 13:00:16'),(83,'DEFUNTO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(84,'DEMONIO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(85,'DEM+NIO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(86,'DIFUNTO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(87,'DOIDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(88,'DOIDO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(89,'EGUA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(90,'#NAME?','2017-08-14 13:00:16','2017-08-14 13:00:16'),(91,'ESCROTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(92,'ESCROTO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(93,'ESPORRADA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(94,'ESPORRADO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(95,'ESPORRO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(96,'ESP+RRO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(97,'ESTUPIDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(98,'EST+PIDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(99,'ESTUPIDEZ','2017-08-14 13:00:16','2017-08-14 13:00:16'),(100,'ESTUPIDO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(101,'EST+PIDO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(102,'FEDIDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(103,'FEDIDO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(104,'FEDOR','2017-08-14 13:00:16','2017-08-14 13:00:16'),(105,'FEDORENTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(106,'FEIA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(107,'FEIO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(108,'FEIOSA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(109,'FEIOSO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(110,'FEIOZA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(111,'FEIOZO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(112,'FELACAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(113,'FELA_+O','2017-08-14 13:00:16','2017-08-14 13:00:16'),(114,'FENDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(115,'FODA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(116,'FODAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(117,'FOD+O','2017-08-14 13:00:16','2017-08-14 13:00:16'),(118,'FODE','2017-08-14 13:00:16','2017-08-14 13:00:16'),(119,'FODIDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(120,'FODIDO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(121,'FORNICA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(122,'FUDENDO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(123,'FUDECAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(124,'FUDE_+O','2017-08-14 13:00:16','2017-08-14 13:00:16'),(125,'FUDIDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(126,'FUDIDO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(127,'FURADA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(128,'FURADO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(129,'FURAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(130,'FUR+O','2017-08-14 13:00:16','2017-08-14 13:00:16'),(131,'FURNICA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(132,'FURNICAR','2017-08-14 13:00:16','2017-08-14 13:00:16'),(133,'FURO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(134,'FURONA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(135,'GAIATA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(136,'GAIATO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(137,'GAY','2017-08-14 13:00:16','2017-08-14 13:00:16'),(138,'GONORREA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(139,'GONORREIA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(140,'GOSMA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(141,'GOSMENTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(142,'GOSMENTO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(143,'GRELINHO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(144,'GRELO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(145,'HOMO-SEXUAL','2017-08-14 13:00:16','2017-08-14 13:00:16'),(146,'HOMOSEXUAL','2017-08-14 13:00:16','2017-08-14 13:00:16'),(147,'HOMOSSEXUAL','2017-08-14 13:00:16','2017-08-14 13:00:16'),(148,'IDIOTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(149,'IDIOTICE','2017-08-14 13:00:16','2017-08-14 13:00:16'),(150,'IMBECIL','2017-08-14 13:00:16','2017-08-14 13:00:16'),(151,'ISCROTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(152,'ISCROTO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(153,'JAPA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(154,'LADRA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(155,'LADRAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(156,'LADR+O','2017-08-14 13:00:16','2017-08-14 13:00:16'),(157,'LADROEIRA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(158,'LADRONA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(159,'LALAU','2017-08-14 13:00:16','2017-08-14 13:00:16'),(160,'LEPROSA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(161,'LEPROSO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(162,'LESBICA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(163,'L+SBICA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(164,'MACACA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(165,'MACACO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(166,'MACHONA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(167,'MACHORRA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(168,'MANGUACA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(169,'MANGUA_A','2017-08-14 13:00:16','2017-08-14 13:00:16'),(170,'MASTURBA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(171,'MELECA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(172,'MERDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(173,'MIJA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(174,'MIJADA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(175,'MIJADO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(176,'MIJO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(177,'MOCREA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(178,'MOCR+A','2017-08-14 13:00:16','2017-08-14 13:00:16'),(179,'MOCREIA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(180,'MOCR+IA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(181,'MOLECA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(182,'MOLEQUE','2017-08-14 13:00:16','2017-08-14 13:00:16'),(183,'MONDRONGA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(184,'MONDRONGO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(185,'NABA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(186,'NADEGA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(187,'NOJEIRA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(188,'NOJENTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(189,'NOJENTO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(190,'NOJO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(191,'OLHOTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(192,'OTARIA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(193,'OT-RIA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(194,'OTARIO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(195,'OT-RIO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(196,'PACA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(197,'PASPALHA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(198,'PASPALHAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(199,'PASPALHO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(200,'PAU','2017-08-14 13:00:16','2017-08-14 13:00:16'),(201,'PEIA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(202,'PEIDO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(203,'PEMBA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(204,'PENIS','2017-08-14 13:00:16','2017-08-14 13:00:16'),(205,'P-NIS','2017-08-14 13:00:16','2017-08-14 13:00:16'),(206,'PENTELHA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(207,'PENTELHO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(208,'PERERECA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(209,'PERU','2017-08-14 13:00:16','2017-08-14 13:00:16'),(210,'PER+','2017-08-14 13:00:16','2017-08-14 13:00:16'),(211,'PICA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(212,'PICAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(213,'PIC+O','2017-08-14 13:00:16','2017-08-14 13:00:16'),(214,'PILANTRA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(215,'PIRANHA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(216,'PIROCA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(217,'PIROCO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(218,'PIRU','2017-08-14 13:00:16','2017-08-14 13:00:16'),(219,'PORRA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(220,'PREGA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(221,'PROSTIBULO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(222,'PROST-BULO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(223,'PROSTITUTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(224,'PROSTITUTO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(225,'PUNHETA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(226,'PUNHETAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(227,'PUNHET+O','2017-08-14 13:00:16','2017-08-14 13:00:16'),(228,'PUS','2017-08-14 13:00:16','2017-08-14 13:00:16'),(229,'PUSTULA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(230,'P+STULA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(231,'PUTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(232,'PUTO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(233,'PUXA-SACO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(234,'PUXASACO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(235,'RABAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(236,'RAB+O','2017-08-14 13:00:16','2017-08-14 13:00:16'),(237,'RABO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(238,'RABUDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(239,'RABUDAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(240,'RABUD+O','2017-08-14 13:00:16','2017-08-14 13:00:16'),(241,'RABUDO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(242,'RABUDONA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(243,'RACHA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(244,'RACHADA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(245,'RACHADAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(246,'RACHAD+O','2017-08-14 13:00:16','2017-08-14 13:00:16'),(247,'RACHADINHA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(248,'RACHADINHO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(249,'RACHADO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(250,'RAMELA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(251,'REMELA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(252,'RETARDADA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(253,'RETARDADO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(254,'RIDICULA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(255,'RID-CULA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(256,'ROLA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(257,'ROLINHA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(258,'ROSCA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(259,'SACANA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(260,'SAFADA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(261,'SAFADO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(262,'SAPATAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(263,'SAPAT+O','2017-08-14 13:00:16','2017-08-14 13:00:16'),(264,'SIFILIS','2017-08-14 13:00:16','2017-08-14 13:00:16'),(265,'S-FILIS','2017-08-14 13:00:16','2017-08-14 13:00:16'),(266,'SIRIRICA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(267,'TARADA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(268,'TARADO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(269,'TESTUDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(270,'TEZAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(271,'TEZ+O','2017-08-14 13:00:16','2017-08-14 13:00:16'),(272,'TEZUDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(273,'TEZUDO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(274,'TROCHA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(275,'TROLHA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(276,'TROUCHA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(277,'TROUXA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(278,'TROXA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(279,'VACA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(280,'VAGABUNDA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(281,'VAGABUNDO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(282,'VAGINA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(283,'VEADA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(284,'VEADAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(285,'VEAD+O','2017-08-14 13:00:16','2017-08-14 13:00:16'),(286,'VEADO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(287,'VIADA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(288,'VIADO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(289,'VIADAO','2017-08-14 13:00:16','2017-08-14 13:00:16'),(290,'VIAD+O','2017-08-14 13:00:16','2017-08-14 13:00:16'),(291,'XAVASCA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(292,'XERERECA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(293,'XEXECA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(294,'XIBIU','2017-08-14 13:00:16','2017-08-14 13:00:16'),(295,'XIBUMBA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(296,'XOTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(297,'XOCHOTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(298,'XOXOTA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(299,'XANA','2017-08-14 13:00:16','2017-08-14 13:00:16'),(300,'XANINHA','2017-08-14 13:00:16','2017-08-14 13:00:16');
UNLOCK TABLES;

--
-- Table structure for table `app_post_publicidade`
--

DROP TABLE IF EXISTS `app_post_publicidade`;
CREATE TABLE `app_post_publicidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `imagemFd` varchar(255) NOT NULL,
  `mime` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_post_publicidade_app_post_FK` (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Table structure for table `app_post_video`
--

DROP TABLE IF EXISTS `app_post_video`;
CREATE TABLE `app_post_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `videoFd` varchar(255) NOT NULL,
  `mime` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_post_video_app_post_FK` (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Table structure for table `app_usuario`
--

DROP TABLE IF EXISTS `app_usuario`;
CREATE TABLE `app_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_perfil_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `facebookToken` varchar(255) DEFAULT NULL,
  `pushtoToken` varchar(255) DEFAULT NULL,
  `facebookId` varchar(255) NOT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `auth_token` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `app_usuario_app_usuario_perfil_FK` (`usuario_perfil_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Table structure for table `app_usuario_amigo`
--

DROP TABLE IF EXISTS `app_usuario_amigo`;
CREATE TABLE `app_usuario_amigo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perfil_id_1` int(11) NOT NULL,
  `perfil_id_2` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `action_perfil_id` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_usuario_amigo_app_usuario_perfil_FK` (`perfil_id_1`),
  KEY `app_usuario_amigo_app_usuario_perfil_FK_2` (`perfil_id_2`),
  KEY `app_usuario_amigo_app_usuario_perfil_FK_3` (`action_perfil_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
--
-- Table structure for table `app_usuario_avatar`
--

DROP TABLE IF EXISTS `app_usuario_avatar`;
CREATE TABLE `app_usuario_avatar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avatarFd` varchar(255) NOT NULL,
  `mime` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Table structure for table `app_usuario_config`
--

DROP TABLE IF EXISTS `app_usuario_config`;
CREATE TABLE `app_usuario_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perfil_id` int(11) NOT NULL,
  `gps_privacidade` int(11) DEFAULT NULL,
  `corte_privacidade` int(11) DEFAULT NULL,
  `msg_privacidade` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_usuario_config_app_usuario_perfil_FK` (`perfil_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `app_usuario_notificacao`
--

DROP TABLE IF EXISTS `app_usuario_notificacao`;
CREATE TABLE `app_usuario_notificacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perfil_id_1` int(11) DEFAULT NULL,
  `perfil_id_2` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `texto` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `action_perfil_id` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_usuario_notificacao_app_usuario_perfil_FK` (`perfil_id_1`),
  KEY `app_usuario_notificacao_app_usuario_perfil_FK_3` (`action_perfil_id`),
  KEY `app_usuario_notificacao_app_usuario_perfil_FK_2` (`perfil_id_2`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;

--
-- Table structure for table `app_usuario_perfil`
--

DROP TABLE IF EXISTS `app_usuario_perfil`;
CREATE TABLE `app_usuario_perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relacionamento_id` int(11) DEFAULT NULL,
  `avatar_id` int(11) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `data_nascimento` date NOT NULL,
  `genero` varchar(255) DEFAULT NULL,
  `ministro` int(11) DEFAULT NULL,
  `lider` int(11) DEFAULT NULL,
  `discipulador` int(11) DEFAULT NULL,
  `membro` int(11) DEFAULT NULL,
  `relacionando` int(11) DEFAULT NULL,
  `estado_id` int(11) DEFAULT NULL,
  `cidade_id` int(11) DEFAULT NULL,
  `capa` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_usuario_perfil_app_usuario_relacionamento_FK` (`relacionamento_id`),
  KEY `app_usuario_perfil_app_usuario_avatar_FK` (`avatar_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Table structure for table `app_usuario_relacionamento`
--

DROP TABLE IF EXISTS `app_usuario_relacionamento`;
CREATE TABLE `app_usuario_relacionamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_usuario_relacionamento`
--

LOCK TABLES `app_usuario_relacionamento` WRITE;
INSERT INTO `app_usuario_relacionamento` VALUES (1,'Solteiro',NULL,NULL),(2,'Casado',NULL,NULL);
UNLOCK TABLES;


ALTER TABLE app_post 
 ADD CONSTRAINT `app_post_app_usuario_perfil_FK` FOREIGN KEY (`perfil_id`) REFERENCES `app_usuario_perfil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_post_comentario 
 ADD CONSTRAINT `app_post_comentario_app_post_FK` FOREIGN KEY (`post_id`) REFERENCES `app_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_post_denuncia 
 ADD CONSTRAINT `app_post_denuncia_app_post_FK` FOREIGN KEY (`post_id`) REFERENCES `app_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_post_denuncia 
 ADD CONSTRAINT `app_post_denuncia_app_usuario_perfil_FK` FOREIGN KEY (`usuario_perfil_id`) REFERENCES `app_usuario_perfil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_post_imagem 
 ADD CONSTRAINT `app_post_imagem_app_post_FK` FOREIGN KEY (`post_id`) REFERENCES `app_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_post_like 
 ADD CONSTRAINT `app_post_like_app_post_FK` FOREIGN KEY (`post_id`) REFERENCES `app_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_post_like 
 ADD CONSTRAINT `app_post_like_app_usuario_perfil_FK` FOREIGN KEY (`usuario_perfil_id`) REFERENCES `app_usuario_perfil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_post_publicidade 
 ADD CONSTRAINT `app_post_publicidade_app_post_FK` FOREIGN KEY (`post_id`) REFERENCES `app_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_post_video 
 ADD CONSTRAINT `app_post_video_app_post_FK` FOREIGN KEY (`post_id`) REFERENCES `app_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_usuario 
 ADD CONSTRAINT `app_usuario_app_usuario_perfil_FK` FOREIGN KEY (`usuario_perfil_id`) REFERENCES `app_usuario_perfil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_usuario_amigo 
 ADD CONSTRAINT `app_usuario_amigo_app_usuario_perfil_FK` FOREIGN KEY (`perfil_id_1`) REFERENCES `app_usuario_perfil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_usuario_amigo 
 ADD CONSTRAINT `app_usuario_amigo_app_usuario_perfil_FK_2` FOREIGN KEY (`perfil_id_2`) REFERENCES `app_usuario_perfil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_usuario_amigo 
 ADD CONSTRAINT `app_usuario_amigo_app_usuario_perfil_FK_3` FOREIGN KEY (`action_perfil_id`) REFERENCES `app_usuario_perfil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_usuario_config 
 ADD CONSTRAINT `app_usuario_config_app_usuario_perfil_FK` FOREIGN KEY (`perfil_id`) REFERENCES `app_usuario_perfil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_usuario_notificacao 
 ADD CONSTRAINT `app_usuario_notificacao_app_usuario_perfil_FK` FOREIGN KEY (`perfil_id_1`) REFERENCES `app_usuario_perfil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_usuario_notificacao 
 ADD CONSTRAINT `app_usuario_notificacao_app_usuario_perfil_FK_2` FOREIGN KEY (`perfil_id_2`) REFERENCES `app_usuario_perfil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_usuario_notificacao 
 ADD CONSTRAINT `app_usuario_notificacao_app_usuario_perfil_FK_3` FOREIGN KEY (`action_perfil_id`) REFERENCES `app_usuario_perfil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_usuario_perfil 
 ADD CONSTRAINT `app_usuario_perfil_app_usuario_avatar_FK` FOREIGN KEY (`avatar_id`) REFERENCES `app_usuario_avatar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE app_usuario_perfil 
 ADD CONSTRAINT `app_usuario_perfil_app_usuario_relacionamento_FK` FOREIGN KEY (`relacionamento_id`) REFERENCES `app_usuario_relacionamento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

