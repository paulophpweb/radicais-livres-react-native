<?php

class App_Controller_BaseController extends Zend_Controller_Action
{
	public $models = array();
	public $modelAtual = '';
	public $msg = null;
	
    public function init(){
    	// pega os dados da sessao
    	$identity = Zend_Auth::getInstance()->getIdentity();
    	$this->view->sessao = $identity;
		// classes uteis para usar
        $this->uteis = new App_AbstractController();
		// nome do modulo
		$this->modulo = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
		$this->view->modulo = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
		// nome do controller
		$this->controle = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$this->view->controle = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		// nome da acao
		$this->acao = Zend_Controller_Front::getInstance()->getRequest()->getActionName();
		$this->view->acao = Zend_Controller_Front::getInstance()->getRequest()->getActionName();
		// pega o id do usuario logado
		if(isset($identity->id))
		  $this->idUsuario = $identity->id_usuario;
		if(count($this->models)){
			// loader dos models $models 
			foreach ($this->models as $value) {
				$modelLoader = 'Sistema_Model_'.$value.'';
				$modelName = 'model'.$value.'';
				$this->{$modelName} = new $modelLoader();
			}
		}
		// inclue o model de logs
		$this->modelLog = new Sistema_Model_Logs();
		if($this->modelAtual){
			// inclue o model do controle atual
			$modelAtualLoader = 'Sistema_Model_'.$this->modelAtual.'';
			$this->model = new $modelAtualLoader();
		}
		// Desabilita o layout sempre que uma requisição ajax ocorrer.
		if($this->getRequest()->isXmlHttpRequest()) {
		    
		}
    }
    
    /**
	 * Lista os dados na view
	 */
	public function indexAction()
	{
	    // verifica se tem acao para remover
	    $this->view->remover = Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->modulo.":".$this->controle, "remover");
	    $this->view->alterar = Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->modulo.":".$this->controle, "alterar");
	    
	    if ($this->getRequest()->isXmlHttpRequest()) {
	        $this->_helper->layout()->disableLayout();
	        $this->_helper->viewRenderer->setNoRender(true);

	        $offset        		= $this->_getParam('start',0);
	        $registroPagina     = $this->_getParam('length',10);
	        $pesquisa           = $this->_getParam('search','');
	        
	        $aPesquisa = array();
	        $order = "";
            
	        // faz a pesquisa
	        if(!is_numeric($pesquisa['value'])){
	            $aPesquisa['valor'] = urldecode($pesquisa['value']);
	        }else{
	            $aPesquisa['valor'] = intval($pesquisa['value']);
	        }
	        
	        // pega os dados de ordenacao
	        if($this->_getParam("order")){
	            $ordenar = $this->_getParam("order");
	            if($ordenar[0]['column'] && $ordenar[0]['dir']){
	               $coluna = $this->_getParam("columns");
	               $order = $coluna[$ordenar[0]['column']]['data']." ".$ordenar[0]['dir'];
	            }
	            $parametro = $this->_getParam("sorting");
	        }
	        $res = $this->model->listarTodos($aPesquisa,$registroPagina,$offset,$order);
	        foreach ($res["res"] as $key => $value)
	        {
	            if($this->view->alterar)
	               $res["res"][$key]['alterar'] = '<a class="btn btn-info btn-sm" href="'.($this->view->url(array("module" => "sistema","controller"=>$this->controle, "action"=>"form","id" => $res["res"][$key][$this->model->primarykey]),null,true)).'">Editar</a>';
	            if($this->view->remover)
	               $res["res"][$key]['remover'] = '<a class="btn btn-danger btn-sm" onclick="DeletarIndex('.($res["res"][$key][$this->model->primarykey]).')" href="javascript:;">Remover</a>';
	        }

	        echo json_encode(array("data" => $res["res"],'draw' => $this->_getParam("draw",'1'),'recordsTotal' => count($res["res"]),'recordsFiltered' => $res['total']));
	    }
		
	}
    
    /**
     * Formulario de incluir ou alterar
     */
    public function formAction()
    {
    
    }

	public function excluirAction()
	{
	    
	    $resposta = array();
	    $array = false;
	    $condicao = true;
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout()->disableLayout();
	     
	    $ids = $this->getRequest()->getParam('id');
	    if($ids){
	        if(is_array($ids)){
	            $array = true;
	            $ids = implode(",", $this->getRequest()->getParam('id'));
	            $integerIDs = array_map('intval', $this->getRequest()->getParam('id'));
	        }
            // chama a funcao excluir
            $result = $this->model->excluir($ids,$this->msg);
    
            if($result){
                $resposta['status'] = "sucesso";
                $resposta['msg'] = $this->msg;
            }else{
                $resposta['status'] = "erro";
                $resposta['msg'] = $this->msg;
            }
    
            echo json_encode($resposta);
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Nenhum dado recebido!";
	        echo json_encode($resposta);
	    }
	}
	
	/**
	 * Abre o modal de pesquisa
	 */
	public function modalAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->view->campos = $this->getRequest()->getParam('campos');
	    $campoJson = array();
	    if($this->getRequest()->getParam('campos')){
	        foreach ($this->getRequest()->getParam('campos') as $key => $value){
	            $campoJson[] = array('data' => $key);
	        }
	    }
	    $this->view->camposJson = json_encode($campoJson);
	    $this->view->camposSelecionar = json_encode($this->getRequest()->getParam('selecionar'));
	    $this->view->setScriptPath("../library/App/views/scripts");
	    $this->renderScript("global/modal.phtml");
	}
	
	/**
	 * Metodo para abrir o modal da webcam
	 */
	public function webcamAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->view->setScriptPath("../library/App/views/scripts");
	    $this->renderScript("global/webcam.phtml");
	}
	
	/**
	 * metodo para recortar a imagem
	 */
	public function recortarImagemAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->view->setScriptPath("../library/App/views/scripts");
	    $this->renderScript("global/recortar-imagem.phtml");
	}
	
	public function gravarLog($descricao){
		 // Grava o Log
        $data = array();
        $data['modulo'] = $this->modulo;
        $data['controller'] = $this->controle;
        $data['metodo'] = $this->acao;
        $data['ip'] = $_SERVER['REMOTE_ADDR'];
        $data['data'] = time();
        $data['usuario_id'] = Zend_Auth::getInstance()->getIdentity()->id;
        $data['usuario_nome'] = Zend_Auth::getInstance()->getIdentity()->nome;
        $data['descricao'] = $descricao;
        return $this->modelLog->save($data);
	}

}