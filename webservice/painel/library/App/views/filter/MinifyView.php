<?php
class App_views_filter_MinifyView implements Zend_Filter_Interface
{
    public function filter($string)
    {
        return preg_replace(
            array('/>/', '/\s+</', '/[\r\n]+/'),
            array('>', '<', ' '),
            $string
        );
    }
}