<?php
class Zend_View_Helper_TotalUsuario extends Zend_View_Helper_Abstract
{
    public function TotalUsuario()
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$chAction     = strtolower(Zend_Controller_Front::getInstance()->getRequest()->getActionName());
		$chController = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		
        $sql  = 'select ';
		$sql .= '	count(*) as total ';
		$sql .= 'from ';
		$sql .= '	sca_usuario u ';
		
		$result = $db->fetchRow($sql);
		
		return $result["total"];
		
	}
}