<?php
class Zend_View_Helper_Awsome extends Zend_View_Helper_Abstract
{
    
    /**
     * Font Awesome
     * Exemplo:
     * <select class="fontawesome-select form-control input-sm">
     <?php foreach ($this->helper->fontAwesome() as $key => $value):?>
     <option value="<?php echo $value;?>">&#x<?php echo $key;?>; <?php echo $value;?></option>
     <?php endforeach;?>
     </select>
     *
     * @param string $path css font awesome
     * @param string $class_prefix prefixo da fonte padrao = `fa-`
     * @return array
     */
    public function Awsome($class_prefix = 'fa-')
    {
        $path = 'media/sistema/dist/css/font-awesome.css';
        if(!file_exists($path)){
            return false;//se o caminho não existe retorna falso.
        }
        $css = file_get_contents($path);
        $pattern = '/\.('. $class_prefix .'(?:\w+(?:-)?)+):before\s+{\s*content:\s*"(.+)";\s+}/';
        preg_match_all($pattern, $css, $matches, PREG_SET_ORDER);
    
        $icons = array('' => '');
        foreach ($matches as $match){
            $icons[$match[1]] = '&#x' . str_replace('\\', '', $match[2]) . '; ' . $match[1];
        }
        return $icons;
    }

}