<?php
class Zend_View_Helper_Minify extends Zend_View_Helper_Abstract
{
    public function Minify($files, $ext, $folderName)
    {   
        // The folder of the files your about to minify
        // PUBLICPATH should be the path to your public ZF folder

        // Set update needed flag to false
        $update_needed = false;

        // This is the file ext of the cached files
        $cacheFileExt = "." . $ext;

        // The list of files sent is exploded into an array
        $filesExploded = explode(',', $files);
        echo $filesExploded;
        exit();

        // The full cached file path is an md5 of the files string
        $cacheFilePath =  md5($files) . $cacheFileExt;

        // The filename of the cached file 
        $cacheFileName = preg_replace("#[^a-zA-Z0-9\.]#", "", end(explode("/", $cacheFilePath)));

        // Obtains the modified time of the cache file
        $cacheFileDate = is_file($cacheFilePath) ? filemtime($cacheFilePath) : 0;

        // Create new array for storing the list of valid files
        $fileList = array();

        // For each file
        foreach($filesExploded as $f)
        {
            // determine full path of the full and append extension
            $f = $f . '.' . $ext;

            // If the determined path is a file
            if(is_file($f))
            {
                // If the file's modified time is after the cached file's modified time
                // Then an update of the cached file is needed
                if(filemtime($f) > $cacheFileDate)
                    $update_needed = true;

                // File is valid add to list 
                $fileList[] = $f;
            }
        }

        // If the cache folder's modified time is after the cached file's modified time
        // Then an update is needed
        exit();
        if(filemtime($folder) > $cacheFileDate) 
            $update_needed = true;

        // If an update is needed then optmise the valid files
        if($update_needed)
            $this->optmiseFiles($fileList, $cacheFilePath, $ext);

        // Finally check if the cached file path is valid and return the absolute URL
        // for te cached file
        if(is_file($cacheFilePath))
            return "/" . $folderName . "/" . $cacheFileName;

        // Throw Exception
        throw new Exception("No minified file cached");             
    }

    private function optimise($code, $ext)
    {
        // Do not optmise JS files
        // I had problems getting JS files optmised and still function
        if($ext == "js")
            return $code;

        // Remove comments from CSS
        while(($i = strpos($code, '/*')) !== false)
        {
            $i2 = strpos($code, '*/',$i);

            if($i2 === false) 
                break;

            $code = substr($code, 0, $i).substr($code, $i2 + 2);
        }

        // Remove other elements from CSS
        $code = str_replace('/*','',$code);
        $code = str_replace("\n",' ',$code);
        $code = str_replace("\r",' ',$code);
        $code = str_replace("\t",' ',$code);
        $code = @ereg_replace('[ ]+',' ',$code);
        $code = str_replace(': ',':', $code);
        $code = str_replace('; ',';', $code);
        $code = str_replace(', ',',', $code);
        $code = str_replace(' :',':', $code);
        $code = str_replace(' ;',';', $code);
        $code = str_replace(' ,',',', $code);

        // Return optimised code
        return $code;
    }

    // Optmise the list of files
    private function optmiseFiles($fileList, $cacheFilePath, $ext)
    {
        // Empty String to start with
        $code = '';

        // Check files list in case just one file was passed
        if(is_array($fileList))
        {
            // Foreach of the valid files optmise the code if the file is valid
            foreach($fileList as $f)
                $code .= is_file($f) ? $this->optimise(implode('', file($f)), $ext) : '';
        }
        // Else if a valid file is passed optmise the code
        else
            $code = is_file($fileList) ? $this->optimise(implode('', file($fileList)), $ext) : '';

        // Open the cache file
        $f = @fopen($cacheFilePath, 'w');

        // If open successful
        if(is_resource($f))
        {
            // Write code to the cache file
            fwrite($f, $code);

            // close cache file
            fclose($f);
        }
    }   
}