<?php
/**
 * Dados do servidor
 * 
 * @author Paulo Henrique
 * @version 1.0
 */

class Zend_View_Helper_Servidor extends Zend_View_Helper_Abstract {

	/**
	 * Helper para mostrar os dados do servidor
	 *
	 */
	public function Servidor(){
		return array(
				"totaldiskspace" => disk_total_space ('/') / 1073741824,
				"disk_free_space" => disk_free_space ('/') / 1073741824,
				"memory_usage" => memory_get_usage(),
				"ip" => $this->getRealUserIp()
				);
	}
	
	private function getRealUserIp(){
	    switch(true){
	      case (!empty($_SERVER['HTTP_X_REAL_IP'])) : return $_SERVER['HTTP_X_REAL_IP'];
	      case (!empty($_SERVER['HTTP_CLIENT_IP'])) : return $_SERVER['HTTP_CLIENT_IP'];
	      case (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) : return $_SERVER['HTTP_X_FORWARDED_FOR'];
	      default : return $_SERVER['REMOTE_ADDR'];
	    }
	 }
    
}
