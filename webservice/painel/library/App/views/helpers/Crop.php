<?php 
class Zend_View_Helper_Crop extends Zend_View_Helper_Abstract
{
    /**
     * Helper para redimensionar imagem cropada
     *
     * @param $file - Caminho da Imagem
     * @param $width - Largura em pixels
     * @param $height - Altura em pixels
     * @param bool $keepRatio - Será que precisamos de manter a relação de aspecto para o novo imagem?
     * @return string - Retorna a miniatura
     */
    public function Crop($post, $dir){
        
        // Post
        $imgUrl = $post['imgUrl'];
        if(file_exists(APPLICATION_PATH."/../public_html".$post['imgUrl'])){
            $imgUrl = APPLICATION_PATH."/../public_html".$post['imgUrl'];
        }
        if(!file_exists($dir)){
            mkdir($dir, 0777, true);
        }
        // original sizes
        $imgInitW = $post['imgInitW'];
        $imgInitH = $post['imgInitH'];
        // resized sizes
        $imgW = $post['imgW'];
        $imgH = $post['imgH'];
        // offsets
        $imgY1 = $post['imgY1'];
        $imgX1 = $post['imgX1'];
        // crop box
        $cropW = $post['cropW'];
        $cropH = $post['cropH'];
        // rotation angle
        $angle = $post['rotation'];
         
        // qualidade do jpg
        $jpeg_quality = 100;
        $diretorio =  $dir;
        $output_filename = $diretorio.uniqid().rand();
         
        $what = getimagesize($imgUrl);
         
        switch(strtolower($what['mime']))
        {
            case 'image/png':
                $img_r = imagecreatefrompng($imgUrl);
                $source_image = imagecreatefrompng($imgUrl);
                $type = '.png';
                break;
            case 'image/jpeg':
                $img_r = imagecreatefromjpeg($imgUrl);
                $source_image = imagecreatefromjpeg($imgUrl);
                error_log("jpg");
                $type = '.jpeg';
                break;
            case 'image/gif':
                $img_r = imagecreatefromgif($imgUrl);
                $source_image = imagecreatefromgif($imgUrl);
                $type = '.gif';
                break;
            default: die('image type not supported');
        }
         
        if(!is_writable(dirname($output_filename))){
            $response = Array(
                "status" => 'erro',
                "message" => 'Não foi possivel ler o diretório.'
            );
        }else{
             
            // resize the original image to size of editor
            $resizedImage = imagecreatetruecolor($imgW, $imgH);
            imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
            // rotate the rezized image
            $rotated_image = imagerotate($resizedImage, -$angle, 0);
            // find new width & height of rotated image
            $rotated_width = imagesx($rotated_image);
            $rotated_height = imagesy($rotated_image);
            // diff between rotated & original sizes
            $dx = $rotated_width - $imgW;
            $dy = $rotated_height - $imgH;
            // crop rotated image to fit into original rezized rectangle
            $cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
            imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
            imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
            // crop image into selected area
            $final_image = imagecreatetruecolor($cropW, $cropH);
            imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
            imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
            // finally output png image
            imagejpeg($final_image, $output_filename.$type, $jpeg_quality);
            $response = Array(
                "status" => 'sucesso',
                "url" => $this->view->baseUrl($output_filename.$type)
            );
        }
        return $response;
        
    }
}