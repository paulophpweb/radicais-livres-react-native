<?php
class Zend_View_Helper_GetActionByControle extends Zend_View_Helper_Abstract
{
    //$controllers = $this->GetActionByControle(array('controle' => $this->controle, 'id_grupo' => Zend_Controller_Front::getInstance()->getRequest()->getParam('id')));
    public function GetActionByControle(array $params = array())
	{
		
		$resposta = array();
	    $acao = array();
	    $myAcao = array();
	    $controle = $params['controle'];
	    $id_grupo = $params['id_grupo'];
	    $tblAcl = new Sistema_Model_Acl();
	    if($controle)
	       $acao = $tblAcl->getActionByController($controle);
	    if($id_grupo)
	       $myAcao = $tblAcl->getActionByControllerMy($controle,$id_grupo);
	    foreach ($acao as $key => $value){
	        if($this->in_multiarray($value['id'], $myAcao,'id')){
	            $acao[$key]['checked'] = true;
	        }else{
	            $acao[$key]['checked'] = false;
	        }
	    }
		
		return array('dados' => $acao, "dados_my_action" => $myAcao);
		
	}
	
	private function in_multiarray($elem, $array,$field)
	{
	    $top = sizeof($array) - 1;
	    $bottom = 0;
	    while($bottom <= $top)
	    {
	        if($array[$bottom][$field] == $elem)
	            return true;
	        else
	            if(is_array($array[$bottom][$field]))
	                if(in_multiarray($elem, ($array[$bottom][$field])))
	                    return true;
	
	                $bottom++;
	    }
	    return false;
	}
}