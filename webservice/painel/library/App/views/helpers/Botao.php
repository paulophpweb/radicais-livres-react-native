<?php
class Zend_View_Helper_Botao extends Zend_View_Helper_Abstract
{
    public function botao($dados)
    {
        $view = Zend_Layout::getMvcInstance()->getView();
        $registy = Zend_Auth::getInstance()->getIdentity();
        $request = new Zend_Controller_Request_Http();
        $controle = strtolower(Zend_Controller_Front::getInstance()->getRequest()->getControllerName());
        $modulo = strtolower(Zend_Controller_Front::getInstance()->getRequest()->getModuleName());
        
        $html = '';
        foreach ($dados as $key => $value){
            $habilitado = false;
            if(isset($value['habilitado']) && $value['habilitado']){
                $habilitado = $value['habilitado'];
            }  
            if(Zend_Registry::get('acl')->isAllowed($registy->id_grupo, $modulo.":".$controle, $value['btn']) && $habilitado == 'true'):
                $html .= '<button type="button" onclick="btnAcao(\''.$value['btn'].'\')" class="'.$value['classe'].' '.$value['btn'].'">'.$value['text'].'</button>';
            endif;
        }
        return $html;
      
    }
}