<?php
class Zend_View_Helper_Tab extends Zend_View_Helper_Abstract
{
    public function tab($dados)
    {
        $view = Zend_Layout::getMvcInstance()->getView();
        $registy = Zend_Auth::getInstance()->getIdentity();
        $request = new Zend_Controller_Request_Http();
        $controle = strtolower(Zend_Controller_Front::getInstance()->getRequest()->getControllerName());
        $modulo = strtolower(Zend_Controller_Front::getInstance()->getRequest()->getModuleName());
        
        $html = '<ul class="nav nav-tabs">';
        foreach ($dados as $key => $value){
            if(Zend_Registry::get('acl')->isAllowed($registy->id_grupo, $modulo.":".$controle, $key)):
                $html  .= '<li class="'.(isset($value['class']) ? $value['class'] : "").' '.(isset($value['disabled']) && $value['disabled'] == true  ? "disabled" : "").' '.(isset($value['active']) ? 'active' : "").'">';
                $html  .= '     <a data-params=\''.(isset($value['params']) ? json_encode($value['params']) : '').'\' data-toggle="tabajax" href="'.$view->baseUrl('').'sistema/'.$controle.'/'.$key.'" data-target="#'.$key.'" data-toggle="tab">'.$value['label'].'</a>';
                $html  .= '</li>';
            endif;
        }
        $html .= '</ul>';
        return $html;
      
    }
}