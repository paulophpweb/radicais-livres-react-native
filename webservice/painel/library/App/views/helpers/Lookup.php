<?php
class Zend_View_Helper_Lookup extends Zend_View_Helper_Abstract
{
    public function lookup($dados)
    {
        $view = Zend_Layout::getMvcInstance()->getView();
        $registy = Zend_Auth::getInstance()->getIdentity();
        $request = new Zend_Controller_Request_Http();
        $controle = strtolower(Zend_Controller_Front::getInstance()->getRequest()->getControllerName());
        
        $html = '<span class="pull-right">';
        $html .= '  <button onclick=\'lookup("'.(isset($dados['url']) ? $dados['url'] : "").'","'.(isset($dados['campos']) ? base64_encode(json_encode($dados['campos'])) : '').'","'.(isset($dados['selecionar']) ? base64_encode(json_encode($dados['selecionar'])) : "").'")\' class="btn btn-default btn-sm-default" type="button">';
        $html .= '      <span class="glyphicon glyphicon glyphicon-search" aria-hidden="true"></span>';
        $html .= '  </button>';
        $html .= '</span>';

        return $html;
      
    }
}