<?php
class Zend_View_Helper_Controllers extends Zend_View_Helper_Abstract
{
    public function Controllers()
    {

        $controllers = $this->_getController();
        return $controllers;
    }
    
    private static function _getController() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql  = 'select ';
		$sql .= '	a.controller ';
		$sql .= 'from ';
		$sql .= '	acl a ';
		$sql .= 'WHERE a.controller != \'error\' and a.controller != \'index\' ';
		$sql .= 'group by a.controller ';
		$sql .= 'order by ';
		$sql .= '	a.id DESC ';
        
        $result = $db->fetchAll($sql);
        
        return $result;
    }

}