<?php
class Zend_View_Helper_Menu extends Zend_View_Helper_Abstract
{
    public function menu()
    {
        $view = Zend_Layout::getMvcInstance()->getView();
        $registy = Zend_Auth::getInstance()->getIdentity();
        $modulo = $this->_getModulo();
        $menu = array();
        $request = new Zend_Controller_Request_Http();
        
        
        $i = 0;
        $html = '';
        foreach ($modulo as $val) {
            if($registy->id_grupo){
                $params = array("grupo_id" => $registy->id_grupo,'is_root' => $registy->is_root,'modulo_id' => $val['id'],'controller'=>$request->getControllerName(),'action'=>$request->getActionName());
                $menu = $this->_getMenu($params);
            }
            if(count($menu)){
                $html  .= '<li class="treeview">';
                $html  .= '    <a href="javascript:;" >';
                $html  .= '        <i class="fa '.$val['icone'].'"></i>';
                $html  .= '            '.$val['nome'].'';
                $html  .= '        <i class="fa fa-angle-left pull-right"></i>';
                $html  .= '    </a>';
                $html  .= '    <ul class="treeview-menu">';
                foreach ($menu as $value){
                    $html  .= '    <li>';
                    $html  .= '        <a class="black '.(strtolower(Zend_Controller_Front::getInstance()->getRequest()->getActionName()) == $value['nm_action'] && strtolower(Zend_Controller_Front::getInstance()->getRequest()->getControllerName()) == $value['nm_controller']  ? "active" : "").'" href="'.$view->baseUrl().'/'.$value['nm_modulo'].'/'.$value['nm_controller'].'/'.$value['nm_action'].'">';
                    $html  .= '            '.$value['nome'].'';
                    $html  .= '        </a>';
                    $html  .= '    </li>';
                }
                $html  .= '    </ul>';
                $html  .= '</li>';
            }
            $i++;
        }
        return $html;
    }
    
    private static function _getModulo() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql  = 'select ';
		$sql .= '	m.id, ';
		$sql .= '	m.nome, ';
		$sql .= '	m.icone ';
		$sql .= 'from ';
		$sql .= '	modulo m ';
		$sql .= 'where status = 1 ';
		$sql .= 'order by ';
		$sql .= '	m.ordem asc ';
        
        $result = $db->fetchAll($sql);
        
        return $result;
    }
    
    private static function _getMenu($condicao = array()) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql  = 'select ';
        $sql .= '	m.modulo_id, ';
        $sql .= '	m.nome, ';
        $sql .= '	m.nm_controller, ';
        $sql .= '	m.nm_modulo, ';
        $sql .= '	m.nm_action, ';
        $sql .= '	m.ordem, ';
        $sql .= '	m.status ';
        $sql .= 'from ';
        $sql .= '	menu m ';
        $sql .= '	left join acl a on m.nm_controller = a.controller AND m.nm_action = a.action ';
        $sql .= '	left join acl_to_grupo ag on a.id = ag.acl_id ';
		$sql .= 'where m.status = 1 ';
		if(isset($condicao["modulo_id"]) && $condicao["modulo_id"])
		    $sql .= 'and m.modulo_id = '.$condicao["modulo_id"].' ';
		if(isset($condicao["grupo_id"]) && $condicao["grupo_id"] && !$condicao['is_root'])
		    $sql .= 'and ag.grupo_id = '.$condicao["grupo_id"].' ';
        
        $result = $db->fetchAll($sql);
    
        return $result;
    }
    
    private static function _getSubMenu($modulo_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql  = 'select ';
		$sql .= '	ms.id, ';
		$sql .= '	ms.nome, ';
		$sql .= '	ms.ctrl, ';
		$sql .= '	ms.action, ';
		$sql .= '	ms.modulo_id ';
		$sql .= 'from ';
		$sql .= '	 modulo_menu_sub ms ';
		$sql .= 'where ';
		$sql .= '    ms.modulo_id = '.$modulo_id.' ' ;
		$sql .= '    and ms.status = 1 ' ;
		$sql .= 'order by ';
		$sql .= '	ms.ordem asc ';
		
        $result = $db->fetchAll($sql);
        
        return $result;
    }
}