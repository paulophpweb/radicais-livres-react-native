<?php
// http://www.developerfiles.com/creating-acl-with-database-in-zend-framework/ kkk2

class App_Plugin_Acl extends Zend_Controller_Plugin_Abstract
{
   private $ignoreAction = array('webcam','recortar-imagem','form','get-abas','modal','get-botao');
   private $ignoreController = array('index');
   public function preDispatch(Zend_Controller_Request_Abstract $request)
    {

        if($request->getModuleName() == "sistema"){
            $auth = Zend_Auth::getInstance();
            $authModel=new Sistema_Model_CaAuth();
            if (!$auth->hasIdentity()){
                $controller = $request->getControllerName();
                $action = $request->getActionName();
                if($action != "logar" && $action != "error" && $action != "request-bitbucket" && $action != "get-dash"){
                    $request->setModuleName('sistema');
                    $request->setControllerName('index');
                    $request->setActionName('login');
                }
                return;
            }
     
            $request=$this->getRequest();
            $aclResource=new Sistema_Model_Acl();
            //verifica se o controle e a action existe, senao manda para o erro 404
            if( !$aclResource->resourceValid($request)){
                $request->setModuleName('error');
                $request->setControllerName('error');
                $request->setActionName('error');
                return;
            }
            
            $modulo = $request->getModuleName();
            $controller = $request->getControllerName();
            $action = $request->getActionName();
            //Verifica se existe o resource no banco de dados, senão cria ele.
            if( !$aclResource->resourceExists($modulo,$controller, $action)){
                // so grava se os for diferente destas acoes e destes controllers
                if(!in_array($controller, $this->ignoreController) && !in_array($action, $this->ignoreAction)){
                    $aclResource->createResource($modulo,$controller,$action);
                }
            }
            //Get role_id
            $grupo_id=$auth->getIdentity()->id_grupo;
            $is_root=$auth->getIdentity()->is_root;
            // Instancia a Acl
            $acl = new Zend_Acl();
            // adciona o grupo
            $acl->addRole(new Zend_Acl_Role($grupo_id));
            
    
            if($is_root){//If grupo for root não cria os resources e tem acesso total
                //Mostra todos os controllers
                $resources=$aclResource->getAllResources();
                
                // Add the existing resources to ACL
                foreach($resources as $resource){
                    if(isset($resource["controller"]) && isset($resource["module"]))
                        $acl->add(new Zend_Acl_Resource($resource["module"].":".$resource["controller"]));
                }
                $acl->allow($grupo_id);
                Zend_Registry::set('acl', $acl);
            }else{
                //Mostra todos os controllers
                $resources=$aclResource->getAllResources();  
                $acl->add(new Zend_Acl_Resource('sistema:index'));
                $acl->add(new Zend_Acl_Resource('sistema:error'));
                // Add the existing resources to ACL
                foreach($resources as $resource){
                	if(isset($resource["controller"]) && isset($resource["module"]))
                    	$acl->add(new Zend_Acl_Resource($resource["module"].":".$resource["controller"]));
                }  
                $userAllowedResources = '';
                if($grupo_id)
    			//Pega as permissao deste grupo
                $userAllowedResources=$aclResource->getCurrentRoleAllowedResources($grupo_id);
    			
    			// Adciona as permissão no ACL
                $acl->allow($grupo_id, "sistema:index");
                $acl->allow($grupo_id, "sistema:error",array("error"));
                if($userAllowedResources){
                    foreach($userAllowedResources as $controllerName =>$allowedActions){
                        $arrayAllowedActions=array();
                        foreach($allowedActions as $allowedAction){
                            $arrayAllowedActions[]=$allowedAction;
                        }
                        $acl->allow($grupo_id, $controllerName,$arrayAllowedActions);
                    }
                }
    			
    			//Salva a Acl no Registro
    	        Zend_Registry::set('acl', $acl);
    	        
    	        //Verifica se você tem acesso, senão manda para o acesso negado.
    	        if(!$acl->isAllowed($grupo_id,$modulo.":".$controller,$action)){
    	            if(!$this->getRequest()->isXmlHttpRequest()) {
    	                // ignora esta acao e este controller
    	                if(!in_array($controller, $this->ignoreController) && !in_array($action, $this->ignoreAction)){
            	            $request->setControllerName('error');
            	            $request->setActionName('acesso-negado');
    	                }
    	            }else{
    	                if(!in_array($controller, $this->ignoreController) && !in_array($action, $this->ignoreAction)){
    	                   $resposta = array(
    	                       'status' => 'error',
    	                       'msg' => "Você não tem permissão para acessar este controlador ".$controller." e esta ação ".$action."."
    	                   );
    	                   echo json_encode($resposta);
    	                   exit();
    	                }
    	            }
    	            return;
    	        }
    
    		}   
        }  
           
    }
}