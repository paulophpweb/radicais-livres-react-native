app.register.controller('webcam', function Ctrl($scope, Scopes) {
	Scopes.store('webcam', $scope);
	
	$scope.picture = "";

	var modalInstance = Scopes.get($parametros.returncontrole).modalInstance;
	$scope.callback = function(media){
		console.log(media);
	}
	
	modalInstance.result.then(function (selectedItem) {
      
    }, function () {
    	if($scope.picture)
    		Scopes.get($parametros.returncontrole).imagePerfil = $scope.picture;
    });
});