
//angular aba usuário
app.$register.service(
    "$modelabamenu",
    function( $http, $q, $loader ) {
        // Return
        return({
        	getById: getById,
        	getAll: getAll,
        	incluir: incluir,
        	alterar: alterar,
        	remove: remove
        });
        // ---
        // PUBLIC METHODS.
        //
        /**
         * pega os dados remoto por id
         */
        function getById(obj) {
        	$loader.show("Carregando...");
            var request = $http({
                method: "post",
                url: _baseUrl+_controller+"/get-menu-by-id",
                data: obj
            });
            return( request.then( handleSuccess, handleError ) );
        }
        
        /**
         * Pega os dados todos remoto
         */
        function getAll(obj) {
        	$loader.show("Carregando...");
            var request = $http({
                method: "post",
                url: _baseUrl+_controller+"/get-menu-por-modulo",
                data: $.param(obj)
            });
            return( request.then( handleSuccess, handleError ) );
        }
        
        /**
         * Remove um dado remoto
         */
        function remove( ids ) {
        	$loader.show("Carregando...");
            var request = $http({
                method: "post",
                url: _baseUrl+_controller+"/remover-menu",
                data: $.param(ids)
            });
            return( request.then( handleSuccess, handleError ) );
        }
        
        /**
         * inclui os dados remoto por id
         */
        function incluir(obj) {
        	$loader.show("Carregando...");
            var request = $http({
                method: "post",
                url: _baseUrl+_controller+"/incluir-menu",
                data: obj
            });
            return( request.then( handleSuccess, handleError ) );
        }
        
        /**
         * altera os dados remoto por id
         */
        function alterar(obj) {
        	$loader.show("Carregando...");
            var request = $http({
                method: "post",
                url: _baseUrl+_controller+"/alterar-menu",
                data: obj
            });
            return( request.then( handleSuccess, handleError ) );
        }

        // ---
        // PRIVATE METHODS.
        // ---
        // I transform the error response, unwrapping the application dta from
        // the API response payload.
        function handleError( response ) {
        	$loader.hide();
            // The API response from the server should be returned in a
            // nomralized format. However, if the request was not handled by the
            // server (or what not handles properly - ex. server error), then we
            // may have to normalize it on our end, as best we can.
            if (
                ! angular.isObject( response.data ) ||
                ! response.data.message
                ) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        }
        // I transform the successful response, unwrapping the application data
        // from the API response payload.
        function handleSuccess( response ) {
        	$loader.hide();
            return( response.data );
        }
    }
);

/*
 * Controller Aba Modulo
 */
app.register.controller('sca-modulo_aba-menu', function Ctrl($scope,Scopes,NgTableParams,$notify,$location,$element,$uibModalStack,$modelabamenu,$validator,$loader) {
	Scopes.store('sca-modulo_aba-menu', $scope);
	
	$scope.btn = [];
	$scope.botaoAcao = [];
	$scope.btnAcao = btnAcao;
	$scope.setModulo = setModulo;
	$scope.del = deletar;
	$scope.alter = formAlterar;
	$scope.dados = {};
	$scope.isCollapsed = true;
	$scope.podeIncluir = false;
	$scope.form = {};
	$id = "";
	
	// verifica se tem o id na url
	if($location.$$absUrl.indexOf('id') > -1){
		$id = $location.$$absUrl.match(/id\/[0-9]*/).toString().replace("id/",""); 
		$scope.podeIncluir = true;
	}
	
	loadDadosRemoto();
	
	/**
	 * Pega os dados remoto
	 */
	function loadDadosRemoto(){
		$scope.tableParams = new NgTableParams({}, {
	      getData: function(params) {
	        // ajax request
	    	var $data = params.url();
	    	$data.modulo_id = $id;
	    	return $modelabamenu.getAll($data)
		    .then(
		        function( data ) {
		        	if(data.status == "sucesso" && data.dados.res){
		        		aplicarDadosRemoto( data.dados.res );
		        		params.total(data.dados.total); // recal. page nav controls
		    	        return $scope.dados;
		        	}else{
		        		$notify.open(data.msg,3000,"error");
		        	}
		        }
		    );
	      }
	    });
	}
	
	/**
	 * Aplica os dados remoto a view
	 */
    function aplicarDadosRemoto( dados ) {
        $scope.dados = dados;
    }
	
	
	/**
	 * Recebe os dados remoto por id
	 */
	function formAlterar($e,$index){
		var $data = $.param({id:$scope.dados[$index].id});
    	return $modelabamenu.getById($data)
	    .then(
	        function( data ) {
	        	if(data.status == "sucesso" && data.dados){
	        		setForm(data.dados);
	        		$scope.isCollapsed = false;
	        		enableAlterar('formMenu');
	        	}else{
	        		$notify.open(data.msg,3000,"error");
	        		$scope.dados = $scope.dados;
	        	}
	        }
	    );
	}
	
	/**
     * Deletar um modulo
     */
	function deletar($event,$index){
		alertify.confirm("Deseja realmente excluir este menu? Nome: "+$scope.dados[$index].nome+" - ID: "+$scope.dados[$index].id+"",function(status){
			if(status){
				$modelabamenu.remove({id:$scope.dados[$index].id})
			    .then(
			        function( data ) {
			        	if(data.status == "sucesso"){
			        		loadDadosRemoto();
			        	}else{
			        		$notify.open(data.msg,3000,"error");
			        	}
			        }
			    );
			}
		});
	}
	
	/**
	 * Grava os dados do modulo no scope
	 */
	function setModulo( dados ) {
        $scope.dados = dados;
    }
	
	/**
	 * Grava os dados do form no scope
	 */
	function setForm( dados ) {
        $scope.form = dados;
    }
	
	/**
	 * Verfica o tipo de acao dos botoes e faz a acao
	 */
	function btnAcao($tipo){
		if($tipo == "limpar"){
			setForm({});
			enableCadastrar('formMenu');
		}else if($tipo == "alterar"){
			if($validator.validar('formMenu')){
				var $data = $.param($scope.form);
		    	return $modelabamenu.alterar($data)
			    .then(
			        function( data ) {
			        	if(data.status == "sucesso"){
			        		$scope.isCollapsed = true;
			        		loadDadosRemoto();
			        		$notify.open(data.msg,2000,"success");
			        	}else{
			        		$notify.open(data.msg,2000,"error");
			        	}
			        }
			    );
			}

		}else if($tipo == "incluir"){
			if($validator.validar('formMenu')){
				$scope.form.modulo_id = $id;
				var $data = $.param($scope.form);
		    	return $modelabamenu.incluir($data)
			    .then(
			        function( data ) {
			        	if(data.status == "sucesso" && data.dados){
			        		loadDadosRemoto();
			        	}else{
			        		$notify.open(data.msg,2000,"error");
			        	}
			        }
			    );
			}
		}else if($tipo == "remover"){
			alertify.confirm("Deseja realmente excluir este menu? Nome: "+$scope.form.nome+" - ID: "+$scope.form.id+"",function(status){
				if(status){
					$modelabamenu.remove({id:$scope.form.id})
				    .then(
				        function( data ) {
				        	if(data.status == "sucesso"){
				        		setForm({});
				        		$scope.isCollapsed = true;
				        		enableCadastrar('formMenu');
				        		loadDadosRemoto();
				        	}else{
				        		$notify.open(data.msg,3000,"error");
				        	}
				        }
				    );
				}
			});
		}
	}
	
	// Antes do Ajax
	$scope.$on('$includeContentRequested', function($obj) {
		$loader.show("Carregando...");
	});
	// Depois que carregou
	$scope.$on('$includeContentLoaded', function() {
		$loader.hide();
	});
    
});

