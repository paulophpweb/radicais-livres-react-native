
//angular aba usuário
app.$register.service(
    "$modelacl",
    function( $http, $q, $loader ) {
        // Return
        return({
        	getByController: getByController,
        	incluir: incluir
        });
        // ---
        // PUBLIC METHODS.
        //
        /**
         * pega os dados remoto por id
         */
        function getByController(obj) {
        	$loader.show("Carregando...");
            var request = $http({
                method: "post",
                url: _baseUrl+"index/get-acao",
                data: obj
            });
            return( request.then( handleSuccess, handleError ) );
        }
        
        /**
         * inclui os dados remoto por id
         */
        function incluir(obj) {
        	$loader.show("Carregando...");
            var request = $http({
                method: "post",
                url: _baseUrl+"index/incluir-acl",
                data: obj
            });
            return( request.then( handleSuccess, handleError ) );
        }

        // ---
        // PRIVATE METHODS.
        // ---
        // I transform the error response, unwrapping the application dta from
        // the API response payload.
        function handleError( response ) {
        	$loader.hide();
            // The API response from the server should be returned in a
            // nomralized format. However, if the request was not handled by the
            // server (or what not handles properly - ex. server error), then we
            // may have to normalize it on our end, as best we can.
            if (
                ! angular.isObject( response.data ) ||
                ! response.data.message
                ) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        }
        // I transform the successful response, unwrapping the application data
        // from the API response payload.
        function handleSuccess( response ) {
        	$loader.hide();
            return( response.data );
        }
    }
);


/*
 * Controller Aba Avatar
 */
app.register.controller('sca-grupo_aba-acl', function Ctrl($scope, Scopes,$http,$notify,$modelacl,$location,$loader) {
	Scopes.store('sca-grupo_aba-acl', $scope);
	$scope.controle = '';
	$scope.dados = {};
	$scope.acao = {};
	$scope.getActions = getActions;
	$scope.checarTodos = checarTodos;
	$scope.salvar = salvar;
	$id = "";
	
	// verifica se tem o id na url
	if($location.$$absUrl.indexOf('id') > -1){
		$id = $location.$$absUrl.match(/id\/[0-9]*/).toString().replace("id/",""); 
	}
	
	//varre o array buscando os controllers
	if(Scopes.get("sca-grupo_form").tabs){
		angular.forEach(Scopes.get("sca-grupo_form").tabs, function(value, key) {
			if(value.controllers){
				setControllers(value.controllers);
			}
		});
	}
	
	/**
	 * Grava os dados do controllers no scope
	 */
	function setControllers( dados ) {
        $scope.dados = dados;
        
    }
	
	/**
	 * Grava os dados do action no scope
	 */
	function setActions( dados ) {
        $scope.acao = dados;
        
    }
	
	/**
	 * Funcao de checar todos
	 */
	function salvar($check){
		var $dadosEnviar = [];
		angular.forEach($scope.acao, function(value, key) {
			if($scope.acao[key].checked == true){
				$dadosEnviar.push($scope.acao[key].id);
			}
			
		});
		var $data = $.param({acl_id:$dadosEnviar,id_grupo:$id,controle:$scope.controle});
		return $modelacl.incluir($data)
	    .then(
	        function( data ) {
	        	if(data.status == "sucesso"){
	        		$notify.open(data.msg,3000,"success");
	        	}else{
	        		$notify.open(data.msg,3000,"error");
	        	}
	        }
	    );
	}
	
	/**
	 * Grava os dados do grupo no scope
	 */
	function getActions() {
		var $data = $.param({controle:$scope.controle,id_grupo:$id});
    	return $modelacl.getByController($data)
	    .then(
	        function( data ) {
	        	if(data.status == "sucesso" && data.dados){
	        		setActions( data.dados );
	        	}else{
	        		$notify.open(data.msg,3000,"error");
	        	}
	        }
	    );
        
    }
	
	/**
	 * Funcao de checar todos
	 */
	function checarTodos($check){
		
		if($check){
			angular.forEach($scope.acao, function(value, key) {
				$scope.acao[key].checked = true;
			});
		}else{
			angular.forEach($scope.acao, function(value, key) {
				$scope.acao[key].checked = false;
			});
		}
	}

	
	// Antes do Ajax
	$scope.$on('$includeContentRequested', function($obj) {
		$loader.show("Carregando...");
	});
	// Depois que carregou
	$scope.$on('$includeContentLoaded', function() {
		$loader.hide();
	});
    
});

