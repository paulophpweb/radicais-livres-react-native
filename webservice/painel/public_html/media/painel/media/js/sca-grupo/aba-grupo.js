
//angular aba usuário
app.$register.service(
    "$modelabagrupo",
    function( $http, $q, $loader ) {
        // Return
        return({
        	getById: getById,
        	incluir: incluir,
        	alterar: alterar,
        	remove: remove
        });
        // ---
        // PUBLIC METHODS.
        //
        /**
         * pega os dados remoto por id
         */
        function getById(obj) {
        	$loader.show("Carregando...");
            var request = $http({
                method: "post",
                url: _baseUrl+_controller+"/get-grupo",
                data: obj
            });
            return( request.then( handleSuccess, handleError ) );
        }
        
        /**
         * Remove um dado remoto
         */
        function remove( ids ) {
        	$loader.show("Carregando...");
            var request = $http({
                method: "post",
                url: _baseUrl+_controller+"/remover",
                data: $.param(ids)
            });
            return( request.then( handleSuccess, handleError ) );
        }
        
        /**
         * inclui os dados remoto por id
         */
        function incluir(obj) {
        	$loader.show("Carregando...");
            var request = $http({
                method: "post",
                url: _baseUrl+_controller+"/incluir",
                data: obj
            });
            return( request.then( handleSuccess, handleError ) );
        }
        
        /**
         * altera os dados remoto por id
         */
        function alterar(obj) {
        	$loader.show("Carregando...");
            var request = $http({
                method: "post",
                url: _baseUrl+_controller+"/alterar",
                data: obj
            });
            return( request.then( handleSuccess, handleError ) );
        }

        // ---
        // PRIVATE METHODS.
        // ---
        // I transform the error response, unwrapping the application dta from
        // the API response payload.
        function handleError( response ) {
        	$loader.hide();
            // The API response from the server should be returned in a
            // nomralized format. However, if the request was not handled by the
            // server (or what not handles properly - ex. server error), then we
            // may have to normalize it on our end, as best we can.
            if (
                ! angular.isObject( response.data ) ||
                ! response.data.message
                ) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        }
        // I transform the successful response, unwrapping the application data
        // from the API response payload.
        function handleSuccess( response ) {
        	$loader.hide();
            return( response.data );
        }
    }
);

/*
 * Controller Aba Grupo
 */
app.register.controller('sca-grupo_aba-grupo', function Ctrl($scope,Scopes,$notify,$location,$element,$uibModalStack,$modelabagrupo,$validator,$loader) {
	Scopes.store('sca-grupo_aba-grupo', $scope);
	
	$scope.btn = [];
	$scope.botaoAcao = [];
	$scope.btnAcao = btnAcao;
	$scope.setGrupo = setGrupo;
	$scope.loadById = loadById;
	$scope.dados = {};
	$id = "";
	
	// verifica se tem o id na url
	if($location.$$absUrl.indexOf('id') > -1){
		$id = $location.$$absUrl.match(/id\/[0-9]*/).toString().replace("id/",""); 
	}
	
	//varre o array buscando os controllers
	if(Scopes.get("sca-grupo_form").tabs){
		angular.forEach(Scopes.get("sca-grupo_form").tabs, function(value, key) {
			if(value.grupo){
				setGrupo(value.grupo);
			}
		});
	}
	
	/**
	 * Recebe os dados remoto por id
	 */
	function loadById($obj){
		
		var $data = $.param($obj);
    	return $modelabagrupo.getById($data)
	    .then(
	        function( data ) {
	        	if(data.status == "sucesso" && data.dados){
	        		setGrupo(data.dados);
	        		// se o id passado na url for direfente do id do scope atualiza a pagina.
	        		if($id != data.dados.id_grupo){
	        			window.location.href= _baseUrl+_controller+"/form/id/"+data.dados.id_grupo;
	        		}
	        	}else{
	        		$notify.open(data.msg,3000,"error");
	        		$scope.dados = $scope.dados;
	        	}
	        }
	    );
	}
	
	/**
	 * Grava os dados do grupo no scope
	 */
	function setGrupo( dados ) {
        $scope.dados = dados;
    }
	
	/**
	 * Verfica o tipo de acao dos botoes e faz a acao
	 */
	function btnAcao($tipo){
		if($tipo == "limpar"){
			var $obj = $("#limpar");
			$('#form').each (function(){
				  this.reset();
			});
			window.location.href= _baseUrl+_controller+"/form";
		}else if($tipo == "alterar"){
			if($validator.validar('form')){
				var $data = $.param($scope.dados);
		    	return $modelabagrupo.alterar($data)
			    .then(
			        function( data ) {
			        	if(data.status == "sucesso"){
			        		// se o grupo for root nao precisa de controle de acesso
			        		/*if($scope.dados.is_root == 1){
			        			Scopes.get("sca-grupo_form").tabs[1].disabled = true;
			        		}else{
			        			Scopes.get("sca-grupo_form").tabs[1].disabled = false;
			        		}*/
			        		$notify.open(data.msg,2000,"success");
			        	}else{
			        		$notify.open(data.msg,2000,"error");
			        	}
			        }
			    );
			}

		}else if($tipo == "incluir"){
			if($validator.validar('form')){
				var $data = $.param($scope.dados);
		    	return $modelabagrupo.incluir($data)
			    .then(
			        function( data ) {
			        	if(data.status == "sucesso" && data.dados){
			        		console.log(data.dados);
			        		$notify.open(data.msg,2000,"success",function(){
			        			window.location.href= _baseUrl+_controller+"/form/id/"+data.dados.id_grupo;
			        		});
			        	}else{
			        		$notify.open(data.msg,2000,"error");
			        	}
			        }
			    );
			}
		}else if($tipo == "remover"){
			alertify.confirm("Deseja realmente excluir este grupo? Nome: "+$scope.dados.nm_grupo+" - ID: "+$scope.dados.id_grupo+"",function(status){
				if(status){
					$modelabagrupo.remove({id:$scope.dados.id_grupo})
				    .then(
				        function( data ) {
				        	if(data.status == "sucesso"){
				        		$notify.open(data.msg,2000,"success",function(){
				        			window.location.href= _baseUrl+_controller+"/index";
				        		});
				        	}else{
				        		$notify.open(data.msg,3000,"error");
				        	}
				        }
				    );
				}
			});
		}
	}
	
	// Antes do Ajax
	$scope.$on('$includeContentRequested', function($obj) {
		$loader.show("Carregando...");
	});
	// Depois que carregou
	$scope.$on('$includeContentLoaded', function() {
		$loader.hide();
	});
    
});

