

$(function(){
	$.ajaxSetup({
        beforeSend: function(xhr) {
           carregando.show("Carregando.");
        },
        error: function (x, status, error) {
            alertOnError("Erro: " + status + "nError: " + error);
            carregando.hide();
        },
        success: function(result,status,xhr) {
        	carregando.hide();
        },
        complete: function(xhr, stat) {
        	carregando.hide();
        }
    });
	// abre as abas com ajax
	$('[data-toggle="tabajax"]').click(function(e) {
		if(!$(this).parent().hasClass('disabled')){
		    var $this = $(this),
		        loadurl = $this.attr('href'),
		        params = $this.attr('data-params'),
		        targ = $this.attr('data-target');

		    var $content = $(targ).html();
	        if($content == "" || $content == null){
			    if(params){
			    	params = JSON.parse(params);
			    }
			    $.post(loadurl,params,function(res){
			    	$(targ).html(res);
			    	habilitarCheckboxCustom();
			        habilitarSelectCustom();
				}, 'html');
			    $this.tab('show');
			}else{
				$this.tab('show');
			}
		}
	    return false;
	});
	
	// abre as abas com ajax
	$('[data-toggle="tabajax"]').parent().each(function(index) {
		if(index == 0){
			var $this = $(this).find('a'),
	        loadurl = $this.attr('href'),
	        params = $this.attr('data-params'),
	        targ = $this.attr('data-target');
	        var $content = $(targ).html();
	        if($content == "" || $content == null){
			    if(params){
			    	params = JSON.parse(params);
			    }
			    $.post(loadurl,params,function(res){
			    	$(targ).html(res);
			    	habilitarCheckboxCustom();
			        habilitarSelectCustom();
				}, 'html');
			}
		    
		    $(targ).addClass('active');
	
		    
		}
	    
	    return false;
	});
	
    
	habilitarCheckboxCustom();
	if($('select').length != 0 || $('.wpmse_select2').length != 0){
		habilitarSelectCustom();
	}
});

function habilitarSelectCustom(){
	$('select').select2();
    
    $('.wpmse_select2').select2({
        width: "100%",
        templateResult: format
    });
}

function habilitarCheckboxCustom(){
	$('input').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue',
      increaseArea: '20%' // optional
    });
}


function format(icon) {
    var originalOption = icon.element;
    return $(originalOption).data('icon');
}

function ajaxSubmit(jForm, erros, args){
	var formData = new FormData();
	jForm.find("input, select, textarea").each(function(index){
		if($(this).attr("type") == "file"){
			formData.append($(this).attr("name"), $(this)[0].files[0]);
		}else{
			formData.append($(this).attr("name"), $(this).val());
		}
		
	});
    args = args || {};
    if (erros.length > 0) {
        var _mensagem = "";
        $(erros).each(function(idx, item) {
            _mensagem += item + '<br>';
        });
        noty({
		    text: _mensagem,
		    modal:true,
		    killer: true,
		    layout:"center",
		    type:"alert",
		    timeout:5000
		});
    } else {
        $.ajax({
            url: jForm.attr('action'),
            data: formData,
            type: jForm.attr('method'),
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            dataType: "json",
            beforeSend: function() {
                $('input:submit, button[type=submit]').attr('disabled', 'disabled');
               jForm.find("button[type=submit]").html("<i class='fa fa-spinner fa-pulse'></i>");
            },
            error:function(data){
                if(args.error != null)args.error(data.responseText);
                noty({
        		    text: data.statusText,
        		    modal:true,
        		    killer: true,
        		    layout:"center",
        		    type:"alert",
        		    timeout:2000
        		});
            },
            complete: function(data) {
                $('input:submit, button[type=submit]').removeAttr('disabled');
                jForm.find("button[type=submit]").html("ENVIAR");
                if(data.responseJSON.situacao == "sucesso"){
                	if(args.onComplete)
                		args.onComplete(data);
                	jForm.trigger('reset');
                	if(args.redirect != null && args.redirect != ''){
                		window.location.href=args.redirect;
                	}else{
	                	noty({
	            		    text: data.responseJSON.msg,
	            		    modal:true,
	            		    killer: true,
	            		    layout:"center",
	            		    type:"success",
	            		    timeout:2000,
	            		    open: 'animated bounceInLeft', // Animate.css class names
	            	        close: 'animated bounceOutLeft', // Animate.css class names
	            		});
                	}
                }else{
                	noty({
            		    text: data.responseJSON.msg,
            		    modal:true,
            		    killer: true,
            		    layout:"center",
            		    type:"error",
            		    timeout:4000,
            		    open: 'animated bounceInLeft', // Animate.css class names
            	        close: 'animated bounceOutLeft', // Animate.css class names
            		});
                }
                if(args.complete != null)args.complete(data.responseJSON.msg);
            }
        });

    }
 


}


var carregando = (function ($) {

    // Cria a vid
	var $dialog = $(
		'<div id="loadingPadrao" style="background-color:#fff1a8; width:200px; border:1px solid #ccc; -webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.75); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.75); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.75); padding: 5px; z-index:9999999; position:fixed; top:-100px; left:50%; margin-left:-100px; text-align:center; -webkit-border-radius: 2px; -moz-border-radius: 2px; border-radius: 2px;">' +
			'<p></p>'+
		'</div>' );

	return {
		/*
		 * Abre o Dialog
		 */
		show: function (message) {
			$('body').append($dialog);
			$('#loadingPadrao').stop().animate({
				top: '0px'
			},200);
			$dialog.find('p').text(message);
		},
		/**
		 * Fecha o Dialog
		 */
		hide: function () {
			$('#loadingPadrao').stop().animate({
				top: '-100px'
			},200);
			$('#loadingPadrao').remove();
		}
	}

})(jQuery);

var notificar = (function ($) {

    // Cria a vid
	var $dialog = $(
		'<div id="loadingPadrao" style="background-color:#fff1a8; width:200px; border:1px solid #ccc; -webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.75); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.75); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.75); padding: 5px; z-index:9999999; position:fixed; top:-100px; left:50%; margin-left:-100px; text-align:center; -webkit-border-radius: 2px; -moz-border-radius: 2px; border-radius: 2px;">' +
			'<p></p>'+
		'</div>' );

	return {
		/*
		 * Abre o Dialog
		 */
		open: function ($msg, $time, $type, $onComplete) {
			$type ? $type : "information";
			noty({
			    text: $msg,
			    modal:true,
			    killer: true,
			    layout:"center",
			    type:$type,
			    maxVisible: 3,
			    force:true,
			    timeout:$time ? $time : 100000,
				callback: {
			        onShow: function() {},
			        afterShow: function() {},
			        onClose: function() {
			        	$.noty.closeAll();
				    	$.noty.clearQueue();
				    	if($onComplete)
				    		$onComplete();
			        },
			        afterClose: function() {},
			        onCloseClick: function() {
			        	if($onComplete)
				    		$onComplete();
			        },
			    },
			});
		},
		/**
		 * Fecha o Dialog
		 */
		close: function () {
			setTimeout(function(){ 
	    		$.noty.closeAll();
		    	$.noty.clearQueue();
		    	if($onComplete)
		    		$onComplete();
	    	}, 1000);
		}
	}

})(jQuery);




function lookup($url,$campos,$selecionar){
	if($campos){
		$campos = JSON.parse(atob($campos));
	}
	if($selecionar){
		$selecionar = JSON.parse(atob($selecionar));
	}
	if($url){
		$.post($url,{campos:$campos,selecionar:$selecionar},function(res){
			bootbox.dialog({
		        title: "Pesquisar",
		        message: res,
		        buttons: {
		            success: {
		                label: "Selecionar",
		                className: "btn-success btnSelecionar",
		                callback: function () {
		                	bootbox.hideAll();
		                }
		            }
		        }
		    });
		}, 'html');
	}
}

function enableCadastrar($form){
	$("#"+$form+" .incluir").removeAttr("disabled");
	$("#"+$form+" .incluir").css("display","");
	$("#"+$form+" .alterar").attr("disabled",true);
	$("#"+$form+" .alterar").css("display","none");
	$("#"+$form+" .excluir").attr("disabled",true);
	$("#"+$form+" .excluir").css("display","none");
	$("#"+$form+" .limpar").attr("disabled",true);
	$("#"+$form+" .limpar").css("display","none");
}

function enableAlterar($form){
	$("#"+$form+" .incluir").attr("disabled",true);
	$("#"+$form+" .incluir").css("display","none");
	$("#"+$form+" .alterar").removeAttr("disabled");
	$("#"+$form+" .alterar").css("display","");
	$("#"+$form+" .excluir").removeAttr("disabled");
	$("#"+$form+" .excluir").css("display","");
	$("#"+$form+" .limpar").removeAttr("disabled");
	$("#"+$form+" .limpar").css("display","");
}

//**dataURL to blob**
function dataURLtoBlob(dataurl) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], {type:mime});
}

//**blob to dataURL**
function blobToDataURL(blob, callback) {
    var a = new FileReader();
    a.onload = function(e) {callback(e.target.result);}
    a.readAsDataURL(blob);
}

Array.prototype.objIndexOf = function(val) {
    var cnt =-1;
    for (var i=0, n=this.length;i<n;i++) {
      cnt++;
      for(var o in this[i]) {
        if (this[i][o]==val) return cnt;
      }
    }
    return -1;
}

function indexOf2d(arr, val) {
    var index = [-1, -1];

    if (!Array.isArray(arr)) {
        return index;
    }

    arr.some(function (sub, posX) {
        if (!Array.isArray(sub)) {
            return false;
        }

        var posY = sub.indexOf(val);

        if (posY !== -1) {
            index[0] = posX;
            index[1] = posY;
            return true;
        }

        return false;
    });

    return index;
}

$.extend({
  password: function (length, special) {
    var iteration = 0;
    var password = "";
    var randomNumber;
    if(special == undefined){
        var special = false;
    }
    while(iteration < length){
        randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
        if(!special){
            if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
            if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
            if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
            if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
        }
        iteration++;
        password += String.fromCharCode(randomNumber);
    }
    return password;
  }
});

var $validator = (function ($) {

    // Cria a vid
	var $dialog = $(
		'<div id="loadingPadrao" style="background-color:#fff1a8; width:200px; border:1px solid #ccc; -webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.75); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.75); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.75); padding: 5px; z-index:9999999; position:fixed; top:-100px; left:50%; margin-left:-100px; text-align:center; -webkit-border-radius: 2px; -moz-border-radius: 2px; border-radius: 2px;">' +
			'<p></p>'+
		'</div>' );

	return {
		/*
    	 * Exemplo 
    	 * 
    	 * <input type="email" name="email" data-error="Email obrigatório" data-type="email"  required placeholder="E-mail" />
    	 * 
    	 * <input type="text" name="celular" mask="telefone" data-error="Celular obrigatório" data-type="string" placeholder="Celular" required />
    	 * 
    	 */
        validar: function($form) {
        	var erros = [];
            var $form = $("#"+$form);
            var formData = new FormData();
            $form.find("input, select, textarea").each(function(index){
                var $obj = $(this);
                if($obj.attr("data-type") == "string"){
                	if($obj.val() != "" || $obj.attr("required") != undefined){
	                    if (!$obj.inSizeRangeString(1)){
	                        erros.push({
	                            msg: $obj.attr("data-error")
	                        });
	                    }else{
	                    	formData.append($obj.attr("name"), $obj.val());
	                    }
                	}
                }else if($obj.attr("data-type") == "url"){
                	if($obj.val() != "" || $obj.attr("required") != undefined){
	                	if (!$obj.isValidUrl()){
	                        erros.push({
	                            msg: $obj.attr("data-error")
	                        });
	                    }else{
	                    	formData.append($obj.attr("name"), $obj.val());
	                    }
                	}
                }else if($obj.attr("data-type") == "select"){
                	if($obj.val() != "" || $obj.attr("required") != undefined){
	                	if ($obj.val() == ""){
	                		erros.push({
	                			msg: $obj.attr("data-error")
	                		});
	                	}else{
	                    	formData.append($obj.attr("name"), $obj.val());
	                    }
                	}
                }else if($obj.attr("data-type") == "email"){
                	if($obj.val() != "" || $obj.attr("required") != undefined){
	                    if (!$obj.isValidEMAIL()){
	                        erros.push({
	                            msg: $obj.attr("data-error")
	                        });
	                    }else{
	                    	formData.append($obj.attr("name"), $obj.val());
	                    }
                	}
                }else if($obj.attr("data-type") == "radio-check"){
                	var $tipos = $(this).attr("name");
                	if($form.find("input[name="+$tipos+"]").length > 1){
                		if(!$form.find("input[name="+$tipos+"]:checked").length){
        	        		erros.push({
        	                    msg: $("input[name="+$tipos+"]").attr("data-error")
        	                });
                		}
                	}else if(!$passou){
                		if(!$form.find("input[name="+$tipos+"]:checked").length){
        	        		erros.push({
        	                    msg: $("input[name="+$tipos+"]").attr("data-error")
        	                });
                		}
                	}
                }else if($obj.attr("data-type") == "external" && $obj.attr("data-valida-url")){
	          	  var $data = $.param({campo:$($obj).val()});
          		     $http({
                          method: "post",
                          url: $obj.attr("data-valida-url"),
                          data: $data
                      }).success(function($data, $status, $headers, $config){
                      	if($data.status == "error"){
                      		erros.push({
        	                    msg: $("input[name="+$tipos+"]").attr("data-error")
        	                });
                      	}
          			  }).error(function($data, $status, $headers, $config) {
          				
          			  });
                }else if($obj.attr("type") == "file"){
        			if($obj.attr("name")){
        				if($obj[0].files.length == 1){
        					formData.append($obj.attr("name"), $obj[0].files[0]);
        				}else if($obj[0].files.length > 1){
            				$tamanho = $obj[0].files.length;
            				for(var $i=0;$i<$tamanho;$i++){
            					$nome = $obj.attr("name");
            					formData.append($nome+"[]", $obj[0].files[$i]);
            				}
        				}
        			}
        		}
                 
            });
            
            var msg = erros.map(function(obj) { return obj.msg; });
            erros = msg.filter(function(v,i) { return msg.indexOf(v) == i; });
            if(erros.length > 0){
	            var _mensagem = "";
	            $(erros).each(function(idx, item) {
	                _mensagem += item + '<br>';
	            });
	            noty({
	    		    text: _mensagem,
	    		    modal:true,
	    		    killer: true,
	    		    layout:"center",
	    		    type:"information",
	    		    timeout:5000
	    		});
	            return false;
            }else{
            	return formData;
            }
        }
	}

})(jQuery);

$.fn.inSizeRangeString = function(min, max) {
    var len = this.val().length;
    if (this.val() == this.attr('placeholder')) return false;
    if (min != null && max != null) {
        if (len < min || len > max)return false;
    } else if (min != null && max == null) {
        if (len < min)return false;
    } else if (min == null && max != null) {
        if (len > max)return false;
    }
    return true;
};
$.fn.isValidEMAIL = function() {
    var email = this.val();
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(email);
};

$.fn.isValidUrl = function() {
    var url = this.val();
    var urlPattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return urlPattern.test(url);
};