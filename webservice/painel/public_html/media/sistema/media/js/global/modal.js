
var table = "";

$(function () {
	table = $('#tablesModal').DataTable({
	  "paging": true,
	  "lengthChange": true,
	  "searching": true,
	  "ordering": true,
	  "info": true,
	  "autoWidth": false,
	  "ajax": _baseUrl+_modulo+"/"+$_controller+"/index",
	  "processing": true,
      "serverSide": true,
      "responsive": true,
      "language": {
	    "sEmptyTable": "Nenhum registro encontrado",
	    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
	    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
	    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
	    "sInfoPostFix": "",
	    "sInfoThousands": ".",
	    "sLengthMenu": "_MENU_ resultados por página",
	    "sLoadingRecords": "Carregando...",
	    "sProcessing": "Processando...",
	    "sZeroRecords": "Nenhum registro encontrado",
	    "sSearch": "Pesquisar",
	    "oPaginate": {
	        "sNext": "Próximo",
	        "sPrevious": "Anterior",
	        "sFirst": "Primeiro",
	        "sLast": "Último"
	    },
	    "oAria": {
	        "sSortAscending": ": Ordenar colunas de forma ascendente",
	        "sSortDescending": ": Ordenar colunas de forma descendente"
	    }
      },
      "columns": $_camposJson,
	});
});

$('#tablesModal tbody').on( 'click', 'tr', function () {
    if ( $(this).hasClass('selected') ) {
        $(this).removeClass('selected');
    }
    else {
        table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
    }
} );

$('.btnSelecionar').click( function () {
    $dadosSelecionado = table.row('.selected').data();
    var $campos = Object.keys($parametros.campos);
    var $name = "";
    
    for(var $index in $_camposSelecionar){
    	$name = $_camposSelecionar[$index];
    	$("input[name="+$name+"]").val($dadosSelecionado[$_camposSelecionar[$index]]);
    }
    
    
} );