

var table = "";


$(function () {
	table = $('#tablesGeral').DataTable({
	  "paging": true,
	  "lengthChange": true,
	  "searching": true,
	  "ordering": true,
	  "info": true,
	  "autoWidth": false,
	  "ajax": _baseUrl+"sistema/sca-modulo/index",
	  "processing": true,
      "serverSide": true,
      "responsive": true,
      "language": {
	    "sEmptyTable": "Nenhum registro encontrado",
	    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
	    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
	    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
	    "sInfoPostFix": "",
	    "sInfoThousands": ".",
	    "sLengthMenu": "_MENU_ resultados por página",
	    "sLoadingRecords": "Carregando...",
	    "sProcessing": "Processando...",
	    "sZeroRecords": "Nenhum registro encontrado",
	    "sSearch": "Pesquisar",
	    "oPaginate": {
	        "sNext": "Próximo",
	        "sPrevious": "Anterior",
	        "sFirst": "Primeiro",
	        "sLast": "Último"
	    },
	    "oAria": {
	        "sSortAscending": ": Ordenar colunas de forma ascendente",
	        "sSortDescending": ": Ordenar colunas de forma descendente"
	    }
      },
      "columns": [
          {
        	  'targets': 0,
              "searchable":      false,
              "orderable":      false,
              "data":           "id",
              'render': function (data, type, full, meta){
                  return '<input type="checkbox" name="id[]" value="' 
                     + $('<div/>').text(data).html() + '">';
              }
          },
          { "data": "id" },
          { "data": "nome" },
          {
              "orderable":      false,
              "data":           'alterar',
              "defaultContent": ''
          },
          {
              "orderable":      false,
              "data":           'remover',
              "defaultContent": ''
          },
      ],
	});
	
	// checar todos
    $('#example-select-all').on('ifChecked', function(event){
    	// Check/uncheck all checkboxes in the table
 	   	var rows = table.rows({ 'search': 'applied' }).nodes();
 	   	$('input[type="checkbox"]', rows).prop('checked', this.checked);
	});
    // descelecionar todos
    $('#example-select-all').on('ifUnchecked', function(event){
    	// Check/uncheck all checkboxes in the table
 	   	var rows = table.rows({ 'search': 'applied' }).nodes();
 	   	$('input[type="checkbox"]', rows).prop('checked', false);
	});
});

function excluirSelecionados(){
	  var $ids = [];
      // Iterate over all checkboxes in the table
      table.$('input[type="checkbox"]').each(function(){
    	  
	        // If checkbox is checked
	        if(this.checked){
	        	$ids.push(this.value);
	        	
	        }
      });
      if($ids.length){
    	  alertify.confirm("Deseja realmente excluir os selecionados? ",function(status){
    		  if(status){
			      $.post(_baseUrl+_modulo+"/"+_controller+"/excluir",{id: $ids},function(res){
						if (res.status == "sucesso") {
							$('#tablesGeral').DataTable().ajax.reload();
						}else if(res.status == "erro"){
							notificar.open(res.msg,3000,"error");
						}else{
							notificar.open(res,3000,"error");
						}
				  }, 'json');
    		  }
    	  });
      }else{
    	  notificar.open("Nenhum item selecionado para remover",3000,"error");
      }
}



function DeletarIndex($id){
	alertify.confirm("Deseja realmente excluir este modulo? ",function(status){
		if(status){
			$.post(_baseUrl+_modulo+"/"+_controller+"/excluir",{id: $id},function(res){
				if (res.status == "sucesso") {
					$('#tablesGeral').DataTable().ajax.reload();
				}else if(res.status == "erro"){
					notificar.open(res.msg,3000,"error");
				}else{
					notificar.open(res,3000,"error");
				}
			}, 'json');
		}
	});
}

function atualizarTabela(){
	$('#tablesGeral').DataTable().ajax.reload();
}

//angular login
app.controller('sca-modulo_index', function index_login($scope,$http,$validator,$notify ) {		
	
});