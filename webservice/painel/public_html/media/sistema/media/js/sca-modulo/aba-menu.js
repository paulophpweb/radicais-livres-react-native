/**
 * Grava os dados do grupo no scope
 */
function getActions($controller,$selected) {
	var $data = $.param({controle:$controller});
	if($controller){
		$.post(_baseUrl+_modulo+"/index/get-acao-by-controller",{controle:$controller},function(res){
			$('.acaoSelect').html("");
			$('.acaoSelect').attr('disabled',false);
			
			var $options = "";
			for(var $index in res['dados']){
				if(res['dados'][$index].id != undefined)
					$options += "<option "+($selected == res['dados'][$index].action ? 'selected="selected"' : "")+" value='"+res['dados'][$index].action+"'>"+res['dados'][$index].action+"</option>"
			}
			
			$('.acaoSelect').append($options);
			if($selected){
				$("#formMenu select[name=nm_action]").select2("destroy");
				$("#formMenu select[name=nm_action]").select2();
			}
		}, 'json');
	}else{
		$('.acaoSelect').html("");
		$('.acaoSelect').attr('disabled',true);
		$('.acaoSelect').html("<option value=''>Selecione uma ação</option>");
	}
    
}

/**
 * Verfica o tipo de acao dos botoes e faz a acao
 */
function btnAcaoMenu($tipo){
	if($tipo == "limpar"){
		limparFormularioMenu();
	}else if($tipo == "alterar"){
		if($validator.validar('formMenu')){
			$.post(_baseUrl+_modulo+"/"+_controller+"/alterar-menu",$("#formMenu").serializeArray(),function(res){
				if(res){
        			$('.resultCadastro').html('');
        			$('.resultCadastro').html(res);
				}else{
					notificar.open("Um erro inesperado aconteceu.",3000,"error");
				}
			}, 'html');
		}

	}else if($tipo == "incluir"){
		if($validator.validar('formMenu')){
			$.post(_baseUrl+_modulo+"/"+_controller+"/incluir-menu",$("#formMenu").serializeArray(),function(res){
				if(res){
        			$('.resultCadastro').html('');
        			$('.resultCadastro').html(res);
				}else{
					notificar.open("Um erro inesperado aconteceu.",3000,"error");
				}
				
			}, 'html');
		}
	}else if($tipo == "excluir"){
		excluirMenu({id:$('#formMenu input[name=id]').val(),modulo_id:$('#formMenu input[name=modulo_id]').val()});
	}
}

function excluirMenu($dados){
	alertify.confirm("Deseja realmente excluir este menu?",function(status){
		if(status){
			$.post(_baseUrl+_modulo+"/"+_controller+"/excluir-menu",$dados,function(res){
				if(res){
        			$('.resultCadastro').html('');
        			$('.resultCadastro').html(res);
        			limparFormularioMenu();
				}else{
					notificar.open("Um erro inesperado aconteceu.",3000,"error");
				}
		  }, 'html');
		}
	});
}

function editarMenu($dados){

	$("#formMenu input[name=nome]").val($dados.nome);
	$('#formMenu select[name=nm_controller]').attr("data-selected",$dados.nm_action);
	$('#formMenu select[name=nm_controller]').val($dados.nm_controller).trigger('change');
	$('#formMenu select[name=ordem]').val($dados.ordem).trigger('change');
	$('#formMenu select[name=status]').val($dados.status).trigger('change');
	
	$("#formMenu input[name=id]").val($dados.id);
	$("#formMenu input[name=modulo_id]").val($dados.modulo_id);
	enableAlterar('formMenu');
	
}


$(function(){
	enableCadastrar('formMenu');
});

function limparFormularioMenu(){
	$("#formMenu input[name=nome]").val("");
	$('#formMenu select[name=nm_controller]').val("").trigger('change');
	$('#formMenu select[name=ordem]').val("").trigger('change');
	$('#formMenu select[name=status]').val("0").trigger('change');
	$("#formMenu input[name=id]").val("");
	$("#formMenu select").select2("destroy");
	$("#formMenu select").select2();
	enableCadastrar('formMenu');
}
	
	


