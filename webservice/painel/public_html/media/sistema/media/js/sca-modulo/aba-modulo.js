

$(function(){
	
});


/**
 * Verfica o tipo de acao dos botoes e faz a acao
 */
function btnAcao($tipo){
	if($tipo == "limpar"){
		var $obj = $("#limpar");
		$('#form').each (function(){
			  this.reset();
		});
		window.location.href= _baseUrl+_modulo+"/"+_controller+"/form";
	}else if($tipo == "alterar"){
		if($validator.validar('form')){
			$.post(_baseUrl+_modulo+"/"+_controller+"/alterar",$("#form").serializeArray(),function(res){
				if (res.status == "sucesso") {
					notificar.open(res.msg,2000,"success");
				}else if(res.status == "erro"){
					notificar.open(res.msg,3000,"error");
				}else{
					notificar.open(res,3000,"error");
				}
			}, 'json');
		}

	}else if($tipo == "incluir"){
		if($validator.validar('form')){
			$.post(_baseUrl+_modulo+"/"+_controller+"/incluir",$("#form").serializeArray(),function(res){
				if (res.status == "sucesso") {
					notificar.open(res.msg,2000,"success",function(){
	        			window.location.href= _baseUrl+_modulo+"/"+_controller+"/form/id/"+res.dados.id;
	        		});
				}else if(res.status == "erro"){
					notificar.open(res.msg,3000,"error");
				}else{
					notificar.open(res,3000,"error");
				}
			}, 'json');
		}
	}else if($tipo == "excluir"){
		alertify.confirm("Deseja realmente excluir este módulo?",function(status){
			if(status){
				$.post(_baseUrl+_modulo+"/"+_controller+"/excluir",{id: $('input[name=id]').val()},function(res){
					if (res.status == "sucesso") {
						window.location.href= _baseUrl+_modulo+"/"+_controller+"/index";
					}else if(res.status == "erro"){
						notificar.open(res.msg,3000,"error");
					}else{
						notificar.open(res,3000,"error");
					}
			  }, 'json');
			}
		});
	}
}