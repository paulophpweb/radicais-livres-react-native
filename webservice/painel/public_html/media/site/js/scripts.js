
! function($) {
    $(function() {
    	$baseUrl = $('body').attr('data-baseUrl');
        $assets = $baseUrl + 'assets/';

			"use strict";
			var masterslider_15c2 = new MasterSlider();

			// slider controls
			masterslider_15c2.control('arrows'     ,{ autohide:true, overVideo:true  });
			masterslider_15c2.control('bullets'    ,{ autohide:true, overVideo:true, dir:'h', align:'bottom', space:6 , margin:10  });
			masterslider_15c2.control('timebar'    ,{ autohide:false, overVideo:true, align:'bottom', color:'#FFFFFF'  , width:4 });
			// slider setup
			masterslider_15c2.setup("MS587deea2515c2", {
				width           : 1400,
				height          : 600,
				minHeight       : 0,
				space           : 0,
				start           : 1,
				grabCursor      : true,
				swipe           : false,
				mouse           : true,
				keyboard        : false,
				layout          : "fullwidth",
				wheel           : false,
				autoplay        : true,
				instantStartLayers:true,
				loop            : true,
				shuffle         : false,
				preload         : 0,
				heightLimit     : true,
				autoHeight      : false,
				smoothHeight    : true,
				endPause        : false,
				overPause       : true,
				fillMode        : "fill",
				centerControls  : false,
				startOnAppear   : false,
				layersMode      : "center",
				autofillTarget  : "",
				hideLayers      : false,
				fullscreenMargin: 0,
				speed           : 10,
				dir             : "h",
				parallaxMode    : 'swipe',
				view            : "basic"
			});
        
    })
}(window.jQuery)