<?php

class IndexController extends App_Controller_BaseController 
{
    public $models = array('SggAvatar','Acl','ScaUsuario');
    public $modelAtual = '';
    public $msg = null;

	public function indexAction()
	{
	 $tblModulo = new Painel_Model_CgModulo();
	 $tblModulo->listarTodos();

	}
	
	public function loginAction()
	{
	      
	}
	
	public function requestBitbucketAction()
	{
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout()->disableLayout();
	    $deploy = new App_Custom_BitbucketDeploy();
	}
	
	public function logarAction()
	{
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout()->disableLayout();
	    $resposta = array();
	    
	    $dbAdapter = Zend_Db_Table_Abstract::getDefaultAdapter();
	    $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
	    
	    $authAdapter->setTableName('sca_usuario')
	    ->setIdentityColumn('login_usuario')
	    ->setCredentialColumn('password_usuario')
	    ->getDbSelect()
	    ->joinLeft( array('g' => 'sca_grupo'), 'g.id_grupo = sca_usuario.id_grupo', array('nm_grupo','is_root') );
	    
	    $authAdapter->setIdentity($this->getRequest()->getParam('login_usuario'))
	    ->setCredential($this->getRequest()->getParam('password_usuario'))
	    ->setCredentialTreatment('MD5(?) and st_usuario = 1');
	    
	    //Realiza autenticacao
	    $result = $authAdapter->authenticate();
	    //Verifica se a autenticacao foi validada
	    if($result->isValid()){
	        //obtem os dados do usuario
	        $usuario = $authAdapter->getResultRowObject();
	        $tblUsuario = new Painel_Model_ScaUsuario();
	        $data = array('id_usuario' => $usuario->id_usuario,'ultimo_acesso_usuario' => date('Y-m-d H:i:s'));
	        $tblUsuario->save($data,$this->msg);
	        //Armazena seus dados na sessao
	        $storage = Zend_Auth::getInstance()->getStorage();
	        $storage->write($usuario);
	        // se não for para lembrar os dados expira a sessao em 30 minutos
	        if(!$this->getRequest()->getParam('lembrar')){
    	        $session = new Zend_Session_Namespace( 'Zend_Auth' );
    	        $session->setExpirationSeconds( 1800 );
	        }
	        //Redireciona para o Index
	        $resposta['situacao'] = "success";
	        $resposta['msg'] = "Logando aguarde...";
	    }else{
	        $resposta['situacao'] = "error";
	        $resposta['msg'] = "Usuário inativo ou senha incorreta.";
	    }
	    
	    echo json_encode($resposta);
	}
	
	public function getAvatarAction()
	{
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout()->disableLayout();
	    $resposta = array();
	    $id_avatar = $this->_getParam("id_avatar");
	    $avatar = $this->modelSggAvatar->fetchByKey($id_avatar,$this->msg); 
	    
	    if($avatar){
	        //Redireciona para o Index
	        $resposta['situacao'] = "sucesso";
	        $resposta['msg'] = "Carregado com sucesso.";
	        $resposta['dados'] = $avatar;
	    }else{
	        $resposta['situacao'] = "error";
	        $resposta['msg'] = "Erro ao carregar o avatar.";
	    }
	     
	    echo json_encode($resposta);
	}
	
	public function getDashAction()
	{
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout()->disableLayout();
	    if($this->view->sessao){
    	    $resposta = array();
    	    $usuarios = $this->modelScaUsuario->fetchAll("st_usuario = 1");
    
            $resposta['situacao'] = "sucesso";
            $resposta['msg'] = "Carregado com sucesso.";
            $resposta['dados'] = array('qtde_usuario' => count($usuarios));
	    }else{
	        $resposta['situacao'] = "erro";
	        $resposta['msg'] = "Precisa estar logado.";
	    }

	
	    echo json_encode($resposta);
	}
	
	public function getAcaoAction()
	{
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout()->disableLayout();
	    $resposta = array();
	    $acao = array();
	    $myAcao = array();
	    $controle = $this->_getParam("controle");
	    $id_grupo = $this->_getParam("id_grupo");
	    if($controle)
	       $acao = $this->modelAcl->getActionByController($controle);
	    if($id_grupo)
	       $myAcao = $this->modelAcl->getActionByControllerMy($controle,$id_grupo);
	    foreach ($acao as $key => $value){
	        if($this->in_multiarray($value['id'], $myAcao,'id')){
	            $acao[$key]['checked'] = true;
	        }else{
	            $acao[$key]['checked'] = false;
	        }
	    }
	     
	    if($acao){
	        $resposta['status'] = "sucesso";
	        $resposta['msg'] = "Carregado com sucesso.";
	        $resposta['dados'] = $acao;
	        $resposta['dados_my_action'] = $myAcao;
	    }else{
	        $resposta['status'] = "error";
	        $resposta['msg'] = "Erro ao carregar o avatar.";
	    }
	
	    echo json_encode($resposta);
	}
	
	public function incluirAclAction()
	{
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout()->disableLayout();
	    $resposta = array();
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $removerAclToGrupo = $this->modelAcl->removeAclToGrupo('atg.grupo_id in('.$post['id_grupo'].') and a.controller = \''.$post['controle'].'\' ',$this->msg);
	        if($removerAclToGrupo){
	            if(isset($post['acl_id'])){
    	            foreach ($post['acl_id'] as $value){
    	                $form = array(
    	                    'acl_id' => $value,
    	                    'grupo_id' => $post['id_grupo']
    	                );
    	                $removerAclToGrupo = $this->modelAcl->salvarAclToGrupo($form,$this->msg);
    	            }
	            }
	            
	        }
	        
    	    if($removerAclToGrupo){
    	        $resposta['status'] = "sucesso";
    	        $resposta['msg'] = "Registro inserido com sucesso!";
    	    }else{
    	        $resposta['status'] = "error";
    	        $resposta['msg'] = $this->msg;
    	    }
	    }else{
	        $resposta['status'] = "error";
	        $resposta['msg'] = "Erro ao receber os dados.";
	    }
	
	    echo json_encode($resposta);
	}
	
	public function sairAction()
	{
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout()->disableLayout();
	    Zend_Auth::getInstance()->clearIdentity();
	    return $this->_helper->redirector->goToRoute( array('controller' => 'index'), null, true);
	
	
	}
	
	public function sessaoAction()
	{
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout()->disableLayout();
	    $resposta = array();
	    
	    $resposta['situacao'] = "success";
	    $resposta['dados'] = $this->view->sessao;
	    
	    echo json_encode($resposta);

	}
	
	private function in_multiarray($elem, $array,$field)
	{
	    $top = sizeof($array) - 1;
	    $bottom = 0;
	    while($bottom <= $top)
	    {
	        if($array[$bottom][$field] == $elem)
	            return true;
	        else
	            if(is_array($array[$bottom][$field]))
	                if(in_multiarray($elem, ($array[$bottom][$field])))
	                    return true;
	
	                $bottom++;
	    }
	    return false;
	}

}