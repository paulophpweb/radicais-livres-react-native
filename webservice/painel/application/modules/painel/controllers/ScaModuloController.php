<?php

class ScaModuloController extends App_Controller_BaseController
{
	public $models = array('ScaMenu');
	public $modelAtual = 'ScaModulo';
	public $msg = null;
	
	
	/**
	 * Lista os dados na view
	 */
	public function getMenuPorModuloAction()
	{ 
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    
	    $this->view->remover = Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->controle, "remover-menu");
	    $this->view->alterar = Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->controle, "alterar-menu");

        $offset        		= $this->_getParam('offset',0);
        $page          		= $this->_getParam('page',1);
        $registroPagina     = $this->_getParam('count',10);
         
        $aPesquisa = array("modulo_id" => $this->_getParam("modulo_id",null));
        $order = "";
        $offset = ($registroPagina*$page)-$registroPagina;
        if($this->_getParam("filter")){
             
            // pega todos os dados do filtro de pesquisa
            foreach ($this->_getParam("filter") as $key => $value){
                if(!is_numeric($value)){
    	               $aPesquisa[$key] = urldecode($value);
                }else{
                    $aPesquisa[$key] = intval($value);
                }
            }
        }
         
        // pega os dados de ordenacao
        if($this->_getParam("sorting")){
            $parametro = $this->_getParam("sorting");
            $order = key($this->_getParam("sorting"))." ".$parametro[key($this->_getParam("sorting"))];
        }
        $res = $this->modelScaMenu->listarTodos($aPesquisa,$registroPagina,$offset,$order);
         
	   foreach ($res["res"] as $key => $value)
        {
            $res["res"][$key]["del"] = "true";
            if(!$this->view->remover){
                $res["res"][$key]["selected"] = "false";
                $res["res"][$key]["del"] = "false";
            }
            if($this->view->alterar){
                $res["res"][$key]["selected"] = "false";
                $res["res"][$key]["alter"] = "true";
            }
        }

        echo json_encode(array("msg"=>"Dados carregado","status" => "sucesso","dados" => $res));
	
	}
	
	public function getBotaoMenuAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    $id        		= $this->getRequest()->getParam('id');
	    $dados = array();
	    $podeIncluir = Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->controle, "incluir-menu");
	    $podeAlterar = Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->controle, "alterar-menu");
	    $podeRemover = Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->controle, "remover-menu");
	    if(!$id){
	
	        $dados[] = array('text' => "Incluir",'classe' => 'btn btn-success','model' => "btn.incluir",'btn' => "incluir","disabled" => $podeIncluir ? "" : "disabled");
	        $dados[] = array('text' => "Alterar",'classe' => 'btn btn-info','model' => "btn.alterar",'btn' => "alterar","disabled" => 'disabled');
	        $dados[] = array('text' => "Remover",'classe' => 'btn btn-danger','model' => "btn.remover",'btn' => "remover","disabled" => 'disabled');
	        $dados[] = array('text' => "Limpar",'classe' => 'btn btn-default','model' => "btn.limpar",'btn' => "limpar","disabled" => $podeIncluir ? "" : "disabled");
	
	    }else if($id){
	        $dados[] = array('text' => "Incluir",'classe' => 'btn btn-success','model' => "btn.incluir",'btn' => "incluir","disabled" => "disabled");
	        $dados[] = array('text' => "Alterar",'classe' => 'btn btn-info','model' => "btn.alterar",'btn' => "alterar","disabled" => $podeAlterar ? "" : "disabled");
	        $dados[] = array('text' => "Remover",'classe' => 'btn btn-danger','model' => "btn.remover",'btn' => "remover","disabled" => $podeRemover ? "" : "disabled");
	        $dados[] = array('text' => "Limpar",'classe' => 'btn btn-default','model' => "btn.limpar",'btn' => "limpar","disabled" => $podeIncluir ? "" : "disabled");
	    }
	
	    $html = "";
	    foreach ($dados as $key => $value){
	        $html .= '<button '.$value['disabled'].' style="'.($value['disabled'] ? "opacity:0.1;" : "").'" type="button" ng-model="'.$value['model'].'" ng-click="btnAcao(\''.$value['btn'].'\')" class="'.$value['classe'].' '.$value['btn'].'">'.$value['text'].'</button>';
	    }
	
	    echo $html;
	}
	/**
	 * Incluir um grupo
	 */
	public function incluirAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $form = array(
	            'id' => isset($post['id']) ? $post['id'] : '',
	            'nome' => $post['nome'],
	            'icone' => $post['icone'],
	            'ordem' => $post['ordem'],
	            'status' => $post['status']
	        );
	        $result = $this->model->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	            $resposta['dados'] = $result;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	    
	    echo json_encode($resposta);
	}
	
	/**
	 * Incluir um grupo
	 */
	public function incluirMenuAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $form = array(
	            'id' => isset($post['id']) ? $post['id'] : '',
	            'modulo_id' => $post['modulo_id'],
	            'nm_action' => $post['nm_action'],
	            'nm_controller' => $post['nm_controller'],
	            'nm_modulo' => $post['nm_modulo'],
	            'nome' => $post['nome'],
	            'ordem' => $post['ordem'],
	            'status' => $post['status']
	        );
	        $result = $this->modelScaMenu->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	            $resposta['dados'] = $result;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	     
	    echo json_encode($resposta);
	}
	
	/**
	 * Alterar um grupo
	 */
	public function alterarAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $form = array(
	            'id' => isset($post['id']) ? $post['id'] : '',
	            'nome' => $post['nome'],
	            'icone' => $post['icone'],
	            'ordem' => $post['ordem'],
	            'status' => $post['status']
	        );
	       
	        $result = $this->model->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	    
	    echo json_encode($resposta);
	    
	}
	
	/**
	 * Alterar um menu
	 */
	public function alterarMenuAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $form = array(
	            'id' => isset($post['id']) ? $post['id'] : '',
	            'modulo_id' => $post['modulo_id'],
	            'nm_action' => $post['nm_action'],
	            'nm_controller' => $post['nm_controller'],
	            'nm_modulo' => $post['nm_modulo'],
	            'nome' => $post['nome'],
	            'ordem' => $post['ordem'],
	            'status' => $post['status']
	        );
	
	        $result = $this->modelScaMenu->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	     
	    echo json_encode($resposta);
	     
	}
	/**
	 * Pega o grupo por id
	 */
	public function getMenuByIdAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    $id = $this->_getParam("id");
	    
	    $res = $this->modelScaMenu->fetchByKey($id,$this->msg);

	    echo json_encode(array("msg"=>$this->msg,"status" => "sucesso","dados" => $res));
	}
	/**
	 * Pegas as abas e lista na view
	 */
	public function getAbasAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    $request = Zend_Controller_Front::getInstance()->getRequest();
	    $res = array();
	    $modulo = array();
	    $menu = array();
	    if($this->_getParam("id_modulo")){
	       $modulo = $this->model->fetchByKey($this->_getParam("id_modulo"),$this->msg);
	       $menu = $this->modelScaMenu->fetchByModulo($this->_getParam("id_modulo"),$this->msg);
	    }
	    
	    if(Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->controle, "aba-modulo"))
	       $res[] = array('title' => "Modulo",'url' => $this->_helper->url("aba-modulo",$this->controle),'disabled' => false,'modulo' => $modulo);
	    if(Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->controle, "aba-menu"))
	        $res[] = array('title' => "Menu",'url' => $this->_helper->url("aba-menu",$this->controle),'disabled' => false,'menu' => $menu);
	    
	    echo json_encode(array("msg"=>"Abas carregada","status" => "sucesso","dados" => $res));
	}
	/**
	 * Lista a aba de usuário
	 */
	public function abaModuloAction()
	{
	    $this->_helper->layout()->disableLayout();

	}
	/**
	 * Lista a aba de usuário
	 */
	public function abaMenuAction()
	{
	    $this->_helper->layout()->disableLayout();
	
	}
	/**
	 * remove um grupo ou mais
	 */
	public function removerAction()
	{
	    
	    $resposta = array();
	    $array = false;
	    $condicao = true;
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout()->disableLayout();
	    
	    $ids = $this->getRequest()->getParam('id');
	    if($ids){
    	    if(is_array($ids)){
    	       $array = true;
    	       $ids = implode(",", $this->getRequest()->getParam('id'));
    	       $integerIDs = array_map('intval', $this->getRequest()->getParam('id')); 
    	    }
    	    if($array){
    	        if(in_array((int)$this->view->sessao->id_grupo, $integerIDs)){
    	            $condicao = false;
    	        } 
    	    }
    	    if($condicao){
    	        // verifica se tem usuario neste grupo
    	        $menu = $this->modelScaMenu->fetchAll("modulo_id in(".$ids.")")->toArray();
    	        if($menu){
    	            $resposta['status'] = "erro";
    	            $resposta['msg'] = "Não foi possível excluir, existem menus relacionados a este módulo.";
    	            echo json_encode($resposta);
    	            exit();
    	        }
        	    // chama a funcao excluir
        	    $result = $this->model->remove("id in(".$ids.")",$this->msg);
        	     
        	    if($result){
        	        $resposta['status'] = "sucesso";
        	        $resposta['msg'] = $this->msg;
        	    }else{
        	        $resposta['status'] = "erro";
        	        $resposta['msg'] = $this->msg;
        	    }
        	     
        	    echo json_encode($resposta);
    	    }else{
    	        $resposta['status'] = "error";
    	        $resposta['msg'] = "Erro ao Excluir, você não pode excluir seu usuário!";
    	        
    	        echo json_encode($resposta);
    	    }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Nenhum dado recebido!";
	        echo json_encode($resposta);
	    }
	}
	
	/**
	 * remove um menu ou mais
	 */
	public function removerMenuAction()
	{
	     
	    $resposta = array();
	    $array = false;
	    $condicao = true;
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout()->disableLayout();
	     
	    $ids = $this->getRequest()->getParam('id');
	    if($ids){
	        if(is_array($ids)){
	    	       $array = true;
	    	       $ids = implode(",", $this->getRequest()->getParam('id'));
	    	       $integerIDs = array_map('intval', $this->getRequest()->getParam('id'));
	        }
	        if($condicao){
	            // chama a funcao excluir
	            $result = $this->modelScaMenu->remove("id in(".$ids.")",$this->msg);
	
	            if($result){
	                $resposta['status'] = "sucesso";
	                $resposta['msg'] = $this->msg;
	            }else{
	                $resposta['status'] = "erro";
	                $resposta['msg'] = $this->msg;
	            }
	
	            echo json_encode($resposta);
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Nenhum dado recebido!";
	        echo json_encode($resposta);
	    }
	}


}