<?php

class ScaGrupoController extends App_Controller_BaseController
{
	public $models = array('Acl','ScaUsuario');
	public $modelAtual = 'ScaGrupo';
	public $msg = null;
	/**
	 * Lista os dados na view
	 */
	public function indexAction()
	{
	    // verifica se tem acao para remover
	    $this->view->remover = Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->controle, "remover");
	    $this->view->alterar = Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->controle, "alterar");
	    
	    if ($this->getRequest()->isXmlHttpRequest()) {
	        $this->_helper->layout()->disableLayout();
	        $this->_helper->viewRenderer->setNoRender(true);

	        $offset        		= $this->_getParam('offset',0);
	        $page          		= $this->_getParam('page',1);
	        $registroPagina     = $this->_getParam('count',10);
	        
	        $aPesquisa = array();
	        $order = "";
	        $offset = ($registroPagina*$page)-$registroPagina;
	        if($this->_getParam("filter")){
	            
    	        // pega todos os dados do filtro de pesquisa
    	        foreach ($this->_getParam("filter") as $key => $value){
    	            if(!is_numeric($value)){
    	               $aPesquisa[$key] = urldecode($value);
    	            }else{
    	                $aPesquisa[$key] = intval($value);
    	            }
    	        }
	        }
	        
	        // pega os dados de ordenacao
	        if($this->_getParam("sorting")){
	            $parametro = $this->_getParam("sorting");
	           $order = key($this->_getParam("sorting"))." ".$parametro[key($this->_getParam("sorting"))];
	        }
	        $res = $this->model->listarTodos($aPesquisa,$registroPagina,$offset,$order);
	        
	        foreach ($res["res"] as $key => $value)
	        {
	            $res["res"][$key]["del"] = "true";
	            if($this->view->sessao->id_grupo == $res["res"][$key]["id_grupo"] || !$this->view->remover){
	               $res["res"][$key]["selected"] = "false";
	               $res["res"][$key]["del"] = "false";
	            }
	            if($this->view->alterar){
	                $res["res"][$key]["selected"] = "false";
	                $res["res"][$key]["alter"] = "true";
	            }
	        }

	        echo json_encode(array("msg"=>"Dados carregado","status" => "sucesso","dados" => $res));
	    }
		
	}
	
	/**
	 * Incluir um grupo
	 */
	public function incluirAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $form = array(
	            'id_grupo' => isset($post['id_grupo']) ? $post['id_grupo'] : '',
	            'nm_grupo' => $post['nm_grupo'],
	            'dh_cadastro' => date("Y-m-d H:i:s"),
	            'id_usuario_cadastro' => $this->view->sessao->id_usuario,
	            'is_root' => $post['is_root']
	        );
	        $result = $this->model->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	            $resposta['dados'] = $result;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	    
	    echo json_encode($resposta);
	}
	
	/**
	 * Alterar um grupo
	 */
	public function alterarAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $form = array();
	        $form['id_grupo'] = $post['id_grupo'];
	        $form['nm_grupo'] = $post['nm_grupo'];
	        $form['is_root'] = $post['is_root'];
	       
	        $result = $this->model->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	    
	    echo json_encode($resposta);
	    
	}
	/**
	 * Pega o grupo por id
	 */
	/*public function getGrupoAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    $id = $this->_getParam("id");
	    
	    $res = $this->model->fetchByKey($id,$this->msg);

	    echo json_encode(array("msg"=>$this->msg,"status" => "sucesso","dados" => $res));
	}*/
	/**
	 * Pegas as abas e lista na view
	 */
	public function getAbasAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    $request = Zend_Controller_Front::getInstance()->getRequest();
	    $res = array();
	    $grupo = array('is_root' => 1);
	    $controllers = array();
	    if($this->_getParam("id_grupo")){
	       $grupo = $this->model->fetchByKey($this->_getParam("id_grupo"),$this->msg);
	       $controllers = $this->modelAcl->getAllResources();
	    }
	    
	    if(Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->controle, "aba-grupo"))
	       $res[] = array('aba'=>'aba-grupo','title' => "Grupo",'url' => $this->_helper->url("aba-grupo",$this->controle),'disabled' => false,'grupo' => $grupo);
	    if(Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->controle, "aba-acl") && $grupo['is_root']  == 0)
	        $res[] = array('aba'=>'aba-acl','title' => "Controle de acesso",'url' => $this->_helper->url("aba-acl",$this->controle),'disabled' => ($grupo['is_root']  == 1 ? true : false),'controllers' => $controllers);
	    
	    echo json_encode(array("msg"=>"Abas carregada","status" => "sucesso","dados" => $res));
	}
	
	/**
	 * Lista a aba de usuário
	 */
	public function abaGrupoAction()
	{
	    $this->_helper->layout()->disableLayout();
	   

	}
	/**
	 * Lista a aba de controle de acesso
	 */
	public function abaAclAction()
	{
	    $this->_helper->layout()->disableLayout();
	
	}
	/**
	 * remove um grupo ou mais
	 */
	public function removerAction()
	{
	    
	    $resposta = array();
	    $array = false;
	    $condicao = true;
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout()->disableLayout();
	    
	    $ids = $this->getRequest()->getParam('id');
	    if($ids){
    	    if(is_array($ids)){
    	       $array = true;
    	       $ids = implode(",", $this->getRequest()->getParam('id'));
    	       $integerIDs = array_map('intval', $this->getRequest()->getParam('id')); 
    	    }
    	    if($array){
    	        if(in_array((int)$this->view->sessao->id_grupo, $integerIDs)){
    	            $condicao = false;
    	        } 
    	    }
    	    if($condicao){
    	        // verifica se tem usuario neste grupo
    	        $usuarios = $this->modelScaUsuario->fetchAll("id_grupo in(".$ids.")")->toArray();
    	        if($usuarios){
    	            $resposta['status'] = "erro";
    	            $resposta['msg'] = "Não foi possível excluir, existem usuários relacionados a este grupo.";
    	            echo json_encode($resposta);
    	            exit();
    	        }
        	    // chama a funcao excluir
        	    $result = $this->model->removeCustom("id_grupo in(".$ids.")",$ids,$this->msg);
        	     
        	    if($result){
        	        $resposta['status'] = "sucesso";
        	        $resposta['msg'] = $this->msg;
        	    }else{
        	        $resposta['status'] = "erro";
        	        $resposta['msg'] = $this->msg;
        	    }
        	     
        	    echo json_encode($resposta);
    	    }else{
    	        $resposta['status'] = "error";
    	        $resposta['msg'] = "Erro ao Excluir, você não pode excluir seu usuário!";
    	        
    	        echo json_encode($resposta);
    	    }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Nenhum dado recebido!";
	        echo json_encode($resposta);
	    }
	}
	/**
	 * (non-PHPdoc)
	 * @see App_Controller_BaseController::getBotaoAction()
	 * Pega o botao verificando as permissoes
	 */
	public function getBotaoAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    $id        		= $this->getRequest()->getParam('id');
	    $dados = array();
	    $podeIncluir = Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->controle, "incluir");
	    $podeAlterar = Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->controle, "alterar");
	    $podeRemover = Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->controle, "remover");
	    if(!$id){
	
	        $dados[] = array('text' => "Incluir",'classe' => 'btn btn-success','model' => "btn.incluir",'btn' => "incluir","disabled" => $podeIncluir ? "" : "disabled");
	        $dados[] = array('text' => "Alterar",'classe' => 'btn btn-info','model' => "btn.alterar",'btn' => "alterar","disabled" => 'disabled');
	        if($this->view->sessao->id_grupo != $id)
	           $dados[] = array('text' => "Remover",'classe' => 'btn btn-danger','model' => "btn.remover",'btn' => "remover","disabled" => 'disabled');
	        $dados[] = array('text' => "Limpar",'classe' => 'btn btn-default','model' => "btn.limpar",'btn' => "limpar","disabled" => $podeIncluir ? "" : "disabled");
	
	    }else if($id){
	        $dados[] = array('text' => "Incluir",'classe' => 'btn btn-success','model' => "btn.incluir",'btn' => "incluir","disabled" => "disabled");
	        $dados[] = array('text' => "Alterar",'classe' => 'btn btn-info','model' => "btn.alterar",'btn' => "alterar","disabled" => $podeAlterar ? "" : "disabled");
	        if($this->view->sessao->id_grupo != $id)
	           $dados[] = array('text' => "Remover",'classe' => 'btn btn-danger','model' => "btn.remover",'btn' => "remover","disabled" => $podeRemover ? "" : "disabled");
	        $dados[] = array('text' => "Limpar",'classe' => 'btn btn-default','model' => "btn.limpar",'btn' => "limpar","disabled" => $podeIncluir ? "" : "disabled");
	    }
	
	    $html = "";
	    foreach ($dados as $key => $value){
	        $html .= '<button '.$value['disabled'].' style="'.($value['disabled'] ? "opacity:0.1;" : "").'" type="button" id="'.$value['btn'].'" ng-model="'.$value['model'].'" ng-click="btnAcao(\''.$value['btn'].'\')" class="'.$value['classe'].'">'.$value['text'].'</button>';
	    }
	
	    echo $html;
	}


}