<?php
/**
 *
 * @author Paulo Henrique
 * @see http://www.pauloph.com.br
 */
class Sistema_Bootstrap extends Zend_Application_Module_Bootstrap
{
	
     protected function _initAutoload()
     {
      $autoloader = new Zend_Application_Module_Autoloader(array(
           'namespace' => 'Sistema_',
           'basePath' => dirname(__FILE__)
      ));
      return $autoloader;
     }
     
     protected function _initCache() {
         $cache = Zend_Cache::factory('Page', 'File', array(
             'lifetime' => 7200,
             'automatic_serialization' => true,
             'caching' => true
         ), array(
             'cache_dir' => APPLICATION_PATH."/../data/cache"
         ));
         Zend_Db_Table::setDefaultMetadataCache($cache);
         Zend_Date::setOptions(array(
         'cache' => $cache
         ));
     }
		
	public function _initPluginBrokers()
	{
		$front = Zend_Controller_Front::getInstance();
		$front->registerPlugin(new App_Plugin_Acl());
	}
}