<?php

class Sistema_ScaUsuarioController extends App_Controller_BaseController
{
	public $models = array('SggAvatar');
	public $modelAtual = 'ScaUsuario';
	public $msg = null;
	/**
	 * Lista os dados na view
	 */
	public function indexAction()
	{
	    // verifica se tem acao para remover
	    $this->view->remover = Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->modulo.":".$this->controle, "excluir");
	    $this->view->alterar = Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->modulo.":".$this->controle, "alterar");
	    
	    if ($this->getRequest()->isXmlHttpRequest()) {
	        $this->_helper->layout()->disableLayout();
	        $this->_helper->viewRenderer->setNoRender(true);

	        $offset        		= $this->_getParam('start',0);
	        $registroPagina     = $this->_getParam('length',10);
	        $pesquisa           = $this->_getParam('search','');
	        
	        $aPesquisa = array();
	        $order = "";
            
	        // faz a pesquisa
	        if(!is_numeric($pesquisa['value'])){
	            $aPesquisa['valor'] = urldecode($pesquisa['value']);
	        }else{
	            $aPesquisa['valor'] = intval($pesquisa['value']);
	        }
	        
	        // pega os dados de ordenacao
	        if($this->_getParam("order")){
	            $ordenar = $this->_getParam("order");
	            if($ordenar[0]['column'] && $ordenar[0]['dir']){
	               $coluna = $this->_getParam("columns");
	               $order = $coluna[$ordenar[0]['column']]['data']." ".$ordenar[0]['dir'];
	            }
	            $parametro = $this->_getParam("sorting");
	        }
	        $res = $this->model->listarTodos($aPesquisa,$registroPagina,$offset,$order);
	        foreach ($res["res"] as $key => $value)
	        {
	            if($this->view->alterar)
	               $res["res"][$key]['alterar'] = '<a class="btn btn-info btn-sm" href="'.($this->view->url(array("module" => "sistema","controller"=>$this->controle, "action"=>"form","id" => $res["res"][$key]['id_usuario']),null,true)).'">Editar</a>';
	               $res["res"][$key]['remover'] = $this->view->sessao->id_usuario == $res["res"][$key]["id_usuario"] || !$this->view->remover || $res["res"][$key]['is_root'] ? '' : '<a class="btn btn-danger btn-sm" onclick="ScaUsuarioDeletar('.($res["res"][$key]['id_usuario']).')" href="javascript:;">Remover</a>';
	        }

	        echo json_encode(array("data" => $res["res"],'draw' => $this->_getParam("draw",'1'),'recordsTotal' => count($res["res"]),'recordsFiltered' => $res['total']));
	    }
		
	}
	/**
	 * Incluir um usuario
	 */
	public function incluirAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $validaLogin = $this->model->fetchRow($this->model->select()->where('login_usuario = ?', $post['login_usuario']));
	        if($validaLogin){
	            $resposta['status'] = "erro";
	            $resposta['msg'] = "Este login esta sendo usado.";
	            echo json_encode($resposta);
	            exit();
	        }
	        if(isset($post['password_usuario'])){
	            $post['password_usuario'] = md5($post['password_usuario']);
	        }
	        unset($post['id_usuario']);
	        unset($post['nm_grupo']);
	        $result = $this->model->save($post,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	            $resposta['dados'] = $result;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	    
	    echo json_encode($resposta);
	}
	/**
	 * Salvar o avatar
	 */
	public function salvarAvatarAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    $resposta = array();
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $usuario = $this->model->fetchRow("id_usuario = ".$post['id_usuario']);
	        $post['id_avatar'] = $usuario['id_avatar'];
	        $upload = $this->view->Crop($post,'upload/perfil/');
	        
	        $form = array(
	            "arquivo" =>   $upload['url']
	        );
	        if($upload['url']){
	            $result = $this->modelSggAvatar->save($form,$this->msg);
	            if($result){
	                $form = array();
	                $form['id_avatar'] = $result['id_avatar'];
	                $form['id_usuario'] = $post['id_usuario'];
	                
	                if($form['id_avatar'] && $form['id_usuario']){
	                    $resultUsuario = $this->model->save($form,$this->msg);
	                    if($resultUsuario){
	                        if($post['id_avatar'] != null && $post['id_avatar'] != 'null'){
	                            $excluirAvatar = $this->modelSggAvatar->excluir($post['id_avatar'],$this->msg);
	                        }
	                        $resposta['status'] = "sucesso";
	                        $resposta['url'] = $upload['url'];
	                        $resposta['message'] = $this->msg;
	                        $resposta['dados'] = array('id_avatar' => $result['id_avatar']);
	                    }else{
	                        $resposta['status'] = "erro";
	                        $resposta['message'] = $this->msg;
	                    }
	                }else{
	                    $resposta['status'] = "sucesso";
	                    $resposta['url'] = $upload['url'];
	                    $resposta['message'] = $this->msg;
	                    $resposta['dados'] = array('id_avatar' => $result['id_avatar']);
	                }
	                 
	            }
	            echo json_encode($resposta);
	        }else{
	            echo json_encode(array('status' => 'erro','message' =>"Um erro inesperado aconteceu."));
	        }
	    }
	       
	    
	}
	
	/**
	 * Alterar um usuario
	 */
	public function alterarAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $form = array();
	        if(isset($post['password_usuario']))
	            $form['password_usuario'] = md5($post['password_usuario']);
	        $form['id_usuario'] = $post['id_usuario'];
	        $form['nm_usuario'] = $post['nm_usuario'];
	        $form['login_usuario'] = $post['login_usuario'];
	        $form['st_usuario'] = $post['st_usuario'];
	        $form['id_grupo'] = isset($post['id_grupo']) ? $post['id_grupo'] : '';
	       
	        $result = $this->model->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	    
	    echo json_encode($resposta);
	    
	}
	/**
	 * Lista a aba de usuário
	 */
	public function abaUsuarioAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->view->usuario = array();
	    if($this->_getParam("id_usuario")){
	        $this->view->usuario = $this->model->fetchByKey($this->_getParam("id_usuario"),$this->msg);
	    }

	}
	/**
	 * Aba de avatar
	 */
	public function abaAvatarAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->view->usuario = array();
	    if($this->_getParam("id_usuario")){
	        $this->view->usuario = $this->model->fetchByKey($this->_getParam("id_usuario"),$this->msg);
	    }
	    
	}
	/**
	 * Função para excluir um usuário
	 */
	public function excluirAction()
	{
	    
	    $resposta = array();
	    $array = false;
	    $condicao = true;
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout()->disableLayout();
	    
	    $ids = $this->getRequest()->getParam('id');
	    if($ids){
    	    if(is_array($ids)){
    	       $array = true;
    	       $ids = implode(",", $this->getRequest()->getParam('id'));
    	       $integerIDs = array_map('intval', $this->getRequest()->getParam('id')); 
    	    }
    	    if($array){
    	        if(in_array((int)$this->view->sessao->id_usuario, $integerIDs)){
    	            $condicao = false;
    	        } 
    	    }
    	    if($condicao){
    	        // deleta os avatar destes usuários
    	        $usuarios = $this->model->fetchAll("id_usuario in(".$ids.")")->toArray();
    	        foreach ($usuarios as $usuario){
    	            $resultAvatar = $this->modelSggAvatar->excluir("id_avatar in(".$usuario['id_avatar'].")");
    	        }
        	    // chama a funcao excluir
        	    $result = $this->model->excluir($ids,$this->msg);
        	     
        	    if($result){
        	        $resposta['status'] = "sucesso";
        	        $resposta['msg'] = $this->msg;
        	    }else{
        	        $resposta['status'] = "erro";
        	        $resposta['msg'] = $this->msg;
        	    }
        	     
        	    echo json_encode($resposta);
    	    }else{
    	        $resposta['status'] = "erro";
    	        $resposta['msg'] = "Erro ao Excluir, você não pode excluir seu usuário!";
    	        
    	        echo json_encode($resposta);
    	    }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Nenhum dado recebido!";
	        echo json_encode($resposta);
	    }
	}

}