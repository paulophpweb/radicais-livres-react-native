<?php

class Sistema_ScaGrupoController extends App_Controller_BaseController
{
	public $models = array('Acl','ScaUsuario');
	public $modelAtual = 'ScaGrupo';
	public $msg = null;
	
	/**
	 * Lista os dados na view
	 */
	public function indexAction()
	{
	    // verifica se tem acao para remover
	    $this->view->remover = Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->modulo.":".$this->controle, "excluir");
	    $this->view->alterar = Zend_Registry::get('acl')->isAllowed($this->view->sessao->id_grupo, $this->modulo.":".$this->controle, "alterar");
	     
	    if ($this->getRequest()->isXmlHttpRequest()) {
	        $this->_helper->layout()->disableLayout();
	        $this->_helper->viewRenderer->setNoRender(true);
	
	        $offset        		= $this->_getParam('start',0);
	        $registroPagina     = $this->_getParam('length',10);
	        $pesquisa           = $this->_getParam('search','');
	         
	        $aPesquisa = array();
	        $order = "";
	
	        // faz a pesquisa
	        if(!is_numeric($pesquisa['value'])){
	            $aPesquisa['valor'] = urldecode($pesquisa['value']);
	        }else{
	            $aPesquisa['valor'] = intval($pesquisa['value']);
	        }
	         
	        // pega os dados de ordenacao
	        if($this->_getParam("order")){
	            $ordenar = $this->_getParam("order");
	            if($ordenar[0]['column'] && $ordenar[0]['dir']){
	                $coluna = $this->_getParam("columns");
	                $order = $coluna[$ordenar[0]['column']]['data']." ".$ordenar[0]['dir'];
	            }
	            $parametro = $this->_getParam("sorting");
	        }
	        $res = $this->model->listarTodos($aPesquisa,$registroPagina,$offset,$order);
	        foreach ($res["res"] as $key => $value)
	        {
	            if($this->view->alterar)
	                $res["res"][$key]['alterar'] = '<a class="btn btn-info btn-sm" href="'.($this->view->url(array("module" => "sistema","controller"=>$this->controle, "action"=>"form","id" => $res["res"][$key]['id_grupo']),null,true)).'">Editar</a>';
	            $res["res"][$key]['remover'] = $this->view->sessao->id_grupo == $res["res"][$key]["id_grupo"] || $res["res"][$key]["is_root"] == 1 || !$this->view->remover ? '' : '<a class="btn btn-danger btn-sm" onclick="DeletarIndex('.($res["res"][$key]['id_grupo']).')" href="javascript:;">Remover</a>';
	        }
	
	        echo json_encode(array("data" => $res["res"],'draw' => $this->_getParam("draw",'1'),'recordsTotal' => count($res["res"]),'recordsFiltered' => $res['total']));
	    }
	
	}

	
	/**
	 * Incluir um grupo
	 */
	public function incluirAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $form = array(
	            'id_grupo' => isset($post['id_grupo']) ? $post['id_grupo'] : '',
	            'nm_grupo' => $post['nm_grupo'],
	            'dh_cadastro' => date("Y-m-d H:i:s"),
	            'id_usuario_cadastro' => $this->view->sessao->id_usuario,
	            'is_root' => $post['is_root']
	        );
	        $result = $this->model->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	            $resposta['dados'] = $result;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	    
	    echo json_encode($resposta);
	}
	
	/**
	 * Alterar um grupo
	 */
	public function alterarAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $form = array();
	        $form['id_grupo'] = $post['id_grupo'];
	        $form['nm_grupo'] = $post['nm_grupo'];
	        $form['is_root'] = $post['is_root'];
	       
	        $result = $this->model->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	    
	    echo json_encode($resposta);
	    
	}
	/**
	 * Pega o grupo por id
	 */
	/*public function getGrupoAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    $id = $this->_getParam("id");
	    
	    $res = $this->model->fetchByKey($id,$this->msg);

	    echo json_encode(array("msg"=>$this->msg,"status" => "sucesso","dados" => $res));
	}*/
	
	/**
	 * Lista a aba de usuário
	 */
	public function abaGrupoAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->view->grupo = array();
	    if($this->_getParam("id_grupo")){
	        $this->view->grupo = $this->model->fetchByKey($this->_getParam("id_grupo"),$this->msg);
	    }

	}
	/**
	 * Lista a aba de controle de acesso
	 */
	public function abaAclAction()
	{
	    $this->_helper->layout()->disableLayout();
	
	}
	/**
	 * remove um grupo ou mais
	 */
	public function excluirAction()
	{
	    
	    $resposta = array();
	    $array = false;
	    $condicao = true;
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout()->disableLayout();
	    
	    $ids = $this->getRequest()->getParam('id');
	    if($ids){
    	    if(is_array($ids)){
    	       $array = true;
    	       $ids = implode(",", $this->getRequest()->getParam('id'));
    	       $integerIDs = array_map('intval', $this->getRequest()->getParam('id')); 
    	    }
    	    if($array){
    	        if(in_array((int)$this->view->sessao->id_grupo, $integerIDs)){
    	            $condicao = false;
    	        } 
    	    }
    	    if($condicao){
    	        // verifica se tem usuario neste grupo
    	        $usuarios = $this->modelScaUsuario->fetchAll("id_grupo in(".$ids.")")->toArray();
    	        if($usuarios){
    	            $resposta['status'] = "erro";
    	            $resposta['msg'] = "Não foi possível excluir, existem usuários relacionados a este grupo.";
    	            echo json_encode($resposta);
    	            exit();
    	        }
        	    // chama a funcao excluir
        	    $result = $this->model->removeCustom("id_grupo in(".$ids.")",$ids,$this->msg);
        	     
        	    if($result){
        	        $resposta['status'] = "sucesso";
        	        $resposta['msg'] = $this->msg;
        	    }else{
        	        $resposta['status'] = "erro";
        	        $resposta['msg'] = $this->msg;
        	    }
        	     
        	    echo json_encode($resposta);
    	    }else{
    	        $resposta['status'] = "error";
    	        $resposta['msg'] = "Erro ao Excluir, você não pode excluir seu usuário!";
    	        
    	        echo json_encode($resposta);
    	    }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Nenhum dado recebido!";
	        echo json_encode($resposta);
	    }
	}


}