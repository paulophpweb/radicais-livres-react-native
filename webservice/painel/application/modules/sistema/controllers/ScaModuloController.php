<?php

class Sistema_ScaModuloController extends App_Controller_BaseController
{
	public $models = array("ScaMenu");
	public $modelAtual = 'ScaModulo';
	public $msg = null;

	
	/**
	 * Incluir um grupo
	 */
	public function incluirAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $form = array(
	            'id' => isset($post['id']) ? $post['id'] : '',
	            'nome' => $post['nome'],
	            'icone' => $post['icone'],
	            'ordem' => $post['ordem'],
	            'status' => $post['status']
	        );
	        $result = $this->model->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	            $resposta['dados'] = $result;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	    
	    echo json_encode($resposta);
	}
	
	/**
	 * Incluir um menu
	 */
	public function incluirMenuAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $form = array(
	            'id' => isset($post['id']) ? $post['id'] : '',
	            'nome' => $post['nome'],
	            'modulo_id' => $post['modulo_id'],
	            'nm_modulo' => "sistema",
	            'nm_controller' => $post['nm_controller'],
	            'nm_action' => $post['nm_action'],
	            'ordem' => $post['ordem'],
	            'status' => $post['status']
	        );
	        $result = $this->modelScaMenu->save($form,$this->msg);
	        if($result){
	            $this->view->menu = array();
        	    if($post['modulo_id']){
        	        $this->view->menu = $this->modelScaMenu->fetchByModulo($post['modulo_id'],$this->msg);
        	        // retorna o partial
        	        echo $this->view->partial("partial/modulo-menu.phtml",array('menu' => $this->view->menu,"modulo" => $this->modulo,"controle" => $this->controle,"sessao" => $this->view->sessao));
	            }
	        }
	    }
	}
	
	/**
	 * Alterar um grupo
	 */
	public function alterarAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $form = array();
	        $form['id'] = $post['id'];
	        $form['nome'] = $post['nome'];
	        $form['icone'] = $post['icone']; 
	        $form['ordem'] = $post['ordem']; 
	        $form['status'] = $post['status']; 
	       
	        $result = $this->model->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	    
	    echo json_encode($resposta);
	    
	}
	/**
	 * Alterar um menu
	 */
	public function alterarMenuAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $form = array(
	            'id' => isset($post['id']) ? $post['id'] : '',
	            'nome' => $post['nome'],
	            'modulo_id' => $post['modulo_id'],
	            'nm_modulo' => "sistema",
	            'nm_controller' => $post['nm_controller'],
	            'nm_action' => $post['nm_action'],
	            'ordem' => $post['ordem'],
	            'status' => $post['status']
	        );
	       
	        $result = $this->modelScaMenu->save($form,$this->msg);
	        if($result){
    	        $this->view->menu = array();
    	        if($this->getRequest()->getParam('modulo_id')){
    	            $this->view->menu = $this->modelScaMenu->fetchByModulo($this->getRequest()->getParam('modulo_id'),$this->msg);
    	            // retorna o partial
    	            echo $this->view->partial("partial/modulo-menu.phtml",array('menu' => $this->view->menu,"modulo" => $this->modulo,"controle" => $this->controle,"sessao" => $this->view->sessao));
    	        }
    	    }
	    }
	    
	}
	/**
	 * Pega o grupo por id
	 */
	/*public function getGrupoAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    $id = $this->_getParam("id");
	    
	    $res = $this->model->fetchByKey($id,$this->msg);

	    echo json_encode(array("msg"=>$this->msg,"status" => "sucesso","dados" => $res));
	}*/
	
	/**
	 * Lista a aba de usuário
	 */
	public function abaModuloAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->view->modulo = array();
	    if($this->_getParam("id")){
	        $this->view->modulo = $this->model->fetchByKey($this->_getParam("id"),$this->msg);
	    }

	}
	/**
	 * Lista a aba de menu
	 */
	public function abaMenuAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->view->menu = array();
	    if($this->_getParam("modulo_id")){
	        $this->view->menu = $this->modelScaMenu->fetchByModulo($this->_getParam("modulo_id"),$this->msg);
	    }
	
	}
	/**
	 * remove um grupo ou mais
	 */
	public function excluirAction()
	{
	    
	    $resposta = array();
	    $array = false;
	    $condicao = true;
	    $this->_helper->viewRenderer->setNoRender(true);
	    $this->_helper->layout()->disableLayout();
	    
	    $ids = $this->getRequest()->getParam('id');
	    if($ids){
    	    if(is_array($ids)){
    	       $array = true;
    	       $ids = implode(",", $this->getRequest()->getParam('id'));
    	       $integerIDs = array_map('intval', $this->getRequest()->getParam('id')); 
    	    }
    	    if($condicao){
    	        // verifica se tem usuario neste grupo
    	        $menus = $this->modelScaMenu->fetchAll("modulo_id in(".$ids.")")->toArray();
    	        if($menus){
    	            $resposta['status'] = "erro";
    	            $resposta['msg'] = "Não foi possível excluir, existem menus relacionados a este módulo.";
    	            echo json_encode($resposta);
    	            exit();
    	        }
        	    // chama a funcao excluir
        	    $result = $this->model->removeCustom("id in(".$ids.")",$ids,$this->msg);
        	     
        	    if($result){
        	        $resposta['status'] = "sucesso";
        	        $resposta['msg'] = $this->msg;
        	    }else{
        	        $resposta['status'] = "erro";
        	        $resposta['msg'] = $this->msg;
        	    }
        	     
        	    echo json_encode($resposta);
    	    }else{
    	        $resposta['status'] = "error";
    	        $resposta['msg'] = "Erro ao Excluir, você não pode excluir seu usuário!";
    	        
    	        echo json_encode($resposta);
    	    }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Nenhum dado recebido!";
	        echo json_encode($resposta);
	    }
	}
	/**
	 * remove um menu ou mais
	 */
	public function excluirMenuAction()
	{
	    
	    $resposta = array();
	    $array = false;
	    $condicao = true;
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    
	    $ids = $this->getRequest()->getParam('id');
	    if($ids){
    	    if(is_array($ids)){
    	       $array = true;
    	       $ids = implode(",", $this->getRequest()->getParam('id'));
    	       $integerIDs = array_map('intval', $this->getRequest()->getParam('id')); 
    	    }
    	    if($condicao){
        	    // chama a funcao excluir
        	    $result = $this->modelScaMenu->excluir($ids,$this->msg);
        	    
        	    if($result){
        	        $this->view->menu = array();
        	        if($this->getRequest()->getParam('modulo_id')){
        	            $this->view->menu = $this->modelScaMenu->fetchByModulo($this->getRequest()->getParam('modulo_id'),$this->msg);
        	            // retorna o partial
        	            echo $this->view->partial("partial/modulo-menu.phtml",array('menu' => $this->view->menu,"modulo" => $this->modulo,"controle" => $this->controle,"sessao" => $this->view->sessao));
        	        }
        	    }
    	    }
	    }
	}


}