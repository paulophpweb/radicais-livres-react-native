<?php

class Sistema_SgpSenhaController extends App_Controller_BaseController
{
	public $models = array();
	public $modelAtual = 'SgpSenha';
	public $msg = null;

	
	/**
	 * Incluir um grupo
	 */
	public function incluirAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    $encrypt = new App_Custom_Encrypt();
	    
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $form = array(
	            'id_senha' => isset($post['id_senha']) ? $post['id_senha'] : '',
	            'id_categoria' => isset($post['id_categoria']) ? $post['id_categoria'] : '',
	            'nm_senha' => $post['nm_senha'],
	            'nm_login' => $post['nm_login'],
	            'senha' => $encrypt->encode($post['senha']),
	            'login_url' => $post['login_url'],
	            'obs' => $post['obs'],
	            'dh_cadastro' => date("Y-m-d H:i:s"),
	            'id_usuario_cadastro' => $this->view->sessao->id_usuario,
	            'st_senha' => $post['st_senha']
	        );
	        $result = $this->model->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	            $resposta['dados'] = $result;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	    
	    echo json_encode($resposta);
	}
	
	/**
	 * Alterar um grupo
	 */
	public function alterarAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    $encrypt = new App_Custom_Encrypt();
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	         $form = array(
	            'id_senha' => isset($post['id_senha']) ? $post['id_senha'] : '',
	            'id_categoria' => isset($post['id_categoria']) ? $post['id_categoria'] : '',
	            'nm_senha' => $post['nm_senha'],
	            'nm_login' => $post['nm_login'],
	            'senha' => $encrypt->encode($post['senha']),
	            'login_url' => $post['login_url'],
	            'obs' => $post['obs'],
	            'dh_cadastro' => date("Y-m-d H:i:s"),
	            'id_usuario_cadastro' => $this->view->sessao->id_usuario,
	            'st_senha' => $post['st_senha']
	        );
	       
	        $result = $this->model->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	    
	    echo json_encode($resposta);
	    
	}
	
	/**
	 * Lista a aba de usuário
	 */
	public function abaSenhaAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->view->senha = array();
	    $encrypt = new App_Custom_Encrypt();
	    if($this->_getParam("id_senha")){
	        $this->view->senha = $this->model->fetchByKey($this->_getParam("id_senha"),$this->msg);
	        $this->view->senha['senha'] = $encrypt->decode($this->view->senha['senha']);
	    }

	}


}