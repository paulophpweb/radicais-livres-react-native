<?php

class Sistema_SgpCategoriaController extends App_Controller_BaseController
{
	public $models = array();
	public $modelAtual = 'SgpCategoria';
	public $msg = null;

	
	/**
	 * Incluir um grupo
	 */
	public function incluirAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $form = array(
	            'id_categoria' => isset($post['id_categoria']) ? $post['id_categoria'] : '',
	            'nm_categoria' => $post['nm_categoria'],
	            'dh_cadastro' => date("Y-m-d H:i:s"),
	            'id_usuario_cadastro' => $this->view->sessao->id_usuario,
	            'st_categoria' => $post['st_categoria']
	        );
	        $result = $this->model->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	            $resposta['dados'] = $result;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	    
	    echo json_encode($resposta);
	}
	
	/**
	 * Alterar um grupo
	 */
	public function alterarAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	         $form = array(
	            'id_categoria' => isset($post['id_categoria']) ? $post['id_categoria'] : '',
	            'nm_categoria' => $post['nm_categoria'],
	            'dh_cadastro' => date("Y-m-d H:i:s"),
	            'id_usuario_cadastro' => $this->view->sessao->id_usuario,
	            'st_categoria' => $post['st_categoria']
	        );
	       
	        $result = $this->model->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	    
	    echo json_encode($resposta);
	    
	}
	
	/**
	 * Lista a aba de usuário
	 */
	public function abaCategoriaAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->view->categoria = array();
	    if($this->_getParam("id_categoria")){
	        $this->view->categoria = $this->model->fetchByKey($this->_getParam("id_categoria"),$this->msg);
	    }

	}


}