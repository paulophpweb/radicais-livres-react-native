<?php

class Sistema_SgsBannerController extends App_Controller_BaseController
{
	public $models = array();
	public $modelAtual = 'SgsBanner';
	public $msg = null;

	
	/**
	 * Incluir um grupo
	 */
	public function incluirAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	        $form = array(
	            'id_banner' => isset($post['id_banner']) ? $post['id_banner'] : '',
	            'nm_banner' => $post['nm_banner'],
	            'dh_cadastro' => date("Y-m-d H:i:s"),
	            'id_usuario_cadastro' => $this->view->sessao->id_usuario
	        );
	        $result = $this->model->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	            $resposta['dados'] = $result;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	    
	    echo json_encode($resposta);
	}
	
	/**
	 * Alterar um grupo
	 */
	public function alterarAction()
	{
	    $resposta = array();
	    $this->_helper->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	    if ($this->getRequest()->isPost()){
	        $post = $this->getRequest()->getPost();
	         $form = array(
	            'id_banner' => isset($post['id_banner']) ? $post['id_banner'] : '',
	            'nm_banner' => $post['nm_banner'],
	            'dh_cadastro' => date("Y-m-d H:i:s"),
	            'id_usuario_cadastro' => $this->view->sessao->id_usuario
	        );
	       
	        $result = $this->model->save($form,$this->msg);
	        if($result){
	            $resposta['status'] = "sucesso";
	            $resposta['msg'] = $this->msg;
	        }else{
	            $resposta['status'] = "erro";
	            $resposta['msg'] = $this->msg;
	        }
	    }else{
	        $resposta['status'] = "erro";
	        $resposta['msg'] = "Um erro inesperado aconteceu.";
	    }
	    
	    echo json_encode($resposta);
	    
	}
	
	/**
	 * Lista a aba de usuário
	 */
	public function abaBannerAction()
	{
	    $this->_helper->layout()->disableLayout();
	    $this->view->banner = array();
	    if($this->_getParam("id_banner")){
	        $this->view->banner = $this->model->fetchByKey($this->_getParam("id_banner"),$this->msg);
	    }
	
	}


}