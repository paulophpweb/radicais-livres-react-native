<?php

class Sistema_Model_SggAvatar extends App_Model_Default
{
    
    protected $_name = 'sgg_avatar';
    public $id = null;
    protected $primarykey = "id_avatar";
	
    /**
     * 
     * @param unknown $arraySearch
     * @param string $limit
     * @param string $offset
     * @param string $order
     * @param string $msg
     * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     */
	public function listarTodos($arraySearch = array(), $limit = null, $offset = null, $order = 'u.id_usuario', &$msg = '')
	{
		// SQL para buscar os registros
		$sql = $this->getAdapter()->select()
		->from(array('a' => $this->_name));
		if (isset($arraySearch['valor']) && $arraySearch['valor'] && is_int($arraySearch['valor'])) {
		    $sql->where('a.id_avatar = ?', $arraySearch['valor']);
		}
		
		// SQL para buscar a quantidade de páginas existentes
		$sqlCount = $this->getAdapter()->query($sql->__toString());

		$sql->order($order)->limit($limit, $offset);
		$stmt = $sql->query();
 	
		$return['res']   = $stmt->fetchAll();
		$return['total'] = $sqlCount->rowCount();
	
		return $return;
	}
	
	/**
	 * 
	 * @param int $id
	 * @param string $msg
	 * @return mixed
	 */
	public function fetchByKey($id, &$msg = null)
	{
	    $sql = $this->getAdapter()->select()
		->from(array('a' => $this->_name));
		$sql->where('a.id_avatar = ?', $id);
		
		$res = $this->getAdapter()->fetchRow($sql);
	
	    if (!$res) {
	        $msg = $this->msg['select']['not-found'];
	    }
		
	    return $res;
	}
	
	/**
	 * Remove algum registro do banco de dados
	 *
	 * @param string $condition
	 * @param string $msg
	 * @return boolean
	 */
	public function excluir($ids, &$msg = null)
	{
	    try {
	        $sql = $this->getAdapter()->select()
		    ->from($this->_name);
		    $sql->where($this->primarykey." in(".$ids.")");
	        $res = $this->getAdapter()->fetchRow($sql);
	        unlink(APPLICATION_PATH."/../public_html".$res['arquivo']);
	        $this->getAdapter()->delete($this->_name,$this->primarykey." in(".$ids.")");
	        $msg = $this->msg['delete']['success'];
	        return true;
	    } catch (Zend_Exception $e) {
	        $msg = $e->getMessage() . "\n";
	        return false;
	    }
	}
}