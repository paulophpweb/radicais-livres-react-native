<?php

class Sistema_Model_Acl extends App_Model_Default
{
	
	protected $_name = 'acl';
    public $id = null;
    protected $primarykey = "id";
	
	private function _dataExistsCtrlAction(array $data)
    {
        $module = isset($data['module']) ? $data['module'] : "";
    	$controller = isset($data['controller']) ? $data['controller'] : "";
		$action = isset($data['action']) ? $data['action'] : "";
        $sql = $this->getAdapter()->select()->from(array('a' => 'acl'), 'count(id) as qtd')->where("a.controller = '".$controller."' and a.action = '".$action."' and a.module = '".$module."'");
        $res = $this->getAdapter()->fetchRow($sql);
        if (isset($res['qtd']) && $res['qtd'] > 0) {
            return true;
        } else {
            return false;
        }
    }
    
   public static function resourceValid($request){
        // Verifica se o controller e valido
        $dispatcher = Zend_Controller_Front::getInstance()->getDispatcher();
        if (!$dispatcher->isDispatchable($request)) {
            return false;
        }
        // Verifica se a action e valida
        $front      = Zend_Controller_Front::getInstance();
        $dispatcher = $front->getDispatcher();
        $controllerClass = $dispatcher->getControllerClass($request);
        $controllerclassName = $dispatcher->loadClass($controllerClass);
        $actionName = $dispatcher->getActionMethod($request);
        $controllerObject = new ReflectionClass($controllerclassName);      
        if(!$controllerObject->hasMethod($actionName)){
            return false;   
        }       
        return true;
   }
   
   public function getAllResources(){
        $sql = $this->getAdapter()->select(array("controller","module"))->from("acl")->group("controller");
		$res = $this->getAdapter()->fetchAll($sql);
		if($res){
    		foreach ($res as $key => $value){
    		    if($value['controller'] == 'index' || $value['controller'] == 'error'){
    		         unset($res[$key]);
    		    }
    		}
        }
        return $res;
    }
    
    public function getActionByController($controle){
        $sql = $this->getAdapter()->select("controller")->from("acl")->where("controller = ? ",$controle);
        $res = $this->getAdapter()->fetchAll($sql);
        return $res;
    }
    
    public function getActionByControllerMy($controle,$id_grupo){
        $sql = $this->getAdapter()->select()
        ->from(array('a' => 'acl'))
        ->joinLeft( array('atg' => 'acl_to_grupo'), 'a.id = atg.acl_id', array('grupo_id') )
        ->where("a.controller = '".$controle."' AND atg.grupo_id = ".$id_grupo." ");
        $res = $this->getAdapter()->fetchAll($sql);
        return $res;
    }
   
   public function resourceExists($module=null,$controller=null,$action=null){
        if(!$controller || !$action || !$module) throw new Exception("Error resourceExists(), the controller/action/module não existe");
        $result=$this->_dataExistsCtrlAction(array("controller" => $controller,"action" => $action, "module" => $module));
        if($result){
            return true;
        }
        return false;
    }
   
   public function createResource($module=null,$controller=null,$action=null){
        if(!$controller || !$action | !$module) throw new Exception("Error resourceExists(), the controller/action/module não existe");
        $data=array('controller'=>$controller,'action'=>$action,'module'=>$module);
        return $this->save($data);
    }
   
    public function getCurrentRoleAllowedResources($grupo_id=null){
        if(!$grupo_id) throw new Exception("Error getCurrentUserPermissions(), grupo id e vazio");
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql='SELECT A.controller,A.action,A.module  FROM acl_to_grupo ATR INNER JOIN acl A ON A.id=ATR.acl_id WHERE grupo_id=? ORDER BY A.controller';
        $stmt = $db->query($sql,$grupo_id);
        $out= $stmt->fetchAll();
        $controller='';
        $module='';
        $resources=array();
        foreach ($out as $value){
            if($value['controller']!=$controller){
                $controller=$value['controller'];
            }
            if($value['module']!=$module){
                $module=$value['module'];
            }
            $resources[$module.":".$controller][]=$value['action'];
        }
        return $resources;
    }
    
    /**
     * Remove algum registro do banco de dados
     *
     * @param string $condition
     * @param string $msg
     * @return boolean
     */
    public function removeAclToGrupo($condition, &$msg = null)
    {
        try {
            $sql = "DELETE atg FROM acl as a LEFT JOIN acl_to_grupo as atg ON a.id=atg.acl_id WHERE ".$condition." ;";
            $this->getAdapter()->query($sql);
            $msg = $this->msg['delete']['success'];
            return true;
        } catch (Zend_Exception $e) {
            $msg = $e->getMessage() . "\n";
            return false;
        }
    }
    
    /**
     * Salva um registro no banco de dados
     *
     * @param array $data
     * @param string $msg
     * @return mixed
     */
    public function salvarAclToGrupo(array $data, &$msg = null)
    {
         
        try{
            $retorno = $this->getAdapter()->insert('acl_to_grupo', $data);
            if($retorno){
    	           $msg = $this->msg['insert']['success'];
    	           return true;
            }else{
    	           $msg = $this->msg['insert']['error'];
    	           return false;
            }
                 
        }catch (Zend_Exception $e) {
            $msg = $e->getMessage() . "\n";
            return false;
        }
    }
}