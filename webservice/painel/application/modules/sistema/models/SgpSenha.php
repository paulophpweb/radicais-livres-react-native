<?php

class Sistema_Model_SgpSenha extends App_Model_Default
{
    
    protected $_name = 'sgp_senha';
    public $id = null;
    public $primarykey = "id_senha";
	
    /**
     * 
     * @param unknown $arraySearch
     * @param string $limit
     * @param string $offset
     * @param string $order
     * @param string $msg
     * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     */
	public function listarTodos($arraySearch = array(), $limit = null, $offset = null, $order = 's.id_senha', &$msg = '')
	{
		// SQL para buscar os registros
		$sql = $this->getAdapter()->select()
		->from(array('s' => $this->_name))
		->joinLeft( array('c' => 'sgp_categoria'), 's.id_categoria = c.id_categoria', array('nm_categoria') );
		if (isset($arraySearch['valor']) && $arraySearch['valor'] && is_int($arraySearch['valor'])) {
		    $sql->where('s.id_senha = ?', $arraySearch['valor']);
		}
	
		if (isset($arraySearch['valor']) && $arraySearch['valor'] && !is_int($arraySearch['valor'])) {
			$sql->where('s.nm_senha LIKE ?', "%{$arraySearch['valor']}%");
		}
		
		// SQL para buscar a quantidade de páginas existentes
		$sqlCount = $this->getAdapter()->query($sql->__toString());

		$sql->order($order)->limit($limit, $offset);
		$stmt = $sql->query();
 	
		$return['res']   = $stmt->fetchAll();
		$return['total'] = $sqlCount->rowCount();
	
		return $return;
	}
	
	/**
	 *
	 * @param int $id
	 * @param string $msg
	 * @return mixed
	 */
	public function fetchByKey($id, &$msg = null)
	{
	    try {
	        if($id){
	            $sql = 'SELECT '.$this->_name.'.*, sgp_categoria.nm_categoria FROM '.$this->_name.' LEFT JOIN sgp_categoria ON '.$this->_name.'.id_categoria = sgp_categoria.id_categoria  WHERE '.$this->primarykey.' = ?';

	            $res = $this->getAdapter()->fetchRow($sql,$id);
	             
	            if (!$res) {
	                $msg = $this->msg['select']['not-found'];
	            }
	             
	            return $res;
	        }else{
	            $msg = "O Id não foi recebido";
	            return false;
	        }
	    }catch (Zend_Exception $e) {
	        $msg = $e->getMessage() . "\n";
	        return false;
	    }
	}
}