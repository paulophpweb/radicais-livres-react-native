<?php

class Sistema_Model_ScaMenu extends App_Model_Default
{
    
    protected $_name = 'menu';
    public $id = null;
    protected $primarykey = "id";
	
    /**
     * 
     * @param unknown $arraySearch
     * @param string $limit
     * @param string $offset
     * @param string $order
     * @param string $msg
     * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     */
	public function listarTodos($arraySearch = array(), $limit = null, $offset = null, $order = 'm.id', &$msg = '')
	{
		// SQL para buscar os registros
		$sql = $this->getAdapter()->select()
		->from(array('m' => $this->_name));
		if (isset($arraySearch['valor']) && $arraySearch['valor'] && is_int($arraySearch['valor'])) {
		    $sql->where('m.id = ?', $arraySearch['valor']);
		}
		
		if (isset($arraySearch['modulo_id']) && $arraySearch['modulo_id']) {
		    $sql->where('m.modulo_id = ?', $arraySearch['modulo_id']);
		}
	
		if (isset($arraySearch['valor']) && $arraySearch['valor'] && !is_int($arraySearch['valor'])) {
			$sql->where('m.nome LIKE ?', "%{$arraySearch['valor']}%");
		}
		
		// SQL para buscar a quantidade de páginas existentes
		$sqlCount = $this->getAdapter()->query($sql->__toString());

		$sql->order($order)->limit($limit, $offset);
		$stmt = $sql->query();
 	
		$return['res']   = $stmt->fetchAll();
		$return['total'] = $sqlCount->rowCount();
	
		return $return;
	}
	
	/**
	 * 
	 * @param int $id
	 * @param string $msg
	 * @return mixed
	 */
	public function fetchByKey($id, &$msg = null)
	{
	    $sql = $this->getAdapter()->select()
		->from(array('m' => $this->_name));
		$sql->where('m.id = ?', $id);
		
		$res = $this->getAdapter()->fetchRow($sql);
	
	    if (!$res) {
	        $msg = $this->msg['select']['not-found'];
	    }
		
	    return $res;
	}
	
	/**
	 *
	 * @param int $id_modulo
	 * @param string $msg
	 * @return mixed
	 */
	public function fetchByModulo($id_modulo, &$msg = null)
	{
	    $sql = $this->getAdapter()->select()
	    ->from(array('m' => $this->_name));
	    $sql->where('m.modulo_id = ?', $id_modulo);
	    
	    $return['res']   = $this->getAdapter()->fetchAll($sql);
	
	    if (!$return) {
	        $msg = $this->msg['select']['not-found'];
	    }
	
	    return $return;
	}
}