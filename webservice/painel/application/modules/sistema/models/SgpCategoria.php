<?php

class Sistema_Model_SgpCategoria extends App_Model_Default
{
    
    protected $_name = 'sgp_categoria';
    public $id = null;
    public $primarykey = "id_categoria";
	
    /**
     * 
     * @param unknown $arraySearch
     * @param string $limit
     * @param string $offset
     * @param string $order
     * @param string $msg
     * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     */
	public function listarTodos($arraySearch = array(), $limit = null, $offset = null, $order = 'c.id_categoria', &$msg = '')
	{
		// SQL para buscar os registros
		$sql = $this->getAdapter()->select()
		->from(array('c' => $this->_name));
		if (isset($arraySearch['valor']) && $arraySearch['valor'] && is_int($arraySearch['valor'])) {
		    $sql->where('c.id_categoria = ?', $arraySearch['valor']);
		}
	
		if (isset($arraySearch['valor']) && $arraySearch['valor'] && !is_int($arraySearch['valor'])) {
			$sql->where('c.nm_categoria LIKE ?', "%{$arraySearch['valor']}%");
		}
		
		// SQL para buscar a quantidade de páginas existentes
		$sqlCount = $this->getAdapter()->query($sql->__toString());

		$sql->order($order)->limit($limit, $offset);
		$stmt = $sql->query();
 	
		$return['res']   = $stmt->fetchAll();
		$return['total'] = $sqlCount->rowCount();
	
		return $return;
	}
}