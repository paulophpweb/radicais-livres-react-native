<?php
/**
 * 
 * @author Paulo Henrique
 * @see http://www.pauloph.com.br
 *
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	
	public function _initAutoload()
	{
	    $modules = array(
	            'Site',
		        'Painel',
	            'Webservice',
	            'Sistema'
	    );
	
	    foreach ($modules as $module) {
	        $autoloader = new Zend_Application_Module_Autoloader(array(
	                'namespace' => ucfirst($module),
	                'basePath'  => APPLICATION_PATH . '/modules/' . strtolower($module),
	        ));
	    }
	
	    return $autoloader;
	}
	/**
	 * Coloca o arquivo html minificado
	 */
	public function _initMinifyHTML()
	{
        // coloca o html em uma unica linha
       /* $this->bootstrap('view'); 
        $view = $this->getResource('view');
	    $view->addFilterPath('App/views/filter', 'App_views_filter_')
	    ->addFilter('MinifyView');*/
	}
	
    
    protected function _initPlugins()
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        $bootstrap = $this->getApplication();
        if ($bootstrap instanceof Zend_Application) {
            $bootstrap = $this;
        }
        $front = $bootstrap->getResource('FrontController');
        $bootstrap->bootstrap('FrontController');
        $front = $bootstrap->getResource('FrontController');
        
        
        
    }
    
    
}
