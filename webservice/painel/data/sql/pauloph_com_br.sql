-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 11-Jan-2017 às 12:19
-- Versão do servidor: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pauloph.com.br`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acl`
--

CREATE TABLE `acl` (
  `id` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `controller` varchar(100) NOT NULL,
  `action` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `acl`
--

INSERT INTO `acl` (`id`, `module`, `controller`, `action`) VALUES
(92, 'sistema', 'error', 'error'),
(93, 'sistema', 'sca-usuario', 'index'),
(94, 'sistema', 'sca-usuario', 'aba-usuario'),
(95, 'sistema', 'sca-grupo', 'index'),
(96, 'sistema', 'sca-usuario', 'aba-avatar'),
(97, 'sistema', 'sca-usuario', 'salvar-avatar'),
(98, 'sistema', 'sca-usuario', 'excluir'),
(99, 'sistema', 'sca-usuario', 'alterar'),
(100, 'sistema', 'sca-grupo', 'aba-grupo'),
(101, 'sistema', 'sca-grupo', 'aba-acl'),
(102, 'sistema', 'sca-usuario', 'incluir'),
(103, 'sistema', 'sca-grupo', 'excluir'),
(104, 'sistema', 'sca-grupo', 'alterar'),
(105, 'sistema', 'sca-grupo', 'incluir'),
(106, 'sistema', 'sca-modulo', 'index'),
(107, 'sistema', 'sca-modulo', 'aba-modulo'),
(108, 'sistema', 'sca-modulo', 'incluir'),
(109, 'sistema', 'sca-modulo', 'aba-menu'),
(110, 'sistema', 'sgp-categoria', 'index'),
(111, 'sistema', 'sca-modulo', 'incluir-menu'),
(112, 'sistema', 'sgp-senha', 'index'),
(113, 'sistema', 'sgp-categoria', 'aba-categoria'),
(114, 'sistema', 'sgp-categoria', 'incluir'),
(115, 'sistema', 'sgp-senha', 'aba-senha'),
(116, 'sistema', 'sgp-senha', 'incluir');

-- --------------------------------------------------------

--
-- Estrutura da tabela `acl_to_grupo`
--

CREATE TABLE `acl_to_grupo` (
  `id` int(11) NOT NULL,
  `acl_id` int(11) NOT NULL,
  `grupo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `acl_to_grupo`
--

INSERT INTO `acl_to_grupo` (`id`, `acl_id`, `grupo_id`) VALUES
(29, 93, 10),
(30, 94, 10),
(31, 96, 10),
(32, 97, 10),
(33, 98, 10),
(34, 99, 10),
(35, 102, 10),
(59, 95, 10),
(60, 100, 10),
(61, 101, 10),
(62, 103, 10),
(63, 104, 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `modulo_id` int(11) NOT NULL,
  `nm_modulo` varchar(255) NOT NULL,
  `nm_controller` varchar(255) NOT NULL,
  `nm_action` varchar(255) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `menu`
--

INSERT INTO `menu` (`id`, `modulo_id`, `nm_modulo`, `nm_controller`, `nm_action`, `nome`, `ordem`, `status`) VALUES
(1, 1, 'sistema', 'sca-usuario', 'index', 'Usuários', 1, 1),
(2, 1, 'sistema', 'sca-grupo', 'index', 'Grupo', 1, 1),
(3, 1, 'sistema', 'sca-modulo', 'index', 'Módulo', 1, 1),
(4, 5, 'sistema', 'sca-formulario', 'index', 'Contato', 2, 1),
(5, 6, 'sistema', 'sgp-categoria', 'index', 'Categoria', 1, 1),
(6, 6, 'sistema', 'sgp-senha', 'index', 'Senhas', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo`
--

CREATE TABLE `modulo` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `icone` varchar(20) NOT NULL COMMENT 'class do icone fonte awsome',
  `ordem` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `modulo`
--

INSERT INTO `modulo` (`id`, `nome`, `icone`, `ordem`, `status`) VALUES
(1, 'Controle de acesso', 'fa-users', 1, 1),
(4, 'Controle Geral', 'fa-wrench', 1, 1),
(5, 'Gerenciar site', 'fa-folder-open', 2, 1),
(6, 'Gerenciar Senha', 'fa-key', 3, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sca_grupo`
--

CREATE TABLE `sca_grupo` (
  `id_grupo` int(11) NOT NULL,
  `nm_grupo` varchar(255) NOT NULL,
  `dh_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_usuario_cadastro` int(11) NOT NULL,
  `is_root` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `sca_grupo`
--

INSERT INTO `sca_grupo` (`id_grupo`, `nm_grupo`, `dh_cadastro`, `id_usuario_cadastro`, `is_root`) VALUES
(3, 'Admin', '2016-02-24 23:05:01', 42, 1),
(10, 'Usuário comum', '2016-02-15 12:40:20', 42, 0),
(11, 'kfdsjlk fjdsa', '2016-03-16 12:54:51', 42, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sca_usuario`
--

CREATE TABLE `sca_usuario` (
  `id_usuario` int(11) NOT NULL,
  `nm_usuario` varchar(255) NOT NULL DEFAULT '',
  `password_usuario` varchar(255) NOT NULL DEFAULT '',
  `login_usuario` varchar(255) NOT NULL DEFAULT '',
  `id_avatar` int(11) DEFAULT NULL,
  `st_usuario` tinyint(1) NOT NULL DEFAULT '0',
  `id_grupo` int(11) DEFAULT NULL,
  `ultimo_acesso_usuario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `sca_usuario`
--

INSERT INTO `sca_usuario` (`id_usuario`, `nm_usuario`, `password_usuario`, `login_usuario`, `id_avatar`, `st_usuario`, `id_grupo`, `ultimo_acesso_usuario`) VALUES
(42, 'admin', '202cb962ac59075b964b07152d234b70', 'admin', 121, 1, 3, '2017-01-11 11:17:18'),
(43, 'Paulo 2', 'd41d8cd98f00b204e9800998ecf8427e', 'admin2', 116, 1, 3, '2016-03-16 13:15:26'),
(46, 'Paulo', 'd41d8cd98f00b204e9800998ecf8427e', 'adminnnn', NULL, 1, 10, '2016-03-16 22:02:31');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sgg_avatar`
--

CREATE TABLE `sgg_avatar` (
  `id_avatar` int(11) NOT NULL,
  `arquivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `sgg_avatar`
--

INSERT INTO `sgg_avatar` (`id_avatar`, `arquivo`) VALUES
(111, '/upload/perfil/56ce3d3aed6841497678916.png'),
(115, '/upload/perfil/56cef13b600c21784956964.png'),
(116, '/upload/perfil/56d040b79c97a401259045.png'),
(118, '/upload/perfil/56e94ff1f35de800429646.jpeg'),
(121, '/upload/perfil/5876143eb91e0656038212.jpeg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sgp_categoria`
--

CREATE TABLE `sgp_categoria` (
  `id_categoria` int(11) NOT NULL,
  `nm_categoria` varchar(255) NOT NULL,
  `dh_cadastro` datetime NOT NULL,
  `id_usuario_cadastro` int(11) NOT NULL,
  `st_categoria` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `sgp_categoria`
--

INSERT INTO `sgp_categoria` (`id_categoria`, `nm_categoria`, `dh_cadastro`, `id_usuario_cadastro`, `st_categoria`) VALUES
(1, 'Ftp', '2017-01-11 08:51:12', 42, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sgp_senha`
--

CREATE TABLE `sgp_senha` (
  `id_senha` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `nm_senha` varchar(255) NOT NULL,
  `nm_login` varchar(255) NOT NULL,
  `senha` varchar(400) NOT NULL,
  `login_url` varchar(255) NOT NULL,
  `obs` text NOT NULL,
  `dh_cadastro` datetime NOT NULL,
  `id_usuario_cadastro` int(11) NOT NULL,
  `st_senha` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `sgp_senha`
--

INSERT INTO `sgp_senha` (`id_senha`, `id_categoria`, `nm_senha`, `nm_login`, `senha`, `login_url`, `obs`, `dh_cadastro`, `id_usuario_cadastro`, `st_senha`) VALUES
(1, 1, 'Teste', 'teste', 'iJ-mGhDTeHOU5rmAlQPBhLi4_E_0BsPrkFeyzWy1eNo', 'http://www.google.com', 'fsdadfdsafa', '2017-01-11 08:58:54', 42, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl`
--
ALTER TABLE `acl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `acl_to_grupo`
--
ALTER TABLE `acl_to_grupo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `acl_id` (`acl_id`),
  ADD KEY `grupo_id` (`grupo_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `modulo_id` (`modulo_id`);

--
-- Indexes for table `modulo`
--
ALTER TABLE `modulo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sca_grupo`
--
ALTER TABLE `sca_grupo`
  ADD PRIMARY KEY (`id_grupo`),
  ADD KEY `id_usuario_cadastro` (`id_usuario_cadastro`);

--
-- Indexes for table `sca_usuario`
--
ALTER TABLE `sca_usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `id_avatar` (`id_avatar`),
  ADD KEY `id_grupo` (`id_grupo`);

--
-- Indexes for table `sgg_avatar`
--
ALTER TABLE `sgg_avatar`
  ADD PRIMARY KEY (`id_avatar`);

--
-- Indexes for table `sgp_categoria`
--
ALTER TABLE `sgp_categoria`
  ADD PRIMARY KEY (`id_categoria`),
  ADD KEY `id_usuario_cadastro` (`id_usuario_cadastro`);

--
-- Indexes for table `sgp_senha`
--
ALTER TABLE `sgp_senha`
  ADD PRIMARY KEY (`id_senha`),
  ADD KEY `id_categoria` (`id_categoria`),
  ADD KEY `id_usuario_cadastro` (`id_usuario_cadastro`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acl`
--
ALTER TABLE `acl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;
--
-- AUTO_INCREMENT for table `acl_to_grupo`
--
ALTER TABLE `acl_to_grupo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `modulo`
--
ALTER TABLE `modulo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sca_grupo`
--
ALTER TABLE `sca_grupo`
  MODIFY `id_grupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sca_usuario`
--
ALTER TABLE `sca_usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `sgg_avatar`
--
ALTER TABLE `sgg_avatar`
  MODIFY `id_avatar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT for table `sgp_categoria`
--
ALTER TABLE `sgp_categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sgp_senha`
--
ALTER TABLE `sgp_senha`
  MODIFY `id_senha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `acl_to_grupo`
--
ALTER TABLE `acl_to_grupo`
  ADD CONSTRAINT `acl_to_grupo_ibfk_1` FOREIGN KEY (`acl_id`) REFERENCES `acl` (`id`),
  ADD CONSTRAINT `acl_to_grupo_ibfk_2` FOREIGN KEY (`grupo_id`) REFERENCES `sca_grupo` (`id_grupo`);

--
-- Limitadores para a tabela `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`modulo_id`) REFERENCES `modulo` (`id`);

--
-- Limitadores para a tabela `sca_grupo`
--
ALTER TABLE `sca_grupo`
  ADD CONSTRAINT `sca_grupo_ibfk_1` FOREIGN KEY (`id_usuario_cadastro`) REFERENCES `sca_usuario` (`id_usuario`);

--
-- Limitadores para a tabela `sca_usuario`
--
ALTER TABLE `sca_usuario`
  ADD CONSTRAINT `sca_usuario_ibfk_1` FOREIGN KEY (`id_grupo`) REFERENCES `sca_grupo` (`id_grupo`),
  ADD CONSTRAINT `sca_usuario_ibfk_2` FOREIGN KEY (`id_avatar`) REFERENCES `sgg_avatar` (`id_avatar`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
