/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/
  '/': {
    view: 'homepage'
  },
  '/auth/facebook_login': {
    controller: "AuthController", action: "facebook_login"
  },
  '/postcomentario/comentar': {
    controller: "PostComentario", action: "comentar"
  },
  '/postcomentario/get_ultimos_comentarios': {
    controller: "PostComentario", action: "get_ultimos_comentarios"
  },
  '/postcomentario/excluir': {
    controller: "PostComentario", action: "excluir"
  },
  '/post/criar': {
    controller: "Post", action: "criar"
  },
  '/post/getsocketid': {
    controller: "Post", action: "getSocketID"
  },
  '/post/getpost': {
    controller: "Post", action: "getPost"
  },
  '/post/timeline': {
    controller: "Post", action: "timeline"
  },
  '/post/excluir': {
    controller: "Post", action: "excluir"
  },
  '/postdenuncia/denunciar': {
    controller: "PostDenuncia", action: "denunciar"
  },
  '/postimagem/imagem/:id/:largura/:altura': {
    controller: "PostImagem", action: "imagem"
  },
  '/postvideo/video/:id/:video': {
    controller: "PostVideo", action: "video"
  },
  '/postvideo/imagem/:id': {
    controller: "PostVideo", action: "imagem"
  },
  '/postlike/like': {
    controller: "PostLike", action: "like"
  },
  '/postlike/get_like': {
    controller: "PostLike", action: "get_like"
  },
  '/postlike/unlike': {
    controller: "PostLike", action: "unlike"
  },
  '/postpublicidade/imagem/:id/:largura/:altura': {
    controller: "PostPublicidade", action: "imagem"
  },
  '/usuarioamigo/seguir': {
    controller: "UsuarioAmigo", action: "seguir"
  },
  '/usuarioamigo/aceitar': {
    controller: "UsuarioAmigo", action: "aceitar"
  },
  '/usuarioavatar/avatar/:id/:largura/:altura': {
    controller: "UsuarioAvatar", action: "avatar"
  },
  '/usuario/atualizar': {
    controller: "Usuario", action: "atualizar"
  },
  '/usuario/get': {
    controller: "Usuario", action: "get"
  },
  '/usuario/pesquisar': {
    controller: "Usuario", action: "pesquisar"
  },
  '/usuario/atualizar': {
    controller: "Usuario", action: "atualizar"
  },
  '/usuarionotificacao/get': {
    controller: "UsuarioNotificacao", action: "get"
  },
  '/usuarionotificacao/get_total': {
    controller: "UsuarioNotificacao", action: "get_total"
  },
  '/usuarionotificacao/excluir': {
    controller: "UsuarioNotificacao", action: "excluir"
  },
  '/usuariorelacionamento/get_relacionamentos': {
    controller: "UsuarioRelacionamento", action: "get_relacionamentos"
  },
  
  

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
