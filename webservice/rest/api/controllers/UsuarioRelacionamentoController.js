/**
 * UsuarioController
 *
 * @description :: Api para manipular os usuários
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

   /**
 * Pega os dados e lista
 *
 * (POST /usuariorelacionamento/get_relacionamentos)
 */
 get_relacionamentos: function(req, res) {
    if(!req.body.usuario_perfil_id)
      return res.json({status:"erro",msg: 'Parametros inválidos'});

    // pega e lista
    UsuarioRelacionamento.find().populateAll().sort("createdAt DESC").then(function(relacionamento){
      res.json({
          data:relacionamento,
          status:"sucesso",
          msg: "Dados recebidos com sucesso."
      });
    });

  },
};