var fs = require('fs');
var nestedPop = require('nested-pop');
var Promise = require('bluebird');
var ffmpeg = require('fluent-ffmpeg');

module.exports = {
  /**
   * mostra o video do post pelo post_id
   *
   * (GET /postvideo/video/:id)
   */
  video: function (req, res){

    req.validate({
      id: 'string'
    });

    //ffmpeg -i 9db3c950-0f66-46c1-b42c-63b484ba420c.mov -vcodec copy -acodec copy out.mp4

    var sql = "SELECT v.*";
    sql += " FROM app_post_video AS v";
    sql += " WHERE v.post_id = "+req.param('id');

    PostVideo.query(sql, [ ] ,function(err, video) {
      if (err) return res.negotiate(err);
      if (!video.length) return res.notFound();
      video = video[0];
      if (!video.videoFd) {
        return res.notFound();
      }

      const path = video.videoFd
      const stat = fs.statSync(path)
      const fileSize = stat.size
      const range = req.headers.range

      if (range) {
        const parts = range.replace(/bytes=/, "").split("-")
        const start = parseInt(parts[0], 10)
        const end = parts[1]
          ? parseInt(parts[1], 10)
          : fileSize-1

        const chunksize = (end-start)+1
        const file = fs.createReadStream(path, {start, end})
        const head = {
          'Content-Range': `bytes ${start}-${end}/${fileSize}`,
          'Accept-Ranges': 'bytes',
          'Content-Length': chunksize,
          'Content-Type': 'video/mp4',
        }

        res.writeHead(206, head)
        file.pipe(res)
      } else {
        const head = {
          'Content-Length': fileSize,
          'Content-Type': 'video/mp4',
        }
        res.writeHead(200, head)
        fs.createReadStream(path).pipe(res)
      }
    });
  },

  /**
   * mostra o avatar da pessoa pelo perfil_id
   *
   * (GET postvideo/imagem/:id)
   */
  imagem: function (req, res){
    req.validate({
      id: 'string'
    });
    var _this = this;
    PostVideo.findOne({id:req.param('id')},function(err, avatar) {
      if (err) return res.negotiate(err);
      if (!avatar) return res.notFound();
      if (!avatar.videoFd) {
        return res.notFound();
      }
      var SkipperDisk = require('skipper-disk');
      var fileAdapter = SkipperDisk(/* optional opts */);

      res.set("Content-disposition", "inline");
      res.set("Content-type", "image/png");


      	var name = avatar.name.split('.');
        nome = name[0];
        name = require('path').resolve(sails.config.appPath, 'assets/upload/'+nome+".png");

        // mostra o file
        fileAdapter.read(name)
        .on('error', function (err){
          return res.notFound();
        })
        .pipe(res);
    });
  },

}