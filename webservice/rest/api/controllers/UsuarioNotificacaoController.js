/**
 * UsuarioController
 *
 * @description :: Api para manipular os usuários
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var Promise = require('bluebird');

module.exports = {
  /**
   * Recebe os dados
   *
   * (POST /usuarionotificacao/get)
   */
  get: function(req, res) {
    if(!req.body.usuario_perfil_id)
      return res.json({status:"erro",msg: 'Parametros inválidos'});

    if(!req.body.limit)
      req.body.limit = 7;

    var sql =
      "SELECT * FROM (SELECT "+
        "notificacao.id, "+
        "notificacao.perfil_id_2, "+
        "notificacao.createdAt, "+
        "notificacao.status, "+
        "notificacao.type "+
      "FROM "+
        "app_usuario_notificacao as notificacao "+
      "WHERE 1 ) as sl WHERE perfil_id_2 = "+req.body.usuario_perfil_id+" "+
      "ORDER BY type='amizade' DESC, createdAt DESC ";


    if(req.body.limit && req.body.pagina){
      sql     += "LIMIT "+(req.body.limit*(req.body.pagina-1))+", "+req.body.limit;
    } else {
      sql     += "LIMIT 0"+", "+req.body.limit;
    }

    var userQueryAsync = Promise.promisify(UsuarioNotificacao.query);
    userQueryAsync(sql, [ ])
    .then(function(models) {
         var asyncs = [];
         var index = 0;
         response = models;
        if(models.length){
          for(var i = 0 in models){
            // pega os dados async
            asyncs.push(function(callback) {
              if(!models[index].status)
                UsuarioNotificacao.update({id:models[index].id}, {status:1}).exec(function(err, update){});
               // pega os dados do perfil pelo post id
               UsuarioNotificacao.findOne(models[index].id).populateAll().then(function(notificacao){
                   response[index].notificacao = notificacao;
                   index++;
                   callback();
               });
           });
         }
         async.series(asyncs, function(err) {
           if (!err) {
               sails.sockets.blast('usuarionotificacao_get', {usuario_perfil_id:req.body.usuario_perfil_id});
               res.json({status:"sucesso", msg: "Resultado Encontrado", data:response});
           }else{
               res.json({status:"erro", msg: "Aconteceu um erro #notificacao64"});
           }
         });
       }else{
          res.json({status:"erro", msg: "Nenhum resultado encontrado"});
       }
    });

  },

/**
 * Recebe os dados
 *
 * (POST /usuarionotificacao/get_total)
 */
get_total: function(req, res) {
  if(!req.body.usuario_perfil_id)
    return res.json({status:"erro",msg: 'Parametros inválidos'});

  var sql =
    "SELECT count(*) as total FROM (SELECT "+
      "notificacao.id, "+
      "notificacao.perfil_id_2, "+
      "notificacao.createdAt, "+
      "notificacao.status "+
    "FROM "+
      "app_usuario_notificacao as notificacao "+
    "WHERE notificacao.status is null AND notificacao.perfil_id_2 = "+req.body.usuario_perfil_id+" ) as sl ";

  var userQueryAsync = Promise.promisify(UsuarioNotificacao.query);
  userQueryAsync(sql, [ ])
  .then(function(models) {
       if (models.length) {
           sails.sockets.blast('usuarionotificacao_get_total', {total:models[0].total});
           res.json({status:"sucesso", msg: "Resultado Encontrado", data:models[0].total});
       }else{
           res.json({status:"erro", msg: "Nenhuma Novidade"});
       }
      
  });

},

/**
 * Pega os dados e exclui
 *
 * (POST /usuarionotificacao/excluir)
 */
 excluir: function(req, res) {
    if(!req.body.notificacao_id)
      return res.json({status:"erro",msg: 'Parametros inválidos'});

    UsuarioNotificacao.destroy({
      id: req.body.notificacao_id
    })
    .exec(function(err){
      if(!err){
        if(req.body.perfil_id_1){
          UsuarioAmigo.destroy({
            or : [
              { perfil_id_1: req.body.usuario_perfil_id, perfil_id_2: req.body.perfil_id_1 },
              { perfil_id_2: req.body.usuario_perfil_id, perfil_id_1: req.body.perfil_id_1 }
            ],
            action_perfil_id: req.body.perfil_id_1
          })
          .exec(function(err){
              sails.sockets.blast('usuarionotificacao_excluir', {id:req.body.notificacao_id});
              res.json({
                  status:"sucesso",
                  msg: "Notificação excluida com sucesso."
              });
          });
        }else{
          sails.sockets.blast('usuarionotificacao_excluir', {id:req.body.notificacao_id});
          res.json({
              status:"sucesso",
              msg: "Notificação excluida com sucesso."
          });
        }
        
      }else{
        return res.json({status:"erro",msg: "Aconteceu um erro #notificacao127"});
      }
    });

  },
};