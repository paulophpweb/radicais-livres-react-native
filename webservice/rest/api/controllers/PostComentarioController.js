/**
 * UsuarioController
 *
 * @description :: Api para manipular os usuários
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
 
 /**
 * Pega os dados e salva
 *
 * (POST /postcomentario/comentar)
 */
 comentar: function(req, res) {
    if(!req.body.usuario_perfil_id || !req.body.post_id || !req.body.texto)
      return res.json({status:"erro",msg: 'Parametros inválidos'});

    // quebra o texto para pegar palavroes
    var textos = req.body.texto.split(" ");
    var aTextos = [];
    if(textos.length){
      for(var i = 0 in textos){
        aTextos.push(textos[i].trim());
      }
    }

    // verifica se tem palavrão
    PostPalavrao.findOne({
      nome: { 'like' : aTextos }
    }).then(function(palavrao){
      if(palavrao){
        return res.json({status:"erro",msg: "Não aceitamos esta palavra '"+palavrao.nome+"' nos comentários. "});
      }else{
        PostComentario.create({
          post_id: req.body.post_id,
          usuario_perfil_id:req.body.usuario_perfil_id,
          texto:req.body.texto
        })
        .exec(function(err, dados){
          if(!err){
            PostComentario.findOne({
              id:dados.id
            })
            .populate("usuario_perfil_id")
            .exec(function(err, dados){
              if(err){
                return res.json({status:"erro",msg: "Aconteceu um erro #comentatario48"});
              }else{
                Post.findOne({
                  id: req.body.post_id
                })
                .exec(function(err, post){
                    PushService.notificar(post.perfil_id,req.body.usuario_id,"Comentário", "Fez um comentário no seu post - "+req.body.texto, null, null,true,'comentario',req.body.post_id);
                    sails.sockets.blast('postcomentario_comentar', dados);
                    res.json({
                        data:dados,
                        status:"sucesso",
                        msg: "Comentado com sucesso."
                    });
                });
                
              }
            });
          }else{
            return res.json({status:"erro",msg: "Aconteceu um erro #comentatario66"});
          }
        });
      }
    });
  },

   /**
 * Pega os dados e lista
 *
 * (POST /postcomentario/get_ultimos_comentarios)
 */
 get_ultimos_comentarios: function(req, res) {
    if(!req.body.usuario_perfil_id || !req.body.post_id)
      return res.json({status:"erro",msg: 'Parametros inválidos'});

    // pega os ultimos 3 comentarios no post
    PostComentario.find({post_id:req.body.post_id}).populateAll().sort("createdAt DESC").limit(req.body.ignorelimit ? 10000000000 : 3).then(function(comentarios){
      sails.sockets.blast('postcomentario_get_ultimos_comentarios', {id:req.body.comentario_id});
      res.json({
          data:comentarios,
          status:"sucesso",
          msg: "Dados recebidos com sucesso."
      });
    });

  },

  /**
 * Pega os dados e exclui
 *
 * (POST /postcomentario/excluir)
 */
 excluir: function(req, res) {
    if(!req.body.usuario_perfil_id || !req.body.comentario_id || !req.body.post_id)
      return res.json({status:"erro",msg: 'Parametros inválidos'});

    PostComentario.destroy({
      id: req.body.comentario_id
    })
    .exec(function(err){
      if(!err){
        sails.sockets.blast('postcomentario_excluir', {id:req.body.comentario_id, post_id:req.body.post_id});
        res.json({
            status:"sucesso",
            msg: "Comentário excluido com sucesso."
        });
      }else{
        return res.json({status:"erro",msg: "Aconteceu um erro #comentatario114"});
      }
    });

  },
};