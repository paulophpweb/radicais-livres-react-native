/**
 * UsuarioController
 *
 * @description :: Api para manipular os usuários
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
 var fs = require('fs');
 var nestedPop = require('nested-pop');
 var Promise = require('bluebird');
 var ffmpeg = require('fluent-ffmpeg');

module.exports = {
  /**
   * Salva os dados do post
   *
   * (POST /post/criar)
   */
  criar: function (req, res) {
    if(!req.body.texto || !req.body.usuario_perfil_id)
      return res.json({status:"erro",msg: 'Parametros inválidos'});

    // quebra o texto para pegar palavroes
    var textos = req.body.texto.split(" ");
    var aTextos = [];
    if(textos.length){
      for(var i = 0 in textos){
        aTextos.push(textos[i].trim());
      }
    }
    
    // verifica se tem palavrão
    PostPalavrao.findOne({
      nome: { 'like' : aTextos }
    }).then(function(palavrao){
      if(palavrao){
        return res.json({status:"erro",msg: "Não aceitamos esta palavra '"+palavrao.nome+"' no post. "});
      }else{
        Post.create({
          texto:req.body.texto,
          perfil_id:req.body.usuario_perfil_id,
        }).exec(function (err, dados) {
          if (err) {
            return res.json({status:"erro",msg: "Aconteceu um erro #post42"});
          }
          if (dados) {

                req.file('video').upload({
                  maxBytes: 1000000000,
                  dirname: require('path').resolve(sails.config.appPath, 'assets/upload'),
                },function whenDone(err, uploadedFilesVideo) {
                  if(!err){
                    if(uploadedFilesVideo){
                      if (uploadedFilesVideo.length){
                        var name = uploadedFilesVideo[0].fd.split('/');
                        name = name[name.length-1];
                        name_extensao = name.split('.');
                        var newFd = require('path').resolve(sails.config.appPath, 'assets/upload/'+name_extensao[0]+".mp4");
                        var newFdImage = require('path').resolve(sails.config.appPath, 'assets/upload');
                        // converte o vídeo
                        ffmpeg(uploadedFilesVideo[0].fd)
                        .screenshots({
                          timestamps: ['50%'],
                          filename: name_extensao[0]+".png",
                          folder: newFdImage,
                          size: '640x480'
                        })
                        .save(newFd).outputOptions([
                          '-vcodec copy',
                          '-acodec copy'
                        ]);
                        // grava o video no banco
                        PostVideo.create({
                          post_id:dados.id,
                          videoFd: newFd,
                          mime:"video/mp4",
                          name:name_extensao[0]+".mp4"
                        }).exec(function (err, video) {
                          dados.video = video;
                          sails.sockets.blast('post_criar', dados);
                          return res.json({data:dados, status:"sucesso", msg: "Aleluia, Deus seja louvado, ocorreu tudo bem."});
                        });
                      }
                    }
                  }else if(uploadedFilesVideo){
                    if(uploadedFilesVideo.length){
                      Post.destroy({id:dados.id}).exec(function (err) {
                        return res.json({status:"erro",msg: "Um erro inesperado aconteceu."});
                      });
                    }
                  }
                });

                req.file('imagem').upload({
                  maxBytes: 1000000000,
                  dirname: require('path').resolve(sails.config.appPath, 'assets/upload'),
                },function whenDone(err, uploadedFilesImagem) {
                  if(!err){
                    if(uploadedFilesImagem){
                      if (uploadedFilesImagem.length){
                        var name = uploadedFilesImagem[0].fd.split('/');
                        name = name[name.length-1];

                        // grava a imagem no banco
                        PostImagem.create({
                          post_id:dados.id,
                          imagemFd: uploadedFilesImagem[0].fd,
                          mime:uploadedFilesImagem[0].type,
                          name:name
                        }).exec(function (err, imagem) {
                          dados.imagem = imagem;
                          sails.sockets.blast('post_criar', dados);
                          return res.json({data:dados, status:"sucesso", msg: "Aleluia, Deus seja louvado, ocorreu tudo bem."});
                        });
                      }
                    }
                  }else if(uploadedFilesImagem){
                    if(uploadedFilesImagem.length){
                      Post.destroy({id:dados.id}).exec(function (err) {
                        return res.json({status:"erro",msg: "Um erro inesperado aconteceu."});
                      });
                    }
                  }
                });
          }
        });
      }
    });
    return;
  },
  /**
   * Pega o Socket
   *
   * (POST /post/getsocketid)
   */
  getSocketID: function(req, res) {
    if (!req.isSocket) {
      return res.badRequest();
    }

    var socketId = sails.sockets.getId(req);
    if(socketId){
      sails.sockets.join(socketId);
      return res.json(socketId);
    }else{
      return res.badRequest();
    }

    //sails.sockets.join(socketId, 'funSockets');

    //return res.json(socketId);
  },
  /**
   * Pega o Post
   *
   * (POST /post/getpost)
   */
  getPost: function(req, res) {

    if(!req.body.usuario_id || !req.body.post_id || !req.body.usuario_perfil_id)
      return res.json({status:"erro",msg: 'Parametros inválidos'});
    var sql =
      "SELECT * FROM (SELECT "+
        "post.*, "+
        "(SELECT count(id) FROM app_post_like WHERE post.id = post_id) as total_likes, "+
        "(SELECT count(id) FROM app_post_comentario WHERE post.id = post_id) as total_comentarios, "+
        "(SELECT count(id) FROM app_post_denuncia WHERE post.id = post_id) as total_denuncia, "+
        "(SELECT count(id) FROM app_usuario_amigo WHERE ((perfil_id_1 = "+req.body.usuario_perfil_id+" AND perfil_id_2 = perfil_id) OR (perfil_id_2 = "+req.body.usuario_perfil_id+" AND perfil_id_1 = perfil_id)) AND status = 1) as amigo "+
      "FROM "+
        "app_post as post "+
      "WHERE 1 ) as sl WHERE id = "+req.body.post_id+" "+
      "ORDER BY createdAt DESC ";

    var userQueryAsync = Promise.promisify(Post.query);
    userQueryAsync(sql, [ ])
    .then(function(models) {
        if(models.length){
          models = models[0]
          var asyncs = [];
          response = models;
          // pega os dados async
          asyncs.push(function(callback) {
             // pega os dados do perfil pelo post id
             UsuarioPerfil.findOne(models.perfil_id).populateAll().then(function(perfil){
                 response.usuario_perfil_id = perfil;
                 // pegar o video pelo post id
                 PostVideo.findOne({post_id:models.id}).populateAll().then(function(video){
                    response.video = video;
                    // pega a imagem pelo post id
                    PostImagem.findOne({post_id:models.id}).populateAll().then(function(imagem){
                      response.imagem = imagem;
                      // pega os ultimos 3 likes no post
                      PostLike.find({post_id:models.id}).populateAll().sort("createdAt DESC").limit(req.body.ignorelimit ? 10000000000 : 3).then(function(likes){
                        response.ultimos_likes = likes;
                        // pega os ultimos 3 comentarios no post
                        PostComentario.find({post_id:models.id}).populateAll().sort("createdAt DESC").limit(req.body.ignorelimit ? 10000000000 : 3).then(function(comentarios){
                          response.ultimos_comentarios = comentarios;
                          callback();
                        });
                      });
                      
                    });
                 });
                 
             })
         });
         async.series(asyncs, function(err) {
           if (!err) {
               res.json({status:"sucesso", msg: "Resultado Encontrado", data:response});
           }else{
               res.json({status:"erro", msg: "Aconteceu um erro #post201"});
           }
         });
       }else{
          res.json({status:"erro", msg: "Nenhum post encontrado"});
       }
    });
  },
  /**
   * Timeline
   *
   * (POST /post/timeline)
   */
  timeline: function (req, res) {
    var response = [];
    if(!req.body.usuario_id || !req.body.sort || !req.body.usuario_perfil_id)
      return res.json({status:"erro",msg: 'Parametros inválidos'});
    if(!req.body.limit)
      req.body.limit = 10;

    var $where_ = "";
    if(req.body.usuario_perfil_id_search)
       $where_ += "AND perfil_id = "+req.body.usuario_perfil_id_search+" ";

    var sql =
      "SELECT * FROM (SELECT "+
        "post.*, "+
        "(SELECT count(id) FROM app_post_like WHERE post.id = post_id) as total_likes, "+
        "(SELECT count(id) FROM app_post_comentario WHERE post.id = post_id) as total_comentarios, "+
        "(SELECT count(id) FROM app_post_denuncia WHERE post.id = post_id) as total_denuncia, "+
        "(SELECT count(id) FROM app_usuario_amigo WHERE ((perfil_id_1 = "+req.body.usuario_perfil_id+" AND perfil_id_2 = perfil_id) OR (perfil_id_2 = "+req.body.usuario_perfil_id+" AND perfil_id_1 = perfil_id)) AND status = 1) as amigo "+
      "FROM "+
        "app_post as post "+
      "WHERE 1 ) as sl WHERE 1 AND (amigo = 1 OR perfil_id = "+req.body.usuario_perfil_id+") AND (total_denuncia <= 5 OR perfil_id is null) "+$where_+
      "ORDER BY createdAt DESC ";
    if(req.body.limit && req.body.pagina){
      sql     += "LIMIT "+(req.body.limit*(req.body.pagina-1))+", "+req.body.limit;
    } else {
      sql     += "LIMIT 0"+", "+req.body.limit;
    }

    var userQueryAsync = Promise.promisify(Post.query);
    userQueryAsync(sql, [ ])
    .then(function(models) {
        var asyncs = [];
        var index = 0;
        response = models;
        if(models.length){
            for(var i = 0 in models){
              // pega os dados async
              asyncs.push(function(callback) {
                if(models[index].perfil_id){
                 // pega os dados do perfil pelo post id
                 UsuarioPerfil.findOne({id:models[index].perfil_id}).populateAll().then(function(perfil){
                     response[index].usuario_perfil_id = perfil;
                     // pegar o video pelo post id
                     PostVideo.findOne({post_id:models[index].id}).populateAll().then(function(video){
                        response[index].video = video;
                        // pega a imagem pelo post id
                        PostImagem.findOne({post_id:models[index].id}).populateAll().then(function(imagem){
                          response[index].imagem = imagem;
                          // pega os ultimos 3 likes no post
                          PostLike.find({post_id:models[index].id}).populateAll().sort("createdAt DESC").limit(3).then(function(likes){
                            response[index].ultimos_likes = likes;
                            // pega os ultimos 3 comentarios no post
                            PostComentario.find({post_id:models[index].id}).populateAll().sort("createdAt DESC").limit(3).then(function(comentarios){
                              response[index].ultimos_comentarios = comentarios;
                              index++;
                              callback();
                            });
                          });
                          
                        });
                     });
                     
                 });
                }else{
                  // pega a imagem pelo post id
                  PostPublicidade.findOne({post_id:models[index].id}).populateAll().then(function(publicidade){
                    response[index].publicidade = publicidade;
                    // pegar o video pelo post id
                     PostVideo.findOne({post_id:models[index].id}).populateAll().then(function(video){
                        response[index].video = video;
                        // pega a imagem pelo post id
                        PostImagem.findOne({post_id:models[index].id}).populateAll().then(function(imagem){
                          response[index].imagem = imagem;
                          // pega os ultimos 3 likes no post
                          PostLike.find({post_id:models[index].id}).populateAll().sort("createdAt DESC").limit(3).then(function(likes){
                            response[index].ultimos_likes = likes;
                            // pega os ultimos 3 comentarios no post
                            PostComentario.find({post_id:models[index].id}).populateAll().sort("createdAt DESC").limit(3).then(function(comentarios){
                              response[index].ultimos_comentarios = comentarios;
                              index++;
                              callback();
                            });
                          });
                          
                        });
                     });
                  });
                }
             });
            }
            async.series(asyncs, function(err) {
               if (!err) {
                   res.json({status:"sucesso", msg: "Resultado Encontrado", data:response});
               }else{
                   res.json({status:"erro", msg: "Aconteceu um erro #post304"});
               }
            });
        }else{
          res.json({status:"erro", msg: "Nenhum post encontrado"});
        }
    });
  },
   /**
 * Pega os dados e exclui
 *
 * (POST /post/excluir)
 */
 excluir: function(req, res) {
    if(!req.body.usuario_perfil_id || !req.body.post_id)
      return res.json({status:"erro",msg: 'Parametros inválidos'});
    // deleta o post
    Post.destroy({
      id: req.body.post_id
    })
    .exec(function(err){
      if(!err){
        // deleta os comentarios
        PostComentario.destroy({
          post_id: req.body.post_id
        })
        .exec(function(err){
          if(!err){
            // deleta a imagem
            PostImagem.destroy({
              post_id: req.body.post_id
            })
            .exec(function(err){
              if(!err){
                // deleta o likers
                PostLike.destroy({
                  post_id: req.body.post_id
                })
                .exec(function(err){
                  if(!err){
                    // deleta o video
                    PostVideo.destroy({
                      post_id: req.body.post_id
                    })
                    .exec(function(err){
                      if(!err){
                        sails.sockets.blast('post_excluir', {id:req.body.post_id});
                        res.json({
                            status:"sucesso",
                            msg: "Post excluido com sucesso."
                        });
                      }else{
                        res.json({
                            status:"sucesso",
                            msg: "Aconteceu um erro #post391"
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });

  },
};
