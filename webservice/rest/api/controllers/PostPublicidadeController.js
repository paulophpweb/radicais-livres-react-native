/**
 * PostImagemController
 *
 * @description :: Api para manipular os usuários
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

 var fs = require('fs');
 var gm = require('gm');

module.exports = {
  /**
   * mostra o avatar da pessoa pelo perfil_id
   *
   * (GET postimagem/imagem/:id)
   */
  imagem: function (req, res){
    req.validate({
      id: 'string'
    });
    var _this = this;
    PostPublicidade.findOne({id:req.param('id')},function(err, avatar) {
      if (err) return res.negotiate(err);
      if (!avatar) return res.notFound();
      if (!avatar.imagemFd) {
        return res.notFound();
      }
      var SkipperDisk = require('skipper-disk');
      var fileAdapter = SkipperDisk(/* optional opts */);

      res.set("Content-disposition", "inline");
      res.set("Content-type", avatar.mime);

      if(req.param('largura') || req.param('altura')){
        var name = avatar.name.split('.');
        nome = name[0];
        extensao = name[name.length-1];
        name = require('path').resolve(sails.config.appPath, 'assets/upload')+"/"+nome+"_"+req.param('largura')+"_"+req.param('altura')+"."+extensao;
        if(!fs.existsSync(name)){
          _this.process(avatar.imagemFd,avatar.name,req.param('largura'),req.param('altura'),function (data) {
              // mostra o file
              fileAdapter.read(avatar.imagemFd)
              .on('error', function (err){
                return res.notFound();
              })
              .pipe(res);
          });
        }else{
          // mostra o file
          fileAdapter.read(name)
          .on('error', function (err){
            return res.notFound();
          })
          .pipe(res);
        }
      }else{
        // mostra o file
        fileAdapter.read(avatar.imagemFd)
        .on('error', function (err){
          return res.notFound();
        })
        .pipe(res);
      }
    });
  },

  resize: function (srcPath, dstPath, width, height, done) {
      gm(srcPath)
          .options({imageMagick: true})
          .resize(width,height)
          .write(dstPath, done);
  },

  process: function (uri, fileName, largura, altura, done) {
      var self = this;
      var name = fileName.split('.');
      nome = name[0];
      extensao = name[name.length-1];
      name = require('path').resolve(sails.config.appPath, 'assets/upload')+"/"+nome+"_"+largura+"_"+altura+"."+extensao;
      self.resize(uri, name, largura, altura, done);
  }
};