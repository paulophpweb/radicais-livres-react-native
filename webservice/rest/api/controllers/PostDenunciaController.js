/**
 * UsuarioController
 *
 * @description :: Api para manipular os usuários
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
 /**
 * Pega os dados e salva
 *
 * (POST /postdenuncia/denunciar)
 */
 denunciar: function(req, res) {
    if(!req.body.usuario_perfil_id || !req.body.post_id || !req.body.texto)
      return res.json({status:"erro",msg: 'Parametros inválidos'});

    PostDenuncia.find({
      post_id: req.body.post_id,
      usuario_perfil_id:req.body.usuario_perfil_id
    })
    .exec(function(err, dados){
      if(!dados.length){
      	PostDenuncia.findOrCreate({
      		post_id: req.body.post_id,
      		usuario_perfil_id:req.body.usuario_perfil_id
      	},{
          post_id: req.body.post_id,
          usuario_perfil_id:req.body.usuario_perfil_id,
          texto:req.body.texto
        })
        .exec(function(err, dados){
        	if(err){
        		return res.json({status:"erro",msg: "Aconteceu um erro #denuncia34"});
        	}else{
            PostDenuncia.findOne({
              post_id: req.body.post_id,
              usuario_perfil_id:req.body.usuario_perfil_id
            })
            .populate("post_id")
            .exec(function(err, dados){
              Post.findOne({
                id: req.body.post_id
              })
              .exec(function(err, post){
                sails.sockets.blast('postdenuncia_denunciar', dados);
                res.json({
                    data:dados,
                    status:"sucesso",
                    msg: "Post Denunciado com sucesso."
                });
              });
              
            }); 
        	}
       	});
      }else{
        return res.json({status:"erro",msg: "Você já denunciou este post."});
      }
    });

  },
};