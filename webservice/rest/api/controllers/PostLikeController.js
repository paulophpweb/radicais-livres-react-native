/**
 * UsuarioController
 *
 * @description :: Api para manipular os usuários
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
 /**
 * Pega os dados e salva
 *
 * (POST /postlike/like)
 */
 like: function(req, res) {
    if(!req.body.usuario_perfil_id || !req.body.post_id)
      return res.json({status:"erro",msg: 'Parametros inválidos'});

    PostLike.find({
      post_id: req.body.post_id,
      usuario_perfil_id:req.body.usuario_perfil_id
    })
    .exec(function(err, dados){
      if(!dados.length){
      	PostLike.findOrCreate({
      		post_id: req.body.post_id,
      		usuario_perfil_id:req.body.usuario_perfil_id
      	},{
          post_id: req.body.post_id,
          usuario_perfil_id:req.body.usuario_perfil_id
        })
        .exec(function(err, dados){
        	if(err){
        		return res.json({status:"erro",msg: "Aconteceu um erro #like33"});

        	}else{
            PostLike.findOne({
              post_id: req.body.post_id,
              usuario_perfil_id:req.body.usuario_perfil_id
            })
            .populate("usuario_perfil_id")
            .exec(function(err, dados){

              Post.findOne({
                id: req.body.post_id
              })
              .exec(function(err, post){
                sails.sockets.blast('postlike_like', dados);
                PushService.notificar(post.perfil_id,req.body.usuario_id,"Radical", "Achou Radical o seu post - "+post.texto, null, null,true,'like',req.body.post_id);
                res.json({
                    data:dados,
                    status:"sucesso",
                    msg: "Post Curtido com sucesso."
                });
              });
              
            }); 
        	}
       	});
      }else{
        return res.json({status:"erro",msg: "Você já curtiu este post."});
      }
    });

  },
  /**
   * Recebe os dados
   *
   * (POST /postlike/get_like)
   */
  get_like: function(req, res) {
    if(!req.body.post_id)
      return res.json({status:"erro",msg: 'Parametros inválidos'});

  	PostLike.find({
  		post_id: req.body.post_id
  	})
    .exec(function(err, dados){
    	if(err){
    		return res.json({status:"erro",msg: "Aconteceu um erro #post79"});
    	}else if(dados.length){
    		 res.json({
	              data:dados,
	              status:"sucesso",
	              msg: "Resultado encontrado"
	         });
    	}else{
    		return res.json({status:"erro",msg: "Nenhum resultado encontrado"});
    	}
   	});

  },

   /**
 * Pega os dados e exclui
 *
 * (POST /postlike/unlike)
 */
 unlike: function(req, res) {
    if(!req.body.usuario_perfil_id || !req.body.like_id)
      return res.json({status:"erro",msg: 'Parametros inválidos'});

    PostLike.destroy({
      id: req.body.like_id
    })
    .exec(function(err){
      if(!err){
        sails.sockets.blast('postlike_unlike', {id:req.body.like_id});
        res.json({
            status:"sucesso",
            msg: "Like excluido com sucesso."
        });
      }else{
        return res.json({status:"erro",msg: "Aconteceu um erro #post113"});
      }
    });

  },
};