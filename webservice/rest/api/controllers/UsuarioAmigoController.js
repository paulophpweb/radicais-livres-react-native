/**
 * UsuarioAmigoController
 *
 * @description :: Api para manipular os usuários
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
 
 /**
 * Pega os dados e salva
 *
 * (POST /usuarioamigo/seguir)
 */
 seguir: function(req, res) {
    if(!req.body.perfil_id_2)
      return res.json({status:"erro",msg: 'Parametros inválidos'});

    UsuarioAmigo.findOne({
      or : [
        { perfil_id_1: req.body.usuario_perfil_id, perfil_id_2: req.body.perfil_id_2 },
        { perfil_id_2: req.body.usuario_perfil_id, perfil_id_1: req.body.perfil_id_2 }
      ]
    })
    .exec(function(err, dados){
      if(err){
        return res.json({status:"erro",msg: 'Aconteceu um erro #usuarioamigo27'});
      }else{
        if(dados){
          return res.json({status:"erro",msg: 'Você já adicionou este amigo, aguarde a confirmação.'});
        }else{
          UsuarioAmigo.create({
            perfil_id_1: req.body.usuario_perfil_id,
            perfil_id_2:req.body.perfil_id_2,
            action_perfil_id:req.body.usuario_perfil_id
          })
          .exec(function(err, dados){
            if(!err){
              sails.sockets.blast('usuarioamigo_seguir', dados);
              PushService.notificar(req.body.perfil_id_2,req.body.usuario_id,"Radical", "Pediu para seguir você. ", null, null,true,'amizade');
              res.json({
                  data:dados,
                  status:"sucesso",
                  msg: "Aguarde a confirmação do amigo."
              });
            }else{
              return res.json({status:"erro",msg: "Aconteceu um erro #usuarioamigo47"});
            }
          });
        }
      }
    });

  },
    /**
 * aceita
 *
 * (POST /usuarioamigo/aceitar)
 */
 aceitar: function(req, res) {
    if(!req.body.perfil_id_1 || !req.body.id)
      return res.json({status:"erro",msg: 'Parametros inválidos'});

    UsuarioAmigo.findOne({
      or : [
        { perfil_id_1: req.body.usuario_perfil_id, perfil_id_2: req.body.perfil_id_1 },
        { perfil_id_2: req.body.usuario_perfil_id, perfil_id_1: req.body.perfil_id_1 }
      ],
      action_perfil_id: req.body.perfil_id_1,
      status:1
    })
    .exec(function(err, dados){
      if(!err){
        if(!dados){
          UsuarioAmigo.update({
            or : [
              { perfil_id_1: req.body.usuario_perfil_id },
              { perfil_id_2: req.body.usuario_perfil_id }
            ],
            action_perfil_id: req.body.perfil_id_1
          }, {
            status:1,
          }).exec(function(err, update_perfil){
            if(!err){
                UsuarioNotificacao.destroy({id: req.body.id}).exec(function(err){});
                PushService.notificar(req.body.perfil_id_1,req.body.usuario_id,"Radical", "Aceitou a sua amizade. ", null, null,true,'amizade_aceita');
                return res.json({
                    data:update_perfil[0],
                    status:"sucesso"
                });
            }else{
              return res.json({status:"erro",msg: 'Erro, algo inesperado aconteceu, tente novamente'});
            }
          });
        }else{
          return res.json({status:"erro",msg: 'Você já aceitou este amigo.'});
        }
      }else{
        return res.json({status:"erro",msg: 'Aconteceu um erro #usuarioamigo101'});
      }
    });

  },

};