/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

 var fs = require('fs');
 var moment = require('moment');

module.exports = {
  /**
   * Pega os dados do facebook e salva
   *
   * (POST /auth/facebook_login)
   */
  facebook_login: function (req, res) {
    var _this = this;
    var facebookId = req.param('facebookId');
    var facebookToken = req.param('facebookToken');
    var data_nascimento = req.param('data_nascimento');
    var nome = req.param('nome');
    var email = req.param('email');
    var genero = req.param('genero');

    if (!facebookId) {
      return res.json({status:"erro",msg: 'Parametros inválidos'});
    }

    $query = [{facebookId: facebookId}];
    if(email)
      $query.push({email:email});

    Usuario.findOrCreate({
      or : $query
    },{
      facebookId:facebookId
    })
    .exec(function(err, dados){
      console.log(err);
        if (!dados) {
          return res.json({status:"erro",msg: 'Erro, usuário não encontrado'});
        }else{

          $data = {
            facebookId:facebookId,
            facebookToken:facebookToken,
            auth_token:jwToken.issue({id : dados.id })
          };
          if(email)
            $data.email = email;
          Usuario.update({id:dados.id}, $data).exec(function(err, update_user){
            if(err) { 
              return res.json({status:"erro",msg: "Aconteceu um erro #auth47"});
            } else {
                if(!update_user[0].usuario_perfil_id){
                  UsuarioPerfil.create({
                    data_nascimento: data_nascimento ? moment(data_nascimento, 'DD/MM/YYYY').format() : '1987-01-22',
                    //data_nascimento: '1987-01-22',
                    nome:nome,
                    genero:genero,
                  }).exec(function(err, profile){
                    if(err) { 
                      return res.json({status:"erro",msg: "Aconteceu um erro #auth57"});
                    } else {
                      // faz o upload da imagem
                      req.file('photo').upload({
                        dirname: require('path').resolve(sails.config.appPath, 'assets/upload'),
                        maxBytes: 10000000
                      },function whenDone(err, uploadedFiles) {
                        // se tiver arquivos faz o upload
                        if (uploadedFiles.length){
                          var name = uploadedFiles[0].fd.split('/');
                          name = name[name.length-1];
                          UsuarioAvatar.create({
                            avatarFd: uploadedFiles[0].fd,
                            mime:uploadedFiles[0].type,
                            name:name
                          }).exec(function (err, avatar) {
                              UsuarioPerfil.update({id:profile.id}, {avatar_id:avatar.id}).exec(function(err, update_perfil){
                                // fim do upload
                                Usuario.update({id:update_user[0].id}, {usuario_perfil_id:profile.id}).exec(function(err, update_perfil){
                                  if(!err){
                                    // popula e retorna
                                    Usuario.findOne({id:update_user[0].id})
                                    .populateAll()
                                    .exec(function(err, dados){
                                      if(err){
                                        return res.json({status:"erro",msg: 'Aconteceu um erro #auth82'});
                                      }else{
                                        // Save the user in the session
                                        req.session.usuario = dados;
                                        res.json({
                                            data:dados,
                                            status:"sucesso"
                                        });
                                      }
                                    });
                                  }else{
                                    return res.json({status:"erro",msg: 'Aconteceu um erro #auth93'});
                                  }
                                });
                              });
                          });
                        }
                      });
                      
                    }
                  });
                }else if(update_user[0].usuario_perfil_id){
                  
                  // popula e retorna
                  Usuario.findOne({id:update_user[0].id})
                  .populateAll()
                  .exec(function(err, dados){
                    if(err){
                      return res.json({status:"erro",msg: 'Aconteceu um erro #auth110'});
                    }else{
                      // Save the user in the session
                      req.session.usuario = dados;
                      return res.json({
                          data:dados,
                          status:"sucesso"
                      });
                    }
                  });
                }else{
                  return res.json({status:"erro",msg: 'Erro, algo inesperado aconteceu, tente novamente'});
                }
            }
          });
          
        }
    });
  },
};
