/**
 * UsuarioController
 *
 * @description :: Api para manipular os usuários
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
 var Promise = require('bluebird');
 var CryptoJS = require("crypto-js");
 var fs = require('fs');
  var moment = require('moment');

module.exports = {

  /**
   * Pega o usuario
   *
   * (POST /usuario/get)
   */
   get: function(req, res) {
    if(!req.body.usuario_perfil_id_search)
      return res.json({status:"erro",msg: 'Parametros inválidos'});

    var sql =
      "SELECT * FROM (SELECT "+
        "usuario.id, "+
        "relacionamento.nome as relacionamento, "+
        "(SELECT count(id) FROM app_usuario_amigo WHERE ((perfil_id_1 = "+req.body.usuario_perfil_id+" AND perfil_id_2 = usuario.usuario_perfil_id) OR (perfil_id_2 = "+req.body.usuario_perfil_id+" AND perfil_id_1 = usuario.usuario_perfil_id)) AND status = 1) as amigo, "+
        "(SELECT count(id) FROM app_usuario_amigo WHERE ((perfil_id_1 = "+req.body.usuario_perfil_id+" AND perfil_id_2 = usuario.usuario_perfil_id) OR (perfil_id_2 = "+req.body.usuario_perfil_id+" AND perfil_id_1 = usuario.usuario_perfil_id)) AND status is null) as aguardando_amizade "+
      "FROM "+
        "app_usuario as usuario "+
        "LEFT JOIN app_usuario_perfil as perfil ON usuario.usuario_perfil_id = perfil.id "+
        "LEFT JOIN app_usuario_relacionamento as relacionamento ON perfil.relacionamento_id = relacionamento.id "+
      "WHERE usuario_perfil_id = "+req.body.usuario_perfil_id_search+" ) as sl ";

      Usuario.query(sql,[],function(err, usuario_sql) {
        if(!err && usuario_sql.length){
          usuario_sql = usuario_sql[0];
          // pega e lista
          Usuario.findOne({usuario_perfil_id:req.body.usuario_perfil_id_search}).populateAll().then(function(usuario){
              var sqlSeguindo =
              "SELECT * FROM (SELECT "+
                "count(amizade.id) as total "+
              "FROM "+
                "app_usuario_amigo as amizade "+
              "WHERE ((perfil_id_1 = "+req.body.usuario_perfil_id_search+") OR (perfil_id_2 = "+req.body.usuario_perfil_id_search+")) AND status = 1 AND action_perfil_id = "+req.body.usuario_perfil_id_search+") as sl ";
              UsuarioAmigo.query(sqlSeguindo,[],function(err, rawSeguindo) {
                if (err) { 
                  return res.json({status:"erro",msg: 'Um erro aconteceu'});
                }else{
                  usuario.seguindo = rawSeguindo[0].total;
                }
                var sqlSeguidores =
                "SELECT * FROM (SELECT "+
                  "count(amizade.id) as total "+
                "FROM "+
                  "app_usuario_amigo as amizade "+
                "WHERE ((perfil_id_1 = "+req.body.usuario_perfil_id_search+") OR (perfil_id_2 = "+req.body.usuario_perfil_id_search+")) AND status = 1 AND action_perfil_id <> "+req.body.usuario_perfil_id_search+") as sl ";
                UsuarioAmigo.query(sqlSeguidores,[],function(err, rawSeguidores) {
                  if (err) { 
                    return res.json({status:"erro",msg: 'Um erro aconteceu'});
                  }else{
                    usuario.seguidores = rawSeguidores[0].total;
                  }
                  usuario.amigo = usuario_sql.amigo;
                  usuario.aguardando_amizade = usuario_sql.aguardando_amizade;
                  usuario.relacionamento = usuario_sql.relacionamento;
                  res.json({
                      data:usuario,
                      status:"sucesso",
                      msg: "Dados recebidos com sucesso."
                  });
                }); 
              });
          });
        }else{
          console.log(err);
          return res.json({status:"erro",msg: 'Um erro aconteceu'});
        }

      });
  },
	/**
   * Pega os usuarios pela pesquisa
   *
   * (POST /usuario/pesquisar)
   */
  pesquisar: function(req, res) {

    if(!req.body.usuario_id || !req.body.usuario_perfil_id)
      return res.json({status:"erro",msg: 'Parametros inválidos'});
    var chave = "jesusunicocaminho";

  	if(!req.body.limit)
      req.body.limit = 7;
    var where = "WHERE 1 ";
    if(!req.body.qrcode){
      if(!req.body.pesquisa && req.body.seguindo != "true" && req.body.seguidores != "true"){
        // filtro pessoas nao relacionando
        if(req.body.naorelacionando){
          where += "AND relacionando is null ";
        }else if(!req.body.naorelacionando){
          where += "AND relacionando = 1 ";
        }
      }
      //filtro de pastor
      if(req.body.pastor == "true")
        where += "AND pastor = 1 ";
      //filtro de discipulador
      if(req.body.discipulador == "true")
        where += "AND discipulador = 1 ";
      //filtro de lider
      if(req.body.lider == "true")
        where += "AND lider = 1 ";
      //filtro de membro
      if(req.body.membro == "true")
        where += "AND membro = 1 ";
      //filtro de genero
      if(req.body.genero)
        where += "AND genero = '"+req.body.genero+"' ";
      //filtro de idade
      if(req.body.idade_minima && req.body.idade_maxima)
        where += "AND idade >= '"+req.body.idade_minima+"' AND idade <= '"+req.body.idade_maxima+"' ";
      // filtro para não puxar meu usuario e nem meu amigo
      //where += "AND amigo <> 1 AND id <> "+req.body.usuario_id+" ";
      if(req.body.seguindo != "true" && req.body.seguidores != "true"){
        where += "AND id <> "+req.body.usuario_id+" ";
        if(req.body.pastor != "true" && !req.body.pesquisa){
          where += "AND (estado_civil <> 'Casado' OR estado_civil is null) ";
        }else if(!req.body.pesquisa){
          where += "AND (estado_civil <> 'Casado' OR estado_civil is null) ";
        }
      }else if(req.body.seguindo == "true"){
        where += "AND seguindo = 1 ";
      }else if(req.body.seguidores == "true"){
        where += "AND seguidor = 1 ";
      }

    if(req.body.pesquisa)
        where += "AND nome LIKE '%"+req.body.pesquisa+"%' ";
    }else{
      if(req.body.qrcode)
      where += "AND id = '"+req.body.qrcode+"' ";
    }

    if(req.body.limit && req.body.pagina){
      sql     += "LIMIT "+(req.body.limit*(req.body.pagina-1))+", "+req.body.limit;
    } else {
      sql     += "LIMIT 0"+", "+req.body.limit;
    }

    var sql =
      "SELECT * FROM (SELECT "+
        "usuario.id, "+
        "usuario.latitude, "+
        "usuario.longitude, "+
        "usuario.usuario_perfil_id, "+
        "usuario.createdAt, "+
        "perfil.relacionando, "+
        "perfil.ministro as pastor, "+
        "perfil.lider, "+
        "perfil.nome, "+
        "perfil.discipulador, "+
        "perfil.membro, "+
        "perfil.data_nascimento, "+
        "perfil.genero, "+
        "relacionamento.nome as estado_civil, "+
        "TIMESTAMPDIFF(YEAR, data_nascimento, CURDATE()) as idade, "+
        "(SELECT count(id) FROM app_usuario_amigo WHERE ((perfil_id_1 = "+req.body.usuario_perfil_id+" AND perfil_id_2 = perfil.id) OR (perfil_id_2 = "+req.body.usuario_perfil_id+" AND perfil_id_1 = perfil.id)) AND status = 1) as amigo, "+
        "(SELECT count(id) FROM app_usuario_amigo WHERE ((perfil_id_1 = "+req.body.usuario_perfil_id+" AND perfil_id_2 = perfil.id) OR (perfil_id_2 = "+req.body.usuario_perfil_id+" AND perfil_id_1 = perfil.id)) AND status is null) as aguardando_amizade, "+
        "(SELECT count(id) FROM app_usuario_amigo WHERE ((perfil_id_1 = "+req.body.usuario_perfil_id+" AND perfil_id_2 = perfil.id) OR (perfil_id_2 = "+req.body.usuario_perfil_id+" AND perfil_id_1 = perfil.id)) AND status = 1 AND action_perfil_id = "+req.body.usuario_perfil_id+") as seguindo, "+
        "(SELECT count(id) FROM app_usuario_amigo WHERE ((perfil_id_1 = "+req.body.usuario_perfil_id+" AND perfil_id_2 = perfil.id) OR (perfil_id_2 = "+req.body.usuario_perfil_id+" AND perfil_id_1 = perfil.id)) AND status = 1 AND action_perfil_id <> "+req.body.usuario_perfil_id+") as seguidor "+
      "FROM "+
        "app_usuario as usuario "+
        "LEFT JOIN app_usuario_perfil as perfil ON usuario.usuario_perfil_id = perfil.id "+
        "LEFT JOIN app_usuario_relacionamento as relacionamento ON perfil.relacionamento_id = relacionamento.id "+
      "WHERE 1 ) as sl "+where+" "+
      "ORDER BY createdAt DESC ";


  	if(req.body.limit && req.body.pagina){
      sql     += "LIMIT "+(req.body.limit*(req.body.pagina-1))+", "+req.body.limit;
    } else {
      sql     += "LIMIT 0"+", "+req.body.limit;
    }

    var userQueryAsync = Promise.promisify(Usuario.query);
    userQueryAsync(sql, [ ])
    .then(function(models) {
         var asyncs = [];
         var index = 0;
         response = models;
        if(models.length){
          for(var i = 0 in models){
            // pega os dados async
            asyncs.push(function(callback) {
               // pega os dados do perfil pelo post id
               UsuarioPerfil.findOne(models[index].usuario_perfil_id).populateAll().then(function(perfil){
                   response[index].usuario_perfil_id = perfil;
                   index++;
                   callback();
               });
           });
         }
         async.series(asyncs, function(err) {
           if (!err) {
               res.json({status:"sucesso", msg: "Resultado Encontrado", data:response});
           }else{
               res.json({status:"erro", msg: "Aconteceu um erro #usuario140"});
           }
         });
       }else{
          res.json({status:"erro", msg: "Nenhum resultado encontrado"});
       }
    });
  },
   /**
 * Pega os dados e atualiza
 *
 * (POST /usuario/atualizar)
 */
 atualizar: function(req, res) {
    if(!req.body.usuario_perfil_id)
      return res.json({status:"erro",msg: 'Parametros inválidos'});

    // faz o upload da imagem
    req.file('photo').upload({
      dirname: require('path').resolve(sails.config.appPath, 'assets/upload'),
      maxBytes: 10000000
    },function whenDone(err, uploadedFiles) {
      // se tiver arquivos faz o upload
      if (uploadedFiles.length){
        var name = uploadedFiles[0].fd.split('/');
        name = name[name.length-1];
        UsuarioAvatar.create({
          avatarFd: uploadedFiles[0].fd,
          mime:uploadedFiles[0].type,
          name:name
        }).exec(function (err, avatar) {
            // pega o avatar e deleta do banco
            UsuarioPerfil.findOne({id:req.body.usuario_perfil_id}).exec(function(err, dados){
              UsuarioAvatar.destroy({id: dados.avatar_id}).exec(function(err){});
            });
            UsuarioPerfil.update({id:req.body.usuario_perfil_id}, {avatar_id:avatar.id}).exec(function(err, update_perfil){
              // popula e retorna
              Usuario.findOne({usuario_perfil_id:req.body.usuario_perfil_id})
              .populateAll()
              .exec(function(err, dados){
                if(err){
                  return res.json({status:"erro",msg: 'Erro, algo inesperado aconteceu, tente novamente'});
                }else{
                  return res.json({
                      data:dados,
                      status:"sucesso"
                  });
                }
              });
            });
        });
      }else{
        var $objeto = {
          relacionamento_id:req.body.relacionamento_id,
          membro:req.body.membro == "true" ? 1 : null,
          lider:req.body.lider == "true" ? 1 : null,
          discipulador:req.body.discipulador == "true" ? 1 : null,
          ministro:req.body.pastor == "true" ? 1 : null,
          relacionando:req.body.relacionando == "true" ? 1 : null
        };
        if(req.body.data_nascimento)
          $objeto.data_nascimento = moment(req.body.data_nascimento, 'DD/MM/YYYY').format();
        if(req.body.email)
          $objeto.email = req.body.email

        UsuarioPerfil.update({id:req.body.usuario_perfil_id}, $objeto).exec(function(err, update_perfil){
          if(!err && update_perfil.length){
            // popula e retorna
            Usuario.findOne({id:req.body.usuario_id})
            .populateAll()
            .exec(function(err, dados){
              if(err){
                return res.json({status:"logout",msg: 'Erro, algo inesperado aconteceu, tente novamente'});
              }else{
                return res.json({
                    data:dados,
                    status:"sucesso"
                });
              }
            });
          }else{
            return res.json({status:"logout",msg: 'Aconteceu um erro #usuario179'});
          }
        });
      }
    });

  },
};