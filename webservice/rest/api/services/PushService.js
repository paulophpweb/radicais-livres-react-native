var http = require('http');

module.exports.notificar = function(usuario_perfil_id_to,my_usuario_id,titulo, mensagem, onSucess, onError, concat, type, post_id) {
   if(!usuario_perfil_id_to || !my_usuario_id){
     if(onError)
        onError("Usuário ID nao definido");
     return false;
   }
    
   Usuario.findOne({id:my_usuario_id})
   .populateAll()
  .exec(function(err, myusuario){
    if(err){
      if(onError)
        onError("Erro, algo inesperado aconteceu, tente novamente");
      return false; 
    }else{
         Usuario
         .findOne({usuario_perfil_id:usuario_perfil_id_to})
         .exec(function(err, tousuario){
            $objeto = {
              perfil_id_1: myusuario.usuario_perfil_id.id,
              perfil_id_2: usuario_perfil_id_to,
              texto:mensagem,
              type:type ? type : "",
              action_perfil_id:myusuario.usuario_perfil_id.id
            };
            if(post_id)
              $objeto.post_id = post_id;
            UsuarioNotificacao.create($objeto).exec(function(err, dados){
              if(!err){
                sails.sockets.blast('push_notificacao', {usuario_perfil_id:usuario_perfil_id_to});
                if(tousuario && tousuario.pushtoToken){
                    var message = {
                        "to": tousuario.pushtoToken+"",
                        "notification": {
                            "title": titulo,
                            "body": concat ? myusuario.usuario_perfil_id.nome+" "+mensagem : mensagem,
                            "sound": "default"
                        }
                    };

                    var postData = JSON.stringify(message);
                    var options = {
                        hostname: 'fcm.googleapis.com',
                        path: '/fcm/send',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json; charset=UTF-8',
                            'Authorization': 'key=AIzaSyC1WPk1Tzr6rnNcs3wd12sPFjK4ZBIk3Y4'
                        }
                    };

                    var requestHttp = http.request(options, function (res) {
                        res.setEncoding('utf8');
                        res.on('data', function (chunk) {
                          console.log(chunk);
                            if(onSucess)
                                onSucess(chunk);
                            return true;
                        });
                        res.on('error', function (e) {
                            if(onError)
                                onError(e.message);
                            return false;
                        });
                    });
                    requestHttp.write(postData);
                    requestHttp.end();

                    requestHttp.on('error', function (e) {
                        if(onError)
                            onError(e.message);
                        return false;
                    });
                }else{
                    if(onError)
                        onError("Token não encontrado");
                    return false;
                }
              }else{
                console.log(err);
              }
            });
            
         });
    }
   });

   
};


