module.exports = {
  
  schema: true,
  autoPK:true,
  tableName: 'app_usuario_config',
  
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true 
    },
    perfil_id: {
      type: 'integer',
      model: 'UsuarioPerfil',
      required: true
    },
    gps_privacidade: {
      type: 'integer'
    },
    corte_privacidade: {
      type: 'integer'
    },
    msg_privacidade: {
      type: 'integer'
    }
  }
};