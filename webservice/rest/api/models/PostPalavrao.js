module.exports = {
  
  schema: true,
  autoPK:true,
  tableName: 'app_post_palavrao',
  
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true 
    },
    nome: {
      type: 'string',
      required: true
    }
  }
};