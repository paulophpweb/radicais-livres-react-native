module.exports = {
  
  schema: true,
  autoPK:true,
  tableName: 'app_usuario_perfil',
  
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true 
    },
    relacionamento_id: {
      type: 'integer',
      model: 'UsuarioRelacionamento',
    },
    avatar_id: {
      type: 'integer',
      model: 'UsuarioAvatar',
    },
    nome: {
      type: 'string',
      required: true
    },
    data_nascimento: {
      type: 'date',
      required: true
    },
    genero: {
      type: 'string'
    },
    ministro: {
      type: 'integer'
    },
    lider: {
      type: 'integer'
    },
    discipulador: {
      type: 'integer'
    },
    membro: {
      type: 'integer'
    },
    relacionando: {
      type: 'integer'
    },
    estado_id: {
      type: 'integer'
    },
    cidade_id: {
      type: 'integer'
    },
    capa: {
      type: 'string'
    },
    status: {
      type: 'integer'
    }
  }
};