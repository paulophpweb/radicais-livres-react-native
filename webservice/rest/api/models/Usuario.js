/**
 * Users.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

// We don't want to store password with out encryption

module.exports = {
  
  schema: true,
  autoPK:true,
  tableName: 'app_usuario',
  
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true 
    },
    usuario_perfil_id: {
      type: 'integer',
      model: 'UsuarioPerfil',
    },
    email: {
      type: 'email',
      unique: true // Yes unique one
    },
    facebookToken: {
      type: 'string',
    },
    pushtoToken: {
      type: 'string',
    },
    facebookId: {
      type: 'string',
      required: true
    },
    latitude: {
      type: 'string'
    },
    longitude: {
      type: 'string'
    },
    auth_token:{
      type: 'string',
    }
  }
};