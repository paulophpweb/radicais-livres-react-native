module.exports = {
  
  schema: true,
  autoPK:true,
  tableName: 'app_post_video',
  
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true 
    },
    post_id: {
      type: 'integer',
      model: 'Post',
      required: true
    },
    videoFd: {
      type: 'string',
      required: true
    },
    mime:{
      type: 'string',
      required: true
    },
    name:{
      type: 'string',
      required: true
    },
  }
};