module.exports = {
  
  schema: true,
  autoPK:true,
  tableName: 'app_usuario_amigo',
  
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true 
    },
    perfil_id_1: {
      type: 'integer',
      model: 'UsuarioPerfil',
      required: true
    },
    perfil_id_2: {
      type: 'integer',
      model: 'UsuarioPerfil',
      required: true
    },
    status: {
      type: 'integer'
    },
    action_perfil_id: {
      type: 'integer',
      model: 'UsuarioPerfil',
      required: true
    }
  },
};