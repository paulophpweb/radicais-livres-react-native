module.exports = {
  
  schema: true,
  autoPK:true,
  tableName: 'app_usuario_avatar',
  
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true 
    },
    avatarFd: {
      type: 'string',
      required: true
    },
    mime:{
      type: 'string',
      required: true
    },
    name:{
      type: 'string',
      required: true
    }
  },
};