module.exports = {
  
  schema: true,
  autoPK:true,
  tableName: 'app_post_denuncia',
  
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true 
    },
    post_id: {
      type: 'integer',
      model: 'Post',
      required: true
    },
    usuario_perfil_id: {
      type: 'integer',
      model: 'UsuarioPerfil',
      required: true
    },
    texto: {
      type: 'string',
      required: true
    },
  }
};