module.exports = {
  
  schema: true,
  autoPK:true,
  tableName: 'app_post',
  
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true 
    },
    perfil_id: {
      type: 'integer',
      model: 'UsuarioPerfil'
    },
    video: {
      collection: 'PostVideo',
      via: 'post_id'
    },
    imagem: {
      collection: 'PostImagem',
      via: 'post_id'
    },
    like: {
      collection: 'PostLike',
      via: 'post_id'
    },
    texto: {
      type: 'string',
      required: true
    }
  }
};