module.exports = {
  
  schema: true,
  autoPK:true,
  tableName: 'app_usuario_relacionamento',
  
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true 
    },
    nome: {
      type: 'string',
      required: true
    }
  }
};