module.exports = {
  
  schema: true,
  autoPK:true,
  tableName: 'app_usuario_notificacao',
  
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true 
    },
    perfil_id_1: {
      type: 'integer',
      model: 'UsuarioPerfil'
    },
    perfil_id_2: {
      type: 'integer',
      model: 'UsuarioPerfil'
    },
    post_id: {
      type: 'integer',
      model: 'Post'
    },
    texto: {
      type: 'string',
      required: true
    },
    type: {
      type: 'string',
      required: true
    },
    status: {
      type: 'integer'
    },
    action_perfil_id: {
      type: 'integer',
      model: 'UsuarioPerfil'
    }
  },
};