module.exports = {
  
  schema: true,
  autoPK:true,
  tableName: 'app_post_publicidade',
  
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true 
    },
    post_id: {
      type: 'integer',
      model: 'Post',
      required: true
    },
    imagemFd: {
      type: 'string',
      required: true
    },
    mime:{
      type: 'string',
      required: true
    },
    name:{
      type: 'string',
      required: true
    },
  }
};