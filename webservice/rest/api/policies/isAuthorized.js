/**
 * isAuthorized
 *
 * @description :: Policy to check if user is authorized with JSON web token
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Policies
 */

module.exports = function (req, res, next) {
  var token;

  if (req.headers && req.headers.authorization) {
    var parts = req.headers.authorization.split(' ');
    if (parts.length == 2) {
      var scheme = parts[0],
        credentials = parts[1];

      if (/^Bearer$/i.test(scheme)) {
        token = credentials;
      }
    } else {
      return res.json(200, {status:"erro",msg: 'Formato de autorização inválido'});
    }
  } else if (req.body.token) {
    token = req.body.token;
    // We delete the token from param to not mess with blueprints
    delete req.query.token;
  } else {
    return res.json(200, {status:"erro",msg: 'Autorização de token inválida'});
  }

  if(!req.body.usuario_id){
      return res.json(200, {status:"erro",msg: 'Um erro de parâmetro aconteceu'});
  }
   Usuario.findOne(req.body.usuario_id)
   .exec(function(err, dados){
      if(dados){
        // update nos dados
        if(req.body.token_firebase)
          Usuario.update({id:req.body.usuario_id}, {pushtoToken:req.body.token_firebase}).exec(function(err, update_perfil){});

        // update nos dados
        if(req.body.latitude_usuario && req.body.longitude_usuario)
          Usuario.update({id:req.body.usuario_id}, {latitude:req.body.latitude_usuario,longitude:req.body.longitude_usuario}).exec(function(err, update_perfil){});
       
        // update o socket
        if (req.isSocket) {
          var socketId = sails.sockets.getId(req);
          if(socketId)
            sails.sockets.join(socketId);
        }
        jwToken.verify(token, function (err, token) {
          if (err) return res.json(200, {status:"logout",msg: 'Token inválido'});
          req.token = token; // This is the decrypted token or the payload you provided
          next();
        });
      }else{
        return res.json(200, {status:"logout",msg: 'Autorização de token inválida'});
      }
   });
  
};