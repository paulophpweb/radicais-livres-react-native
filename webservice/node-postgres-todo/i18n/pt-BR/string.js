module.exports = {
	/* Mensagens de erro genéricos */	
	erro: "Opa, precisamos orar mais, parece que temos um erro de conexão com o aplicativo",
		
	/* Mensagens de erro do perfil */
	erroSdPerfilCadastroNome: "O campo nome não pode ser vazio",
	erroSdPerfilCadastroSobrenome: "O campo sobrenome não pode ser vazio",
	erroSdPerfilCadastroEmail: "O campo email não pode ser vazio",
	erroSdPerfilCadastroEmailInvalido: "O email informado é inválido",
	erroSdPerfilCadastroEmailExiste: "O email informado já está cadastrado na nossa base de dados",		
	erroSdPerfilCadastroSenha: "O campo senha não pode ser vazio",	
	erroSdPerfilCadastroDataNascimento: "O campo data de nascimento não pode ser vazio",
	erroSdPerfilCadastroGenero: "O campo gênero não pode ser vazio",
	erroSdPerfilCadastroFoto: "Infelizmente não conseguimos subir esta foto",
	
	/* Dados atualizados */
	perfilAtualizado: "Seus dados foram atualizados com sucesso",
	senhaAtualizada: "Sua senha foi atualizada com sucesso",
	
	/* Logar no perfil */
	loginInvalido: "A senha que você digitou está incorreta. Tem certeza que o Caps Lock está desligado?",
	
	/* Recuperar senha perfil */
	enviarCodigoRecuperarSenha: "Foi enviado um código de confirmação para você alterar sua senha para o email",
	codigoInvalido: "Código inválido, tente novamente mais tarde",
	
	/* Template recuperar senha */
	bodyRecuperarSenha: "Alguém solicitou para redefinir sua senha no aplicativo Radicais Livres, utilize o código abaixo para redefinir sua senha:",
	bodyRecuperarSenhaAlerta: "Se você não solicitou uma nova senha, por favor ignore esta mensagem.",
	bodyRecuperarSenhaSubject: "Alguém solicitou uma nova senha para sua conta do Radicais Livres",	
	insertSdPerfilCadastro: "O perfil foi adicionado com sucesso",
	
	/* Insert denúncia */
	insertSdPerfilDenuncia: "A denúncia foi realizada com sucesso",	
	erroSdPerfilDenunciaDuplicada: "A denúncia deste perfil já se encontra cadastrada em nossa base de dados",
	
	/* Insert amigo */
	insertSdPerfilSolicitacao: "A solicitação foi enviada",
	amigoRemovido: "Amigo removido com sucesso",
	erroSdPerfilSolicitacaoDuplicada: "Esta solicitação já foi enviada e está aguardando aceitação",
	insertSdPerfilAmigo: "A amizade foi adicionada com sucesso",	
	erroSdPerfilAmizadeDuplicada: "Esta amizade já se encontra adicionada para o seu cadastro",
	
	/* Bloquear amigo */
	bloquearAmigo: "Esta amizade foi bloqueada",
	desbloquearAmigo: "A amizade foi desbloqueada",	
	mensagem_enviar: "Você recebeu uma nova mensagem.",	
	mensagem_enviar_subject: "Radicais Livres - Nova Mensagem.",
	
};