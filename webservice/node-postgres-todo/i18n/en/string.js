module.exports = {
	/* Mensagens de erro genéricos */	
	erro: "Ops, we need to pray more, it seems to have a connection error with the application",
		
	/* Mensagens de erro do perfil */
	erroSdPerfilCadastroNome: "The field name can not be empty",
	erroSdPerfilCadastroSobrenome: "The last name field can not be empty",
	erroSdPerfilCadastroEmail: "The email field can not be empty",
	erroSdPerfilCadastroEmailInvalido: "The email address you entered is invalid",
	erroSdPerfilCadastroEmailExiste: "The email address you entered is already registered in our database",		
	erroSdPerfilCadastroSenha: "The password field can not be empty",	
	erroSdPerfilCadastroDataNascimento: "The date of birth field can not be empty",
	erroSdPerfilCadastroGenero: "Field gender can not be empty",
	erroSdPerfilCadastroFoto: "Unfortunately we could not climb this photo",
	
	/* Dados atualizados */
	perfilAtualizado: "The data have been updated successfully",
	senhaAtualizada: "Your password has been successfully updated",
	
	/* Logar no perfil */
	loginInvalido: "The password you entered is incorrect. Are you sure that Caps Lock is off?",
	
	/* Recuperar senha perfil */
	enviarCodigoRecuperarSenha: "A confirmation code was sent to you to change your password to your email",
	codigoInvalido: "Invalid code, try again later",
	
	/* Template recuperar senha */
	bodyRecuperarSenha: "Someone has asked to reset your password in Radicais Livres application, use the following code to reset your password:",
	bodyRecuperarSenhaAlerta: "If you did not request a new password, please ignore this message.",
	bodyRecuperarSenhaSubject: "Someone requested a new password for your account Radicais Livres",	
	insertSdPerfilCadastro: "The profile has been successfully added",

	/* Insert denúncia */
	insertSdPerfilDenuncia: "The complaint was successful",
	erroSdPerfilDenunciaDuplicada: "The termination of this profile is already registered in our database",
	
	/* Insert amigo */
	insertSdPerfilSolicitacao: "The request was sent",
	amigoRemovido: "Amigo removido com sucesso",
	erroSdPerfilSolicitacaoDuplicada: "This request has already been sent and is awaiting acceptance",
	insertSdPerfilAmigo: "Friendship has been successfully added",	
	erroSdPerfilAmizadeDuplicada: "This friendship is already added to the register",
	
	/* Bloquear amigo */
	bloquearAmigo: "This friendship has been locked",
	desbloquearAmigo: "Friendship was unlocked",	
};