var moment = require('moment');

module.exports = function(app){	
	// remove espaços do início e fim da string
	var _trim = function (string){
		if(string == null || string == undefined){
			return '';
		}
		switch (typeof string) {			
			case 'object' :
			case 'array'  :
			case 'function':	
				return '';
		}
		string = ''+string;
		return string.replace(/^\s+|\s+$/gm,'');		
	};
	
	var _capitalize = function ($str){		
		 return $str.charAt(0).toUpperCase() + $str.slice(1).toLowerCase();		
	};
		
	var rand = function (){				
		return Math.floor(Math.random()*1000000);
	};
	
	var _uniqid = function () {
		return moment().format('YYYYmmddhmmss').toString()+"_"+rand()+"_"+rand()+"_"+rand();
	};
	
	var _empty = function (string) {
		return _trim(string) == '';
	};
		
	return {				
		trim:_trim,
		empty:_empty,
		capitalize:_capitalize,
		uniqid:_uniqid
	};
};
