//https://github.com/neoziro/push-notify
const pool = require('../library/db');
var path = require('path');
var connectionString = require(path.join(__dirname, '../', 'config'));


module.exports = function(app){

	// Método de envio
	var _send = function($obj){
		var results = [];

		if($obj.cadastro_id_amizade != null && $obj.cadastro_id_amizade != undefined ){

				pool.connect(function (err, client, done) {

					if(err) {
			        	app.uteis.httpError("pt", err, $res, "erro");
			        }

					if($obj.todos == null && $obj.todos == undefined){
						// Verificando se o udid corresponde ao celular deste usuário
						var query = client.query("SELECT sd_c.os, sd_c.tokenpush, sd_c.cadastro_id  FROM sd_perfil.sd_cadastro AS sd_c WHERE sd_c.cadastro_id = "+$obj.cadastro_id_amizade+" ", function(err, result){
				        	if(err) {
					        	app.uteis.httpError("pt", err, $res, "erro");
					        }
				        });
					}else if($obj.cadastro_id_amizade != null && $obj.cadastro_id_amizade != undefined ){
						// Verificando se o udid corresponde ao celular deste usuário
						var query = client.query("SELECT sd_c.os, sd_c.tokenpush, sd_c.cadastro_id FROM sd_perfil.sd_cadastro AS sd_c WHERE sd_c.cadastro_id IS NOT NULL AND sd_c.cadastro_id IN (SELECT cadastro_id_amizade FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id_amizade+") ", function(err, result){
				        	if(err) {
					        	app.uteis.httpError("pt", err, $res, "erro");
					        }
				        });
					}else{
						console.log("push erro");
					}

			        // Fluxo retorna uma linha de cada vez
			        query.on('row', function(row) {
			            results.push(row);
			        });

			        // Depois de todos os dados, encerro a conexão e retorno os dados
			        query.on('end', function() {
			            client.end();
			            if(!results.length > 0){
			            	return $obj.onComplete("Ops, aconteceu um erro inesperado ");
			            } else {
			            	var $dadosUsuario = results[0];
			            	var $cadastro_id_array_android = [];
			            	var $cadastro_id_array_ios = [];
			            	var $sucesso = true;
			            	if(results.length > 1){
			            		var $j;
			            		for($j in results){
			            			if(results[$j]["os"] == "android" && results[$j]["tokenpush"] != null && results[$j]["tokenpush"] != ""){
			            				$cadastro_id_array_android.push(results[$j]["tokenpush"]);
			            			}else if(results[$j]["os"] == "ios" && results[$j]["tokenpush"] != null && results[$j]["tokenpush"] != ""){
			            				$cadastro_id_array_ios.push(results[$j]["tokenpush"]);
			            			}
			            		}
			            	}else{
			            		if(results[0]["os"] == "android" && results[0]["tokenpush"] != null && results[0]["tokenpush"] != "")
			            			$cadastro_id_array_android.push(results[0]["tokenpush"]);
			            		if(results[0]["os"] == "ios" && results[0]["tokenpush"] != null && results[0]["tokenpush"] != "")
			            			$cadastro_id_array_ios.push(results[0]["tokenpush"]);
			            	}

			            	// se tiver o push envia
			            	if($cadastro_id_array_android.length >= 1){
			            		console.log("entrou no android para o push");

			            		var gcm = require('push-notify').gcm({
		            			  apiKey: 'AIzaSyBJm2Yxq5Z4TjGi6p8AGE3x_CChOQsmwEc',
		            			  retries: 1
		            			});

			            		gcm.send({
		            			  registrationId: $cadastro_id_array_android,
		            			  delayWhileIdle: true,
		            			  data: {
			    			        payload: {
			    			        	dados:$obj.retorno ? $obj.retorno : [],
					            	    controller:$obj.controller ? $obj.controller : "",
			    			        	android: {
			    			        		title : $obj.titulo,
			    	                    	alert : $obj.descricao,
			    	                    	vibrate: true,
			    	                    	sound: "notificacao",
			    	                    	icon:"appicon"
			    			        	}
			    			        }
		            			  }
		            			});

			            		gcm.on('transmitted', function (result, message, registrationId) {

			            		});

			            		gcm.on('transmissionError', function (error, message, registrationId) {
			            			$sucesso = false;
			            		});
			            	}

			            	if($cadastro_id_array_ios.length >= 1){
			            		// Create a new APN sender.
			            		console.log("entrou no ios para o push");

			            		var apn = require('apn');

											var options = {
											  token: {
											    key: path.join(__dirname, 'APNsAuthKey_Y26258K93B.p8'),
											    keyId: "Y26258K93B",
											    teamId: "5SSRSY6C8D"
											  },
											  production: true
											};
											var apnProvider = new apn.Provider(options);

											var note = new apn.Notification();
											// The topic is usually the bundle identifier of your application.
											note.topic = "com.radicaislivres.app";
											note.badge = 1;
											note.alert = $obj.titulo+' - '+$obj.descricao;
											note.sound = 'default';
											note.payload = {
													dados:$obj.retorno ? $obj.retorno : [],
													controller:$obj.controller ? $obj.controller : ""
											};
											apnProvider.send(note, $cadastro_id_array_ios).then( result => {
												  if(result.sent.length){
														$sucesso = true;
												  }else{
														$sucesso = false;
													}
											    console.log("sent:", result.sent.length);
											    //console.log("failed:", result.failed.length);
											    //console.log(result.failed);
											});

											apnProvider.shutdown();

											if($sucesso){
												return $obj.onComplete(true);
											}else{
												return $obj.onError("Erro ao transmitir o push.");
											}
			            	}
			            	if($sucesso){
			            		return $obj.onComplete(true);
			            	}else{
			            		return $obj.onError("Erro ao transmitir o push.");
			            	}

			            }
			        });

				});

		}

	};

	return {
		send: _send
	};
};
