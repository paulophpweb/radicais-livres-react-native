
module.exports = function(app){
	var fs = require('fs');
	var path = require('path');

	// funcao para retornar a internacionalizacao
	var _i18n = function(params, callback){
		// Linguagem
		var pathLang = path.join(__dirname, '../', 'i18n/'+params.lang+'/string.js');
		fs.exists(pathLang, function (exists) {
			if(!exists){
				pathLang = path.join(__dirname, '../', 'i18n/pt/string.js');
			}
		    var lang = require(pathLang);
		    callback(lang);
		});
	};

	// funcao para erro padrao
	var _httpError = function(lang, error, res, $keyError){
		console.log(error);return false;
		return res.json({msg:lang[$keyError], error:error, status:"erro"});
	};


	// valida se é email
	var _isEmail = function (email){
		return /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(email);
	};


	return {
		i18n:_i18n,
		httpError:_httpError,
		isEmail:_isEmail
	};
};
