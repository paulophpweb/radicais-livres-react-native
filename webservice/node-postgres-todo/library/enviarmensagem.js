var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');
var smtpTransport = require('nodemailer-smtp-transport');
var swig = require('swig');
var path = require('path');

module.exports = function(app){
	
	// Método de envio
	var _send = function($obj){			
		if($obj.email == "" || $obj.email == undefined){
			return $obj.onComplete("Ops, aconteceu um erro inesperado: O email não pode ser vazio");
		}
		
		// username + password 
		/*var options = {
		    auth: {
		        api_user: 'sitiodigital',
		        api_key: 'vilmar020406'
		    }
		};*/		    
		//var mailer = nodemailer.createTransport(sgTransport(options));
		var options = {
			host: 'mail.pauloph.a2hosted.com',
		    port: 465,
		    secure: true, // use SSL
		    auth: {
		        user: 'radicaislivres@pauloph.a2hosted.com',
		        pass: '123mudar'
		    },
		    tls: {
		        rejectUnauthorized:false
		    }, 
	    };   
		//var mailer = nodemailer.createTransport(sgTransport(options));
		var mailer = nodemailer.createTransport(options);
		
		// template
		var pathTemplate = path.join(__dirname, '../', 'views/templates/enviarmensagem.html');
		var template = swig.compileFile(pathTemplate);		
		var output = template({
			body: $obj.lang.mensagem_enviar,
			bodyalerta: $obj.texto
		});
		
		var email = {
			    to: $obj.email,
			    from: $obj.from,
			    subject: $obj.lang.mensagem_enviar_subject,				    
			    html: output
			};
			 
		mailer.sendMail(email, function(err, res) {
		    if (err) { 
		    	return $obj.onComplete("Ops, aconteceu um erro inesperado "+err);
		    } else {				    	
		    	return $obj.onComplete(true);
		    }
		    
		});

	};
			
	return {						
		send: _send
	};
};
