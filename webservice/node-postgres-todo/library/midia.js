var easyimg = require('easyimage');
var gm = require('gm');
var fs = require('fs');
var blobStream = require('blob-stream');

var path = require('path');
var pastaServidor = "upload";
var getCwd = pastaServidor;

module.exports = function(app){

	// Decode imagem com base 64
	var _decodeBase64Image = function (dataString) {
		  var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
		    response = {};

		  if (matches.length !== 3) {
		    return new Error('Invalid input string');
		  }

		  response.type = matches[1];
		  response.data = new Buffer(matches[2], 'base64');

		  return response;
		};

	// Método crop de imagem
	var _crop = function ($obj) {
		var origem = getCwd+$obj.filename;
		var pastaDestino = getCwd+$obj.destino;

		easyimg.info(origem).then(
		  function(file) {
			  easyimg.rescrop(
					    {
					        src:origem,
					        dst:pastaDestino+file.name,
					        cropwidth:$obj.largura,
					        cropheight:$obj.altura,
									width:$obj.largura,
					        height:$obj.altura
					    }
					).then(
							function(file){
								return $obj.onComplete(true);
							},
							function(err){
								return $obj.onComplete(false);
							}
					);
		  }, function (err) {
			  return $obj.onComplete(false);
		  }
		);
	};

	// Método resize de imagem
	var _resize = function ($obj) {
		var origem = getCwd+$obj.filename;
		var pastaDestino = getCwd+$obj.destino;

		easyimg.info(origem).then(
				function(file) {
					easyimg.resize(
							{
								src:origem,
								dst:pastaDestino+file.name,
								width:$obj.largura,
								height:$obj.altura
							}
					).then(
							function(file){
								return $obj.onComplete(true);
							},
							function(err){
								return $obj.onComplete(false);
							}
					);
				}, function (err) {
					return $obj.onComplete(false);
				}
		);
	};

	// Método resize de imagens
	var _cropall = function ($obj) {
		//var origem = getCwd+$obj.filename;
		//var pastaDestino = getCwd+$obj.destino;

		for(var i=0;i<$obj.tamanho.length;i++){
			console.log("i = "+ i);
		}

		/*easyimg.info(origem).then(
				function(file) {
					easyimg.resize(
							{
								src:origem,
								dst:pastaDestino+file.name,
								width:$obj.largura,
								height:$obj.altura
							}
					).then(
							function(file){
								return $obj.onComplete(true);
							},
							function(err){
								return $obj.onComplete(false);
							}
					);
				}, function (err) {
					return $obj.onComplete(false);
				}
		);*/
	};

	// Método para gerar icones
	var _geraicone = function ($obj) {
			if($obj.filename){
				var origem = getCwd+$obj.filename;
				var nomeimagemarray = $obj.filename.split("/");
				var nomeimagem = nomeimagemarray[nomeimagemarray.length - 1];
				var pastaDestino = getCwd+$obj.destino;
				fs.exists(pastaDestino+(nomeimagem), function(exists) {
				    if (!exists) {
				    	gm(origem)
						.resize(50, 50)
						.autoOrient()
						.quality(100)
						.borderColor("black")
						.border(3,3)
						/*.toBuffer('PNG',function (err, buffer) {
						  if (err){
							  return $obj.onComplete(false);
						  }else{
							  return $obj.onComplete(buffer.toString("base64"));

						  }
						});*/
						.write(pastaDestino+(nomeimagem), function (err) {
						  if (!err){ return $obj.onComplete((nomeimagem));}else{return $obj.onComplete(false);};
						});
				    }else{
				    	$obj.onComplete((nomeimagem));
				    }
				});
			}

	};

	// Método resize de imagem
	var _imagereturnbase64 = function ($obj) {
		// pega a imgem em base 64
		if($obj.dados.image){
			var base64image = $obj.dados.image;

			var b = new Buffer(base64image,'base64');

			/*lwip.open(b,"jpeg", function(err, image){
			  // checa erro
			  if(!err){
				  if($obj.dados.width && $obj.dados.height){
					  image.crop($obj.dados.width,$obj.dados.height, function(err, image){
						  if(!err){
						      // retorna o buffer
						      image.toBuffer('jpg',{quality: 90}, function(err, image){
						    	 return $obj.onComplete(image.toString("base64"));

						      });
						  }else{
							  return $obj.onComplete(false);
						  }
					  });
				  }else{
					  return $obj.onComplete(image.toString("base64"));
				  }

			   }else{
				   console.log(err);
				   return $obj.onComplete(false);
			   }

			});*/
		}else{
			return $obj.onComplete(false);
		}
	};

	return {
		decodeBase64Image: _decodeBase64Image,
		crop: _crop,
		resize: _resize,
		cropall: _cropall,
		imagereturnbase64: _imagereturnbase64,
		geraicone: _geraicone
	};
};
