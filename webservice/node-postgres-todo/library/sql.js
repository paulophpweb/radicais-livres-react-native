
module.exports = function(app){
	//var pg = require('pg');
	var path = require('path');
	const pool = require('../library/db');
	//var connectionString = require(path.join(__dirname, '../', 'config'));
	//var client = new pg.Client(connectionString);
	var _params = null;
	var _res = null;
	var _onComplete = null;
	var _boot = function($params, $res, $onComplete){

		_params = $params;
		_res = $res;
		_res = $onComplete;
		if($params.cadastro_id != null && $params.cadastro_id != '' && $params.udid != null && $params.udid != ''){
			var results = [];
			var results2 = [];

			pool.connect(function (err, client, done) {
				var $query = "";
				if(err) {
        	//app.uteis.httpError("pt", err, $res, "erro");
        }
        var sqlToken = "";
        // se tiver token id coloca a condicao
        if($params.tokenpush != null && $params.tokenpush != '' && $params.tokenpush != undefined){
        	sqlToken = " AND sd_c.tokenpush = '"+$params.tokenpush+"' ";
        }
        // atualiza o ultimo login
        if($params.ult_login != null && $params.ult_login != "" && $params.cadastro_id != null && $params.cadastro_id != ''){
        	$query += "UPDATE sd_perfil.sd_cadastro SET ult_login = '"+$params.ult_login+"', logado = 1 WHERE cadastro_id = "+$params.cadastro_id+"; ";
        }
        // atualiza a latitude
        if($params.lat != null && $params.lat != "" && $params.lng != null && $params.lng != ""){
        	$query += "UPDATE sd_perfil.sd_cadastro SET lat = '"+$params.lat+"', lng = '"+$params.lng+"', data_ult_localizacao = '"+$params.ult_login+"' WHERE cadastro_id = "+$params.cadastro_id+"; ";
        }
        var query_1 = client.query($query, function(err, result){});
        $query = "SELECT sd_c.cadastro_id, sd_c.udid FROM sd_perfil.sd_cadastro AS sd_c WHERE sd_c.cadastro_id = "+$params.cadastro_id+" AND sd_c.udid = '"+$params.udid+"' "+sqlToken;
        // Verificando se o udid corresponde ao celular deste usuário
				var query = client.query($query, function(err, result){
        	if(err) {
	        	app.uteis.httpError("pt", err, $res, "erro");
	        }
        });

        // Fluxo retorna uma linha de cada vez
        query.on('row', function(row) {
            results.push(row);
        });

        // Depois de todos os dados, encerro a conexão e retorno os dados
        query.on('end', function() {
            client.end();
            if(!results.length > 0){
            	return $res.json({status: "logout"});
            } else {
            		$onComplete();
            	return this;
            }
        });
		   });
		} else {
			$onComplete();
			return this;
		}
	};

	/*
	 * Metodo para retornar um ou varios registros
	 * onComplete:Function
	 * onError:Function
	 */
	var _query = function($obj){
		var results = [];
		app.uteis.i18n(_params, function(lang){
			pool.connect(function (err, client, done) {
		        // SQL Query > Selecionando dados
		        var query = client.query($obj.query, function(err, result){
		        	if(err) {
			        	if($obj.onError != null){
			        		if($obj.onErrorParams != null){
			        			$obj.onError.apply(this, [lang, err].concat($obj.onErrorParams));
			        		}else $obj.onError(lang, err);
			        	}
			        	else app.uteis.httpError(lang, err, _res, "erro");
			        }
		        });

		        // Fluxo retorna uma linha de cada vez
		        query.on('row', function(row) {
		            results.push(row);
		        });


		        // Depois de todos os dados, encerro a conexão e retorno os dados
		        query.on('end', function() {
		            client.end();
		            $obj.onComplete(lang, results);
		            //if($obj.onComplete != null)$obj.onComplete(lang, results);
		        	//else $obj.onComplete(lang, results);
		        });

		    });
		});
	};

	var _schema = function($obj, $array){
		var retorno = {};
		for(var a in $array){
			var b = $array[a];
			if($obj[b] != null){
				retorno[b] = $obj[b];
			}
		}
		//return $obj;
		return retorno;
	};

	// remove espaços do início e fim da string
	var _sqlFunction = function (string){
		return string = "_sqlFunction"+string;
	};

	return {query: _query, schema: _schema, boot:_boot, sqlFunction: _sqlFunction};
};
