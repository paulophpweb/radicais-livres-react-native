var moment = require('moment');
var fs = require('fs');
var path = require('path');

module.exports = function(app){

	var Sdevento = app.models.sdevento;
	var SdeventoPessoa = app.models.sdeventopessoa;

	var SdeventoController = {
		/*
		 * Action listar eventos
		 */
		get: function(req,res){
			// Método construtor
			Sdevento.boot(app, req.body, res,function(){
				var limit = req.body.limit;
				var offset = req.body.offset;
				var $estado_id = req.body.estado_id;
				var $cidade_id = req.body.cidade_id;
				var $titulo = req.body.titulo;
				var $is_root = req.body.is_root;
				$where = "";

				if($estado_id != null || (app.string.trim($estado_id) != '')){
					$where += " AND sd_es.estado_id = "+$estado_id+" ";
				}

				if($titulo != null || (app.string.trim($titulo) != '')){
					$where += " AND upper(remove_acento(sd_e.titulo)) like upper(remove_acento('%"+$titulo+"%')) ";
				}

				if($cidade_id != null || (app.string.trim($cidade_id) != '')){
					$where += " AND sd_e.cidade_id = "+$cidade_id+" ";
				}
				// Trazendo todos perfis
				Sdevento.get({
			    	onComplete:function(lang, data){
			    		if(!data){
			    			data = {};
			    		}
			    		return res.json({status: "sucesso", data: data});
			    	},
			    	cadastro_id: req.body.cadastro_id,
			    	order: "sd_e.data ASC",
			    	tipo: req.body.tipo,
			    	limit: limit,
				    offset:offset,
				    is_root:$is_root,
				    where:$where
			    });
			});
		},

		/*
		 * Action inserir perfil
		 */
		insert: function(req, res){
			var $cadastro_id_amizade = req.body.cadastro_id_amizade;
			var $cadastro_id = req.body.cadastro_id;
			var $notificacao = req.body.notificacao;
			var $os = req.body.os;

			req.body = app.sql.schema(req.body, ['lang', 'udid', 'titulo', 'data','data_fim', 'latitude', 'longitude', 'cidade_id', 'ativo', 'foto', 'endereco','sobre','cadastro_id','url']);
			console.log(req.body);
			Sdevento.boot(app, req.body, res, function(){
				delete req.body.udid;
				delete req.body.lang;
				//req.body.foto = req.files.foto;
				verificarFoto({
					onComplete: function(msgErro, filename){
						if(msgErro != null){
							return res.json({status: "erro", msg: msgErro});
						} else {
							// Gravando no banco de dados
							req.body.foto = "/images/sdevento/800x600/"+filename;
							Sdevento.insert({
								onComplete:function(lang, data){
									if(data.length > 0){
										app.enviarpush.send({
											onComplete: function(data2){
												if(data2){
													return res.json({status: "sucesso", msg: "Evento cadastrado com sucesso.", data: data});
												}else{
													return res.json({status: "erro", msg: "Um erro inesperado aconteceu ao enviar a notificação."});
												}
											},
											onError: function(msg){
												return res.json({status: "erro", msg: msg});
											},
											cadastro_id_amizade: $cadastro_id,
											titulo:"Radicais Livres",
											descricao:$notificacao,
											os:$os,
											controller:"/evento/procurar",
											todos:true
										});
									}else{
										return res.json({status: "erro", msg: "Erro inesperado."});
									}
								}
							});
						}
					},
					req: req,
					app: app
				});
			});

		},

		/*
		 * Ação de atualizar os dados do perfil
		 */
		update: function(req,res){
			req.body = app.sql.schema(req.body, ['lang', 'cadastro_id', 'evento_id', 'tipo']);
			Sdevento.boot(app, req.body, res, function(){
				delete req.body.lang;

				Sdevento.update({
					onComplete:function(lang, data){
						if(data.length > 0){
							return res.json({status: "alerta", msg: "Deu tudo certo"});
						} else {
							return res.json({status: "erro", msg: "Algo inesperado aconteceu ao atualizar os dados do perfil"});
						}
			    	},
			    	update: req.body,
			    	where: "evento_id = '"+req.body.evento_id+"'"
			    });
			});
		},

	};

	/*
	 * Verifica e envia a foto pro servidor
	 */
	var verificarFoto = function ($obj){
		// Gravando imagem na pasta
		var pastaServidor = "upload";
		var pastaPerfil = "/images/sdevento/800x600/";
		var arquivo = $obj.app.string.uniqid()+".jpg";
		var pathImage = pastaServidor+pastaPerfil+arquivo;

		if($obj.req.files.foto != undefined && $obj.req.files.foto != null){
			fs.readFile($obj.req.files.foto.path, function (err, data) {
				if(err != null){
					return $obj.onComplete(err, null);
				} else {
					fs.writeFile(pathImage, data, function(err) {

						if(err != null){
							return $obj.onComplete("Erro inesperado: Falha ao subir arquivo File", null);
						} else {
							// Eliminando arquivo obsoleto e retornando o arquivo
							fs.unlink($obj.req.files.foto.path, function(err){
								if(err != null){
									return $obj.onComplete("Erro inesperado: Falha ao eliminar arquivo obsoleto", null);
								} else {
									return $obj.onComplete(null, arquivo);
								}
							});
						}

					});
				}
			});
		} else {
			return $obj.onComplete("Erro inesperado: Nenhuma foto enviada para o método", null);
		}
	};

	return SdeventoController;

};
