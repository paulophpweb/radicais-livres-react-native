var moment = require('moment');
var fs = require('fs');
var path = require('path');

module.exports = function(app){

	var SdmanipulateimageController = {
		/*
		 * Action listar eventos
		 */
		resize: function(req,res){
			app.midia.imagereturnbase64({
				onComplete: function($obj){
					return res.json({status: "sucesso", data: $obj});
				},
				dados: req.body
			});
		}
	}

	return SdmanipulateimageController;

};
