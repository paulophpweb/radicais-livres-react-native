var moment = require('moment');
var fs = require('fs');
var path = require('path');

module.exports = function(app){

	var Sdmapa = app.models.sdmapa;
	var Sdperfilnotificacao = app.models.sdperfilnotificacao;
	var Sdevento = app.models.sdevento;
	var SdeventoPessoa = app.models.sdeventopessoa;
	var Sdperfiligreja = app.models.sdperfiligreja;

	var SdmapaController = {

		/*
		 * Action listar eventos
		 */
		getusuarioslatlng: function(req,res){
			// Método construtor
			Sdmapa.boot(app, req.body, res,function(){
				var limit = req.body.limit;
				var offset = req.body.offset;
				var $is_root = req.body.is_root;
				var $ult_login = req.body.ult_login;
				var $lat = req.body.lat;
				var $lng = req.body.lng;
				var $latitudeUm = req.body.latitudeUm;
				var $latitudeTres = req.body.latitudeTres;
				var $longitudeUm = req.body.longitudeUm;
				var $longitudeDois = req.body.longitudeDois;
				var $nome_pessoa = req.body.nome_pessoa;
				var $nome_evento = req.body.nome_evento;
				var $igreja_id = req.body.igreja_id;
				var $cadastro_id = req.body.cadastro_id;
				var $dados = [];
				// Trazendo todos perfis
				Sdmapa.getusuarioslatlng({
			    	onComplete:function(lang, data){
			    		var $retorno = true;
			    		if(!data.length){
			    			$dados = [{dados:[]}];
			    		}else{
			    			$dados =[{dados:data}];
				    		var $j = 0;

				    		for ($j in $dados[0]["dados"]){

				    			app.midia.geraicone({
				    				filename: $dados[0]["dados"][$j]["foto"],
				    				destino: "/images/sdperfilcadastro/icone/",
				    				onComplete: function(dadosmidia){
				    					console.log(dadosmidia,"acessou");
										if(!dadosmidia){
											$retorno = false;
										}
									}
				    			});
				    		}

			    		}

			    		if($retorno){

			    			Sdevento.boot(app, req.body, res,function(){
			    				Sdevento.get({
			    			    	onComplete:function(lang, data2){
			    			    		var $dadosrecebidos = data2;
			    			    		if(!data2.length){
			    			    			$dados.push({evento:[]});
			    			    		}else{
			    			    			$dados.push({evento:$dadosrecebidos});
			    			    		}



			    			    		Sdperfiligreja.boot(app, req.body, res,function(){
						    				Sdperfiligreja.get({
						    			    	onComplete:function(lang, data3){
						    			    		if(!data3.length){
						    			    			$dados.push({igreja:[]});
						    			    		}else{
						    			    			$dados.push({igreja:data3});
						    			    		}
									    			return res.json({status: "sucesso", data: $dados});
						    			    	},
						    			    	cadastro_id: req.body.cadastro_id,
						    			    	order: "sd_i.igreja_id DESC",
						    				    is_root:$is_root,
						    				    latitudeUm:$latitudeUm,
						    				    latitudeTres:$latitudeTres,
						    				    longitudeUm:$longitudeUm,
						    				    longitudeDois:$longitudeDois,
						    				    igreja_id:$igreja_id
						    			    });
						    			});
			    			    	},
			    			    	cadastro_id: req.body.cadastro_id,
			    			    	order: "sd_e.data ASC",
			    				    is_root:$is_root,
			    				    latitudeUm:$latitudeUm,
			    				    latitudeTres:$latitudeTres,
			    				    longitudeUm:$longitudeUm,
			    				    longitudeDois:$longitudeDois,
			    				    nome_evento:$nome_evento,
			    				    tipo:"proximo"
			    			    });
			    			});
			    		}else{
			    			//return res.json({status: "erro", msg: "Erro inesperado: Falha ao gerar os icones do mapa"});
			    			return res.json({status: "sucesso", data: data});
			    		}

			    	},
			    	cadastro_id: req.body.cadastro_id,
			    	order: "sd_c.cadastro_id DESC",
			    	limit: limit,
				    offset:offset,
				    is_root:$is_root,
				    ult_login:$ult_login,
				    lat:$lat,
				    lng:$lng,
				    latitudeUm:$latitudeUm,
				    latitudeTres:$latitudeTres,
				    longitudeUm:$longitudeUm,
				    longitudeDois:$longitudeDois,
				    nome_pessoa:$nome_pessoa,
				    igreja_id:$igreja_id
			    });
			});
		},

	};

	return SdmapaController;

};
