var moment = require('moment');
var momentTimezone = require('moment-timezone');
var fs = require('fs');
var path = require('path');

module.exports = function(app){

	var Sdperfilcadastro = app.models.sdperfilcadastro;
	var Sdtimelinepost = app.models.sdtimelinepost;
	var Sdperfiligreja = app.models.sdperfiligreja;
	var Sdperfilformacao = app.models.sdperfilformacao;
	var Sdperfilrelacionamento = app.models.sdperfilrelacionamento;
	var Sdperfildenunciacadastro = app.models.sdperfildenunciacadastro;
	var Sdperfilcadastroamizade = app.models.sdperfilcadastroamizade;
	var Sdperfilcadastrosolicitacao = app.models.sdperfilcadastrosolicitacao;
	var Sdperfilcadastrobloqueio = app.models.sdperfilcadastrobloqueio;
	var Sdperfilmensagem = app.models.sdperfilmensagem;
	var Sdperfilnotificacao = app.models.sdperfilnotificacao;
	var Sdperfilmotivodenunciacadastro = app.models.sdperfilmotivodenunciacadastro;

	var SdperfilcadastroController = {

		/*
		 * Ação de marcar o perfil como deslogado
		 */
		deslogar: function(req,res){
			req.body = app.sql.schema(req.body, ['lang', 'cadastro_id']);
			Sdperfilcadastro.boot(app, req.body, res, function(){
				delete req.body.lang;
				req.body.logado = '0';

				Sdperfilcadastro.update({
					onComplete:function(lang, data){
						if(data.length > 0){
							return res.json({status: "alerta", msg: "Deslogou."});
						} else {
							return res.json({status: "erro", msg: "Algo inesperado aconteceu ao deslogar."});
						}
			    	},
			    	update: req.body,
			    	where: "cadastro_id = '"+req.body.cadastro_id+"'"
			    });
			});
		},
		/*
		 * Action listar perfis
		 */
		get: function(req,res){
			// Montando o where conforme requisição de busca
			var where = "sd_c.cadastro_id IS NOT NULL AND sd_c.excluido = 0";

			var cadastro_id_pessoa = req.body.cadastro_id_pessoa;
			var cadastro_id = req.body.cadastro_id;
			var is_root = req.body.is_root;
			if(cadastro_id_pessoa != '' && cadastro_id_pessoa != undefined){
				where+= " AND sd_c.cadastro_id = "+cadastro_id_pessoa+"";
			} else {
				var cadastro_id = req.body.cadastro_id;
				if(cadastro_id != '' && cadastro_id != undefined){
					where+= " AND sd_c.cadastro_id = "+cadastro_id+"";
				}
			}

			var nome = req.body.nome;
			if(nome != '' && nome != undefined){
				where+= " AND (upper(remove_acento(sd_c.nome)) like upper(remove_acento('%"+nome+"%'))";
				where+= " OR upper(remove_acento(sd_c.sobrenome)) like upper(remove_acento('%"+nome+"%')))";
			}

			var idade = req.body.idade;
			if(idade != '' && idade != undefined){
				where+= " AND extract(year from age(sd_c.data_nascimento)) = '"+idade+"'";
			}

			var relacionamento_id = req.body.relacionamento_id;
			if(relacionamento_id != '' && relacionamento_id != undefined){
				where+= " AND sd_c.relacionamento_id = '"+relacionamento_id+"'";
			}

			var cidade_id = req.body.cidade_id;
			if(cidade_id != '' && cidade_id != undefined){
				where+= " AND sd_c.igreja_id IN (SELECT igreja_id FROM sd_perfil.sd_igreja WHERE cidade_id = '"+cidade_id+"')";
			}

			var igreja_id = req.body.igreja_id;
			if(igreja_id != '' && igreja_id != undefined){
				where+= " AND sd_c.igreja_id = '"+igreja_id+"'";
			}

			if(req.body.timeline == "true"){
				// Método construtor Timeline
				Sdtimelinepost.boot(app, req.body, res, function(){
					// Trazendo todos os posts da timeline
					Sdtimelinepost.get({
				    	onComplete:function(lang, dataTimeline){
				    		var dadosTimeline = dataTimeline;

				    		// Método construtor perfil
							Sdperfilcadastro.boot(app, req.body, res, function(){
								// Trazendo todos perfis
								Sdperfilcadastro.get({
							    	onComplete:function(lang, data){
							    		return res.json({status: "sucesso", data: {perfil: data, timeline: dadosTimeline}});
							    	},
							    	where: where,
							    	order: "sd_c.cadastro_id DESC",
							    	cadastro_id:cadastro_id,
							    	is_root:is_root,
							    	cadastro_id_pessoa:cadastro_id_pessoa,
							    });
							});
				    	},
				    	cadastro_id: cadastro_id,
						cadastro_id_pessoa: cadastro_id_pessoa,
				    	order: "sd_p.post_id DESC"
				    });
				});
			}else{
				// Método construtor
				Sdperfilcadastro.boot(app, req.body, res,function(){

					// Trazendo todos perfis
					Sdperfilcadastro.get({
				    	onComplete:function(lang, data){
				    		if(!data){
				    			data = {};
				    		}
				    		return res.json({status: "sucesso", data: data});
				    	},
				    	where: where,
				    	is_root:is_root,
				    	order: "sd_c.cadastro_id DESC"
				    });
				});

		    }
		},

		/*
		 * Action para procurar amizades
		 */
		findamizades: function(req,res){

			// Montando o where conforme requisição de busca
			var where = "sd_c.cadastro_id IS NOT NULL AND sd_c.excluido = 0";
			var busca = false;
			var is_root = req.body.is_root;

			/*var cadastro_id = req.body.cadastro_id;
			if(!app.string.empty(cadastro_id)){
				// Retornando os que não são amigos nem ele enviou solicitação de amizade
				where += " AND sd_c.cadastro_id <> "+cadastro_id+"";
				where += " AND sd_c.cadastro_id NOT IN (SELECT cadastro_id_solicitado FROM sd_perfil.sd_cadastro_solicitacao WHERE cadastro_id = "+cadastro_id+")";
				where += " AND sd_c.cadastro_id NOT IN (SELECT cadastro_id FROM sd_perfil.sd_cadastro_solicitacao WHERE cadastro_id_solicitado = "+cadastro_id+")";
				busca = true;
			}*/

			var nome = req.body.nome;
			if(!app.string.empty(nome)){
				var termos = nome.split(" ");
				where+= " AND (";
				where+= "upper(remove_acento(sd_c.nome)) like upper(remove_acento('%"+termos[0]+"%'))";

				var aux = 0;
				var termoSobrenome = false;
				for(obj in termos) {
					if(aux > 0 && !termoSobrenome){
						where+= " AND upper(remove_acento(sd_c.sobrenome)) like upper(remove_acento('%"+termos[obj]+"%'))";
						termoSobrenome = true;
					}
					aux++;
				}

				where+= " )";
				busca = true;
			}

			var idade = req.body.idade;
			if(!app.string.empty(idade)){
				where+= " AND extract(year from age(sd_c.data_nascimento)) = '"+idade+"'";
				busca = true;
			}

			var relacionamento_id = req.body.relacionamento_id;
			if(!app.string.empty(relacionamento_id)){
				where+= " AND sd_c.relacionamento_id = '"+relacionamento_id+"'";
				busca = true;
			}

			var estado_id = req.body.estado_id;
			if(!app.string.empty(estado_id)){
				where+= " AND sd_es.estado_id = '"+estado_id+"'";
				busca = true;
			}

			var cidade_id = req.body.cidade_id;
			if(!app.string.empty(cidade_id)){
				//where+= " AND sd_c.igreja_id IN (SELECT igreja_id FROM sd_perfil.sd_igreja WHERE cidade_id = '"+cidade_id+"')";
				where+= " AND sd_c.cidade_id = '"+cidade_id+"'";
				busca = true;
			}

			var igreja_id = req.body.igreja_id;
			if(!app.string.empty(igreja_id)){
				where+= " AND sd_c.igreja_id = '"+igreja_id+"'";
				busca = true;
			}

			var funcao_id = req.body.funcao_id;
			if(!app.string.empty(funcao_id)){
				where+= " AND sd_c.funcao_id = '"+funcao_id+"'";
				busca = true;
			}

			var limit = req.body.limit;
			var offset = req.body.offset;
			if(busca){
				// Método construtor
				Sdperfilcadastro.boot(app, req.body, res, function(){
					// Trazendo todos perfis
					Sdperfilcadastro.findAmizades({
				    	onComplete:function(lang, data){
				    		return res.json({status: "sucesso", data: data});
				    	},
				    	is_root:is_root,
				    	where: where,
				    	order: "sd_c.cadastro_id DESC",
				    	limit: limit,
				    	offset:offset
				    });
				});

			} else {
				return res.json({status: "erro", msg: "Por favor informe ao menos um parâmetro de busca"});
			}

		},

		/*
		 * Action listar sobre
		 */
		sobre: function(req,res){
			// Montando o where conforme requisição de busca
			var where = "sd_c.cadastro_id IS NOT NULL AND sd_c.excluido = 0";

			var cadastro_id = req.body.cadastro_id;
			if(cadastro_id != '' && cadastro_id != undefined){
				where+= " AND sd_c.cadastro_id = "+cadastro_id+"";
			} else {
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido"});
			}

			// Método construtor
			Sdperfilcadastro.boot(app, req.body, res, function(){
				// Trazendo todos perfis
				Sdperfilcadastro.sobre({
			    	onComplete:function(lang, data){
			    		return res.json({status: "sucesso", data: data});
			    	},
			    	where: where
			    });
			});

		},

		/*
		 * Action listar sugestão de perfis
		 */
		getsugestao: function(req,res){
			var cadastro_id = req.body.cadastro_id;
			var is_root = req.body.is_root;
			if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
				return res.json({status: "erro", msg: "Erro inesperado, parâmetro de busca não postado"});
			}

			// Método construtor
			Sdperfilcadastro.boot(app, req.body, res, function(){
				// Trazendo todos perfis
				Sdperfilcadastro.getsugestao({
			    	onComplete:function(lang, data){
			    		return res.json({status: "sucesso", data: data});
			    	},
			    	cadastro_id: cadastro_id,
			    	is_root:is_root,
			    	order: "cadastro_id DESC"
			    });
			});
		},


		/*
		 * Action listar mensagens
		 */
		getmensagem: function(req,res){
			var where = "sp_m.cadastro_id IS NOT NULL AND sp_c.excluido = 0";

			var cadastro_id = req.body.cadastro_id;
			var $ult_login = req.body.ult_login;
			if(cadastro_id != '' && cadastro_id != undefined){
				where+= " AND sp_m.cadastro_id = "+cadastro_id+"";
			} else {
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido"});
			}

			var limit = req.body.limit;
			var offset = req.body.offset;
			// Método construtor
			Sdperfilmensagem.boot(app, req.body, res, function(){
				// Trazendo todos perfis
				Sdperfilmensagem.get({
			    	onComplete:function(lang, data){
			    		return res.json({status: "sucesso", data: data});
			    	},
			    	where: where,
			    	order: "data_cadastro DESC",
			    	limit: limit,
				    offset:offset,
				    ult_login:$ult_login,
				    cadastro_id:cadastro_id
			    });
			});
		},

		/*
		 * Action listar mensagens
		 */
		getnotificacao: function(req,res){
			var where = "sp_n.cadastro_id IS NOT NULL AND sp_c.excluido = 0 AND sp_cc.excluido = 0";

			var cadastro_id = req.body.cadastro_id;
			var $ult_login = req.body.ult_login;
			if(cadastro_id != '' && cadastro_id != undefined){
				where+= " AND sp_n.cadastro_id = "+cadastro_id+"";
			} else {
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido"});
			}

			var limit = req.body.limit;
			var offset = req.body.offset;
			// Método construtor
			Sdperfilnotificacao.boot(app, req.body, res, function(){
				// Trazendo todos perfis
				Sdperfilnotificacao.get({
			    	onComplete:function(lang, data){
			    		return res.json({status: "sucesso", data: data});
			    	},
			    	where: where,
			    	order: "sp_n.data_cadastro DESC",
			    	limit: limit,
				    offset:offset,
				    cadastro_id:cadastro_id,
				    ult_login:$ult_login
			    });
			});
		},

		/*
		 * Action listar mensagens
		 */
		getmotivodenuncia: function(req,res){

			// Método construtor
			Sdperfilmotivodenunciacadastro.boot(app, req.body, res, function(){
				// Trazendo todos perfis
				Sdperfilmotivodenunciacadastro.get({
			    	onComplete:function(lang, data){
			    		return res.json({status: "sucesso", data: data});
			    	}
			    });
			});
		},

		/*
		 * Action inserir comentário
		 */
		enviarmensagem: function(req, res){
			$notificacao = req.body.notificacao;
			$cadastro_id = req.body.cadastro_id;
			$cadastro_id_amizade = req.body.cadastro_id_amizade;
			$os = req.body.os;
			// Validar post
			var remetente = req.body.nome;

			var remetente = req.body.nome;
			if(remetente == null || (app.string.trim(remetente) == '')){
				return res.json({status: "erro", msg: "Erro inesperado: Validação #nome"});
			}
			var email = req.body.email;
			if(email == null || (app.string.trim(email) == '')){
				return res.json({status: "erro", msg: "Erro inesperado: Validação #email"});
			}
			req.body = app.sql.schema(req.body, ['cadastro_id', 'texto', 'cadastro_id_amizade']);

			Sdperfilmensagem.boot(app, req.body, res, function(){

				app.uteis.i18n(req.body, function(lang){
					validateFormMensagem({
						onComplete: function(data){
							if(data){
								return res.json({status: "erro", msg: data});
							} else {
								//Inserindo o comentário
								Sdperfilmensagem.insert({
							    	onComplete:function(lang, data){
							    		//Enviando email
										/*app.enviarmensagem.send({
											onComplete: function(data){
												if(!data){
													return res.json({status: "erro", msg: "Algo inesperado aconteceu ao enviar o email alertando nova mensagem"});
												}
											},
											email: email,
											from: "corteradicaislivres@gmail.com",
											lang: lang,
											texto: "De: "+remetente+" - "+req.body.texto
										});*/
										if($cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
						    			   //Enviando push
											app.enviarpush.send({
												onComplete: function(data2){
													if(data2){
														if($cadastro_id != null && $cadastro_id != undefined && $cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
															Sdperfilnotificacao.boot(app, {cadastro_id:$cadastro_id,cadastro_id_amizade:$cadastro_id_amizade,texto:"Te enviou uma mensagem: "+req.body.texto+""}, res, function(){
																Sdperfilnotificacao.insert({
																	onComplete: function(data3){
																		// retorna os dados
																	}
																});
															});
														}
														return res.json({status: "sucesso", data: data});
													}else{
														return res.json({status: "sucesso", data: data});
													}
												},
												cadastro_id_amizade: $cadastro_id,
												titulo:"Radicais Livres",
												descricao:$notificacao,
												os:$os
											});
										}else{
											return res.json({status: "sucesso", data: data});
										}

							    	}
							    });
							}
						},
						params: req.body,
						app: app,
						res: res,
						lang: lang
					});
				});
			});
		},

		/*
		 * Action remover mensagem
		 */
		removermensagem: function(req, res){
			$notificacao = req.body.notificacao;
			$cadastro_id = req.body.cadastro_id;
			$cadastro_id_amizade = req.body.cadastro_id_amizade;
			$os = req.body.os;

			var cadastro_id = req.body.cadastro_id;
			if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
				return res.json({status: "erro", msg: "Erro inesperado: Validação #cadastro_id"});
			}
			var mensagem_id = req.body.mensagem_id;
			if(mensagem_id == null || (app.string.trim(mensagem_id) == '')){
				return res.json({status: "erro", msg: "Erro inesperado: Validação #mensagem_id"});
			}
			req.body = app.sql.schema(req.body, ['cadastro_id', 'mensagem_id']);

			Sdperfilmensagem.boot(app, req.body, res, function(){

				app.uteis.i18n(req.body, function(lang){
					validateFormMensagemExcluir({
						onComplete: function(data){
							if(data){
								return res.json({status: "erro", msg: data});
							} else {
								//Inserindo o comentário
								Sdperfilmensagem.remover({
							    	onComplete:function(lang, data){
							    		if($cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
						    			   //Enviando push
											app.enviarpush.send({
													onComplete: function(data2){
														if(data2){
															return res.json({status: "sucesso", msg: "Mensagem deletado com sucesso!", data: data});
														}else{
															return res.json({status: "sucesso", msg: "Mensagem deletado com sucesso!", data: data});
														}
													},
													cadastro_id_amizade: $cadastro_id_amizade,
													titulo:"Radicais Livres",
													descricao:$notificacao,
													os:$os
												});
											}else{
												return res.json({status: "sucesso", msg: "Mensagem deletado com sucesso!", data: data});
											}

							    	},
							    where: "mensagem_id = '"+mensagem_id+"' AND cadastro_id = '"+cadastro_id+"'"
							    });
							}
						},
						params: req.body,
						app: app,
						res: res,
						lang: lang
					});
				});
			});
		},

		/*
		 * Action listar sugestão de perfis
		 */
		getsugestao2: function(req,res){
			var cadastro_id = req.body.cadastro_id;
			if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
				return res.json({status: "erro", msg: "Erro inesperado, parâmetro de busca não postado"});
			}

			// Método construtor
			Sdperfilcadastro.boot(app, req.body, res, function(){
				// Trazendo todos perfis
				Sdperfilcadastro.getsugestao2({
			    	onComplete:function(lang, data){
			    		return res.json({status: "sucesso", data: data});
			    	},
			    	cadastro_id: cadastro_id,
			    	order: "cadastro_id DESC"
			    });
			});

		},

		/*
		 * Action listar solicitações e sugestões
		 */
		getsolicitacaosugestao: function(req,res){
			var cadastro_id = req.body.cadastro_id;
			var is_root = req.body.is_root;
			if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
				return res.json({status: "erro", msg: "Erro inesperado, parâmetro de busca não postado"});
			}

			// Método construtor
			Sdperfilcadastro.boot(app, req.body, res, function(){
				// Trazendo todos perfis
				if(is_root == 0){
					Sdperfilcadastro.getsolicitacaosugestao({
				    	onComplete:function(lang, data){
				    		return res.json({status: "sucesso", data: data});
				    	},
				    	cadastro_id: cadastro_id,
				    	is_root:is_root,
				    	order: "cadastro_id DESC"
				    });
				}else{
					return res.json({status: "sucesso", data: []});
				}
			});

		},

		/*
		 * Action listar sugestão de perfis
		 */
		listaramigos: function(req,res){
			var cadastro_id = req.body.cadastro_id;
			var $condicao = "";
			var is_root = req.body.is_root;
			if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
				return res.json({status: "erro", msg: "Erro inesperado, parâmetro de busca não postado"});
			}

			// Quantidade de itens
			var limit = req.body.limit;
			var offset = req.body.offset;
			if(is_root == 1){
				$condicao = "sd_c.excluido = 0";
			}else{
				$condicao = "sd_ca.cadastro_id = '"+cadastro_id+"' AND sd_c.excluido = 0 ";
			}
			// Método construtor
			Sdperfilcadastroamizade.boot(app, req.body, res, function(){
				// Trazendo todos amigos
				Sdperfilcadastroamizade.getamizades({
					onComplete:function(lang, data){
						return res.json({status: "sucesso", data: data});
					},
					is_root: is_root,
					cadastro_id: cadastro_id,
					where: $condicao,
					order: "sd_c.cadastro_id DESC",
					limit: limit,
					offset: offset
				});
			});

		},

		/*
		 * Action listar igrejas
		 */
		getigreja: function(req,res){
			var texto = req.body.texto;
			var limit = req.body.limit;
			if(limit == null){
				if(texto == null || (app.string.trim(texto) == '')){
					return res.json({status: "erro", msg: "Erro inesperado, parâmetro de busca não postado"});
				}
			}

			var where = "";
			if(limit == null){
				where+= " AND (upper(remove_acento(sd_i.nome)) like upper(remove_acento('%"+texto+"%'))";
				where+= " OR upper(remove_acento(sd_i.chaves_de_busca)) like upper(remove_acento('%"+texto+"%')))";
				where+= " OR sd_i.igreja_id = 9";
			}


			// Método construtor
			Sdperfiligreja.boot(app, req.body, res, function(){
				// Trazendo todos perfis
				Sdperfiligreja.get({
					onComplete:function(lang, data){
						return res.json({status: "sucesso", data: data});
					},
					where: where,
					order: "sd_i.igreja_id=9 DESC, sd_i.igreja_id DESC",
					limit:limit
				});
			});


		},

		/*
		 * Action listar formações
		 */
		getformacao: function(req,res){
			var texto = req.body.texto;
			var limit = req.body.limit;
			if(limit == null){
				if(texto == null || (app.string.trim(texto) == '')){
					return res.json({status: "erro", msg: "Erro inesperado, parâmetro de busca não postado"});
				}
			}

			var where = "formacao_id IS NOT NULL";
			if(limit == null){
				where+= " AND upper(remove_acento(nome)) like upper(remove_acento('%"+texto+"%'))";
			}

			// Método construtor
			Sdperfilformacao.boot(app, req.body, res,function(){
				// Trazendo todos perfis
				Sdperfilformacao.get({
					onComplete:function(lang, data){
						return res.json({status: "sucesso", data: data});
					},
					where: where,
					order: "formacao_id DESC",
					limit: limit
				});
			});
		},

		/*
		 * Action listar relacionamentos
		 */
		getrelacionamento: function(req,res){
			var where = "relacionamento_id IS NOT NULL";

			var texto = req.body.texto;
			if(texto != null && (app.string.trim(texto) != '')){
				where+= " AND upper(remove_acento(nome)) like upper(remove_acento('%"+texto+"%'))";
			}

			// Método construtor
			Sdperfilrelacionamento.boot(app, req.body, res,function(){
				// Trazendo todos perfis
				Sdperfilrelacionamento.get({
					onComplete:function(lang, data){
						return res.json({status: "sucesso", data: data});
					},
					where: where,
					order: "relacionamento_id DESC"
				});
			});


		},

		/*
		 * Action listar relacionamentos do cadastro
		 */
		getrelacionamentocadastro: function(req,res){
			var limit = req.body.limit;
			if(req.body.cadastro_id == null || (app.string.trim(req.body.cadastro_id) == '')){
				return res.json({status: "alerta", msg: "Erro inesperado: Parâmetro cadastro_id indefinido"});
			}

			if(req.body.genero == null || (app.string.trim(req.body.genero) == '')){
				return res.json({status: "alerta", msg: "Erro inesperado: Parâmetro genero indefinido"});
			}

			var where = "sd_c.genero <> '"+req.body.genero+"'";
			where += " AND (sd_c.cadastro_id_relacionamento IS NULL OR sd_c.cadastro_id_relacionamento = '"+req.body.cadastro_id+"' ) ";
			where += " AND sd_ca.cadastro_id = '"+req.body.cadastro_id+"'";

			// Método construtor
			Sdperfilcadastroamizade.boot(app, req.body, res,function(){
				// Trazendo todos amigos
				Sdperfilcadastroamizade.getamizades({
					onComplete:function(lang, data){
						return res.json({status: "sucesso", data: data});
					},
					where: where,
					order: "sd_ca.cadastro_id DESC",
					limit:limit
				});
			});

		},

		/*
		 * Método para validar o código de recuperação de senha
		 */
		checarcodigo: function(req,res){

			if(req.body.codigo_alterar_senha == null || (app.string.trim(req.body.codigo_alterar_senha) == '')){
				return res.json({status: "alerta", msg: "Erro inesperado: Parâmetro de requisição indefinido"});
			}

			if(req.body.email == null || (app.string.trim(req.body.email) == '')){
				return res.json({status: "alerta", msg: "Erro inesperado: Parâmetro de requisição indefinido"});
			}

			var where = "sd_c.codigo_alterar_senha = '"+req.body.codigo_alterar_senha+"' AND sd_c.email = '"+req.body.email+"'";

			// Método construtor
			Sdperfilcadastro.boot(app, req.body, res,function(){
				// Trazendo todos perfis
				Sdperfilcadastro.get({
					onComplete:function(lang, data){
						if(data.length > 0){
							return res.json({status: "sucesso", data:{cadastro_id: data[0].cadastro_id}});
						} else {
							return res.json({status: "erro", msg: lang.codigoInvalido});
						}
					},
					where: where,
					order: "sd_c.cadastro_id DESC"
				});
			});

		},

		/*
		 * Action inserir perfil
		 */
		insert: function(req, res){
			/*
			var ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
			if(ip != "177.41.75.157"){
				return res.json({status: "erro", msg: "Olá, percebemos que você baixou uma versão BETA do nosso app, o mesmo ainda não está liberado e está sendo implementado e corrigido. O Lançamento oficial do mesmo está agendado para a próxima semana."});
			}
			*/
			console.log('vai cadastrar',req.body);
			// Validar post
			if(!req.body.data_nascimento || req.body.data_nascimento == undefined){
				req.body.data_nascimento = moment().format('YYYY-MM-DD').toString();
				if(req.body.lang == "pt")
					req.body.data_nascimento = moment().format('DD/MM/YYYY').toString();
			}
			req.body = app.sql.schema(req.body, ['lang', 'udid', 'nome', 'sobrenome', 'email', 'senha', 'data_nascimento', 'genero', 'foto', 'igreja_id', 'relacionamento_id', 'formacao_id', 'cadastro_id_relacionamento', 'profissao_id','os','tokenpush','cidade_id']);
			Sdperfilcadastro.boot(app, req.body, res, function(){
				app.uteis.i18n(req.body, function(lang){
					validateFormCadastro({
						onComplete: function(data){
							$lang = req.body.lang;
							delete req.body.lang;
							if(data){
								return res.json({status: "erro", msg: data});
							} else {
								//Tratando os dados
								if($lang == "pt"){
									req.body.data_nascimento = moment(req.body.data_nascimento, 'DD/MM/YYYY').format('YYYY-MM-DD').toString();
								}
								req.body.ativo = true;
								req.body.genero = app.string.capitalize(req.body.genero);
								req.body.senha = app.sql.sqlFunction("MD5('"+req.body.senha+"')");

								// Se não tiver foto já cadastro aqui...
								var foto = req.files.foto;
								if(foto == undefined){
									foto = req.body.foto;
								}
								if(foto == null || foto == undefined){
									Sdperfilcadastro.insert({
								    	onComplete:function(lang, data){
								    		var cadastro_id = data[0].cadastro_id;
								    		// Trazendo o perfil do login
											Sdperfilcadastro.get({
										    	onComplete:function(lang, data){
										    		if(data.length > 0){
										    			return res.json({status: "sucesso", msg: lang.insertSdPerfilCadastro, data: data});
										    		} else {
										    			return res.json({status: "erro", msg: "Erro inesperado: cadastro_id não inserido"});
										    		}
										    	},
										    	where: "sd_c.cadastro_id = "+cadastro_id+""
										    });
								    	}
								    });
								} else {
									// Verifica se a imagem está vindo do facebook ou de um base64
									verificarFoto({
										onComplete: function(msgErro, filename){
											if(msgErro != null){
												return res.json({status: "erro", msg: msgErro});
											} else {
												// Cropando imagem 100x100
												app.midia.crop({
													onComplete: function(data){
														if(data){
															// Cropando imagem 50x50
															app.midia.resize({
																onComplete: function(data){
																	if(data){
																		// Gravando no banco de dados
																		req.body.foto = "/images/sdperfilcadastro/100x100/"+filename;
																		Sdperfilcadastro.insert({
																	    	onComplete:function(lang, data){
																	    		var cadastro_id = data[0].cadastro_id;
																	    		// Trazendo o perfil do login
																				Sdperfilcadastro.get({
																			    	onComplete:function(lang, data){
																			    		if(data.length > 0){
																			    			return res.json({status: "sucesso", msg: lang.insertSdPerfilCadastro,  data: data});
																			    		} else {
																			    			return res.json({status: "erro", msg: "Erro inesperado: cadastro_id não inserido"});
																			    		}
																			    	},
																			    	where: "sd_c.cadastro_id = "+cadastro_id+""
																			    });
																	    	}
																	    });
																	} else {
																		return res.json({status: "erro", msg: "Erro inesperado: Falha ao realizar crop 50x50", data: data});
																	}
																},
																filename: "/images/sdperfilcadastro/100x100/"+filename,
																largura: 200,
																altura: 200,
																destino: "/images/sdperfilcadastro/50x50/"
															});

														} else {
															return res.json({status: "erro", msg: "Erro inesperado: Falha ao realizar crop 100x100", data: data});
														}
													},
													filename: "/images/sdperfilcadastro/100x100/"+filename,
													largura: 300,
													altura: 300,
													destino: "/images/sdperfilcadastro/100x100/"
												});
											}
										},
										req: req,
										app: app
									});
								}
							}
						},
						onUpdate: function(data){
							Sdperfilcadastro.boot(app, data, res, function(){
								var update = [];
								update['excluido'] = '0';

								Sdperfilcadastro.update({
									onComplete:function(lang, data){
										var cadastro_id = data[0].cadastro_id;
							    		// Trazendo o perfil do login
										Sdperfilcadastro.get({
									    	onComplete:function(lang, data){
									    		if(data.length > 0){
									    			return res.json({status: "sucesso", msg: lang.insertSdPerfilCadastro, data: data});
									    		} else {
									    			return res.json({status: "erro", msg: "Erro inesperado: cadastro_id não inserido"});
									    		}
									    	},
									    	where: "sd_c.cadastro_id = "+cadastro_id+""
									    });
							    	},
							    	update: update,
							    	where: "cadastro_id = '"+data[0].cadastro_id+"'"
							    });

							});
						},
						params: req.body,
						app: app,
						res: res,
						lang: lang
					});
				});
			});

		},

		/*
		 * Ação de atualizar os dados do perfil
		 */
		atualizar: function(req,res){
			req.body = app.sql.schema(req.body, ['lang', 'cadastro_id', 'nome', 'sobrenome', 'email', 'senha', 'data_nascimento', 'genero', 'foto', 'igreja_id', 'cidade_id', 'relacionamento_id', 'formacao_id', 'cadastro_id_relacionamento', 'profissao_id','capa','funcao_id']);
			if(req.body.cadastro_id == null || (app.string.trim(req.body.cadastro_id) == '')){
				return res.json({status: "alerta", msg: "Erro inesperado: Parâmetro de requisição indefinido"});
			}
			Sdperfilcadastro.boot(app, req.body, res, function(){
				app.uteis.i18n(req.body, function(lang){
					delete req.body.lang;
					validateFormAtualizar({
						onComplete: function(data){
							if(data){
								return res.json({status: "erro", msg: data});
							} else {
								var msgSucesso = lang.perfilAtualizado;
								var foto = req.files.foto;
								var capa = req.files.capa;
								// Se não tiver foto já atualizo aqui...
								if(foto == undefined && capa == undefined){

									//Tratando os dados
									if (typeof req.body.senha != 'undefined') {
										req.body.senha = app.sql.sqlFunction("MD5('"+req.body.senha+"')");
										msgSucesso = lang.senhaAtualizada;
									}

									if (typeof req.body.data_nascimento != 'undefined') {
										req.body.data_nascimento = moment(req.body.data_nascimento, 'DD/MM/YYYY').format('YYYY-MM-DD').toString();
									}
									if(req.body.relacionamento_id && req.body.relacionamento_id == 5){
										req.body.cadastro_id_relacionamento = 0;
										req.body.data_inicio_relacionamento = null;
										req.body.data_aceitacao_relacionamento = null;

										// Coloca o companheiro solteiro tbm
										Sdperfilcadastro.get({
									    	onComplete:function(lang, data_get){
									    		if(data_get.length > 0){
									    			if(data_get[0].cadastro_id_relacionamento){
										    			var updateRelacionamento = [];
														updateRelacionamento['cadastro_id_relacionamento'] = 0;
														updateRelacionamento['data_inicio_relacionamento'] = null;
														updateRelacionamento['data_aceitacao_relacionamento'] = null;
														updateRelacionamento['relacionamento_id'] = req.body.relacionamento_id;
										    			Sdperfilcadastro.update({
															onComplete:function(lang, data_update_1){
																// termina a execucao
																Sdperfilcadastro.update({
																	onComplete:function(lang, data){
																		if(data.length > 0){
																			return res.json({status: "alerta", msg: msgSucesso});
																		} else {
																			return res.json({status: "erro", msg: "Algo inesperado aconteceu ao atualizar os dados do perfil"});
																		}
															    	},
															    	update: req.body,
															    	where: "cadastro_id = '"+req.body.cadastro_id+"'"
															    });

													    	},
													    	update: updateRelacionamento,
													    	where: "cadastro_id = '"+data_get[0].cadastro_id_relacionamento+"'"
													    });
									    			}else{
									    				// termina a execucao
														Sdperfilcadastro.update({
															onComplete:function(lang, data){
																if(data.length > 0){
																	return res.json({status: "alerta", msg: msgSucesso});
																} else {
																	return res.json({status: "erro", msg: "Algo inesperado aconteceu ao atualizar os dados do perfil"});
																}
													    	},
													    	update: req.body,
													    	where: "cadastro_id = '"+req.body.cadastro_id+"'"
													    });
									    			}
									    		}
									    	},
									    	where: "sd_c.cadastro_id = "+req.body.cadastro_id+""
									    });

										
									}else{
										// termina a execucao
										Sdperfilcadastro.update({
											onComplete:function(lang, data){
												if(data.length > 0){
													return res.json({status: "alerta", msg: msgSucesso});
												} else {
													return res.json({status: "erro", msg: "Algo inesperado aconteceu ao atualizar os dados do perfil"});
												}
									    	},
									    	update: req.body,
									    	where: "cadastro_id = '"+req.body.cadastro_id+"'"
									    });
									}
									
								} else {

									// Elimino a foto que estiver cadastrada antes de prosseguir
									Sdperfilcadastro.get({

										onComplete:function(lang, data){
											var pathImageObsoleta = null;
											var pathCapaObsoleta = null;

											if(data[0].foto != null && (app.string.trim(data[0].foto) != '') && foto != null && (app.string.trim(foto) != '')){
												// Foto obsoleta
												var fotoObsoleta = data[0].foto;
												var pastaServidor = "upload";
												pathImageObsoleta = path.join(__dirname, '../', '../', '../', pastaServidor+fotoObsoleta);
											}

											if(data[0].capa != null && (app.string.trim(data[0].capa) != '') && capa != null && (app.string.trim(capa) != '')){
												// Foto obsoleta
												var capaObsoleta = data[0].capa;
												var pastaServidor = "upload";
												pathCapaObsoleta = path.join(__dirname, '../', '../', '../', pastaServidor+capaObsoleta);
											}

											if(capa != null){
												//verificarCapa
												verificarCapa({
													onComplete: function(msgErro, filename){
														if(msgErro != null){
															return res.json({status: "erro", msg: msgErro});
														} else {
															// Gravando no banco de dados
															req.body.capa = "/images/sdperfilcadastro/capa/"+filename;
															Sdperfilcadastro.update({
																onComplete:function(lang, data){
																	if(data.length > 0){
																		//Removendo as fotos obsoletas do perfil
																		if(pathCapaObsoleta == null){
																			return res.json({status: "alerta", msg: msgSucesso, data: {capa: req.body.capa}});
																		} else {
																			// Eliminando arquivo obsoleto e retornando o arquivo
																			fs.unlink(pathCapaObsoleta, function(err){
																				if(err != null){
																					return res.json({status: "alerta", msg: "Erro inesperado: Falha ao eliminar arquivo obsoleto pós atualização"});
																				} else {
																					return res.json({status: "alerta", msg: msgSucesso, data: {capa: req.body.capa}});
																				}
																			});
																		}

																	} else {
																		return res.json({status: "erro", msg: "Algo inesperado aconteceu ao atualizar os dados do perfil"});
																	}
														    	},
														    	update: req.body,
														    	where: "cadastro_id = '"+req.body.cadastro_id+"'"
														    });
														}
													},
													req: req,
													app: app
												});
											}

											if(foto != null){
												// Verifica se a imagem está vindo do facebook ou de um base64
												verificarFoto({
													onComplete: function(msgErro, filename){
														if(msgErro != null){
															return res.json({status: "erro", msg: msgErro});
														} else {
															// Cropando imagem 100x100
															app.midia.crop({
																onComplete: function(data){
																	if(data){
																		// Cropando imagem 50x50
																		app.midia.resize({
																			onComplete: function(data){
																				if(data){
																					// Gravando no banco de dados
																					req.body.foto = "/images/sdperfilcadastro/100x100/"+filename;
																					Sdperfilcadastro.update({
																						onComplete:function(lang, data){
																							if(data.length > 0){
																								//Removendo as fotos obsoletas do perfil
																								if(pathImageObsoleta == null){
																									return res.json({status: "alerta", msg: msgSucesso, data: {foto: req.body.foto}});
																								} else {
																									// Eliminando arquivo obsoleto e retornando o arquivo
																									fs.unlink(pathImageObsoleta, function(err){
																										if(err != null){
																											return res.json({status: "alerta", msg: "Erro inesperado: Falha ao eliminar arquivo obsoleto pós atualização 100x100"});
																										} else {
																											pathImageObsoleta = pathImageObsoleta.replace("100x100", "50x50");
																											fs.unlink(pathImageObsoleta, function(err){
																												if(err != null){
																													return res.json({status: "alerta", msg: "Erro inesperado: Falha ao eliminar arquivo obsoleto pós atualização 50x50"});
																												} else {
																													return res.json({status: "alerta", msg: msgSucesso, data: {foto: req.body.foto}});
																												}
																											});
																										}
																									});
																								}

																							} else {
																								return res.json({status: "erro", msg: "Algo inesperado aconteceu ao atualizar os dados do perfil"});
																							}
																				    	},
																				    	update: req.body,
																				    	where: "cadastro_id = '"+req.body.cadastro_id+"'"
																				    });
																				} else {
																					return res.json({status: "erro", msg: "Erro inesperado: Falha ao realizar crop 50x50", data: data});
																				}
																			},
																			filename: "/images/sdperfilcadastro/100x100/"+filename,
																			largura: 200,
																			altura: 200,
																			destino: "/images/sdperfilcadastro/50x50/"
																		});

																	} else {
																		return res.json({status: "erro", msg: "Erro inesperado: Falha ao realizar crop 100x100", data: data});
																	}
																},
																filename: "/images/sdperfilcadastro/100x100/"+filename,
																largura: 300,
																altura: 300,
																destino: "/images/sdperfilcadastro/100x100/"
															});
														}
													},
													req: req,
													app: app
												});
										    }
										},
										where: "sd_c.cadastro_id = '"+req.body.cadastro_id+"'"
									});

								}
							}
						},
						params: req.body,
						app: app,
						res: res,
						lang: lang
					});
				});

			});
		},

		/*
		 * Ação de atualizar os dados do perfil
		 */
		privacidade: function(req,res){
			req.body = app.sql.schema(req.body, ['lang', 'cadastro_id', 'privacidade_ver_conteudo', 'privacidade_entrar_contato']);
			Sdperfilcadastro.boot(app, req.body, res, function(){
				app.uteis.i18n(req.body, function(lang){
					validateFormPrivacidade({
						onComplete: function(data){
							if(data){
								return res.json({status: "erro", msg: data});
							} else {
								delete req.body.lang;

								var msgSucesso = lang.perfilAtualizado;

								Sdperfilcadastro.update({
									onComplete:function(lang, data){
										if(data.length > 0){
											return res.json({status: "alerta", msg: msgSucesso});
										} else {
											return res.json({status: "erro", msg: "Algo inesperado aconteceu ao atualizar os dados do perfil"});
										}
							    	},
							    	update: req.body,
							    	where: "cadastro_id = '"+req.body.cadastro_id+"'"
							    });
							}
						},
						params: req.body,
						app: app,
						res: res,
						lang: lang
					});
				});

			});
		},

		/*
		 * Action de login
		 */
		login: function(req,res){

			Sdperfilcadastro.boot(app, req.body, res,function(){
				$tokenpush = req.body.tokenpush;
				app.uteis.i18n(req.body, function(lang){
					// Validar post
					validateFormLogin({
						onComplete: function(data){
							if(data){
								return res.json({status: "erro", msg: data});
							} else {
								// Trazendo o perfil do login
								Sdperfilcadastro.get({
							    	onComplete:function(lang, data){
							    		if(data.length > 0){
							    			data[0].udid = req.body.udid;
							    			delete data[0].senha;
							    			var retorno = data[0];
							    			//return res.json({status: "sucesso", data: retorno});
							    			var update = [];
											update['udid'] = req.body.udid;
											if(req.body.tokenpush)
												update['tokenpush'] = req.body.tokenpush;
											if(req.body.os)
												update['os'] = req.body.os;
							    			Sdperfilcadastro.update({
												onComplete:function(lang, data){
													//return res.json({status: "querie", data: data});
													if(data.length > 0){
														return res.json({status: "sucesso", data: retorno});
													} else {
														return res.json({status: "erro", msg: "Algo inesperado aconteceu ao gravar o novo udid"});
													}
										    	},
										    	update: update,
										    	where: "email = '"+req.body.email+"'"
										    });
							    		} else {

							    			return res.json({status: "erro", msg: lang.loginInvalido});
							    		}
							    	},
							    	where: "sd_c.senha = MD5('"+req.body.senha+"') AND sd_c.email = '"+req.body.email+"' AND sd_c.excluido = 0"
							    });
							}
						},
						params: req.body,
						app: app,
						res: res,
						lang: lang
					});
				});

			});
		},

		/*
		 * Action denunciar perfil
		 */
		denunciar: function(req, res){
			$notificacao = req.body.notificacao;
			$cadastro_id = req.body.cadastro_id;
			$cadastro_id_amizade = req.body.cadastro_id_denunciado;
			$os = req.body.os;
			$denuncia = req.body.denuncia;
			$emailTo = req.body.email;
			// Validar post
			app.uteis.i18n(req.body, function(lang){
				req.body = app.sql.schema(req.body, ['motivo_denuncia_cadastro', 'cadastro_id', 'cadastro_id_denunciado', 'denuncia']);
				Sdperfildenunciacadastro.boot(app, req.body, res, function(){
					validateFormDenuncia({
						onComplete: function(data){
							if(data){
								return res.json({status: "erro", msg: data});
							} else {
								//Inserindo denuncia
								Sdperfildenunciacadastro.insert({
							    	onComplete:function(lang, data){
							    		//Enviando email
										app.enviarmensagem.send({
											onComplete: function(data4){
												if(!data4){
													return res.json({status: "erro", msg: "Algo inesperado aconteceu ao enviar o email alertando a denuncia"});
												}else{
													app.enviarmensagem.send({
														onComplete: function(data4){
															if(!data4){
																return res.json({status: "erro", msg: "Algo inesperado aconteceu ao enviar o email alertando a denuncia"});
															}
														},
														email: $emailTo,
														from: "corteradicaislivres@gmail.com",
														lang: lang,
														texto: "Seu perfil foi denunciado pelo motivo: "+$denuncia
													});
												}
											},
											email: "paulophpweb@gmail.com",
											from: "corteradicaislivres@gmail.com",
											lang: lang,
											texto: "O perfil ("+$cadastro_id_amizade+") foi denunciado pelo perfil ("+$cadastro_id+"). "+$denuncia
										});
										if($cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
						    			   //Enviando push
											app.enviarpush.send({
												onComplete: function(data2){
													if(data2){
														if($cadastro_id != null && $cadastro_id != undefined && $cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
															Sdperfilnotificacao.boot(app, {cadastro_id:$cadastro_id_amizade,cadastro_id_amizade:$cadastro_id,texto:"Te denunciou."}, res, function(){
																Sdperfilnotificacao.insert({
																	onComplete: function(data3){
																		// retorna os dados
																	}
																});
															});
														}
														return res.json({status: "sucesso", msg: lang.insertSdPerfilDenuncia, data: data});
													}else{
														return res.json({status: "sucesso", msg: lang.insertSdPerfilDenuncia, data: data});
													}
												},
												cadastro_id: $cadastro_id,
												cadastro_id_amizade: $cadastro_id_amizade,
												titulo:"Radicais Livres",
												descricao:$notificacao,
												os:$os
											});
										}else{
											return res.json({status: "sucesso", msg: lang.insertSdPerfilDenuncia, data: data});
										}

							    	}
							    });
							}
						},
						params: req.body,
						app: app,
						res: res,
						lang: lang
					});

				});
			});
		},

		/*
		 * Action bloquear perfil
		 */
		bloquear: function(req, res){
			// Validar post
			$notificacao = req.body.notificacao;
			$cadastro_id = req.body.cadastro_id;
			$cadastro_id_amizade = req.body.cadastro_id_amizade;
			$os = req.body.os;

			app.uteis.i18n(req.body, function(lang){
				delete req.body.lang;
				req.body = app.sql.schema(req.body, ['cadastro_id', 'cadastro_id_amizade']);
				Sdperfilcadastrobloqueio.boot(app, req.body, res, function(){
					validateFormBloqueio({
						onComplete: function(data){
							if(data){
								return res.json({status: "erro", msg: data});
							} else {
								//Inserindo bloqueio
								Sdperfilcadastrobloqueio.insert({
							    	onComplete:function(lang, data){
							    		/*if($cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
							    			//Enviando push
											app.enviarpush.send({
												onComplete: function(data2){
													if(data2){
														if($cadastro_id != null && $cadastro_id != undefined && $cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
															Sdperfilnotificacao.boot(app, {cadastro_id:$cadastro_id_amizade,cadastro_id_amizade:$cadastro_id,texto:"Te bloqueou."}, res, function(){
																Sdperfilnotificacao.insert({
																	onComplete: function(data3){
																		console.log(data3);
																	}
																});
															});
														}
														return res.json({status: "alerta", msg: lang.bloquearAmigo});
													}else{
														return res.json({status: "alerta", msg: lang.bloquearAmigo});
													}
												},
												cadastro_id: $cadastro_id,
												cadastro_id_amizade: $cadastro_id_amizade,
												titulo:"Radicais Livres",
												descricao:$notificacao,
												os:$os
											});
										}else{
											return res.json({status: "alerta", msg: lang.bloquearAmigo});
										}*/
							    		return res.json({status: "alerta", msg: lang.bloquearAmigo});
							    	}
							    });
							}
						},
						params: req.body,
						app: app,
						res: res,
						lang: lang
					});

				});
			});
		},

		/*
		 * Action desbloquear perfil
		 */
		desbloquear: function(req, res){
			$notificacao = req.body.notificacao;
			$cadastro_id = req.body.cadastro_id;
			$cadastro_id_amizade = req.body.cadastro_id_amizade;
			$os = req.body.os;

			// Validar post
			app.uteis.i18n(req.body, function(lang){

				Sdperfilcadastrobloqueio.boot(app, req.body, res, function(){
					//Removendo bloqueio
					Sdperfilcadastrobloqueio.remover({
						onComplete:function(lang, data){
							if(data.length > 0){
								/*if($cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
					    			//Enviando push
									app.enviarpush.send({
										onComplete: function(data2){
											if(data2){
												if($cadastro_id != null && $cadastro_id != undefined && $cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
													Sdperfilnotificacao.boot(app, {cadastro_id:$cadastro_id_amizade,cadastro_id_amizade:$cadastro_id,texto:"Te desbloqueou."}, res, function(){
														Sdperfilnotificacao.insert({
															onComplete: function(data3){
																console.log(data3);
															}
														});
													});
												}
												return res.json({status: "alerta", msg: lang.desbloquearAmigo});
											}else{
												return res.json({status: "alerta", msg: lang.desbloquearAmigo});
											}
										},
										cadastro_id: $cadastro_id,
										cadastro_id_amizade: $cadastro_id_amizade,
										titulo:"Radicais Livres",
										descricao:$notificacao,
										os:$os
									});
								}else{
									return res.json({status: "alerta", msg: lang.desbloquearAmigo});
								}*/
								return res.json({status: "alerta", msg: lang.desbloquearAmigo});
							} else {
								return res.json({status: "erro", msg: "O amigo não foi desbloqueado, verifique se o mesmo já se encontra liberado"});
							}
				    	},
				    	where: "cadastro_id = '"+req.body.cadastro_id+"' AND cadastro_id_amizade = '"+req.body.cadastro_id_amizade+"'"
				    });

				});
			});
		},

		/*
		 * Action adicionar amigo
		 */
		inseriramigo: function(req, res){
			$notificacao = req.body.notificacao;
			$cadastro_id = req.body.cadastro_id;
			$cadastro_id_amizade = req.body.cadastro_id_amizade;
			$os = req.body.os;

			// Validar post
			app.uteis.i18n(req.body, function(lang){
				validateFormAmizade({
					onComplete: function(data){
						if(data){
							return res.json({status: "erro", msg: data});
						} else {
							//Inserindo amizades
							req.body.cadastro_id = req.body.cadastro_id;
							req.body.cadastro_id_amizade = req.body.cadastro_id_solicitado;
							var language = req.body.lang;
							var cadastro_id_solicitado = req.body.cadastro_id_solicitado;

							req.body = app.sql.schema(req.body, ['cadastro_id', 'cadastro_id_amizade', 'solicitacao_id']);
							Sdperfilcadastroamizade.boot(app, req.body, res, function(){
								Sdperfilcadastroamizade.insert({
									onComplete:function(lang, data){
										req.body.lang = language;
										req.body.cadastro_id_solicitado = cadastro_id_solicitado;
										inserirAmizade({
											onComplete: function(data){
												if(data){
													if($cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
									    			   //Enviando push
														app.enviarpush.send({
															onComplete: function(data2){
																if(data2){
																	if($cadastro_id != null && $cadastro_id != undefined && $cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
																		Sdperfilnotificacao.boot(app, {cadastro_id:$cadastro_id_amizade,cadastro_id_amizade:$cadastro_id,texto:"Aceitou sua amizade."}, res, function(){
																			Sdperfilnotificacao.insert({
																				onComplete: function(data3){
																					// retorna os dados
																				}
																			});
																		});
																	}
																	return res.json({status: "alerta", msg: lang.insertSdPerfilAmigo});
																}else{
																	return res.json({status: "alerta", msg: lang.insertSdPerfilAmigo});
																}
															},
															cadastro_id: $cadastro_id,
															cadastro_id_amizade: $cadastro_id_amizade,
															titulo:"Radicais Livres",
															descricao:$notificacao,
															os:$os
														});
													}else{
														return res.json({status: "alerta", msg: lang.insertSdPerfilAmigo});
													}

												} else {
													return res.json({status: "erro", msg: "Erro inesperado: Falha ao inserir o amigo"});
												}
											},
											params: req.body,
											app: app,
											res: res,
											lang: lang
										});
									}
								});

							});
						}
					},
					params: req.body,
					app: app,
					res: res,
					lang: lang
				});
			});
		},


		/*
		 * Action remover amigo
		 */
		removeramigo: function(req, res){
			$notificacao = req.body.notificacao;
			$cadastro_id = req.body.cadastro_id;
			$cadastro_id_amizade = req.body.cadastro_id_amizade;
			$os = req.body.os;

			// Validar post
			var cadastro_id = req.body.cadastro_id;
			if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
				return res.json({status: "erro", msg: "Erro inesperado, parâmetro #1 não postado"});
			}

			var cadastro_id_amizade = req.body.cadastro_id_amizade;
			if(cadastro_id_amizade == null || (app.string.trim(cadastro_id_amizade) == '')){
				return res.json({status: "erro", msg: "Erro inesperado, parâmetro #2 não postado"});
			}

			if(!app.string.empty(cadastro_id) && !app.string.empty(cadastro_id_amizade)){
				app.uteis.i18n(req.body, function(lang){
					delete req.body.lang;
					Sdperfilcadastrosolicitacao.boot(app, req.body, res, function(){
						Sdperfilcadastrosolicitacao.remover({
							onComplete:function(lang, data){
								if(data.length > 0){
									/*if($cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
					    			   //Enviando push
										app.enviarpush.send({
											onComplete: function(data2){
												if(data2){
													if($cadastro_id != null && $cadastro_id != undefined && $cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
														Sdperfilnotificacao.boot(app, {cadastro_id:$cadastro_id_amizade,cadastro_id_amizade:$cadastro_id,texto:"Removeu a amizade."}, res, function(){
															Sdperfilnotificacao.insert({
																onComplete: function(data3){
																	// retorna os dados
																}
															});
														});
													}
													return res.json({status: "alerta", msg: lang.amigoRemovido});
												}else{
													return res.json({status: "alerta", msg: lang.amigoRemovido});
												}
											},
											cadastro_id: $cadastro_id,
											cadastro_id_amizade: $cadastro_id_amizade,
											titulo:"Radicais Livres",
											descricao:$notificacao,
											os:$os
										});
									}else{
										return res.json({status: "alerta", msg: lang.amigoRemovido});
									}*/
									return res.json({status: "alerta", msg: lang.amigoRemovido});

								} else {
									return res.json({status: "erro", msg: "O amigo não foi deletado, verifique se o mesmo já foi deletado"});
								}
					    	},
					    	where: "(cadastro_id = '"+req.body.cadastro_id+"' AND cadastro_id_solicitado = '"+req.body.cadastro_id_amizade+"') OR (cadastro_id = '"+req.body.cadastro_id_amizade+"' AND cadastro_id_solicitado = '"+req.body.cadastro_id+"')"
					    });

					});
				});
			} else {
				return res.json({status: "erro", msg: "Parâmetros de requisição inválidos"});
			}
		},

		/*
		 * Action de solicitar amigo
		 */
		inserirsolicitacao: function(req, res){
			$notificacao = req.body.notificacao;
			$cadastro_id = req.body.cadastro_id;
			$cadastro_id_amizade = req.body.cadastro_id_solicitado;
			$os = req.body.os;

			// Validar post
			app.uteis.i18n(req.body, function(lang){
				req.body = app.sql.schema(req.body, ['cadastro_id', 'cadastro_id_solicitado']);
				Sdperfilcadastrosolicitacao.boot(app, req.body, res, function(){
					validateFormSolicitacao({
						onComplete: function(data){
							if(data){
								return res.json({status: "erro", msg: data});
							} else {
								//Inserindo solicitacao
								Sdperfilcadastrosolicitacao.insert({
									onComplete:function(lang, data){
										if($cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
							    			//Enviando push
											app.enviarpush.send({
												onComplete: function(data2){
													if(data2){
														if($cadastro_id != null && $cadastro_id != undefined && $cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
															Sdperfilnotificacao.boot(app, {cadastro_id:$cadastro_id_amizade,cadastro_id_amizade:$cadastro_id,texto:"Solicitou sua amizade."}, res, function(){
																Sdperfilnotificacao.insert({
																	onComplete: function(data3){

																	}
																});
															});
														}
														return res.json({status: "alerta", msg: lang.insertSdPerfilSolicitacao, data: data});
													}else{
														return res.json({status: "alerta", msg: lang.insertSdPerfilSolicitacao, data: data});
													}
												},
												cadastro_id: $cadastro_id,
												cadastro_id_amizade: $cadastro_id_amizade,
												titulo:"Radicais Livres",
												descricao:$notificacao,
												os:$os
											});
										}else{
											return res.json({status: "alerta", msg: lang.insertSdPerfilSolicitacao, data: data});
										}

									}
								});
							}
						},
						params: req.body,
						app: app,
						res: res,
						lang: lang
					});

				});
			});
		},

		/*
		 * Action de listar solicitacoes
		 */
		listarsolicitacao: function(req, res){
			var cadastro_id = req.body.cadastro_id;
			if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
				return res.json({status: "erro", msg: "Erro inesperado, parâmetro de busca não postado"});
			}

			Sdperfilcadastrosolicitacao.boot(app, req.body, res, function(){
				Sdperfilcadastrosolicitacao.getsolicitacoes({
					onComplete:function(lang, data){
						if(data.length > 0){
							return res.json({status: "sucesso", data: data});
						} else {
							//Se a validação deu tudo certo retorno aqui...
							return res.json({status: "sucesso", data: null});
						}
					},
					where: "sd_cs.cadastro_id_solicitado = '"+cadastro_id+"' AND sd_cs.solicitacao_id NOT IN (SELECT solicitacao_id FROM sd_perfil.sd_cadastro_amizade)"
				});

			});
		},

		/*
		 * Action de enviarsenha
		 */
		enviarsenha: function(req,res){
			app.uteis.i18n(req.body, function(lang){
				// Verificando se é um email válido
				var email = req.body.email;
				if(!app.uteis.isEmail(email)){
					return res.json(lang.erroSdPerfilCadastroEmailInvalido);
				};

				Sdperfilcadastro.boot(app, req.body, res, function(){
					var update = [];
					var codigo = Math.floor(Math.random()*900000) + 100000;
					update['codigo_alterar_senha'] = codigo;

					Sdperfilcadastro.update({
						onComplete:function(lang, data){
							if(data.length > 0){
								//Enviando email
								app.recuperarsenha.send({
									onComplete: function(data){
										if(data){
											return res.json({status: "alerta", msg: lang.enviarCodigoRecuperarSenha+" "+email, data: data});
										} else {
											return res.json({status: "erro", msg: "Algo inesperado aconteceu ao enviar o email com o código de redefinir senha"});
										}
									},
									email: email,
									from: "corteradicaislivres@gmail.com",
									lang: lang,
									codigo: codigo
								});
							} else {
								return res.json({status: "erro", msg: "Algo inesperado aconteceu ao gravar o código para redefinir senha"});
							}
				    	},
				    	update: update,
				    	where: "email = '"+email+"'"
				    });

				});
			});
		},

		/*
		 * Action de remover conta
		 */
		removerconta: function(req,res){
			app.uteis.i18n(req.body, function(lang){

				Sdperfilcadastro.boot(app, req.body, res, function(){
					var update = [];
					update['excluido'] = '1';

					Sdperfilcadastro.update({
						onComplete:function(lang, data){
							if(data.length > 0){
								//Enviando email
								return res.json({status: "alerta", msg: "Excluido com sucesso."});
							} else {
								return res.json({status: "erro", msg: "Algo inesperado ocorreu."});
							}
				    	},
				    	update: update,
				    	where: "cadastro_id = '"+req.body.cadastro_id+"'"
				    });

				});
			});
		},

		/*
		 * Action de remover conta
		 */
		alterarsenha: function(req,res){
			app.uteis.i18n(req.body, function(lang){

				Sdperfilcadastro.boot(app, req.body, res, function(){
					var update = [];
					update['senha'] = app.sql.sqlFunction("MD5('"+req.body.senha+"')");

					Sdperfilcadastro.update({
						onComplete:function(lang, data){
							if(data.length > 0){
								return res.json({status: "alerta", msg: "Senha alterada com sucesso!"});
							} else {
								return res.json({status: "erro", msg: "Algo inesperado ocorreu."});
							}
				    	},
				    	update: update,
				    	where: "cadastro_id = '"+req.body.cadastro_id+"'"
				    });

				});
			});
		}
	};

	/*
	 * Verifica se a foto é um file ou facebook e retorna o caminho do upload
	 */
	var verificarFoto = function ($obj){
		// Gravando imagem na pasta
		var pastaServidor = "upload";
		var pastaPerfil = "/images/sdperfilcadastro/100x100/";
		var arquivo = $obj.app.string.uniqid()+".jpg";
		var pathImage = pastaServidor+pastaPerfil+arquivo;

		if($obj.req.files.foto != undefined && $obj.req.files.foto != null){
			fs.readFile($obj.req.files.foto.path, function (err, data) {
				if(err != null){
					return $obj.onComplete(err, null);
				} else {
					fs.writeFile(pathImage, data, function(err) {

						if(err != null){
							return $obj.onComplete("Erro inesperado: Falha ao subir arquivo File", null);
						} else {
							// Eliminando arquivo obsoleto e retornando o arquivo
							fs.unlink($obj.req.files.foto.path, function(err){
								if(err != null){
									return $obj.onComplete("Erro inesperado: Falha ao eliminar arquivo obsoleto", null);
								} else {
									return $obj.onComplete(null, arquivo);
								}
							});
						}

					});
				}
			});
		} else if ($obj.req.body.foto != null && $obj.req.body.foto != undefined) {
			var foto = $obj.req.body.foto;
			var options = {url: "http://graph.facebook.com/"+foto+"/picture?type=normal"};

		} else {
			return $obj.onComplete("Erro inesperado: Nenhuma foto enviada para o método", null);
		}
	};

	/*
	 * Verifica se a capa é um file
	 */
	var verificarCapa = function ($obj){
		// Gravando imagem na pasta
		var pastaServidor = "upload";
		var pastaPerfil = "/images/sdperfilcadastro/capa/";
		var arquivo = $obj.app.string.uniqid()+".jpg";
		var pathImage = pastaServidor+pastaPerfil+arquivo;

		if($obj.req.files.capa != undefined && $obj.req.files.capa != null){
			fs.readFile($obj.req.files.capa.path, function (err, data) {
				if(err != null){
					return $obj.onComplete(err, null);
				} else {
					fs.writeFile(pathImage, data, function(err) {

						if(err != null){
							return $obj.onComplete("Erro inesperado: Falha ao subir arquivo File", null);
						} else {
							// Eliminando arquivo obsoleto e retornando o arquivo
							fs.unlink($obj.req.files.capa.path, function(err){
								if(err != null){
									return $obj.onComplete("Erro inesperado: Falha ao eliminar arquivo obsoleto", null);
								} else {
									return $obj.onComplete(null, arquivo);
								}
							});
						}

					});
				}
			});
		} else {
			return $obj.onComplete("Erro inesperado: Nenhuma capa enviada para o método", null);
		}
	};

	/*
	 * Validar dados de login antes de tentar logar
	 */
	var validateFormLogin = function($obj){
		var senha = $obj.params.senha;
		if(senha == null || (app.string.trim(senha) == '')){
			return $obj.onComplete($obj.lang.erroSdPerfilCadastroSenha);
		}

		// Verificando se é um email válido
		var email = $obj.params.email;
		if(!app.uteis.isEmail(email)){
			return $obj.onComplete($obj.lang.erroSdPerfilCadastroEmailInvalido);
		};

		return $obj.onComplete(false);
	};

	/*
	 * Validar denuncia
	 */
	var validateFormDenuncia = function($obj){
		var motivo_denuncia_cadastro = $obj.params.motivo_denuncia_cadastro;
		if(motivo_denuncia_cadastro == null || (app.string.trim(motivo_denuncia_cadastro) == '')){
			return $obj.onComplete("Erro inesperado: Validação #motivo_denuncia_cadastro");
		}

		var cadastro_id = $obj.params.cadastro_id;
		if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
			return $obj.onComplete("Erro inesperado: Validação #cadastro_id");
		}

		var cadastro_id_denunciado = $obj.params.cadastro_id_denunciado;
		if(cadastro_id_denunciado == null || (app.string.trim(cadastro_id_denunciado) == '')){
			return $obj.onComplete("Erro inesperado: Validação #cadastro_id_denunciado");
		}

		var denuncia = $obj.params.denuncia;
		if(denuncia == null || (app.string.trim(denuncia) == '')){
			return $obj.onComplete("Erro inesperado: Validação #denuncia");
		}

		// Verificando se a denuncia já foi feita...
		Sdperfildenunciacadastro.boot(app, $obj.params, $obj.res, function(){

			Sdperfildenunciacadastro.get({
				onComplete:function(lang, data){

					if(data.length > 0){
						return $obj.onComplete(lang.erroSdPerfilDenunciaDuplicada);
					} else {
						//Se a validação deu tudo certo retorno aqui...
						return $obj.onComplete(false);
					}
				},
				where: "cadastro_id = '"+cadastro_id+"' AND cadastro_id_denunciado = '"+cadastro_id_denunciado+"'"
			});
		});
	};

	/*
	 * Validar bloqueio
	 */
	var validateFormBloqueio = function($obj){
		var cadastro_id = $obj.params.cadastro_id;
		if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
			return $obj.onComplete("Erro inesperado: Validação #cadastro_id");
		}

		var cadastro_id_amizade = $obj.params.cadastro_id_amizade;
		if(cadastro_id_amizade == null || (app.string.trim(cadastro_id_amizade) == '')){
			return $obj.onComplete("Erro inesperado: Validação #cadastro_id_amizade");
		}

		// Verificando se o bloqueio já foi feito...
		Sdperfilcadastrobloqueio.boot(app, $obj.params, $obj.res, function(){

			Sdperfilcadastrobloqueio.get({
				onComplete:function(lang, data){
					if(data.length > 0){
						return $obj.onComplete("Erro inesperado: O perfil já foi bloqueado");
					} else {
						//Se a validação deu tudo certo retorno aqui...
						return $obj.onComplete(false);
					}
				},
				where: "cadastro_id = '"+cadastro_id+"' AND cadastro_id_amizade = '"+cadastro_id_amizade+"'"
			});
		});
	};

	/*
	 * Inserindo amigo reverso
	 */
	var inserirAmizade = function($obj){
		var cadastro_id = $obj.params.cadastro_id;
		$obj.params.cadastro_id = $obj.params.cadastro_id_amizade;
		$obj.params.cadastro_id_amizade = cadastro_id;
		delete $obj.params.lang;
		delete $obj.params.cadastro_id_solicitado;
		Sdperfilcadastroamizade.boot(app, $obj.params, $obj.res, function(){

			Sdperfilcadastroamizade.insert({
				onComplete:function(lang, data){
					if(data.length > 0){
						return $obj.onComplete(true);
					} else {
						return $obj.onComplete(false);
					}
				}
			});
		});
	};

	/*
	 * Validar novo amigo
	 */
	var validateFormAmizade = function($obj){
		var cadastro_id = $obj.params.cadastro_id_solicitado;
		if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
			return $obj.onComplete("Erro inesperado: Validação #cadastro_id");
		}

		var cadastro_id_amizade = $obj.params.cadastro_id;
		if(cadastro_id_amizade == null || (app.string.trim(cadastro_id_amizade) == '')){
			return $obj.onComplete("Erro inesperado: Validação #cadastro_id_amizade");
		}

		var solicitacao_id = $obj.params.solicitacao_id;
		if(solicitacao_id == null || (app.string.trim(solicitacao_id) == '')){
			return $obj.onComplete("Erro inesperado: Validação #solicitacao_id");
		}

		if(cadastro_id == cadastro_id_amizade){
			return $obj.onComplete("Erro inesperado: #cadastro_id == #cadastro_id_amizade");
		}

		// Verificando se o amigo já foi vinculado...
		Sdperfilcadastroamizade.boot(app, $obj.params, $obj.res, function(){

			Sdperfilcadastroamizade.get({
				onComplete:function(lang, data){
					if(data.length > 0){
						return $obj.onComplete(lang.erroSdPerfilAmizadeDuplicada);
					} else {
						//Se a validação deu tudo certo retorno aqui...
						return $obj.onComplete(false);
					}
				},
				where: "(cadastro_id = '"+cadastro_id+"' AND cadastro_id_amizade = '"+cadastro_id_amizade+"') OR (cadastro_id = '"+cadastro_id_amizade+"' AND cadastro_id_amizade = '"+cadastro_id+"')"
			});
		});
	};

	/*
	 * Validar nova solicitacao
	 */
	var validateFormSolicitacao = function($obj){
		var cadastro_id = $obj.params.cadastro_id;
		if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
			return $obj.onComplete("Erro inesperado: Validação #cadastro_id");
		}

		var cadastro_id_solicitado = $obj.params.cadastro_id_solicitado;
		if(cadastro_id_solicitado == null || (app.string.trim(cadastro_id_solicitado) == '')){
			return $obj.onComplete("Erro inesperado: Validação #cadastro_id_solicitado");
		}

		if(cadastro_id == cadastro_id_solicitado){
			return $obj.onComplete("Erro inesperado: #cadastro_id == #cadastro_id_solicitado");
		}

		// Verificando se o amigo já foi vinculado...
		Sdperfilcadastrosolicitacao.boot(app, $obj.params, $obj.res, function(){

			Sdperfilcadastrosolicitacao.get({
				onComplete:function(lang, data){
					if(data.length > 0){
						return $obj.onComplete(lang.erroSdPerfilSolicitacaoDuplicada);
					} else {
						//Se a validação deu tudo certo retorno aqui...
						return $obj.onComplete(false);
					}
				},
				where: "(cadastro_id = '"+cadastro_id_solicitado+"' AND cadastro_id_solicitado = '"+cadastro_id+"') OR (cadastro_id = '"+cadastro_id+"' AND cadastro_id_solicitado = '"+cadastro_id_solicitado+"')"
			});
		});
	};

	/*
	 * Validar Mensagem
	 */
	var validateFormMensagem = function($obj){
		var cadastro_id = $obj.params.cadastro_id;
		if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
			return $obj.onComplete("Erro inesperado: Validação #cadastro_id");
		}

		var cadastro_id_amizade = $obj.params.cadastro_id_amizade;
		if(cadastro_id_amizade == null || (app.string.trim(cadastro_id_amizade) == '')){
			return $obj.onComplete("Erro inesperado: Validação #cadastro_id_amizade");
		}

		var texto = $obj.params.texto;
		if(texto == null || (app.string.trim(texto) == '')){
			return $obj.onComplete("Erro inesperado: Validação #texto");
		}

		//Se a validação deu tudo certo retorno aqui...
		return $obj.onComplete(null);
	};


	/*
	 * Validar Mensagem Excluir
	 */
	var validateFormMensagemExcluir = function($obj){
		var cadastro_id = $obj.params.cadastro_id;
		if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
			return $obj.onComplete("Erro inesperado: Validação #cadastro_id");
		}

		var mensagem_id = $obj.params.mensagem_id;
		if(mensagem_id == null || (app.string.trim(mensagem_id) == '')){
			return $obj.onComplete("Erro inesperado: Validação #mensagem_id");
		}

		//Se a validação deu tudo certo retorno aqui...
		return $obj.onComplete(null);
	};

	/*
	 * Validar dados de cadastro inicial antes de validar privacidade
	 */
	var validateFormPrivacidade = function($obj){

		var cadastro_id = $obj.params.cadastro_id;
		if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
			return $obj.onComplete("Aconteceu um erro inesperado: É obrigatório passar o cadastro_id para atualizar um cadastro");
		}

		if (typeof $obj.params.privacidade_entrar_contato != 'undefined') {
			var entrar_contato = $obj.params.privacidade_entrar_contato;
			if(entrar_contato == null || (app.string.trim(entrar_contato) == '' || (entrar_contato != 'todos' && entrar_contato != 'somente_amigos'))){
				return $obj.onComplete("Aconteceu um erro inesperado: #privacidade_entrar_contato inválido");
			}
		}

		if (typeof $obj.params.privacidade_ver_conteudo != 'undefined') {
			var ver_conteudo = $obj.params.privacidade_ver_conteudo;
			if(ver_conteudo == null || (app.string.trim(ver_conteudo) == '' || (ver_conteudo != 'todos' && ver_conteudo != 'somente_amigos'))){
				return $obj.onComplete("Aconteceu um erro inesperado: #privacidade_ver_conteudo inválido");
			}
		}

		if(($obj.params.privacidade_entrar_contato == null || app.string.trim($obj.params.privacidade_entrar_contato) == '') && ($obj.params.privacidade_ver_conteudo == null || app.string.trim($obj.params.privacidade_ver_conteudo) == '')) {
			return $obj.onComplete("Aconteceu um erro inesperado: #privacidade_entrar_contato e #privacidade_ver_conteudo vazios");
		}

		//Se a validação deu tudo certo retorno aqui...
		return $obj.onComplete(null);
	};

	/*
	 * Validar dados de cadastro inicial antes de atualizar
	 */
	var validateFormAtualizar = function($obj){

		var cadastro_id = $obj.params.cadastro_id;
		if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
			return $obj.onComplete("Aconteceu um erro inesperado: É obrigatório passar o cadastro_id para atualizar um cadastro");
		}

		//Validando nome
		if (typeof $obj.params.nome != 'undefined') {
			var nome = $obj.params.nome;
			if(nome == null || (app.string.trim(nome) == '')){
				return $obj.onComplete($obj.lang.erroSdPerfilCadastroNome);
			}
		}

		//Validando sobrenome
		if (typeof $obj.params.sobrenome != 'undefined') {
			var sobrenome = $obj.params.sobrenome;
			if(sobrenome == null || (app.string.trim(sobrenome) == '')){
				return $obj.onComplete($obj.lang.erroSdPerfilCadastroSobrenome);
			}
		}

		//Validando data_nascimento
		if (typeof $obj.params.data_nascimento != 'undefined') {
			var data_nascimento = $obj.params.data_nascimento;
			if(data_nascimento == null || (app.string.trim(data_nascimento) == '')){
				return $obj.onComplete($obj.lang.erroSdPerfilCadastroDataNascimento);
			}
		}

		//Validando genero
		if (typeof $obj.params.genero != 'undefined') {
			var genero = $obj.params.genero;
			if(genero == null || (app.string.trim(genero) == '')){
				return $obj.onComplete($obj.lang.erroSdPerfilCadastroGenero);
			}
		}

		if (typeof $obj.params.senha != 'undefined') {
			var senha = $obj.params.senha;
			if(senha == null || (app.string.trim(senha) == '')){
				return $obj.onComplete($obj.lang.erroSdPerfilCadastroSenha);
			}
		}

		//Validando email informado
		if (typeof $obj.params.email != 'undefined') {
			// Verificando se é um email válido
			var email = $obj.params.email;
			if(!app.uteis.isEmail(email)){
				return $obj.onComplete($obj.lang.erroSdPerfilCadastroEmailInvalido);
			};

			// Verificando se o email já está cadastrado
			Sdperfilcadastro.boot(app, $obj.params, $obj.res, function(){

				Sdperfilcadastro.get({
					onComplete:function(lang, data){
						if(data.length > 0){
							return $obj.onComplete(lang.erroSdPerfilCadastroEmailExiste);
						} else {
							if(email == null){
								return $obj.onComplete(lang.erroSdPerfilCadastroEmail);
							}

							//Se a validação deu tudo certo retorno aqui...
							return $obj.onComplete(null);
						}

					},
					where: "sd_c.email = '"+email+"' AND sd_c.cadastro_id <> '"+$obj.params.cadastro_id+"'"
				});
			});
		} else {
			return $obj.onComplete(null);
		}
	};

	/*
	 * Validar dados de cadastro inicial antes de inserir
	 */
	var validateFormCadastro = function($obj){
		var nome = $obj.params.nome;
		if(nome == null || (app.string.trim(nome) == '')){
			return $obj.onComplete($obj.lang.erroSdPerfilCadastroNome);
		}

		var sobrenome = $obj.params.sobrenome;
		if(sobrenome == null || (app.string.trim(sobrenome) == '')){
			return $obj.onComplete($obj.lang.erroSdPerfilCadastroSobrenome);
		}

		var data_nascimento = $obj.params.data_nascimento;
		if(data_nascimento == null || (app.string.trim(data_nascimento) == '')){
			return $obj.onComplete($obj.lang.erroSdPerfilCadastroDataNascimento);
		}

		var genero = $obj.params.genero;
		if(genero == null || (app.string.trim(genero) == '')){
			return $obj.onComplete($obj.lang.erroSdPerfilCadastroGenero);
		}

		var senha = $obj.params.senha;
		if(senha == null || (app.string.trim(senha) == '')){
			return $obj.onComplete($obj.lang.erroSdPerfilCadastroSenha);
		}

		// Verificando se é um email válido
		var email = $obj.params.email;
		if(!app.uteis.isEmail(email)){
			return $obj.onComplete($obj.lang.erroSdPerfilCadastroEmailInvalido);
		};

		// Verificando se o perfil ta excluido
		Sdperfilcadastro.boot(app, $obj.params, $obj.res, function(){

			Sdperfilcadastro.findAmizades({
				onComplete:function(lang, data){
					if(data.length > 0){
						return $obj.onUpdate(data);
					}else{
						Sdperfilcadastro.findAmizades({
							onComplete:function(lang, data){
								if(data.length > 0){
									return $obj.onComplete(lang.erroSdPerfilCadastroEmailExiste);
								}

								if(email == null){
									return $obj.onComplete(lang.erroSdPerfilCadastroEmail);
								}

								//Se a validação deu tudo certo retorno aqui...
								return $obj.onComplete(null);
							},
							where: "sd_c.email = '"+email+"'"
						});
					}
				},
				where: "sd_c.email = '"+email+"' AND sd_c.excluido = 1"
			});
		});
	};

	return SdperfilcadastroController;

};
