var moment = require('moment');
var fs = require('fs');
var path = require('path');

module.exports = function(app){

	var Sdchat = app.models.sdchat;
	var Sdperfilnotificacao = app.models.sdperfilnotificacao;

	var SdchatController = {
		/*
		 * Action listar eventos
		 */
		get: function(req,res){
			// Método construtor
			Sdchat.boot(app, req.body, res,function(){
				var limit = req.body.limit;
				var offset = req.body.offset;
				var $estado_id = req.body.estado_id;
				var $cidade_id = req.body.cidade_id;
				var $titulo = req.body.titulo;
				var $is_root = req.body.is_root;
				$where = "";

				if($estado_id != null || (app.string.trim($estado_id) != '')){
					$where += " AND sd_es.estado_id = "+$estado_id+" ";
				}

				if($titulo != null || (app.string.trim($titulo) != '')){
					$where += " AND upper(remove_acento(sd_e.titulo)) like upper(remove_acento('%"+$titulo+"%')) ";
				}

				if($cidade_id != null || (app.string.trim($cidade_id) != '')){
					$where += " AND sd_e.cidade_id = "+$cidade_id+" ";
				}
				// Trazendo todos perfis
				Sdchat.get({
			    	onComplete:function(lang, data){
			    		if(!data){
			    			data = {};
			    		}
			    		return res.json({status: "sucesso", data: data});
			    	},
			    	cadastro_id: req.body.cadastro_id,
			    	order: "sd_e.data ASC",
			    	tipo: req.body.tipo,
			    	limit: limit,
				    offset:offset,
				    is_root:$is_root,
				    where:$where
			    });
			});
		},

		/*
		 * Action listar eventos
		 */
		getAmigosOnline: function(req,res){
			console.log(req.body);
			// Método construtor
			Sdchat.boot(app, req.body, res,function(){
				var limit = req.body.limit;
				var offset = req.body.offset;
				var $is_root = req.body.is_root;
				var $ult_login = req.body.ult_login;

				// Trazendo todos perfis
				Sdchat.getAmigosOnline({
			    	onComplete:function(lang, data){
			    		if(!data){
			    			data = {};
			    		}
			    		return res.json({status: "sucesso", data: data});
			    	},
			    	cadastro_id: req.body.cadastro_id,
			    	order: "online DESC, ausente DESC, nao_lido ASC, sd_c.nome ASC",
			    	limit: limit,
				    offset:offset,
				    is_root:$is_root,
				    ult_login:$ult_login
			    });
			});
		},

		/*
		 * Action listar eventos
		 */
		getChatConversa: function(req,res){
			// Método construtor
			Sdchat.boot(app, req.body, res,function(){
				var limit = req.body.limit;
				var offset = req.body.offset;
				var $is_root = req.body.is_root;
				var $ult_login = req.body.ult_login;

				// Trazendo todos as conversar
				Sdchat.getchatconversa({
			    	onComplete:function(lang, data){
			    		if(!data){
			    			data = {};
			    		}
			    		return res.json({status: "sucesso", data: data});
			    	},
			    	cadastro_id: req.body.cadastro_id,
			    	cadastro_id_amizade: req.body.cadastro_id_pessoa,
			    	order: "sd_ch.data DESC",
			    	limit: limit,
				    offset:offset,
				    is_root:$is_root,
				    ult_login:$ult_login
			    });
			});
		},

		/*
		 * Action inserir chat conversa
		 */
		chatinsert: function(req, res){

			var $cadastro_id_amizade = req.body.cadastro_id_amizade;
			var $cadastro_id = req.body.cadastro_id;
			var $notificacao = req.body.notificacao;
			var $os = req.body.os;

			req.body = app.sql.schema(req.body, ['lang','cadastro_id','cadastro_id_amizade','texto','ult_login']);

			Sdchat.boot(app, req.body, res, function(){
				req.body.data = req.body.ult_login;
				delete req.body.lang;
				delete req.body.ult_login;
				Sdchat.insert({
			    	onComplete:function(lang, data){
			    		if(data){
				    		app.enviarpush.send({
								onComplete: function(data2){
									if(data2){
										return res.json({status: "sucesso", data: data});
									}else{
										return res.json({status: "erro", msg: "Um erro inesperado aconteceu ao enviar a notificação."});
									}
								},
								onError: function(msg){
									return res.json({status: "erro", msg: msg});
								},
								cadastro_id_amizade: $cadastro_id_amizade,
								titulo:"Radicais Livres",
								descricao:$notificacao,
								os:$os,
								controller:"/chat/chatconversa"
							});
			    		}else{
			    			return res.json({status: "erro", msg: "Um erro inesperado aconteceu ao enviar a mensagem."});
			    		}
			    	}
			    });



			});

		},

		/*
		 * Ação de atualizar os dados do perfil
		 */
		update: function(req,res){
			req.body = app.sql.schema(req.body, ['lang', 'cadastro_id', 'evento_id', 'tipo']);
			Sdchat.boot(app, req.body, res, function(){
				delete req.body.lang;

				Sdchat.update({
					onComplete:function(lang, data){
						if(data.length > 0){
							return res.json({status: "alerta", msg: "Deu tudo certo"});
						} else {
							return res.json({status: "erro", msg: "Algo inesperado aconteceu ao atualizar os dados do perfil"});
						}
			    	},
			    	update: req.body,
			    	where: "evento_id = '"+req.body.evento_id+"'"
			    });
			});
		},

	};

	return SdchatController;

};
