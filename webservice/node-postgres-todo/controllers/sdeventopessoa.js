var moment = require('moment');
var fs = require('fs');
var path = require('path');

module.exports = function(app){

	var SdeventoPessoa = app.models.sdeventopessoa;

	var SdeventoPessoaController = {

		/*
		 * Ação de atualizar os dados do perfil
		 */
		insert: function(req,res){
			req.body = app.sql.schema(req.body, ['lang', 'cadastro_id', 'evento_id', 'tipo']);
			SdeventoPessoa.boot(app, req.body, res, function(){
				delete req.body.lang;

				SdeventoPessoa.insert({
					onComplete:function(lang, data){
						if(data.length > 0){
							return res.json({status: "alerta", msg: "Deu tudo certo"});
						} else {
							return res.json({status: "erro", msg: "Algo inesperado aconteceu ao atualizar os dados do perfil"});
						}
			    	}
			    });
			});
		},

		/*
		 * Ação de atualizar os dados do perfil
		 */
		update: function(req,res){
			req.body = app.sql.schema(req.body, ['lang', 'cadastro_id', 'evento_id', 'tipo']);
			$evento_id = req.body.evento_id;
			$cadastro_id = req.body.cadastro_id;
			SdeventoPessoa.boot(app, req.body, res, function(){
				delete req.body.lang;
				delete req.body.evento_id;
				delete req.body.cadastro_id;

				SdeventoPessoa.update({
					onComplete:function(lang, data){
						if(data.length > 0){
							return res.json({status: "alerta", msg: "Deu tudo certo"});
						} else {
							return res.json({status: "erro", msg: "Algo inesperado aconteceu ao gravar o código para redefinir senha"});
						}
			    	},
			    	update: req.body,
			    	where: "evento_id = '"+$evento_id+"' AND cadastro_id = '"+$cadastro_id+"'"
			    });
			});
		},

	};

	return SdeventoPessoaController;

};
