var moment = require('moment');
var fs = require('fs');
var path = require('path');

module.exports = function(app){

	var Sdpublic = app.models.sdpublic;
	var Sdchat = app.models.sdchat;

	var SdpublicController = {
		/*
		 * Action listar estado
		 */
		getfuncao: function(req,res){
			var texto = req.body.texto;
			var where = "sd_f.funcao_id IS NOT NULL";
			if(texto != null || (app.string.trim(texto) != '')){
				where+= " AND upper(remove_acento(sd_f.nome)) like upper(remove_acento('%"+texto+"%'))";
			}
			// Método construtor
			Sdpublic.boot(app, req.body, res,function(){
				var limit = req.body.limit;
				var offset = req.body.offset;
				// Trazendo todos perfis
				Sdpublic.getfuncao({
			    	onComplete:function(lang, data){
			    		if(!data){
			    			data = {};
			    		}
			    		return res.json({status: "sucesso", data: data});
			    	},
			    	where:where,
			    	order: "sd_f.funcao_id ASC",
			    	limit: limit,
				    offset:offset
			    });
			});
		},
		/*
		 * Testar notificação paulo
		 */
		testenotificacao: function(req,res){

			var apn = require('apn');

			var options = {
			  token: {
			    key: path.join(__dirname, '../library/APNsAuthKey_Y26258K93B.p8'),
			    keyId: "Y26258K93B",
			    teamId: "5SSRSY6C8D"
			  },
			  production: true
			};
			var apnProvider = new apn.Provider(options);

			var note = new apn.Notification();
			// The topic is usually the bundle identifier of your application.
			note.topic = "com.radicaislivres.app";
			note.badge = 1;
			note.alert = 'paulo fazendo teste';
			note.sound = 'default';
			note.payload = {
					dados:[],
					controller:""
			};
			apnProvider.send(note, 'e39200d298974f65c1edf505b2beec54c1ab8d31f3c7bc4b5ac99bd31f86ab7a').then( result => {
			    console.log("sent:", result.sent.length);
      			console.log("failed:", result.failed.length);
      			console.log(result.failed);
			    //console.log("failed:", result.failed.length);
			    //console.log(result.failed);
			});
		},
		/*
		 * Action listar estado
		 */
		getestado: function(req,res){
			var texto = req.body.texto;
			var where = "sd_e.estado_id IS NOT NULL";
			if(texto != null || (app.string.trim(texto) != '')){
				where+= " AND upper(remove_acento(sd_e.nome)) like upper(remove_acento('%"+texto+"%'))";
			}
			// Método construtor
			Sdpublic.boot(app, req.body, res,function(){
				var limit = req.body.limit;
				var offset = req.body.offset;
				// Trazendo todos perfis
				Sdpublic.getestado({
			    	onComplete:function(lang, data){
			    		if(!data){
			    			data = {};
			    		}
			    		return res.json({status: "sucesso", data: data});
			    	},
			    	where:where,
			    	order: "sd_e.estado_id ASC",
			    	limit: limit,
				    offset:offset
			    });
			});
		},
		/*
		 * Action get mensagem igreja
		 */
		getmensagemigreja: function(req,res){
			return res.json({status: "sucesso", data: "Caso a sua igreja não esteja cadastrada, entre em contato com o administrador pedrozapublicacoes@gmail.com."});
		},

		/*
		 * Action listar todas as notificacoes
		 */
		getnotificacaoall: function(req,res){
			Sdpublic.boot(app, req.body, res,function(){
				var limit = req.body.limit;
				var offset = req.body.offset;
				var $is_root = req.body.is_root;
				var $ult_login = req.body.ult_login;
				var $retorno = {};

				// Trazendo todas as notificacao de chat
				Sdpublic.getTotalNotificacaoChat({
			    	onComplete:function(lang, data){
			    		if(!data){
			    			data = {};
			    		}
			    		$retorno.chat = data[0];
			    		// Trazendo todas as notificacao de solicitacao
		    			Sdpublic.getTotalNotificacaoSolicitacao({
		    				onComplete:function(lang, data){
		    					$retorno.solicitacao = data[0];
		    						// Trazendo todas as notificacao de mensagem
					    			Sdpublic.getTotalNotificacaoMensagem({
					    				onComplete:function(lang, data){
					    					$retorno.mensagem = data[0];
					    					Sdpublic.getTotalNotificacaoNotificacao({
							    				onComplete:function(lang, data){
							    					$retorno.notificacao = data[0];
							    					return res.json({status: "sucesso", data: $retorno});
							    				},
							    				cadastro_id: req.body.cadastro_id,
							    				is_root:$is_root
							    			});
					    				},
					    				cadastro_id: req.body.cadastro_id,
					    				is_root:$is_root
					    			});
		    				},
		    				cadastro_id: req.body.cadastro_id
		    			});


			    	},
			    	cadastro_id: req.body.cadastro_id
			    });
			});
		},
		/*
		 * Action listar cidade
		 */
		getcidade: function(req,res){
			var texto = req.body.texto;
			var estado_id = req.body.estado_id;
			var where = "sd_c.cidade_id IS NOT NULL";
			if(texto != null || (app.string.trim(texto) != '')){
				where+= " AND upper(remove_acento(sd_c.nome)) like upper(remove_acento('%"+texto+"%'))";
			}
			if(estado_id != null || (app.string.trim(estado_id) != '')){
				where+= " AND sd_c.estado_id = '"+estado_id+"' ";
			}
			// Método construtor
			Sdpublic.boot(app, req.body, res,function(){
				var limit = req.body.limit;
				var offset = req.body.offset;
				// Trazendo todos perfis
				Sdpublic.getcidade({
			    	onComplete:function(lang, data){
			    		if(!data){
			    			data = {};
			    		}
			    		return res.json({status: "sucesso", data: data});
			    	},
			    	where: where,
			    	order: "sd_c.cidade_id ASC",
			    	limit: limit,
				    offset:offset
			    });
			});
		},

	};

	return SdpublicController;

};
