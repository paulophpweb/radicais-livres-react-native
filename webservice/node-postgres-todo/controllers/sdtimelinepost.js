var moment = require('moment');
var fs = require('fs');
var path = require('path');

module.exports = function(app){
	// Model
	var Sdtimelinepost = app.models.sdtimelinepost;
	var Sdtimelinepostlike = app.models.sdtimelinepostlike;
	var Sdtimelinepostlikecomments = app.models.sdtimelinepostlikecomments;
	var Sdperfilcadastro = app.models.sdperfilcadastro;
	var Sdperfilnotificacao = app.models.sdperfilnotificacao;

	var SdtimelinepostController = {

		/*
		 * Action inserir perfil
		 */
		insert: function(req, res){
			// Validar post
			$notificacao = req.body.notificacao;
			$compartilhando = req.body.compartilhando;
			$cadastro_id = req.body.cadastro_id;
			$cadastro_id_amizade = req.body.cadastro_id_amizade;
			$os = req.body.os;

			var foto_caminho = "";
			if(req.body.foto != null && req.body.foto != undefined && req.body.foto != ''){
				foto_caminho = req.body.foto;
			}

			req.body = app.sql.schema(req.body, ['lang', 'cadastro_id', 'texto', 'arquivo','cadastro_id_compartilhado','post_id_compartilhado','video_id','video_tipo','facebook_usuario','url_texto']);
			
			Sdtimelinepost.boot(app, req.body, res, function(){

				app.uteis.i18n(req.body, function(lang){
					validateFormInserir({
						onComplete: function(data){
							delete req.body.lang;
							if(data){
								return res.json({status: "erro", msg: data});
							} else {
								var arquivo = req.files.arquivo;

								// Se não tiver foto já cadastro aqui...
								if(arquivo == null || arquivo == undefined){
									delete req.body.arquivo;
									if(foto_caminho != '')req.body.arquivo = foto_caminho;
									Sdtimelinepost.insert({
								    	onComplete:function(lang, data){
								    		if($compartilhando == 1){
								    			if($cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
									    			//Enviando push
													app.enviarpush.send({
														onComplete: function(data2){
															if(data2){
																if($cadastro_id != null && $cadastro_id != undefined && $cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
																	Sdperfilnotificacao.boot(app, {cadastro_id:$cadastro_id_amizade,cadastro_id_amizade:$cadastro_id,texto:"Compartilhou seu post",post_id:data[0].post_id}, res, function(){
																		Sdperfilnotificacao.insert({
																			onComplete: function(data3){
																				// retorn os dados
																			}
																		});
																	});
																}
																return res.json({status: "sucesso", data: data});
															}else{
																return res.json({status: "sucesso", data: data});
															}
														},
														cadastro_id_amizade: $cadastro_id_amizade,
														titulo:"Radicais Livres",
														descricao:$notificacao,
														os:$os
													});
												}else{
													return res.json({status: "sucesso", data: data});
												}
								    		}else{
								    			return res.json({status: "sucesso", data: data});
								    		}

								    	}
								    });
								} else {
									// Verifica e trata o arquivo
									verificarArquivo({
										onComplete: function(msgErro, filename){
											if(msgErro != null){
												return res.json({status: "erro", msg: msgErro});
											} else {
												// Cropando imagem 300x170
												app.midia.crop({
													onComplete: function(data){
														if(data){
															// Cropando imagem 150x150
															app.midia.crop({
																onComplete: function(data){
																	if(data){
																		// Gravando no banco de dados
																		req.body.arquivo = "/images/sdtimelinepost/300x170/"+filename;
																		req.body.tipo = "foto";
																		Sdtimelinepost.insert({
																	    	onComplete:function(lang, data){
																	    		data[0].foto = "/images/sdtimelinepost/300x170/"+filename;
																	    		return res.json({status: "sucesso", data: data});
																	    	}
																	    });
																	} else {
																		return res.json({status: "erro", msg: "Erro inesperado: Falha ao realizar crop 150x150", data: data});
																	}
																},
																filename: "/images/sdtimelinepost/300x170/"+filename,
																largura: 150,
																altura: 150,
																destino: "/images/sdtimelinepost/150x150/"
															});

														} else {
															return res.json({status: "erro", msg: "Erro inesperado: Falha ao realizar crop 300x170", data: data});
														}
													},
													filename: "/images/sdtimelinepost/300xprop/"+filename,
													largura: 300,
													altura: 170,
													destino: "/images/sdtimelinepost/300x170/"
												});
											}
										},
										req: req,
										app: app
									});
								}
							}
						},
						params: req.body,
						app: app,
						res: res,
						lang: lang
					});
				});
			});
		},

		/*
		 * Action inserir comentário
		 */
		insertcomment: function(req, res){
			$notificacao = req.body.notificacao;
			$cadastro_id = req.body.cadastro_id;
			$cadastro_id_amizade = req.body.cadastro_id_amizade;
			$post_id = req.body.post_id;
			$os = req.body.os;
			// Validar post
			req.body = app.sql.schema(req.body, ['cadastro_id', 'texto', 'post_id']);
			Sdtimelinepost.boot(app, req.body, res, function(){

				app.uteis.i18n(req.body, function(lang){
					validateFormInserirComment({
						onComplete: function(data){
							if(data){
								return res.json({status: "erro", msg: data});
							} else {
								//Inserindo o comentário
								Sdtimelinepost.insertcomment({
							    	onComplete:function(lang, data){
							    		if($cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
							    			//Enviando push
							    			console.log( "enviar o push");
											app.enviarpush.send({
												onComplete: function(data2){
													if(data2){
														if($cadastro_id != null && $cadastro_id != undefined && $cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
															Sdperfilnotificacao.boot(app, {cadastro_id:$cadastro_id_amizade,cadastro_id_amizade:$cadastro_id,texto:"Comentou seu post: "+req.body.texto,post_id:$post_id}, res, function(){
																Sdperfilnotificacao.insert({
																	onComplete: function(data3){
																		// retorna os dados
																	}
																});
															});
														}
														return res.json({status: "sucesso", data: data});
													}else{
														return res.json({status: "sucesso", data: data});
													}
												},
												cadastro_id_amizade: $cadastro_id_amizade,
												titulo:"Radicais Livres",
												descricao:$notificacao,
												os:$os
											});
										}else{
											return res.json({status: "sucesso", data: data});
										}

							    	}
							    });
							}
						},
						params: req.body,
						app: app,
						res: res,
						lang: lang
					});
				});
			});
		},

		/*
		 * Action remover mensagem
		 */
		removercomentario: function(req, res){
			$cadastro_id_amizade = req.body.cadastro_id_amizade;
			$notificacao = req.body.notificacao;
			$os = req.body.os;
			$post_id = req.body.post_id;

			var cadastro_id = req.body.cadastro_id;
			if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
				return res.json({status: "erro", msg: "Erro inesperado: Validação #cadastro_id"});
			}

			var comment_id = req.body.comment_id;
			if(comment_id == null || (app.string.trim(comment_id) == '')){
				return res.json({status: "erro", msg: "Erro inesperado: Validação #comment_id"});
			}

			var post_id = req.body.post_id;
			if(post_id == null || (app.string.trim(post_id) == '')){
				return res.json({status: "erro", msg: "Erro inesperado: Validação #post_id"});
			}

			req.body = app.sql.schema(req.body, ['cadastro_id', 'comment_id','post_id']);

			Sdtimelinepost.boot(app, req.body, res, function(){

				app.uteis.i18n(req.body, function(lang){
					//Removendo comentario
					Sdtimelinepost.removercomentario({
				    	onComplete:function(lang, data){
				    		if($cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
				    			//Enviando push
								app.enviarpush.send({
									onComplete: function(data2){
										if(data2){
											if($cadastro_id != null && $cadastro_id != undefined && $cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
												Sdperfilnotificacao.boot(app, {cadastro_id:$cadastro_id_amizade,cadastro_id_amizade:$cadastro_id,texto:"Removeu o comentário do post",post_id:$post_id}, res, function(){
													Sdperfilnotificacao.insert({
														onComplete: function(data3){
															// retorna os dados
														}
													});
												});
											}
											return res.json({status: "sucesso", msg: "Comentário deletado com sucesso!", data: data});
										}else{
											return res.json({status: "sucesso", msg: "Comentário deletado com sucesso!", data: data});
										}
									},
									cadastro_id_amizade: $cadastro_id_amizade,
									titulo:"Radicais Livres",
									descricao:$notificacao,
									os:$os
								});
							}else{
								return res.json({status: "sucesso", msg: "Comentário deletado com sucesso!", data: data});
							}

				    	},
				    where: "comment_id = '"+comment_id+"' AND cadastro_id = '"+cadastro_id+"'",
				    post_id: post_id,
				    comment_id:comment_id
				    });
				});
			});
		},

	    /*
		 * Action remover mensagem
		 */
		removerpost: function(req, res){
			$cadastro_id_amizade = req.body.cadastro_id_amizade;
			$notificacao = req.body.notificacao;
			$os = req.body.os;
			$post_id = req.body.post_id;

			var cadastro_id = req.body.cadastro_id;
			if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
				return res.json({status: "erro", msg: "Erro inesperado: Validação #cadastro_id"});
			}

			var post_id = req.body.post_id;
			if(post_id == null || (app.string.trim(post_id) == '')){
				return res.json({status: "erro", msg: "Erro inesperado: Validação #post_id"});
			}

			req.body = app.sql.schema(req.body, ['cadastro_id','post_id']);

			Sdtimelinepost.boot(app, req.body, res, function(){

				app.uteis.i18n(req.body, function(lang){
					//Removendo comentario
					Sdtimelinepost.removerpost({
				    	onComplete:function(lang, data){
				    		if($cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
				    			//Enviando push
								app.enviarpush.send({
										onComplete: function(data2){
											if(data2){
												if($cadastro_id != null && $cadastro_id != undefined && $cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
													Sdperfilnotificacao.boot(app, {cadastro_id:$cadastro_id_amizade,cadastro_id_amizade:$cadastro_id,texto:"Removeu o post",post_id:$post_id}, res, function(){
														Sdperfilnotificacao.insert({
															onComplete: function(data3){
																// retorna os dados
															}
														});
													});
												}
												return res.json({status: "sucesso", msg: "Post deletado com sucesso!", data: data});
											}else{
												return res.json({status: "sucesso", msg: "Post deletado com sucesso!", data: data});
											}
										},
										cadastro_id_amizade: $cadastro_id_amizade,
										titulo:"Radicais Livres",
										descricao:$notificacao,
										os:$os
									});
								}else{
									return res.json({status: "sucesso", msg: "Post deletado com sucesso!", data: data});
								}

				    	},
				    post_id: post_id,
				    cadastro_id: cadastro_id
				    });
				});
			});
		},

		/*
		 * Action curtir post
		 */
		curtir: function(req, res){
			$notificacao = req.body.notificacao;
			$cadastro_id = req.body.cadastro_id;
			$cadastro_id_amizade = req.body.cadastro_id_amizade;
			$os = req.body.os;

			// Validar post
			app.uteis.i18n(req.body, function(lang){
				req.body = app.sql.schema(req.body, ['lang', 'cadastro_id', 'post_id']);
				Sdtimelinepostlike.boot(app, req.body, res, function(){

					delete req.body.lang;
					validateFormCurtir({
						onComplete: function(data){
							if(data){
								return res.json({status: "erro", msg: data});
							} else {
								//Inserindo curtir
								Sdtimelinepostlike.insert({
							    	onComplete:function(lang, data){
							    		if($cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
							    			//Enviando push
											app.enviarpush.send({
												onComplete: function(data2){
													if(data2){
														if($cadastro_id != null && $cadastro_id != undefined && $cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
															Sdperfilnotificacao.boot(app, {cadastro_id:$cadastro_id_amizade,cadastro_id_amizade:$cadastro_id,texto:"Curtiu seu post",post_id:req.body.post_id}, res, function(){
																Sdperfilnotificacao.insert({
																	onComplete: function(data3){
																		// retorna os dados
																	}
																});
															});
														}
														return res.json({status: "sucesso", data: data});
													}else{
														return res.json({status: "sucesso", data: data});
													}
												},
												cadastro_id_amizade: $cadastro_id_amizade,
												titulo:"Radicais Livres",
												descricao:$notificacao,
												os:$os
											});
										}else{
											return res.json({status: "sucesso", data: data});
										}

							    	}
							    });
							}
						},
						params: req.body,
						app: app,
						res: res,
						lang: lang
					});
				});
			});
		},

		/*
		 * Action descurtir post
		 */
		descurtir: function(req, res){
			$cadastro_id = req.body.cadastro_id;
			$post_id = req.body.post_id;
			$os = req.body.os;

			// Validar post
			app.uteis.i18n(req.body, function(lang){
				req.body = app.sql.schema(req.body, ['lang', 'cadastro_id', 'post_id']);
				Sdtimelinepostlike.boot(app, req.body, res, function(){

					delete req.body.lang;
					//Inserindo curtir
					Sdtimelinepostlike.descurtir({
				    	onComplete:function(lang, data){
				    		return res.json({status: "sucesso", data: data});
				    	},
				    	cadastro_id:$cadastro_id,
				    	post_id:$post_id

				    });
				});
			});
		},


		/*
		 * Action curtir commentarios
		 */
		curtircomment: function(req, res){
			$notificacao = req.body.notificacao;
			$cadastro_id = req.body.cadastro_id;
			$cadastro_id_amizade = req.body.cadastro_id_amizade;
			$os = req.body.os;

			// Validar post
			app.uteis.i18n(req.body, function(lang){
				req.body = app.sql.schema(req.body, ['lang', 'cadastro_id', 'post_id','comment_id']);
				Sdtimelinepostlikecomments.boot(app, req.body, res, function(){
					delete req.body.lang;
					//return res.json({status: "erro", msg: req.body});
					validateFormCurtirComentario({
						onComplete: function(data){
							if(data){
								return res.json({status: "erro", msg: data});
							} else {
								//Inserindo curtir
								Sdtimelinepostlikecomments.insert({
							    	onComplete:function(lang, data){
							    		if($cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
							    			//Enviando push
											app.enviarpush.send({
												onComplete: function(data2){
													if(data2){
														if($cadastro_id != null && $cadastro_id != undefined && $cadastro_id_amizade != null && $cadastro_id_amizade != undefined ){
															Sdperfilnotificacao.boot(app, {cadastro_id:$cadastro_id_amizade,cadastro_id_amizade:$cadastro_id,texto:"Curtiu seu comentário no post",post_id:req.body.post_id}, res, function(){
																Sdperfilnotificacao.insert({
																	onComplete: function(data3){
																		// retorna os dados
																	}
																});
															});
														}
														return res.json({status: "sucesso", data: data});
													}else{
														return res.json({status: "sucesso", data: data});
													}
												},
												cadastro_id_amizade: $cadastro_id_amizade,
												titulo:"Radicais Livres",
												descricao:$notificacao,
												os:$os
											});
										}else{
											return res.json({status: "sucesso", data: data});
										}
							    		return res.json({status: "sucesso", data: data});

							    	}
							    });
							}
						},
						params: req.body,
						app: app,
						res: res,
						lang: lang
					});
				});
			});
		},

		/*
		 * Action listar timelineperfil
		 */
		timelineperfil: function(req,res){
			var cadastro_id = req.body.cadastro_id;
			var is_root = req.body.is_root;
			if(cadastro_id == '' || cadastro_id == undefined){
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido #cadastro_id"});
			}

			var cadastro_id_pessoa = req.body.cadastro_id_pessoa;
			if(cadastro_id_pessoa == '' || cadastro_id_pessoa == undefined){
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido #cadastro_id_pessoa"});
			}

			// Método construtor
			Sdtimelinepost.boot(app, req.body, res, function(){

				// Trazendo todos perfis
				Sdtimelinepost.get({
			    	onComplete:function(lang, data){
			    		return res.json({status: "sucesso", data: data});
			    	},
			    	cadastro_id: cadastro_id,
			    	is_root: is_root,
			    	cadastro_id_pessoa: cadastro_id_pessoa,
			    	order: "sd_p.post_id DESC"
			    });
			});
		},

		/*
		 * Action listar os perfis que deram like no post
		 */
		getusuarioslike: function(req,res){
			var cadastro_id = req.body.cadastro_id;
			if(cadastro_id == '' || cadastro_id == undefined){
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido #cadastro_id"});
			}

			var post_id = req.body.post_id;
			if(post_id == '' || post_id == undefined){
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido #post_id"});
			}

			// Método construtor
			Sdtimelinepostlike.boot(app, req.body, res, function(){

				// Trazendo todos perfis
				Sdtimelinepostlike.getUsuarioLikes({
			    	onComplete:function(lang, data){
			    		return res.json({status: "sucesso", data: data});
			    	},
			    	post_id: post_id,
			    	order: "sd_pl.like_id DESC"
			    });
			});
		},

		/*
		 * Action listar os perfis que deram comentaram no post
		 */
		getusuarioscommented: function(req,res){
			var cadastro_id = req.body.cadastro_id;
			if(cadastro_id == '' || cadastro_id == undefined){
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido #cadastro_id"});
			}

			var post_id = req.body.post_id;
			if(post_id == '' || post_id == undefined){
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido #post_id"});
			}

			// Método construtor
			Sdtimelinepost.boot(app, req.body, res, function(){

				// Trazendo todos perfis
				Sdtimelinepost.getUsuarioCommented({
			    	onComplete:function(lang, data){
			    		return res.json({status: "sucesso", data: data});
			    	},
			    	post_id: post_id,
			    	order: "sd_pc.comment_id DESC"
			    });
			});
		},

		/*
		 * Action listar os perfis que deram like no post
		 */
		getusuariosshared: function(req,res){
			var cadastro_id = req.body.cadastro_id;
			if(cadastro_id == '' || cadastro_id == undefined){
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido #cadastro_id"});
			}

			var post_id = req.body.post_id;
			if(post_id == '' || post_id == undefined){
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido #post_id"});
			}

			// Método construtor
			Sdtimelinepost.boot(app, req.body, res, function(){

				// Trazendo todos perfis
				Sdtimelinepost.getUsuarioShared({
			    	onComplete:function(lang, data){
			    		return res.json({status: "sucesso", data: data});
			    	},
			    	post_id: post_id,
			    	order: "sd_p.post_id DESC"
			    });
			});
		},

		/*
		 * Action listar timelineperfilnovo
		 */
		timelineperfilnovo: function(req,res){
			var cadastro_id = req.body.cadastro_id;
			var is_root = req.body.is_root;
			if(cadastro_id == '' || cadastro_id == undefined){
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido #cadastro_id"});
			}

			var cadastro_id_pessoa = req.body.cadastro_id_pessoa;
			if(cadastro_id_pessoa == '' || cadastro_id_pessoa == undefined){
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido #cadastro_id_pessoa"});
			}

			var dataPerfil = null;

			// Método construtor
			Sdtimelinepost.boot(app, req.body, res, function(){

				// Trazendo todos perfis
				Sdtimelinepost.get({
					onComplete:function(lang, data){
						return res.json({status: "sucesso", dataPerfil: dataPerfil, data: data});
					},
					cadastro_id: cadastro_id,
					is_root: is_root,
					cadastro_id_pessoa: cadastro_id_pessoa,
					order: "sd_p.post_id DESC"
				});
			});
		},

		/*
		 * Action listar post
		 */
		getpost: function(req,res){

			var post_id = req.body.post_id;
			var cadastro_id = req.body.cadastro_id;
			var is_root = req.body.is_root;
			if(post_id == '' || post_id == undefined){
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido #post_id"});
			}
			// Método construtor
			Sdtimelinepost.boot(app, req.body, res, function(){

				// Trazendo todos perfis
				Sdtimelinepost.getpost({
					onComplete:function(lang, data){
						return res.json({status: "sucesso", data: data});
					},
					post_id: post_id,
					cadastro_id: cadastro_id,
					is_root: is_root,
					order: "sd_p.post_id DESC"
				});
			});
		},

		/*
		 * Action listar timeline geral
		 */
		timeline: function(req,res){
			//req.body = req.query;

			var cadastro_id = req.body.cadastro_id;
			var is_root = req.body.is_root;
			var limit = req.body.limit;
			var offset = req.body.offset;
			if(!app.string.empty(cadastro_id)){

				// Método construtor
				Sdtimelinepost.boot(app, req.body, res, function(){

					// Trazendo todos perfis
					Sdtimelinepost.gettimeline({
						onComplete:function(lang, data){
							return res.json({status: "sucesso", data: data});
						},
						cadastro_id: cadastro_id,
						is_root: is_root,
						order: "sd_p.post_id DESC",
						limit: limit,
				        offset:offset
					});
				});
			} else {
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido #cadastro_id"});
			}
		},

		/*
		 * Action listar fotos do perfil
		 */
		timelineperfilfotos: function(req,res){
			var cadastro_id_pessoa = req.body.cadastro_id_pessoa;
			if(cadastro_id_pessoa == '' || cadastro_id_pessoa == undefined){
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido #cadastro_id_pessoa"});
			}
			// Método construtor
			Sdtimelinepost.boot(app, req.body, res, function(){

				// Trazendo todos perfis
				Sdtimelinepost.getfotos({
					onComplete:function(lang, data){
						return res.json({status: "sucesso", data: data});
					},
					cadastro_id_pessoa: cadastro_id_pessoa,
					order: "sd_p.post_id DESC"
				});
			});
		},

		/*
		 * Action listar comentarios
		 */
		comentarios: function(req,res){
			var cadastro_id_pessoa = req.body.cadastro_id_pessoa;
			var cadastro_id = req.body.cadastro_id;
			var is_root = req.body.is_root;
			var post_id = req.body.post_id;
			var limit = req.body.limit;
			var offset = req.body.offset;
			if(cadastro_id_pessoa == '' || cadastro_id_pessoa == undefined && post_id == '' || post_id == undefined){
				return res.json({status: "erro", msg: "Erro inesperado parametro_invalido #cadastro_id_pessoa e #post_id"});
			}
			// Método construtor
			Sdtimelinepost.boot(app, req.body, res, function(){

				// Trazendo todos perfis
				Sdtimelinepost.getcomentarios({
					onComplete:function(lang, data){
						return res.json({status: "sucesso", data: data});
					},
					cadastro_id_pessoa: cadastro_id_pessoa,
					cadastro_id: cadastro_id,
					is_root: is_root,
					post_id: post_id,
					limit: limit,
				    offset:offset,
					order: "spc.comment_id DESC"
				});
			});
		},
	};

	/*
	 * Validar dados do post antes de inserir
	 */
	var validateFormInserir = function($obj){
		var cadastro_id = $obj.params.cadastro_id;
		if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
			return $obj.onComplete("Erro inesperado: É necessário enviar o cadastro_id");
		}

		var texto = $obj.params.texto;
		var arquivo = $obj.params.arquivo;
		if((texto == null || (app.string.trim(texto) == '')) || (arquivo == null || (app.string.trim(arquivo) == ''))){
			//return $obj.onComplete("Erro inesperado: É necessário publicar algo (texto ou arquivo)");
		}

		return $obj.onComplete(false);
	};


	/*
	 * Validar dados do post antes de inserir
	 */
	var validateFormInserirComment = function($obj){
		var cadastro_id = $obj.params.cadastro_id;
		if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
			return $obj.onComplete("Erro inesperado: É necessário enviar o cadastro_id");
		}

		var post_id = $obj.params.post_id;
		if(post_id == null || (app.string.trim(post_id) == '')){
			return $obj.onComplete("Erro inesperado: É necessário enviar o post_id");
		}

		var texto = $obj.params.texto;
		if(texto == null || (app.string.trim(texto) == '')){
			return $obj.onComplete("Erro inesperado: O Campo Texto deve ser preenchido.");
		}

		return $obj.onComplete(false);
	};

	/*
	 * Verifica se o arquivo é um file ou youtube e retorna o caminho do upload
	 */
	var verificarArquivo = function ($obj){
		// Gravando imagem na pasta
		var pastaServidor = "upload";
		var pastaTimeline = "/images/sdtimelinepost/300xprop/";
		var arquivo = $obj.app.string.uniqid()+".jpg";
		if (!fs.existsSync(pastaServidor+pastaTimeline)){
		    fs.mkdirSync(pastaServidor+pastaTimeline);
		}
		var pathImage = pastaServidor+pastaTimeline+arquivo;
		if($obj.req.files.arquivo != undefined && $obj.req.files.arquivo != null){

			fs.readFile($obj.req.files.arquivo.path, function (err, data) {
				if(err != null){
					return $obj.onComplete(err, null);
				} else {
					fs.writeFile(pathImage, data, function(err) {
						if(err != null){
							return $obj.onComplete("Erro inesperado: Falha ao subir arquivo File "+err, null);
						} else {
							// Eliminando arquivo obsoleto e retornando o arquivo
							fs.unlink($obj.req.files.arquivo.path, function(err){
								if(err != null){
									return $obj.onComplete("Erro inesperado: Falha ao eliminar arquivo obsoleto", null);
								} else {
									return $obj.onComplete(null, arquivo);
								}
							});
						}
					});
				}
			});
		} else {
			return $obj.onComplete("Erro inesperado: Arquivo não reconhecido", null);
		}
	};

	/*
	 * Validar curtir
	 */
	var validateFormCurtir = function($obj){
		var cadastro_id = $obj.params.cadastro_id;
		if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
			return $obj.onComplete("Erro inesperado: Validação #cadastro_id");
		}

		var post_id = $obj.params.post_id;
		if(post_id == null || (app.string.trim(post_id) == '')){
			return $obj.onComplete("Erro inesperado: Validação #post_id");
		}

		// Verificando se a denuncia já foi feita...
		Sdtimelinepostlike.boot(app, $obj.params, $obj.res, function(){

			Sdtimelinepostlike.get({
				onComplete:function(lang, data){
					if(data.length > 0){
						return $obj.onComplete("Erro inesperado: O post já foi curtido por este #cadastro_id");
					} else {
						//Se a validação deu tudo certo retorno aqui...
						return $obj.onComplete(false);
					}
				},
				where: "cadastro_id = '"+cadastro_id+"' AND post_id = '"+post_id+"'"
			});
		});
	};


	/*
	 * Validar curtir comentario
	 */
	var validateFormCurtirComentario = function($obj){
		var cadastro_id = $obj.params.cadastro_id;
		if(cadastro_id == null || (app.string.trim(cadastro_id) == '')){
			return $obj.onComplete("Erro inesperado: Validação #cadastro_id");
		}

		var post_id = $obj.params.post_id;
		if(post_id == null || (app.string.trim(post_id) == '')){
			return $obj.onComplete("Erro inesperado: Validação #post_id");
		}

		var comment_id = $obj.params.comment_id;
		if(comment_id == null || (app.string.trim(comment_id) == '')){
			return $obj.onComplete("Erro inesperado: Validação #comment_id");
		}

		// Verificando se ja curtiu...
		Sdtimelinepostlikecomments.boot(app, $obj.params, $obj.res, function(){

			Sdtimelinepostlikecomments.get({
				onComplete:function(lang, data){
					if(data.length > 0){
						return $obj.onComplete("Erro inesperado: O post já foi curtido por este #cadastro_id");
					} else {
						//Se a validação deu tudo certo retorno aqui...
						return $obj.onComplete(false);
					}
				},
				where: "cadastro_id = '"+cadastro_id+"' AND post_id = '"+post_id+"' AND comment_id = '"+comment_id+"'"
			});
		});
	};

	return SdtimelinepostController;

};
