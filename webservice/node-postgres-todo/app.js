/**
 * Module dependencies.
 */
var express      = require('express');
var load         = require('express-load');


// VERSAO 4
var bodyParser	 = require('body-parser');
var errorHandler = require('errorhandler');

var app = express();
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: true,limit: '50mb'}));

// aceitando arquivos multipart Ex: files
var multer = require('multer');
app.use(multer({dest:  '/tmp/'}));

// development only
if ('development' == app.get('env')) {
  app.use(errorHandler());
}

// importando library de utilidades globais
var path = require('path');
app.uteis = new (require(path.join(__dirname, 'library/uteis')))(app);
app.sql = new (require(path.join(__dirname, 'library/sql')))(app);
app.string = new (require(path.join(__dirname, 'library/string')))(app);
app.midia = new (require(path.join(__dirname, 'library/midia')))(app);
app.recuperarsenha = new (require(path.join(__dirname, 'library/recuperarsenha')))(app);
app.enviarmensagem = new (require(path.join(__dirname, 'library/enviarmensagem')))(app);
app.enviarpush = new (require(path.join(__dirname, 'library/enviarpush')))(app);

// comprimindo
var compression = require('compression');
app.use(compression()); //use compression
app.use(express.static(path.join(__dirname, 'public')));

// imprimindo dados
load('models').then('controllers').then('routes').into(app);

// alterado
app.listen(49155, function(){
  console.log('Servidor rodando na porta 49155...');
});
