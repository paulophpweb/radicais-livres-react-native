module.exports = function(app){
	var sdevento = app.controllers.sdevento;
	var sdeventopessoa = app.controllers.sdeventopessoa;
		
	app.post('/sdevento/get', sdevento.get);
	app.post('/sdevento/insert', sdevento.insert);
	app.post('/sdeventopessoa/insert', sdeventopessoa.insert);
	app.post('/sdeventopessoa/update', sdeventopessoa.update);
};
