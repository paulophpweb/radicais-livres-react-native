module.exports = function(app){
	var sdpublic = app.controllers.sdpublic;
		
	app.post('/sdpublic/getestado', sdpublic.getestado);
	app.post('/sdpublic/getcidade', sdpublic.getcidade);
	app.post('/sdpublic/getnotificacaoall', sdpublic.getnotificacaoall);
	app.post('/sdpublic/getmensagemigreja', sdpublic.getmensagemigreja);
	app.post('/sdpublic/getfuncao', sdpublic.getfuncao);
	app.get('/sdpublic/testenotificacao', sdpublic.testenotificacao);
};
