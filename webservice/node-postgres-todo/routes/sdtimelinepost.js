module.exports = function(app){
	var sdtimelinepost = app.controllers.sdtimelinepost;
	
	app.post('/sdtimelinepost/getpost', sdtimelinepost.getpost);			
	app.post('/sdtimelinepost/timelineperfil', sdtimelinepost.timelineperfil);			
	app.post('/sdtimelinepost/timelineperfilnovo', sdtimelinepost.timelineperfilnovo);			
	app.post('/sdtimelinepost/timelineperfilfotos', sdtimelinepost.timelineperfilfotos);			
	app.post('/sdtimelinepost/timeline', sdtimelinepost.timeline);			
	app.post('/sdtimelinepost/insert', sdtimelinepost.insert);			
	app.post('/sdtimelinepost/curtir', sdtimelinepost.curtir);
	app.post('/sdtimelinepost/descurtir', sdtimelinepost.descurtir);
	app.post('/sdtimelinepost/comentarios', sdtimelinepost.comentarios);
	app.post('/sdtimelinepost/insertcomment', sdtimelinepost.insertcomment);
	app.post('/sdtimelinepost/curtircomment', sdtimelinepost.curtircomment);
	app.post('/sdtimelinepost/removercomentario', sdtimelinepost.removercomentario);	
	app.post('/sdtimelinepost/removerpost', sdtimelinepost.removerpost);
	app.post('/sdtimelinepost/getusuarioslike', sdtimelinepost.getusuarioslike);
	app.post('/sdtimelinepost/getusuariosshared', sdtimelinepost.getusuariosshared);	
	app.post('/sdtimelinepost/getusuarioscommented', sdtimelinepost.getusuarioscommented);		
};
