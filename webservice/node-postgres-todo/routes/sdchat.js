module.exports = function(app){
	var sdchat = app.controllers.sdchat;

	app.post('/sdchat/get', sdchat.get);
	app.post('/sdchat/getamigosonline', sdchat.getAmigosOnline);
	app.post('/sdchat/getchatconversa', sdchat.getChatConversa);

	//app.post('/sdchat/insert', sdchat.insert);
	app.post('/sdchat/chatinsert', sdchat.chatinsert);
};

//curl -H "Content-Type: application/json" -X POST -d '{"cadastro_id":"1","udid":"BF7B6271-6A99-4806-BAEC-97E66EDFFB7B"}' http://radicaislivresantigo.pauloph.a2hosted.com/sdchat/get
