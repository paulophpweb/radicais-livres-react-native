module.exports = new function(){

 /*
  * Metodo de inicializacao, como se fosse o construtor
  * _app pode ser instanciado em todos outros metodos
  */
 var _app = null;
 var _res = null;
 var _params = null;
 var _onComplete = null;
 var _boot = function($app, $params, $res, $onComplete){
  _app = $app;
  _params = $params;
  _res = $res;
  _onComplete = $onComplete;
  _app.sql.boot(_params, _res, _onComplete);
  return this;
 };

 /*
  * Metodo para retornar um ou varios registros da pessoa visitada
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
var _getpost = function($obj){

	var sql = "SELECT sd_p.*,sd_c.foto,sd_c.nome, sd_c.sobrenome,sd_c.genero, sd_c.tokenpush,";
	sql += " CASE WHEN sd_l.post_id > 0 THEN 1 ELSE 0 END AS curtiu,";
	sql += " CASE WHEN sd_ca.cadastro_id_amizade > 0 THEN 1 ELSE 0 END AS amigo,";
	sql += " sd_cc.nome as nome_compartilhado,";
	// pega todos os comentarios do post
	sql += " (SELECT count(sd_timeline.sd_post_comment.comment_id) as comentarioss FROM sd_timeline.sd_post_comment WHERE sd_timeline.sd_post_comment.post_id = sd_p.post_id) as comentarios_db,";
	// pega todos os likes do post
	sql += " (SELECT count(sd_timeline.sd_post_like.like_id) as likess FROM sd_timeline.sd_post_like WHERE sd_timeline.sd_post_like.post_id = sd_p.post_id) as likes_db,";
	// pega todos os compartilhamentos do post
	sql += " (SELECT count(sd_timeline.sd_post.post_id_compartilhado) as compartilhamentoss FROM sd_timeline.sd_post WHERE sd_timeline.sd_post.post_id_compartilhado = sd_p.post_id ) as compartilhamentos_db";

	sql += " FROM sd_timeline.sd_post AS sd_p";
	sql += " INNER JOIN sd_perfil.sd_cadastro AS sd_c ON sd_c.cadastro_id = sd_p.cadastro_id";
	sql += " LEFT JOIN sd_perfil.sd_cadastro AS sd_cc ON sd_cc.cadastro_id = sd_p.cadastro_id_compartilhado";
	sql += " LEFT JOIN sd_timeline.sd_post_like AS sd_l ON sd_l.post_id = sd_p.post_id AND sd_l.cadastro_id = "+$obj.cadastro_id+"";
	sql += " LEFT JOIN sd_perfil.sd_cadastro_amizade AS sd_ca ON sd_ca.cadastro_id_amizade = "+$obj.cadastro_id+"";

	sql+= " WHERE sd_p.post_id IS NOT NULL ";

	sql+= " AND sd_p.post_id = "+$obj.post_id+"";


	sql+= " GROUP BY sd_l.post_id, sd_p.post_id, sd_c.cadastro_id, sd_cc.cadastro_id, sd_ca.cadastro_id_amizade";
	if($obj.order != null){
		sql     += " ORDER BY "+$obj.order;
	}

	if($obj.limit != null){
		sql     += " LIMIT "+$obj.limit;
	} else {
		sql     += " LIMIT 10";
	}

	$obj.query = sql;
	_app.sql.query($obj);
	return this;
};

 /*
  * Metodo para retornar um ou varios registros da pessoa visitada
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
var _get = function($obj){
		var sql = "SELECT sd_p.*,sd_c.foto,sd_c.nome, sd_c.sobrenome,sd_c.genero, sd_c.tokenpush,";
	sql += " CASE WHEN sd_l.post_id > 0 THEN 1 ELSE 0 END AS curtiu,";
	sql += " CASE WHEN sd_ca.cadastro_id_amizade > 0 THEN 1 ELSE 0 END AS amigo,";
	sql += " sd_cc.nome as nome_compartilhado,";
	// pega todos os comentarios do post
	sql += " (SELECT count(sd_timeline.sd_post_comment.comment_id) as comentarioss FROM sd_timeline.sd_post_comment WHERE sd_timeline.sd_post_comment.post_id = sd_p.post_id) as comentarios_db,";
	// pega todos os likes do post
	sql += " (SELECT count(sd_timeline.sd_post_like.like_id) as likess FROM sd_timeline.sd_post_like WHERE sd_timeline.sd_post_like.post_id = sd_p.post_id) as likes_db,";
	// pega todos os compartilhamentos do post
	sql += " (SELECT count(sd_timeline.sd_post.post_id_compartilhado) as compartilhamentoss FROM sd_timeline.sd_post WHERE sd_timeline.sd_post.post_id_compartilhado = sd_p.post_id ) as compartilhamentos_db";

	sql += " FROM sd_timeline.sd_post AS sd_p";
	sql += " INNER JOIN sd_perfil.sd_cadastro AS sd_c ON sd_c.cadastro_id = sd_p.cadastro_id";
	sql += " LEFT JOIN sd_perfil.sd_cadastro AS sd_cc ON sd_cc.cadastro_id = sd_p.cadastro_id_compartilhado";
	sql += " LEFT JOIN sd_timeline.sd_post_like AS sd_l ON sd_l.post_id = sd_p.post_id AND sd_l.cadastro_id = "+$obj.cadastro_id+"";
	sql += " LEFT JOIN sd_perfil.sd_cadastro_amizade AS sd_ca ON sd_ca.cadastro_id_amizade = "+$obj.cadastro_id+"";

	sql+= " WHERE sd_p.post_id IS NOT NULL AND sd_c.excluido = 0 ";

	sql+= " AND sd_p.cadastro_id = "+$obj.cadastro_id_pessoa+"";


	sql+= " GROUP BY sd_l.post_id, sd_p.post_id, sd_c.cadastro_id, sd_cc.cadastro_id, sd_ca.cadastro_id_amizade";
	if($obj.order != null){
		sql     += " ORDER BY "+$obj.order;
	}

	//return $obj.onComplete("pt",sql);

	if($obj.limit != null){
		sql     += " LIMIT "+$obj.limit;
	} else {
		sql     += " LIMIT 10";
	}

	$obj.query = sql;
	_app.sql.query($obj);
	return this;
};

/*
 * Metodo para retornar as fotos de um determinado perfil
 * onComplete:Function Callback Sempre que quiser retornar algo utilize este
 * onError:Function Callback
 * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
 * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
 */
var _getfotos = function($obj){
	var sql = "SELECT sd_p.cadastro_id, arquivo as foto";

	sql += " FROM sd_timeline.sd_post AS sd_p";

	sql+= " WHERE sd_p.post_id IS NOT NULL";
	sql+= " AND sd_p.cadastro_id = "+$obj.cadastro_id_pessoa+"";
	sql+= " AND sd_p.tipo = 'foto' AND sd_p.arquivo IS NOT NULL";

	if($obj.order != null){
		sql     += " ORDER BY "+$obj.order;
	}

	if($obj.limit != null){
		sql     += " LIMIT "+$obj.limit;
	} else {
		sql     += " LIMIT 10";
	}

	$obj.query = sql;
	_app.sql.query($obj);
	return this;
};

/*
 * Metodo para retornar os comentarios de um post
 * onComplete:Function Callback Sempre que quiser retornar algo utilize este
 * onError:Function Callback
 * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
 * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
 */
var _getcomentarios = function($obj){
	// select
	var sql =
	"SELECT "+
		"spc.comment_id, "+
		"spc.cadastro_id, "+
		"spc.post_id, "+
		"spc.data_cadastro, "+
		"(SELECT cadastro_id FROM sd_timeline.sd_post_comment_like WHERE cadastro_id = "+$obj.cadastro_id+" AND comment_id = spc.comment_id) as curtiu, "+
		"spc.texto, "+
		"spc.likes, "+
		"sdp.foto, "+
		"sdp.nome, "+
		"sdp.sobrenome, "+
		"sdp.tokenpush, "+
		"sdp.genero "+
	"FROM "+
		"sd_timeline.sd_post_comment as spc "+
		"LEFT JOIN sd_perfil.sd_cadastro as sdp on sdp.cadastro_id = spc.cadastro_id  "+
	"WHERE "+
		"spc.post_id IS NOT NULL AND sdp.excluido = 0 ";

	if($obj.post_id != null){
		sql     += "AND spc.post_id = "+$obj.post_id+" ";
	}

	if($obj.order != null){
		sql     += " ORDER BY "+$obj.order;
	}

	if($obj.limit != null){
		sql     += " LIMIT "+$obj.limit;
	} else {
		sql     += " LIMIT 10";
	}

	if($obj.offset != null){
		  sql     += " OFFSET "+$obj.offset;
	}

	//return $obj.onComplete("pt",sql);

	$obj.query = sql;
	_app.sql.query($obj);
	return this;
};

 /*
  * Metodo para inserir um comentario
  */
 var _insertcomment = function($obj){
	var sql = "INSERT INTO sd_timeline.sd_post_comment";
	var i = 0;
	var colunas = "(";
	var valores = "(";
	for(obj in _params) {
		valor = String(_params[obj]);
		colunas += (i == 0)?obj:", "+obj;

		//Se o valor for uma função então eu retiro as aspas
		if(valor.indexOf("_sqlFunction") >= 0){
			valor = valor.replace("_sqlFunction", "");
		} else {
			valor = "'"+valor+"'";
		}

		valores += (i == 0)?valor:", "+valor;
		i++;
	}
	colunas += ")";
	valores += ")";

	sql+= colunas+" values "+valores+" RETURNING comment_id";

	$obj.query = sql;
	_app.sql.query($obj);

	return this;
 };

/*
 * Metodo para retornar a linha do tempo com as amizades do usuario
 * onComplete:Function Callback Sempre que quiser retornar algo utilize este
 * onError:Function Callback
 * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
 * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
 */
var _gettimeline = function($obj){
	var sql = "SELECT sd_p.*,sd_c.foto,sd_c.nome, sd_c.sobrenome,sd_c.genero,sd_c.tokenpush, sd_c.email,";
	sql += " CASE WHEN sd_l.post_id > 0 THEN 1 ELSE 0 END AS curtiu,";
	sql += " sd_cc.nome as nome_compartilhado,";
	// pega todos os comentarios do post
	sql += " (SELECT count(sd_timeline.sd_post_comment.comment_id) as comentarioss FROM sd_timeline.sd_post_comment WHERE sd_timeline.sd_post_comment.post_id = sd_p.post_id) as comentarios_db,";
	// pega todos os likes do post
	sql += " (SELECT count(sd_timeline.sd_post_like.like_id) as likess FROM sd_timeline.sd_post_like WHERE sd_timeline.sd_post_like.post_id = sd_p.post_id) as likes_db,";
	// pega todos os compartilhamentos do post
	sql += " (SELECT count(sd_timeline.sd_post.post_id_compartilhado) as compartilhamentoss FROM sd_timeline.sd_post WHERE sd_timeline.sd_post.post_id_compartilhado = sd_p.post_id ) as compartilhamentos_db";

	sql += " FROM sd_timeline.sd_post AS sd_p";
	sql += " INNER JOIN sd_perfil.sd_cadastro AS sd_c ON sd_c.cadastro_id = sd_p.cadastro_id";
	sql += " LEFT JOIN sd_perfil.sd_cadastro AS sd_cc ON sd_cc.cadastro_id = sd_p.cadastro_id_compartilhado";
	sql += " LEFT JOIN sd_timeline.sd_post_like AS sd_l ON sd_l.post_id = sd_p.post_id AND sd_l.cadastro_id = "+$obj.cadastro_id+"";

	sql+= " WHERE sd_p.post_id IS NOT NULL AND sd_c.excluido = 0 ";
	if($obj.is_root == 1){
		sql+= " AND (sd_p.cadastro_id NOT IN (SELECT cadastro_id_amizade FROM sd_perfil.sd_cadastro_bloqueio WHERE cadastro_id = "+$obj.cadastro_id+"))";
	}else{
		sql+= " AND (sd_p.cadastro_id NOT IN (SELECT cadastro_id_amizade FROM sd_perfil.sd_cadastro_bloqueio WHERE cadastro_id = "+$obj.cadastro_id+")";
		sql+= " AND sd_p.cadastro_id IN (SELECT cadastro_id_amizade FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id+"))";
	}
	sql+= " OR sd_p.cadastro_id = "+$obj.cadastro_id+" OR sd_c.is_root = 1";

	sql+= " GROUP BY sd_l.post_id, sd_p.post_id, sd_c.cadastro_id, sd_cc.cadastro_id";

	if($obj.order != null){
		sql     += " ORDER BY "+$obj.order;
	}

	if($obj.limit != null){
		sql     += " LIMIT "+$obj.limit;
	} else {
		sql     += " LIMIT 10";
	}

	if($obj.offset != null){
		  sql     += " OFFSET "+$obj.offset;
	}

	$obj.query = sql;
	_app.sql.query($obj);
	return this;
};

 /*
  * Metodo para inserir um novo registro
  */
var _insert = function($obj){
	var sql = "INSERT INTO sd_timeline.sd_post";
	var i = 0;
	var colunas = "(";
	var valores = "(";
	for(obj in _params) {
		valor = String(_params[obj]);
		colunas += (i == 0)?obj:", "+obj;

		//Se o valor for uma função então eu retiro as aspas
		if(valor.indexOf("_sqlFunction") >= 0){
			valor = valor.replace("_sqlFunction", "");
		} else {
			valor = "'"+valor+"'";
		}

		valores += (i == 0)?valor:", "+valor;
		i++;
	}
	colunas += ")";
	valores += ")";

	sql+= colunas+" values "+valores+" RETURNING post_id";
	$obj.query = sql;
	_app.sql.query($obj);

	return this;
 };

 /*
  * Metodo para remover comentario
  */
 var _removercomentario = function($obj){
  var sql = "DELETE FROM sd_timeline.sd_post_comment_like";

  //O where é obrigatório
  sql += " WHERE comment_id = "+$obj.comment_id+";";

  sql += " DELETE FROM sd_timeline.sd_post_comment";

  //O where é obrigatório
  sql += " WHERE "+$obj.where+";";

  sql += " UPDATE sd_timeline.sd_post SET comentarios = comentarios - 1";

  //O where é obrigatório
  sql += " WHERE post_id =  "+$obj.post_id+";";

  $obj.query = sql;
  _app.sql.query($obj);

  return this;
 };

  /*
  * Metodo para remover post
  */
 var _removerpost = function($obj){

  var sql = " DELETE FROM sd_timeline.sd_post_comment_like";

  //O where é obrigatório
  sql += " WHERE post_id = "+$obj.post_id+" ;";


  sql += " DELETE FROM sd_perfil.sd_notificacao";

  //O where é obrigatório
  sql += " WHERE post_id = "+$obj.post_id+" ;";

  sql += " DELETE FROM sd_timeline.sd_post_comment";

  //O where é obrigatório
  sql += " WHERE post_id = "+$obj.post_id+" ;";

  sql += "DELETE FROM sd_timeline.sd_post_like";

  //O where é obrigatório
  sql += " WHERE post_id = "+$obj.post_id+" ;";

  sql += " DELETE FROM sd_timeline.sd_post";

  //O where é obrigatório
  sql += " WHERE post_id = "+$obj.post_id+" AND cadastro_id = "+$obj.cadastro_id+" ;";

  sql += " DELETE FROM sd_timeline.sd_post";

  //O where é obrigatório
  sql += " WHERE post_id = "+$obj.post_id+" AND cadastro_id = "+$obj.cadastro_id+" ;";


  $obj.query = sql;
  _app.sql.query($obj);

  return this;
 };

  /*
  * Metodo para atualizar um post
  */
 var _update = function($obj){
  var sql = "UPDATE sd_timeline.sd_post SET ";
  var i = 0;
  var sets = '';

  for(var obj in $obj.update) {
   coluna = obj;
   valor = $obj.update[obj].toString();

   //Se o valor for uma função então eu retiro as aspas
   if(valor.indexOf("_sqlFunction") >= 0){
    valor = valor.replace("_sqlFunction", "");
   } else {
    valor = "'"+valor+"'";
   }

   set = ""+obj+" = "+valor+"";
   sets += (i == 0)?set:", "+set;

   i++;
  }

  //O where é obrigatório
  sql += sets+" WHERE "+$obj.where+" RETURNING sd_timeline.sd_post.post_id";

  $obj.query = sql;
  _app.sql.query($obj);

  return this;
 };


 /*
 * Metodo para retornar um ou varios registros
 * onComplete:Function Callback Sempre que quiser retornar algo utilize este
 * onError:Function Callback
 * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
 * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
 */
	var _getUsuarioShared = function($obj){

		// select
		var sql =
		"SELECT "+
			"DISTINCT ON (sd_c.cadastro_id) "+
			"sd_c.* "+
		"FROM "+
			"sd_timeline.sd_post sd_p "+
			"LEFT JOIN sd_perfil.sd_cadastro AS sd_c ON sd_c.cadastro_id = sd_p.cadastro_id  "+
		"WHERE "+
			"sd_p.post_id IS NOT NULL ";
		if($obj.post_id != null){
			sql     += " AND sd_p.post_id_compartilhado = "+$obj.post_id;
		}
		if($obj.order != null){
			sql     += " ORDER BY sd_c.cadastro_id, "+$obj.order;
		}
		if($obj.limit != null){
			sql     += " LIMIT "+$obj.limit;
		} else {
			sql     += " LIMIT 10";
		}

		$obj.query = sql;
		_app.sql.query($obj);
		return this;
	};
/*
 * Metodo para retornar um ou varios registros
 * onComplete:Function Callback Sempre que quiser retornar algo utilize este
 * onError:Function Callback
 * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
 * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
 */
	var _getUsuarioCommented = function($obj){

		// select
		var sql =
		"SELECT "+
			"DISTINCT ON (sd_c.cadastro_id) "+
			"sd_c.* "+

		"FROM "+
			"sd_timeline.sd_post_comment sd_pc "+
			"LEFT JOIN sd_perfil.sd_cadastro AS sd_c ON sd_c.cadastro_id = sd_pc.cadastro_id  "+
		"WHERE "+
			"sd_pc.post_id IS NOT NULL ";
		if($obj.post_id != null){
			sql     += " AND sd_pc.post_id = "+$obj.post_id;
		}
		if($obj.order != null){
			sql     += " ORDER BY sd_c.cadastro_id, "+$obj.order;
		}
		if($obj.limit != null){
			sql     += " LIMIT "+$obj.limit;
		} else {
			sql     += " LIMIT 10";
		}

		$obj.query = sql;
		_app.sql.query($obj);
		return this;
	};

 return {get:_get, getpost: _getpost, getfotos:_getfotos,gettimeline:_gettimeline, insert:_insert, insertcomment:_insertcomment, boot:_boot,getcomentarios:_getcomentarios,removercomentario:_removercomentario,removerpost:_removerpost,update:_update,getUsuarioShared:_getUsuarioShared,getUsuarioCommented:_getUsuarioCommented};
};
