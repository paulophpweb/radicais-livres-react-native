module.exports = new function(){
	
	/*
  * Metodo de inicializacao, como se fosse o construtor
  * _app pode ser instanciado em todos outros metodos
  */
 var _app = null;
 var _res = null;
 var _params = null;
 var _onComplete = null;
 var _boot = function($app, $params, $res, $onComplete){
  _app = $app;
  _params = $params;
  _res = $res;
  _onComplete = $onComplete;
  _app.sql.boot(_params, _res, _onComplete);
  return this;
 };
		
	/*
	 * Metodo para retornar um ou varios registros
	 * onComplete:Function Callback Sempre que quiser retornar algo utilize este
	 * onError:Function Callback
	 * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
	 * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
	 */
	var _get = function($obj){
		var sql = "SELECT * FROM sd_timeline.sd_post_like";
		
		if($obj.where != null){
			sql     += " WHERE "+$obj.where;
		}
		
		if($obj.order != null){
			sql     += " ORDER BY "+$obj.order;
		}
		
		if($obj.limit != null){
			sql     += " LIMIT "+$obj.limit;
		} else {
			sql     += " LIMIT 10";
		}	
		
		$obj.query = sql;		
		_app.sql.query($obj);
		return this;
	};
	
	/*
	 * Metodo para retornar um ou varios registros
	 * onComplete:Function Callback Sempre que quiser retornar algo utilize este
	 * onError:Function Callback
	 * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
	 * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
	 */
	var _getUsuarioLikes = function($obj){
		
		// select
		var sql =
		"SELECT "+ 
			"DISTINCT ON (sd_c.cadastro_id) "+
			"sd_c.* "+
		"FROM "+
			"sd_timeline.sd_post_like sd_pl "+
			"LEFT JOIN sd_perfil.sd_cadastro AS sd_c ON sd_c.cadastro_id = sd_pl.cadastro_id  "+
		"WHERE "+ 
			"sd_pl.post_id IS NOT NULL ";
		if($obj.post_id != null){
			sql     += " AND sd_pl.post_id = "+$obj.post_id;
		}
		if($obj.order != null){
			sql     += " ORDER BY sd_c.cadastro_id, "+$obj.order;
		}
		if($obj.limit != null){
			sql     += " LIMIT "+$obj.limit;
		} else {
			sql     += " LIMIT 10";
		}

		$obj.query = sql;		
		_app.sql.query($obj);
		return this;
	};
		
	/*
	 * Metodo para inserir um novo registro
	 */
	var _insert = function($obj){		
		var sql = "INSERT INTO sd_timeline.sd_post_like";
		var i = 0;
		var colunas = "(";
		var valores = "(";
		for(obj in _params) {			
			valor = String(_params[obj]);			
			colunas += (i == 0)?obj:", "+obj;
			
			//Se o valor for uma função então eu retiro as aspas			
			if(valor.indexOf("_sqlFunction") >= 0){
				valor = valor.replace("_sqlFunction", "");
			} else {
				valor = "'"+valor+"'";
			}
			
			valores += (i == 0)?valor:", "+valor;
			i++;			
		}
		colunas += ")";
		valores += ")";
		
		sql+= colunas+" values "+valores+" RETURNING like_id";		
		
		$obj.query = sql;				
		_app.sql.query($obj);
		
		return this;		
	};
	
	  /*
	  * Metodo para deslike
	  */
	 var _descurtir = function($obj){  
	  if($obj.post_id && $obj.cadastro_id){

		  var sql = " DELETE FROM sd_timeline.sd_post_like";
		  
		  //O where é obrigatório
		  sql += " WHERE post_id = "+$obj.post_id+" AND cadastro_id = "+$obj.cadastro_id+"  ;";
		  
	
		  $obj.query = sql;  
		  _app.sql.query($obj);
	  }
	  
	  return this;    
	 };
		
	return {get:_get, insert:_insert, boot:_boot, getUsuarioLikes:_getUsuarioLikes, descurtir:_descurtir};
};
