module.exports = new function(){
 
 /*
  * Metodo de inicializacao, como se fosse o construtor
  * _app pode ser instanciado em todos outros metodos
  */
 var _app = null;
 var _res = null;
 var _params = null;
 var _onComplete = null;
 var _boot = function($app, $params, $res, $onComplete){
  _app = $app;
  _params = $params;
  _res = $res;
  _onComplete = $onComplete;
  _app.sql.boot(_params, _res, _onComplete);
  return this;
 };
  
 /*
  * Metodo para retornar um ou varios registros
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
 var _getusuarioslatlng = function($obj){
	 var sql = "SELECT sd_c.* ";
	 sql += ", sd_i.nome as igreja_nome ";
	  sql += " FROM sd_perfil.sd_cadastro AS sd_c";
	  
	  sql += " LEFT JOIN sd_perfil.sd_igreja AS sd_i ON sd_i.igreja_id = sd_c.igreja_id";
	  sql += " LEFT JOIN sd_perfil.sd_formacao AS sd_f ON sd_f.formacao_id = sd_c.formacao_id";
	  sql += " LEFT JOIN sd_perfil.sd_profissao AS sd_p ON sd_p.profissao_id = sd_c.profissao_id";  
	  sql += " LEFT JOIN sd_perfil.sd_relacionamento AS sd_r ON sd_r.relacionamento_id = sd_c.relacionamento_id";
	  sql += " LEFT JOIN sd_perfil.sd_cadastro AS sd_cr ON sd_cr.cadastro_id = sd_c.cadastro_id_relacionamento";
	  sql += " WHERE sd_c.cadastro_id IS NOT NULL";
	  sql += " AND sd_c.lat IS NOT NULL AND sd_c.lng IS NOT NULL";
	  if($obj.latitudeUm != null && $obj.latitudeTres != null && $obj.longitudeUm != null && $obj.longitudeDois != null){
		  sql += " AND (ABS(sd_c.lat::numeric) >= "+Math.abs($obj.latitudeUm)+"  AND ABS(sd_c.lat::numeric) <= "+Math.abs($obj.latitudeTres)+")";	
		  sql += " AND (ABS(sd_c.lng::numeric) <= "+Math.abs($obj.longitudeUm)+"  AND ABS(sd_c.lng::numeric) >= "+Math.abs($obj.longitudeDois)+")";	
	  }
	  
	  if($obj.igreja_id != null){
		  sql += " AND sd_c.igreja_id = "+$obj.igreja_id;
	  }
	  
	  if($obj.nome_pessoa != null){
		  sql += " AND upper(remove_acento(sd_c.nome)) like upper(remove_acento('%"+$obj.nome_pessoa+"%')) ";
	  }

	  if($obj.is_root == 0)
		  sql += " AND (sd_c.cadastro_id IN (SELECT cadastro_id_amizade FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id+") OR sd_c.is_root = 1) ";
	        
	  if($obj.where != null){
		  sql     += " WHERE "+$obj.where;
	  }
	    
	  if($obj.order != null){
		  sql     += " ORDER BY "+$obj.order;
	  }
	  
	  if($obj.limit != null){
		  sql     += " LIMIT "+$obj.limit;
	  } else {
		  sql     += " LIMIT 10";
	  } 
	  
	  if($obj.offset != null){
		  sql     += " OFFSET "+$obj.offset;
	  }
	    
	  $obj.query = sql;  
	  _app.sql.query($obj);
	  return this;
 };
 
 return {getusuarioslatlng:_getusuarioslatlng, boot:_boot};
};
