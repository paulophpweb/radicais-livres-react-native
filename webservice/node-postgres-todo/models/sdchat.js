module.exports = new function(){

 /*
  * Metodo de inicializacao, como se fosse o construtor
  * _app pode ser instanciado em todos outros metodos
  */
 var _app = null;
 var _res = null;
 var _params = null;
 var _onComplete = null;
 var _boot = function($app, $params, $res, $onComplete){
  _app = $app;
  _params = $params;
  _res = $res;
  _onComplete = $onComplete;
  _app.sql.boot(_params, _res, _onComplete);
  return this;
 };

 /*
  * Metodo para retornar um ou varios registros
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
 var _get = function($obj){
	 	// select
		var sql =
		"SELECT "+
			"sd_e.* "+
			", sd_c.nome as cidade "+
			", sd_es.nome as estado "+
			", sd_ca.nome as cadastro_nome "+
			", sd_ca.sobrenome as cadastro_sobrenome "+
			", sd_ep.total "+
			", CASE WHEN sd_com.tipo = 'comparecerei'  THEN 1 ELSE 0 END AS comparecerei "+
			", CASE WHEN sd_com.tipo = 'talvez'  THEN 1 ELSE 0 END AS talvez "+
			", CASE WHEN sd_com.tipo = 'nao_posso_ir' THEN 1 ELSE 0 END AS nao_posso_ir "+
		"FROM "+
			"sd_evento.sd_evento as sd_e "+
			"LEFT JOIN sd_perfil.sd_cadastro as sd_ca on sd_ca.cadastro_id = sd_e.cadastro_id "+
			"LEFT JOIN public.sd_cidade as sd_c on sd_c.cidade_id = sd_e.cidade_id "+
			"LEFT JOIN public.sd_estado as sd_es on sd_es.estado_id = sd_c.estado_id "+
			"LEFT JOIN (SELECT " +
			"					sd_e.evento_id, " +
			"					count(case when sd_ep.tipo = 'comparecerei' then sd_ep.cadastro_id end) as total " +
			" 			FROM " +
			"					sd_evento.sd_evento as sd_e " +
			"			LEFT JOIN sd_evento.sd_evento_pessoa AS sd_ep " +
			"					ON sd_e.evento_id = sd_ep.evento_id " +
			"			WHERE " +
			"					sd_e.cadastro_id IS NOT NULL group by sd_e.evento_id " +
			"			) as sd_ep ON (sd_e.evento_id = sd_ep.evento_id) "+
			"LEFT JOIN (SELECT sd_ep.evento_id, sd_ep.tipo  FROM sd_evento.sd_evento_pessoa AS sd_ep WHERE sd_ep.cadastro_id = "+$obj.cadastro_id+"  ) as sd_com ON (sd_com.evento_id = sd_ep.evento_id) "+
			($obj.is_root == 0 ? "INNER JOIN sd_perfil.sd_cadastro_amizade as sd_am on sd_am.cadastro_id = sd_e.cadastro_id AND sd_am.cadastro_id_amizade = "+$obj.cadastro_id+" " : "")+
		"WHERE "+
			"sd_e.evento_id IS NOT NULL ";
		if($obj.tipo == "proximo")
			sql += " AND sd_e.data >= now() ";
		if($obj.tipo == "anterior")
			sql += " AND sd_e.data <= now() ";

		if($obj.where != null){
			  sql     += $obj.where;
		}

		if($obj.order != null){
			sql     += " ORDER BY "+$obj.order;
		}

		if($obj.limit != null){
			sql     += " LIMIT "+$obj.limit;
		} else {
			sql     += " LIMIT 10";
		}

		if($obj.offset != null){
			  sql     += " OFFSET "+$obj.offset;
		}

		$obj.query = sql;
		_app.sql.query($obj);
		return this;
 };

 /*
  * Metodo para retornar um ou varios registros
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
 var _getchatconversa = function($obj){
	 	// select
		var sql =
		"SELECT "+
			"sd_ch.* "+
			", sd_ca.nome "+
			", sd_ca.sobrenome "+
			", sd_caam.nome as amigo_nome "+
			", sd_caam.nome as amigo_sobrenome "+
		"FROM "+
			"sd_chat.sd_chat_pessoa as sd_ch "+
			"LEFT JOIN sd_perfil.sd_cadastro as sd_ca on sd_ca.cadastro_id = sd_ch.cadastro_id "+
			"LEFT JOIN sd_perfil.sd_cadastro as sd_caam on sd_caam.cadastro_id = sd_ch.cadastro_id_amizade "+
		"WHERE "+
			"sd_ch.chat_pessoa_id IS NOT NULL "+
			($obj.cadastro_id_amizade && $obj.cadastro_id ? " AND (sd_ch.cadastro_id = "+$obj.cadastro_id+" AND sd_ch.cadastro_id_amizade = "+$obj.cadastro_id_amizade+") " : "")+
			($obj.cadastro_id_amizade && $obj.cadastro_id ? " OR (sd_ch.cadastro_id = "+$obj.cadastro_id_amizade+" AND sd_ch.cadastro_id_amizade = "+$obj.cadastro_id+") " : "");


		if($obj.where != null){
			  sql     += $obj.where;
		}

		if($obj.order != null){
			sql     += " ORDER BY "+$obj.order;
		}

		if($obj.limit != null){
			sql     += " LIMIT "+$obj.limit;
		} else {
			sql     += " LIMIT 50";
		}

		if($obj.offset != null){
			  sql     += " OFFSET "+$obj.offset;
		}
		// atualiza as mensagem para lida
		sql += "; UPDATE sd_chat.sd_chat_pessoa SET st = 1, data_recebido = '"+$obj.ult_login+"'  WHERE cadastro_id_amizade = "+$obj.cadastro_id+" AND cadastro_id =  "+$obj.cadastro_id_amizade+" AND data_recebido is null ; ";

		$obj.query = sql;
		_app.sql.query($obj);
		return this;
 };

 /*
  * Metodo para retornar um ou varios registros
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
 var _getAmigosOnline = function($obj){
  var sql = "SELECT sd_c.* ";
  sql += ", sd_custom.total as nao_lido ";
  if($obj.ult_login)
	  sql += ", age('"+$obj.ult_login+"',sd_c.ult_login) as teste "
  // se for menor que 5 minutos ele esta online
  sql += ", CASE WHEN age('"+$obj.ult_login+"',sd_c.ult_login) >= '1 seconds' AND age('"+$obj.ult_login+"',sd_c.ult_login) <= '9 minutes' AND sd_c.logado = 1 THEN 1 ELSE 0 END AS online ";
  //se for menor que 10 minutos ele esta ausente
  sql += ", CASE WHEN age('"+$obj.ult_login+"',sd_c.ult_login) >= '10 minutes' AND age('"+$obj.ult_login+"',sd_c.ult_login) <= '14 minutes' AND sd_c.logado = 1  THEN 1 ELSE 0 END AS ausente ";
  //se for menor que 15 minutos ele esta offline
  sql += ", CASE WHEN age('"+$obj.ult_login+"',sd_c.ult_login) >= '15 minutes' OR sd_c.logado = 0  THEN 1 ELSE 0 END AS offline ";
  sql += " FROM sd_perfil.sd_cadastro AS sd_c";

  sql += " LEFT JOIN sd_perfil.sd_igreja AS sd_i ON sd_i.igreja_id = sd_c.igreja_id";
  sql += " LEFT JOIN sd_perfil.sd_formacao AS sd_f ON sd_f.formacao_id = sd_c.formacao_id";
  sql += " LEFT JOIN sd_perfil.sd_profissao AS sd_p ON sd_p.profissao_id = sd_c.profissao_id";
  sql += " LEFT JOIN sd_perfil.sd_relacionamento AS sd_r ON sd_r.relacionamento_id = sd_c.relacionamento_id";
  sql += " LEFT JOIN sd_perfil.sd_cadastro AS sd_cr ON sd_cr.cadastro_id = sd_c.cadastro_id_relacionamento";
  sql += " LEFT JOIN (SELECT cadastro_id, count(cadastro_id_amizade) as total FROM sd_chat.sd_chat_pessoa WHERE cadastro_id_amizade = "+$obj.cadastro_id+" AND st = 0 GROUP BY cadastro_id) as sd_custom ON sd_c.cadastro_id = sd_custom.cadastro_id ";
  sql += " WHERE sd_c.cadastro_id IS NOT NULL";
  if($obj.is_root == 0)
	  sql += " AND sd_c.cadastro_id IN (SELECT cadastro_id_amizade FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id+") OR sd_c.is_root = 1 ";

  if($obj.where != null){
	  sql     += " WHERE "+$obj.where;
  }

  if($obj.order != null){
	  sql     += " ORDER BY "+$obj.order;
  }

  if($obj.limit != null){
	  sql     += " LIMIT "+$obj.limit;
  } else {
	  sql     += " LIMIT 10";
  }

  if($obj.offset != null){
	  sql     += " OFFSET "+$obj.offset;
  }

  $obj.query = sql;
  _app.sql.query($obj);
  return this;
 };


 /*
  * Metodo para inserir um novo registro
  */
 var _insert = function($obj){
  var sql = "INSERT INTO sd_chat.sd_chat_pessoa";
  var i = 0;
  var colunas = "(";
  var valores = "(";
  for(obj in _params) {
   valor = String(_params[obj]);
   colunas += (i == 0)?obj:", "+obj;

   //Se o valor for uma função então eu retiro as aspas
   if(valor.indexOf("_sqlFunction") >= 0){
    valor = valor.replace("_sqlFunction", "");
   } else {
    valor = "'"+valor+"'";
   }

   valores += (i == 0)?valor:", "+valor;
   i++;
  }
  colunas += ")";
  valores += ")";

  sql+= colunas+" values "+valores+" RETURNING chat_pessoa_id";
  $obj.query = sql;
  _app.sql.query($obj);

  return this;
 };

 /*
  * Metodo para atualizar um registro
  */
 var _update = function($obj){
  var sql = "UPDATE sd_chat.sd_chat_pessoa SET ";
  var i = 0;
  var sets = '';

  for(var obj in $obj.update) {
   coluna = obj;
   valor = $obj.update[obj].toString();

   //Se o valor for uma função então eu retiro as aspas
   if(valor.indexOf("_sqlFunction") >= 0){
    valor = valor.replace("_sqlFunction", "");
   } else {
    valor = "'"+valor+"'";
   }

   set = ""+obj+" = "+valor+"";
   sets += (i == 0)?set:", "+set;

   i++;
  }

  //O where é obrigatório
  sql += sets+" WHERE "+$obj.where+" RETURNING sd_chat.sd_chat_pessoa.chat_pessoa_id";

  $obj.query = sql;
  _app.sql.query($obj);

  return this;
 };

 return {get:_get, getchatconversa:_getchatconversa, insert:_insert, update:_update, getAmigosOnline:_getAmigosOnline, boot:_boot};
};
