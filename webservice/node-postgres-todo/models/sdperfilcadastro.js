module.exports = new function(){

 /*
  * Metodo de inicializacao, como se fosse o construtor
  * _app pode ser instanciado em todos outros metodos
  */
 var _app = null;
 var _res = null;
 var _params = null;
 var _onComplete = null;
 var _boot = function($app, $params, $res, $onComplete){
  _app = $app;
  _params = $params;
  _res = $res;
  _onComplete = $onComplete;
  _app.sql.boot(_params, _res, _onComplete);
  return this;
 };

 /*
  * Metodo para retornar um ou varios registros
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
 var _get = function($obj){
  var sql = "SELECT sd_c.*, sd_i.nome AS igreja_nome, sd_ci.nome as cidade, sd_es.nome as estado, sd_fu.nome as funcao,";
  sql += "sd_f.nome AS formacao_nome, sd_p.nome AS profissao_nome,";
  sql += "sd_r.nome AS estado_civil,";
  sql += "sd_cr.nome || ' ' || sd_cr.sobrenome AS relacionamento";

  if($obj.cadastro_id && $obj.cadastro_id_pessoa){
  // verifica e esta pesquisando um amigo e se a pessoa ja e amigo e tbm se ja solicitou amizade
  sql += ",(SELECT count(cadastro_id) FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id+" AND cadastro_id_amizade = "+$obj.cadastro_id_pessoa+" ) as amigo";
  sql += ",(SELECT count(cadastro_id) FROM sd_perfil.sd_cadastro_solicitacao WHERE cadastro_id = "+$obj.cadastro_id+" AND cadastro_id_solicitado = "+$obj.cadastro_id_pessoa+" ) as solicitou";
  sql += ",(SELECT count(cadastro_id) FROM sd_perfil.sd_cadastro_bloqueio WHERE cadastro_id = "+$obj.cadastro_id+" AND cadastro_id_amizade = "+$obj.cadastro_id_pessoa+" ) as bloqueado";
  sql += ",(SELECT count(cadastro_id) FROM sd_perfil.sd_cadastro_solicitacao WHERE cadastro_id_solicitado = "+$obj.cadastro_id+" AND cadastro_id = "+$obj.cadastro_id_pessoa+" ) as aguardando";
  sql += ",(SELECT solicitacao_id FROM sd_perfil.sd_cadastro_solicitacao WHERE cadastro_id_solicitado = "+$obj.cadastro_id+" AND cadastro_id = "+$obj.cadastro_id_pessoa+" ) as solicitacao_id";
  }


  sql += " FROM sd_perfil.sd_cadastro AS sd_c";

  sql += " LEFT JOIN sd_perfil.sd_igreja AS sd_i ON sd_i.igreja_id = sd_c.igreja_id";
  sql += " LEFT JOIN sd_perfil.sd_formacao AS sd_f ON sd_f.formacao_id = sd_c.formacao_id";
  sql += " LEFT JOIN sd_perfil.sd_profissao AS sd_p ON sd_p.profissao_id = sd_c.profissao_id";
  sql += " LEFT JOIN sd_perfil.sd_relacionamento AS sd_r ON sd_r.relacionamento_id = sd_c.relacionamento_id";
  sql += " LEFT JOIN sd_perfil.sd_cadastro AS sd_cr ON sd_cr.cadastro_id = sd_c.cadastro_id_relacionamento";
  sql += " LEFT JOIN public.sd_cidade AS sd_ci ON sd_c.cidade_id = sd_ci.cidade_id";
  sql += " LEFT JOIN public.sd_estado AS sd_es ON sd_ci.estado_id = sd_es.estado_id";
  sql += " LEFT JOIN sd_perfil.sd_funcao AS sd_fu ON sd_c.funcao_id = sd_fu.funcao_id";

  if($obj.where != null){
	  sql     += " WHERE "+$obj.where;
  }

  if($obj.order != null){
	  sql     += " ORDER BY "+$obj.order;
  }

  if($obj.limit != null){
	  sql     += " LIMIT "+$obj.limit;
  } else {
	  sql     += " LIMIT 10";
  }

  $obj.query = sql;


  _app.sql.query($obj);
  return this;
 };

 /*
  * Metodo para retornar um ou varios registros
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
 var _findAmizades = function($obj){
  var sql = "SELECT sd_c.*, sd_ci.nome as cidade, sd_es.nome as estado, sd_cr.nome as nome_relacionado, sd_cr.sobrenome as sobrenome_relacionado, ";
  sql += " sd_r.nome as relacionamento, sd_fu.nome as funcao ";

  sql += " FROM sd_perfil.sd_cadastro AS sd_c";

  sql += " LEFT JOIN sd_perfil.sd_igreja AS sd_i ON sd_i.igreja_id = sd_c.igreja_id";
  sql += " LEFT JOIN sd_perfil.sd_formacao AS sd_f ON sd_f.formacao_id = sd_c.formacao_id";
  sql += " LEFT JOIN sd_perfil.sd_profissao AS sd_p ON sd_p.profissao_id = sd_c.profissao_id";
  sql += " LEFT JOIN sd_perfil.sd_relacionamento AS sd_r ON sd_r.relacionamento_id = sd_c.relacionamento_id";
  sql += " LEFT JOIN sd_perfil.sd_cadastro AS sd_cr ON sd_cr.cadastro_id = sd_c.cadastro_id_relacionamento";
  sql += " LEFT JOIN public.sd_cidade AS sd_ci ON sd_c.cidade_id = sd_ci.cidade_id";
  sql += " LEFT JOIN public.sd_estado AS sd_es ON sd_ci.estado_id = sd_es.estado_id";
  sql += " LEFT JOIN sd_perfil.sd_funcao AS sd_fu ON sd_c.funcao_id = sd_fu.funcao_id";

  if($obj.where != null){
	  sql     += " WHERE "+$obj.where;
  }

  if($obj.order != null){
	  sql     += " ORDER BY "+$obj.order;
  }

  if($obj.limit != null){
	  sql     += " LIMIT "+$obj.limit;
  } else {
	  sql     += " LIMIT 10";
  }

  if($obj.offset != null){
	  sql     += " OFFSET "+$obj.offset;
  }

  $obj.query = sql;
  _app.sql.query($obj);
  return this;
 };

 /*
  * Metodo para retornar dados da tela Sobre o perfil
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
 var _sobre = function($obj){
  var sql = "SELECT sd_c.genero, extract(year from age(sd_c.data_nascimento)) || ' anos' AS idade, sd_i.nome AS igreja_nome, sd_ci.nome as cidade, sd_es.nome as estado,";
  sql += "sd_f.nome AS formacao_nome, sd_p.nome AS profissao_nome, sd_fu.nome as funcao, ";
  sql += "sd_r.nome AS estado_civil,";
  sql += "sd_cr.nome || ' ' || sd_cr.sobrenome AS relacionamento";

  sql += " FROM sd_perfil.sd_cadastro AS sd_c";

  sql += " LEFT JOIN sd_perfil.sd_igreja AS sd_i ON sd_i.igreja_id = sd_c.igreja_id";
  sql += " LEFT JOIN sd_perfil.sd_formacao AS sd_f ON sd_f.formacao_id = sd_c.formacao_id";
  sql += " LEFT JOIN sd_perfil.sd_profissao AS sd_p ON sd_p.profissao_id = sd_c.profissao_id";
  sql += " LEFT JOIN sd_perfil.sd_relacionamento AS sd_r ON sd_r.relacionamento_id = sd_c.relacionamento_id";
  sql += " LEFT JOIN sd_perfil.sd_cadastro AS sd_cr ON sd_cr.cadastro_id = sd_c.cadastro_id_relacionamento";
  sql += " LEFT JOIN public.sd_cidade AS sd_ci ON sd_c.cidade_id = sd_ci.cidade_id";
  sql += " LEFT JOIN public.sd_estado AS sd_es ON sd_ci.estado_id = sd_es.estado_id";
  sql += " LEFT JOIN sd_perfil.sd_funcao AS sd_fu ON sd_c.funcao_id = sd_fu.funcao_id";

  if($obj.where != null){
	  sql     += " WHERE "+$obj.where;
  }

  " ORDER BY sd_c.cadastro_id DESC";

  if($obj.limit != null){
	  sql     += " LIMIT "+$obj.limit;
  } else {
	  sql     += " LIMIT 10";
  }

  $obj.query = sql;
  _app.sql.query($obj);
  return this;
 };

 /*
  * Metodo para retornar um ou varios registros sugeridos para o perfil
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
 var _getsugestao = function($obj){
  var sql = "SELECT sugestao_id AS cadastro_id, cadastro.nome, cadastro.sobrenome, cadastro.foto, COUNT(*) AS total_amigos_comum";

  sql += " FROM";
  sql += "(";
  sql += "SELECT ca.cadastro_id_amizade AS sugestao_id, ca.cadastro_id AS amigo_comum";
  sql += " FROM sd_perfil.sd_cadastro_amizade AS ca";
  sql += " WHERE (";
  sql += " ca.cadastro_id NOT IN (SELECT cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id_amizade = "+$obj.cadastro_id+" UNION SELECT cadastro_id_amizade AS cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id+")";
  sql += " OR";
  sql += " ca.cadastro_id_amizade NOT IN (SELECT cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id_amizade = "+$obj.cadastro_id+" UNION SELECT cadastro_id_amizade AS cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id+")";
  sql += " )";
  sql += " AND ca.cadastro_id <> "+$obj.cadastro_id+"";
  sql += " AND ca.cadastro_id_amizade <> "+$obj.cadastro_id+"";
  sql += " AND ca.cadastro_id IN (SELECT cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id_amizade = "+$obj.cadastro_id+")";


  sql += " UNION";

  sql += " SELECT ca.cadastro_id AS sugestao_id, ca.cadastro_id_amizade AS amigo_comum";
  sql += " FROM sd_perfil.sd_cadastro_amizade AS ca";
  sql += " WHERE (";
  sql += " ca.cadastro_id NOT IN (SELECT cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id_amizade = "+$obj.cadastro_id+" UNION SELECT cadastro_id_amizade AS cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id+")";
  sql += " OR";
  sql += " ca.cadastro_id_amizade NOT IN (SELECT cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id_amizade = "+$obj.cadastro_id+" UNION SELECT cadastro_id_amizade AS cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id+")";
  sql += " )";
  sql += " AND ca.cadastro_id <> "+$obj.cadastro_id+"";
  sql += " AND ca.cadastro_id_amizade <> "+$obj.cadastro_id+"";
  sql += " AND ca.cadastro_id_amizade IN (SELECT cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id_amizade = "+$obj.cadastro_id+")";

  sql += " ) AS sugestao, sd_perfil.sd_cadastro AS cadastro";

  sql += " WHERE ";
  sql += " cadastro.cadastro_id = sugestao.sugestao_id AND cadastro.excluido = 0";
  sql += " GROUP BY sugestao.sugestao_id, cadastro.cadastro_id ORDER BY total_amigos_comum DESC ";

  if($obj.limit != null){
   sql     += " LIMIT "+$obj.limit;
  } else {
   sql     += " LIMIT 10";
  }

  $obj.query = sql;
  _app.sql.query($obj);
  return this;
 };

 /*
  * Metodo para retornar um ou varios registros sugeridos para o perfil
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
 var _getsolicitacaosugestao = function($obj){
  // Solicitações pendentes
  var sql = " SELECT geral.*, ";
  sql += " sd_c.nome, sd_c.genero, sd_c.sobrenome,sd_c.foto, sd_ci.nome as cidade, sd_es.nome as estado, sd_cr.nome as nome_relacionado, sd_cr.sobrenome as sobrenome_relacionado, sd_r.nome as relacionamento, sd_fu.nome as funcao";
  sql += " FROM ((SELECT 'solicitacao' as tipo, sd_s.solicitacao_id, sd_c.cadastro_id , 1 as qtde";
  sql += " FROM sd_perfil.sd_cadastro AS sd_c";
  sql += " INNER JOIN sd_perfil.sd_cadastro_solicitacao AS sd_s ON sd_s.cadastro_id = sd_c.cadastro_id";
  sql += " AND sd_s.cadastro_id NOT IN (SELECT cadastro_id_amizade FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id+")";
  sql += " AND sd_s.cadastro_id_solicitado = "+$obj.cadastro_id+"";
  sql += " LIMIT 30)";

  // Perfils sugeridos
  sql += " UNION (SELECT 'sugestao' as tipo, 0 as solicitacao_id, sugestao_id AS cadastro_id, count(*) as qtde ";
  sql += " FROM";
  sql += " (";
  sql += " SELECT ca.cadastro_id_amizade AS sugestao_id, ca.cadastro_id AS amigo_comum ";
  sql += " FROM sd_perfil.sd_cadastro_amizade AS ca";
  sql += " WHERE (";
  sql += " ca.cadastro_id NOT IN (SELECT cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id_amizade = "+$obj.cadastro_id+" UNION SELECT cadastro_id_amizade AS cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id+")";
  sql += " OR";
  sql += " ca.cadastro_id_amizade NOT IN (SELECT cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id_amizade = "+$obj.cadastro_id+" UNION SELECT cadastro_id_amizade AS cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id+")";
  sql += " )";
  sql += " AND ca.cadastro_id <> "+$obj.cadastro_id+"";
  sql += " AND ca.cadastro_id_amizade <> "+$obj.cadastro_id+"";
  sql += " AND ca.cadastro_id IN (SELECT cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id_amizade = "+$obj.cadastro_id+")";


  sql += " UNION";

  sql += " SELECT ca.cadastro_id AS sugestao_id, ca.cadastro_id_amizade AS amigo_comum ";
  sql += " FROM sd_perfil.sd_cadastro_amizade AS ca";
  sql += " WHERE (";
  sql += " ca.cadastro_id NOT IN (SELECT cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id_amizade = "+$obj.cadastro_id+" UNION SELECT cadastro_id_amizade AS cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id+")";
  sql += " OR";
  sql += " ca.cadastro_id_amizade NOT IN (SELECT cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id_amizade = "+$obj.cadastro_id+" UNION SELECT cadastro_id_amizade AS cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id+")";
  sql += " )";
  sql += " AND ca.cadastro_id <> "+$obj.cadastro_id+"";
  sql += " AND ca.cadastro_id_amizade <> "+$obj.cadastro_id+"";
  sql += " AND ca.cadastro_id_amizade IN (SELECT cadastro_id FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id_amizade = "+$obj.cadastro_id+")";

  sql += " ) AS sugestao, sd_perfil.sd_cadastro AS cadastro";

  sql += " WHERE ";
  sql += " cadastro.cadastro_id = sugestao.sugestao_id AND cadastro.excluido = 0";
  sql += " GROUP BY sugestao.sugestao_id, cadastro.cadastro_id ORDER BY qtde DESC";

  sql     += " LIMIT 10)) as geral ";
  sql += " LEFT JOIN sd_perfil.sd_cadastro AS sd_c ON geral.cadastro_id = sd_c.cadastro_id";
  sql += " LEFT JOIN public.sd_cidade AS sd_ci ON sd_c.cidade_id = sd_ci.cidade_id";
  sql += " LEFT JOIN public.sd_estado AS sd_es ON sd_ci.estado_id = sd_es.estado_id";
  sql += " LEFT JOIN sd_perfil.sd_cadastro AS sd_cr ON sd_c.cadastro_id_relacionamento = sd_cr.cadastro_id";
  sql += " LEFT JOIN sd_perfil.sd_relacionamento AS sd_r ON sd_c.relacionamento_id = sd_r.relacionamento_id";
  sql += " LEFT JOIN sd_perfil.sd_funcao AS sd_fu ON sd_c.funcao_id = sd_fu.funcao_id";
  sql += " ORDER BY qtde DESC ";

 //console.log(sql);
  //return $obj.onComplete("pt", sql);

  $obj.query = sql;
  _app.sql.query($obj);
  return this;
 };

 /*
  * Metodo para inserir um novo registro
  */
 var _insert = function($obj){
  var sql = "INSERT INTO sd_perfil.sd_cadastro";
  var i = 0;
  var colunas = "(";
  var valores = "(";
  for(obj in _params) {
   valor = String(_params[obj]);
   colunas += (i == 0)?obj:", "+obj;

   //Se o valor for uma função então eu retiro as aspas
   if(valor.indexOf("_sqlFunction") >= 0){
    valor = valor.replace("_sqlFunction", "");
   } else {
    valor = "'"+valor+"'";
   }

   valores += (i == 0)?valor:", "+valor;
   i++;
  }
  colunas += ")";
  valores += ")";

  sql+= colunas+" values "+valores+" RETURNING cadastro_id";
  $obj.query = sql;
  console.log(sql);

  _app.sql.query($obj);

  return this;
 };

 /*
  * Metodo para atualizar um registro
  */
 var _update = function($obj){
  var sql = "UPDATE sd_perfil.sd_cadastro SET ";
  var i = 0;
  var sets = '';

  for(var obj in $obj.update) {
   coluna = obj;
   if (typeof $obj.update[obj] === 'string' || $obj.update[obj] instanceof String){
    valor = $obj.update[obj].toString();
  }else{
    valor = $obj.update[obj];
  }

   if (typeof $obj.update[obj] === 'string' || $obj.update[obj] instanceof String){
     //Se o valor for uma função então eu retiro as aspas
     if(valor.indexOf("_sqlFunction") >= 0){
      valor = valor.replace("_sqlFunction", "");
     } else {
      valor = "'"+valor+"'";
     }
   }
   
   set = ""+obj+" = "+valor+"";
   sets += (i == 0)?set:", "+set;

   i++;
  }

  //O where é obrigatório
  sql += sets+" WHERE "+$obj.where+" RETURNING sd_perfil.sd_cadastro.cadastro_id";
  console.log(sql);

  $obj.query = sql;
  _app.sql.query($obj);

  return this;
 };

 return {get:_get, findAmizades: _findAmizades, getsugestao:_getsugestao, getsolicitacaosugestao:_getsolicitacaosugestao, sobre:_sobre, insert:_insert, update:_update, boot:_boot};
};
