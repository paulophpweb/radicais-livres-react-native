module.exports = new function(){

 /*
  * Metodo de inicializacao, como se fosse o construtor
  * _app pode ser instanciado em todos outros metodos
  */
 var _app = null;
 var _res = null;
 var _params = null;
 var _onComplete = null;
 var _boot = function($app, $params, $res, $onComplete){
  _app = $app;
  _params = $params;
  _res = $res;
  _onComplete = $onComplete;
  _app.sql.boot(_params, _res, _onComplete);
  return this;
 };

 /*
 * Metodo para retornar um ou varios registros
 * onComplete:Function Callback Sempre que quiser retornar algo utilize este
 * onError:Function Callback
 * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
 * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
 */
 var _getestado = function($obj){
	var sql = "SELECT "+
			  "		sd_e.* "+
			  "FROM  "+
			  "		public.sd_estado as sd_e ";

	if($obj.where != null){
		sql     += " WHERE "+$obj.where;
	}

	if($obj.order != null){
		sql     += " ORDER BY "+$obj.order;
	}

	if($obj.limit != null){
		sql     += " LIMIT "+$obj.limit;
	} else {
		sql     += " LIMIT 10";
	}

	if($obj.offset != null){
		  sql     += " OFFSET "+$obj.offset;
	}

	$obj.query = sql;
	_app.sql.query($obj);
	return this;
 };

  /*
 * Metodo para retornar um ou varios registros
 * onComplete:Function Callback Sempre que quiser retornar algo utilize este
 * onError:Function Callback
 * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
 * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
 */
 var _getfuncao = function($obj){
  var sql = "SELECT "+
        "   sd_f.* "+
        "FROM  "+
        "   sd_perfil.sd_funcao as sd_f ";

  if($obj.where != null){
    sql     += " WHERE "+$obj.where;
  }

  if($obj.order != null){
    sql     += " ORDER BY "+$obj.order;
  }

  if($obj.limit != null){
    sql     += " LIMIT "+$obj.limit;
  } else {
    sql     += " LIMIT 10";
  }

  if($obj.offset != null){
      sql     += " OFFSET "+$obj.offset;
  }

  $obj.query = sql;
  _app.sql.query($obj);
  return this;
 };

 /*
  * Metodo para retornar um ou varios registros sugeridos para o perfil
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
 var _getTotalNotificacaoSolicitacao = function($obj){
  // Solicitações pendentes
  var sql = "SELECT count(sd_c.cadastro_id) as total";
  sql += " FROM sd_perfil.sd_cadastro AS sd_c";
  sql += " INNER JOIN sd_perfil.sd_cadastro_solicitacao AS sd_s ON sd_s.cadastro_id = sd_c.cadastro_id";
  sql += " AND sd_s.cadastro_id NOT IN (SELECT cadastro_id_amizade FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id+")";
  sql += " AND sd_s.cadastro_id_solicitado = "+$obj.cadastro_id+"";
  sql += " LIMIT 30";

  $obj.query = sql;
  _app.sql.query($obj);
  return this;
 };

 /*
  * Metodo para retornar um ou varios registros
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
 var _getTotalNotificacaoChat = function($obj){
  var sql = "SELECT count(sd_custom.total) as total ";
  sql += " FROM sd_perfil.sd_cadastro AS sd_c";
  sql += " LEFT JOIN (SELECT cadastro_id, count(cadastro_id_amizade) as total FROM sd_chat.sd_chat_pessoa WHERE cadastro_id_amizade = "+$obj.cadastro_id+" AND st = 0 GROUP BY cadastro_id) as sd_custom ON sd_c.cadastro_id = sd_custom.cadastro_id ";
  sql += " WHERE sd_c.cadastro_id IS NOT NULL";
  if($obj.is_root == 0)
	  sql += " AND sd_c.cadastro_id IN (SELECT cadastro_id_amizade FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id+") OR sd_c.is_root = 1 ";


  $obj.query = sql;
  _app.sql.query($obj);
  return this;
 };

 /*
  * Metodo para retornar um ou varios registros
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
 var _getTotalNotificacaoMensagem = function($obj){
  var sql = "SELECT SUM(total) as total FROM (SELECT cadastro_id_amizade, count(cadastro_id) as total FROM sd_perfil.sd_mensagem WHERE cadastro_id = "+$obj.cadastro_id+" AND st = 0 GROUP BY cadastro_id_amizade) as custom ";

  $obj.query = sql;
  _app.sql.query($obj);
  return this;
 };

 /*
  * Metodo para retornar um ou varios registros
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
 var _getTotalNotificacaoNotificacao = function($obj){
  var sql = "SELECT SUM(total) as total FROM (SELECT cadastro_id_amizade, count(cadastro_id) as total FROM sd_perfil.sd_notificacao WHERE cadastro_id = "+$obj.cadastro_id+" AND st = 0 GROUP BY cadastro_id_amizade) as custom ";

  $obj.query = sql;
  _app.sql.query($obj);
  return this;
 };

 /*
  * Metodo para retornar um ou varios registros
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
  var _getcidade = function($obj){
 	var sql = "SELECT "+
 			  "		sd_c.* "+
 			  "FROM  "+
 			  "		public.sd_cidade as sd_c ";

 	if($obj.where != null){
 		sql     += " WHERE "+$obj.where;
 	}

 	if($obj.order != null){
 		sql     += " ORDER BY "+$obj.order;
 	}

 	if($obj.limit != null){
 		sql     += " LIMIT "+$obj.limit;
 	} else {
 		sql     += " LIMIT 10";
 	}

 	if($obj.offset != null){
 		  sql     += " OFFSET "+$obj.offset;
 	}

 	$obj.query = sql;
 	_app.sql.query($obj);
 	return this;
  };


 /*
  * Metodo para inserir um novo registro
  */
 var _insert = function($obj){
  var sql = "INSERT INTO public.sd_estado";
  var i = 0;
  var colunas = "(";
  var valores = "(";
  for(obj in _params) {
   valor = String(_params[obj]);
   colunas += (i == 0)?obj:", "+obj;

   //Se o valor for uma função então eu retiro as aspas
   if(valor.indexOf("_sqlFunction") >= 0){
    valor = valor.replace("_sqlFunction", "");
   } else {
    valor = "'"+valor+"'";
   }

   valores += (i == 0)?valor:", "+valor;
   i++;
  }
  colunas += ")";
  valores += ")";

  sql+= colunas+" values "+valores+" RETURNING estado_id";
  $obj.query = sql;
  _app.sql.query($obj);

  return this;
 };

 /*
  * Metodo para atualizar um registro
  */
 var _update = function($obj){
  var sql = "UPDATE public.sd_estado SET ";
  var i = 0;
  var sets = '';

  for(var obj in $obj.update) {
   coluna = obj;
   valor = $obj.update[obj].toString();

   //Se o valor for uma função então eu retiro as aspas
   if(valor.indexOf("_sqlFunction") >= 0){
    valor = valor.replace("_sqlFunction", "");
   } else {
    valor = "'"+valor+"'";
   }

   set = ""+obj+" = "+valor+"";
   sets += (i == 0)?set:", "+set;

   i++;
  }

  //O where é obrigatório
  sql += sets+" WHERE "+$obj.where+" RETURNING public.sd_estado.estado_id";

  $obj.query = sql;
  _app.sql.query($obj);

  return this;
 };

 return {insert:_insert,getestado:_getestado,getfuncao:_getfuncao,getcidade:_getcidade, update:_update, getTotalNotificacaoSolicitacao:_getTotalNotificacaoSolicitacao,getTotalNotificacaoChat:_getTotalNotificacaoChat,getTotalNotificacaoMensagem:_getTotalNotificacaoMensagem,getTotalNotificacaoNotificacao:_getTotalNotificacaoNotificacao, boot:_boot};
};
