module.exports = new function(){
	
	/*
  * Metodo de inicializacao, como se fosse o construtor
  * _app pode ser instanciado em todos outros metodos
  */
 var _app = null;
 var _res = null;
 var _params = null;
 var _onComplete = null;
 var _boot = function($app, $params, $res, $onComplete){
  _app = $app;
  _params = $params;
  _res = $res;
  _onComplete = $onComplete;
  _app.sql.boot(_params, _res, _onComplete);
  return this;
 };
		
	/*
	 * Metodo para retornar um ou varios registros
	 * onComplete:Function Callback Sempre que quiser retornar algo utilize este
	 * onError:Function Callback
	 * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
	 * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
	 */
	var _get = function($obj){
		var sql = "SELECT * FROM sd_perfil.sd_cadastro_solicitacao";
		
		if($obj.where != null){
			sql     += " WHERE "+$obj.where;
		}
		
		if($obj.order != null){
			sql     += " ORDER BY "+$obj.order;
		}
		
		if($obj.limit != null){
			sql     += " LIMIT "+$obj.limit;
		} else {
			sql     += " LIMIT 10";
		}	
		
		//return $obj.onComplete("pt", sql);
		
		$obj.query = sql;		
		_app.sql.query($obj);
		return this;
	};
	
	/*
	 * Metodo para retornar um ou varios registros
	 * onComplete:Function Callback Sempre que quiser retornar algo utilize este
	 * onError:Function Callback
	 * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
	 * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
	 */
	var _getsolicitacoes = function($obj){
		var sql = "SELECT sd_cs.cadastro_id, sd_cs.cadastro_id_solicitado, sd_cs.solicitacao_id, sd_c.foto, sd_c.nome, sd_c.sobrenome FROM sd_perfil.sd_cadastro_solicitacao AS sd_cs";
		sql += " LEFT JOIN sd_perfil.sd_cadastro AS sd_c ON sd_c.cadastro_id = sd_cs.cadastro_id";
		
		if($obj.where != null){
			sql     += " WHERE "+$obj.where;
		}
		
		if($obj.order != null){
			sql     += " ORDER BY "+$obj.order;
		}
		
		if($obj.limit != null){
			sql     += " LIMIT "+$obj.limit;
		} else {
			sql     += " LIMIT 10";
		}	
		
		//return $obj.onComplete("pt", sql);
		
		$obj.query = sql;		
		_app.sql.query($obj);
		return this;
	};
		
	/*
	 * Metodo para inserir um novo registro
	 */
	var _insert = function($obj){		
		var sql = "INSERT INTO sd_perfil.sd_cadastro_solicitacao";
		var i = 0;
		var colunas = "(";
		var valores = "(";
		for(obj in _params) {			
			valor = String(_params[obj]);			
			colunas += (i == 0)?obj:", "+obj;
			
			//Se o valor for uma função então eu retiro as aspas			
			if(valor.indexOf("_sqlFunction") >= 0){
				valor = valor.replace("_sqlFunction", "");
			} else {
				valor = "'"+valor+"'";
			}
			
			valores += (i == 0)?valor:", "+valor;
			i++;			
		}
		colunas += ")";
		valores += ")";
		
		sql+= colunas+" values "+valores+" RETURNING solicitacao_id";		
				
		$obj.query = sql;				
		_app.sql.query($obj);
		
		return this;		
	};
	
	/*
	 * Metodo para atualizar um registro
	 */
	var _update = function($obj){				
		var sql = "UPDATE sd_perfil.sd_cadastro_solicitacao SET ";
		var i = 0;		
		var sets = '';
		
		for(var obj in $obj.update) {						
			coluna = obj;
			valor = $obj.update[obj].toString();
			
			//Se o valor for uma função então eu retiro as aspas			
			if(valor.indexOf("_sqlFunction") >= 0){
				valor = valor.replace("_sqlFunction", "");
			} else {
				valor = "'"+valor+"'";
			}
						
			set = ""+obj+" = "+valor+"";									
			sets += (i == 0)?set:", "+set;
			
			i++;
		}		
		
		//O where é obrigatório
		sql += sets+" WHERE "+$obj.where+" RETURNING sd_perfil.sd_cadastro_solicitacao.solicitacao_id";								
		$obj.query = sql;		
		_app.sql.query($obj);
		
		return this;				
	};
	

	 /*
	  * Metodo para atualizar um registro
	  */
	 var _remover = function($obj){    
	  var sql = "DELETE FROM sd_perfil.sd_cadastro_solicitacao";
	  	  
	  //O where é obrigatório
	  sql += " WHERE "+$obj.where+" RETURNING sd_perfil.sd_cadastro_solicitacao.solicitacao_id";
	  
	  //return $obj.onComplete("pt",sql);
	  
	  $obj.query = sql;  
	  _app.sql.query($obj);
	  
	  return this;    
	 };
		
	return {get:_get, getsolicitacoes:_getsolicitacoes, insert:_insert, update:_update, remover: _remover, boot:_boot};
};
