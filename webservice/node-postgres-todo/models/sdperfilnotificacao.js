module.exports = new function(){
	
	/*
  * Metodo de inicializacao, como se fosse o construtor
  * _app pode ser instanciado em todos outros metodos
  */
 var _app = null;
 var _res = null;
 var _params = null;
 var _onComplete = null;
 var _boot = function($app, $params, $res, $onComplete){
  _app = $app;
  _params = $params;
  _res = $res;
  _onComplete = $onComplete;
  _app.sql.boot(_params, _res, _onComplete);
  return this;
 };
		
 /*
	 * Metodo para retornar um ou varios registros
	 * onComplete:Function Callback Sempre que quiser retornar algo utilize este
	 * onError:Function Callback
	 * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
	 * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
	 */
	var _get = function($obj){
		var sql = "SELECT "+
				  "		sp_n.*, "+
				  "		sp_c.nome, "+
				  "		sp_c.sobrenome, "+
				  "		sp_c.foto, "+
				  "		sp_c.genero, "+
				  "		sp_cc.nome as nome_amigo, "+
				  "		sp_cc.sobrenome as sobrenome_amigo, "+
				  "		sp_cc.foto as foto_amigo, "+
				  "		sp_cc.genero as genero_amigo "+
				  "FROM  "+
				  "		sd_perfil.sd_notificacao as sp_n "+
				  "LEFT JOIN "+
				  "		sd_perfil.sd_cadastro sp_cc ON sp_cc.cadastro_id = sp_n.cadastro_id_amizade "+
				  "LEFT JOIN "+
				  "		sd_perfil.sd_cadastro sp_c ON sp_c.cadastro_id = sp_n.cadastro_id ";
		
		if($obj.where != null){
			sql     += " WHERE "+$obj.where;
		}
		
		if($obj.order != null){
			sql     += " ORDER BY "+$obj.order;
		}
		
		if($obj.limit != null){
			sql     += " LIMIT "+$obj.limit;
		} else {
			sql     += " LIMIT 10";
		}
		
		if($obj.offset != null){
			  sql     += " OFFSET "+$obj.offset;
		}
		
		sql += "; UPDATE sd_perfil.sd_notificacao SET st = 1, data_recebido = '"+$obj.ult_login+"'  WHERE cadastro_id = "+$obj.cadastro_id+" AND data_recebido is null ; ";
		
		$obj.query = sql;		
		_app.sql.query($obj);
		return this;
	};
		
	/*
	 * Metodo para inserir um novo registro
	 */
	var _insert = function($obj){
		var sql = "INSERT INTO sd_perfil.sd_notificacao";
		var i = 0;
		var colunas = "(";
		var valores = "(";
		for(obj in _params) {			
			valor = String(_params[obj]);			
			colunas += (i == 0)?obj:", "+obj;
			
			//Se o valor for uma função então eu retiro as aspas			
			if(valor.indexOf("_sqlFunction") >= 0){
				valor = valor.replace("_sqlFunction", "");
			} else {
				valor = "'"+valor+"'";
			}
			
			valores += (i == 0)?valor:", "+valor;
			i++;			
		}
		colunas += ")";
		valores += ")";
		
		sql+= colunas+" values "+valores+" RETURNING notificacao_id";	
		
		//return $obj.onComplete("pt",sql);
		
		$obj.query = sql;				
		_app.sql.query($obj);
		
		return this;		
	};
		
	return {get:_get, insert:_insert, boot:_boot};
};
