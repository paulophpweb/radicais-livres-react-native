module.exports = new function(){
	
	/*
	  * Metodo de inicializacao, como se fosse o construtor
	  * _app pode ser instanciado em todos outros metodos
	  */
	 var _app = null;
	 var _res = null;
	 var _params = null;
	 var _onComplete = null;
	 var _boot = function($app, $params, $res, $onComplete){
	  _app = $app;
	  _params = $params;
	  _res = $res;
	  _onComplete = $onComplete;
	  _app.sql.boot(_params, _res, _onComplete);
	  return this;
	 };
		
	/*
	 * Metodo para retornar um ou varios registros
	 * onComplete:Function Callback Sempre que quiser retornar algo utilize este
	 * onError:Function Callback
	 * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
	 * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
	 */
	var _get = function($obj){
		var sql = "SELECT * FROM sd_perfil.sd_cadastro_amizade";
		
		if($obj.where != null){
			sql     += " WHERE "+$obj.where;
		}
		
		if($obj.order != null){
			sql     += " ORDER BY "+$obj.order;
		}
		
		if($obj.limit != null){
			sql     += " LIMIT "+$obj.limit;
		} else {
			sql     += " LIMIT 10";
		}
		
		if($obj.offset != null){
			sql     += " OFFSET "+$obj.offset;
		}	
		
		$obj.query = sql;		
		_app.sql.query($obj);
		return this;
	};
	
	/*
	 * Metodo para retornar um ou varios registros
	 * onComplete:Function Callback Sempre que quiser retornar algo utilize este
	 * onError:Function Callback
	 * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
	 * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
	 */
	var _getamizades = function($obj){
		var sql = "SELECT sd_c.*, sd_ci.nome as cidade, sd_es.nome as estado, sd_cr.nome as nome_relacionado, sd_cr.sobrenome as sobrenome_relacionado, ";
		sql += " sd_r.nome as relacionamento, sd_fu.nome as funcao, ";
		sql += " CASE WHEN sd_cb.cadastro_id > 0 THEN 1 ELSE 0 END AS bloqueado";
		//sql += "(SELECT count(cadastro_id) FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = sd_ca.cadastro_id_amizade) as qtde_amigo";
		sql += " FROM sd_perfil.sd_cadastro_amizade AS sd_ca";
		sql += " LEFT JOIN sd_perfil.sd_cadastro AS sd_c ON sd_c.cadastro_id = sd_ca.cadastro_id_amizade";
		sql += " LEFT JOIN sd_perfil.sd_cadastro_bloqueio AS sd_cb ON sd_cb.cadastro_id = sd_ca.cadastro_id AND sd_cb.cadastro_id_amizade = sd_c.cadastro_id";
		sql += " LEFT JOIN public.sd_cidade AS sd_ci ON sd_c.cidade_id = sd_ci.cidade_id";
  		sql += " LEFT JOIN public.sd_estado AS sd_es ON sd_ci.estado_id = sd_es.estado_id";
  		sql += " LEFT JOIN sd_perfil.sd_cadastro AS sd_cr ON sd_c.cadastro_id_relacionamento = sd_cr.cadastro_id";
  		sql += " LEFT JOIN sd_perfil.sd_relacionamento AS sd_r ON sd_c.relacionamento_id = sd_r.relacionamento_id";
  		sql += " LEFT JOIN sd_perfil.sd_funcao AS sd_fu ON sd_c.funcao_id = sd_fu.funcao_id";
		
		if($obj.where != null){
			sql     += " WHERE "+$obj.where;
		}
		
		if($obj.order != null){
			sql     += " ORDER BY "+$obj.order;
		}
		
		if($obj.limit != null){
			sql     += " LIMIT "+$obj.limit;
		} else {
			sql     += " LIMIT 10";
		}
		
		if($obj.offset != null){
			  sql     += " OFFSET "+$obj.offset;
		}
		//console.log(sql);
		//return $obj.onComplete("pt", sql);
		
		$obj.query = sql;		
		_app.sql.query($obj);
		return this;
	};
		
	/*
	 * Metodo para inserir um novo registro
	 */
	var _insert = function($obj){		
		var sql = "INSERT INTO sd_perfil.sd_cadastro_amizade";
		var i = 0;
		var colunas = "(";
		var valores = "(";
		for(obj in _params) {			
			valor = String(_params[obj]);			
			colunas += (i == 0)?obj:", "+obj;
			
			//Se o valor for uma função então eu retiro as aspas			
			if(valor.indexOf("_sqlFunction") >= 0){
				valor = valor.replace("_sqlFunction", "");
			} else {
				valor = "'"+valor+"'";
			}
			
			valores += (i == 0)?valor:", "+valor;
			i++;			
		}
		colunas += ")";
		valores += ")";
		
		sql+= colunas+" values "+valores+" RETURNING cadastro_amizade_id";		
			
		$obj.query = sql;				
		_app.sql.query($obj);
		
		return this;		
	};
	
		
	return {get:_get, getamizades:_getamizades, insert:_insert, boot:_boot};
};
