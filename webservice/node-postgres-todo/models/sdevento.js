module.exports = new function(){

 /*
  * Metodo de inicializacao, como se fosse o construtor
  * _app pode ser instanciado em todos outros metodos
  */
 var _app = null;
 var _res = null;
 var _params = null;
 var _onComplete = null;
 var _boot = function($app, $params, $res, $onComplete){
  _app = $app;
  _params = $params;
  _res = $res;
  _onComplete = $onComplete;
  _app.sql.boot(_params, _res, _onComplete);
  return this;
 };

 /*
  * Metodo para retornar um ou varios registros
  * onComplete:Function Callback Sempre que quiser retornar algo utilize este
  * onError:Function Callback
  * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
  * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
  */
 var _get = function($obj){
	 	// select
		var sql =
		"SELECT "+
			"sd_e.* "+
			", sd_c.nome as cidade "+
			", sd_es.nome as estado "+
			", sd_ca.nome as cadastro_nome "+
			", sd_ca.sobrenome as cadastro_sobrenome "+
			", sd_ep.total "+
			", CASE WHEN sd_com.tipo = 'comparecerei'  THEN 1 ELSE 0 END AS comparecerei "+
			", CASE WHEN sd_com.tipo = 'talvez'  THEN 1 ELSE 0 END AS talvez "+
			", CASE WHEN sd_com.tipo = 'nao_posso_ir' THEN 1 ELSE 0 END AS nao_posso_ir "+
		"FROM "+
			"sd_evento.sd_evento as sd_e "+
			"LEFT JOIN sd_perfil.sd_cadastro as sd_ca on sd_ca.cadastro_id = sd_e.cadastro_id "+
			"LEFT JOIN public.sd_cidade as sd_c on sd_c.cidade_id = sd_e.cidade_id "+
			"LEFT JOIN public.sd_estado as sd_es on sd_es.estado_id = sd_c.estado_id "+
			"LEFT JOIN (SELECT " +
			"					sd_e.evento_id, " +
			"					count(case when sd_ep.tipo = 'comparecerei' then sd_ep.cadastro_id end) as total " +
			" 			FROM " +
			"					sd_evento.sd_evento as sd_e " +
			"			LEFT JOIN sd_evento.sd_evento_pessoa AS sd_ep " +
			"					ON sd_e.evento_id = sd_ep.evento_id " +
			"			WHERE " +
			"					sd_e.cadastro_id IS NOT NULL group by sd_e.evento_id " +
			"			) as sd_ep ON (sd_e.evento_id = sd_ep.evento_id) "+
			"LEFT JOIN (SELECT sd_ep.evento_id, sd_ep.tipo  FROM sd_evento.sd_evento_pessoa AS sd_ep WHERE sd_ep.cadastro_id = "+$obj.cadastro_id+"  ) as sd_com ON (sd_com.evento_id = sd_ep.evento_id) "+
		"WHERE "+
			"sd_e.evento_id IS NOT NULL "+
			($obj.is_root == 0 ? "AND (sd_e.cadastro_id IN (SELECT cadastro_id_amizade FROM sd_perfil.sd_cadastro_amizade WHERE cadastro_id = "+$obj.cadastro_id+") OR sd_ca.is_root = 1 OR sd_ca.cadastro_id = "+$obj.cadastro_id+" ) " : "");
		if($obj.latitudeUm != null && $obj.latitudeTres != null && $obj.longitudeUm != null && $obj.longitudeDois != null){
			  sql += " AND (ABS(sd_e.latitude::numeric) >= "+Math.abs($obj.latitudeUm)+"  AND ABS(sd_e.latitude::numeric) <= "+Math.abs($obj.latitudeTres)+")";
			  sql += " AND (ABS(sd_e.longitude::numeric) <= "+Math.abs($obj.longitudeUm)+"  AND ABS(sd_e.longitude::numeric) >= "+Math.abs($obj.longitudeDois)+")";
		  }

		if($obj.nome_evento != null){
			  sql += " AND upper(remove_acento(sd_e.titulo)) like upper(remove_acento('%"+$obj.nome_evento+"%')) ";
		  }

		if($obj.tipo == "proximo")
			sql += " AND sd_e.data_fim >= now() ";
		if($obj.tipo == "anterior")
			sql += " AND sd_e.data_fim <= now() ";

		if($obj.where != null){
			  sql     += $obj.where;
		}

		if($obj.order != null){
			sql     += " ORDER BY "+$obj.order;
		}

		if($obj.limit != null){
			sql     += " LIMIT "+$obj.limit;
		} else {
			sql     += " LIMIT 10";
		}

		if($obj.offset != null){
			  sql     += " OFFSET "+$obj.offset;
		}

		$obj.query = sql;
		_app.sql.query($obj);
		return this;
 };


 /*
  * Metodo para inserir um novo registro
  */
 var _insert = function($obj){
  var sql = "INSERT INTO sd_evento.sd_evento";
  var i = 0;
  var colunas = "(";
  var valores = "(";
  for(obj in _params) {
   valor = String(_params[obj]);
   colunas += (i == 0)?obj:", "+obj;

   //Se o valor for uma função então eu retiro as aspas
   if(valor.indexOf("_sqlFunction") >= 0){
    valor = valor.replace("_sqlFunction", "");
   } else {
    valor = "'"+valor+"'";
   }

   valores += (i == 0)?valor:", "+valor;
   i++;
  }
  colunas += ")";
  valores += ")";

  sql+= colunas+" values "+valores+" RETURNING evento_id";
  $obj.query = sql;
  _app.sql.query($obj);

  return this;
 };

 /*
  * Metodo para atualizar um registro
  */
 var _update = function($obj){
  var sql = "UPDATE sd_evento.sd_evento SET ";
  var i = 0;
  var sets = '';

  for(var obj in $obj.update) {
   coluna = obj;
   valor = $obj.update[obj].toString();

   //Se o valor for uma função então eu retiro as aspas
   if(valor.indexOf("_sqlFunction") >= 0){
    valor = valor.replace("_sqlFunction", "");
   } else {
    valor = "'"+valor+"'";
   }

   set = ""+obj+" = "+valor+"";
   sets += (i == 0)?set:", "+set;

   i++;
  }

  //O where é obrigatório
  sql += sets+" WHERE "+$obj.where+" RETURNING sd_evento.sd_evento.evento_id";

  $obj.query = sql;
  _app.sql.query($obj);

  return this;
 };

 return {get:_get, insert:_insert, update:_update, boot:_boot};
};
