module.exports = new function(){

 /*
  * Metodo de inicializacao, como se fosse o construtor
  * _app pode ser instanciado em todos outros metodos
  */
 var _app = null;
 var _res = null;
 var _params = null;
 var _onComplete = null;
 var _boot = function($app, $params, $res, $onComplete){
  _app = $app;
  _params = $params;
  _res = $res;
  _onComplete = $onComplete;
  _app.sql.boot(_params, _res, _onComplete);
  return this;
 };


 /*
  * Metodo para inserir um novo registro
  */
 var _insert = function($obj){
  var sql = "INSERT INTO sd_evento.sd_evento_pessoa";
  var i = 0;
  var colunas = "(";
  var valores = "(";
  for(obj in _params) {
   valor = String(_params[obj]);
   colunas += (i == 0)?obj:", "+obj;

   //Se o valor for uma função então eu retiro as aspas
   if(valor.indexOf("_sqlFunction") >= 0){
    valor = valor.replace("_sqlFunction", "");
   } else {
    valor = "'"+valor+"'";
   }

   valores += (i == 0)?valor:", "+valor;
   i++;
  }
  colunas += ")";
  valores += ")";

  sql+= colunas+" values "+valores+" RETURNING evento_pessoa_id";

  $obj.query = sql;
  _app.sql.query($obj);

  return this;
 };

 /*
  * Metodo para atualizar um registro
  */
 var _update = function($obj){
  var sql = "UPDATE sd_evento.sd_evento_pessoa SET ";
  var i = 0;
  var sets = '';

  for(var obj in $obj.update) {
   coluna = obj;
   valor = $obj.update[obj].toString();

   //Se o valor for uma função então eu retiro as aspas
   if(valor.indexOf("_sqlFunction") >= 0){
    valor = valor.replace("_sqlFunction", "");
   } else {
    valor = "'"+valor+"'";
   }

   set = ""+obj+" = "+valor+"";
   sets += (i == 0)?set:", "+set;

   i++;
  }

  //O where é obrigatório
  sql += sets+" WHERE "+$obj.where+" RETURNING sd_evento.sd_evento_pessoa.evento_pessoa_id";

  $obj.query = sql;
  _app.sql.query($obj);

  return this;
 };

 return {insert:_insert, update:_update, boot:_boot};
};
