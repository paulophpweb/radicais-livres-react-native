module.exports = new function(){

	/*
  * Metodo de inicializacao, como se fosse o construtor
  * _app pode ser instanciado em todos outros metodos
  */
 var _app = null;
 var _res = null;
 var _params = null;
 var _onComplete = null;
 var _boot = function($app, $params, $res, $onComplete){
  _app = $app;
  _params = $params;
  _res = $res;
  _onComplete = $onComplete;
  _app.sql.boot(_params, _res, _onComplete);
  return this;
 };

	/*
	 * Metodo para retornar um ou varios registros
	 * onComplete:Function Callback Sempre que quiser retornar algo utilize este
	 * onError:Function Callback
	 * $obj.params:Object $obj.req.params.XXX Ex: $obj.req.params.lang
	 * $obj.req:Object O req esta sendo utilizado apenas em uteis.httpError
	 */
	var _get = function($obj){
		var sql = "SELECT sd_i.* FROM sd_perfil.sd_igreja sd_i ";

		sql += " WHERE sd_i.igreja_id IS NOT NULL ";
		if($obj.latitudeUm != null && $obj.latitudeTres != null && $obj.longitudeUm != null && $obj.longitudeDois != null){
			  sql += " AND (ABS(sd_i.latitude::numeric) >= "+Math.abs($obj.latitudeUm)+"  AND ABS(sd_i.latitude::numeric) <= "+Math.abs($obj.latitudeTres)+")";
			  sql += " AND (ABS(sd_i.longitude::numeric) <= "+Math.abs($obj.longitudeUm)+"  AND ABS(sd_i.longitude::numeric) >= "+Math.abs($obj.longitudeDois)+")";
		  }

		if($obj.igreja_id != null){
			  sql += " AND sd_i.igreja_id = "+$obj.igreja_id;
		}

		if($obj.where != null){
			sql     += " "+$obj.where;
		}

		if($obj.order != null){
			sql     += " ORDER BY "+$obj.order;
		}

		if($obj.limit != null){
			sql     += " LIMIT "+$obj.limit;
		} else {
			sql     += " LIMIT 10";
		}	
		console.log(sql);

		$obj.query = sql;
		_app.sql.query($obj);
		return this;
	};

	return {get:_get, boot:_boot};
};
