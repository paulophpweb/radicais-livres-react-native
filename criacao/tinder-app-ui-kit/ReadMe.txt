                          
                              Tinder Mockup for Sketch
			       Version 1.0, January 2017



Hello there!

Thanks for downloading my Tinder Mockup ;)

Feel free to use it anywhere in both personal and commercial projects you like!

Please DO NOT however redistribute, resell, lease, license, sub-license or offer this resource to any third party. This includes uploading the resource to another website, marketplace or media-sharing tool, and offering the resource as a separate attachment from any of your work without my previous authorization.

For any suggestions, questions, and/or comments please contact me at: gilbertodlgarza@gmail.com


Regards, G.
